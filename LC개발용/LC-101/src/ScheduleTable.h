#ifndef __SCHEDULE_TIME_H__
#define __SCHEDULE_TIME_H__


#include <stdio.h>
#include <string.h>  		// strcmp(), strcpy(), strlen() 함수등 주로 문자열과 관련된 함수사용시
#include <stdlib.h>  		// malloc() 함수 사용
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

#include "Util.h"
#include "Uart.h"

#define MaxNumOfScheduleInfo 15
#define MaxNumOfCircuit	16

#define DEFAULT 	0
#define TODAY		1
#define TOMORROW	2

typedef struct
{
	uint8 onTime_10m;
	uint8 ctrlMode;
}Schedule_Info;

typedef struct {
	Schedule_Info scheduleInfo[MaxNumOfCircuit][MaxNumOfScheduleInfo];
	uint8 size[MaxNumOfCircuit];
}Schedule_Table_st;

Schedule_Table_st defaultScheduleTable;
Schedule_Table_st todayScheduleTable;
Schedule_Table_st tomorrowScheduleTable;

void initScheduleTimeTable();
int addSchedule(uint8 scheduleType, uint8 circuitId, uint8 hour, uint8 minute, uint8 ctrlMode);
int addScheduleList(uint8 scheduleType, uint8 circuitId, uint8 numOfSchedule, uint8 hour[], uint8 minute[], uint8 ctrlMode[]);
uint8 isContainScheduleAt(uint8 scheduleType, uint8 circuitId, uint8 hour, uint8 minute);
void schedule_WriteToEEPRom(uint8 scheduleType, uint8 circuitId, uint8 index);
uint8 getControlModeAt(uint8 scheduleType, uint8 circuitId, uint8 hour, uint8 minute);
void resetSchedule(uint8 scheduleType, uint8 circuitId);

#endif

