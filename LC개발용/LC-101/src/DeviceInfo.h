#ifndef __DEVICE_INFO_H__
#define __DEVICE_INFO_H__


#include <stdio.h>
#include <string.h>  		// strcmp(), strcpy(), strlen() 함수등 주로 문자열과 관련된 함수사용시
#include <stdlib.h>  		// malloc() 함수 사용
#include <avr/pgmspace.h>
#include "Util.h"
#include <avr/eeprom.h>

#define NumOfChannels 16
#define OFF								0x00
#define ON								0x01
#define SENSOR							0x02
#define SCHEDULE						0x03
#define DIMMING_0						0x10
#define DIMMING_1						0x11
#define DIMMING_2						0x12
#define DIMMING_3						0x13
#define DIMMING_4						0x14
#define DIMMING_5						0x15
#define DIMMING_6						0x16
#define DIMMING_7						0x17
#define DIMMING_8						0x18
#define DIMMING_9						0x19
#define DIMMING_10						0x1A

typedef struct DeviceInfo
{
	uint16 nwkAddr;					// nwk_id : Zigbee_id (Gateway)
	uint16 deviceType;
	uint16 ieeeId;					// IEEE	: Node_id (Gateway)	//
	uint8 deviceVersion;
	uint8 fwVersion;
	uint8 cnt;
}device_Info;

#define MaxNumOfDevice 60
typedef struct {	
	device_Info deviceInfo[MaxNumOfDevice];
	uint8 size;
} DeviceInfoTable_st;

DeviceInfoTable_st deviceInfoTable;

typedef struct SSRInfo
{
	uint8 ctrlMode[NumOfChannels];
	uint8 state[NumOfChannels];
	uint16 expirationTime[NumOfChannels]; //timer_cnt가 expirationTime이 되면 꺼짐.0: 시간 증가 안함. 항상 꺼짐. 0xffff 항상 켜짐. 시간 증가 안함.else 시간 증가.
	uint16 timerCnt[NumOfChannels];  
	uint8 onCtrlMode[NumOfChannels];;
	uint8 offCtrlMode[NumOfChannels];;
}SSR_State_Info;

SSR_State_Info tmp_SSRInfo;
SSR_State_Info tmp_PrevSSRInfo;

void handleDeviceControl(uint16 nodeId,  int circuitId, uint8 ctrlMode);
void deviceInfo_UpdateSSRInfo();
int deviceInfo_IsUpdatedSSRInfo();
//--------------------------------------------------------------------------------------------//
void initDeviceTable();

void ssrInfo_WriteToEEPRom();
void ssrInfo_WriteToEEPRomOfIndex(uint8 ssrId);


device_Info *findNodeById(uint16 node_Id);
int isContainNodeOfNwkAddr(uint16 nwkAddr);
int isContainNodeOfId(uint16 node_Id);
int isContainNode(uint16 node_Id, uint16 nwkAddr);

device_Info *addDeviceTable(uint16 node_Id, uint16 nwkAddr, uint16 node_Type, uint8 device_version, uint8 fw_version);
int updateDeviceTable(uint16 node_Id, uint16 nwkAddr, uint16 node_Type, uint8 device_version, uint8 fw_version);


void removeNode(uint16 node_Id);
void removeDeviceTable();


uint8 countDeviceTable(uint16 node_Type);
uint8 countDevice();


uint16 getNetAddr(uint16 node_Id);
uint16 getNodeId(uint16 net_Addr);

void printDeviceTable();


#endif

