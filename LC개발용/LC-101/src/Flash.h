#ifndef FLASH__H
#define FLASH__H

#include "util.h"


void flash_init();

void flash_writeUint8(uint16 address, uint8 data);
uint8 flash_readUint8(uint16 address);

void flash_writeUint16(uint16 address, uint16 data);
uint16 flash_readUint16(uint16 address);

void flash_writeUint32(uint16 address, uint32 data);
uint32 flash_readUint32(uint16 address);

void flash_writeBytes(uint16 address, uint8 length, uint8* data);
uint8 flash_readBytes(uint16 address, uint8 *data, uint8 length);

uint8 flash_bufferReadUint8(uint16 address);

uint8 flash_readStatus();

#endif
