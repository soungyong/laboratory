#ifndef __XNET_PROTOCOL_H__
#define __XNET_PROTOCOL_H__


#include <string.h>
#include "Util.h"
#include "NCProtocol.h"
#include "plcs_protocol.h"
#include "Xcps.h"
#include "ZRMProtocol.h"


// --------------- XNetMessage Handler --------------- //
void XNetHandler(uint16 zigbee_Id, uint8 buff[], int buff_length);

void XNetCommandFromGateway(uint8 msg[], int buff_length);
void XNetHandlerFromZigbee(uint16 srcNetAddr, uint8 msg[], int buff_length);

void sendServerMessage(uint8 *msg, uint8 buff_length);

char sendStackedServerMessage();


#endif

