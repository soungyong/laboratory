#ifndef __TESTDEVICEPROTOCOL_H
#define __TESTDEVICEPROTOCOL_H

#include "Util.h"
#include "NCProtocol.h"
#include "DeviceInfo.h"
#include "SSRControl.h"

#define TEST_DEVICE_PROTOCOL_ID			0x90

#define TEST_DEVICE_LC_FIRMWARE_VERSION_REQ		0x20
#define TEST_DEVICE_LC_FIRMWARE_VERSION_RES		0x21
#define TEST_DEVICE_LC_CONTROLINFO_REQ			0x22
#define TEST_DEVICE_LC_CONTROLINFO_RES			0x23
#define TEST_DEVICE_LC_CONTROL_REQ				0x24
#define TEST_DEVICE_LC_CONTROL_RES				0x25
#define TEST_DEVICE_LC_POWERMETER_REQ			0x26
#define TEST_DEVICE_LC_POWERMETER_RES			0x27

#define TEST_DEVICE_LC_DEVICELIST_SIZE_REQ		0x30
#define TEST_DEVICE_LC_DEVICELIST_SIZE_RES		0x31
#define TEST_DEVICE_LC_DEVICEINFO_REQ			0x32
#define TEST_DEVICE_LC_DEVICEINFO_RES			0x33

#define TEST_DEVICE_SENSOR_LEVEL_SET			0x40
#define TEST_DEVICE_SENSOR_LEVEL_RES			0x41
#define TEST_DEVICE_SENSOR_STATE_INFO_REQ		0x42
#define TEST_DEVICE_SENSOR_STATE_INFO_RES		0x43

#define TEST_DEVICE_DMX_CONTROL_REQ				0x50
#define TEST_DEVICE_DMX_CONTROL_RES				0x51

#define TEST_DEVICE_LC_SENSOR_MAPPING_SIZE_REQ	0x60
#define TEST_DEVICE_LC_SENSOR_MAPPING_SIZE_RES	0x61
#define TEST_DEVICE_LC_DIMMER_MAPPING_SIZE_REQ	0x62
#define TEST_DEVICE_LC_DIMMER_MAPPING_SIZE_RES	0x63
#define TEST_DEVICE_LC_SENSOR_MAPPING_INFO_REQ	0x64
#define TEST_DEVICE_LC_SENSOR_MAPPING_INFO_RES	0x65
#define TEST_DEVICE_LC_DIMMER_MAPPING_INFO_REQ	0x66
#define TEST_DEVICE_LC_DIMMER_MAPPING_INFO_RES	0x67

#define TEST_DEVICE_LC_SENSOR_MAPPING_REQ		0x70
#define TEST_DEVICE_LC_SENSOR_MAPPING_RES		0x71
#define TEST_DEVICE_LC_DIMMER_MAPPING_REQ		0x72
#define TEST_DEVICE_LC_DIMMER_MAPPING_RES		0x73
#define TEST_DEVICE_LC_SENSOR_MAPPING_RESET_REQ	0x74
#define TEST_DEVICE_LC_SENSOR_MAPPING_RESET_RES	0x75
#define TEST_DEVICE_LC_DIMMER_MAPPING_RESET_REQ	0x76
#define TEST_DEVICE_LC_DIMMER_MAPPING_RESET_RES	0x77




uint8 getSeqNumGeneratorForTCP();
uint8 plcs_GetTCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len);
uint8 plcs_GetTCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum);


void processNCP_TestDevice(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_StateInfo_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_FirmwareVersion_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_ControlInfo_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_Control_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_Powermeter_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_DeviceList_Size_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_DeviceInfo_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_SensorLevel_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_DMX_Control_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_SensorMappingSize_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_DimmerMappingSize_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_SensorMappingInfo_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_DimmerMappingInfo_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_SensorMapping_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_DimmerMapping_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_SensorMappingReset_Req(uint16 srcNetAddr, uint8 msg[], int length);
void processNCP_TestDevice_DimmerMappingReset_Req(uint16 srcNetAddr, uint8 msg[], int length);



void TestDevice_FirmwareVersion_Res(uint16 seqNum,uint16 nodeId, uint8 version);
void TestDevice_ControlInfo_Res(uint16 seqNum, uint16 nodeId);
void TestDevice_Control_Res(uint16 seqNum, uint16 nodeId, uint8 result);
void TestDevice_Powermeter_Res(uint16 seqNum, uint16 nodeId, uint32 value);
void TestDevice_DeviceList_Size_Res(uint16 seqNum, uint16 nodeId, uint8 numOfDevice);
void TestDevice_DeviceInfo_Res(uint16 seqNum, uint16 nodeId, uint16 deviceId, uint16 type, uint8 version);
void TestDevice_SensorLevel_Res(uint16 seqNum, uint16 nodeId, uint8 result);
void TestDevice_DMX_Control_Res(uint16 seqNum, uint16 nodeId, uint8 result);
void TestDevice_SensorMappingSize_Res(uint16 seqNum, uint16 nodeId);
void TestDevice_DimmerMappingSize_Res(uint16 seqNum, uint16 nodeId);
void TestDevice_SensorMappingInfo_Res(uint16 seqNum, uint16 nodeId, uint8 index);
void TestDevice_DimmerMappingInfo_Res(uint16 seqNum, uint16 nodeId, uint8 index);
void TestDevice_SensorMapping_Res(uint16 seqNum, uint16 nodeId, uint8 result);
void TestDevice_DimmerMapping_Res(uint16 seqNum, uint16 nodeId, uint8 result);
void TestDevice_SensorMappingReset_Res(uint16 seqNum, uint16 nodeId, uint8 result);
void TestDevice_DimmerMappingReset_Res(uint16 seqNum, uint16 nodeId, uint8 result);

#endif
