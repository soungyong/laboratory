#include "Debug.h"
#include <avr/eeprom.h>


DebugInfo_st debugInfo EEMEM;

void debug_UpdateLog(){
	eeprom_write_block(&debug_DebugInfo, &debugInfo, sizeof(DebugInfo_st));	
}

void debug_ReadLog(){
	eeprom_read_block(&debug_DebugInfo, &debugInfo, sizeof(DebugInfo_st));
}

void debug_Reset(){
	uint8 i=0;
	debug_DebugInfo.rebootCnt=0;
	debug_DebugInfo.serverConnectionCnt=0;
	for(i=0; i <5; i++){
		debug_DebugInfo.lastRebootTime[i]=0;
		debug_DebugInfo.lastServerConnectionTime[i]=0;
	}

	debug_UpdateLog();
}

void debug_UpdateReboot(){	
	debug_DebugInfo.rebootCnt++;
	
	debug_DebugInfo.lastRebootTime[0] = Hour;
	debug_DebugInfo.lastRebootTime[1] = Min;	

	debug_UpdateLog();
}

void debug_UpdateConnection(){
	//debug_DebugInfo.serverConnectionCnt++;
	
	debug_DebugInfo.lastServerConnectionTime[0] = Hour;
	debug_DebugInfo.lastServerConnectionTime[1] = Min;
	
	debug_UpdateLog();
}
