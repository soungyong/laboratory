#include "MappingTable.h"
#include "ZRMProtocol.h"
#include "PLCS_Protocol.h"
#include "TestDeviceProtocol.h"
#include <avr/wdt.h>

/////
uint8 tmp_Buff[32];
/////

void processNCP_TestDevice(uint16 srcNetAddr, uint8 msg[], int length) {
	uint8 msgType = msg[8];

	switch (msgType) {
	case TEST_DEVICE_LC_FIRMWARE_VERSION_REQ:
		processNCP_TestDevice_FirmwareVersion_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_CONTROLINFO_REQ:
		processNCP_TestDevice_ControlInfo_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_CONTROL_REQ:
		processNCP_TestDevice_Control_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DEVICELIST_SIZE_REQ:
		processNCP_TestDevice_DeviceList_Size_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DEVICEINFO_REQ:
		processNCP_TestDevice_DeviceInfo_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_SENSOR_LEVEL_SET:
		processNCP_TestDevice_SensorLevel_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_DMX_CONTROL_REQ:
		processNCP_TestDevice_DMX_Control_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_SENSOR_MAPPING_SIZE_REQ:
		processNCP_TestDevice_SensorMappingSize_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DIMMER_MAPPING_SIZE_REQ:
		processNCP_TestDevice_DimmerMappingSize_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_SENSOR_MAPPING_INFO_REQ:
		processNCP_TestDevice_SensorMappingInfo_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DIMMER_MAPPING_INFO_REQ:
		processNCP_TestDevice_DimmerMappingInfo_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_SENSOR_MAPPING_REQ:
		processNCP_TestDevice_SensorMapping_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DIMMER_MAPPING_REQ:
		processNCP_TestDevice_DimmerMapping_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_SENSOR_MAPPING_RESET_REQ:
		processNCP_TestDevice_SensorMappingReset_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DIMMER_MAPPING_RESET_REQ:
		processNCP_TestDevice_DimmerMappingReset_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_POWERMETER_REQ:
		processNCP_TestDevice_Powermeter_Req(srcNetAddr, msg, length);
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//Handle Message Function
////////////////////////////////////////////////////////////////////////////////////////////////////

void processNCP_TestDevice_FirmwareVersion_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum = 0;
	uint16 nodeId = 0;
	uint8 version = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];

	version = PLCS_LC_FW_VER;

	TestDevice_FirmwareVersion_Res(seqNum, nodeId, version);
}

void processNCP_TestDevice_ControlInfo_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum;
	uint16 nodeId = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];

	TestDevice_ControlInfo_Res(seqNum, nodeId);
}

void processNCP_TestDevice_Control_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum;
	uint16 nodeId = 0;
	uint8 circuitId = 0;
	uint8 controlState = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];
	circuitId = msg[9];
	controlState = msg[10];

	handleDeviceControl(nodeId, circuitId, controlState);

	TestDevice_Control_Res(seqNum, nodeId, 0x00);
}

void processNCP_TestDevice_DeviceList_Size_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum;
	uint16 nodeId = 0;
	uint8 numOfDevice = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];

	numOfDevice = deviceInfoTable.size;

	TestDevice_DeviceList_Size_Res(seqNum, nodeId, numOfDevice);
}
void processNCP_TestDevice_DeviceInfo_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint8 seqNum;
	uint16 nodeId = 0;
	uint8 index = 0;

	uint16 deviceId = 0;
	uint16 deviceType = 0;
	uint8 deviceVersion = 0;
	device_Info *device_P;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];
	index = msg[9];

	if (index >= deviceInfoTable.size)
		return;

	device_P = &(deviceInfoTable.deviceInfo[index]);
	deviceId = device_P->ieeeId;
	deviceType = device_P->deviceType;
	deviceVersion = device_P->fwVersion;

	TestDevice_DeviceInfo_Res(seqNum, nodeId, deviceId, deviceType,
			deviceVersion);

}

void tcp_sendSetSensorLevel(uint16 sensorId, uint8 level) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = PLCS_GCP_REQ_SETSENSINGLEVEL;
	payload[len++] = (uint8) (zrmpInfo_0.zrm_Id >> 8);
	payload[len++] = (uint8) (zrmpInfo_0.zrm_Id);
	payload[len++] = (uint8) (sensorId >> 8);
	payload[len++] = (uint8) (sensorId);
	payload[len++] = (uint8) level;

	device_P = findNodeById(sensorId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetGCPMessage(tmp_Buff, sensorId, zrmpInfo_0.zrm_Id,
			payload, len);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}
void processNCP_TestDevice_SensorLevel_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {

	uint16 seqNum;
	uint16 nodeId = 0;
	uint16 deviceId = 0;
	uint8 level = 0;
	device_Info *device_P;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];
	deviceId = msg[9] << 8 | msg[10];
	level = msg[11];

	device_P = findNodeById(deviceId);

	if (device_P == NULL) {
		TestDevice_SensorLevel_Res(seqNum, nodeId, 0x01);
		return;
	}
	tcp_sendSetSensorLevel(deviceId, level);
	TestDevice_SensorLevel_Res(seqNum, nodeId, 0x00);
}
void processNCP_TestDevice_DMX_Control_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum;
	uint16 nodeId = 0;
	uint16 deviceId = 0;
	uint8 level = 0;
	device_Info *device_P;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];
	deviceId = msg[9] << 8 | msg[10];
	level = msg[11];

	device_P = findNodeById(deviceId);

	if (device_P == NULL) {
		TestDevice_DMX_Control_Res(seqNum, nodeId, 0x01);
		return;
	}

	dimmer_SendReqDimmingByNodeId(deviceId, level);
	TestDevice_DMX_Control_Res(seqNum, nodeId, 0x00);
}

void processNCP_TestDevice_SensorMappingSize_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum;
	uint16 nodeId = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];

	TestDevice_SensorMappingSize_Res(seqNum, nodeId);
}

void processNCP_TestDevice_DimmerMappingSize_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum;
	uint16 nodeId = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];

	TestDevice_DimmerMappingSize_Res(seqNum, nodeId);
}

void processNCP_TestDevice_SensorMappingInfo_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum;
	uint16 nodeId = 0;
	uint8 index = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];
	index = msg[9];

	TestDevice_SensorMappingInfo_Res(seqNum, nodeId, index);
}

void processNCP_TestDevice_DimmerMappingInfo_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum;
	uint16 nodeId = 0;
	uint8 index = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];
	index = msg[9];

	TestDevice_DimmerMappingInfo_Res(seqNum, nodeId, index);
}

void processNCP_TestDevice_SensorMapping_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum;
	uint16 nodeId = 0;
	uint16 sensorId;
	uint8 circuitId;
	uint8 onTime;
	uint8 onCtrlMode;
	uint8 offCtrlMode;

	uint8 result = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];
	sensorId = msg[9] << 8 | msg[10];
	circuitId = msg[11];
	onTime = msg[12];
	onCtrlMode = msg[13];
	offCtrlMode = msg[14];

	result = updateSensorMappingTable(zrmpInfo_0.zrm_Id, sensorId, circuitId,
			onTime, onCtrlMode, offCtrlMode);
	if (result == 1)
		result = 0;
	else if (result == 2)
		result = 2;
	else
		result = 0x01;

	TestDevice_SensorMapping_Res(seqNum, nodeId, result);
}

void processNCP_TestDevice_DimmerMapping_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 seqNum;
	uint16 nodeId = 0;
	uint16 dimmerId;
	uint8 channelId;
	uint8 circuitId;

	uint8 result = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];
	dimmerId = msg[9] << 8 | msg[10];
	channelId = msg[11];
	circuitId = msg[12];

	if (updateDimmerMappingTable(dimmerId, channelId, circuitId))
		result = 0x00;
	else
		result = 0x02;

	TestDevice_DimmerMapping_Res(seqNum, nodeId, result);
}

void processNCP_TestDevice_SensorMappingReset_Req(uint16 srcNetAddr,
		uint8 msg[], int length) {
	uint16 seqNum;
	uint16 nodeId = 0;
	uint16 i = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];

	for (i = 0; i < MaxNumOfSensorMappingInfo; i++) {
		if (sensorMappingTable.mappingInfo[i].isEnable == 1) {
			sensorMappingTable.mappingInfo[i].isEnable = 0;
			sensorMappingInfo_WriteToEEPRom(i);
		}
		wdt_reset();
	}

	TestDevice_SensorMappingReset_Res(seqNum, nodeId, 0x00);
}

void processNCP_TestDevice_DimmerMappingReset_Req(uint16 srcNetAddr,
		uint8 msg[], int length) {
	uint16 seqNum;
	uint16 nodeId = 0;
	uint16 i = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];

	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++) {
		if (dimmerMappingTable.mappingInfo[i].isEnable == 1) {
			dimmerMappingTable.mappingInfo[i].isEnable = 0;
			dimmerMappingInfo_WriteToEEPRom(i);
		}
		wdt_reset();
	}

	TestDevice_DimmerMappingReset_Res(seqNum, nodeId, 0x00);
}

void processNCP_TestDevice_Powermeter_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;
	uint16 seqNum = 0;

	seqNum = msg[2] << 8 | msg[3];
	nodeId = msg[6] << 8 | msg[7];

	TestDevice_Powermeter_Res(seqNum, nodeId, 0);
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Send Function
////////////////////////////////////////////////////////////////////////////////////////////////////

void TestDevice_FirmwareVersion_Res(uint16 seqNum, uint16 nodeId, uint8 version) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen = 0;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_LC_FIRMWARE_VERSION_RES;
	payload[len++] = (uint8) version;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_ControlInfo_Res(uint16 seqNum, uint16 nodeId) {
	uint8 payload[25];
	int len = 0;
	device_Info *device_P;
	uint8 i = 0;
	uint8 resultLen;

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	payload[len++] = TEST_DEVICE_LC_CONTROLINFO_RES;
	for (i = 0; i < NumOfChannels; i++) {
		if (tmp_SSRInfo.ctrlMode[i] == 0xff)
			payload[len++] = 0x0A;
		else
			payload[len++] = tmp_SSRInfo.ctrlMode[i];
	}

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_Control_Res(uint16 seqNum, uint16 nodeId, uint8 result) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	payload[len++] = TEST_DEVICE_LC_CONTROL_RES;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) (nodeId);
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_DeviceList_Size_Res(uint16 seqNum, uint16 nodeId,
		uint8 numOfDevice) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_LC_DEVICELIST_SIZE_RES;
	payload[len++] = (uint8) numOfDevice;

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_DeviceInfo_Res(uint16 seqNum, uint16 nodeId, uint16 deviceId,
		uint16 type, uint8 version) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_LC_DEVICEINFO_RES;
	payload[len++] = (uint8) (deviceId >> 8);
	payload[len++] = (uint8) deviceId;
	payload[len++] = (uint8) (type >> 8);
	payload[len++] = (uint8) type;
	payload[len++] = (uint8) version;

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);

}
void TestDevice_SensorLevel_Res(uint16 seqNum, uint16 nodeId, uint8 result) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_SENSOR_LEVEL_RES;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) (nodeId);
	payload[len++] = (uint8) result;

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_DMX_Control_Res(uint16 seqNum, uint16 nodeId, uint8 result) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_DMX_CONTROL_RES;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) (nodeId);
	payload[len++] = (uint8) result;

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_SensorMappingSize_Res(uint16 seqNum, uint16 nodeId) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_LC_SENSOR_MAPPING_SIZE_RES;
	payload[len++] = getSensorMappingTableSize();

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}
void TestDevice_DimmerMappingSize_Res(uint16 seqNum, uint16 nodeId) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_LC_DIMMER_MAPPING_SIZE_RES;
	payload[len++] = getDimmerMappingTableSize();

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_SensorMappingInfo_Res(uint16 seqNum, uint16 nodeId, uint8 index) {
	uint8 payload[20];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;
	uint16 lcId;
	uint16 sensorId;
	uint8 circuitId;
	uint8 onTime;
	uint8 onCtrlMode;
	uint8 offCtrlMode;

	if (index >= getSensorMappingTableSize())
		return;

	lcId = sensorMappingTable.mappingInfo[index].lcId;
	sensorId = sensorMappingTable.mappingInfo[index].sensorId;
	circuitId = sensorMappingTable.mappingInfo[index].circuitId;
	onTime = sensorMappingTable.mappingInfo[index].onTime;
	onCtrlMode = sensorMappingTable.mappingInfo[index].onCtrlMode;
	offCtrlMode = sensorMappingTable.mappingInfo[index].offCtrlMode;

	payload[len++] = TEST_DEVICE_LC_SENSOR_MAPPING_INFO_RES;
	payload[len++] = (uint8) (lcId >> 8);
	payload[len++] = (uint8) (lcId);
	payload[len++] = (uint8) (sensorId >> 8);
	payload[len++] = (uint8) (sensorId);
	payload[len++] = (uint8) (circuitId);
	payload[len++] = (uint8) (onTime);
	payload[len++] = (uint8) (onCtrlMode);
	payload[len++] = (uint8) (offCtrlMode);

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_DimmerMappingInfo_Res(uint16 seqNum, uint16 nodeId, uint8 index) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;
	uint16 dimmerId;
	uint8 channelId;
	uint8 circuitId;

	if (index >= getDimmerMappingTableSize())
		return;

	dimmerId = dimmerMappingTable.mappingInfo[index].dimmerId;
	channelId = dimmerMappingTable.mappingInfo[index].channelId;
	circuitId = dimmerMappingTable.mappingInfo[index].circuitId;

	payload[len++] = TEST_DEVICE_LC_DIMMER_MAPPING_INFO_RES;
	payload[len++] = (uint8) (dimmerId >> 8);
	payload[len++] = (uint8) (dimmerId);
	payload[len++] = (uint8) (channelId);
	payload[len++] = (uint8) (circuitId);

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_SensorMapping_Res(uint16 seqNum, uint16 nodeId, uint8 result) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_LC_SENSOR_MAPPING_RES;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) (nodeId);
	payload[len++] = (uint8) result;

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_DimmerMapping_Res(uint16 seqNum, uint16 nodeId, uint8 result) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_LC_DIMMER_MAPPING_RES;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) (nodeId);
	payload[len++] = (uint8) result;

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_SensorMappingReset_Res(uint16 seqNum, uint16 nodeId,
		uint8 result) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_LC_SENSOR_MAPPING_RESET_RES;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) (nodeId);
	payload[len++] = (uint8) result;

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_DimmerMappingReset_Res(uint16 seqNum, uint16 nodeId,
		uint8 result) {
	uint8 payload[10];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_LC_DIMMER_MAPPING_RESET_RES;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) (nodeId);
	payload[len++] = (uint8) result;

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

void TestDevice_Powermeter_Res(uint16 seqNum, uint16 nodeId, uint32 value) {
	uint8 payload[20];
	int len = 0;
	uint8 resultLen;
	device_Info *device_P;

	payload[len++] = TEST_DEVICE_LC_POWERMETER_RES;
	payload[len++] = (uint8) (value >> 24);
	payload[len++] = (uint8) (value >> 16);
	payload[len++] = (uint8) (value >> 8);
	payload[len++] = (uint8) (value);

	device_P = findNodeById(nodeId);
	if (device_P == NULL)
		return;

	resultLen = plcs_GetTCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(device_P->nwkAddr, tmp_Buff, resultLen);
}

uint8 getSeqNumGeneratorForTCP() {
	static uint8 seqNumGeneratorForTCP = 0;
	return seqNumGeneratorForTCP++;
}

uint8 plcs_GetTCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = TEST_DEVICE_PROTOCOL_ID;
	resultMsg[resultLen++] = getSeqNumGeneratorForTCP();
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetTCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = TEST_DEVICE_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

