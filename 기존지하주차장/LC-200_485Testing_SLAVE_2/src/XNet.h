#ifndef __XNET_H__
#define __XNET_H__

/**
	XNet : multiple sub-net networking layer.

	This class is used for handle multiple protocol/subnet multiplexing/demultiplexing.
	ex.
			node --[NCP]------- [GW] --------  [GCP]--- [XCP]---- ethernet/cdma.
			  +----[NCP_FW]--------------------[NCP_FW]--+
*/

#include "Util.h"

/**
	XNET protocol definitions as classes.
*/
/**
	Header control.
	XNET header = {hdr(2bits), net_type(2bits), dst(2bits), src(2bits)}
*/
#define VERSION_MASK		(0xC0)		// MSB 2 BITS.
#define VERSION				(0x40)		// XNet version 2.0
#define VERSION_2_0			(0x40)		// XNet version 2.0 (alias)


#define NET_MASK			(0x30)		// BIT 5,4 : 2 BITS.
#define NET_ETHERNET		(0x00)		// 
#define NET_CDMA			(0x10)		// 
#define NET_ZIGBEE			(0x20)		// 
#define NET_UNKNOWN		(0x30)		// <reserved>

#define DST_MASK			(0x0C)		// BIT 3,2 : 2 bits
#define DST_SERVER			(0x00)		// 
#define DST_GATEWAY			(0x04)		// 
#define DST_NODE			(0x08)		// 

#define SRC_MASK			(0x03)		// BIT 3,2 : 2 bits
#define SRC_SERVER			(0x00)		// 
#define SRC_GATEWAY			(0x01)		// 
#define SRC_NODE			(0x02)		// 


//-- for old codes compatibility.
/**
	Basic control protocol :
	logging in, & disconnect(logout)
 */
//static const uint8	XCP_CONTROL		= 0x00;

// Gatway to Server via Ethernet

#define G2S_ETHERNET		(VERSION_2_0 | NET_ETHERNET | SRC_GATEWAY | DST_SERVER) 	// 0x41
#define G2S_CDMA			(VERSION_2_0 | NET_CDMA | SRC_GATEWAY | DST_SERVER)		// 0x51

#define S2G_ETHERNET		(VERSION_2_0 | NET_ETHERNET | SRC_SERVER | DST_GATEWAY)		// 0x44
#define S2G_CDMA			(VERSION_2_0 | NET_CDMA | SRC_SERVER | DST_GATEWAY)		// 0x54

	// Node to Gateway via Zigbee
#define N2G_ZIGBEE			(VERSION_2_0 | NET_ZIGBEE | SRC_NODE | DST_GATEWAY)			// 0x66
#define G2N_ZIGBEE			(VERSION_2_0 | NET_ZIGBEE | SRC_GATEWAY | DST_NODE)			// 0x69

	// Node to Node via Zigbee
#define N2N_ZIGBEE			(VERSION_2_0 | NET_ZIGBEE | SRC_NODE | DST_NODE)			// 0x6A

	// Node to Server via Zigbee/.
#define N2S_ZIGBEE			(VERSION_2_0 | NET_ZIGBEE | SRC_NODE | DST_SERVER)			// 0x62
#define N2S_ETHERNET		(VERSION_2_0 | NET_ETHERNET | SRC_NODE | DST_SERVER)		// 0x42

#define S2N_ZIGBEE			(VERSION_2_0 | NET_ZIGBEE | SRC_SERVER | DST_NODE )			// 0x68
#define S2N_ETHERNET		(VERSION_2_0 | NET_ETHERNET | SRC_SERVER | DST_NODE)		// 0x48

#endif

