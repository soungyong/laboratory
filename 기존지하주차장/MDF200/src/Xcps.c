
#include <stdio.h>
#include <string.h>
#include "Xcps.h"
#include "Uart.h"


// protocol message format start/end flag.
#define	ZS_SFLAG	0xFA
#define	ZS_EFLAG	0xAF


/**
	temporary rx packet.
*/
uint8 xcps_rx_packet_zigbee[XCPS_MAX_PDU];
uint8 xcps_rx_packet_rs485[XCPS_MAX_PDU];


// 03/01. xcp zigbee uart transport receive state.
int xcps_rxlen_zigbee = 0;							// total length received.

int xcps_pdu_len_rs485=0;
int xcps_state_rs485=0;
int xcps_rxlen_rs485 = 0;							// total length received.

/**
	uart interface to use.
*/
static usart_getter	xcps_getter_zigbee = (usart_getter)0;
static usart_putter	xcps_putter_zigbee = (usart_putter)0;

static usart_getter	xcps_getter_rs485 = (usart_getter)0;
static usart_putter	xcps_putter_rs485 = (usart_putter)0;



/**

**/
uint8 xcps_init_zigbee(usart_getter getter, usart_putter putter)
{
	xcps_getter_zigbee = getter;
	xcps_putter_zigbee = putter;

	
	return 0;
}


uint8 xcps_init_rs485(usart_getter getter, usart_putter putter)
{
	xcps_getter_rs485 = getter;
	xcps_putter_rs485 = putter;

	//xcps_TxDisabler_rs485();

	return 0;
}

/**

*/
uint8 xcps_send_zigbee(const uint8 *data, uint8 length) {
	uint8 msgLen2 = 0;
	uint8 i=0;
	uint8 checkSum = 0;

	// check putter.
	if (!xcps_putter_zigbee)
		return -1;

	// calc checksum.
	for (i = 0; i < length; i++)
		checkSum += ((uint8) data[i] & 0xFF);

	xcps_putter_zigbee(ZS_SFLAG);
	xcps_putter_zigbee(length);
	for (i = 0; i < length; i++)
		xcps_putter_zigbee(data[i]);
	xcps_putter_zigbee(checkSum);
	xcps_putter_zigbee(ZS_EFLAG);

	return 0;
}

uint8 xcps_recv_zigbee(uint8 *buff, uint8 buff_length) {
	uint8 temp;
	int i;
	uint8 checkSum = 0;

	// check getter.
	if (!xcps_getter_zigbee)
		return -1;

	if (xcps_getter_zigbee(&temp) < 1)
		return 0;


	if (xcps_rxlen_zigbee >= buff_length)
		xcps_rxlen_zigbee = 0;

	xcps_rx_packet_zigbee[xcps_rxlen_zigbee++] = temp;

	if (temp == ZS_SFLAG) {
		if(xcps_rx_packet_zigbee[0]!=ZS_SFLAG){
			xcps_rxlen_zigbee = 0;
			xcps_rx_packet_zigbee[xcps_rxlen_zigbee++] = temp;
			return 0;
		}
	} else if (temp == ZS_EFLAG){
		if(xcps_rxlen_zigbee < 6) return 0;
		if(xcps_rxlen_zigbee - 4 < xcps_rx_packet_zigbee[1]) return 0;
		if(xcps_rxlen_zigbee - 4 > xcps_rx_packet_zigbee[1]) {
			xcps_rxlen_zigbee=0;
			return 0;
		}

		uint8 resultLen=0;
		checkSum = 0;


		//xcps_rx_packet_zigbee contains fa~af.

		//length.
		if (xcps_rxlen_zigbee - 4 != xcps_rx_packet_zigbee[1]){
			xcps_rxlen_zigbee=0;
			return 0;
		}

		//checksum
		for (i = 2; i < xcps_rxlen_zigbee - 2; i++)
			checkSum += ((uint8) xcps_rx_packet_zigbee[i] & 0xFF);

		if (checkSum != xcps_rx_packet_zigbee[xcps_rxlen_zigbee - 2])
			return 0;

		for (i = 2; i < xcps_rxlen_zigbee - 2; i++) {
			buff[resultLen++] = xcps_rx_packet_zigbee[i];
		}
		xcps_rxlen_zigbee=0;

		return resultLen;
	}

	return 0;
}

// -------------------------------------------------------------------------------------------------------------------------------------- //
// -------------------------------------------------------------------------------------------------------------------------------------- //
// -------------------------------------------------------------------------------------------------------------------------------------- //
uint8 xcps_send_rs485(const uint8 *data, uint8 length)
{
	uint8 buff[XCPS_MAX_PDU];
	int i, msgLen = 0;
	uint8 checkSum = 0;

	// check putter.
	if (!xcps_putter_rs485) return -1;

    	buff[msgLen++] = ZS_SFLAG; 			//Start Byte_1
    	buff[msgLen++] = (uint8)(length);

	for (i = 0; i < length; i++)
		buff[msgLen++] = data[i];

	// calc checksum.
	for (i = 2; i < msgLen; i++)
		checkSum += ((uint8)buff[i] & 0xFF);

	buff[msgLen++] = (uint8)(checkSum & 0xFF);

	// end flag.
    buff[msgLen++] = ZS_EFLAG;
   	//xcps_TxEnabler_rs485();
	//MSLEEP(1);
    for(i=0; i < msgLen; i++)
    	xcps_putter_rs485(buff[i]);
	//xcps_TxDisabler_rs485();

	return 0;
}


/**

*/
uint8 xcps_recv_rs485(uint8 *buff, uint8 buff_length)
{
	uint8 temp;
	int i;
	uint8 checkSum = 0;

	// check getter.
	if (!xcps_getter_rs485)
		return -1;

	if(xcps_getter_rs485(&temp) < 1 )
		return 0;

	switch (xcps_state_rs485)
	{
		case 0:
			xcps_rxlen_rs485 = 0;

			if (temp == ZS_SFLAG)
			{
				xcps_state_rs485 = 1;
				xcps_rx_packet_rs485[xcps_rxlen_rs485++] = temp;
			}

			else if (temp == ZS_EFLAG)
			{
				// don't change state, keep find start flag.
			}
		break;

		case 1:	// found start flag
			xcps_rx_packet_rs485[xcps_rxlen_rs485++] = temp;
			xcps_state_rs485 = 2;
			xcps_pdu_len_rs485 = temp;

		break;

		case 2:	// found length
			// fill data.
			xcps_rx_packet_rs485[xcps_rxlen_rs485++] = temp;

			// check length.
			if (xcps_rxlen_rs485 >= (2 + xcps_pdu_len_rs485))
				xcps_state_rs485 = 3;
		break;

		case 3:	// data end, check checksum.
			for (i = 2; i < (2 + xcps_pdu_len_rs485); i++)
				checkSum += (xcps_rx_packet_rs485[i] & 0xFF);

			if (temp == (uint8)checkSum )// Checksum ok.
				xcps_state_rs485 = 4;

			else
				xcps_state_rs485 = 0;
		break;

		case 4:
			if (temp == ZS_EFLAG)
			{
				xcps_state_rs485 = 0;

				// return data to caller.
				for (i = 0;  i < xcps_pdu_len_rs485; i++)
					buff[i] = xcps_rx_packet_rs485[2+i];

				return xcps_pdu_len_rs485;
			}
			else
			{
				// TODO:
				xcps_state_rs485 = 0;
			}
		break;

	default:
		// if you here, something wrong. --> recover to state 0.
		xcps_state_rs485 = 0;
		break;
	}
	return 0;
}

//end of file

