#include "ZRMProtocol.h"
#include "rotary.h"
#include "Timer.h"

uint8 tmp_BuffForZRM[MAX_TX_BUFF];

#define SendBuffSize 0x10
sendBuffData_st sendBuffData[SendBuffSize];
uint8 sendBuffForZRM_Head = 0;
uint8 sendBuffForZRM_Tail = 0;

uint8 lastResult = ERROR;

uint8 getMessageFromSendBuff(sendBuffData_st *message) {
	uint8 len = 0;
	if (getZRMMessageListSize() > 0) {
		memcpy(message->buff, sendBuffData[sendBuffForZRM_Head].buff,
				sendBuffData[sendBuffForZRM_Head].len);
		len = sendBuffData[sendBuffForZRM_Head].len;
		message->len = len;
		message->dstAddr = sendBuffData[sendBuffForZRM_Head].dstAddr;

		sendBuffForZRM_Head += 1;
		sendBuffForZRM_Head &= (SendBuffSize - 1);

		return len;
	}
	return len;
}

uint8 getMessageFromSendBuffWORemove(sendBuffData_st *message) {
	uint8 len = 0;
	if (getZRMMessageListSize() > 0) {
		memcpy(message->buff, sendBuffData[sendBuffForZRM_Head].buff,
				sendBuffData[sendBuffForZRM_Head].len);
		len = sendBuffData[sendBuffForZRM_Head].len;
		message->len = len;
		message->dstAddr = sendBuffData[sendBuffForZRM_Head].dstAddr;

		return len;
	}
	return len;
}

void addMessageToSendBuff(uint16 dstAddr, uint8 *data, uint8 len) {
	if (getZRMMessageListSize() >= (SendBuffSize - 1))
		return;

	memcpy(sendBuffData[sendBuffForZRM_Tail].buff, data, len);
	sendBuffData[sendBuffForZRM_Tail].len = (uint8) len;
	sendBuffData[sendBuffForZRM_Tail].dstAddr = dstAddr;

	sendBuffForZRM_Tail += 1;
	sendBuffForZRM_Tail &= (SendBuffSize - 1);
}

uint8 getZRMMessageListSize() {
	return (sendBuffForZRM_Tail - sendBuffForZRM_Head) & (SendBuffSize - 1);
}

void ZRMPMessage(uint8 buff[], int buff_length) {
	uint16 netAddr = 0;
	switch (buff[0]) {
	case ZRMP_RES_PING:
		// Response Ping
		tmp_zrmp.zrm_Id = (uint16) (buff[11] << 8) | (buff[12]);
		tmp_zrmp.zrm_Panid = (uint16) (buff[13] << 8) | (buff[14]);
		netAddr = (uint16) (buff[15] << 8) | (buff[16]);

		if (netAddr != tmp_zrmp.zrm_NetAddr) {
			timer_set(NCP_TIMER_ID, 1000);
		}
		tmp_zrmp.zrm_NetAddr = netAddr;

		if (tmp_zrmp.zrm_Panid != (0x0100 | rotary_GetValue())) {
			ZRMSendSetPanID(0x0100 | rotary_GetValue());
			ZRMSendReset();
		} else {
			if (getZigbeeState() != ZRM_CONNECT)
				tmp_zrmp.zrm_State = ZRM_PING_COMPLETE;
			ZRMsendConfig();
		}

		break;

	case ZRMP_RES_CONFIG:
		// Response Config
		tmp_zrmp.zrm_Channel = buff[9];
		if (tmp_zrmp.zrm_Channel != (rotary_GetValue() % 16 + 11)) {
			ZRMSendSetChannel(rotary_GetValue() % 16 + 11);
			ZRMsendConfig();
		} else
			tmp_zrmp.zrm_State = ZRM_CONNECT;
		break;

	case ZRMP_RES_NWK_INIT:
		ZRMsendPing();
		break;

	case ZRMP_SUCCESS:
		lastResult = SUCCESS;
		break;
	case ZRMP_ERROR:
		lastResult = ERROR;
		break;
	default:
		// ERROR
		break;
	}
}

uint8 getZigbeeState() {
	return tmp_zrmp.zrm_State;
}

void ZRMsendPing() {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_PING;

	xcps_send_zigbee(tmp_BuffForZRM, len);
	//MSLEEP(3);
}

void ZRMsendConfig() {
	uint8 len = 0;
	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_CONFIG;
	tmp_BuffForZRM[len++] = ZRMP_MSG_FORMAT;

	xcps_send_zigbee(tmp_BuffForZRM, len);
	//MSLEEP(3);
}

void ZRMSendReset() {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_RESET;
	tmp_BuffForZRM[len++] = ZRMP_REQ_RESET;
	//if(getLastResult()!=WAIT)
	xcps_send_zigbee(tmp_BuffForZRM, len);
	//MSLEEP(3);
}

void ZRMSendSetPanID(uint16 panId) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_PANID_SET;
	tmp_BuffForZRM[len++] = panId >> 8;
	tmp_BuffForZRM[len++] = (panId & 0xff);

	xcps_send_zigbee(tmp_BuffForZRM, len);
//	MSLEEP(3);
}

void ZRMSendSetChannel(uint8 channel) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_CONFIG_SET;
	tmp_BuffForZRM[len++] = 0x02;
	tmp_BuffForZRM[len++] = (channel & 0xff);
	xcps_send_zigbee(tmp_BuffForZRM, len);
//	MSLEEP(3);
}

void ZRMSendSetPreconfig(uint8 preconfig) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_CONFIG_SET;
	tmp_BuffForZRM[len++] = 0x05;
	tmp_BuffForZRM[len++] = (preconfig & 0xff);
	xcps_send_zigbee(tmp_BuffForZRM, len);
}

//
void sendToZigbee(uint16 dst_Addr, uint8 msg[], uint8 length) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = 0x10;
	tmp_BuffForZRM[len++] = (uint8) (dst_Addr >> 8);
	tmp_BuffForZRM[len++] = (uint8) (dst_Addr);
	tmp_BuffForZRM[len++] = (uint8) 0x00;
	tmp_BuffForZRM[len++] = (uint8) 0x00;

	len += ZRM_getTranlatedCode(&(tmp_BuffForZRM[len]), msg, length);

	//xcps_send_zigbee(tmp_BuffForZRM, len);
	addMessageToSendBuff(dst_Addr, tmp_BuffForZRM, len);
	//MSLEEP(3);
}

void sendQueueingMessage() {
	static sendBuffData_st sendBuffData;
	static uint8 len;
	static uint32 cnt;
	static uint32 sendFail;
	uint8 result = 0;

	if (getZRMMessageListSize() > 0)
		if (getZigbeeState() == ZRM_CONNECT) {
			if (getLastResult() == SUCCESS) {
				getMessageFromSendBuff(&sendBuffData);
				sendFail = 0;
				lastResult = ERROR;

				result = 1;
			} else {
				result = 0;
			}
		}

	if (getZRMMessageListSize() > 0 && getZigbeeState() == ZRM_CONNECT) {
		if (getLastResult() == WAIT) {
			cnt++;
			if (cnt > 2) {
				cnt = 0;
				lastResult = ERROR;
			} else
				return;
		}

		len = getMessageFromSendBuffWORemove(&sendBuffData);
		if (len > 0) {
			sendFail++;
			if (sendFail > 10) {
				sendFail = 0;
				lastResult = SUCCESS;
				return;
			}

			lastResult = WAIT;
			xcps_send_zigbee(sendBuffData.buff, sendBuffData.len);

			len = 0;
		}
	}
}

uint8 getLastResult() {
	return lastResult;
}

uint8 ZRM_getOriginalCode(uint8 *resultBuff, uint8 *buff, uint8 buff_length) {
	uint8 resultLen = 0;
	uint8 i = 0;
	for (i = 0; i < buff_length; i++) {
		if (buff[i] == 0xFF) {
			if (buff[i + 1] == 0xFF)
				resultBuff[resultLen++] = 0xFF;
			else if (buff[i + 1] == 0x01)
				resultBuff[resultLen++] = 0xFA;
			else if (buff[i + 1] == 0x02)
				resultBuff[resultLen++] = 0xAF;
			i++;
		} else
			resultBuff[resultLen++] = buff[i];
	}
	return resultLen;
}

uint8 ZRM_getTranlatedCode(uint8 *resultBuff, uint8 *buff, uint8 buff_length) {
	uint8 resultLen = 0;
	uint8 i = 0;
	for (i = 0; i < buff_length; i++) {
		if (buff[i] == 0xFA) {
			resultBuff[resultLen++] = 0xFF;
			resultBuff[resultLen++] = 0x01;
		} else if (buff[i] == 0xAF) {
			resultBuff[resultLen++] = 0xFF;
			resultBuff[resultLen++] = 0x02;
		} else if (buff[i] == 0xFF) {
			resultBuff[resultLen++] = 0xFF;
			resultBuff[resultLen++] = 0xFF;
		} else
			resultBuff[resultLen++] = buff[i];
	}
	return resultLen;
}

//end of file

