#include "XNetProtocol.h"
#include <string.h>
#include "GC_Protocol.h"
#include "Xcps.h"
#include "ZRMProtocol.h"
#include "NCProtocol.h"
#include "RFU_Protocol.h"

uint8 XNetHandlerFromZigbee(uint16 srcNetAddr, uint8 buff[], int buff_length) {
	uint8 pid;
	uint8 subPid;

	pid = buff[0];
	subPid = buff[1];

	switch (pid) {
	case NCP_PROTOCOL_ID: {
		switch (subPid) {
		case PLCS_NCP_PROTOCOL_ID:
			NCP_ProcessMessageFromZigbee(0, 0, buff, buff_length);
			break;
		case PLCS_GCP_PROTOCOL_ID:
			plcs_GCP_ProcessMessageFromZigbee(0, 0, buff, buff_length);
			break;
		case PLCS_RFU_PROTOCOL_ID:
			plcs_RFUP_ProcessMessage(srcNetAddr, buff[2] << 8 | buff[3], 0, 0, buff, buff_length);
			break;
		}
	}
		break;
	default:
		break;
	}
	return 1;
}

//end of file

