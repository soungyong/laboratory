#include "common.h"

#define ZIGBEE_CONTROL_PACKET          0x00
#define ZIGBEE_DATA_PACKET             0x10

extern unsigned char ieeeAddr[8];
extern unsigned char netAddr[2];
extern unsigned char panId[2];
extern unsigned char channel;

void sendReqZigbeeInfo();
void sendSetZigbeePanId(unsigned char id);
void sendSetZigbeeChannel(unsigned char channel);
void sendSetZigbeePreconfig(unsigned char preconfig);
void handleResZigbeeInfo(char *data);
void sendControlDataToUsart0(char *buf, unsigned char len);
void sendReqZigbeeReset();
int zigbee_getState();

unsigned char getTranslatedCode(char *resultBuf, char* buf, unsigned char len);
unsigned char getOriginalCode(char *resultBuf, char* buf, unsigned char len);
