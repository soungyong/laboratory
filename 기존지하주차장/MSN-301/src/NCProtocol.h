#ifndef __NCP_PROTOCOL_H__
#define __NCP_PROTOCOL_H__

#include <stdio.h>

#define NCP_PROTOCOL_ID			(0x80)	// Protocol ID
#define PLCS_NCP_ID     		(0x01)  	// Protocol Version
#define PLCS_GCP_ID             0x21

#define NCP_REQ_PING				(0x01)	// Ping
#define NCP_RES_PING				(0x02)	// Ping 응답

#define NCP_REQ_REGISTER			(0x11)	// 등록 요청
#define NCP_RES_REGISTER			(0x12)	// 등록 요청 응답

#define NCP_REQ_NODE_INFO			(0x17)	// 노드 정보 요청
#define NCP_RES_NODE_INFO			(0x18)	// 노드 정보 요청 응답

#define PLCS_GCP_REQ_STATE_INFO     (0x24)
#define PLCS_GCP_RES_STATE_INFO     (0x25)
#define PLCS_GCP_REQ_REBOOT         (0x3A)
#define PLCS_GCP_NOTICE_EVENT       (0x70)
#define PLCS_GCP_NOTICE_EVENT_DEBUG (0x7A)

#define PLCS_GCP_REQ_SET_SENSINGLEVEL   (0x80)
#define PLCS_GCP_RES_SET_SENSINGLEVEL   (0x81)
#define PLCS_GCP_REQ_SET_MODE           (0x82)
#define PLCS_GCP_RES_SET_MODE           (0x83)


#endif

