#include "adc.h"
#include "gpio.h"
#include <avr/io.h>
void initADC() {
	ADMUX = 0x00; //select adc input 0
	ACSR = 0x80;
	ADCSRA = 0xA5;
	ADCSRB &= 0xf8;
	DIDR0 = 0xC1;
}
