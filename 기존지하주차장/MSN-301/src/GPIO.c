#include "GPIO.h"        
#include <avr/io.h>
// Declare your global variables here        
void initPort() {
	DDRC = 0x00;
	DDRB = 0x0B;
	DDRD = 0x00;

	PORTB = 0x01;
	PORTC = 0x00;
	PORTD = 0x00;
}

void reset() {
	PORTB &= ~(0x01);
	delay_ms(100);
	PORTB |= 0x01;
}
