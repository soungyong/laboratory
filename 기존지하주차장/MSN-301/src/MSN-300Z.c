/*****************************************************
 This program was produced by the
 CodeWizardAVR V1.25.3 Standard
 Automatic Program Generator
 ?Copyright 1998-2007 Pavel Haiduc, HP InfoTech s.r.l.
 http://www.hpinfotech.com

 Project :
 Version :
 Date    : 8/10/2011
 Author  : funface2
 Company : m2mkorea
 Comments:


 Chip type           : ATmega8
 Program type        : Application
 Clock frequency     : 16.000000 MHz
 Memory model        : Small
 External SRAM size  : 0
 Data Stack size     : 1024
 *****************************************************/
#include <avr/io.h>
#include "common.h"  
#include "util.h"
#include "gpio.h"
#include "usart.h"    
#include "adc.h"    
#include "timer.h"
#include "zigbee.h"     
#include "rotary.h"
#include "NCProtocol.h"   

#include <avr/eeprom.h>
#include <stdio.h>
#include <string.h>   
#include <math.h>

unsigned int Sensing_Standard_Avr = 295;

unsigned char ieeeAddr[8];
unsigned char netAddr[2];
unsigned char panId[2];
unsigned char channel;
unsigned int Sensing_Threshold_Avr_Diff = 0x0019;
unsigned char NORMAL = 1;

void handleMessage();
void processMessage(char * buf, int len);
void sendNCPPing_Req();
void handleNCPPing_Res(int flag);
void sendNCPRegister_Req();
void handleNCPRegister_Res(int zigbeeId, int result);

void handleSendEvent();
void handleSendEvent_Debug();
void handleStateInfoReq(unsigned int seqNum);
void handleSetSensingLevelReq(unsigned int seqNum, unsigned int avrDiff);
void handleSetModeReq(unsigned int seqNum, unsigned char mode);

void sendNCPPacket(char *buf, unsigned char len);
void sendGCPPacket(unsigned int seqNum, char *buf, unsigned char len);
void sendSetSensingLevelRes(unsigned int seqNum, unsigned int avrDiff);
void sendSetModeRes(unsigned int seqNum, unsigned char mode);
void sendSetLevelRes(unsigned int seqNum, unsigned char level);

void initWatchDog();
void resetWatchDog();

unsigned char isReoobt = 0;
char g_isSensored = false;
char g_isSensored_Avr = false;
char g_isSensored_Avr2 = false;
unsigned int g_adc_data_history[NumOFHistory];
unsigned long g_timer0_count_NormalMode = 0;

unsigned int g_zigbeeSendTimer = 0;

#define NumOFDebugHistory 10
unsigned int g_debug_data_history[NumOFDebugHistory];
unsigned char g_debug_data_size = 0;

#define IDLE 0
#define REGISTERED 1
unsigned char RegistrationState = IDLE;
unsigned char PingCount = 0;
unsigned char RegistrationCount = 0;

#define NumOfRecentHistory 3
#define NumOfAvrHistory 10
unsigned int avrHistory[NumOfAvrHistory];

inline void getAvrWithin10ms() {
	unsigned char i;
	unsigned long avr = 0;
	unsigned long dev = 0;
	unsigned int temp = 0;
	unsigned int adc_data_history[NumOFHistory];

	for (i = 0; i < NumOFHistory; i++)
		adc_data_history[i] = g_adc_data_history[i];

	for (i = 0; i < NumOFHistory; i++) {
		avr += adc_data_history[i];
	}
	avr /= NumOFHistory;

	for (i = 0; i < NumOFHistory; i++) {
		if (adc_data_history[i] > avr)
			temp = adc_data_history[i] - avr;
		else
			temp = avr - adc_data_history[i];
		dev += temp;
	}
	dev /= NumOFHistory;

	for (i = 0; i < NumOFHistory; i++) {
		if (adc_data_history[i] > avr)
			temp = adc_data_history[i] - avr;
		else
			temp = avr - adc_data_history[i];
		if (temp > dev * 2)
			adc_data_history[i] = avr;
	}
	avr = 0;
	for (i = 0; i < NumOFHistory; i++)
		avr += adc_data_history[i];
	avr /= NumOFHistory;
	if (g_zigbeeSendTimer == 0) {
		for (i = 0; i < NumOfAvrHistory - 1; i++)
			avrHistory[i] = avrHistory[i + 1];

		avrHistory[NumOfAvrHistory - 1] = avr;

		if (g_debug_data_size < (NumOFDebugHistory - 1))
			g_debug_data_history[g_debug_data_size++] = avr;
	} else if (g_zigbeeSendTimer < 2)
		g_zigbeeSendTimer++;
	else
		g_zigbeeSendTimer = 0;
}

//동작 상태 확인 timer
//10ms 마다 호출.  
unsigned char pb3OutCnt = 100;
char isSensored() {
	unsigned char isSensored = 0;
	unsigned char i = 0;
	unsigned char startIndex = 0;
	unsigned char sensorCount = 0;
	g_isSensored_Avr = 0;

	startIndex = NumOfAvrHistory - NumOfRecentHistory;

	for (i = startIndex; i < NumOfAvrHistory; i++)
		if (avrHistory[i] < Sensing_Standard_Avr)
			break;
		else if (avrHistory[i] - Sensing_Standard_Avr < Sensing_Threshold_Avr_Diff)
			break;

	if (i == NumOfAvrHistory) {
		g_isSensored_Avr = i + 10;
		isSensored = true;
	}

	for (i = startIndex; i < NumOfAvrHistory; i++)
		if (avrHistory[i] > Sensing_Standard_Avr)
			break;
		else if (Sensing_Standard_Avr - avrHistory[i] < Sensing_Threshold_Avr_Diff)
			break;

	if (i == NumOfAvrHistory) {
		g_isSensored_Avr = i + 10;
		isSensored = true;
	}

	for (i = 0; i < NumOfAvrHistory; i++) {
		if (avrHistory[i] > Sensing_Standard_Avr) {
			if (avrHistory[i] - Sensing_Standard_Avr > Sensing_Threshold_Avr_Diff)
				sensorCount++;
		} else {
			if (Sensing_Standard_Avr - avrHistory[i] > Sensing_Threshold_Avr_Diff)
				sensorCount++;
		}
	}
	if (sensorCount > NumOfRecentHistory * 1.5) {
		g_isSensored_Avr2 = sensorCount + 10;
		isSensored = true;
	} else {
		g_isSensored_Avr2 = sensorCount;
		isSensored |= false;
	}

	g_isSensored = isSensored;

	if (g_isSensored)
		pb3OutCnt = 0;
	if (pb3OutCnt < 100) {
		pb3OutCnt++;
		PORTB |= 0x08;
	} else
		PORTB &= ~(0x08);

	return g_isSensored;
}

unsigned int timer0_cnt_Registration = 0;
unsigned int timer0_cnt_Ping = 0;
unsigned int timer0_cnt_NetAddr = 0;
unsigned int timer0_cnt_PanSetting = 0;
//unsigned int timer0_cnt_LED=0;
unsigned char prevSensorState = false;
unsigned char sensorState = false;

//10ms
unsigned int g_NumOfEventSentCount = 0;
unsigned int g_EventSendingInterval = 10;
unsigned int g_EventSentTimer = 0;
unsigned int g_timer1CountForEventSent = 0;

void timer1_Fired() {
	getAvrWithin10ms();
	asm("sei");
	//update g_isSensored 값.
	prevSensorState = sensorState;
	sensorState = isSensored();

	if (sensorState)
		LED_ON;
	else
		LED_OFF;

	if (zigbee_getState()) {
		if (prevSensorState == false) {
			if (sensorState == true && (g_EventSentTimer > g_EventSendingInterval)) {
				//감지 상태 전송.
				handleSendEvent();
				g_NumOfEventSentCount++;
				g_EventSentTimer = 0;
				if (g_EventSendingInterval <= 100) //1초
					g_EventSendingInterval += 10; //100ms
			}
		} else if ((g_EventSentTimer > g_EventSendingInterval) && sensorState == true) {
			handleSendEvent();
			g_NumOfEventSentCount++;
			g_EventSentTimer = 0;
			if (g_EventSendingInterval <= 100) //1초
				g_EventSendingInterval += 10; //100ms
		}
	}
	if (g_EventSentTimer < 200) //2초
		g_EventSentTimer++;
	g_timer1CountForEventSent++;

	if (g_timer1CountForEventSent >= 100) { //1초
		if (g_EventSendingInterval >= 10 && g_NumOfEventSentCount == 0)
			g_EventSendingInterval -= 10;
		g_NumOfEventSentCount = 0;
		g_timer1CountForEventSent = 0;
	}
}

//100ms
void timer0_Fired() {
	timer0_cnt_NetAddr++;
	if (timer0_cnt_NetAddr > 20) { //setting된 zigbee panID확인.
		sendReqZigbeeInfo();
		if (panId[0] != 0x01 || panId[1] != getRotaryValue() || (channel != (11 + (getRotaryValue() % 16)))) {
			timer0_cnt_PanSetting++;
		} else {
			timer0_cnt_PanSetting = 0;
		}
		if (timer0_cnt_PanSetting >= 3) { //setting된 zigbee panID와 rotary switch와 다를시 재 설정.
			if (panId[1] != getRotaryValue())
				sendSetZigbeePanId(getRotaryValue());
			if (channel != (11 + (getRotaryValue() % 16)))
				sendSetZigbeeChannel(11 + (getRotaryValue() % 16));
			timer0_cnt_PanSetting = 0;
			sendReqZigbeeReset();
		}
		timer0_cnt_NetAddr = 0;
	}

	if (!NORMAL) {
		if (zigbee_getState())
			handleSendEvent_Debug();

		g_timer0_count_NormalMode++;
		if (g_timer0_count_NormalMode > 3000)
			NORMAL = 1;
	}

	if (zigbee_getState()) { //Zigbee is connecting to coordinator
		if (RegistrationState != REGISTERED) {
			timer0_cnt_Registration++;
			if (timer0_cnt_Registration > 100) {
				sendNCPRegister_Req();
				RegistrationCount++;
				if (RegistrationCount > 12) {
					sendReqZigbeeReset();
					reset();
					RegistrationCount = 0;
				}
				timer0_cnt_Registration = 0;
			}
		} else {
			timer0_cnt_Ping++;
			if (timer0_cnt_Ping > 100) {
				PingCount++;
				if (PingCount > 5) {
					RegistrationState = IDLE;
					timer0_cnt_Registration = 0xFF00;
					PingCount = 0;
				} else
					sendNCPPing_Req();
				timer0_cnt_Ping = 0;
			}
		}
	}
}

char g_MsgBuf[DEFAULT_BUF_SIZE];
unsigned int g_MsgBuf_Count = 0;
unsigned char data = 0;

void handleMessage() {
	if (g_MsgBuf_Count >= DEFAULT_BUF_SIZE) {
		g_MsgBuf_Count = 0;
	}
	data = getchar0();
	//putchar0('b');
	//putchar0(data);
	if (data == 0xFA) {
		g_MsgBuf_Count = 0;
		g_MsgBuf[g_MsgBuf_Count++] = data;
	} else if (data == 0xAF) {
		g_MsgBuf[g_MsgBuf_Count++] = data;
		processMessage(g_MsgBuf, g_MsgBuf_Count);
		g_MsgBuf_Count = 0;
	} else {
		g_MsgBuf[g_MsgBuf_Count++] = data;
	}
}

void processMessage(char *buf, int len) {
	int i = 0;
	int length = 0;
	char data[DEFAULT_BUF_SIZE];
	unsigned char checkSum = 0;

	//변환코드 원 코드로 재 변환.
	length += getOriginalCode(data, buf + 1, len - 2);

	//Length 확인.
	if (length - 2 != data[0])
		return;
	//checksum 확인.
	for (i = 1; i < length - 1; i++)
		checkSum += data[i];
	if (checkSum != data[length - 1])
		return;

	if (data[1] == ZIGBEE_CONTROL_PACKET) { //Zigbee Control
		switch (data[2]) {
		case 0x11: { //설정 정보 조회
			handleResZigbeeInfo(data);
		}
			break;
		}
	} else if (data[1] == ZIGBEE_DATA_PACKET) { //Application PDU
		//data 처리.
		if (data[6] == NCP_PROTOCOL_ID) {
			if (data[7] == PLCS_NCP_ID) {
				unsigned int nodeId = (int) data[10] << 8 | data[11];
				if (nodeId != ((int) ieeeAddr[6] << 8 | ieeeAddr[7]))
					return;
				switch (data[14]) {
				case NCP_RES_REGISTER:
					handleNCPRegister_Res(nodeId, (int) data[15] << 8 | data[16]);
					break;
				case NCP_RES_PING:
					handleNCPPing_Res(data[15]);
					break;
				}
			} else if (data[7] == PLCS_GCP_ID) {
				int seqNum = (int) data[8] << 8 | data[9];
				int nodeId = (int) data[17] << 8 | data[18];
				if (nodeId != ((int) ieeeAddr[6] << 8 | ieeeAddr[7]))
					return;
				switch (data[14]) {
				case PLCS_GCP_REQ_STATE_INFO:
					handleStateInfoReq(seqNum);
					break;
				case PLCS_GCP_REQ_SET_MODE:
					handleSetModeReq(seqNum, data[19]);
					break;
				case PLCS_GCP_REQ_SET_SENSINGLEVEL:
					handleSetSensingLevelReq(seqNum, data[19]);
					break;
				case PLCS_GCP_REQ_REBOOT:
					isReoobt = 1;
					break;
				}
			}
		}
	}
}

void handleNCPRegister_Res(int zigbeeId, int result) {
	if (result == 0) {
		RegistrationState = REGISTERED;
		RegistrationCount = 0;
		PingCount = 0;
		sendNCPPing_Req();
	} else
		sendReqZigbeeReset();
}
unsigned char tempBuf[64];
void sendNCPRegister_Req() {
	unsigned char len = 0;

	tempBuf[len++] = NCP_REQ_REGISTER;
	tempBuf[len++] = DEVICE_TYPE >> 8;
	tempBuf[len++] = DEVICE_TYPE;
	tempBuf[len++] = DEVICE_VERSION;
	tempBuf[len++] = FW_VERSION;
	sendNCPPacket(tempBuf, len);
}

void sendNCPPing_Req() {
	unsigned char len = 0;
	tempBuf[len++] = NCP_REQ_PING;
	tempBuf[len++] = 0;
	tempBuf[len++] = 0x00;
	sendNCPPacket(tempBuf, len);
}

void handleNCPPing_Res(int flag) {
	PingCount = 0;
	if (flag == 0x01)
		sendNCPRegister_Req();
}

void handleStateInfoReq(unsigned int seqNum) {
	unsigned char len = 0;
	tempBuf[len++] = PLCS_GCP_RES_STATE_INFO;
	tempBuf[len++] = 0;
	tempBuf[len++] = 0;
	tempBuf[len++] = ieeeAddr[6];
	tempBuf[len++] = ieeeAddr[7];
	tempBuf[len++] = DEVICE_TYPE >> 8;
	tempBuf[len++] = DEVICE_TYPE;
	tempBuf[len++] = Sensing_Threshold_Avr_Diff;

	sendGCPPacket(seqNum, tempBuf, len);
}

void handleSendEvent() {
	unsigned char len = 0;
	tempBuf[len++] = PLCS_GCP_NOTICE_EVENT;
	tempBuf[len++] = 0; //subNodeid
	tempBuf[len++] = 0; //subNodeId
	tempBuf[len++] = 0x01;
	tempBuf[len++] = ieeeAddr[6];
	tempBuf[len++] = ieeeAddr[7];

	sendGCPPacket(0, tempBuf, len);
}

void handleSendEvent_Debug() {
	unsigned char len = 0;
	unsigned char i = 0;
	tempBuf[len++] = PLCS_GCP_NOTICE_EVENT_DEBUG;
	tempBuf[len++] = 0; //subNodeid
	tempBuf[len++] = 0; //subNodeId
	tempBuf[len++] = ieeeAddr[6];
	tempBuf[len++] = ieeeAddr[7];
	tempBuf[len++] = ((int) Sensing_Standard_Avr + Sensing_Threshold_Avr_Diff) >> 8;
	tempBuf[len++] = (int) (Sensing_Standard_Avr + Sensing_Threshold_Avr_Diff);
	tempBuf[len++] = g_isSensored_Avr * 10;
	tempBuf[len++] = g_isSensored_Avr2 * 10;
	tempBuf[len++] = g_debug_data_size;

	for (i = 0; i < g_debug_data_size; i++) {
		tempBuf[len++] = g_debug_data_history[i] >> 8;
		tempBuf[len++] = g_debug_data_history[i];
	}
	g_debug_data_size = 0;

	sendGCPPacket(0, tempBuf, len);
}

void sendSetSensingLevelRes(unsigned int seqNum, unsigned int avrDiff) {
	unsigned char len = 0;

	tempBuf[len++] = PLCS_GCP_RES_SET_SENSINGLEVEL;
	tempBuf[len++] = 0;
	tempBuf[len++] = 0;
	tempBuf[len++] = ieeeAddr[6];
	tempBuf[len++] = ieeeAddr[7];
	tempBuf[len++] = 0;

	sendGCPPacket(seqNum, tempBuf, len);
}

void sendSetModeRes(unsigned int seqNum, unsigned char mode) {
	unsigned char len = 0;
	tempBuf[len++] = PLCS_GCP_RES_SET_MODE;
	tempBuf[len++] = 0;
	tempBuf[len++] = 0;
	tempBuf[len++] = ieeeAddr[6];
	tempBuf[len++] = ieeeAddr[7];
	tempBuf[len++] = 0;

	sendGCPPacket(seqNum, tempBuf, len);
}

void handleSetSensingLevelReq(unsigned int seqNum, unsigned int avrDiff) {
	Sensing_Threshold_Avr_Diff = avrDiff;
	sendSetSensingLevelRes(seqNum, avrDiff);
}

void handleSetModeReq(unsigned int seqNum, unsigned char mode) {
	if (mode == 0x00)
		NORMAL = 1;
	else
		NORMAL = 0;

	g_timer0_count_NormalMode = 0;

	sendSetModeRes(seqNum, mode);
}

unsigned int seqNumGenerator = 0;
void sendNCPPacket(char *payload, unsigned char len) {
	int i = 0;
	static char data[DEFAULT_BUF_SIZE];
	unsigned int length = 0;
	unsigned char checkSum = 0;
	unsigned char lenTemp = 0;
	unsigned char seqNum[2];
	seqNumGenerator = 0xdddd;
	seqNum[0] = seqNumGenerator >> 8;
	seqNum[1] = seqNumGenerator;
	seqNumGenerator++;

	lenTemp = len + 13;

	data[length++] = 0xFA;
	length += getTranslatedCode(&(data[length]), &lenTemp, 1);
	data[length++] = 0x10;
	data[length++] = 0x00;
	data[length++] = 0x00;
	length += getTranslatedCode(&(data[length]), netAddr, 2);
	data[length++] = NCP_PROTOCOL_ID;
	data[length++] = PLCS_NCP_ID;
	length += getTranslatedCode(&(data[length]), seqNum, 2);
	data[length++] = 0;
	data[length++] = 0;
	data[length++] = ieeeAddr[6];
	data[length++] = ieeeAddr[7];
	length += getTranslatedCode(&(data[length]), payload, len);

	for (i = 0; i < len; i++) {
		checkSum += payload[i];
	}
	checkSum += 0x10 + netAddr[0] + netAddr[1] + NCP_PROTOCOL_ID + PLCS_NCP_ID + seqNum[0] + seqNum[1] + ieeeAddr[6] + ieeeAddr[7];

	length += getTranslatedCode(&(data[length]), &checkSum, 1);
	data[length++] = 0xAF;
	putstr0(data, length);
}

void sendGCPPacket(unsigned int seqNum, char *payload, unsigned char len) {
	int i = 0;
	char data[DEFAULT_BUF_SIZE];
	unsigned int length = 0;
	unsigned char checkSum = 0;
	unsigned char lenTemp = 0;
	unsigned char seqNum_byte[2];

	seqNum_byte[0] = seqNum >> 8;
	seqNum_byte[1] = seqNum;

	lenTemp = len + 13;

	data[length++] = 0xFA;
	length += getTranslatedCode(&(data[length]), &lenTemp, 1);
	data[length++] = 0x10;
	data[length++] = 0x00;
	data[length++] = 0x00;
	length += getTranslatedCode(&(data[length]), netAddr, 2);
	data[length++] = NCP_PROTOCOL_ID;
	data[length++] = PLCS_GCP_ID;
	length += getTranslatedCode(&(data[length]), seqNum_byte, 2);
	data[length++] = 0;
	data[length++] = 0;
	data[length++] = ieeeAddr[6];
	data[length++] = ieeeAddr[7];
	length += getTranslatedCode(&(data[length]), payload, len);

	for (i = 0; i < len; i++) {
		checkSum += payload[i];
	}
	checkSum += 0x10 + netAddr[0] + netAddr[1] + NCP_PROTOCOL_ID + PLCS_GCP_ID + seqNum_byte[0] + seqNum_byte[1] + ieeeAddr[6] + ieeeAddr[7];

	length += getTranslatedCode(&(data[length]), &checkSum, 1);
	data[length++] = 0xAF;
	g_zigbeeSendTimer = 1;
	putstr0(data, length);
}

void initWatchDog() {
	// Watchdog Timer initialization
	// Watchdog Timer Prescaler: OSC/2048k
#pragma optsize-
	WDTCSR = 0x18;
	//delay_ms(1);
	WDTCSR = 0x1E;
	//delay_ms(1);
	//WDTCSR=0x0E;
#ifdef _OPTIMIZE_SIZE_
#pragma optsize+
#endif

}

void disableWatchDog() {
	// Watchdog Timer initialization
	// Watchdog Timer Prescaler: OSC/2048k
#pragma optsize-
	WDTCSR = 0x17;
	delay_ms(1);
	WDTCSR = 0x07;
#ifdef _OPTIMIZE_SIZE_
#pragma optsize+
#endif
}

void resetWatchDog() {
	disableWatchDog();
	initWatchDog();
}

void main(void) {
	unsigned char i = 0;

	initPort();
	initRotary();
	initUsart();
	initADC();
	LED_ON;

	for (i = 0; i < 5; i++) {
		LED_ON;
		delay_ms(200);
		LED_OFF;
		delay_ms(200);
	}
	LED_ON;

	sendReqZigbeeInfo();
	delay_ms(500);
	LED_OFF;

	initTimer0();
	initTimer1();
	initWatchDog();
	/*
	 while(1) {
	 delay_ms(500);
	 reset();
	 } */

	sendSetZigbeePreconfig(0x15);
	delay_ms(100);
	sendReqZigbeeReset();
	delay_ms(1000);

	// Global enable interrupts
	asm("sei");

	while (1) {
		handleMessage();
	};
}
