#ifndef _GPIO__H     
#define _GPIO__H
#include "common.h"

#define LED_ON PORTB |= 0x02
#define LED_OFF PORTB &= ~(0x02) 
#define LED_TOGGLE PORTB ^= 0x02;
void initPort();        
void reset();
#endif