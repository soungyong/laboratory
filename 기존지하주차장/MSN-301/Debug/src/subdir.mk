################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/ADC.c \
../src/GPIO.c \
../src/MSN-300Z.c \
../src/NCProtocol.c \
../src/RadarSensorProtocol.c \
../src/Zigbee.c \
../src/rotary.c \
../src/timer.c \
../src/usart.c 

OBJS += \
./src/ADC.o \
./src/GPIO.o \
./src/MSN-300Z.o \
./src/NCProtocol.o \
./src/RadarSensorProtocol.o \
./src/Zigbee.o \
./src/rotary.o \
./src/timer.o \
./src/usart.o 

C_DEPS += \
./src/ADC.d \
./src/GPIO.d \
./src/MSN-300Z.d \
./src/NCProtocol.d \
./src/RadarSensorProtocol.d \
./src/Zigbee.d \
./src/rotary.d \
./src/timer.d \
./src/usart.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega88pa -DF_CPU=8000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


