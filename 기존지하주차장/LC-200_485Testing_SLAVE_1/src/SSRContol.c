#include "SSRControl.h"

void SSR_init() {
	uint8 i = 0;

	DDRK |= 0xff;
	DDRL |= 0xff;

	for (i = 0; i < 8; i++) {
		SSR_On_Index(i);
		tmp_CircuitInfo.ctrlMode[i] = ON;
		tmp_CircuitInfo.state[i] = ON;
	}
}

uint8 GetSSRState(uint8 ssrID) {
	if ((PINF & (1 << ssrID)) == 0) // Low : SSR_ON
		return 1;

	else
		return 0;
}

void SSR_On_Index(int index) {
	if (index > 7 || index < 0)
		return;

	SSR_ON(1 << (7-index));
	LATCH_ON(1 << (7-index));

}

void SSR_Off_Index(int index) {
	if (index > 7 || index < 0)
		return;

	SSR_OFF(1 << (7-index));
	LATCH_OFF(1 << (7-index));
}
