#ifndef __MPR_PROTOCOL_H__
#define __MPR_PROTOCOL_H__
#include "util.h"
extern int mprToken;
char mpr_HasToken();
char mpr_GetAssignedTokenId();
void mpr_AssignToken(int id);
void mpr_ReleaseToken();
char plcs_NCP_AssignToken(uint16 nodeId);
#endif
