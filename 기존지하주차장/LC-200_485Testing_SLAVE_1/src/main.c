#include "main.h"
#include "SSRControl.h"
#include "DimmerProtocol.h"
#include "debug.h"
#include "PatternBasedControl.h"
#include "Rotary.h"
#include "flash.h"
#include "RFU_Protocol.h"
#include "MPRProtocol.h"

#include "DeviceInfo.h"
#include "MyDebug.h"

// Avrx hw Peripheral initialization
#define CPUCLK 		16000000L     		// CPU xtal
#define TICKRATE 	1000				// AvrX timer queue Tick rate
#define TCNT0_INIT 	(CPUCLK/128/TICKRATE)

// NB: Clock divisor bits are different between classic and mega103 chips!
// NB: IAR chooses to use different CPU identifiers for their C compiler
// NB: GCC changes the IO bit names just for fun...

#define TCCR0_INIT	((1<<CS02) | (0<<CS01) | (1<<CS00))

#define	ACCESS_0WAIT			0
#define	ACCESS_1WAIT			1
#define	ACCESS_2WAIT			2
#define	ACCESS_3WAIT			3
#define	ACCESS_NUM_WAIT		ACCESS_3WAIT

device_Info tmp_DevInfo_D;

//Sensor_Table *SSRID;

uint8 Min, Hour;
uint8 NowTime;
uint8 applyingSchedule = DEFAULT;

uint8 SensorPacket[64];
uint8 GatewayPacket[64];
uint8 MPRPacket[64];

volatile uint32 g_PowerMeterValue = 0;

void xmem_enable(void) __attribute__ ((naked)) __attribute ((section(".init1")));

//-----------------------------------------------------------------------------
//MCU Initialize
void InitMCU() {
	/*PORTA = 0x00;
	 DDRA = 0xFF;
	 PORTC = 0x00;
	 DDRC = 0xFF;
	 PORTD = 0x80;
	 DDRD = 0x90;
	 */
	//LATCH
//	PORTL = 0xFF;
	//DDRL = 0xFF;
	//ked
	PORTK = 0xFF;
	DDRK = 0xFF;

	DDRD |= 0x20;
	PORTD |= 0x20;

	DDRL |= 0xFF;
	PORTL |= 0xFF;

	DDRG |= 0x20;
	/*
	 PORTG = 0xFF;
	 DDRG = 0xFF;
	 */

}

void resetZigbee0() {
	PORTE |= 0x04;
	MSLEEP(100);
	PORTE &= ~(0x04);
}

void resetZigbee1() {
	PORTJ |= 0x04;
	MSLEEP(100);
	PORTJ &= ~(0x04);
}

void xmem_enable(void) {
#if (ACCESS_NUM_WAIT == ACCESS_0WAIT)
	MCUCR = 0x80;
	XMCRA=0x40;

#elif (ACCESS_NUM_WAIT == ACCESS_1WAIT)
	MCUCR = 0xC0; // MCU control regiseter : enable external ram
	XMCRA=0x40;// External Memory Control Register A :
			   // Low sector   : 0x1100 ~ 0x7FFF
			   // Upper sector : 0x8000 ~ 0xFFFF

#elif (ACCESS_NUM_WAIT == ACCESS_2WAIT )
	MCUCR = 0x80;
	XMCRA=0x42;

#elif (ACCESS_NUM_WAIT == ACCESS_3WAIT)
	MCUCR = 0x80;
	XMCRA = 0xCA;
	XMCRB = 0x80;

#else
#error "unknown atmega128 number wait type"
#endif
}

//----------------------------------------------------------------------//

void WDT_INIT() {
	MCUSR &= ~(1 << WDRF); // WatchDog Init(Low)
	wdt_enable(WDTO_4S);
	// WatchDog Reset Time(High)
}

void Led_Task() {
	static uint8 state = 0;

	if (timer_isfired(ON_LED_TIMER_ID)) {
		wdt_reset();

		if (state == 1)
			STATE_LED_OFF();
		else
			STATE_LED_ON();

		state = !state;

		timer_clear(ON_LED_TIMER_ID);
		timer_set(ON_LED_TIMER_ID, 500);
	}
}

void ControlTask() {
	static uint8 i = 0;
	static uint8 j = 0;
	static uint16 whReport_Count = 0;
	static uint16 stateInfo_Count = 0;
	static uint16 clockCount = 0;
	static uint8 prevState[NumOfChannels];

	if (timer_isfired(ON_TIME_COUNT_ID)) {
		clockCount++;

		// 시간 업데이트
		if (clockCount >= 600) {
			Min += 1;
			if (Min >= 60) {
				Hour += 1;
				Min = 0;
			}
			if (Hour >= 24) {
				Hour = 0;
			}
			clockCount = 0;
		}

		// 30초 마다 한번씩 센서에 state 요청
		if (clockCount % 300 == 3) {
			for (int i = 0; i < deviceInfoTable.size; i++) {
				wdt_reset();
				device_Info device = deviceInfoTable.deviceInfo[i];
				if (device.deviceType == PLCS_ZSENSOR_TYPE) {
					int len = 0;
					uint8 payload[10];
					uint8 tmp_Buff[200];
					uint8 resultLen;
					len = 0;
					payload[len++] = PLCS_GCP_REQ_STATEINFO;
					payload[len++] = device.ieeeId >> 8;
					payload[len++] = device.ieeeId;
					payload[len++] = device.ieeeId >> 8;
					payload[len++] = device.ieeeId;

					resultLen = plcs_GetGCPMessage(tmp_Buff, device.ieeeId, zrmpInfo_0.zrm_Id, payload, len);
					sendMessage(ZIGBEE, device.ieeeId, tmp_Buff, resultLen);
				}
			}
		}

		//30초 마다 한번씩 일루디밍 계산
		if (clockCount % 300 == 20) {
			// 한번씩 illu_Dimming_State 계산.
			uint8 circuitIdIndex;
			uint16 sensorMappingInfoIndex = 0;
			uint8 numOfSensorOfAll = 0;
			uint8 numOfSensorOfCircuit = 0;
			uint32 avrLuxOfAll = 0;
			uint32 avrLuxOfCircuit = 0;
			device_Info *deviceP = NULL;

			for (circuitIdIndex = 0; circuitIdIndex < NumOfChannels; circuitIdIndex++) {
				wdt_reset();
				avrLuxOfAll = 0;
				avrLuxOfCircuit = 0;
				numOfSensorOfAll = 0;
				numOfSensorOfCircuit = 0;
				for (sensorMappingInfoIndex = 0; sensorMappingInfoIndex < MaxNumOfSensorMappingInfo; sensorMappingInfoIndex++) {
					wdt_reset();
					deviceP = findNodeById(sensorMappingTable.mappingInfo[sensorMappingInfoIndex].sensorId);
					if (deviceP == NULL) {
//						DEBUG("CONTINUE BY 3\n\r");
						continue;
					}

					SensorMappingInfo_st info_st = sensorMappingTable.mappingInfo[sensorMappingInfoIndex];

					wdt_reset();
					USART2_TxEnable();
					DEBUG("CircuitId : %d, deviceP.ieeeId 0X%04X, deviceP.deviceType 0X%04X\n\r", circuitIdIndex, deviceP->ieeeId, deviceP->deviceType);
					_delay_ms(1);
					DEBUG("SensorId : 0X%04X, lcId : 0X%04X, isEnable : 0X%04X, circuit:0X%04X\n\r", info_st.sensorId, info_st.lcId, info_st.isEnable, info_st.circuitId);
					_delay_ms(1);
					wdt_reset();

					if (sensorMappingTable.mappingInfo[sensorMappingInfoIndex].isEnable == 0) {
						DEBUG("CONTINUE BY 1\n\r");
						continue;
					}
					if (sensorMappingTable.mappingInfo[sensorMappingInfoIndex].lcId != zrmpInfo_0.zrm_Id) {
						DEBUG("CONTINUE BY 2\n\r");
						continue;
					}
					if (deviceP == NULL) {
						DEBUG("CONTINUE BY 3\n\r");
						continue;
					}
					if (sensorMappingTable.mappingInfo[sensorMappingInfoIndex].circuitId == circuitIdIndex) {
						DEBUG("==>ADDING LUX VALUE TO CIRCUIT %d \n\r", circuitIdIndex);
						_delay_ms(1);
						avrLuxOfCircuit += deviceP->luxAvr;
						numOfSensorOfCircuit++;
					}
					avrLuxOfAll += deviceP->luxAvr;
					numOfSensorOfAll++;

					USART2_TxDisable();
				}

				if (numOfSensorOfCircuit > 0) {
					avrLuxOfCircuit /= numOfSensorOfCircuit;
				} else {
					if (numOfSensorOfAll > 0) {
						avrLuxOfAll /= numOfSensorOfAll;
					}
					if (numOfSensorOfCircuit == 0) {
						avrLuxOfCircuit = avrLuxOfAll;
					}
				}

				uint16 luxDiff = 0;
				uint16 unitLux = 70;
				uint16 targetLux = 0;

				// 일루 디밍일 시
				if (tmp_CircuitInfo.ctrlMode[circuitIdIndex] >= ILLU_DIMMING_0 && tmp_CircuitInfo.ctrlMode[circuitIdIndex] <= ILLU_DIMMING_10) {
					targetLux = 1000 - ((tmp_CircuitInfo.ctrlMode[circuitIdIndex] - ILLU_DIMMING_0) * unitLux);

					if (targetLux > avrLuxOfCircuit) {
						luxDiff = targetLux - avrLuxOfCircuit;

						if (luxDiff > 2 * unitLux) {
							if (tmp_CircuitInfo.prevIlluState[circuitIdIndex][tmp_CircuitInfo.ctrlMode[circuitIdIndex] - ILLU_DIMMING_0] > DIMMING_0) {
								tmp_CircuitInfo.prevIlluState[circuitIdIndex][tmp_CircuitInfo.ctrlMode[circuitIdIndex] - ILLU_DIMMING_0]--;
							}
						}

					} else if (avrLuxOfCircuit > targetLux) {
						luxDiff = avrLuxOfCircuit - targetLux;
						if (luxDiff > 2 * unitLux) {
							if (tmp_CircuitInfo.prevIlluState[circuitIdIndex][tmp_CircuitInfo.ctrlMode[circuitIdIndex] - ILLU_DIMMING_0] < DIMMING_10)
								tmp_CircuitInfo.prevIlluState[circuitIdIndex][tmp_CircuitInfo.ctrlMode[circuitIdIndex] - ILLU_DIMMING_0]++;
						}
					}
					tmp_CircuitInfo.prevIlluStateAge[circuitIdIndex][tmp_CircuitInfo.ctrlMode[circuitIdIndex] - ILLU_DIMMING_0] = 0;
				}

				for (j = 0; j < 11; j++) {
					if (tmp_CircuitInfo.prevIlluStateAge[circuitIdIndex][j] < 60) {
						tmp_CircuitInfo.prevIlluStateAge[circuitIdIndex][j]++;
					} else {
						//30분 이상 이전 상태 업데이트 안되었으면 원 디밍 상태로 이전 상태 복원.
						tmp_CircuitInfo.prevIlluState[circuitIdIndex][j] = tmp_CircuitInfo.ctrlMode[circuitIdIndex] - ILLU_DIMMING_0 + DIMMING_0;
					}
				}
			}
			debug_UpdateConnection();
		}

		whReport_Count++;
		if (whReport_Count > 600) {
			if (ncp_ConnState() == NCP_NET_REGISTER)
				plcs_GCP_SendPowermeterRes(GATEWAY, 0, 0, zrmpInfo_0.zrm_Id, g_PowerMeterValue);
			whReport_Count = 0;
		}

		//stateInfo report;
		if (stateInfo_Count > 20) {
			plcs_GCP_SendStateInfoRes(GATEWAY, 0, 0, zrmpInfo_0.zrm_Id, 0);
			stateInfo_Count = 0;
		}
		stateInfo_Count++;

		for (i = 0; i < NumOfChannels; i++) {
			if (tmp_CircuitInfo.ctrlMode[i] == SENSOR) {
				if (tmp_CircuitInfo.expirationTime[i] == 0x00)
					tmp_CircuitInfo.state[i] = tmp_CircuitInfo.offCtrlMode[i];
				else if (tmp_CircuitInfo.expirationTime[i] == 0xffff) {
					tmp_CircuitInfo.state[i] = tmp_CircuitInfo.onCtrlMode[i];
				} else {
					tmp_CircuitInfo.timerCnt[i] = tmp_CircuitInfo.timerCnt[i] + 1;
					if (tmp_CircuitInfo.expirationTime[i] * 10 <= tmp_CircuitInfo.timerCnt[i]) {
						tmp_CircuitInfo.state[i] = tmp_CircuitInfo.offCtrlMode[i];
						tmp_CircuitInfo.timerCnt[i] = 0;
						tmp_CircuitInfo.expirationTime[i] = 0;
					} else {
						tmp_CircuitInfo.state[i] = tmp_CircuitInfo.onCtrlMode[i];
					}
				}
			} else if (tmp_CircuitInfo.ctrlMode[i] == SCHEDULE) {
				uint8 ctrlState = getControlModeAt(applyingSchedule, i, Hour, Min);
				if (ctrlState == SENSOR) {
					if (tmp_CircuitInfo.expirationTime[i] == 0x00)
						tmp_CircuitInfo.state[i] = tmp_CircuitInfo.offCtrlMode[i];
					else if (tmp_CircuitInfo.expirationTime[i] == 0xffff) {
						tmp_CircuitInfo.state[i] = tmp_CircuitInfo.onCtrlMode[i];
					} else {
						tmp_CircuitInfo.timerCnt[i] = tmp_CircuitInfo.timerCnt[i] + 1;
						if (tmp_CircuitInfo.expirationTime[i] * 10 <= tmp_CircuitInfo.timerCnt[i]) {
							tmp_CircuitInfo.state[i] = tmp_CircuitInfo.offCtrlMode[i];
							tmp_CircuitInfo.timerCnt[i] = 0;
							tmp_CircuitInfo.expirationTime[i] = 0;
						} else {
							tmp_CircuitInfo.state[i] = tmp_CircuitInfo.onCtrlMode[i];
						}
					}
				} else
					tmp_CircuitInfo.state[i] = ctrlState;
			} else if (tmp_CircuitInfo.ctrlMode[i] >= ILLU_DIMMING_0 && tmp_CircuitInfo.ctrlMode[i] <= ILLU_DIMMING_10) {
				//state에 IlluState 적용
				tmp_CircuitInfo.state[i] = tmp_CircuitInfo.prevIlluState[i][tmp_CircuitInfo.ctrlMode[i] - ILLU_DIMMING_0];
			} else {
				tmp_CircuitInfo.state[i] = tmp_CircuitInfo.ctrlMode[i];
			}
		}

		//state에 따라 회로 제어.
		for (i = 0; i < NumOfChannels; i++) {
			if (tmp_CircuitInfo.state[i] == OFF) {
				SSR_Off_Index(i);
			} else if (tmp_CircuitInfo.state[i] == ON) {
				SSR_On_Index(i);
			} else {
				SSR_On_Index(i);
			}
			if (prevState[i] != tmp_CircuitInfo.state[i])
				timer_set(DIMM_CONTROL_TIMER_ID, 0);

			prevState[i] = tmp_CircuitInfo.state[i];
		}
		PORTD ^= 0x20; //Latch enable

		timer_clear(ON_TIME_COUNT_ID);
		timer_set(ON_TIME_COUNT_ID, 100);
	}
}

uint8 ZigbeePacket[MAX_TX_BUFF];
uint8 msgFromZigbee[MAX_TX_BUFF];
void Zigbee_Task() {
	static uint8 Recvlen = 0;
	static uint16 count = 1;
	static uint16 zigbeeHWResetCount_NetworkTrouble_0 = 0;
	static uint16 zigbeeHWResetCount_NetworkTrouble_1 = 0;
	static uint8 i = 0;

	if (timer_isfired(ZIGBEE_PING_ID)) {
		uint8 numOfOfflineDevice = 0;
		if (count++ == 5) {
			ZRMsendPing(ZIGBEE_0);
			ZRMsendPing(ZIGBEE_1);
			count = 0;
		}
		zigbeeHWResetCount_NetworkTrouble_0++;
		zigbeeHWResetCount_NetworkTrouble_1++;

		for (i = 0; i < deviceInfoTable.size; i++) {
			if (deviceInfoTable.deviceInfo[i].connectionState == ONLINE)
				deviceInfoTable.deviceInfo[i].cnt++;

			if (deviceInfoTable.deviceInfo[i].cnt > 120) {
				deviceInfoTable.deviceInfo[i].connectionState = OFFLINE;
			}

			if (deviceInfoTable.deviceInfo[i].connectionState == OFFLINE && deviceInfoTable.deviceInfo[i].deviceType == PLCS_ZSENSOR_TYPE)
				numOfOfflineDevice++;
		}

		if (numOfOfflineDevice / 1.0 / deviceInfoTable.size > 0.2) {
			resetZigbee0();
			for (i = 0; i < NumOfChannels; i++)
				if (tmp_CircuitInfo.ctrlMode[i] == SENSOR)
					if (tmp_CircuitInfo.expirationTime[i] != 0xffff) {
						tmp_CircuitInfo.timerCnt[i] = 0;
						tmp_CircuitInfo.expirationTime[i] = 100;
					}

			for (i = 0; i < deviceInfoTable.size; i++) {
				deviceInfoTable.deviceInfo[i].connectionState = ONLINE;
				deviceInfoTable.deviceInfo[i].cnt = 0;
			}
		}

		timer_clear(ZIGBEE_PING_ID);
		timer_set(ZIGBEE_PING_ID, 1000);
	}

	if (zigbeeHWResetCount_NetworkTrouble_0 > 20) {
		resetZigbee0();
		zigbeeHWResetCount_NetworkTrouble_0 = 0;
		if (deviceInfoTable.size > 0) {
			for (i = 0; i < NumOfChannels; i++)
				if (tmp_CircuitInfo.ctrlMode[i] == SENSOR)
					if (tmp_CircuitInfo.expirationTime[i] != 0xffff) {
						tmp_CircuitInfo.timerCnt[i] = 0;
						tmp_CircuitInfo.expirationTime[i] = 100;
					}
		}
	}

	if (zigbeeHWResetCount_NetworkTrouble_1 > 60) {
		resetZigbee1();
		zigbeeHWResetCount_NetworkTrouble_1 = 0;
	}

	if ((Recvlen = xcps_recv_zigbee_0(ZigbeePacket, 64)) > 0) {
		uint8 buff_len = 0;
		uint16 Src_Addr;
		uint16 Dst_Addr;

		switch (ZigbeePacket[0]) {
		case 0x10: // Communication between lc<-> zigbee device.
			if (getZigbeeState(ZIGBEE_0) == ZRM_CONNECT) {
				Dst_Addr = (uint16) (ZigbeePacket[1] << 8) | (ZigbeePacket[2]);
				Src_Addr = (uint16) (ZigbeePacket[3] << 8) | (ZigbeePacket[4]);

				buff_len = ZRM_getOriginalCode(msgFromZigbee, &(ZigbeePacket[5]), Recvlen - 5);

				XNetHandlerFromZigbee(Src_Addr, msgFromZigbee, buff_len);
				zigbeeHWResetCount_NetworkTrouble_0 = 0;
			}
			break;
		case 0x00: // Communication between lc<->zigbee
			buff_len = (Recvlen - 1);
			ZRMPMessage(ZIGBEE_0, &ZigbeePacket[1], buff_len);
			break;

		default: // Error Mesaage
			break;
		}
	}

	if ((Recvlen = xcps_recv_zigbee_1(ZigbeePacket, 64)) > 0) {
		uint8 buff_len = 0;
		uint16 Src_Addr;
		uint16 Dst_Addr;

		switch (ZigbeePacket[0]) {
		case 0x10: // Communication between lc<-> zigbee device.
			if (getZigbeeState(ZIGBEE_1) == ZRM_CONNECT) {
				Dst_Addr = (uint16) (ZigbeePacket[1] << 8) | (ZigbeePacket[2]);
				Src_Addr = (uint16) (ZigbeePacket[3] << 8) | (ZigbeePacket[4]);

				buff_len = ZRM_getOriginalCode(msgFromZigbee, &(ZigbeePacket[5]), Recvlen - 5);

				XNetHandlerFromZigbee(Src_Addr, msgFromZigbee, buff_len);
				zigbeeHWResetCount_NetworkTrouble_1 = 0;
			}
			break;
		case 0x00: // Communication between lc<->zigbee
			buff_len = (Recvlen - 1);
			ZRMPMessage(ZIGBEE_1, &ZigbeePacket[1], buff_len);
			break;

		default: // Error Mesaage
			break;
		}
	}

}

void GatewayRx_Task() {
	static uint8 Recvlen;

	if ((Recvlen = xcps_recv_rs485(GatewayPacket, 64)) > 0) {
		XNetCommandFromGateway(GatewayPacket, Recvlen);
	} // End of (Recvlen = xcps...)
}

void GatewayTx_Task() {
	static uint8 cnt = 0;
	if (timer_isfired(RESEND_TIMER_ID)) {
		if (hasToken())
			if (sendStackedServerMessage() == 0 && cnt > 10) {
				plcs_NCP_Send_NCPReqReleaseToken(GATEWAY, 0);
				cnt = 0;
			}
		if (cnt < 100)
			cnt++;
		timer_clear(RESEND_TIMER_ID);
		timer_set(RESEND_TIMER_ID, 10);
	}

	if (timer_isfired(ON_REGISTER_GW_TIMER_ID)) {
		if ((ncp_ConnState() != NCP_NET_REGISTER) && (getZigbeeState(ZIGBEE_0) == ZRM_CONNECT)) {
			ncp_SendRegisterReq(GATEWAY);
			timer_clear(ON_REGISTER_GW_TIMER_ID);
			timer_set(ON_REGISTER_GW_TIMER_ID, 5000);
		} else {
			timer_clear(ON_REGISTER_GW_TIMER_ID);
			timer_set(ON_REGISTER_GW_TIMER_ID, 60000);
		}
	}

	if (timer_isfired(NCP_PING_TIMER_ID)) {
		ncp_SendPingReq(zrmpInfo_0.zrm_Id);

		timer_clear(NCP_PING_TIMER_ID);
		timer_set(NCP_PING_TIMER_ID, 10000);
	}
}

void MPRRx_Task() {
	static uint8 Recvlen;

	if ((Recvlen = xcps_recv_rs485ForMPR(MPRPacket, 64)) > 0) {
		XNetCommandFromMPR(MPRPacket, Recvlen);
	} // End of (Recvlen = xcps...)
}

void MPRTx_Task() {
	if (timer_isfired(SEND_TIMER_MPR_ID)) {
		if (mpr_HasToken())
			sendStackedMPRMessage();

		timer_clear(SEND_TIMER_MPR_ID);
		timer_set(SEND_TIMER_MPR_ID, 10);
	}
}

void ZigbeeSend_Task() {
	if (timer_isfired(ZIGBEE_SEND_ID)) {
		timer_clear(ZIGBEE_SEND_ID);
		timer_set(ZIGBEE_SEND_ID, 50);

		sendQueueingMessage(ZIGBEE_0);
		sendQueueingMessage(ZIGBEE_1);
	}
}

//Dimmer Control Data 전송 전용.
void DimmerControl_Task() {
	static uint8 dimmCnt = 0;
	uint8 index = 0;
	uint8 index2 = 0;

	if (timer_isfired(DIMM_CONTROL_TIMER_ID)) {
		timer_clear(DIMM_CONTROL_TIMER_ID);
		timer_set(DIMM_CONTROL_TIMER_ID, 100);

		dimmCnt++;

		//상태 바뀐 녀석들부터 전송.
		for (index = 0; index < dimmerControlInfoList.size; index++)
			for (index2 = 0; index2 < 4; index2++) {
				if (dimmerControlInfoList.info[index].channelState[index2] == NULL)
					continue;

				if (dimmerControlInfoList.info[index].prevChannelState[index2] != *(dimmerControlInfoList.info[index].channelState[index2])) {
					dimmer_SendReqDimming(&(dimmerControlInfoList.info[index]));
					break;
				}
			}
		//2초당 1회 전체 제어값 재전송
		if (dimmCnt > 20) {
			dimmCnt = 0;
			for (index = 0; index < dimmerControlInfoList.size; index++)
				dimmer_SendReqDimming(&(dimmerControlInfoList.info[index]));
		}
	}
}

void NoticeMode_Task() {
	if (timer_isfired(GCP_NOTICEMDOE_ID)) {
		timer_clear(GCP_NOTICEMDOE_ID);
		gcp_SetNoticeMode(1);
	}
}

void Token_Task() {
	static char index = 0;
	static int cnt = 0;

	if (timer_isfired(TOKEN_TIMER_MPR_ID)) {
		if (mpr_HasToken()) {
			if (cnt > 50) {

				//assign token
				mpr_AssignToken(index);
				index++;
				if (index >= 8)
					index = 0;
				cnt = 0;
			}
		} else if (cnt > 70) {
			mpr_ReleaseToken();
			cnt = 0;
		}
		cnt++;
		timer_clear(TOKEN_TIMER_MPR_ID);
		timer_set(TOKEN_TIMER_MPR_ID, 1);
	}
}
//-----------------------------------------------------------------------------
// Main function
//-----------------------------------------------------------------------------
int main(void) {
	InitMCU();
	InitUART();
	SSR_init();
	timer_init();

	WDT_INIT();
	wdt_reset();
	MSLEEP(1000);
	wdt_reset();

	timer_set(1, 500);
	timer_set(2, 1000);

	// 디버그 이닛
	FILE mystdout = FDEV_SETUP_STREAM((void *)USART3_Transmit, NULL, _FDEV_SETUP_WRITE);
	stdout = &mystdout;

	DEBUG("================\n\r");
	DEBUG("StartProgram....\n\r");
	DEBUG("================\n\r");

	while (1) {
		wdt_reset();
		uint8 tmp;
		USART2_TxDisable();

		if (USART2_Receive(&tmp) > 0) {
			if (tmp == '1') {
				USART2_TxEnable();
				_delay_ms(1);
				USART2_Transmit(tmp);
				_delay_ms(1);
				USART2_TxDisable();
			}
		}

		if (timer_isfired(1)) {
			USART2_TxEnable();
			_delay_ms(1);
			USART2_Transmit('3');
			_delay_ms(1);
			USART2_TxDisable();

			static char flag = 0;
			if (flag) {
				SSR_On_Index(0);
				flag = 0;
			} else {
				SSR_Off_Index(0);
				flag = 1;
			}
			timer_set(1, 1000);
		}

	}

	return 0;
}

