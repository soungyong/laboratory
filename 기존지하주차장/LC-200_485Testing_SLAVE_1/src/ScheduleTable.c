#include "ScheduleTable.h"
#include "deviceInfo.h"
#include "Flash.h"
#include <avr/wdt.h>

uint8 tempBuffForSchedule[255];
void initScheduleTimeTable() {
	uint16 i = 0;
	uint8 j = 0;
	uint8 k = 0;

	//Page당 8개 mapping 정보 기록. 총 240바이트.
	for (i = 0; i < MaxNumOfCircuit; i += 8) {
		wdt_reset();
		flash_readBytes(FLASH_ADDR_SCHEDULE_INFO + i / 8, tempBuffForSchedule, 255);
		for (j = 0; j < 8; j++) {
			for (k = 0; k < MaxNumOfScheduleInfo; k++) {
				scheduleTable_1.scheduleInfo[i * 8 + j][k].ctrlMode = tempBuffForSchedule[0 + 2 * k
						+ j * 3];
				scheduleTable_1.scheduleInfo[i * 8 + j][k].onTime_10m = tempBuffForSchedule[1
						+ 2 * k + j * 3];
			}
		}
	}
	flash_readBytes(FLASH_ADDR_SCHEDULE_INFO + 2, tempBuffForSchedule, 255);
	for (i = 0; i < MaxNumOfCircuit; i++)
		scheduleTable_1.size[i] = tempBuffForSchedule[i];

	for (i = 0; i < MaxNumOfCircuit; i += 8) {
		wdt_reset();
		flash_readBytes(FLASH_ADDR_SCHEDULE_INFO + (i / 8) + 3, tempBuffForSchedule, 255);
		for (j = 0; j < 8; j++) {
			for (k = 0; k < MaxNumOfScheduleInfo; k++) {
				scheduleTable_2.scheduleInfo[i * 8 + j][k].ctrlMode = tempBuffForSchedule[0 + 2 * k
						+ j * 3];
				scheduleTable_2.scheduleInfo[i * 8 + j][k].onTime_10m = tempBuffForSchedule[1
						+ 2 * k + j * 3];
			}
		}
	}
	flash_readBytes(FLASH_ADDR_SCHEDULE_INFO + 2 + 3, tempBuffForSchedule, 255);
	for (i = 0; i < MaxNumOfCircuit; i++)
		scheduleTable_2.size[i] = tempBuffForSchedule[i];

	for (i = 0; i < MaxNumOfCircuit; i++) {
		defaultScheduleTable.size[i] = 0;

		if (scheduleTable_1.size[i] >= MaxNumOfScheduleInfo)
			scheduleTable_1.size[i] = 0;
		if (scheduleTable_2.size[i] >= MaxNumOfScheduleInfo)
			scheduleTable_2.size[i] = 0;
	}

	/*
	 for (i = 0; i < MaxNumOfCircuit; i++) {
	 scheduleTable_1.size[i] = 0;
	 scheduleTable_2.size[i] = 0;
	 //eeprom_read_block(&(defaultScheduleTable.size[i]), &ee_DefaultScheduleTimeSize[i],
	 //	sizeof(uint8));

	 if (defaultScheduleTable.size[i] >= MaxNumOfScheduleInfo)
	 defaultScheduleTable.size[i] = 0;
	 if (scheduleTable_1.size[i] >= MaxNumOfScheduleInfo)
	 scheduleTable_1.size[i] = 0;
	 if (scheduleTable_2.size[i] >= MaxNumOfScheduleInfo)
	 scheduleTable_2.size[i] = 0;

	 //for (j = 0; j < defaultScheduleTable.size[i]; j++)
	 //eeprom_read_block(&(defaultScheduleTable.scheduleInfo[i][j]),
	 //	&(ee_DefaultScheduleTime[i][j]), sizeof(Schedule_Info));
	 }*/
}

// --------------------------------------------------------------------------------------------------------- //
// ------------------ Add Schedule Table ---------------------------------------------------------------------- //
// --------------------------------------------------------------------------------------------------------- //
int addSchedule(uint8 scheduleType, uint8 circuitId, uint8 hour, uint8 minute, uint8 ctrlMode) {
	uint8 onTime = 0;
	uint8 index = 255;

	Schedule_Table_st * scheduleTable_p = NULL;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == SCHEDULE_1)
		scheduleTable_p = &scheduleTable_1;
	else if (scheduleType == SCHEDULE_2)
		scheduleTable_p = &scheduleTable_2;
	else
		return 0;

	if (scheduleTable_p->size[circuitId] >= MaxNumOfScheduleInfo)
		return 0;
	if (circuitId >= MaxNumOfCircuit)
		return 0;

	if (isContainScheduleAt(scheduleType, circuitId, hour, minute)) {
		onTime = hour * 6 + minute / 10;

		for (index = 0; index < scheduleTable_p->size[circuitId]; index++)
			if (scheduleTable_p->scheduleInfo[circuitId][index].onTime_10m == onTime)
				scheduleTable_p->scheduleInfo[circuitId][index].ctrlMode = ctrlMode;
	} else {
		scheduleTable_p->scheduleInfo[circuitId][scheduleTable_p->size[circuitId]].onTime_10m = hour
				* 6 + minute / 10;
		scheduleTable_p->scheduleInfo[circuitId][scheduleTable_p->size[circuitId]].ctrlMode =
				ctrlMode;
		(scheduleTable_p->size[circuitId])++;
	}

	return 1;
}

int addScheduleList(uint8 scheduleType, uint8 circuitId, uint8 numOfSchedule, uint8 hour[],
		uint8 minute[], uint8 ctrlMode[]) {
	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == SCHEDULE_1)
		scheduleTable_p = &scheduleTable_1;
	else if (scheduleType == SCHEDULE_2)
		scheduleTable_p = &scheduleTable_2;
	else
		return 0;

	if (scheduleTable_p->size[circuitId] + numOfSchedule > MaxNumOfScheduleInfo)
		return 0;
	if (circuitId >= MaxNumOfCircuit)
		return 0;

	for (int i = 0; i < numOfSchedule; i++) {
		addSchedule(scheduleType, circuitId, hour[i], minute[i], ctrlMode[i]);
		wdt_reset();
	}

	schedule_WriteToEEPRomAll(scheduleType, circuitId);

	return 1;
}

uint8 getControlModeAt(uint8 scheduleType, uint8 circuitId, uint8 hour, uint8 minute) {
	uint8 i = 0;
	uint8 onTime;
	uint8 scheduleTime = 200;
	uint8 resultCtrlMode = ON;

	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		return ON;
	else if (scheduleType == SCHEDULE_1)
		scheduleTable_p = &scheduleTable_1;
	else if (scheduleType == SCHEDULE_2)
		scheduleTable_p = &scheduleTable_2;
	else
		return ON;

	if (scheduleTable_p->size[circuitId] == 0)
		return ON;
	else if (scheduleTable_p->size[circuitId] == 1)
		return scheduleTable_p->scheduleInfo[circuitId][0].ctrlMode;

	onTime = hour * 6 + minute / 10;

	for (i = 0; i < scheduleTable_p->size[circuitId]; i++) {
		if (scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m <= onTime)
			if (scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m >= scheduleTime
					|| scheduleTime == 200) {
				scheduleTime = scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m;
				resultCtrlMode = scheduleTable_p->scheduleInfo[circuitId][i].ctrlMode;
			}
	}

	if (scheduleTime == 200) {
		scheduleTime = 0;
		for (i = 0; i < scheduleTable_p->size[circuitId]; i++) {
			if (scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m >= scheduleTime) {
				scheduleTime = scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m;
				resultCtrlMode = scheduleTable_p->scheduleInfo[circuitId][i].ctrlMode;
			}
		}

		return resultCtrlMode;
	}

	return resultCtrlMode;
}

uint8 isContainScheduleAt(uint8 scheduleType, uint8 circuitId, uint8 hour, uint8 minute) {
	uint8 i = 0;
	uint8 onTime;
	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == SCHEDULE_1)
		scheduleTable_p = &scheduleTable_1;
	else if (scheduleType == SCHEDULE_2)
		scheduleTable_p = &scheduleTable_2;
	else
		return 0;

	onTime = hour * 6 + minute / 10;

	for (i = 0; i < scheduleTable_p->size[circuitId]; i++)
		if (scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m == onTime)
			return 1;

	return 0;
}

void schedule_WriteToEEPRom(uint8 scheduleType, uint8 circuitId, uint8 index) {
	uint8 flash_offSet = 0;
	Schedule_Table_st * scheduleTable_p = NULL;
	if (scheduleType == DEFAULT)
		return;
	else if (scheduleType == SCHEDULE_1) {
		flash_offSet = 0;
		scheduleTable_p = &scheduleTable_1;
	} else if (scheduleType == SCHEDULE_2) {
		flash_offSet = 3;
		scheduleTable_p = &scheduleTable_2;
	}

	wdt_reset();
	flash_readBytes(FLASH_ADDR_SCHEDULE_INFO + (circuitId / 8) + flash_offSet, tempBuffForSchedule,
			255);

	tempBuffForSchedule[0 + 2 * index + (circuitId % 12) * 3] =
			scheduleTable_p->scheduleInfo[circuitId][index].ctrlMode;
	tempBuffForSchedule[1 + 2 * index + (circuitId % 12) * 3] =
			scheduleTable_p->scheduleInfo[circuitId][index].onTime_10m;

	flash_writeBytes(FLASH_ADDR_SCHEDULE_INFO + (circuitId / 8) + flash_offSet, tempBuffForSchedule,
			255);

	flash_readBytes(FLASH_ADDR_SCHEDULE_INFO + 2 + flash_offSet, tempBuffForSchedule, 255);
	tempBuffForSchedule[circuitId] = scheduleTable_p->size[circuitId];
	flash_writeBytes(FLASH_ADDR_SCHEDULE_INFO + 2 + flash_offSet, tempBuffForSchedule, 255);
}

void schedule_WriteToEEPRomAll(uint8 scheduleType, uint8 circuitId) {
	uint8 i = 0;
	uint8 flash_offSet = 0;
	Schedule_Table_st * scheduleTable_p = NULL;
	if (scheduleType == DEFAULT)
		return;
	else if (scheduleType == SCHEDULE_1) {
		flash_offSet = 0;
		scheduleTable_p = &scheduleTable_1;
	} else if (scheduleType == SCHEDULE_2) {
		flash_offSet = 3;
		scheduleTable_p = &scheduleTable_2;
	}

	wdt_reset();
	flash_readBytes(FLASH_ADDR_SCHEDULE_INFO + (circuitId / 8) + flash_offSet, tempBuffForSchedule,
			255);

	for (i = 0; i < MaxNumOfScheduleInfo; i++) {
		tempBuffForSchedule[0 + 2 * i + (circuitId % 12) * 3] =
				scheduleTable_p->scheduleInfo[circuitId][i].ctrlMode;
		tempBuffForSchedule[1 + 2 * i + (circuitId % 12) * 3] =
				scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m;
	}
	flash_writeBytes(FLASH_ADDR_SCHEDULE_INFO + (circuitId / 8) + flash_offSet, tempBuffForSchedule,
			255);

	flash_readBytes(FLASH_ADDR_SCHEDULE_INFO + 2 + flash_offSet, tempBuffForSchedule, 255);
	tempBuffForSchedule[circuitId] = scheduleTable_p->size[circuitId];
	flash_writeBytes(FLASH_ADDR_SCHEDULE_INFO + 2 + flash_offSet, tempBuffForSchedule, 255);
}

void resetScheduleTimeTable(uint8 scheduleType, uint8 circuitId) {
	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == SCHEDULE_1)
		scheduleTable_p = &scheduleTable_1;
	else if (scheduleType == SCHEDULE_2)
		scheduleTable_p = &scheduleTable_2;

	scheduleTable_p->size[circuitId] = 0;
	schedule_WriteToEEPRom(scheduleType, circuitId, 0);
}
// End of File
