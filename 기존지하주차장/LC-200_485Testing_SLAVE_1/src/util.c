#include "util.h"

uint16 toUint16(uint8 hexString[4]) {
	uint16 result = 0;
	uint8 i = 0;

	for (i = 0; i < 4; i++) {
		if (hexString[i] >= '0' && hexString[i] <= '9')
			result |= (hexString[i] - '0') << ((3 - i) * 4);
		else if (hexString[i] >= 'A' && hexString[i] <= 'F')
			result |= (hexString[i] - 'A' + 10) << ((3 - i) * 4);
		else if (hexString[i] >= 'a' && hexString[i] <= 'f')
			result |= (hexString[i] - 'a' + 10) << ((3 - i) * 4);
	}
	return result;
}

uint8 toUint8(uint8 hexString[2]) {
	uint8 result = 0;
	uint8 i = 0;

	for (i = 0; i < 2; i++) {
		if (hexString[i] >= '0' && hexString[i] <= '9')
			result |= (hexString[i] - '0') << ((1 - i) * 4);
		else if (hexString[i] >= 'A' && hexString[i] <= 'F')
			result |= (hexString[i] - 'A' + 10) << ((1 - i) * 4);
		else if (hexString[i] >= 'a' && hexString[i] <= 'f')
			result |= (hexString[i] - 'a' + 10) << ((1 - i) * 4);
	}
	return result;
}

uint8 toHexCharacterOfLowerByte(uint8 ch) {
	uint8 result=0;

	if ((ch & 0x0f) >= 0 && (ch & 0x0f) <= 9)
		result = '0' + (ch & 0x0f);
	else if ((ch & 0x0f) >= 10 && (ch & 0x0f) <= 15)
		result = 'A' + ((ch & 0x0f)-10);

	return result;
}
