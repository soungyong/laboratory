#include "PLCS_Protocol.h"

#include "NCProtocol.h"
#include "SSRControl.h"
#include "debug.h"
#include "PatternBasedControl.h"
#include "MappingTable.h"
#include "ScheduleTable.h"
#include <avr/wdt.h>
#include "DeviceInfo.h"
#include "Timer.h"

// --------------------------------------------------------------------------- //
device_Info tmp_DevInfo_D;
device_Info *tmp_DevInfo_P;

SensorMappingInfo_st *SSRTable;
// --------------------------------------------------------------------------- //

// --------------------------------------------------------------------------- //
device_Info tmp_DevInfo_D;
device_Info *tmp_DevInfo_P;

uint8 dev_Count;
uint16 ctrl_Cmd;
uint16 device_Type;
uint32 z_ieee_Id;

uint8 ncp_NetState = NCP_NET_NOT_CONNECT;

uint8 g_NoticeMode = 1;

char token = 0;
// --------------------------------------------------------------------------- //

uint8 tmp_Buff[64];

void plcs_GCP_ProcessMessage(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint8 msgType;
	uint8 i = 0;

	msgType = msg[8];

	switch (msgType) { // Msg Type of NCP
	case PLCS_GCP_REQ_NUMOFDEVICE:
		plcs_GCP_HandleNumOfDeviceReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_DEVICEINFO:
		plcs_GCP_HandleDeviceInfoReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_STATEINFO:
		plcs_GCP_HandleStateInfoReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_CONTROL_REBOOT:
		plcs_GCP_HandleControlRebootReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_CONTROL_CIRCUIT:
		plcs_GCP_HandleControlCircuitReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_CONTROL_SENSOR:
		plcs_GCP_HandleControlSensorReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_POWERMETER:
		plcs_GCP_HandlePowermeterReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_UPDATEPOWERMETER_LC:
		plcs_GCP_HandleUpdatePowermeterLCReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_UPDATEPOWERMETER:
		plcs_GCP_HandleUpdatePowermeterReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_SENSORMAPPINGSIZE:
		plcs_GCP_HandleSensorMappingSizeReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_DIMMERMAPPINGSIZE:
		plcs_GCP_HandleDimmerMappingSizeReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_SCHEDULESIZE:
		plcs_GCP_HandleScheduleSizeReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_SENSORMAPPINGINFO:
		plcs_GCP_HandleSensorMappingInfoReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_DIMMERMAPPINGINFO:
		plcs_GCP_HandleDimmerMappingInfoReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_SCHEDULEINFO:
		plcs_GCP_HandleScheduleInfoReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_MAPPINGSENSOR:
		plcs_GCP_HandleMappingSensorReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_MAPPINGSENSORLIST:
		plcs_GCP_HandleMappingSensorListReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_RESETMAPPINGSENSOR:
		plcs_GCP_HandleResetSensorMappingReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_MAPPINGDIMMER:
		plcs_GCP_HandleMappingDimmerReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_MAPPINGDIMMERLIST:
		plcs_GCP_HandleMappingDimmerListReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_RESETMAPPINGDIMMER:
		plcs_GCP_HandleResetDimmerMappingReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_ADDSCHEDULE:
		plcs_GCP_HandleAddScheduleReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_ADDSCHEDULELIST:
		plcs_GCP_HandleAddScheduleListReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_RESETSCHEDULE:
		plcs_GCP_HandleResetScheduleReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_NOTICEEVENT_NEIGHBOR:
		plcs_GCP_HandleNoticeEventNeighbor(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_DEBUG_LOG_LC:
		plcs_GCP_HandleDebugLogLCReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_RESET_DEBUG_LOG_LC:
		plcs_GCP_HandleDebugLogLCReset(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_SETSENSINGLEVEL:
		sendMessage(ZIGBEE, msg[11] << 8 | msg[12], msg, length);
		break;
	case PLCS_GCP_REQ_CONTROL_NOTICEMODE:
		plcs_GCP_HandleControlNoticeModeReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_APPLYINGSCHEDULE:
		plcs_GCP_HandleApplyingScheduleReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;

	default:
		break;
	}

	//Response Message
	switch (msgType) { // Msg Type of NCP
	case PLCS_GCP_NOTICEEVENT: {
		msg[9] = zrmpInfo_0.zrm_Id >> 8;
		msg[10] = zrmpInfo_0.zrm_Id;
		if (msg[11] == 0x01) {
			plcs_GCP_HandleSensorEvent(0, seqNum, srcId, destId, msg, length);
		} else if (msg[11] == 0x10) {
			handleSwitchEvent(0, seqNum, srcId, destId, msg, length);
		}
		if (g_NoticeMode == 1)
			sendServerMessage(msg, length);

		for (i = 0; i < deviceInfoTable.size; i++) {
			if (deviceInfoTable.deviceInfo[i].deviceType == 0x2090 && deviceInfoTable.deviceInfo[i].connectionState == ONLINE)
				sendMessage(ZIGBEE, deviceInfoTable.deviceInfo[i].ieeeId, msg, length);
		}
	}
		break;
	case PLCS_GCP_RES_STATEINFO: {
		uint16 deviceType;
		uint16 deviceId;
		uint16 lux;
		device_Info * deviceP = NULL;

		deviceId = msg[11] << 8 | msg[12];
		deviceType = msg[13] << 8 | msg[14];
		lux = msg[18] << 8 | msg[19];

		deviceP = findNodeById(deviceId);
		if (deviceP != NULL && deviceP->deviceType == PLCS_ZSENSOR_TYPE)
			deviceP->luxAvr = deviceP->luxAvr * 0.8 + lux * 0.2;

		msg[9] = zrmpInfo_0.zrm_Id >> 8;
		msg[10] = zrmpInfo_0.zrm_Id;
		sendServerMessage(msg, length);
		{
			uint8 i = 0;
			for (i = 0; i < deviceInfoTable.size; i++)
				if (deviceInfoTable.deviceInfo[i].deviceType == 0x2090 && deviceInfoTable.deviceInfo[i].connectionState == ONLINE)
					sendToZigbee(ZIGBEE_0, deviceInfoTable.deviceInfo[i].netAddr, msg, length);
		}
	}
		break;
	case PLCS_GCP_RES_SETSENSINGLEVEL:
	case PLCS_GCP_RES_POWERMETER: {
		msg[9] = zrmpInfo_0.zrm_Id >> 8;
		msg[10] = zrmpInfo_0.zrm_Id;
		sendServerMessage(msg, length);
		{
			uint8 i = 0;
			for (i = 0; i < deviceInfoTable.size; i++)
				if (deviceInfoTable.deviceInfo[i].deviceType == 0x2090 && deviceInfoTable.deviceInfo[i].connectionState == ONLINE)
					sendToZigbee(ZIGBEE_0, deviceInfoTable.deviceInfo[i].netAddr, msg, length);
		}
	}
		break;
	case PLCS_GCP_NOTICE_SENSOR_VALUE: {
		msg[9] = zrmpInfo_0.zrm_Id >> 8;
		msg[10] = zrmpInfo_0.zrm_Id;
		sendServerMessage(msg, length);
	}
		break;
	}
}

void NCP_ProcessMessage(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint8 msgType;
	msgType = msg[8];

	switch (msgType) { // Msg Type of NCP
	case NCP_RES_PING:
		ncp_ProcessPingRes(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case NCP_RES_REGISTER:
		ncp_ProcessRegisterRes(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case NCP_SEND_TIME_INFO:
		npc_ProcessSendTimeInfo(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case NCP_REQ_REGISTER_BROADCAST:
		plcs_NCP_ProcessRegisterBroadcastReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case NCP_ASSIGN_TOKEN:
		plcs_NCP_ProcessAssignToken(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case NCP_RES_TOKEN_RELEASE:
		plcs_NCP_ProcessReleaseTokenRes(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	}

	//Request Message
	switch (msgType) { // Msg Type of NCP
	case NCP_REQ_PING:
		ncp_ProcessPingReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case NCP_REQ_REGISTER:
		ncp_ProcessRegisterReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	}
}

void ncp_ProcessRegisterReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 deviceType;
	uint8 deviceVersion;
	uint8 fwVersion;
	// Request Register

	//msg[8] == messageType
	deviceType = (uint16) ((msg[9] << 8) | (msg[10]));
	deviceVersion = (uint8) msg[11];
	fwVersion = (uint8) msg[12];

	updateDeviceTable(srcId, deviceType, deviceVersion, fwVersion);
	ncp_SendRegisterRes(srcNodeType, srcId, seqNum, srcId, deviceType, 0x0000);

	npc_SendRegisterNodeReq(GATEWAY, 0, srcId, deviceType);
}

void ncp_ProcessPingReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {

	device_Info *pDeviceInfo;
	//msg[8] = messageType
	pDeviceInfo = findNodeById(srcId);
	pDeviceInfo->cnt = 0;

	if (pDeviceInfo != NULL) {
		if (pDeviceInfo->deviceType == 0 || pDeviceInfo->fwVersion == 0)
			ncp_SendRegisterRes(srcNodeType, srcId, 0, srcId, device_Type, 1);
		else
			ncp_SendPingRes(srcNodeType, srcId, seqNum, srcId, device_Type, 0x00, 30);

	} else {
		ncp_SendRegisterRes(srcNodeType, srcId, 0, srcId, device_Type, 1);
	}
}

void ncp_ProcessRegisterRes(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 result;

	result = msg[9] << 8 | msg[10];
	if (result == 0) {
		if (ncp_NetState != NCP_NET_REGISTER)
			debug_UpdateConnection();

		ncp_NetState = NCP_NET_REGISTER;
		ncp_SendPingReq(srcNodeType);
		registerAllNodetoServer();

		timer_set(NCP_PING_TIMER_ID, (1000));
	} else
		ncp_SendRegisterReq(GATEWAY);
}

void ncp_ProcessPingRes(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint8 flag = 0;
	uint8 pingInterval;

	flag = msg[10];
	pingInterval = msg[11] << 8 | msg[12];

	if (pingInterval == 0)
		pingInterval = 0xffff;
	if (flag == 0x01) {
		ncp_SendRegisterReq(srcNodeType);
		registerAllNodetoServer();
	}

	timer_set(NCP_PING_TIMER_ID, (pingInterval * 1000));
}

void npc_ProcessSendTimeInfo(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint8 hour;
	uint8 min;
	hour = msg[9];
	min = msg[10];

	Hour = hour;
	Min = min;
}

void ncp_SendPingReq(uint8 dstNodeType) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_REQ_PING;
	payload[len++] = NCP_VERSION;
	payload[len++] = 0;

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len);

	sendMessage(dstNodeType, 0, tmp_Buff, resultLen);
}

void ncp_SendRegisterReq(uint8 dstNodeType) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_REQ_REGISTER;
	payload[len++] = (uint8) (PLCS_LC_100Z_TYPE >> 8);
	payload[len++] = (uint8) PLCS_LC_100Z_TYPE;
	payload[len++] = PLCS_LC_FW_VER;
	payload[len++] = PLCS_LC_FW_VER;

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len);
	sendMessage(dstNodeType, 0, tmp_Buff, resultLen);
}

void ncp_SendRegisterReqWithoutQueue(uint16 nodeId) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_REQ_REGISTER;
	payload[len++] = (uint8) (PLCS_LC_100Z_TYPE >> 8);
	payload[len++] = (uint8) PLCS_LC_100Z_TYPE;
	payload[len++] = PLCS_LC_FW_VER;
	payload[len++] = PLCS_LC_FW_VER;

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, nodeId, payload, len);

	sendServerMessageWithoutQueue(tmp_Buff, resultLen);
}

/////////////////////////////////////////////////
void ncp_SendPingRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint16 device_Type, uint8 flag, uint8 timeOut) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_RES_PING;
	payload[len++] = NCP_VERSION;
	payload[len++] = (uint8) (flag);
	payload[len++] = (uint8) (timeOut >> 8);
	payload[len++] = (uint8) (timeOut);

	resultLen = plcs_GetNCPResMessage(tmp_Buff, nodeId, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void ncp_SendRegisterRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 node_Id, uint16 deviceType, uint16 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_RES_REGISTER;
	payload[len++] = (uint8) (result >> 8);
	payload[len++] = (uint8) (result);
	payload[len++] = 0x00;
	payload[len++] = 0x1e;

	resultLen = plcs_GetNCPResMessage(tmp_Buff, dstNodeId, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void npc_SendRegisterNodeReq(uint8 dstNodeType, uint16 dstNodeId, uint16 node_Id, uint16 device_Type) {
	int len = 0;
	uint8 payload[20];
	uint8 resultLen = 0;

	payload[len++] = NCP_REQ_REGISTER_NODE;
	payload[len++] = zrmpInfo_0.zrm_Id >> 8;
	payload[len++] = zrmpInfo_0.zrm_Id;
	payload[len++] = (uint8) (device_Type >> 8);
	payload[len++] = (uint8) (device_Type);

	switch (device_Type) {
	case PLCS_LC_100Z_TYPE:
		payload[len++] = (uint8) (PLCS_LC_FW_VER);
		payload[len++] = (uint8) (PLCS_LC_FW_VER);
		break;
	case PLCS_ZDIMMER_TYPE:
	case PLCS_ZSENSOR_TYPE:
	case PLCS_ZMDF_TYPE:
	case PLCS_SSG_TYPE:
	case PLCS_MPR_TYPE:
		tmp_DevInfo_P = findNodeById(node_Id);
		if (tmp_DevInfo_P == NULL)
			return;
		if (tmp_DevInfo_P->deviceType != device_Type)
			return;
		payload[len++] = tmp_DevInfo_P->ieeeId >> 8;
		payload[len++] = tmp_DevInfo_P->ieeeId;
		payload[len++] = tmp_DevInfo_P->deviceVersion;
		payload[len++] = tmp_DevInfo_P->fwVersion;
		payload[len++] = tmp_DevInfo_P->ieeeId >> 8;
		payload[len++] = tmp_DevInfo_P->ieeeId;

		break;
	default:
		return;
	}

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len);
	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);

}

void registerAllNodetoServer() {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		npc_SendRegisterNodeReq(0, deviceInfoTable.deviceInfo[i].ieeeId, deviceInfoTable.deviceInfo[i].deviceType, deviceInfoTable.deviceInfo[i].ieeeId);

}
uint8 ncp_ConnState() {
	return ncp_NetState;
}

char hasToken() {
	if (token == 0)
		return 0;

	return 1;
}

void plcs_NCP_ProcessRegisterBroadcastReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	char numOfNode;
	char flag;
	uint16 nodeId;
	char isRegistered = 0;
	char i;

	//msg[9] == msgType
	//msg[9, 10] = nodeId

	flag = msg[11];
	numOfNode = msg[12];
	for (i = 0; i < numOfNode; i++) {
		nodeId = msg[13 + i * 2] << 8 | msg[14 + i * 2];
		if (nodeId == zrmpInfo_0.zrm_Id)
			isRegistered = 1;
	}

	if (isRegistered == 0) {
		switch (flag) {
		case 0:
			MSLEEP(zrmpInfo_0.zrm_Id % 17 * 70);
			break;
		case 1:
			MSLEEP(zrmpInfo_0.zrm_Id % 13 * 70);
			break;
		case 2:
			MSLEEP(zrmpInfo_0.zrm_Id % 11 * 70);
			break;
		case 3:
			MSLEEP(zrmpInfo_0.zrm_Id % 7 * 70);
			break;
		case 4:
			MSLEEP(zrmpInfo_0.zrm_Id % 5 * 70);
			break;
		case 5:
			MSLEEP(zrmpInfo_0.zrm_Id % 3 * 70);
			break;
		}

		ncp_SendRegisterReqWithoutQueue(zrmpInfo_0.zrm_Id);
	}
}

void plcs_NCP_ProcessAssignToken(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	token = 1;
}

void plcs_NCP_ProcessReleaseTokenRes(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	token = 0;
}

void plcs_NCP_Send_NCPReqReleaseToken(uint8 dstNodeType, uint16 dstNodeId) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_REQ_TOKEN_RELEASE;
	payload[len++] = zrmpInfo_0.zrm_Id >> 8;
	payload[len++] = zrmpInfo_0.zrm_Id;

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_HandleNumOfDeviceReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 size = 0;
	uint8 i = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].deviceType != 0x2090)
			size++;

	plcs_GCP_SendNumOfDeviceRes(srcNodeType, srcId, seqNum, nodeId, size);
}

void plcs_GCP_HandleDeviceInfoReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 index;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	index = msg[11];

	plcs_GCP_SendDeviceInfoRes(srcNodeType, srcId, seqNum, nodeId, index);
}

void plcs_GCP_HandleStateInfoReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 deviceId;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	deviceId = msg[11] << 8 | msg[12];

	if (deviceId == 0) {
		plcs_GCP_SendStateInfoRes(srcNodeType, srcId, seqNum, nodeId, deviceId);
	} else { //deviceId!=0 서브 노드에게 전달.
		sendMessage(ZIGBEE, deviceId, msg, length);
		sendMessage(MPR, deviceId, msg, length);
	}
}

void plcs_GCP_HandleControlRebootReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 deviceId;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	deviceId = msg[11] << 8 | msg[12];

	if (deviceId == 0) {
		while (1)
			;
	} else { //deviceId!=0 서브 노드에게 전달.
		sendMessage(ZIGBEE, deviceId, msg, length);
		sendMessage(MPR, deviceId, msg, length);
	}
	//deviceId!=0 서브 노드에게 전달.

}

void plcs_GCP_HandleControlCircuitReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	int circuitId;
	uint8 ctrlMode;

	//msg[8] = message type
	nodeId = msg[9] << 8 | msg[10];
	circuitId = msg[11];
	ctrlMode = msg[12];

	handleDeviceControl(nodeId, circuitId, ctrlMode);

	if (circuitId <= 15)
		plcs_GCP_SendControlCircuitRes(srcNodeType, srcId, seqNum, nodeId, 0x00);
	else
		plcs_GCP_SendControlCircuitRes(srcNodeType, srcId, seqNum, nodeId, 0x10);
}

void plcs_GCP_HandleControlNoticeModeReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 enable;

	//msg[8] = message type
	nodeId = msg[9] << 8 | msg[10];
	enable = msg[11];

	g_NoticeMode = enable;
	timer_set(GCP_NOTICEMDOE_ID, 600000);
}

void plcs_GCP_HandleControlSensorReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	int circuitId;
	uint8 onTime;
	uint8 onCtrlMode;
	uint8 offCtrlMode;

	//msg[8] = message type
	nodeId = msg[9] << 8 | msg[10];
	circuitId = msg[11];
	onTime = msg[12];
	onCtrlMode = msg[13];
	offCtrlMode = msg[14];

	if (circuitId <= 15) {
		updateSensorMappingTableByCircuit(circuitId, onTime, onCtrlMode, offCtrlMode);
		plcs_GCP_SendControlSensorRes(srcNodeType, srcId, seqNum, nodeId, 0x00);

	} else
		plcs_GCP_SendControlSensorRes(srcNodeType, srcId, seqNum, nodeId, 0x10);
}

void plcs_GCP_HandlePowermeterReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 mdfId;
	uint16 nodeId;
	nodeId = msg[9] << 8 | msg[10];
	mdfId = msg[11] << 8 | msg[12];

	if (mdfId != 0) {
		sendMessage(ZIGBEE, mdfId, msg, length);
	} else {
		plcs_GCP_SendPowermeterRes(srcNodeType, mdfId, seqNum, nodeId, g_PowerMeterValue);
	}
}

void plcs_GCP_HandleUpdatePowermeterLCReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	device_Info* deviceInfo_P;
	uint16 mdfId;
	uint16 nodeId;
	uint8 mdfChannel;
	uint32 powerMeterValue;

	nodeId = msg[9] << 8 | msg[10];
	mdfId = msg[11] << 8 | msg[12];
	mdfChannel = msg[13];
	if (mdfId != 0) {
	} else {
		powerMeterValue = ((uint32) msg[13]) << 24 | ((uint32) msg[14]) << 16 | ((uint32) msg[15]) << 8 | ((uint32) msg[16]);
		// Write to EEPROM
		eeprom_write_dword((uint32_t*) POWER_METER_VALUE, powerMeterValue);
		g_PowerMeterValue = powerMeterValue;
	}
}

void plcs_GCP_HandleUpdatePowermeterReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 mdfId;
	uint16 nodeId;
	uint8 mdfChannel;
	uint32 powerMeterValue;

	nodeId = msg[9] << 8 | msg[10];
	mdfId = msg[11] << 8 | msg[12];
	mdfChannel = msg[13];
	if (mdfId != 0) {
		//deviceInfo_P = findNodeById(mdfId);
		//if (deviceInfo_P != NULL)
		sendMessage(ZIGBEE, mdfId, msg, length);
	} else {
		powerMeterValue = ((uint32) msg[14]) << 24 | ((uint32) msg[15]) << 16 | ((uint32) msg[16]) << 8 | ((uint32) msg[17]);
		// Write to EEPROM
		eeprom_write_dword((uint32_t*) POWER_METER_VALUE, powerMeterValue);
		g_PowerMeterValue = powerMeterValue;
	}
}

void plcs_GCP_HandleSensorMappingSizeReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];

	plcs_GCP_SendSensorMappingSizeRes(srcNodeType, srcId, seqNum, nodeId, getSensorMappingTableSize());
}

void plcs_GCP_HandleDimmerMappingSizeReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];

	plcs_GCP_SendDimmerMappingSizeRes(srcNodeType, srcId, seqNum, nodeId, getDimmerMappingTableSize());
}

void plcs_GCP_HandleScheduleSizeReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 scheduleType;
	uint8 resultSize = 0;
	uint8 i = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	scheduleType = msg[11];

	if (scheduleType == DEFAULT)
		for (i = 0; i < MaxNumOfCircuit; i++)
			resultSize += defaultScheduleTable.size[i];
	else if (scheduleType == SCHEDULE_1)
		for (i = 0; i < MaxNumOfCircuit; i++)
			resultSize += scheduleTable_1.size[i];
	else if (scheduleType == SCHEDULE_2)
		for (i = 0; i < MaxNumOfCircuit; i++)
			resultSize += scheduleTable_2.size[i];

	plcs_GCP_SendScheduleSizeRes(srcNodeType, srcId, seqNum, nodeId, scheduleType, resultSize);
}

void plcs_GCP_HandleSensorMappingInfoReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 index;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	index = msg[11];

	plcs_GCP_SendSensorMappingInfoRes(srcNodeType, srcId, seqNum, nodeId, index);
}

void plcs_GCP_HandleDimmerMappingInfoReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 index;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	index = msg[11];

	plcs_GCP_SendDimmerMappingInfoRes(srcNodeType, srcId, seqNum, nodeId, index);
}

void plcs_GCP_HandleScheduleInfoReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 scheduleType;
	uint8 index;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	scheduleType = msg[11];
	index = msg[12];

	plcs_GCP_SendScheduleInfoRes(srcNodeType, srcId, seqNum, nodeId, scheduleType, index);
}

void plcs_GCP_HandleMappingSensorReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 lcId;
	uint16 sensorId;
	uint8 circuitId;
	uint8 onTime;
	uint8 onCtrlMode;
	uint8 offCtrlMode;
	uint8 result = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	lcId = msg[11] << 8 | msg[12];
	sensorId = msg[13] << 8 | msg[14];
	circuitId = msg[15];
	onTime = msg[16];
	onCtrlMode = msg[17];
	offCtrlMode = msg[18];

	if (circuitId > 15)
		plcs_GCP_SendMappingSensorRes(srcNodeType, srcId, seqNum, nodeId, 0x11);

	result = updateSensorMappingTable(lcId, sensorId, circuitId, onTime, onCtrlMode, offCtrlMode);
	if (result == 0)
		plcs_GCP_SendMappingSensorRes(srcNodeType, srcId, seqNum, nodeId, 0x10);
	else if (result == 1) //add success
		plcs_GCP_SendMappingSensorRes(srcNodeType, srcId, seqNum, nodeId, 0x00);
	else if (result == 2) //update success
		plcs_GCP_SendMappingSensorRes(srcNodeType, srcId, seqNum, nodeId, 0x01);
}

void plcs_GCP_HandleMappingSensorListReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 lcId;
	uint8 numOfSensors;
	uint16 sensorId[20];
	uint8 circuitId;
	uint8 onTime;
	uint8 onCtrlMode;
	uint8 offCtrlMode;
	uint8 result = 0;
	uint8 i = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	lcId = msg[11] << 8 | msg[12];
	numOfSensors = msg[13];
	for (i = 0; i < numOfSensors; i++)
		sensorId[i] = msg[14 + i * 2] << 8 | msg[15 + i * 2];
	circuitId = msg[14 + numOfSensors * 2];
	onTime = msg[15 + numOfSensors * 2];
	onCtrlMode = msg[16 + numOfSensors * 2];
	offCtrlMode = msg[17 + numOfSensors * 2];

	if (circuitId > 15)
		plcs_GCP_SendMappingSensorListRes(srcNodeType, srcId, seqNum, nodeId, 0x11);

	for (i = 0; i < numOfSensors; i++)
		if (findSensorMappingInfoById(lcId, sensorId[i], circuitId) != -1)
			result = 1;

	for (int i = 0; i < numOfSensors; i++) {
		updateSensorMappingTable(lcId, sensorId[i], circuitId, onTime, onCtrlMode, offCtrlMode);
		wdt_reset();
	}

	if (result == 1)
		plcs_GCP_SendMappingSensorListRes(srcNodeType, srcId, seqNum, nodeId, 0x01);
	else
		plcs_GCP_SendMappingSensorListRes(srcNodeType, srcId, seqNum, nodeId, 0x00);
}

void plcs_GCP_HandleResetSensorMappingReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 circuitId;
	uint16 i = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	circuitId = msg[11];

	plcs_GCP_SendResetSensorMappingRes(srcNodeType, srcId, seqNum, nodeId, 0x00);

	for (i = 0; i < MaxNumOfSensorMappingInfo; i++) {
		if (sensorMappingTable.mappingInfo[i].isEnable == 1)
			if (sensorMappingTable.mappingInfo[i].circuitId == circuitId || circuitId == 0xff) {
				sensorMappingTable.mappingInfo[i].isEnable = 0;
				//sensorMappingInfo_WriteToEEPRom(i);

				//wdt_reset();
			}
	}
	sensorMappingInfo_WriteAll();
}

void plcs_GCP_HandleMappingDimmerReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 dimmerId;
	uint8 channelId;
	uint8 circuitId;

	uint8 result = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	dimmerId = msg[11] << 8 | msg[12];
	channelId = msg[13];
	circuitId = msg[14];

	if (circuitId > 15)
		plcs_GCP_SendMappingDimmerRes(srcNodeType, srcId, seqNum, nodeId, 0x11);

	result = updateDimmerMappingTable(dimmerId, channelId, circuitId);

	if (result == 0) //이미 매핑된 디머.
		plcs_GCP_SendMappingDimmerRes(srcNodeType, srcId, seqNum, nodeId, 0x31);
	else if (result == 2) //이미 매핑된 디머.
		plcs_GCP_SendMappingDimmerRes(srcNodeType, srcId, seqNum, nodeId, 0x32);
	else
		plcs_GCP_SendMappingDimmerRes(srcNodeType, srcId, seqNum, nodeId, 0x00);

	updateDimmerControlInfoList();
}

void plcs_GCP_HandleMappingDimmerListReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 numOfDimmer;
	uint16 dimmerId[20];
	uint8 channelId[20];
	uint8 circuitId;
	uint8 i = 0;
	uint8 result = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	numOfDimmer = msg[11];
	for (i = 0; i < numOfDimmer; i++) {
		dimmerId[i] = msg[12 + i * 3] << 8 | msg[13 + i * 3];
		channelId[i] = msg[14 + i * 3];
	}
	circuitId = msg[12 + i * 3];

	if (circuitId > 15)
		plcs_GCP_SendMappingDimmerListRes(srcNodeType, srcId, seqNum, nodeId, 0x11);

	for (i = 0; i < numOfDimmer; i++)
		if (findDimmerMappingInfoById(dimmerId[i], channelId[i]) != -1)
			result = 1;

	if (result == 0)
		for (i = 0; i < numOfDimmer; i++) {
			updateDimmerMappingTable(dimmerId[i], channelId[i], circuitId);
			wdt_reset();
		}

	if (result == 1) //이미 매핑된 디머.
		plcs_GCP_SendMappingDimmerListRes(srcNodeType, srcId, seqNum, nodeId, 0x31);
	else
		plcs_GCP_SendMappingDimmerListRes(srcNodeType, srcId, seqNum, nodeId, 0x00);

	updateDimmerControlInfoList();
}

void plcs_GCP_HandleResetDimmerMappingReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 circuitId;
	uint16 i = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	circuitId = msg[11];

	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++) {
		if (dimmerMappingTable.mappingInfo[i].isEnable == 1)
			if (dimmerMappingTable.mappingInfo[i].circuitId == circuitId || circuitId == 0xff) {
				dimmerMappingTable.mappingInfo[i].isEnable = 0;
				//dimmerMappingInfo_WriteToEEPRom(i);
				//wdt_reset();
			}
	}
	plcs_GCP_SendResetDimmerMappingRes(srcNodeType, srcId, seqNum, nodeId, 0x00);

	dimmerMappingInfo_WriteAll();
	updateDimmerControlInfoList();
}

void plcs_GCP_HandleAddScheduleReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 scheduleType;
	uint8 circuitId;
	uint8 hour;
	uint8 minute;
	uint8 ctrlMode;

	uint8 result = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	scheduleType = msg[11];
	circuitId = msg[12];
	hour = msg[13];
	minute = msg[14];
	ctrlMode = msg[15];

	if (circuitId > 15)
		plcs_GCP_SendAddScheduleRes(srcNodeType, srcId, seqNum, nodeId, 0x11);

	if (isContainScheduleAt(scheduleType, circuitId, hour, minute) == 1)
		result = 0x01;
	if (addSchedule(scheduleType, circuitId, hour, minute, ctrlMode) == 0)
		result = 0x10;
	else if (result != 0x01) {
		result = 0x00;
		schedule_WriteToEEPRomAll(scheduleType, circuitId);
	}
	plcs_GCP_SendAddScheduleRes(srcNodeType, srcId, seqNum, nodeId, result);
}

void plcs_GCP_HandleAddScheduleListReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 scheduleType;
	uint8 circuitId;
	uint8 hour[15];
	uint8 minute[15];
	uint8 ctrlMode[15];
	uint8 numOfSchedule = 0;
	uint8 i = 0;

	uint8 result = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	scheduleType = msg[11];
	circuitId = msg[12];
	numOfSchedule = msg[13];

	for (i = 0; i < numOfSchedule; i++) {
		hour[i] = msg[14 + i * 3];
		minute[i] = msg[15 + i * 3];
		ctrlMode[i] = msg[16 + i * 3];
	}

	if (circuitId > 15)
		plcs_GCP_SendAddScheduleListRes(srcNodeType, srcId, seqNum, nodeId, 0x11);

	for (i = 0; i < numOfSchedule; i++)
		if (isContainScheduleAt(scheduleType, circuitId, hour[i], minute[i]) == 1)
			result = 0x01;

	if (addScheduleList(scheduleType, circuitId, numOfSchedule, hour, minute, ctrlMode) == 0)
		result = 0x10;
	else if (result != 0x02)
		result = 0x00;

	plcs_GCP_SendAddScheduleListRes(srcNodeType, srcId, seqNum, nodeId, result);
}

void plcs_GCP_HandleResetScheduleReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 scheduleType;
	uint8 circuitId;
	uint8 i = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	scheduleType = msg[11];
	circuitId = msg[12];

	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == SCHEDULE_1)
		scheduleTable_p = &scheduleTable_1;
	else if (scheduleType == SCHEDULE_2)
		scheduleTable_p = &scheduleTable_2;
	else
		return;

	if (circuitId == 0xff) {
		for (i = 0; i < MaxNumOfCircuit; i++) {
			scheduleTable_p->size[i] = 0;
			schedule_WriteToEEPRom(scheduleType, circuitId, 0);
		}
	} else {
		scheduleTable_p->size[circuitId] = 0;
		schedule_WriteToEEPRom(scheduleType, circuitId, 0);
	}
	plcs_GCP_SendResetScheduleRes(srcNodeType, srcId, seqNum, nodeId, 0x00);
}

void plcs_GCP_HandleApplyingScheduleReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 scheduleType;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	scheduleType = msg[11];

	if (scheduleType != 0x00)
		applyingSchedule = scheduleType;

	plcs_GCP_SendApplyingScheduleRes(srcNodeType, srcId, seqNum, nodeId, applyingSchedule);
}

void plcs_GCP_HandleNoticeEventNeighbor(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 lcId;
	uint16 sensorId;

	//msg[8]=messagetype
	lcId = msg[11] << 8 | msg[12];
	sensorId = msg[13] << 8 | msg[14];

	handleSensorEvent(lcId, sensorId);
}

void plcs_GCP_HandleDebugLogLCReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];

	plcs_GCP_SendDebugLogLCRes(srcNodeType, srcId, seqNum, nodeId, debug_DebugInfo.rebootCnt, debug_DebugInfo.lastRebootTime, debug_DebugInfo.serverConnectionCnt,
			debug_DebugInfo.lastServerConnectionTime);
}

void plcs_GCP_HandleDebugLogLCReset(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	debug_Reset();
}

void plcs_GCP_HandleSensorEvent(uint8 srcNodeType, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 sensorId;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	sensorId = msg[12] << 8 | msg[13];

	handleSensorEvent(nodeId, sensorId);
}

void handleSensorEvent(uint16 lcId, uint16 sensorId) {
	uint16 i = 0;
	uint8 circuitId;
	for (i = 0; i < MaxNumOfSensorMappingInfo; i++) {
		if (sensorMappingTable.mappingInfo[i].sensorId == sensorId && sensorMappingTable.mappingInfo[i].lcId == lcId && sensorMappingTable.mappingInfo[i].isEnable == 1) {
			circuitId = sensorMappingTable.mappingInfo[i].circuitId;
			if (tmp_CircuitInfo.ctrlMode[circuitId] == SENSOR || tmp_CircuitInfo.ctrlMode[circuitId] == SCHEDULE) {
				if (tmp_CircuitInfo.expirationTime[circuitId] < sensorMappingTable.mappingInfo[i].onTime || tmp_CircuitInfo.expirationTime[circuitId] == 0xffff) {
					tmp_CircuitInfo.timerCnt[circuitId] = 0;
					tmp_CircuitInfo.expirationTime[circuitId] = sensorMappingTable.mappingInfo[i].onTime;
					tmp_CircuitInfo.offCtrlMode[circuitId] = sensorMappingTable.mappingInfo[i].offCtrlMode;
					tmp_CircuitInfo.onCtrlMode[circuitId] = sensorMappingTable.mappingInfo[i].onCtrlMode;
				} else {
					tmp_CircuitInfo.timerCnt[circuitId] = 0;
				}
			}
		}
	}
}

void handleSwitchEvent(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 switchId;
	uint8 buttonNum = 0;
	uint8 circuitId = 0;
	uint8 ctrlMode = 0;

//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	switchId = msg[12] << 8 | msg[13];
	buttonNum = msg[14];

	if (buttonNum < 3)
		circuitId = 0;
	else
		circuitId = 1;
	buttonNum %= 3;

	if (buttonNum == 0)
		ctrlMode = ON;
	else if (buttonNum == 1)
		ctrlMode = DIMMING_5;
	else if (buttonNum == 2)
		ctrlMode = OFF;
	else
		ctrlMode = OFF;

	handleDeviceControl(nodeId, circuitId, ctrlMode);

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
/////////////////////////////////////////////////////////////////////////////////////////////////////////
uint16 seqNumGenerator = 0;
uint16 getSeqNumGenerator() {
	return seqNumGenerator++;
}

uint8 plcs_GetNCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGenerator();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetNCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[], uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetGCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGenerator();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_GCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetGCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[], uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_GCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

/////////send gcp message
void plcs_GCP_SendNumOfDeviceRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 size) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_NUMOFDEVICE;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (size);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);
	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendDeviceInfoRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 index) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;
	uint16 deviceId;
	uint16 deviceType;
	uint8 firmVersion;

	if (deviceInfoTable.size < index && index != 200)
		return;
	else if (index == 200) {
		nodeId = 0;
		deviceId = nodeId;
		deviceType = PLCS_LC_100Z_TYPE;
		firmVersion = (uint8) PLCS_LC_FW_VER;
	} else {
		device_Info* deviceInfo_P = NULL;
		uint8 i = 0;
		uint8 cnt = 0;
		for (i = 0; i < deviceInfoTable.size; i++) {
			if (deviceInfoTable.deviceInfo[i].deviceType != 0x2090)
				cnt++;

			deviceInfo_P = &(deviceInfoTable.deviceInfo[i]);
			if ((cnt - 1) == index)
				break;

		}
		nodeId = zrmpInfo_0.zrm_Id;
		deviceId = deviceInfo_P->ieeeId;
		deviceType = deviceInfo_P->deviceType;
		firmVersion = deviceInfo_P->fwVersion;
	}

	if (deviceType == 0)
		return;

	payload[len++] = PLCS_GCP_RES_DEVICEINFO;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (deviceId >> 8);
	payload[len++] = (uint8) deviceId;
	payload[len++] = (uint8) (deviceType >> 8);
	payload[len++] = (uint8) deviceType;
	payload[len++] = (uint8) firmVersion;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendStateInfoRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint16 deviceId) {
	uint8 len = 0;
	uint8 payload[60];
	uint8 resultLen = 0;
	uint16 deviceType = PLCS_LC_100Z_TYPE;
	uint8 i;

	payload[len++] = PLCS_GCP_RES_STATEINFO;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (deviceId >> 8);
	payload[len++] = (uint8) deviceId;
	payload[len++] = (uint8) (deviceType >> 8);
	payload[len++] = (uint8) deviceType;

	for (i = 0; i < 16; i++)
		payload[len++] = tmp_CircuitInfo.state[i];
	for (i = 0; i < 16; i++)
		payload[len++] = tmp_CircuitInfo.ctrlMode[i];

	payload[len++] = (uint8) (zrmpInfo_0.zrm_Panid >> 8);
	payload[len++] = (uint8) zrmpInfo_0.zrm_Panid;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, deviceId, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendControlCircuitRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_CONTROL_CIRCUIT;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) result;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendControlSensorRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_CONTROL_SENSOR;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) result;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendPowermeterRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint32 powermeterValue) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_POWERMETER;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) 0;
	payload[len++] = (uint8) 0;
	payload[len++] = (uint8) 0;
	payload[len++] = (uint8) (powermeterValue >> 24);
	payload[len++] = (uint8) (powermeterValue >> 16);
	payload[len++] = (uint8) (powermeterValue >> 8);
	payload[len++] = (uint8) (powermeterValue);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendSensorMappingSizeRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 size) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_SENSORMAPPINGSIZE;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (size);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendDimmerMappingSizeRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 size) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_DIMMERMAPPINGSIZE;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (size);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendScheduleSizeRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 scheduleType, uint8 size) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_SCHEDULESIZE;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (scheduleType);
	payload[len++] = (uint8) (size);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendSensorMappingInfoRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 index) {
	int len = 0;
	uint8 payload[20];
	uint8 resultLen = 0;
	SensorMappingInfo_st *mappingInfo;

	if (index >= getSensorMappingTableSize())
		return;

	mappingInfo = getSensorMappingInfoByIndex(index);

	payload[len++] = PLCS_GCP_RES_SENSORMAPPINGINFO;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (mappingInfo->lcId >> 8);
	payload[len++] = (uint8) (mappingInfo->lcId);
	payload[len++] = (uint8) (mappingInfo->sensorId >> 8);
	payload[len++] = (uint8) (mappingInfo->sensorId);
	payload[len++] = (uint8) (mappingInfo->circuitId);
	payload[len++] = (uint8) (mappingInfo->onTime);
	payload[len++] = (uint8) (mappingInfo->onCtrlMode);
	payload[len++] = (uint8) (mappingInfo->offCtrlMode);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendDimmerMappingInfoRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 index) {
	int len = 0;
	uint8 payload[20];
	uint8 resultLen = 0;
	DimmerMappingInfo_st *dimmerMappingInfo_p = NULL;

	if (index >= getDimmerMappingTableSize())
		return;

	dimmerMappingInfo_p = getDimmerMappingInfoByIndex(index);

	payload[len++] = PLCS_GCP_RES_DIMMERMAPPINGINFO;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (dimmerMappingInfo_p->dimmerId >> 8);
	payload[len++] = (uint8) (dimmerMappingInfo_p->dimmerId);
	payload[len++] = (uint8) (dimmerMappingInfo_p->channelId);
	payload[len++] = (uint8) (dimmerMappingInfo_p->circuitId);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendScheduleInfoRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 scheduleType, uint8 index) {
	int len = 0;
	uint8 payload[20];
	uint8 resultLen = 0;
	uint8 totalSizeOfSchedule = 0;
	uint8 i = 0;
	uint8 circuitId = 0;
	uint8 indexAtCircuitId = 0;

	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == SCHEDULE_1)
		scheduleTable_p = &scheduleTable_1;
	else if (scheduleType == SCHEDULE_2)
		scheduleTable_p = &scheduleTable_2;
	else
		return;

	for (i = 0; i < MaxNumOfCircuit; i++) {
		totalSizeOfSchedule += scheduleTable_p->size[i];
	}

	if (index >= totalSizeOfSchedule)
		return;

	totalSizeOfSchedule = 0;
	for (i = 0; i < MaxNumOfCircuit; i++) {
		totalSizeOfSchedule += scheduleTable_p->size[i];
		if (totalSizeOfSchedule > index) {
			circuitId = i;
			indexAtCircuitId = totalSizeOfSchedule - index - 1;
			break;
		}
	}

	payload[len++] = PLCS_GCP_RES_SCHEDULEINFO;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = scheduleType;
	payload[len++] = circuitId;
	payload[len++] = (uint8) (scheduleTable_p->scheduleInfo[circuitId][indexAtCircuitId].onTime_10m / 6);
	payload[len++] = (uint8) ((scheduleTable_p->scheduleInfo[circuitId][indexAtCircuitId].onTime_10m % 6) * 10);
	payload[len++] = (uint8) (scheduleTable_p->scheduleInfo[circuitId][indexAtCircuitId].ctrlMode);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendMappingSensorRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_MAPPINGSENSOR;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendMappingSensorListRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_MAPPINGSENSORLIST;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendResetSensorMappingRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_RESETMAPPINGSENSOR;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendMappingDimmerRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_MAPPINGDIMMER;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendMappingDimmerListRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_MAPPINGDIMMERLIST;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendResetDimmerMappingRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_RESETMAPPINGDIMMER;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendResetScheduleRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_RESETSCHEDULE;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendApplyingScheduleRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_APPLYINGSCHEDULE;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendAddScheduleRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_ADDSCHEDULE;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendAddScheduleListRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_ADDSCHEDULELIST;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (result);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}
void plcs_GCP_SendNoticeEvent(uint8 dstNodeType, uint16 dstNodeId, uint16 nodeId, uint8 dataFormat, uint16 sensorId) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_NOTICEEVENT;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (dataFormat);
	payload[len++] = (uint8) (sensorId >> 8);
	payload[len++] = (uint8) sensorId;

	resultLen = plcs_GetGCPMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len);
	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendDebugLogLCRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum, uint16 nodeId, uint16 rebootCnt, uint8 lastRebootTime[], uint16 serverConnectionCnt,
		uint8 lastServerConnectionTime[]) {
	int len = 0;
	uint8 i = 0;
	uint8 payload[40];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_DEBUG_LOG_LC;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (rebootCnt >> 8);
	payload[len++] = (uint8) rebootCnt;

	for (i = 0; i < 2; i++)
		payload[len++] = lastRebootTime[i];

	payload[len++] = (uint8) (serverConnectionCnt >> 8);
	payload[len++] = (uint8) (serverConnectionCnt);
	for (i = 0; i < 2; i++)
		payload[len++] = lastServerConnectionTime[i];

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo_0.zrm_Id, payload, len, seqNum);
	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

uint8 gcp_GetNoticeMode() {
	return g_NoticeMode;
}
void gcp_SetNoticeMode(uint8 enable) {
	g_NoticeMode = enable;
}
