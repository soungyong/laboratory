#include "PatternBasedControl.h"
#include "PLCS_Protocol.h"
#include <avr/wdt.h>
#include "Flash.h"

uint8 ee_PatternInfoTableSize[MaxNumOfCircuit] EEMEM;
Pattern_Info ee_PatternInfo[MaxNumOfCircuit][MaxNumOfPatternInfo] EEMEM;
uint8 tempBuffForPatternInfo[255];
void initPatternTable() {
	uint8 i = 0;
	uint16 j = 0;
	uint8 k = 0;

	//Page당 12개 Circuit의 pattern 정보 기록. 총 240바이트.
	for (i = 0; i < MaxNumOfCircuit; i += 12) {
		wdt_reset();
		flash_readBytes(FLASH_ADDR_PATTERN_INFO + i / 12, tempBuffForPatternInfo, 255);
		for (j = 0; j < 12; j++) {
			for (k = 0; k < MaxNumOfPatternInfo; k++) {
				patternTable.patternInfo[i * 12 + j][k].ctrlMode = tempBuffForPatternInfo[0 + 2 * k
						+ j * 20];
				patternTable.patternInfo[i * 12 + j][k].startTime = tempBuffForPatternInfo[1 + 2 * k
						+ j * 20];
			}
		}
	}
	flash_readBytes(FLASH_ADDR_PATTERN_INFO + 2, tempBuffForPatternInfo, 255);
	for (i = 0; i < MaxNumOfCircuit; i++) {
		patternTable.size[i] = tempBuffForPatternInfo[i];
		if (patternTable.size[i] >= MaxNumOfPatternInfo)
			patternTable.size[i] = 0;
	}
	/*
	 for (i = 0; i < MaxNumOfCircuit; i++) {
	 patternTable.size[i] = 0;
	 eeprom_read_block(&(patternTable.size[i]), &ee_PatternInfoTableSize[i], sizeof(uint8));
	 if (patternTable.size[i] >= MaxNumOfPatternInfo)
	 patternTable.size[i] = 0;

	 for (j = 0; j < patternTable.size[i]; j++)
	 eeprom_read_block(&(patternTable.patternInfo[i][j]), &(ee_PatternInfo[i][j]),
	 sizeof(Pattern_Info));
	 }
	 */
}

int addPatternInfoToTable(uint8 circuitId, uint8 startTime, uint8 ctrlMode) {
	if (circuitId >= MaxNumOfCircuit)
		return 0;
	if (patternTable.size[circuitId] >= MaxNumOfPatternInfo)
		return 0;

	patternTable.patternInfo[circuitId][patternTable.size[circuitId]].startTime = startTime;
	patternTable.patternInfo[circuitId][patternTable.size[circuitId]].ctrlMode = ctrlMode;
	patternTable.size[circuitId]++;

	patternTable_WriteToEEPRom(circuitId, patternTable.size[circuitId] - 1);
	return 1;
}

uint8 getCtrlModeOnTime(uint8 circuitId, uint8 currentTime) {
	uint8 i = 0;
	uint8 ctrlMode = ON;
	uint8 startTime = 0;
	uint8 flag = 0;

	if (circuitId >= MaxNumOfCircuit)
		ctrlMode = ON;
	if (patternTable.size[circuitId] == 0)
		ctrlMode = ON;
	else {
		//현재 시각 보다 앞쪽 패턴 제어 처리
		for (i = 0; i < patternTable.size[circuitId]; i++) {
			if (patternTable.patternInfo[circuitId][i].startTime <= currentTime
					&& patternTable.patternInfo[circuitId][i].startTime >= startTime) {
				startTime = patternTable.patternInfo[circuitId][i].startTime;
				ctrlMode = patternTable.patternInfo[circuitId][i].ctrlMode;
				flag = 1;
			}
		}
		//현재 시각 보다 앞쪽 패턴이 없을 경우 다음 마지막 패턴 처리.(하루는 원형으로 반복되니까.)
		if (flag == 0) {
			startTime = patternTable.patternInfo[circuitId][0].startTime;
			ctrlMode = patternTable.patternInfo[circuitId][0].ctrlMode;
			for (i = 1; i < patternTable.size[circuitId]; i++) {
				if (patternTable.patternInfo[circuitId][i].startTime >= currentTime
						&& patternTable.patternInfo[circuitId][i].startTime >= startTime) {
					startTime = patternTable.patternInfo[circuitId][i].startTime;
					ctrlMode = patternTable.patternInfo[circuitId][i].ctrlMode;
				}
			}
		}
	}

	return ctrlMode;
}

void resetPatternTable(uint8 circuitId) {
	if (circuitId >= MaxNumOfCircuit)
		return;

	patternTable.size[circuitId] = 0;

	patternTable_WriteToEEPRom(circuitId, 0);
}

void patternTable_WriteToEEPRom(uint8 circuitId, uint8 index) {
	//Page당 12개 Circuit의 pattern 정보 기록. 총 240바이트.
	wdt_reset();
	flash_readBytes(FLASH_ADDR_PATTERN_INFO + circuitId / 12, tempBuffForPatternInfo, 255);

	tempBuffForPatternInfo[0 + 2 * index + (circuitId % 12) * 20] =
			patternTable.patternInfo[circuitId][index].ctrlMode;
	tempBuffForPatternInfo[1 + 2 * index + (circuitId % 12) * 20] =
			patternTable.patternInfo[circuitId][index].startTime;

	flash_writeBytes(FLASH_ADDR_PATTERN_INFO + circuitId / 12, tempBuffForPatternInfo, 255);

	flash_readBytes(FLASH_ADDR_PATTERN_INFO + 2, tempBuffForPatternInfo, 255);

	if (patternTable.size[circuitId] >= MaxNumOfPatternInfo)
		patternTable.size[circuitId] = 0;
	tempBuffForPatternInfo[circuitId] = patternTable.size[circuitId];
	flash_writeBytes(FLASH_ADDR_PATTERN_INFO + 2, tempBuffForPatternInfo, 255);
}

