#include "Uart.h"

#define USART_BUFF_SIZE	128
//------------------------------------------------------//
uint8 uart0_rx_buff[USART_BUFF_SIZE];
uint8 uart0_rx_front = 0;
uint8 uart0_rx_tail = 0;
uint8 uart0_rx_len = 0;

uint8 uart0_tx_buff[1];
uint8 uart0_tx_front = 0;
uint8 uart0_tx_tail = 0;
uint8 uart0_tx_len = 0;
//------------------------------------------------------//

//------------------------------------------------------//
uint8 uart1_rx_buff[USART_BUFF_SIZE];
uint8 uart1_rx_front = 0;
uint8 uart1_rx_tail = 0;
uint8 uart1_rx_len = 0;

uint8 uart1_tx_buff[1];
uint8 uart1_tx_front = 0;
uint8 uart1_tx_tail = 0;
uint8 uart1_tx_len = 0;
//------------------------------------------------------//

//------------------------------------------------------//
uint8 uart2_rx_buff[USART_BUFF_SIZE];
uint8 uart2_rx_front = 0;
uint8 uart2_rx_tail = 0;
uint8 uart2_rx_len = 0;

uint8 uart2_tx_buff[1];
uint8 uart2_tx_front = 0;
uint8 uart2_tx_tail = 0;
uint8 uart2_tx_len = 0;
//------------------------------------------------------//

//------------------------------------------------------//
uint8 uart3_rx_buff[USART_BUFF_SIZE];
uint8 uart3_rx_front = 0;
uint8 uart3_rx_tail = 0;
uint8 uart3_rx_len = 0;

uint8 uart3_tx_buff[1];
uint8 uart3_tx_front = 0;
uint8 uart3_tx_tail = 0;
uint8 uart3_tx_len = 0;
//------------------------------------------------------//

#ifdef DEBUG_ENABLE
//***************************************************************************//
static int Putchar(char c, FILE *stream);

static FILE mystdout = FDEV_SETUP_STREAM(Putchar, NULL, _FDEV_SETUP_RW);

static int Putchar(char c, FILE *stream)
{
	if (c == '\n')
	Putchar('\r', stream);
	loop_until_bit_is_set(UCSR1A, UDRE1);
	UDR1 = c;
	return 0;
}
//***************************************************************************//
#endif

void InitUART(void) {
	UCSR0A = 0x02; // U2X = 1
	UCSR0B = 0x98;
	UCSR0C = 0x06;
	UBRR0H = 0x00;
	UBRR0L = 0x10; // 115200 bps

	UCSR3A = 0x02; // U2X = 1
	UCSR3B = 0x98;
	UCSR3C = 0x06;
	UBRR3H = 0x00;
	UBRR3L = 0x10; // 115200 bps

	//UCSR2A = 0x02; // U2X = 1
	UCSR2A = 0x00; // U2X = 0
	UCSR2B = 0x98;
	UCSR2C = 0x06;
	UBRR2H = 0x00;
	UBRR2L = 103; // 115200 bps

	UCSR1A = 0x02; // U2X = 1
	UCSR1B = 0x98;
	UCSR1C = 0x06;
	UBRR1H = 0x00;
	UBRR1L = 0x10; // 115200 bps, double speed

	//setting for usart0
	//full up rx, tx
	DDRE &= ~(0x01);
	DDRE |= 0x02;
	PORTE |= 0x01;

	DDRE &= ~(0x04); //Reset
	PORTE &= ~(0x04);

	DDRF &= ~(0x01); //Factory Reset
	PORTF &= ~(0x01);

	//setting for usart3
	//full up rx, tx
	DDRJ &= ~(0x01);
	DDRJ |= 0x02;
	PORTJ |= 0x01;

	DDRJ &= ~(0x04); //Reset
	PORTJ &= ~(0x04);

	DDRF &= ~(0x02); //Factory Reset
	PORTF &= ~(0x02);

	//setting for usart2
	DDRH |= 0x06;
	DDRH &= ~(0x01);
	PORTH |= 0x01;
	PORTH &= ~(0x06);

	USART2_TxDisable();

	//setting for usart1
	DDRD |= 0x10;

	DDRD &= ~(0x04);
	DDRD |= 0x08;
	PORTD |= 0x04;
	PORTD &= ~(0x08);

	USART1_TxDisable();
	//USART1_TxEnable();

#ifdef DEBUG_ENABLE
	stdout = &mystdout;
#endif
}

void USART1_TxEnable() {
	PORTD |= 0x10;
}
void USART1_TxDisable() {
	PORTD &= ~(0x10);
}

void USART2_TxEnable() {
	PORTH |= 0x04;
}
void USART2_TxDisable() {
	PORTH &= ~(0x04);
}

//--------------------- USART0 -------------------------//
//------------------------------------------------------//
uint8 status0;
uint8 data0;
ISR(USART0_RX_vect) {
	DISABLE_INTERRUPT();
	while (((status0 = UCSR0A) & (1 << RXC0)) == 0)
		;

	data0 = UDR0;

	uart0_rx_buff[uart0_rx_tail] = data0;
	uart0_rx_tail = (uart0_rx_tail + 1) % USART_BUFF_SIZE;
	uart0_rx_len++;
	ENABLE_INTERRUPT();
}

ISR(USART0_UDRE_vect) {
	uint8 buff;

	if (uart0_tx_len > 0) {
		buff = uart0_tx_buff[uart0_tx_front];

		UDR0 = buff;

		uart0_tx_front = (uart0_tx_front + 1) % USART_BUFF_SIZE;
		uart0_tx_len--;
	}

	UCSR0B &= ~0x20; // RXCIE, UDRIE, RXEN, TXEN
}

ISR(USART0_TX_vect) {
	uint8 buff;

	if (uart0_tx_len > 0) {
		buff = uart0_tx_buff[uart0_tx_front];
		//while ((UCSR1A & (1<<UDRE))==0);
		// tx isr.

		UDR0 = buff;

		uart0_tx_front = (uart0_tx_front + 1) % USART_BUFF_SIZE;
		uart0_tx_len--;
	}
}
//------------------------------------------------------//

//------------------------------------------------------//
int USART0_Receive(uint8 *buff) {

	DISABLE_INTERRUPT();

	if (uart0_rx_len > 0) {
		uart0_rx_len--;
		ENABLE_INTERRUPT();
		*buff = uart0_rx_buff[uart0_rx_front];
		uart0_rx_front = (uart0_rx_front + 1) % USART_BUFF_SIZE;

		return 1;
	}

	ENABLE_INTERRUPT();

	return 0;
}

int USART0_Transmit(uint8 buff) {
	while ((UCSR0A & (1 << UDRE0)) == 0)
		;
	// tx isr.

	UDR0 = buff;
	return 1;
}
//------------------------------------------------------//

//--------------------- USART1 --------------------------//
//------------------------------------------------------//
uint8 status1;
uint8 data1;
ISR(USART1_RX_vect) {
	DISABLE_INTERRUPT();
	while (((status1 = UCSR1A) & (1 << RXC1)) == 0)
		;

	data1 = UDR1;

	uart1_rx_buff[uart1_rx_tail] = data1;
	uart1_rx_tail = (uart1_rx_tail + 1) % USART_BUFF_SIZE;
	uart1_rx_len++;
	ENABLE_INTERRUPT();
}

ISR(USART1_UDRE_vect) {
	uint8 buff;

	if (uart1_tx_len > 0) {
		buff = uart1_tx_buff[uart1_tx_front];

		UDR1 = buff;

		uart1_tx_front = (uart1_tx_front + 1) % USART_BUFF_SIZE;
		uart1_tx_len--;
	}

	UCSR1B &= ~0x20; // RXCIE, UDRIE, RXEN, TXEN
}

ISR(USART1_TX_vect) {
	uint8 buff;

	if (uart1_tx_len > 0) {
		buff = uart1_tx_buff[uart1_tx_front];
		//while ((UCSR1A & (1<<UDRE))==0);
		// tx isr.
		UDR1 = buff;
		uart1_tx_front = (uart1_tx_front + 1) % USART_BUFF_SIZE;
		uart1_tx_len--;
	}
}
//------------------------------------------------------//

int USART1_Receive(uint8 *buff) {
	DISABLE_INTERRUPT();

	if (uart1_rx_len > 0) {
		uart1_rx_len--;
		ENABLE_INTERRUPT();
		*buff = uart1_rx_buff[uart1_rx_front];
		uart1_rx_front = (uart1_rx_front + 1) % USART_BUFF_SIZE;

		return 1;
	}

	ENABLE_INTERRUPT();

	return 0;
}

int USART1_Transmit(uint8 buff) {
	while ((UCSR1A & (1 << UDRE1)) == 0)
		;
	// tx isr.
	UDR1 = buff;
	return 1;
}

//------------------------------------------------------//

//--------------------- USART2 -------------------------//
//------------------------------------------------------//
uint8 status2;
uint8 data2;
ISR(USART2_RX_vect) {
	DISABLE_INTERRUPT();
	while (((status2 = UCSR2A) & (1 << RXC2)) == 0)
		;

	data2 = UDR2;

	uart2_rx_buff[uart2_rx_tail] = data2;
	uart2_rx_tail = (uart2_rx_tail + 1) % USART_BUFF_SIZE;
	uart2_rx_len++;
	ENABLE_INTERRUPT();
}

ISR(USART2_UDRE_vect) {
	uint8 buff;

	if (uart2_tx_len > 0) {
		buff = uart2_tx_buff[uart2_tx_front];

		UDR2 = buff;

		uart2_tx_front = (uart2_tx_front + 1) % USART_BUFF_SIZE;
		uart2_tx_len--;
	}

	UCSR2B &= ~0x20; // RXCIE, UDRIE, RXEN, TXEN
}

ISR(USART2_TX_vect) {
	uint8 buff;

	if (uart2_tx_len > 0) {
		buff = uart2_tx_buff[uart2_tx_front];
		//while ((UCSR1A & (1<<UDRE))==0);
		// tx isr.

		UDR2 = buff;

		uart2_tx_front = (uart2_tx_front + 1) % USART_BUFF_SIZE;
		uart2_tx_len--;
	}
}
//------------------------------------------------------//
int USART2_Receive(uint8 *buff) {

	DISABLE_INTERRUPT();

	if (uart2_rx_len > 0) {
		uart2_rx_len--;
		ENABLE_INTERRUPT();
		*buff = uart2_rx_buff[uart2_rx_front];
		uart2_rx_front = (uart2_rx_front + 1) % USART_BUFF_SIZE;

		return 1;
	}

	ENABLE_INTERRUPT();

	return 0;
}

int USART2_Transmit(uint8 buff) {
	while ((UCSR2A & (1 << UDRE2)) == 0)
		;
	// tx isr.

	UDR2 = buff;
	return 1;
}

//------------------------------------------------------//

//--------------------- USART3 -------------------------//
//------------------------------------------------------//
uint8 status3;
uint8 data3;
ISR(USART3_RX_vect) {
	DISABLE_INTERRUPT();
	while (((status3 = UCSR3A) & (1 << RXC3)) == 0)
		;

	data3 = UDR3;

	uart3_rx_buff[uart3_rx_tail] = data3;
	uart3_rx_tail = (uart3_rx_tail + 1) % USART_BUFF_SIZE;
	uart3_rx_len++;
	ENABLE_INTERRUPT();
}

ISR(USART3_UDRE_vect) {
	uint8 buff;

	if (uart3_tx_len > 0) {
		buff = uart3_tx_buff[uart3_tx_front];

		UDR3 = buff;

		uart3_tx_front = (uart3_tx_front + 1) % USART_BUFF_SIZE;
		uart3_tx_len--;
	}

	UCSR3B &= ~0x20; // RXCIE, UDRIE, RXEN, TXEN
}

ISR(USART3_TX_vect) {
	uint8 buff;

	if (uart3_tx_len > 0) {
		buff = uart3_tx_buff[uart3_tx_front];
		//while ((UCSR1A & (1<<UDRE))==0);
		// tx isr.

		UDR3 = buff;

		uart3_tx_front = (uart3_tx_front + 1) % USART_BUFF_SIZE;
		uart3_tx_len--;
	}
}
//------------------------------------------------------//
int USART3_Receive(uint8 *buff) {

	DISABLE_INTERRUPT();

	if (uart3_rx_len > 0) {
		uart3_rx_len--;
		ENABLE_INTERRUPT();
		*buff = uart3_rx_buff[uart3_rx_front];
		uart3_rx_front = (uart3_rx_front + 1) % USART_BUFF_SIZE;

		return 1;
	}

	ENABLE_INTERRUPT();

	return 0;
}

int USART3_Transmit(uint8 buff) {
	while ((UCSR3A & (1 << UDRE3)) == 0)
		;
	// tx isr.

	UDR3 = buff;
	return 1;
}

// End of file

