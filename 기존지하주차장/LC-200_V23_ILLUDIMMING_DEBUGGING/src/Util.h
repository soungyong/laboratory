#ifndef __UTIL_H__
#define __UTIL_H__

#include	<stdio.h>
#include	"util/delay.h"


//#define DEBUG_ENABLE

#define		MSLEEP(x)		_delay_ms(x)
#define		USLEEP(x)		_delay_us(x)


//------------------------------------------------------------------------//
//-------------------------- Timer ID -------------------------------------//
//------------------------------------------------------------------------//
#define NCP_PING_TIMER_ID		1
#define ON_REGISTER_GW_TIMER_ID 2
#define ON_LED_TIMER_ID			3
#define ON_TIME_COUNT_ID		4
#define RESEND_TIMER_ID			5
#define ZIGBEE_SEND_ID			6
#define DIMM_CONTROL_TIMER_ID	7
#define ZIGBEE_PING_ID			8
#define GCP_NOTICEMDOE_ID		9
#define SEND_TIMER_MPR_ID		10
#define TOKEN_TIMER_MPR_ID		11



//------------------------------------------------------------------------//
//-------------------------- Timer Count -----------------------------------//
//------------------------------------------------------------------------//
#define TIME_COUNT				1			// per 1min's


#define PING_TIME					60



//------------------------------------------------------------------------//
//-------------------------- EEPROM ID ------------------------------------//
//------------------------------------------------------------------------//
#define POWER_METER_VALUE		4050




typedef unsigned char uint8;
typedef unsigned int uint16;
typedef unsigned long uint32;

uint16 toUint16(uint8 hexString[4]);
uint8 toUint8(uint8 hexString[2]);
uint8 toHexCharacterOfLowerByte(uint8 ch);

#endif

