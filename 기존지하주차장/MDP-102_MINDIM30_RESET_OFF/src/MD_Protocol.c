#include "MD_Protocol.h"
#include "xcps.h"
#include "nc_protocol.h"
#include <avr/eeprom.h>

uint8 isWatchdogEnabled = 255;
uint8 watchdogResetCount = 255;

void MDP_ProcessMessage(uint8 msg[], int length) {
	uint8 msgType;

	msgType = msg[8];

	switch (msgType) { // Msg Type of NCP
	case PLCS_DP_REQ_DIMMING:
		MDP_ProcessDimmingReq(msg, length);
		break;
	case PLCS_DP_REQ_WATCHDOG:
		MDP_ProcessSetWatchdogReq(msg, length);
		break;
	default:
		break;
	}
}

void MDP_ProcessDimmingReq(uint8 msg[], int length) {
	uint8 channelId = 0;
	uint8 dimmingLevel = 0;

	channelId = msg[11];
	dimmingLevel = msg[12];

	if (channelId < 4) {
		if (dimmingLevel == 0) {
			g_DimmingLevel[channelId] = dimmingLevel;
		} else {
			g_DimmingLevel[channelId] = (int) (178.5 / 229.5 * (double) dimmingLevel + 56.67);
		}
	} else if (channelId == 0xff) {
		if (dimmingLevel == 0) {
			g_DimmingLevel[0] = g_DimmingLevel[1] = g_DimmingLevel[2] = g_DimmingLevel[3] = dimmingLevel;
		} else {
			g_DimmingLevel[0] = g_DimmingLevel[1] = g_DimmingLevel[2] = g_DimmingLevel[3] = (int) (178.5 / 229.5 * (double) dimmingLevel + 56.67);
		}
	}
	g_TestFlag = 1;

	PORTD ^= 0x40;

	if (MDP_GetWatchdogResetCount() > 0)
		MDP_SetWatchdogResetCount(0);
}

void MDP_ProcessSetWatchdogReq(uint8 msg[], int length) {
	isWatchdogEnabled = msg[11];
	if (eeprom_read_byte((uint8_t*) WATCHDOG_ENABLE_VALUE) != isWatchdogEnabled)
		eeprom_write_byte((uint8_t*) WATCHDOG_ENABLE_VALUE, isWatchdogEnabled);
}

uint8 MDP_IsWatchdogEnable() {
	if (isWatchdogEnabled == 255) {
		isWatchdogEnabled = eeprom_read_byte((uint8_t*) WATCHDOG_ENABLE_VALUE);
	}
	return isWatchdogEnabled;
}

uint8 MDP_GetWatchdogResetCount() {
	if (watchdogResetCount == 255) {
		watchdogResetCount = eeprom_read_byte((uint8_t*) WATCHDOG_RESET_COUNT_VALUE);
	}
	return watchdogResetCount;
}

uint8 MDP_SetWatchdogResetCount(uint8 value) {
	if (watchdogResetCount != value) {
		watchdogResetCount = value;
		eeprom_write_byte((uint8_t*) WATCHDOG_RESET_COUNT_VALUE, value);
	}
	return 0;
}
