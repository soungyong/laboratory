#include "Uart.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#define USART_BUFF_SIZE	64
//------------------------------------------------------//
uint8 uart_rx_buff[USART_BUFF_SIZE];
uint8 uart_rx_front = 0;
uint8 uart_rx_tail = 0;
uint8 uart_rx_len = 0;//------------------------------------------------------//



void InitUART(void) {
	UCSRA=0x00;
	UCSRB=0x90;
	UCSRC=0x86;
	UBRRH=0x00;
	UBRRL=0x33; //9600
}

//--------------------- USART0 -------------------------//
//------------------------------------------------------//
uint8 status;
uint8 data;
ISR(USART_RXC_vect) {
	DISABLE_INTERRUPT();
	while (((status = UCSRA) & (1 << RXC)) == 0)
		;

	data = UDR;

	uart_rx_buff[uart_rx_tail] = data;
	uart_rx_tail = (uart_rx_tail + 1) % USART_BUFF_SIZE;
	uart_rx_len++;
	ENABLE_INTERRUPT();
}

//------------------------------------------------------//

//------------------------------------------------------//
int USART_Receive(uint8 *buff) {

	DISABLE_INTERRUPT();

	if (uart_rx_len > 0) {
		uart_rx_len--;
		ENABLE_INTERRUPT();
		*buff = uart_rx_buff[uart_rx_front];
		uart_rx_front = (uart_rx_front + 1) % USART_BUFF_SIZE;

		return 1;
	}

	ENABLE_INTERRUPT();

	return 0;
}

int USART_Transmit(uint8 buff) {
	while ((UCSRA & (1 << UDRE)) == 0)
		;
	// tx isr.

	UDR = buff;
	return 1;
}

// End of file

