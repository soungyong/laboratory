#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "util.h"

typedef struct {
	uint16 rebootCnt;
	uint8 lastRebootTime[2];
	uint16 serverConnectionCnt;
	uint8 lastServerConnectionTime[2];
}DebugInfo_st;

DebugInfo_st debug_DebugInfo;

extern uint8 Min;
extern uint8 Hour;


void debug_UpdateLog();
void debug_ReadLog();
void debug_Reset();
void debug_UpdateReboot();
void debug_UpdateConnection();

#define FLASH_ADDRESS_DEBUG 1

#endif
