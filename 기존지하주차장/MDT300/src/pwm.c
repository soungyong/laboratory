#include <avr/io.h>
#include "util.h"
#include "pwm.h"

void pwm_Init(){
	DDRB|=0x80;
    TCCR2 = 0x6C;
}

void pwm_UnInit(){
	TCCR2=0x00;
}

//duty 1/16
void pwm_On(uint8 value){
	OCR2=value;
}

void pwm_Off(){
	OCR2=0;
}
