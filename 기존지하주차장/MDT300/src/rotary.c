#include "rotary.h"
#include <avr/io.h>
#include <avr/iom128.h>


void initRotary(){
    DDRB &= ~(0x70);
    DDRG &= ~(0x08);
    DDRE &= ~(0xF0);


	PORTB |= 0x70;
	PORTG |= 0x08;
	PORTE |= 0xF0;
}

uint8 rotary_GetValue() {
    uint8 id=0;

	id = (PINB & 0x70);
	id |= ((PING & 0x08) <<4);
	id |= (PINE>>4);
    
    return 0xff-id;
}
