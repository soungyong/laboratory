/**
 Device : DS1307
 I2C with 2 port.

 SCL : I2C_CLOCK
 SDA : I2C_DATA

 - note: initial setup should be done.

 // For I2C Interface 2005-09-12 Added by LeeJJ
 */
//-- for debug.
#include <stdio.h>
#include "../iinchip/types.h"
#include <avrx-io.h>

#define nop()	asm("nop")

// TODO.
#define	SCL_SET()		(PORTD |= 0x01)
#define	SCL_CLR()		(PORTD &= ~0x01)

#define	SDA_SET()		(PORTD |= 0x02)
#define	SDA_CLR()		(PORTD &= ~0x02)

#define	SCL_DIN()		(DDRD &= ~0x01)
#define	SCL_DOUT()		(DDRD |= 0x01)

#define	SDA_DIN()		(DDRD &= ~0x02)
#define	SDA_DOUT()		(DDRD |= 0x02)

#define	SDA_IS_HIGH()	((PIND & 0x02) == 0x02)

/**
 SCL interval(width) for 50K Hz.
 NOTE: SCL max 100 KHz.
 */

#define	DELAY()		Delay_us(10)

void nops(unsigned char num) {
	uint8 i;

	for (i = 0; i < num; i++)
		nop();
}

void Delay_us(uint8 time_us) {
	register uint8 i;

	for (i = 0; i < time_us; i++) {
		asm(" PUSH R0 ");
		asm(" POP R0 ");
		asm(" PUSH R0 ");
		asm(" POP R0 ");
		asm(" PUSH R0 ");
		asm(" POP R0 ");
	}
}

void Delay_ms(uint8 time_ms) {
	register uint8 i;

	for (i = 0; i < time_ms; i++) {
		Delay_us(250);
		Delay_us(250);
		Delay_us(250);
		Delay_us(250);
	}
}

void i2c_start(void) {
	SDA_SET();
	SCL_SET();
	SDA_DOUT();
	SCL_DOUT();

	Delay_us(7);

	SDA_CLR();
	Delay_us(7);
	SCL_CLR();
}

void i2c_stop(void) {
	SDA_DOUT();
	SCL_CLR();
	Delay_us(7);

	SCL_SET();
	Delay_us(4);
	SDA_SET();
	SDA_DIN();
}

/*
 static void i2c_clock(void)
 {
 PORTD |= 0x01;	// Set SCL High
 Delay_us(10);	// Small Delay
 PORTD &= 0xFE;	// Clear SCL Low
 }
 */

/**
 write bytes
 note: start seq is at i2c_start().
 */
void write_i2c_byte(unsigned char byte) {
	uint8 i;

	SDA_SET(); // to enable pullup.
	SDA_DOUT();
	SCL_CLR();

	for (i = 0; i < 8; i++) {
		// set data first
		if ((byte & 0x80) == 0x80)
			SDA_SET();
		else
			SDA_CLR();
		//nops(1);
		DELAY();

		// clk.
		SCL_SET();
		byte = byte << 1; // Shift data in buffer right one
		//nops(1);			// Small Delay
		DELAY();
		if (i == 0)
			nops(2);

		SCL_CLR();
	}

	SDA_SET(); // to enable pullup.
	SDA_DIN();

	DELAY();

	// wait ack.
	SCL_SET();
	DELAY();
	SCL_CLR();
	DELAY();
	//nops(45);			// Small Delay

	// wait ack.
	//if(i2c_ackn()==1)	ucNack=1;
}

/**
 Read ..
 */
unsigned char read_i2c_byte(unsigned char ch) {
	uint8 i, buff = 0;

	SDA_SET(); // to enable internal pull-up : should be done..
	SDA_DIN();
	Delay_us(5);

	for (i = 0; i < 8; i++) {
		buff <<= 1;

		SCL_SET();
		//nops(3);		// Small Delay

		// Read data on SDA pin
		if (SDA_IS_HIGH()) {
			buff |= 0x01;
		}
		DELAY();

		SCL_CLR();
		//nops(3);		// Small Delay
		DELAY();
	}

	//nops(80);			// Long Delay

	// write ack.
	SDA_DOUT();
	if (ch == 0) // Ack
			{
		SDA_CLR(); // ack..
		SCL_SET();
		DELAY();
		SCL_CLR();
		SDA_CLR();
	} else // No Ack
	{
		SDA_SET();
		SCL_SET();
		DELAY();
		SCL_CLR();
		SDA_CLR();
	}

	return buff;
}

void ds1307_initial_config(uint8 sec, uint8 min, uint8 hour, uint8 day,
		uint8 date, uint8 month, uint8 year) {
	// Set SCL, SDA to output
	// Set SCL, SDA High
	SDA_SET();
	SCL_SET();

	SDA_DOUT();
	SCL_DOUT();

	uint8 _sec, _min, _hour, _day, _date, _month, _year;

	_sec = (sec / 10) << 4 | sec % 10;
	_min = (min / 10) << 4 | min % 10;
	if (hour >= 12) {
		_hour = 1 << 5 | ((hour - 12) / 10) << 4 | (hour - 12) % 10;
	} else {
		_hour = (hour / 10) << 4 | hour % 10;
	}
	_day = day;
	_date = (date / 10) << 4 | date % 10;
	_month = (month / 10) << 4 | month % 10;
	_year = ((year % 100) / 10) << 4 | year % 10;

	i2c_start();
	write_i2c_byte(0xD0);
	write_i2c_byte(0x00); // Start
	write_i2c_byte(_sec); // sec
	write_i2c_byte(_min); // min
	write_i2c_byte(_hour); // hr
	write_i2c_byte(_day); // day of week
	write_i2c_byte(_date); // dt
	write_i2c_byte(_month); // mon
	write_i2c_byte(_year); // yr
	write_i2c_byte(0x10); // sqwe
	i2c_stop();

}

void ds1307_init(void) {
	// Set SCL, SDA to output
	// Set SCL, SDA High
	SDA_SET();
	SCL_SET();

	SDA_DOUT();
	SCL_DOUT();
}

/**
 read clock
 */
#if 0
int ds1307_read()
{
	uint8 ucHibyte = 0;
	uint8 ucLobyte = 0;

	i2c_start();

	write_i2c_byte(0xD0);
	DELAY();

	write_i2c_byte(0x00);
	DELAY();

	i2c_start();
	write_i2c_byte(0xD1); // Data Read - slave transmitter mode
	DELAY();

	sec = read_i2c_byte(0);// with ack.
	DELAY();

	min = read_i2c_byte(0);// with ack.
	DELAY();

	hour = read_i2c_byte(0);// with ack.
	DELAY();

	day = read_i2c_byte(0);// with ack.
	DELAY();

	date = read_i2c_byte(1);// with ack.
	DELAY();

	month = read_i2c_byte(1);// with ack.
	DELAY();

	year = read_i2c_byte(1);// with ack.
	DELAY();

	printf_P(PSTR("\n%02X, %02X, %02X, %02X, %02X, %02X, %02X"), sec, min, hour, day, date, month, year);

	i2c_stop();

	return 0;
}
#else
int ds1307_read(uint8 address) {
	uint8 buffer;

	i2c_start();
	write_i2c_byte(0xD0);
	write_i2c_byte(address);
	i2c_stop();

	i2c_start();
	write_i2c_byte(0xD1); // Data Read - slave transmitter mode
	DELAY();

	buffer = read_i2c_byte(0); // with ack.
	i2c_stop();

	switch (address) {
	case 2: //hour
		if ((buffer & 0x40) == 0) //12 hour, am/pm
			if ((buffer & 0x20) == 0)
				return buffer = ((buffer & 0x10) >> 4) * 10 + (buffer & 0x0F);
			else
				return buffer = 12 + ((buffer & 0x10) >> 4) * 10 + (buffer & 0x0F);
		else
			//24hour
			return buffer = ((buffer & 0x30) >> 4) * 10 + (buffer & 0x0F);

		break;
	case 0: //sec
	case 1: //min
	case 4: //day
	case 5: //month
	case 6: //year
		return buffer = (buffer >> 4) * 10 + (buffer & 0x0F);
		break;
	case 3: //dayOfWeak
		return buffer = buffer & 0x0F;
		break;
	}

	return buffer;
}

#endif

