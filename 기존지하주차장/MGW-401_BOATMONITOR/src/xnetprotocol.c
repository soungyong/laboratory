#include <avr/eeprom.h>
#include "XNetProtocol.h"
#include "RFU_Protocol.h"
#include "PLCS_Protocol_LC100.h"
#include "plcs_protocol_server.h"
#include "PLCS_Protocol.h"

uint8 tmp_Data_Server[64];
uint8 tmp_Data_Controler[64];

void XNetPacketFromController(uint8 *msg, int buff_length) {
	uint8 i = 0;
	uint8 tmp_msg[64];
	int len = 0;
	uint8 pid = 0;
	uint8 subPid = 0;
	uint16 seqNum;
	uint16 srcId;
	uint16 destId;

	//tmp_msg에 pid~payload끝까지 저장.

	for (i = 0; i < buff_length; i++)
		tmp_msg[i] = msg[i];

	len = buff_length;

	pid = tmp_msg[0];
	subPid = tmp_msg[1];
	seqNum = msg[2] << 8 | msg[3];
	destId = msg[4] << 8 | msg[5];
	srcId = msg[6] << 8 | msg[7];

	if (pid == NCP_PROTOCOL_ID) {
		if (subPid == PLCS_NCP_PROTOCOL_ID) {
			plcs_NCP_LC100_ProcessMessage(seqNum, srcId, destId, tmp_msg, len);
		} else if (subPid == PLCS_GCP_PROTOCOL_ID) {
			plcs_GCP_LC100_ProcessMessage(seqNum, srcId, destId, tmp_msg, len);
		} else if (subPid == PLCS_RFU_PROTOCOL_ID)
			plcs_RFUP_ProcessMessage(0, seqNum, srcId, destId, msg, len);
	}

}

void XNetPacketFromServer(uint8 *msg, int buff_length) {
	uint8 i = 0;
	uint8 tmp_msg[64];
	int len = 0;
	uint8 pid = 0;
	uint8 subPid = 0;
	uint16 seqNum;
	uint16 srcId;
	uint16 destId;

	//tmp_msg에 pid~payload끝까지 저장.
	for (i = 0; i < buff_length; i++)
		tmp_msg[i] = msg[i];

	len = buff_length;

	pid = tmp_msg[0];
	subPid = tmp_msg[1];
	seqNum = msg[2] << 8 | msg[3];
	destId = msg[4] << 8 | msg[5];
	srcId = msg[6] << 8 | msg[7];

	if (pid == NCP_PROTOCOL_ID) {
		if (subPid == PLCS_NCP_PROTOCOL_ID)
			plcs_NCP_Server_ProcessMessage(seqNum, srcId, destId, tmp_msg, len);
		else if (subPid == PLCS_GCP_PROTOCOL_ID)
			plcs_GCP_Server_ProcessMessage(seqNum, srcId, destId, tmp_msg, len);
		else if (subPid == PLCS_RFU_PROTOCOL_ID)
			plcs_RFUP_ProcessMessage(0, seqNum, srcId, destId, tmp_msg, len);
	}
}

void sendToController(uint8 msg[], int msg_len) {
	int len = 0;
	memcpy(&tmp_Data_Controler[len], msg, msg_len);
	len += msg_len;

	xcps_send(tmp_Data_Controler, len);
	MSLEEP(3);
}

void sendToServer(uint8 msg[], int msg_len) {
	int len = 0;
	if (getSn_SR(SOCK_TCPC) != SOCK_ESTABLISHED)
		return;
	memcpy(&tmp_Data_Server[len], msg, msg_len);
	len += msg_len;

	xcpsnet_send(tmp_Data_Server, len);
	MSLEEP(3);
}

//end of file

