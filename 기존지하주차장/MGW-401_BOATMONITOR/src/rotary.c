#include "rotary.h"
#include <avr/io.h>


void initRotary(){
    DDRB &= ~(0xF0);

	PORTB |= 0xF0;
}

uint8 rotary_GetValue() {
    uint8 id=0;

	id = PINB>>4;
    
    return 0x0f-id;
}
