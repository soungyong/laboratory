#ifndef	__DS1307_I2C_H__
#define	__DS1307_I2C_H__


void nops(unsigned char num);

void Delay_us(uint8 time_us);

void Delay_ms(uint8 time_ms);

void i2c_start(void);

void i2c_stop(void);

void write_i2c_byte(unsigned char byte);

unsigned char read_i2c_byte(unsigned char ch);

//void ds1307_initial_config(void);
void ds1307_initial_config(uint8 sec, uint8 min, uint8 hour, uint8 day, uint8 date, uint8 month, uint8 year );

void ds1307_init(void);

int ds1307_read();



#endif	//__DS1307_I2C_H__
