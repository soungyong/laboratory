#ifndef	__UART_H_
#define	__UART_H_

/**
	ATmega128 USART Interface.
**/


#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdio.h>
#include "../iinchip/types.h"
#include "Util.h"


#define DISABLE_INTERRUPT()     asm("cli")
#define ENABLE_INTERRUPT()      asm("sei")


/**
	command writer (via uart) setting

	int uart_putchar0(uint8 *c);
*/
typedef	int (*usart1_getter)(uint8 *data);
typedef	int (*usart1_putter)(uint8 data);


/**

*/

void InitUART();

int USART0_Transmit(uint8 buff);
int USART0_Receive(uint8 *buff);

int USART1_Transmit(uint8 buff);
int USART1_Receive(uint8 *buff);


#endif
