#ifndef __ROTARY_H
#define __ROTARY_H

#include "../iinchip/types.h"


void initRotary();
uint8 rotary_GetValue();

#endif
