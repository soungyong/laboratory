#include "main.h"
#include "plcs_Protocol_Server.h"
#include "debug.h"
#include "Xcpsnet.h"
#include "Rotary.h"
#include "NCProtocol.h"
#include "Flash.h"

// Avrx hw Peripheral initialization
#define CPUCLK 			16000000L     			// CPU xtal
#define TICKRATE 		1000				// AvrX timer queue Tick rate
#define TCNT0_INIT 		(CPUCLK/128/TICKRATE)

// NB: Clock divisor bits are different between classic and mega103 chips!
// NB: IAR chooses to use different CPU identifiers for their C compiler
// NB: GCC changes the IO bit names just for fun...

#define TCCR0_INIT	((1<<CS02) | (0<<CS01) | (1<<CS00))

char cmd = 0;

int settingState = 0;
int getSettingState() {
	return settingState;
}
int setSettingState(int value) {
	settingState = value;
	return settingState;
}

uint8 plcs_NCP_SERVER_NetState = NET_NOT_CONNECT;
uint16 plcs_NCP_SERVER_PingInterval = 30;
uint8 plcs_NCP_SERVER_Ping_Count = 0;

uint8 commandPacket[64];
uint8 commandMessage[TX_RX_MAX_BUF_SIZE];
uint8 tmp_Packet[TX_RX_MAX_BUF_SIZE];

static uint8 Sock_State = 0;

uint8 m_SokStatus = 2; /** 0:close, 1:ready, 2:connect */
uint16 socket_Length;
uint16 Server_Port = 30011; // MY Connect Port : 30002
uint8 resetCount = 0;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
int Min, Hour;
uint8 NowTime;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

uint16 ncp_GW_Id = 0;

//-----------------------------------------------------------------------------
#define	ATMEGA128_0WAIT			0
#define	ATMEGA128_1WAIT			1
#define	ATMEGA128_2WAIT			2
#define	ATMEGA128_3WAIT			3
#define	ATMEGA128_NUM_WAIT		ATMEGA128_3WAIT
/******************************************************************************/

#if 1
//-----------------------------------------------------------------------------
extern uint8 Server_Ip[4];
extern uint8 my_Ip[4];
extern uint8 mac_Addr[6];
extern uint8 gw_Ip[4];
extern uint8 sub_Net[4];
//-----------------------------------------------------------------------------
#endif

uint8 isOnSensorSharing = false;

//-----------------------------------------------------------------------------
//MCU Initialize
void InitMCU() {
	PORTG = 0xFF;
	DDRG = 0xFF;

	PORTD = 0x00;
	DDRD = 0x30;

	PORTE = 0x80;
	DDRE = 0xF2;

#ifndef __DEF_IINCHIP_INT__	
	EICRA = 0x00;
	EICRB = 0x00;
	EIMSK = 0x00;
	EIFR = 0x00;
#else
	EICRA = 0x00; // External Interrupt Control Register A clear
	EICRB = 0x02;// External Interrupt Control Register B clear // edge
	EIMSK = (1 << INT4);// External Interrupt Mask Register : 0x10
	EIFR = 0xFF;// External Interrupt Flag Register all clear
	DDRE &= ~(1 << INT4);// Set PE Direction
	PORTE |= (1 << INT4);// Set PE Default value

#endif

#if (ATMEGA128_NUM_WAIT == ATMEGA128_0WAIT)
	MCUCR = 0x80;
	XMCRA=0x40;

#elif (ATMEGA128_NUM_WAIT == ATMEGA128_1WAIT)
	MCUCR = 0xC0; // MCU control regiseter : enable external ram
	XMCRA=0x40;// External Memory Control Register A :
			   // Low sector   : 0x1100 ~ 0x7FFF
			   // Upper sector : 0x8000 ~ 0xFFFF

#elif (ATMEGA128_NUM_WAIT == ATMEGA128_2WAIT )
	MCUCR = 0x80;
	XMCRA=0x42;

#elif (ATMEGA128_NUM_WAIT == ATMEGA128_3WAIT)
	MCUCR = 0xC0;
	XMCRA = 0x42;

#else
#error "unknown atmega128 number wait type"
#endif	

#if (__DEF_IINCHIP_BUS__ == __DEF_IINCHIP_SPI_MODE__)
	//SPI Initialize
	IINCHIP_SpiInit();

	IINCHIP_CSInit();
	IINCHIP_CSon();
#endif
}

//----------------------------------------------------------------------//

uint8 stateForSettingTask = 0;
/***** Task Led & Network Setting *****/
void Setting_Task() {
	static char Input_ServerIp[20], Input_GatewayIp[20], Input_DGWIp[20], Input_SubNET[20];
	static uint8 state = 0;
	if (stateForSettingTask != 0)
		return;

	//scanf("%c", &cmd);
	if (!(UCSR0A & (1 << RXC0)))
		return;
	if ((cmd = UDR0) == 0) {
		return;
	}

	switch (cmd) {
	//timer_clear(SETTING_TIMER_ID);

	case '1':
		printf_P(PSTR("\nNew Server IP >> "));
		scanf("%s", Input_ServerIp);
		parsing(Input_ServerIp, Server_Ip);
		cmd = '7';

		printf_P(PSTR("\nNew Server IP: %3d.%3d.%3d.%3d"), Server_Ip[0], Server_Ip[1], Server_Ip[2],
				Server_Ip[3]);

		/*eeprom_write_byte((uint8_t*) 20, Server_Ip[0]);
		 eeprom_write_byte((uint8_t*) 21, Server_Ip[1]);
		 eeprom_write_byte((uint8_t*) 22, Server_Ip[2]);
		 eeprom_write_byte((uint8_t*) 23, Server_Ip[3]);*/
		flash_writeBytes(20, Server_Ip, 4);
		fflush(stdin);
		printf_P(PSTR("\ncmd >> "));
		break;

	case '2':
		printf_P(PSTR("\nNew Gateway IP >> "));
		scanf("%s", Input_GatewayIp);
		parsing(Input_GatewayIp, my_Ip);
		cmd = '7';

		printf_P(PSTR("\nNew Gateway IP: %3d.%3d.%3d.%3d"), my_Ip[0], my_Ip[1], my_Ip[2], my_Ip[3]);

		/*eeprom_write_byte((uint8_t*) 30, my_Ip[0]);
		 eeprom_write_byte((uint8_t*) 31, my_Ip[1]);
		 eeprom_write_byte((uint8_t*) 32, my_Ip[2]);
		 eeprom_write_byte((uint8_t*) 33, my_Ip[3]);*/
		flash_writeBytes(30, my_Ip, 4);
		fflush(stdin);
		printf_P(PSTR("\ncmd >> "));
		break;

	case '3':
		printf_P(PSTR("\nNew Default G/W IP >> "));
		scanf("%s", Input_DGWIp);
		parsing(Input_DGWIp, gw_Ip);
		cmd = '7';

		printf_P(PSTR("\nNew Default GW: %3d.%3d.%3d.%3d"), gw_Ip[0], gw_Ip[1], gw_Ip[2], gw_Ip[3]);

		/*eeprom_write_byte((uint8_t*) 40, gw_Ip[0]);
		 eeprom_write_byte((uint8_t*) 41, gw_Ip[1]);
		 eeprom_write_byte((uint8_t*) 42, gw_Ip[2]);
		 eeprom_write_byte((uint8_t*) 43, gw_Ip[3]);*/
		flash_writeBytes(40, gw_Ip, 4);
		fflush(stdin);
		printf_P(PSTR("\ncmd >> "));
		break;

	case '4':
		printf_P(PSTR("\nSub NET MASK >> "));
		scanf("%s", Input_SubNET);
		parsing(Input_SubNET, sub_Net);
		cmd = '7';

		printf_P(PSTR("\nNew Default GW: %3d.%3d.%3d.%3d"), sub_Net[0], sub_Net[1], sub_Net[2],
				sub_Net[3]);

		/*eeprom_write_byte((uint8_t*) 50, sub_Net[0]);
		 eeprom_write_byte((uint8_t*) 51, sub_Net[1]);
		 eeprom_write_byte((uint8_t*) 52, sub_Net[2]);
		 eeprom_write_byte((uint8_t*) 53, sub_Net[3]);*/
		flash_writeBytes(50, sub_Net, 4);
		fflush(stdin);
		printf_P(PSTR("\ncmd >> "));
		break;

	case '5':
		printf_P(PSTR("\nPort >> "));
		scanf("%d", &Server_Port);
		cmd = '7';

		printf_P(PSTR("\nNew Port: %d"), Server_Port);

		//eeprom_write_word((uint16_t*) 60, Server_Port);
		flash_writeUint16(60, Server_Port);
		fflush(stdin);
		printf_P(PSTR("\ncmd >> "));
		break;
	case '6':
		printf_P(PSTR("\nisOnSensorSharing(1: On, 0: off) >> "));
		scanf("%d", &isOnSensorSharing);
		cmd = '7';

		printf_P(PSTR("\nNew Mode: %d"), isOnSensorSharing);
		if (isOnSensorSharing > 1)
			isOnSensorSharing = 1;

		//eeprom_write_byte((uint8_t*) 70, isOnSensorSharing);
		flash_writeUint8(70, isOnSensorSharing);
		fflush(stdin);
		printf_P(PSTR("\ncmd >> "));
		break;

	case '7':
		//Exit
		printf_P(PSTR("\nExit!\nStart... MGW-300\n"));
		After_Config_Display();
		InitNET();
		setSettingState(1);
		stateForSettingTask = 2;
		WDT_INIT();
		return;
		break;

	default:
		printf_P(PSTR("\ncmd >> "));

		break;
	}
}

int hexToint(uint8 tmp_hex) {
	int TimeValue = 0;

	if ((tmp_hex >= 0x00) && (tmp_hex <= 0x09))
		TimeValue = (int) tmp_hex;
	else if ((tmp_hex >= 0x10) && (tmp_hex <= 0x19))
		TimeValue = ((int) tmp_hex) - 6;
	else if ((tmp_hex >= 0x20) && (tmp_hex <= 0x29))
		TimeValue = ((int) tmp_hex) - 12;
	else if ((tmp_hex >= 0x30) && (tmp_hex <= 0x39))
		TimeValue = ((int) tmp_hex) - 18;
	else if ((tmp_hex >= 0x40) && (tmp_hex <= 0x49))
		TimeValue = ((int) tmp_hex) - 24;
	else if ((tmp_hex >= 0x50) && (tmp_hex <= 0x59))
		TimeValue = ((int) tmp_hex) - 30;

	return (TimeValue);
}

/***** Task Led & Network Setting *****/
void Led_Task() {
	static uint8 state = 0;
	static uint8 WDT_Count = 1;
	static uint8 Convert_Hour, Convert_Min;

	static uint32 Register_Count = 1;
	static uint8 settingCnt = 0;

	if (settingCnt > 10 && stateForSettingTask == 0) {
		InitNET();
		setSettingState(1);
		stateForSettingTask = 1;
		settingCnt = 0;
		WDT_INIT();
	} else if (cmd == '7') {
		settingCnt = 0;
		cmd = 0;
	}

	if (timer_isfired(LED_TIMER_ID)) {
		if (state == 1) {
			if (resetCount < 6)
				wdt_reset();
			STATE_LED_OFF();
		}

		else
			STATE_LED_ON();

		state = !state;
		settingCnt++;
		timer_clear(LED_TIMER_ID);
		timer_set(LED_TIMER_ID, 500);
	}

	if (timer_isfired(TIME_COUNT_ID)) {
		static uint8 flag = 0;
		uint8 i = 0;
		Min = ds1307_read(1);
		Hour = ds1307_read(2);
		if (flag > 10) {
			for (i = 0; i < deviceInfoTable.size; i++)
				plcs_NCP_LC100_SendTimeInfo(deviceInfoTable.deviceInfo[i].nodeId, Hour, Min);
			flag = 0;
		}

		timer_clear(TIME_COUNT_ID);
		if (deviceInfoTable.size == 0)
			timer_set(TIME_COUNT_ID, (TIME_COUNT * 20000));
		else
			timer_set(TIME_COUNT_ID, (TIME_COUNT * 60000));
		plcs_NCP_LC100_SendRegisterBroadcastReq(flag);
		flag++;
	}

}

/***** Task Serial *****/
void Serial_Task() {
	static int Recvlen = 0;

	if ((Recvlen = xcps_recv(commandPacket, 64)) > 0) {
#if(DEBUG_ENABLE==1)
		uint8 i=0;
		if(commandPacket[2]!=0x07 && commandPacket[2]!=0x06 && commandPacket[2]!=0x08) {
			printf_P(PSTR("\nG<-N :"));
			for(i = 0; i < Recvlen; i++)
			printf_P(PSTR("%02X."), commandPacket[i]);

		}
#endif

		XNetPacketFromController(commandPacket, Recvlen);
	}
}

/***** Token Manage *****/
//AVRX_GCC_TASKDEF(Token_Task, 128, 5)					// Gateway <-> Load Controller
void Token_Task() {
	static char index = 0;
	static int cnt = 0;

	//if (plcs_NCP_SERVER_GetNetworkState() == NET_REGISTER) {
	if (timer_isfired(TOKEN_TIMER_ID)) {
		if (cnt > 120 || (cnt > 60 && plcs_NCP_LC100_GetToken() == -1)) {
			if (deviceInfoTable.size > 0
					&& plcs_NCP_LC100_AssignToken(deviceInfoTable.deviceInfo[index].nodeId) == 0) {
				index++;
				index %= deviceInfoTable.size;
			}
			cnt = 0;
		}
		cnt++;
		timer_clear(TOKEN_TIMER_ID);
		timer_set(TOKEN_TIMER_ID, 1);
	}
	//}
}

/***** Task Ethernet *****/
//AVRX_GCC_TASKDEF(Ethenet_Task, 512, 3)				// Server <-> Gateway
void Ethernet_Task() {
	static uint8 socket_openflag = 0;

	Sock_State = getSn_SR(SOCK_TCPC);

	if (plcs_NCP_SERVER_GetNetworkState() == NET_REGISTER) {
		resetCount = 0;

		if (timer_isfired(NCP_PING_TIMER_ID)) {
			plcs_NCP_SERVER_SendPingReq();
			plcs_NCP_SERVER_Ping_Count = plcs_NCP_SERVER_Ping_Count + 1;

			if (plcs_NCP_SERVER_Ping_Count > 3) {
				plcs_NCP_SERVER_NetState = NET_NOT_CONNECT;
				plcs_NCP_SERVER_Ping_Count = 0;
				plcs_NCP_SERVER_SendRegisterReq();
			}

			timer_clear(NCP_PING_TIMER_ID);
			timer_set(NCP_PING_TIMER_ID, plcs_NCP_SERVER_GetPingIntervalTime() * 1000);
		}
	}

	switch (Sock_State) {
	case SOCK_ESTABLISHED:
		if (socket_openflag == 1) {
			if (plcs_NCP_SERVER_GetNetworkState() == NET_NOT_CONNECT) {
				plcs_NCP_SERVER_SendRegisterReq(); /** 1. Register -> 2. Ping **/
				socket_openflag = 0;
			}
		} else {
			if (m_SokStatus == 1) /** 1:ready **/
				m_SokStatus = 2; /** 2:connect **/

			if ((socket_Length = getSn_RX_RSR(SOCK_TCPC)) > 0) {
				if (socket_Length >= TX_RX_MAX_BUF_SIZE)
					socket_Length = TX_RX_MAX_BUF_SIZE;
				recv(SOCK_TCPC, tmp_Packet, socket_Length);

				memcpy(commandMessage, tmp_Packet, socket_Length);
#if(DEBUG_ENABLE==1)

				printf_P(PSTR("\nG<-S :"));
				for(i = 0; i < socket_Length; i++)
				printf_P(PSTR("%02X."), commandMessage[i]);
#endif

				xcpsnet_recv(commandMessage, socket_Length); // Receive Ethernet Message
			}
		}
		break;

	case SOCK_CLOSE_WAIT:
		disconnect(SOCK_TCPC);
		m_SokStatus = 0; /** 0:close */
		plcs_NCP_SERVER_NetState = NET_NOT_CONNECT;
		break;

	case SOCK_CLOSED:
		if (!m_SokStatus)
			m_SokStatus = 1; /** 1:ready */

		plcs_NCP_SERVER_NetState = NET_NOT_CONNECT;

		if (timer_isfired(RECONNECT_TIMER)) {
			if (socket(SOCK_TCPC, Sn_MR_TCP, Server_Port, 0x00) == 0)
				m_SokStatus = 0;
			else
				socket_openflag = connect(SOCK_TCPC, (uint8 *) &Server_Ip, Server_Port);

			timer_clear(RECONNECT_TIMER);
			timer_set(RECONNECT_TIMER, 5000);
		}

		break;

	default:
		// TODO::
		break;

	}
}

void GWID_Task() {
	//if (plcs_NCP_SERVER_GetNetworkState() == NET_REGISTER) {
	if (timer_isfired(GWID_TIMER_ID)) {
		if (ncp_GW_Id != rotary_GetValue())
			while (1)
				;

		timer_clear(GWID_TIMER_ID);
		timer_set(GWID_TIMER_ID, 1000);
	}
	//}
}

//************************************************************************************************************************************//
//******** Gateway Ethernet Setting ******************************************************************************************************//
//************************************************************************************************************************************//
void Before_Config_Display() {
	printf_P(PSTR("\n#########################################"));
	MSLEEP(1);
	printf_P(PSTR("\n# Copyright : M2MKorea, Inc. 2011       #"));
	MSLEEP(1);
	printf_P(PSTR("\n# Homepage  : http://www.m2mkorea.co.kr #"));
	MSLEEP(1);
	printf_P(PSTR("\n#########################################\n\n"));
	MSLEEP(1);

	printf_P(PSTR("\n######## GATEWAY NETWORK SETTING ########"));
	MSLEEP(1);
	printf_P(PSTR("\n#      Select Setting Item Number       #"));
	MSLEEP(1);
	printf_P(PSTR("\n#########################################"));
	MSLEEP(1);
	printf_P(PSTR("\n  [1]. Server IP    :  %3d.%3d.%3d.%3d"), Server_Ip[0], Server_Ip[1],
			Server_Ip[2], Server_Ip[3]);
	MSLEEP(1);
	printf_P(PSTR("\n  [2]. Gateway IP   :  %3d.%3d.%3d.%3d"), my_Ip[0], my_Ip[1], my_Ip[2],
			my_Ip[3]);
	MSLEEP(1);
	printf_P(PSTR("\n  [3]. Default GW   :  %3d.%3d.%3d.%3d"), gw_Ip[0], gw_Ip[1], gw_Ip[2],
			gw_Ip[3]);
	MSLEEP(1);
	printf_P(PSTR("\n  [4]. SubNet MASK  :  %3d.%3d.%3d.%3d"), sub_Net[0], sub_Net[1], sub_Net[2],
			sub_Net[3]);
	MSLEEP(1);
	printf_P(PSTR("\n  [5]. Port         :  %d"), Server_Port);
	MSLEEP(1);
	printf_P(PSTR("\n  [6]. SensorSharing:  %d"), isOnSensorSharing);
	MSLEEP(1);
	printf_P(PSTR("\n  [7]. Quit ?"));
	MSLEEP(1);
	printf_P(PSTR("\n#########################################"));
}

void After_Config_Display() {
	printf_P(PSTR("\n##  Server IP    :  %3d.%3d.%3d.%3d   ##"), Server_Ip[0], Server_Ip[1],
			Server_Ip[2], Server_Ip[3]);
	MSLEEP(1);
	printf_P(PSTR("\n##  Gateway IP   :  %3d.%3d.%3d.%3d   ##"), my_Ip[0], my_Ip[1], my_Ip[2],
			my_Ip[3]);
	MSLEEP(1);
	printf_P(PSTR("\n##  Default GW   :  %3d.%3d.%3d.%3d   ##"), gw_Ip[0], gw_Ip[1], gw_Ip[2],
			gw_Ip[3]);
	MSLEEP(1);
	printf_P(PSTR("\n##  SubNet MASK  :  %3d.%3d.%3d.%3d   ##"), sub_Net[0], sub_Net[1], sub_Net[2],
			sub_Net[3]);
	MSLEEP(1);
	printf_P(PSTR("\n##  Port         :  %d             ##"), Server_Port);
	MSLEEP(1);
	printf_P(PSTR("\n##  SensorShareMode :  %d              ##"), isOnSensorSharing);
}

void parsing(char *String, uint8 *data) {
	char *p;
	uint8 cnt = 0;

	p = strtok(String, ".");

	while (p) {
		data[cnt++] = (uint8) atoi(p);
		p = strtok(NULL, ".");
	}
}

void GateWaySET() {
	uint8 i = 0;
	uint8 flag = 0;
	uint8 server_IpTemp[4];
	uint8 my_IpTemp[4];
	uint8 gw_IpTemp[4];
	uint8 sub_NetTemp[4];
	uint16 portTemp = 0;

	/*for (i = 0; i < 4; i++) {
	 server_IpTemp[i] = eeprom_read_byte((uint8_t*) 20 + i);
	 my_IpTemp[i] = eeprom_read_byte((uint8_t*) 30 + i);
	 gw_IpTemp[i] = eeprom_read_byte((uint8_t*) 40 + i);
	 sub_NetTemp[i] = eeprom_read_byte((uint8_t*) 50 + i);
	 }
	 portTemp = eeprom_read_word((uint16_t*) 60);

	 isOnSensorSharing = eeprom_read_byte((uint8_t*) 70);*/

	 flash_readBytes(20, server_IpTemp, 4);
	 flash_readBytes(30, my_IpTemp, 4);
	 flash_readBytes(40, gw_IpTemp, 4);
	 flash_readBytes(50, sub_NetTemp, 4);

	/*for (i = 0; i < 4; i++) {
		server_IpTemp[i] = flash_readUint8(20 + i);
		my_IpTemp[i] = flash_readUint8(30 + i);
		gw_IpTemp[i] = flash_readUint8(40 + i);
		sub_NetTemp[i] = flash_readUint8(50 + i);
	}*/
	portTemp = flash_readUint16(60);

	isOnSensorSharing = flash_readUint8(70);

	if (isOnSensorSharing > 1)
		isOnSensorSharing = 0;

	for (i = 0; i < 4; i++) {
		if (server_IpTemp[i] != 255)
			flag = 1;
		if (my_IpTemp[i] != 255)
			flag = 1;
		if (gw_IpTemp[i] != 255)
			flag = 1;
		if (sub_NetTemp[i] != 255)
			flag = 1;
	}

	if (flag != 0) {
		for (i = 0; i < 4; i++) {
			Server_Ip[i] = server_IpTemp[i];
			my_Ip[i] = my_IpTemp[i];
			gw_Ip[i] = gw_IpTemp[i];
			sub_Net[i] = sub_NetTemp[i];
		}
		Server_Port = portTemp;
	}
}

void WDT_INIT() {
	MCUCSR &= ~(1 << WDRF); // WatchDog Init(Low)
	wdt_enable(WDTO_2S);
	// WatchDog Reset Time(High)
}

//-----------------------------------------------------------------------------
// Main function
//-----------------------------------------------------------------------------
int main(void) {
	uint8 i = 0;
	InitMCU();
	InitUART();
	initRotary();
	timer_init();
	flash_init();

	GateWaySET();
	initDeviceTable();
	Before_Config_Display();
	MSLEEP(1);
	printf_P(PSTR("\nPress any Number [1 ~ 7].... "));

	TIMSK |= 0x04;

	ncp_GW_Id = rotary_GetValue();

	debug_ReadLog();
	debug_UpdateReboot();

	/*while (1) {
	 WDT_INIT();
	 debug_ReadLog();
	 debug_UpdateReboot();
	 MSLEEP(1000);
	 }*/

	timer_set(LED_TIMER_ID, 500);

	timer_set(TIME_COUNT_ID, (TIME_COUNT * 30000)); // TIME_COUNT 만큼 시간 증가 (Default: 1 Seconds)

	xcps_init(USART1_Receive, USART1_Transmit);

	timer_set(TOKEN_TIMER_ID, 1);

	timer_set(RECONNECT_TIMER, 5000);
	timer_set(NCP_PING_TIMER_ID, 1000);
	timer_set(GWID_TIMER_ID, 1000);

	while (1) {
		Setting_Task();
		Led_Task();
		Serial_Task();
		Ethernet_Task();
		Token_Task();
		GWID_Task();
	}

	return 0;
}

