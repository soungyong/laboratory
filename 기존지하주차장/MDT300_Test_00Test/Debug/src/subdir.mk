################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/Flash.c \
../src/GC_Protocol.c \
../src/MDT-200Z.c \
../src/MD_Protocol.c \
../src/NCProtocol.c \
../src/RFU_Protocol.c \
../src/Timer.c \
../src/Uart.c \
../src/XNetProtocol.c \
../src/Xcps.c \
../src/ZRMProtocol.c \
../src/luminature.c \
../src/pwm.c \
../src/rotary.c \
../src/util.c 

OBJS += \
./src/Flash.o \
./src/GC_Protocol.o \
./src/MDT-200Z.o \
./src/MD_Protocol.o \
./src/NCProtocol.o \
./src/RFU_Protocol.o \
./src/Timer.o \
./src/Uart.o \
./src/XNetProtocol.o \
./src/Xcps.o \
./src/ZRMProtocol.o \
./src/luminature.o \
./src/pwm.o \
./src/rotary.o \
./src/util.o 

C_DEPS += \
./src/Flash.d \
./src/GC_Protocol.d \
./src/MDT-200Z.d \
./src/MD_Protocol.d \
./src/NCProtocol.d \
./src/RFU_Protocol.d \
./src/Timer.d \
./src/Uart.d \
./src/XNetProtocol.d \
./src/Xcps.d \
./src/ZRMProtocol.d \
./src/luminature.d \
./src/pwm.d \
./src/rotary.d \
./src/util.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega128 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


