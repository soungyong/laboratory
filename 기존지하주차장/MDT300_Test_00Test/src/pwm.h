#ifndef __PWM_H
#define __PWM_H

#include "util.h"


void pwm_Init();
void pwm_UnInit();
void pwm_On(uint8 value);
void pwm_Off();

#endif
