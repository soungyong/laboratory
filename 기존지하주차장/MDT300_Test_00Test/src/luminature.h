#ifndef __LUMINATURE_H__
#define __LUMINATURE_H__

#include "util.h"

void luminature_Init();
void luminature_ControlLuminatureDimming(uint8 dimmingLevel);

#endif
