#include "MPRProtocol.h"
#include "NCProtocol.h"
#include "PLCS_Protocol.h"
#include "XNetProtocol.h"
#include "RFU_Protocol.h"

int mprToken = -1;
uint8 tmp_Buff_MPR[MAX_TX_BUFF];

char mpr_GetAssignedTokenId() {
	return mprToken;
}

char mpr_HasToken() {
	if (mprToken == -1)
		return 1;

	return 0;
}

void mpr_AssignToken(int id) {
	plcs_NCP_AssignToken(id);
	mprToken = id;
}

void mpr_ReleaseToken() {
	mprToken = -1;
}

char plcs_NCP_AssignToken(uint16 nodeId) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_ASSIGN_TOKEN;
	payload[len++] = nodeId >> 8;
	payload[len++] = nodeId;

	resultLen = plcs_GetNCPMessage(tmp_Buff_MPR, nodeId, 0, payload, len);
	xcps_send_rs485ForMPR(tmp_Buff_MPR, resultLen);
	return 0;
}
