
#include <stdio.h>
#include <string.h>
#include "Xcps.h"
#include "Uart.h"


// protocol message format start/end flag.
#define	ZS_SFLAG	0xFA
#define	ZS_EFLAG	0xAF


/**
	temporary rx packet.
*/
uint8 xcps_rx_packet_zigbee[XCPS_MAX_PDU];

// 03/01. xcp zigbee uart transport receive state.
static int xcps_state_zigbee = 0;
int xcps_rxlen_zigbee = 0;							// total length received.
int xcps_pdu_len_zigbee = 0;

/**
	uart interface to use.
*/
static usart_getter	xcps_getter_zigbee = (usart_getter)0;
static usart_putter	xcps_putter_zigbee = (usart_putter)0;



/**

**/
int xcps_init_zigbee(usart_getter getter, usart_putter putter)
{
	xcps_getter_zigbee = getter;
	xcps_putter_zigbee = putter;

	
	return 0;
}

/**

*/
uint8 buffForSendZigbee[XCPS_MAX_PDU];
int xcps_send_zigbee(const uint8 *data, int length) {
	uint8 i = 0;
	for (i = 0; i < length; i++)
		xcps_putter_zigbee(data[i]);

	return 0;
}

int xcps_recv_zigbee(uint8 *buff, int buff_length) {
	uint8 temp;
	uint8 i = 0;
	uint8 len = 0;
	// check getter.
	if (!xcps_getter_zigbee)
		return -1;

	if (xcps_getter_zigbee(&temp) < 1)
		return 0;

	if (xcps_rxlen_zigbee >= buff_length)
		xcps_rxlen_zigbee = 0;

	if (temp == 0x00) return 0;

	if (temp == 0x0A) {
		xcps_rx_packet_zigbee[xcps_rxlen_zigbee++] = temp;

		for (i = 0; i < (xcps_rxlen_zigbee-2); i++) {
			if (xcps_rx_packet_zigbee[i]=='\\') {
				buff[len++] = toUint8(&(xcps_rx_packet_zigbee[i+1]));
				i+=2;
			} else
				buff[len++] = xcps_rx_packet_zigbee[i];
		}
		xcps_rxlen_zigbee=0;

		return len;
	} else {
		xcps_rx_packet_zigbee[xcps_rxlen_zigbee++] = temp;
	}
	return 0;
}

//end of file

