#ifndef __ZRM_PROTOCOL_H__
#define __ZRM_PROTOCOL_H__

#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>				// For Used printf_P(PSTR());
#include "Xcps.h"
#include "Util.h"

// ---------------------------------------------------------------------------------------------- //
#define MAX_TX_BUFF					90			// Buffer Size
#define ZRM_INIT					0
#define ZRM_INIT_OI					1
#define ZRM_INIT_SA					2
#define ZRM_INIT_LA					3


#define ZRM_CONFIG					20
#define ZRM_CONFIG_UF				21
#define ZRM_CONFIG_NT				22
#define ZRM_CONFIG_ZS				23
#define ZRM_CONFIG_PI				24
#define ZRM_CONFIG_RECEIVEINCMDMODE 25
#define ZRM_CONFIG_PJ				26
#define ZRM_CONFIG_CM			    28
#define ZRM_CONFIG_EI			    29
#define ZRM_CONFIG_EI_ATZ			30
#define ZRM_CONFIG_RETRY			31
#define ZRM_CONFIG_SOTIMEOUT		32
#define ZRM_CONFIG_SOTIMEOUT		32


#define ZRM_CONNECT					50
#define ZRM_WAIT_RESPONSE			51

#define SUCCESS 1
#define FAIL	0
#define WAIT	2

// ---------------------------------------------------------------------------------------------- //

typedef struct ZRMP_Info {
	uint8 zrm_State;
	uint16 zrm_Id;
	uint16 zrm_Panid;
	uint16 zrm_NetAddr;
} zrmp_Info;

zrmp_Info tmp_zrmp;

// --------------- Message Handler --------------- //
void ZRMPMessage(uint8 tmp_Buff[], int buff_length);

// --------------- Control Message --------------- //
void ZRMsendFR();
void ZRMsendBR();
void ZRMsendQuitDataMode();
void ZRMsendAT();
void ZRMsendUF();
void ZRMsendNT(uint8 nodeType);
void ZRMsendZS();
void ZRMsendPI(uint16 panId);
void ZRMsendCM(uint16 panId);
void ZRMsendPJ();
void ZRMsendATZ();
void ZRMreadLA();
void ZRMreadSA();
void ZRMsendDataMode();
void ZRMreadOI();
void ZRMsendRetry(uint8 cnt);
void ZRMsendSoTimeout(uint16 timeout);
void ZRMsendATS11();
void ZRMsendEI(uint16 panId);

void ZRMSendReset();

uint8 getZigbeeState();
void setZigbeeState(uint8 state);

uint8 getLastResult();

uint8 getMessageListSize();
void addMessageToSendBuff(uint8 *data, uint8 len);
uint8 getMessageFormSendBuff(uint8 *buff);
void sendQueueingMessage();

void sendToZigbeeLA(uint16 dst_Addr, uint8 msg[], int length);
void sendToZigbeeSA(uint16 dst_Addr, uint8 msg[], int length);
#endif

