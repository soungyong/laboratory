#include "ZRMProtocol.h"
#include "rotary.h"
#include <stdio.h>

uint8 tmp_BuffForZRM[MAX_TX_BUFF];
uint8 tmp_lenForZRM = 0;

#define PanIDEnable 1
uint8 zrmCount = 0;

uint8 lastResult = FAIL;
uint16 lastSendDestId = 0;

#define SendBuffSize 7
uint8 sendBuffForZRM_Data[SendBuffSize][MAX_TX_BUFF];
uint8 sendBuffForZRM_Len[SendBuffSize];
uint8 sendBuffForZRM_Head = 0;
uint8 sendBuffForZRM_Tail = 0;

uint8 getMessageFormSendBuff(uint8 *buff) {
	uint8 len = 0;
	if (getMessageListSize() > 0) {
		memcpy(buff, sendBuffForZRM_Data[sendBuffForZRM_Head],
				sendBuffForZRM_Len[sendBuffForZRM_Head]);
		len = sendBuffForZRM_Len[sendBuffForZRM_Head];
		sendBuffForZRM_Head += 1;
		if (sendBuffForZRM_Head >= SendBuffSize)
			sendBuffForZRM_Head = 0;
		return len;
	}
	return len;
}

void addMessageToSendBuff(uint8 *data, uint8 len) {
	if (getMessageListSize() >= SendBuffSize)
		return;

	memcpy(sendBuffForZRM_Data[sendBuffForZRM_Tail], data, len);
	sendBuffForZRM_Len[sendBuffForZRM_Tail] = (uint8) len;
	sendBuffForZRM_Tail += 1;
	if (sendBuffForZRM_Tail >= SendBuffSize)
		sendBuffForZRM_Tail = 0;
}

uint8 getMessageListSize() {
	if (sendBuffForZRM_Tail >= sendBuffForZRM_Head)
		return sendBuffForZRM_Tail - sendBuffForZRM_Head;
	else
		return SendBuffSize + sendBuffForZRM_Tail - sendBuffForZRM_Head;
}

void ZRMPMessage(uint8 buff[], int buff_length) {
	uint8 i = 0;

	if (buff[0] == 'O' && buff[1] == 'K') {
		lastResult = SUCCESS;
	} else if (strncmp(buff, "ERROR", 5) == 0)
		lastResult = FAIL;

	switch (getZigbeeState()) {
	case ZRM_INIT:
		break;
	case ZRM_INIT_OI:
		if (strncmp(buff, "at", 2) == 0) {
		} else if (strncmp(buff, "+000195000", 10) == 0) {
		} else if (buff_length == 4)
			tmp_zrmp.zrm_Panid = toUint16(buff);
		break;
	case ZRM_INIT_SA:
		if (strncmp(buff, "at", 2) == 0) {
		} else if (strncmp(buff, "+000195000", 10) == 0) {
		} else if (buff_length == 4)
			tmp_zrmp.zrm_NetAddr = toUint16(buff);
		break;
	case ZRM_INIT_LA:
		if (strncmp(buff, "at", 2) == 0) {
		} else if (strncmp(buff, "+000195000", 10) == 0) {
		} else if (buff_length == 16)
			tmp_zrmp.zrm_Id = toUint16(&(buff[12]));
		break;
	case ZRM_WAIT_RESPONSE:
		if (getLastResult() == SUCCESS) {
			setZigbeeState(ZRM_CONNECT);
			zrmCount = 0;
		} else if (getLastResult() == FAIL) {
			if (lastSendDestId != 0) {
			}
			setZigbeeState(ZRM_CONNECT);
		}
		break;

	default:
		break;
	}
}

uint8 getZigbeeState() {
	return tmp_zrmp.zrm_State;
}

void setZigbeeState(uint8 state) {
	tmp_zrmp.zrm_State = state;
}

void ZRMsendFR() {
	lastResult = WAIT;
	xcps_send_zigbee("at&f\n", 5);
}

void ZRMsendBR() {
	lastResult = WAIT;
	xcps_send_zigbee("at+ub=115200\n", 13);
}
uint8 flagForQuitDataMode = 0;
void ZRMsendQuitDataMode() {
	lastResult = WAIT;
	if (flagForQuitDataMode == 0) {
		xcps_send_zigbee("+++", 3);
		flagForQuitDataMode = 1;
	} else {
		xcps_send_zigbee("+++\n", 4);
		flagForQuitDataMode = 0;
	}
}
void ZRMsendAT() {
	lastResult = WAIT;
	xcps_send_zigbee("at\n", 3);
}
void ZRMsendUF() {
	xcps_send_zigbee("at+uf=0\n", 8);
}
void ZRMsendNT(uint8 nodeType) {
	lastResult = WAIT;
	if (nodeType == 1)
		xcps_send_zigbee("at+nt=1\n", 8);
	if (nodeType == 2)
		xcps_send_zigbee("at+nt=2\n", 8);
	if (nodeType == 3)
		xcps_send_zigbee("at+nt=3\n", 8);
}
void ZRMsendZS() {
	lastResult = WAIT;
	xcps_send_zigbee("at+zs=1\n", 8);
}
void ZRMsendPI(uint16 panId) {
	uint8 temp[20];
	uint8 tempLen;
	tempLen = sprintf(temp, "at+pi=%04x\n", panId);
	lastResult = WAIT;
	xcps_send_zigbee(temp, tempLen);
}

void ZRMsendEI(uint16 panId) {
	uint8 temp[20];
	uint8 tempLen;
	tempLen = sprintf(temp, "at+ei=000000000000%04x\n", panId);
	lastResult = WAIT;
	xcps_send_zigbee(temp, tempLen);
}

void ZRMsendCM(uint16 panId) {
	uint8 temp[20];
	uint8 tempLen;
	tempLen = sprintf(temp, "at+cm=07FFF800\n");
	lastResult = WAIT;
	xcps_send_zigbee(temp, tempLen);
}
void ZRMreadOI() {
	uint8 temp[20];
	uint8 tempLen;
	tempLen = sprintf(temp, "at+oi\n");
	lastResult = WAIT;
	xcps_send_zigbee(temp, tempLen);
}
void ZRMsendRetry(uint8 cnt) {
	uint8 temp[20];
	uint8 tempLen;
	tempLen = sprintf(temp, "ats32=%d\n", cnt);
	lastResult = WAIT;
	xcps_send_zigbee(temp, tempLen);
}
void ZRMsendSoTimeout(uint16 timeout) {
	uint8 temp[20];
	uint8 tempLen;
	tempLen = sprintf(temp, "ats33=%d\n", timeout);
	lastResult = WAIT;
	xcps_send_zigbee(temp, tempLen);
}
void ZRMsendATS11() {
	uint8 temp[20];
	uint8 tempLen;
	tempLen = sprintf(temp, "ats11=1\n");
	lastResult = WAIT;
	xcps_send_zigbee(temp, tempLen);
}
void ZRMsendPJ() {
	lastResult = WAIT;
	xcps_send_zigbee("at+pj=255\n", 10);
}
void ZRMsendATZ() {
	lastResult = WAIT;
	xcps_send_zigbee("atz\n", 4);
}
void ZRMsendLeave() {
	lastResult = WAIT;
	xcps_send_zigbee("at+leave\n", 9);
}
void ZRMreadLA() {
	lastResult = WAIT;
	xcps_send_zigbee("at+la\n", 6);
}
void ZRMreadSA() {
	lastResult = WAIT;
	xcps_send_zigbee("at+sa\n", 6);
}
void ZRMsendDataMode(uint16 groupId) {
	uint8 temp[20];
	uint8 tempLen;
	tempLen = sprintf(temp, "atm%04x\n", groupId);
	lastResult = WAIT;
	xcps_send_zigbee(temp, tempLen);
}

void sendToZigbeeSA(uint16 dstId, uint8 msg[], int length) {
	uint8 i = 0;
	uint8 j = 0;
	for (i = 0; i < length; i += 40) {
		tmp_lenForZRM = 0;
		tmp_BuffForZRM[tmp_lenForZRM++] = msg[3];
		tmp_BuffForZRM[tmp_lenForZRM++] = length / 40 + 1;
		tmp_BuffForZRM[tmp_lenForZRM++] = i / 40;

		for (j = i; tmp_lenForZRM < 43 && j < length; j++) {
			tmp_BuffForZRM[tmp_lenForZRM++] = msg[j];
		}

		addMessageToSendBuff(tmp_BuffForZRM, tmp_lenForZRM);
	}
}
void sendToZigbee(uint16 dst_Addr, uint8 msg[], int length) {
	int i = 0;

	if (getZigbeeState() != ZRM_CONNECT)
		return;

	tmp_lenForZRM = 0;
	tmp_lenForZRM = sprintf(tmp_BuffForZRM, "at+uc=%04x,", dst_Addr);
	for (i = 0; i < length; i++) {
		tmp_BuffForZRM[tmp_lenForZRM++] = toHexCharacterOfLowerByte(
				msg[i] >> 4);
		tmp_BuffForZRM[tmp_lenForZRM++] = toHexCharacterOfLowerByte(msg[i]);
	}
	tmp_BuffForZRM[tmp_lenForZRM++] = 0x0d;
	tmp_BuffForZRM[tmp_lenForZRM++] = 0x0a;

	lastResult = WAIT;

	setZigbeeState(ZRM_WAIT_RESPONSE);
	xcps_send_zigbee(tmp_BuffForZRM, tmp_lenForZRM);
}

void sendQueueingMessage() {
	static uint8 sendFailCnt = 0;
	static uint8 buff[MAX_TX_BUFF];
	static uint8 len;
	static uint8 flag=0;

	if(flag>0)
		flag++;

	if(flag > 100){
		flag=1;
		if(getZigbeeState()==ZRM_WAIT_RESPONSE)
			setZigbeeState(ZRM_CONNECT);
	}

	if (getMessageListSize()
			> 0 && getZigbeeState()==ZRM_CONNECT) {
		flag=1;
		if (getLastResult() == SUCCESS)
			sendFailCnt = 0;
		else if (getLastResult() == FAIL)
			sendFailCnt++;

		if (sendFailCnt > 30) {
			ZRMsendATZ();
			setZigbeeState(ZRM_INIT);
			sendFailCnt = 0;
		}

		len = getMessageFormSendBuff(buff);
		if (len > 0) {
			sendToZigbee(0, buff, len);
			len = 0;
		}
	}
}

uint8 getLastResult() {
	return lastResult;
}
//end of file

