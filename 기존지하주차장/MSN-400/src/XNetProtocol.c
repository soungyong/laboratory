#include "XNetProtocol.h"
#include "NC_Protocol.h"
#include "MSN_Protocol.h"

// --------------------------------------------------------------------------- //
uint8 tmp_Buff2[64];
// --------------------------------------------------------------------------- //

uint8 XNetHandler(uint8 buff[], int buff_length) {
	uint8 pid;
	uint8 subPid;
	uint16 seqNum;
	uint16 srcId;
	uint16 dstId;
	uint8 i = 0;
	uint8 len = 0;

	if (buff[0] != '+')
		return 0;
	if (buff_length <= 17)
		return 0;
	if (strncmp(buff, "+000195000000", 13) != 0)
		return 0;

	srcId = toUint16(&(buff[13]));

	uint8 buff2[(buff_length - 18) / 2];

	for (i = 24; i < buff_length; i += 2)
		buff2[len++] = toUint8(&(buff[i]));

	pid = buff2[0];
	subPid = buff2[1];
	seqNum = buff2[2] << 8 | buff2[3];
	dstId = buff2[4] << 8 | buff2[5];

	switch (pid) {
	case NCP_PROTOCOL_ID: {
		switch (subPid) {
		case PLCS_NCP_PROTOCOL_ID:
			NCP_ProcessMessageFromZigbee(srcId, dstId, buff2, len);
			break;
		case PLCS_GC_PROTOCOL_ID:
			GCP_ProcessMessageFromZigbee(srcId, dstId, buff2, len);
			break;
		}
	}
		break;
	default:
		break;
	}

	return 1;
}

//end of file

