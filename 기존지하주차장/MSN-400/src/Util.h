#ifndef __UTIL_H__
#define __UTIL_H__

#include	<stdio.h>
#include	"util/delay.h"


//#define DEBUG_ENABLE

#define		MSLEEP(x)		_delay_ms(x)
#define		USLEEP(x)		_delay_us(x)


//------------------------------------------------------------------------//
//-------------------------- Timer ID -------------------------------------//
//------------------------------------------------------------------------//

#define ADC_TIMER_ID				1
#define ZIGBEE_CONFIG_ID			2
#define ON_WTD_TIMER_ID				3
#define NCP_TIMER_ID 				4
#define PANID_TIMER_ID 				5
#define SENSOR_DEBUG_MODE_TIMER_ID 	6
#define SENSOR_10ms_TIMER_ID 		7
#define TEST_LAMP_ID 				8
#define ADC2_TIMER_ID 				9
#define CONNECTION_TIMEOUT_ID		10
#define ZIGBEE_SEND_ID				11




//------------------------------------------------------------------------//
//-------------------------- Timer Count -----------------------------------//
//------------------------------------------------------------------------//
#define TIME_COUNT				1			// per 1min's


#define PING_TIME					60



//------------------------------------------------------------------------//
//-------------------------- EEPROM ID ------------------------------------//
//------------------------------------------------------------------------//
#define FIRMWARE_UPGRADE_FLAG_VALUE				100




typedef unsigned char uint8;
typedef unsigned int uint16;
typedef unsigned long uint32;

uint16 toUint16(uint8 hexString[4]);
uint8 toUint8(uint8 hexString[2]);
uint8 toHexCharacterOfLowerByte(uint8 ch);

#endif



