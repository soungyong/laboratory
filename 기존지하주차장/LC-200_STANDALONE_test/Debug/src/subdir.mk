################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/DeviceInfo.c \
../src/DimmerProtocol.c \
../src/Flash.c \
../src/MPRProtocol.c \
../src/MappingTable.c \
../src/PLCS_Protocol.c \
../src/PatternBasedControl.c \
../src/RFU_Protocol.c \
../src/SSRContol.c \
../src/ScheduleTable.c \
../src/Timer.c \
../src/Uart.c \
../src/XNetProtocol.c \
../src/Xcps.c \
../src/ZRMProtocol.c \
../src/debug.c \
../src/main.c \
../src/rotary.c \
../src/util.c 

OBJS += \
./src/DeviceInfo.o \
./src/DimmerProtocol.o \
./src/Flash.o \
./src/MPRProtocol.o \
./src/MappingTable.o \
./src/PLCS_Protocol.o \
./src/PatternBasedControl.o \
./src/RFU_Protocol.o \
./src/SSRContol.o \
./src/ScheduleTable.o \
./src/Timer.o \
./src/Uart.o \
./src/XNetProtocol.o \
./src/Xcps.o \
./src/ZRMProtocol.o \
./src/debug.o \
./src/main.o \
./src/rotary.o \
./src/util.o 

C_DEPS += \
./src/DeviceInfo.d \
./src/DimmerProtocol.d \
./src/Flash.d \
./src/MPRProtocol.d \
./src/MappingTable.d \
./src/PLCS_Protocol.d \
./src/PatternBasedControl.d \
./src/RFU_Protocol.d \
./src/SSRContol.d \
./src/ScheduleTable.d \
./src/Timer.d \
./src/Uart.d \
./src/XNetProtocol.d \
./src/Xcps.d \
./src/ZRMProtocol.d \
./src/debug.d \
./src/main.d \
./src/rotary.d \
./src/util.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega2560 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


