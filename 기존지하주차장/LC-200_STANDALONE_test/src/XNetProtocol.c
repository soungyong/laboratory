#include "XNetProtocol.h"
#include "RFU_Protocol.h"

// --------------------------------------------------------------------------- //
#define MaxMessageListSize 50
uint8 sendMessageList[MaxMessageListSize][XCPS_MAX_PDU];
uint8 messageLength[MaxMessageListSize];
uint8 messageList_Tail = 0;
uint8 messageList_Head = 0;

uint8 getMessageListSize() {
	if (messageList_Tail >= messageList_Head)
		return messageList_Tail - messageList_Head;
	else
		return MaxMessageListSize + messageList_Tail - messageList_Head;
}

#define MaxMPRMessageListSize 10
uint8 sendMPRMessageList[MaxMPRMessageListSize][XCPS_MAX_PDU];
uint8 mprMessageLength[MaxMPRMessageListSize];
uint8 mprMessageList_Tail = 0;
uint8 mprMessageList_Head = 0;

uint8 getMPRMessageListSize() {
	if (mprMessageList_Tail >= mprMessageList_Head)
		return mprMessageList_Tail - mprMessageList_Head;
	else
		return MaxMPRMessageListSize + mprMessageList_Tail - mprMessageList_Head;
}
// --------------------------------------------------------------------------- //

void XNetHandlerFromZigbee(uint16 srcNetAddr, uint8 msg[], int buff_length) {
	uint8 pid;
	uint8 subPid;
	uint16 seqNum;
	uint16 srcId;
	uint16 destId;
	device_Info *deviceInfo_P = NULL;

	pid = msg[0];
	subPid = msg[1];
	seqNum = msg[2] << 8 | msg[3];
	destId = msg[4] << 8 | msg[5];
	srcId = msg[6] << 8 | msg[7];

	if (srcId != 0) {
		deviceInfo_P = findNodeById(srcId);
		if (deviceInfo_P != NULL) {
			deviceInfo_P->netAddr = srcNetAddr;
			deviceInfo_P->cnt = 0;
			deviceInfo_P->connectionState = ONLINE;
		} else
			addDeviceTable(srcId, 0, 0, 0, srcNetAddr);
	}

	switch (pid) {
	case NCP_PROTOCOL_ID: {
		switch (subPid) {
		case PLCS_NCP_PROTOCOL_ID:
			DEBUG("\nGET NCP!!");
			NCP_ProcessMessage(ZIGBEE, seqNum, srcId, destId, msg, buff_length);
			break;
		case PLCS_GCP_PROTOCOL_ID:
			DEBUG("\nGET GCP!!");
			plcs_GCP_ProcessMessage(ZIGBEE, seqNum, srcId, destId, msg, buff_length);
			break;
		case PLCS_RFU_PROTOCOL_ID:
			plcs_RFUP_ProcessMessage(ZIGBEE, seqNum, srcId, destId, msg, buff_length);
			break;
		}
	}
		break;
	default:
		break;
	}

}

void XNetCommandFromGateway(uint8 msg[], int buff_length) {
	uint8 pid;
	uint8 subPid;
	uint16 subNodeId;
	uint16 seqNum;
	uint16 srcId;
	uint16 destId;

	pid = msg[0];
	subPid = msg[1];
	seqNum = msg[2] << 8 | msg[3];
	destId = msg[4] << 8 | msg[5];
	srcId = msg[6] << 8 | msg[7];

	switch (pid) {
	case NCP_PROTOCOL_ID: {
		switch (subPid) {
		case PLCS_NCP_PROTOCOL_ID:
			if (destId == zrmpInfo_0.zrm_Id || destId == 0xffff)
				NCP_ProcessMessage(GATEWAY, seqNum, srcId, destId, msg, buff_length);
			break;
		case PLCS_GCP_PROTOCOL_ID:
			//subNodeId 확인
			subNodeId = msg[9] << 8 | msg[10];
			if (subNodeId == zrmpInfo_0.zrm_Id)
				plcs_GCP_ProcessMessage(GATEWAY, seqNum, srcId, destId, msg, buff_length);
			break;
		case PLCS_RFU_PROTOCOL_ID:
			subNodeId = msg[11] << 8 | msg[12];
			if (subNodeId == zrmpInfo_0.zrm_Id)
				plcs_RFUP_ProcessMessage(GATEWAY, seqNum, srcId, destId, msg, buff_length);
			break;
		}
	}
		break;
	default:
		break;
	}
}

void XNetCommandFromMPR(uint8 msg[], int buff_length) {
	uint8 pid;
	uint8 subPid;
	uint16 subNodeId;
	uint16 seqNum;
	uint16 srcId;
	uint16 destId;

	pid = msg[0];
	subPid = msg[1];
	seqNum = msg[2] << 8 | msg[3];
	destId = msg[4] << 8 | msg[5];
	srcId = msg[6] << 8 | msg[7];

	switch (pid) {
	case NCP_PROTOCOL_ID: {
		switch (subPid) {
		case PLCS_NCP_PROTOCOL_ID:
			NCP_ProcessMessage(MPR, seqNum, srcId, destId, msg, buff_length);
			break;
		case PLCS_GCP_PROTOCOL_ID:
			//subNodeId 확인
			plcs_GCP_ProcessMessage(MPR, seqNum, srcId, destId, msg, buff_length);
			break;
		case PLCS_RFU_PROTOCOL_ID:
			plcs_RFUP_ProcessMessage(MPR, seqNum, srcId, destId, msg, buff_length);
			break;
		}
	}
		break;
	default:
		break;
	}
}

char sendStackedServerMessage() {
	if (getMessageListSize() > 0) {
		messageList_Head += 1;
		xcps_send_rs485(sendMessageList[messageList_Head - 1], messageLength[messageList_Head - 1]);
		if (messageList_Head >= MaxMessageListSize)
			messageList_Head = 0;
		return 1;
	}
	return 0;
}

char sendStackedMPRMessage() {
	if (getMPRMessageListSize() > 0) {
		mprMessageList_Head += 1;
		xcps_send_rs485ForMPR(sendMPRMessageList[mprMessageList_Head - 1], mprMessageLength[mprMessageList_Head - 1]);
		if (mprMessageList_Head >= MaxMPRMessageListSize)
			mprMessageList_Head = 0;
		return 1;
	}
	return 0;
}

void sendServerMessage(uint8 *msg, uint8 buff_length) {
	if (getMessageListSize() >= (MaxMessageListSize - 2)) {
		return;
	}

	memcpy(sendMessageList[messageList_Tail], msg, buff_length);
	messageLength[messageList_Tail] = (uint8) buff_length;
	messageList_Tail += 1;
	if (messageList_Tail >= MaxMessageListSize)
		messageList_Tail = 0;
}

void sendMPRMessage(uint8 *msg, uint8 buff_length) {
	if (getMPRMessageListSize() >= (MaxMPRMessageListSize - 2)) {
		return;
	}

	memcpy(sendMPRMessageList[mprMessageList_Tail], msg, buff_length);
	mprMessageLength[mprMessageList_Tail] = (uint8) buff_length;
	mprMessageList_Tail += 1;
	if (mprMessageList_Tail >= MaxMPRMessageListSize)
		mprMessageList_Tail = 0;
}

void sendServerMessageWithoutQueue(uint8 *msg, int buff_length) {
	xcps_send_rs485(msg, buff_length);
}

void sendMessage(uint8 dstNodeType, uint16 dstId, uint8 msg[], uint8 length) {
	uint8 temp[4];
	temp[0] = msg[4];
	temp[1] = msg[5];
	temp[2] = msg[6];
	temp[3] = msg[7];

	msg[4] = dstId >> 8;
	msg[5] = dstId;
	msg[6] = zrmpInfo_0.zrm_Id >> 8;
	msg[7] = zrmpInfo_0.zrm_Id;

	if (dstNodeType == GATEWAY) {
		sendServerMessage(msg, length);
	} else if (dstNodeType == MPR) {
		sendMPRMessage(msg, length);
	} else {
		device_Info* deviceInfo_P = NULL;
		deviceInfo_P = findNodeById(dstId);
//		if (deviceInfo_P != NULL) {
//			if (deviceInfo_P->deviceType == PLCS_ZDIMMER_TYPE)
//				sendToZigbee(ZIGBEE_1, deviceInfo_P->netAddr, msg, length);
//			else if (deviceInfo_P->deviceType == 0) {
//				sendToZigbee(ZIGBEE_0, deviceInfo_P->netAddr, msg, length);
//				sendToZigbee(ZIGBEE_1, deviceInfo_P->netAddr, msg, length);
//			} else {
//				sendToZigbee(ZIGBEE_0, deviceInfo_P->netAddr, msg, length);
//			}
//		}

		if (deviceInfo_P != NULL) {
			// 일단 센서부터.
			if (deviceInfo_P->deviceType == PLCS_ZSENSOR_TYPE) {
				DEBUG("\n   sendToZIgbee 1");
				sendToZigbee(ZIGBEE_1, deviceInfo_P->netAddr, msg, length);
			} else {
				sendToZigbee(ZIGBEE_0, deviceInfo_P->netAddr, msg, length);
				sendToZigbee(ZIGBEE_1, deviceInfo_P->netAddr, msg, length);
			}

//			if (deviceInfo_P->deviceType == PLCS_ZDIMMER_TYPE) {
//				sendToZigbee(ZIGBEE_1, deviceInfo_P->netAddr, msg, length);
//			} else if (deviceInfo_P->deviceType == 0) {
//				sendToZigbee(ZIGBEE_0, deviceInfo_P->netAddr, msg, length);
//				sendToZigbee(ZIGBEE_1, deviceInfo_P->netAddr, msg, length);
//			} else {
//				sendToZigbee(ZIGBEE_0, deviceInfo_P->netAddr, msg, length);
//			}
		}
	}

	msg[4] = temp[0];
	msg[5] = temp[1];
	msg[6] = temp[2];
	msg[7] = temp[3];
}

//end of file

