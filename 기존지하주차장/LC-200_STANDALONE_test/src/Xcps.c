#include <stdio.h>
#include <string.h>
#include "Xcps.h"
#include "Uart.h"
#include "Util.h"

// protocol message format start/end flag.
#define	ZS_SFLAG	0xFA
#define	ZS_EFLAG	0xAF

/**
 temporary rx packet.
 */
uint8 xcps_rx_packet_zigbee_0[XCPS_MAX_PDU];
uint8 xcps_rx_packet_zigbee_1[XCPS_MAX_PDU];

uint8 xcps_tx_packet_zigbee_0[XCPS_MAX_PDU];
uint8 xcps_tx_packet_zigbee_1[XCPS_MAX_PDU];

uint8 xcps_rx_packet_rs485[XCPS_MAX_PDU];

uint8 xcps_rx_packet_rs485ForMPR[XCPS_MAX_PDU];

// 03/01. xcp zigbee uart transport receive state.
int xcps_rxlen_zigbee_0 = 0; // total length received.
int xcps_rxlen_zigbee_1 = 0; // total length received.

static int xcps_state_rs485 = 0;
int xcps_rxlen_rs485 = 0; // total length received.
int xcps_pdu_len_rs485 = 0; // packet payload length.

static int xcps_state_rs485ForMPR = 0;
int xcps_rxlen_rs485ForMPR = 0; // total length received.
int xcps_pdu_len_rs485ForMPR = 0; // packet payload length.

/**
 uart interface to use.
 */
/*
 static usart_TxEnabler xcps_TxEnabler_zigbee = (usart_TxEnabler) 0;
 static usart_TxDisabler xcps_TxDisabler_zigbee = (usart_TxDisabler) 0;

 static usart_TxEnabler xcps_TxEnabler_rs485 = (usart_TxEnabler) 0;
 static usart_TxDisabler xcps_TxDisabler_rs485 = (usart_TxDisabler) 0;
 */

static usart_getter xcps_getter_zigbee_0 = (usart_getter) 0;
static usart_putter xcps_putter_zigbee_0 = (usart_putter) 0;

static usart_getter xcps_getter_zigbee_1 = (usart_getter) 0;
static usart_putter xcps_putter_zigbee_1 = (usart_putter) 0;

static usart_getter xcps_getter_rs485 = (usart_getter) 0;
static usart_putter xcps_putter_rs485 = (usart_putter) 0;

static usart_getter xcps_getter_rs485ForMPR = (usart_getter) 0;
static usart_putter xcps_putter_rs485ForMPR = (usart_putter) 0;

/**

 **/
uint8 xcps_init_zigbee_0(usart_getter getter, usart_putter putter) {
	xcps_getter_zigbee_0 = getter;
	xcps_putter_zigbee_0 = putter;

	return 0;
}

uint8 xcps_init_zigbee_1(usart_getter getter, usart_putter putter) {
	xcps_getter_zigbee_1 = getter;
	xcps_putter_zigbee_1 = putter;

	return 0;
}

uint8 xcps_init_rs485(usart_getter getter, usart_putter putter) {
	xcps_getter_rs485 = getter;
	xcps_putter_rs485 = putter;

	//xcps_TxDisabler_rs485();

	return 0;
}

uint8 xcps_init_rs485ForMPR(usart_getter getter, usart_putter putter) {
	xcps_getter_rs485ForMPR = getter;
	xcps_putter_rs485ForMPR = putter;

	//xcps_TxDisabler_rs485();

	return 0;
}

/**

 */
uint8 xcps_send_zigbee_0(const uint8 *data, uint8 length) {
	uint8 msgLen2 = 0;
	uint8 i = 0;
	uint8 checkSum = 0;

	// check putter.
	if (!xcps_putter_zigbee_0)
		return -1;

	// calc checksum.
	for (i = 0; i < length; i++)
		checkSum += ((uint8) data[i] & 0xFF);

	xcps_tx_packet_zigbee_0[msgLen2++] = ZS_SFLAG;
	xcps_tx_packet_zigbee_0[msgLen2++] = length;

	for (i = 0; i < length; i++)
		xcps_tx_packet_zigbee_0[msgLen2++] = data[i];
	xcps_tx_packet_zigbee_0[msgLen2++] = checkSum;

	for (i = (length + 4) % 8; i < 8 && i > 0; i++)
		xcps_tx_packet_zigbee_0[msgLen2++] = ZS_EFLAG;

	xcps_tx_packet_zigbee_0[msgLen2++] = ZS_EFLAG;

	for (i = 0; i < msgLen2; i++)
		xcps_putter_zigbee_0(xcps_tx_packet_zigbee_0[i]);

	return 0;
}

uint8 xcps_recv_zigbee_0(uint8 *buff, uint8 buff_length) {
	uint8 temp;
	int i;
	uint8 checkSum = 0;

	// check getter.
	if (!xcps_getter_zigbee_0)
		return -1;

	if (xcps_getter_zigbee_0(&temp) < 1)
		return 0;

	if (xcps_rxlen_zigbee_0 >= buff_length)
		xcps_rxlen_zigbee_0 = 0;

	xcps_rx_packet_zigbee_0[xcps_rxlen_zigbee_0++] = temp;

	if (temp == ZS_SFLAG) {
		if (xcps_rx_packet_zigbee_0[0] != ZS_SFLAG) {
			xcps_rxlen_zigbee_0 = 0;
			xcps_rx_packet_zigbee_0[xcps_rxlen_zigbee_0++] = temp;
			return 0;
		}
	} else if (temp == ZS_EFLAG) {
		if (xcps_rxlen_zigbee_0 < 6)
			return 0;
		if (xcps_rxlen_zigbee_0 - 4 < xcps_rx_packet_zigbee_0[1])
			return 0;
		if (xcps_rxlen_zigbee_0 - 4 > xcps_rx_packet_zigbee_0[1]) {
			xcps_rxlen_zigbee_0 = 0;
			return 0;
		}

		uint8 resultLen = 0;
		checkSum = 0;

		//xcps_rx_packet_zigbee contains fa~af.

		//length.
		if (xcps_rxlen_zigbee_0 - 4 != xcps_rx_packet_zigbee_0[1]) {
			xcps_rxlen_zigbee_0 = 0;
			return 0;
		}

		//checksum
		for (i = 2; i < xcps_rxlen_zigbee_0 - 2; i++)
			checkSum += ((uint8) xcps_rx_packet_zigbee_0[i] & 0xFF);

		if (checkSum != xcps_rx_packet_zigbee_0[xcps_rxlen_zigbee_0 - 2])
			return 0;

		for (i = 2; i < xcps_rxlen_zigbee_0 - 2; i++) {
			buff[resultLen++] = xcps_rx_packet_zigbee_0[i];
		}
		xcps_rxlen_zigbee_0 = 0;

		return resultLen;
	}

	return 0;
}

uint8 xcps_send_zigbee_1(const uint8 *data, uint8 length) {

	USART3_TxEnable();
	uint8 msgLen2 = 0;
	uint8 i = 0;
	uint8 checkSum = 0;

	// check putter.
	if (!xcps_putter_zigbee_1)
		return -1;

	// calc checksum.
	for (i = 0; i < length; i++)
		checkSum += ((uint8) data[i] & 0xFF);

	xcps_tx_packet_zigbee_1[msgLen2++] = ZS_SFLAG;
	xcps_tx_packet_zigbee_1[msgLen2++] = length;

	for (i = 0; i < length; i++)
		xcps_tx_packet_zigbee_1[msgLen2++] = data[i];
	xcps_tx_packet_zigbee_1[msgLen2++] = checkSum;

	for (i = (length + 4) % 8; i < 8 && i > 0; i++)
		xcps_tx_packet_zigbee_1[msgLen2++] = ZS_EFLAG;

	xcps_tx_packet_zigbee_1[msgLen2++] = ZS_EFLAG;

	for (i = 0; i < msgLen2; i++)
		xcps_putter_zigbee_1(xcps_tx_packet_zigbee_1[i]);
	return 0;
}

uint8 xcps_recv_zigbee_1(uint8 *buff, uint8 buff_length) {
	uint8 temp;
	int i;
	uint8 checkSum = 0;

	// check getter.
	if (!xcps_getter_zigbee_1)
		return -1;

	if (xcps_getter_zigbee_1(&temp) < 1)
		return 0;


	if (xcps_rxlen_zigbee_1 >= buff_length)
		xcps_rxlen_zigbee_1 = 0;

	xcps_rx_packet_zigbee_1[xcps_rxlen_zigbee_1++] = temp;

	if (temp == ZS_SFLAG) {
		if (xcps_rx_packet_zigbee_1[0] != ZS_SFLAG) {
			xcps_rxlen_zigbee_1 = 0;
			xcps_rx_packet_zigbee_1[xcps_rxlen_zigbee_1++] = temp;
			return 0;
		}
	} else if (temp == ZS_EFLAG) {
		if (xcps_rxlen_zigbee_1 < 6)
			return 0;
		if (xcps_rxlen_zigbee_1 - 4 < xcps_rx_packet_zigbee_1[1])
			return 0;
		if (xcps_rxlen_zigbee_1 - 4 > xcps_rx_packet_zigbee_1[1]) {
			xcps_rxlen_zigbee_1 = 0;
			return 0;
		}

		uint8 resultLen = 0;
		checkSum = 0;

		//xcps_rx_packet_zigbee contains fa~af.

		//length.
		if (xcps_rxlen_zigbee_1 - 4 != xcps_rx_packet_zigbee_1[1]) {
			xcps_rxlen_zigbee_1 = 0;
			return 0;
		}

		//checksum
		for (i = 2; i < xcps_rxlen_zigbee_1 - 2; i++)
			checkSum += ((uint8) xcps_rx_packet_zigbee_1[i] & 0xFF);

		if (checkSum != xcps_rx_packet_zigbee_1[xcps_rxlen_zigbee_1 - 2])
			return 0;

		for (i = 2; i < xcps_rxlen_zigbee_1 - 2; i++) {
			buff[resultLen++] = xcps_rx_packet_zigbee_1[i];
		}
		xcps_rxlen_zigbee_1 = 0;

		return resultLen;
	}

	return 0;
}

// -------------------------------------------------------------------------------------------------------------------------------------- //
// -------------------------------------------------------------------------------------------------------------------------------------- //
// -------------------------------------------------------------------------------------------------------------------------------------- //
uint8 xcps_send_rs485(const uint8 *data, uint8 length) {
	uint8 buff[XCPS_MAX_PDU];
	int i, msgLen = 0;
	uint8 checkSum = 0;

	// check putter.
	if (!xcps_putter_rs485)
		return -1;

	buff[msgLen++] = ZS_SFLAG; //Start Byte_1
	buff[msgLen++] = (uint8) (length);

	for (i = 0; i < length; i++)
		buff[msgLen++] = data[i];

	// calc checksum.
	for (i = 2; i < msgLen; i++)
		checkSum += ((uint8) buff[i] & 0xFF);

	buff[msgLen++] = (uint8) (checkSum & 0xFF);

	// end flag.
	buff[msgLen++] = ZS_EFLAG;

	USART1_TxEnable();
	MSLEEP(3);
	for (i = 0; i < msgLen; i++)
		xcps_putter_rs485(buff[i]);
	MSLEEP(3);
	USART1_TxDisable();

	return 0;
}

/**

 */
uint8 xcps_recv_rs485(uint8 *buff, uint8 buff_length) {
	uint8 temp;
	int i;
	uint8 checkSum = 0;

	// check getter.
	if (!xcps_getter_rs485)
		return -1;

	if (xcps_getter_rs485(&temp) < 1)
		return 0;

	//xcps_putter_rs485(temp);
	if (xcps_rxlen_rs485 >= buff_length) {
		xcps_state_rs485 = 0;
	}

	switch (xcps_state_rs485) {
	case 0:
		xcps_rxlen_rs485 = 0;

		if (temp == ZS_SFLAG) {
			xcps_state_rs485 = 1;
			xcps_rx_packet_rs485[xcps_rxlen_rs485++] = temp;
		}

		else if (temp == ZS_EFLAG) {
			// don't change state, keep find start flag.
		}
		break;

	case 1: // found start flag
		xcps_rx_packet_rs485[xcps_rxlen_rs485++] = temp;
		xcps_state_rs485 = 2;
		xcps_pdu_len_rs485 = temp;
		break;

	case 2: // found length
		// fill data.
		xcps_rx_packet_rs485[xcps_rxlen_rs485++] = temp;

		// check length.
		if (xcps_rxlen_rs485 >= (2 + xcps_pdu_len_rs485))
			xcps_state_rs485 = 3;

		if (xcps_rx_packet_rs485[2] != 0x80)
			xcps_state_rs485 = 0;

		break;

	case 3: // data end, check checksum.
		for (i = 2; i < (2 + xcps_pdu_len_rs485); i++)
			checkSum += (xcps_rx_packet_rs485[i] & 0xFF);

		if (temp == (uint8) checkSum) // Checksum ok.
			xcps_state_rs485 = 4;

		else
			xcps_state_rs485 = 0;
		break;

	case 4:
		if (temp == ZS_EFLAG) {
			xcps_state_rs485 = 0;

			// return data to caller.
			for (i = 0; i < xcps_pdu_len_rs485; i++)
				buff[i] = xcps_rx_packet_rs485[2 + i];

			return xcps_pdu_len_rs485;
		} else {
			// TODO:
			xcps_state_rs485 = 0;
		}
		break;
	default:
		// if you here, something wrong. --> recover to state 0.
		xcps_state_rs485 = 0;
		break;
	}
	return 0;
}

uint8 xcps_send_rs485ForMPR(const uint8 *data, uint8 length) {
	uint8 buff[XCPS_MAX_PDU];
	int i, msgLen = 0;
	uint8 checkSum = 0;

	// check putter.
	if (!xcps_putter_rs485ForMPR)
		return -1;

	buff[msgLen++] = ZS_SFLAG; //Start Byte_1
	buff[msgLen++] = (uint8) (length);

	for (i = 0; i < length; i++)
		buff[msgLen++] = data[i];

	// calc checksum.
	for (i = 2; i < msgLen; i++)
		checkSum += ((uint8) buff[i] & 0xFF);

	buff[msgLen++] = (uint8) (checkSum & 0xFF);

	// end flag.
	buff[msgLen++] = ZS_EFLAG;

	USART2_TxEnable();
	MSLEEP(3);
	for (i = 0; i < msgLen; i++)
		xcps_putter_rs485ForMPR(buff[i]);
	MSLEEP(3);
	USART2_TxDisable();

	return 0;
}

uint8 xcps_recv_rs485ForMPR(uint8 *buff, uint8 buff_length) {
	uint8 temp;
	int i;
	uint8 checkSum = 0;

	// check getter.
	if (!xcps_getter_rs485ForMPR)
		return -1;

	if (xcps_getter_rs485ForMPR(&temp) < 1)
		return 0;

	//xcps_putter_rs485(temp);
	if (xcps_rxlen_rs485ForMPR >= buff_length) {
		xcps_state_rs485ForMPR = 0;
	}

	switch (xcps_state_rs485ForMPR) {
	case 0:
		xcps_rxlen_rs485ForMPR = 0;

		if (temp == ZS_SFLAG) {
			xcps_state_rs485ForMPR = 1;
			xcps_rx_packet_rs485ForMPR[xcps_rxlen_rs485ForMPR++] = temp;
		}

		else if (temp == ZS_EFLAG) {
			// don't change state, keep find start flag.
		}
		break;

	case 1: // found start flag
		xcps_rx_packet_rs485ForMPR[xcps_rxlen_rs485ForMPR++] = temp;
		xcps_state_rs485ForMPR = 2;
		xcps_pdu_len_rs485ForMPR = temp;
		break;

	case 2: // found length
		// fill data.
		xcps_rx_packet_rs485ForMPR[xcps_rxlen_rs485ForMPR++] = temp;

		// check length.
		if (xcps_rxlen_rs485ForMPR >= (2 + xcps_pdu_len_rs485ForMPR))
			xcps_state_rs485ForMPR = 3;

		if (xcps_rx_packet_rs485ForMPR[2] != 0x80)
			xcps_state_rs485ForMPR = 0;

		break;

	case 3: // data end, check checksum.
		for (i = 2; i < (2 + xcps_pdu_len_rs485ForMPR); i++)
			checkSum += (xcps_rx_packet_rs485ForMPR[i] & 0xFF);

		if (temp == (uint8) checkSum) // Checksum ok.
			xcps_state_rs485ForMPR = 4;

		else
			xcps_state_rs485ForMPR = 0;
		break;

	case 4:
		if (temp == ZS_EFLAG) {
			xcps_state_rs485ForMPR = 0;

			// return data to caller.
			for (i = 0; i < xcps_pdu_len_rs485ForMPR; i++)
				buff[i] = xcps_rx_packet_rs485ForMPR[2 + i];

			return xcps_pdu_len_rs485ForMPR;
		} else {
			// TODO:
			xcps_state_rs485ForMPR = 0;
		}
		break;
	default:
		// if you here, something wrong. --> recover to state 0.
		xcps_state_rs485ForMPR = 0;
		break;
	}
	return 0;
}

//end of file

