#include "ZRMProtocol.h"
#include "XNetProtocol.h"
#include "rotary.h"

uint8 tmp_BuffForZRM[MAX_TX_BUFF];

#define SendBuffSize 0x30
sendBuffData_st sendBuffData_0[SendBuffSize];
uint8 sendBuffForZRM_Head_0 = 0;
uint8 sendBuffForZRM_Tail_0 = 0;

sendBuffData_st sendBuffData_1[SendBuffSize];
uint8 sendBuffForZRM_Head_1 = 0;
uint8 sendBuffForZRM_Tail_1 = 0;

uint8 lastResult_0 = ERROR;
uint8 lastResult_1 = ERROR;

uint8 getMessageFromSendBuff(uint8 interface, sendBuffData_st *message) {
	uint8 len = 0;

	len = getMessageFromSendBuffWORemove(interface, message);

	if (getZRMMessageListSize(interface) > 0) {
		if (interface == ZIGBEE_0) {
			sendBuffForZRM_Head_0 += 1;
			sendBuffForZRM_Head_0 &= (SendBuffSize - 1);
			return len;
		} else if (interface == ZIGBEE_1) {
			sendBuffForZRM_Head_1 += 1;
			sendBuffForZRM_Head_1 &= (SendBuffSize - 1);
			return len;
		}
	}
	return len;
}

uint8 getMessageFromSendBuffWORemove(uint8 interface, sendBuffData_st *message) {
	uint8 len = 0;

	if (getZRMMessageListSize(interface) > 0) {
		if (interface == ZIGBEE_0) {
			memcpy(message->buff, sendBuffData_0[sendBuffForZRM_Head_0].buff, sendBuffData_0[sendBuffForZRM_Head_0].len);
			len = sendBuffData_0[sendBuffForZRM_Head_0].len;
			message->len = len;
			message->dstAddr = sendBuffData_0[sendBuffForZRM_Head_0].dstAddr;

			return len;
		} else if (interface == ZIGBEE_1) {
			memcpy(message->buff, sendBuffData_1[sendBuffForZRM_Head_1].buff, sendBuffData_1[sendBuffForZRM_Head_1].len);
			len = sendBuffData_1[sendBuffForZRM_Head_1].len;
			message->len = len;
			message->dstAddr = sendBuffData_1[sendBuffForZRM_Head_1].dstAddr;

			return len;
		}
	}
	return len;
}

void addMessageToSendBuff(uint8 interface, uint16 dstAddr, uint8 *data, uint8 len) {
	if (getZRMMessageListSize(interface) >= (SendBuffSize - 1))
		return;

	if (interface == ZIGBEE_0) {
		memcpy(sendBuffData_0[sendBuffForZRM_Tail_0].buff, data, len);
		sendBuffData_0[sendBuffForZRM_Tail_0].len = (uint8) len;
		sendBuffData_0[sendBuffForZRM_Tail_0].dstAddr = dstAddr;

		sendBuffForZRM_Tail_0 += 1;
		sendBuffForZRM_Tail_0 &= (SendBuffSize - 1);

	} else if (interface == ZIGBEE_1) {
		memcpy(sendBuffData_1[sendBuffForZRM_Tail_1].buff, data, len);
		sendBuffData_1[sendBuffForZRM_Tail_1].len = (uint8) len;
		sendBuffData_1[sendBuffForZRM_Tail_1].dstAddr = dstAddr;

		sendBuffForZRM_Tail_1 += 1;
		sendBuffForZRM_Tail_1 &= (SendBuffSize - 1);
	}
}

uint8 getZRMMessageListSize(uint8 interface) {
	if (interface == ZIGBEE_0)
		return (sendBuffForZRM_Tail_0 - sendBuffForZRM_Head_0) & (SendBuffSize - 1);
	else if (interface == ZIGBEE_1)
		return (sendBuffForZRM_Tail_1 - sendBuffForZRM_Head_1) & (SendBuffSize - 1);
	return 0;
}

void ZRMPMessage(uint8 interface, uint8 buff[], int buff_length) {
	zrmp_Info *zInfo_p = NULL;

//	if (interface == ZIGBEE_0)
//		zInfo_p = &zrmpInfo_0;
//	else if (interface == ZIGBEE_1)
//		zInfo_p = &zrmpInfo_1;

	if (interface == ZIGBEE_0)
		zInfo_p = &zrmpInfo_0;
	else {
		return;
	}

	switch (buff[0]) {
	case ZRMP_RES_PING:
		// Response Ping
		zInfo_p->zrm_Id = (uint16) (buff[11] << 8) | (buff[12]);
		zInfo_p->zrm_Panid = (uint16) (buff[13] << 8) | (buff[14]);

		if (zInfo_p->zrm_Panid != (0x0100 | rotary_GetValue())) {
			ZRMSendSetPanID(interface, 0x0100 | rotary_GetValue());
			ZRMSendReset(interface);
		} else {
			if (getZigbeeState(interface) != ZRM_CONNECT) {
				zInfo_p->zrm_State = ZRM_PING_COMPLETE;
			}
			ZRMsendConfig(interface);
		}

		break;

	case ZRMP_RES_CONFIG:
		// Response Config
		zInfo_p->zrm_Channel = buff[9];

		if (interface == ZIGBEE_0) {
			if (zInfo_p->zrm_Channel != (rotary_GetValue() % 16 + 11)) {
				ZRMSendSetChannel(interface, rotary_GetValue() % 16 + 11);
				ZRMsendConfig(interface);
			} else
				zInfo_p->zrm_State = ZRM_CONNECT;
		} else if (interface == ZIGBEE_1) {
			if (zInfo_p->zrm_Channel != ((rotary_GetValue() + 8) % 16 + 11)) {
				ZRMSendSetChannel(interface, (rotary_GetValue() + 8) % 16 + 11);
				ZRMsendConfig(interface);
			} else
				zInfo_p->zrm_State = ZRM_CONNECT;
		}
		break;

	case ZRMP_RES_NWK_INIT:
		ZRMsendPing(interface);
		break;

	case ZRMP_SUCCESS:
		if (interface == ZIGBEE_0)
			lastResult_0 = SUCCESS;
		else
			lastResult_1 = SUCCESS;
		break;
	case ZRMP_ERROR:
		if (interface == ZIGBEE_0)
			lastResult_0 = ERROR;
		else
			lastResult_1 = ERROR;
		break;
	default:
		// ERROR
		break;
	}
}

uint8 getZigbeeState(uint8 interface) {
	if (interface == ZIGBEE_0) {
		return zrmpInfo_0.zrm_State;
	} else {
		//return zrmpInfo_1.zrm_State;
		return ZRM_CONNECT;
	}
}

void ZRMsendPing(uint8 interface) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_PING;
	if (interface == ZIGBEE_0)
		xcps_send_zigbee_0(tmp_BuffForZRM, len);
	else
		xcps_send_zigbee_1(tmp_BuffForZRM, len);
}

void ZRMsendConfig(uint8 interface) {
	uint8 len = 0;
	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_CONFIG;
	tmp_BuffForZRM[len++] = ZRMP_MSG_FORMAT;

	if (interface == ZIGBEE_0)
		xcps_send_zigbee_0(tmp_BuffForZRM, len);
	else
		xcps_send_zigbee_1(tmp_BuffForZRM, len);
}

void ZRMSendReset(uint8 interface) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_RESET;
	tmp_BuffForZRM[len++] = ZRMP_REQ_RESET;

	if (interface == ZIGBEE_0)
		xcps_send_zigbee_0(tmp_BuffForZRM, len);
	else
		xcps_send_zigbee_1(tmp_BuffForZRM, len);
}

void ZRMSendSetPanID(uint8 interface, uint16 panId) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_PANID_SET;
	tmp_BuffForZRM[len++] = panId >> 8;
	tmp_BuffForZRM[len++] = (panId & 0xff);

	if (interface == ZIGBEE_0)
		xcps_send_zigbee_0(tmp_BuffForZRM, len);
	else
		xcps_send_zigbee_1(tmp_BuffForZRM, len);
}

void ZRMSendSetChannel(uint8 interface, uint8 channel) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_CONFIG_SET;
	tmp_BuffForZRM[len++] = 0x02;
	tmp_BuffForZRM[len++] = (channel & 0xff);

	if (interface == ZIGBEE_0)
		xcps_send_zigbee_0(tmp_BuffForZRM, len);
	else
		xcps_send_zigbee_1(tmp_BuffForZRM, len);
//	MSLEEP(3);
}

void ZRMSendSetPreconfig(uint8 interface, uint8 preconfig) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_CONFIG_SET;
	tmp_BuffForZRM[len++] = 0x05;
	tmp_BuffForZRM[len++] = (preconfig & 0xff);

	if (interface == ZIGBEE_0)
		xcps_send_zigbee_0(tmp_BuffForZRM, len);
	else
		xcps_send_zigbee_1(tmp_BuffForZRM, len);
}

void sendToZigbee(uint8 interface, uint16 dst_Addr, uint8 msg[], uint8 length) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = 0x10;
	tmp_BuffForZRM[len++] = (uint8) (dst_Addr >> 8);
	tmp_BuffForZRM[len++] = (uint8) (dst_Addr);
	tmp_BuffForZRM[len++] = (uint8) 0x00;
	tmp_BuffForZRM[len++] = (uint8) 0x00;

	len += ZRM_getTranlatedCode(&(tmp_BuffForZRM[len]), msg, length);

	addMessageToSendBuff(interface, dst_Addr, tmp_BuffForZRM, len);
}

uint8 sendQueueingMessage(uint8 interface) {
	static sendBuffData_st sendBuffData;
	static uint8 len;
	static uint32 cnt[10];
	static uint32 sendFail[10];
	uint8 result = 0;
	uint8 *lastResult_p;
	if (interface == ZIGBEE_0) {
		lastResult_p = &lastResult_0;
	} else {
		lastResult_p = &lastResult_1;
	}

	if (getZRMMessageListSize(interface) > 0) {
		//SA : send and ignore FOR sensors (ZIGBEE_1)
		if (interface == ZIGBEE_1) {
			getMessageFromSendBuff(interface, &sendBuffData);
			xcps_send_zigbee_1(sendBuffData.buff, sendBuffData.len);
			sendFail[interface] = 0;
			result = 1;
			return result;
		}

		if (getZigbeeState(interface) == ZRM_CONNECT) {
			if (getLastResult(interface) == SUCCESS) {
				getMessageFromSendBuff(interface, &sendBuffData);
				sendFail[interface] = 0;
				*lastResult_p = ERROR;
				result = 1;
			} else {
				result = 0;
			}
		}
	}

	if (getZRMMessageListSize(interface) > 0 && getZigbeeState(interface) == ZRM_CONNECT) {
		if (getLastResult(interface) == WAIT) {
			cnt[interface]++;
			if (cnt[interface] > 2) {
				cnt[interface] = 0;
				*lastResult_p = ERROR;
			} else {
				return 0;
			}
		}

		len = getMessageFromSendBuffWORemove(interface, &sendBuffData);
		if (len > 0) {
			sendFail[interface]++;
			if (sendFail[interface] > 10) {
				sendFail[interface] = 0;
				*lastResult_p = SUCCESS;
				return 0;
			}

			*lastResult_p = WAIT;
			if (interface == ZIGBEE_0) {
				xcps_send_zigbee_0(sendBuffData.buff, sendBuffData.len);
			} else if (interface == ZIGBEE_1) {
				xcps_send_zigbee_1(sendBuffData.buff, sendBuffData.len);
			}
			len = 0;
		}
	}

	return result;
}

uint8 getLastResult(uint8 interface) {
	if (interface == ZIGBEE_0)
		return lastResult_0;
	else
		//return lastResult_1;
		//always success
		return SUCCESS;
}

uint8 ZRM_getOriginalCode(uint8 *resultBuff, uint8 *buff, uint8 buff_length) {
	uint8 resultLen = 0;
	uint8 i = 0;
	for (i = 0; i < buff_length; i++) {
		if (buff[i] == 0xFF) {
			if (buff[i + 1] == 0xFF)
				resultBuff[resultLen++] = 0xFF;
			else if (buff[i + 1] == 0x01)
				resultBuff[resultLen++] = 0xFA;
			else if (buff[i + 1] == 0x02)
				resultBuff[resultLen++] = 0xAF;
			i++;
		} else
			resultBuff[resultLen++] = buff[i];
	}
	return resultLen;
}
uint8 ZRM_getTranlatedCode(uint8 *resultBuff, uint8 *buff, uint8 buff_length) {
	uint8 resultLen = 0;
	uint8 i = 0;
	for (i = 0; i < buff_length; i++) {
		if (buff[i] == 0xFA) {
			resultBuff[resultLen++] = 0xFF;
			resultBuff[resultLen++] = 0x01;
		} else if (buff[i] == 0xAF) {
			resultBuff[resultLen++] = 0xFF;
			resultBuff[resultLen++] = 0x02;
		} else if (buff[i] == 0xFF) {
			resultBuff[resultLen++] = 0xFF;
			resultBuff[resultLen++] = 0xFF;
		} else
			resultBuff[resultLen++] = buff[i];
	}
	return resultLen;
}

//end of file

