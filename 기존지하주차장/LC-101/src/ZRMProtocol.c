#include "ZRMProtocol.h"
#include "rotary.h"

uint8 tmp_BuffForZRM[MAX_TX_BUFF];

#define SendBuffSize 20
sendBuffData_st sendBuffData_0[SendBuffSize];
uint8 sendBuffForZRM_Head_0 = 0;
uint8 sendBuffForZRM_Tail_0 = 0;

uint8 lastResult_0 = ERROR;

uint8 getMessageFromSendBuff(sendBuffData_st *message) {
	uint8 len = 0;
	if (getZRMMessageListSize() > 0) {
		memcpy(message->buff, sendBuffData_0[sendBuffForZRM_Head_0].buff, sendBuffData_0[sendBuffForZRM_Head_0].len);
		len = sendBuffData_0[sendBuffForZRM_Head_0].len;
		message->len = len;
		message->dstAddr = sendBuffData_0[sendBuffForZRM_Head_0].dstAddr;

		sendBuffForZRM_Head_0 += 1;
		if (sendBuffForZRM_Head_0 >= SendBuffSize)
			sendBuffForZRM_Head_0 = 0;
		return len;
	}
	return len;
}

uint8 getMessageFromSendBuffWORemove(sendBuffData_st *message) {
	uint8 len = 0;
	if (getZRMMessageListSize() > 0) {
		memcpy(message->buff, sendBuffData_0[sendBuffForZRM_Head_0].buff, sendBuffData_0[sendBuffForZRM_Head_0].len);
		len = sendBuffData_0[sendBuffForZRM_Head_0].len;
		message->len = len;
		message->dstAddr = sendBuffData_0[sendBuffForZRM_Head_0].dstAddr;

		return len;
	}
	return len;
}

void addMessageToSendBuff(uint16 dstAddr, uint8 *data, uint8 len) {
	if (getZRMMessageListSize() >= SendBuffSize)
		return;

	memcpy(sendBuffData_0[sendBuffForZRM_Tail_0].buff, data, len);
	sendBuffData_0[sendBuffForZRM_Tail_0].len = (uint8) len;
	sendBuffData_0[sendBuffForZRM_Tail_0].dstAddr = dstAddr;

	sendBuffForZRM_Tail_0 += 1;
	if (sendBuffForZRM_Tail_0 >= SendBuffSize)
		sendBuffForZRM_Tail_0 = 0;
}

uint8 getZRMMessageListSize() {
	if (sendBuffForZRM_Tail_0 >= sendBuffForZRM_Head_0)
		return sendBuffForZRM_Tail_0 - sendBuffForZRM_Head_0;
	else
		return SendBuffSize + sendBuffForZRM_Tail_0 - sendBuffForZRM_Head_0;
}

void ZRMPMessage(uint8 buff[], int buff_length) {
	switch (buff[0]) {
	case ZRMP_RES_PING:
		// Response Ping
		zrmpInfo_0.zrm_Id = (uint16) (buff[11] << 8) | (buff[12]);
		zrmpInfo_0.zrm_Panid = (uint16) (buff[13] << 8) | (buff[14]);

		if (zrmpInfo_0.zrm_Panid != (0x0100 | rotary_GetValue())) {
			ZRMSendSetPanID(0x0100 | rotary_GetValue());
			ZRMSendReset();
		} else {
			zrmpInfo_0.zrm_State = ZRM_PING_COMPLETE;
			ZRMsendConfig();
		}

		break;

	case ZRMP_RES_CONFIG:
		// Response Config
		zrmpInfo_0.zrm_Channel = buff[9];
		if (zrmpInfo_0.zrm_Channel != (rotary_GetValue() % 16 + 11)) {
			ZRMSendSetChannel(rotary_GetValue() % 16 + 11);
			ZRMsendConfig();
		} else
			zrmpInfo_0.zrm_State = ZRM_CONNECT;
		break;

	case ZRMP_RES_NWK_INIT:
		ZRMsendPing();
		break;

	case ZRMP_SUCCESS:
		lastResult_0 = SUCCESS;
		break;
	case ZRMP_ERROR:
		lastResult_0 = ERROR;
		break;
	default:
		// ERROR
		break;
	}
}

uint8 getZigbeeState() {
	return zrmpInfo_0.zrm_State;
}

void ZRMsendPing() {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_PING;
	if (getLastResult() != WAIT)
		xcps_send_zigbee_0(tmp_BuffForZRM, len);
	//MSLEEP(3);
}

void ZRMsendConfig() {
	uint8 len = 0;
	if (getLastResult() != WAIT)
		tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_CONFIG;
	tmp_BuffForZRM[len++] = ZRMP_MSG_FORMAT;

	if (getLastResult() != WAIT)
		xcps_send_zigbee_0(tmp_BuffForZRM, len);
	//MSLEEP(3);
}

void ZRMSendReset() {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_RESET;
	tmp_BuffForZRM[len++] = ZRMP_REQ_RESET;
	//if(getLastResult()!=WAIT)
	xcps_send_zigbee_0(tmp_BuffForZRM, len);
	//MSLEEP(3);
}

void ZRMSendSetPanID(uint16 panId) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_PANID_SET;
	tmp_BuffForZRM[len++] = panId >> 8;
	tmp_BuffForZRM[len++] = (panId & 0xff);

	xcps_send_zigbee_0(tmp_BuffForZRM, len);
//	MSLEEP(3);
}

void ZRMSendSetChannel(uint8 channel) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_CONFIG_SET;
	tmp_BuffForZRM[len++] = 0x02;
	tmp_BuffForZRM[len++] = (channel & 0xff);
	if (getLastResult() != WAIT)
		xcps_send_zigbee_0(tmp_BuffForZRM, len);
//	MSLEEP(3);
}

void ZRMSendSetPreconfig(uint8 preconfig) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = ZRMP_PROTOCOL_ID;
	tmp_BuffForZRM[len++] = ZRMP_REQ_CONFIG_SET;
	tmp_BuffForZRM[len++] = 0x05;
	tmp_BuffForZRM[len++] = (preconfig & 0xff);
	if (getLastResult() != WAIT)
		xcps_send_zigbee_0(tmp_BuffForZRM, len);
}

//
void sendToZigbee(uint16 dst_Addr, uint8 msg[], uint8 length) {
	uint8 len = 0;

	tmp_BuffForZRM[len++] = 0x10;
	tmp_BuffForZRM[len++] = (uint8) (dst_Addr >> 8);
	tmp_BuffForZRM[len++] = (uint8) (dst_Addr);
	tmp_BuffForZRM[len++] = (uint8) 0x00;
	tmp_BuffForZRM[len++] = (uint8) 0x00;

	len += ZRM_getTranlatedCode(&(tmp_BuffForZRM[len]), msg, length);

	//xcps_send_zigbee(tmp_BuffForZRM, len);
	addMessageToSendBuff(dst_Addr, tmp_BuffForZRM, len);
	//MSLEEP(3);
}

uint8 sendQueueingMessage() {
	static sendBuffData_st sendBuffData;
	static uint8 len;
	uint8 result = 0;

	if (getZRMMessageListSize() > 0 && getZigbeeState() == ZRM_CONNECT) {
		if (getLastResult() == SUCCESS) {
			getMessageFromSendBuff(&sendBuffData);
			lastResult_0 = ERROR;
			result = 1;
		} else {
			result = 0;
		}
	}

	if (getZRMMessageListSize() > 0 && getZigbeeState() == ZRM_CONNECT) {
		len = getMessageFromSendBuffWORemove(&sendBuffData);
		if (len > 0) {
			lastResult_0 = WAIT;
			xcps_send_zigbee_0(sendBuffData.buff, sendBuffData.len);
			len = 0;
		}
	}
	return result;
}

uint8 getLastResult() {
	return lastResult_0;
}

uint8 ZRM_getOriginalCode(uint8 *resultBuff, uint8 *buff, uint8 buff_length) {
	uint8 resultLen = 0;
	uint8 i = 0;
	for (i = 0; i < buff_length; i++) {
		if (buff[i] == 0xFF) {
			if (buff[i + 1] == 0xFF)
				resultBuff[resultLen++] = 0xFF;
			else if (buff[i + 1] == 0x01)
				resultBuff[resultLen++] = 0xFA;
			else if (buff[i + 1] == 0x02)
				resultBuff[resultLen++] = 0xAF;
			i++;
		} else
			resultBuff[resultLen++] = buff[i];
	}
	return resultLen;
}
uint8 ZRM_getTranlatedCode(uint8 *resultBuff, uint8 *buff, uint8 buff_length) {
	uint8 resultLen = 0;
	uint8 i = 0;
	for (i = 0; i < buff_length; i++) {
		if (buff[i] == 0xFA) {
			resultBuff[resultLen++] = 0xFF;
			resultBuff[resultLen++] = 0x01;
		} else if (buff[i] == 0xAF) {
			resultBuff[resultLen++] = 0xFF;
			resultBuff[resultLen++] = 0x02;
		} else if (buff[i] == 0xFF) {
			resultBuff[resultLen++] = 0xFF;
			resultBuff[resultLen++] = 0xFF;
		} else
			resultBuff[resultLen++] = buff[i];
	}
	return resultLen;
}

//end of file

