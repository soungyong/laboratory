#include "MappingTable.h"
#include <avr/eeprom.h>
#include <avr/wdt.h>

//SensorSSRMappingTable_st ee_SensorMappingInfo EEMEM;
SensorMappingInfo_st ee_SensorMappingInfo[MaxNumOfSensorMappingInfo] EEMEM;
uint16 ee_SensorMappingTableSize EEMEM;

//DimmerSSRMappingTable_st ee_DimmerMappingInfo EEMEM;
DimmerMappingInfo_st ee_DimmerSSRMappingInfo[MaxNumOfDimmerMappingInfo] EEMEM;
uint16 ee_DimmerMappingTableSize EEMEM;

// ---------------------------------------------------------------------------------------------- //
// ------------------ Initialize Mapping Table  ------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void initMappingTable() {
	uint16 i = 0;

	for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
		eeprom_read_block(&(sensorMappingTable.mappingInfo[i]),
				&(ee_SensorMappingInfo[i]), sizeof(SensorMappingInfo_st));
}

void sensorMappingInfo_WriteToEEPRom(uint16 index) {

	eeprom_write_block(&(sensorMappingTable.mappingInfo[index]),
			&(ee_SensorMappingInfo[index]), sizeof(SensorMappingInfo_st));

}
// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint16 getSensorMappingTableSize() {
	uint16 i;
	uint16 size = 0;
	for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
		if (sensorMappingTable.mappingInfo[i].isEnable == 1)
			size++;
	return size;
}

SensorMappingInfo_st * getSensorMappingInfoByIndex(uint16 index) {
	uint16 cnt = 0;
	uint16 i = 0;
	for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
		if (sensorMappingTable.mappingInfo[i].isEnable == 1) {
			if (cnt == index) {
				return &(sensorMappingTable.mappingInfo[i]);
			}
			cnt++;
		}
	return NULL;
}

SensorMappingInfo_st *addSensorMappingTable(uint16 lcId, uint16 sensorId,
		uint8 circuitId, uint8 onTime, uint8 onCtrlMode, uint8 offCtrlMode) {
	uint16 i = 0;
	if (getSensorMappingTableSize() >= MaxNumOfSensorMappingInfo)
		return NULL;

	for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
		if (sensorMappingTable.mappingInfo[i].isEnable != 1) {
			sensorMappingTable.mappingInfo[i].lcId = lcId;
			sensorMappingTable.mappingInfo[i].sensorId = sensorId;
			sensorMappingTable.mappingInfo[i].circuitId = circuitId;
			sensorMappingTable.mappingInfo[i].onTime = onTime;
			sensorMappingTable.mappingInfo[i].onCtrlMode = onCtrlMode;
			sensorMappingTable.mappingInfo[i].offCtrlMode = offCtrlMode;
			sensorMappingTable.mappingInfo[i].isEnable = 1;
			break;
		}
	if (i == MaxNumOfSensorMappingInfo)
		return NULL;

	return &(sensorMappingTable.mappingInfo[i]);
}

int updateSensorMappingTableByCircuit(uint8 circuitId, uint8 onTime,
		uint8 onCtrlMode, uint8 offCtrlMode) {
	int index;
	int result = 0;
	uint16 i = 0;

	for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
		if (circuitId == sensorMappingTable.mappingInfo[i].circuitId
				&& sensorMappingTable.mappingInfo[i].isEnable == 1) {
			sensorMappingTable.mappingInfo[i].onTime = onTime;
			sensorMappingTable.mappingInfo[i].onCtrlMode = onCtrlMode;
			sensorMappingTable.mappingInfo[i].offCtrlMode = offCtrlMode;
			sensorMappingInfo_WriteToEEPRom(i);
			wdt_reset();
		}

	return result;
}

int updateSensorMappingTable(uint16 lcId, uint16 sensorId, uint8 circuitId,
		uint8 onTime, uint8 onCtrlMode, uint8 offCtrlMode) {
	int index;
	int result = 0;
	if ((index = findSensorMappingInfoById(lcId, sensorId, circuitId)) != -1) {
		sensorMappingTable.mappingInfo[index].onTime = onTime;
		sensorMappingTable.mappingInfo[index].onCtrlMode = onCtrlMode;
		sensorMappingTable.mappingInfo[index].offCtrlMode = offCtrlMode;
		result = 2;
	} else if (addSensorMappingTable(lcId, sensorId, circuitId, onTime,
			onCtrlMode, offCtrlMode) != NULL)
		result = 1;
	else
		return 0;

	index = findSensorMappingInfoById(lcId, sensorId, circuitId);

	if (index != -1)
		sensorMappingInfo_WriteToEEPRom(index);

	return result;
}

int findSensorMappingInfoById(uint16 lcId, uint16 sensorId, uint8 circuitId) {
	uint16 i = 0;
	for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
		if (sensorId == sensorMappingTable.mappingInfo[i].sensorId
				&& circuitId == sensorMappingTable.mappingInfo[i].circuitId
				&& sensorMappingTable.mappingInfo[i].isEnable == 1)
			return i;
	return -1; // 찾는 값이 없으면 '-1'을 리턴
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //

uint8 getDimmerMappingTableSize() {
	uint8 i;
	uint8 size = 0;
	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++)
		if (dimmerMappingTable.mappingInfo[i].isEnable == 1)
			size++;
	return size;
}

DimmerMappingInfo_st * getDimmerMappingInfoByIndex(uint8 index) {
	uint8 cnt = 0;
	uint8 i = 0;
	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++)
		if (dimmerMappingTable.mappingInfo[i].isEnable == 1) {
			if (cnt == index) {
				return &(dimmerMappingTable.mappingInfo[i]);
			}
			cnt++;
		}
	return NULL;
}

DimmerMappingInfo_st *addDimmerMappingTable(uint16 dimmerId, uint8 channelId,
		uint8 circuitId) {
	uint16 i = 0;

	if (getDimmerMappingTableSize() >= MaxNumOfDimmerMappingInfo)
		return NULL;

	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++) {
		if (dimmerMappingTable.mappingInfo[i].isEnable != 1) {
			dimmerMappingTable.mappingInfo[i].dimmerId = dimmerId;
			dimmerMappingTable.mappingInfo[i].channelId = channelId;
			dimmerMappingTable.mappingInfo[i].circuitId = circuitId;
			dimmerMappingTable.mappingInfo[i].isEnable = 1;
			break;
		}
	}

	if (i == MaxNumOfDimmerMappingInfo)
		return NULL;

	return &(dimmerMappingTable.mappingInfo[i]);
}

int updateDimmerMappingTable(uint16 dimmerId, uint8 channelId, uint8 circuitId) {
	int index;
	int result;

	if ((index = findDimmerMappingInfoById(dimmerId, channelId)) != -1) {
		return 0;
	} else {
		if (addDimmerMappingTable(dimmerId, channelId, circuitId) == NULL)
			return 2;
		else
			result = 1;
	}

	index = findDimmerMappingInfoById(dimmerId, channelId);
	if (index != -1)
		dimmerMappingInfo_WriteToEEPRom(index);
	return result;
}

int findDimmerMappingInfoByPair(uint16 dimmerId, uint8 channelId,
		uint8 circuitId) {
	uint16 i = 0;
	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++)
		if (dimmerId == dimmerMappingTable.mappingInfo[i].dimmerId
				&& channelId == dimmerMappingTable.mappingInfo[i].channelId
				&& circuitId == dimmerMappingTable.mappingInfo[i].circuitId)
			return i;
	return -1; // 찾는 값이 없으면 '-1'을 리턴
}

int findDimmerMappingInfoById(uint16 dimmerId, uint8 channelId) {
	uint16 i = 0;
	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++)
		if (dimmerId == dimmerMappingTable.mappingInfo[i].dimmerId
				&& channelId == dimmerMappingTable.mappingInfo[i].channelId
				&& dimmerMappingTable.mappingInfo[i].isEnable == 1)
			return i;
	return -1; // 찾는 값이 없으면 '-1'을 리턴
}

void initDimmerMappingTable() {
	uint16 i = 0;

	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++)
		eeprom_read_block(&(dimmerMappingTable.mappingInfo[i]),
				&(ee_DimmerSSRMappingInfo[i]), sizeof(DimmerMappingInfo_st));
}

void dimmerMappingInfo_WriteToEEPRom(uint16 index) {
	eeprom_write_block(&(dimmerMappingTable.mappingInfo[index]),
			&(ee_DimmerSSRMappingInfo[index]), sizeof(DimmerMappingInfo_st));
}

// Enf of File
