#include "DimmerProtocol.h"
#include "ZRMProtocol.h"
#include "MappingTable.h"
////////
uint8 tmpBuffForDimmer[32];
///////
static uint16 seqNumGeneratorForDimmer = 0;
uint16 getSeqNumGeneratorForDimmer() {
	return seqNumGeneratorForDimmer++;
}

uint8 plcs_GetDCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGeneratorForDimmer();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_DCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

void dimmer_SendReqDimming(uint8 circuitId, uint8 DimLEVEL) {
	uint16 i = 0;
	int len = 0;
	uint8 payload[10];
	uint8 resultLen;
	device_Info* deviceInfo_P;

	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++) {
		if (dimmerMappingTable.mappingInfo[i].circuitId == circuitId && dimmerMappingTable.mappingInfo[i].isEnable) {
			deviceInfo_P = findNodeById(
					dimmerMappingTable.mappingInfo[i].dimmerId);
			if (deviceInfo_P != NULL) {
				len = 0;
				payload[len++] = PLCS_DCP_REQ_DIMMING;
				payload[len++] = deviceInfo_P->ieeeId >> 8;
				payload[len++] = deviceInfo_P->ieeeId;
				payload[len++] = dimmerMappingTable.mappingInfo[i].channelId;
				payload[len++] = dimmer_DimmingConvert(DimLEVEL);

				resultLen = plcs_GetDCPMessage(tmpBuffForDimmer, deviceInfo_P->ieeeId,
						zrmpInfo_0.zrm_Id, payload, len);
				plcs_SendMessage(deviceInfo_P->nwkAddr, tmpBuffForDimmer, resultLen);
//				MSLEEP(50);
			}
		}
	}
}

void dimmer_SendReqDimmingByNodeId(uint16 nodeId, uint8 channelId, uint8 DimLEVEL) {
	int len = 0;
	uint8 payload[10];
	device_Info* deviceInfo_P;
	uint8 resultLen;

	deviceInfo_P = findNodeById(nodeId);
	if (deviceInfo_P == NULL)
		return;

	len = 0;
	payload[len++] = PLCS_DCP_REQ_DIMMING;
	payload[len++] = deviceInfo_P->ieeeId >> 8;
	payload[len++] = deviceInfo_P->ieeeId;
	payload[len++] = channelId;
	payload[len++] = dimmer_DimmingConvert(DimLEVEL);

	resultLen = plcs_GetDCPMessage(tmpBuffForDimmer, deviceInfo_P->ieeeId,
			zrmpInfo_0.zrm_Id, payload, len);

	plcs_SendMessage(deviceInfo_P->nwkAddr, tmpBuffForDimmer, resultLen);
}

uint8 dimmer_DimmingConvert(uint8 DimLevel) {
	switch (DimLevel) {
	case DIMMING_0:
		return 0x00;
		break;

	case DIMMING_1:
		return 0x20;
		break;

	case DIMMING_2:
		return 0x34;
		break;

	case DIMMING_3:
		return 0x4D;
		break;

	case DIMMING_4:
		return 0x67;
		break;

	case DIMMING_5:
		return 0x80;
		break;

	case DIMMING_6:
		return 0x9A;
		break;

	case DIMMING_7:
		return 0xB3;
		break;

	case DIMMING_8:
		return 0xCD;
		break;

	case DIMMING_9:
		return 0xE6;
		break;

	case DIMMING_10:
		return 0xFE;
		break;

	default:
		return 0xFE;
		break;
	}

}
