#ifndef __MAPPING_TABLE_H__
#define __MAPPING_TABLE_H__


#include <string.h>  			// strcmp(), strcpy(), strlen() 함수등 주로 문자열과 관련된 함수사용시
#include <stdlib.h>  			// malloc() 함수 사용
#include <avr/pgmspace.h>
#include "Util.h"
#include "Uart.h"


#define MaxNumOfSensorMappingInfo 256
#define MaxNumOfDimmerMappingInfo 40

typedef struct {
	uint16 lcId;
	uint16 sensorId;
	uint8 circuitId;
	uint8 onTime;
	uint8 onCtrlMode;
	uint8 offCtrlMode;
	uint8 isEnable;
} SensorMappingInfo_st;

typedef struct {	
	SensorMappingInfo_st mappingInfo[MaxNumOfSensorMappingInfo];
}SensorMappingTable_st;

SensorMappingTable_st sensorMappingTable;

typedef struct {
	uint16 dimmerId;
	uint8 channelId;
	uint8 circuitId;
	uint8 isEnable;
} DimmerMappingInfo_st;

typedef struct {
	DimmerMappingInfo_st mappingInfo[MaxNumOfDimmerMappingInfo];
}DimmerMappingTable_st;

DimmerMappingTable_st dimmerMappingTable;


//--------------------------------------------------------------------------------------------//
void initMappingTable();
uint16 getSensorMappingTableSize();
SensorMappingInfo_st * getSensorMappingInfoByIndex(uint16 index);

void sensorMappingInfo_WriteToEEPRom(uint16 index);
SensorMappingInfo_st *addSensorMappingTable(uint16 lcId, uint16 sensorId, uint8 circuitId, uint8 onTime, uint8 onCtrlMode, uint8 offCtrlMode);
int updateSensorMappingTable(uint16 lcId, uint16 sensorId, uint8 circuitId, uint8 onTime, uint8 onCtrlMode, uint8 offCtrlMode);
int updateSensorMappingTableByCircuit(uint8 circuitId, uint8 onTime, uint8 onCtrlMode, uint8 offCtrlMode);
int findSensorMappingInfoById(uint16 lcId, uint16 sensorId, uint8 circuitId);

void printMappingInfoTable();

void initDimmerMappingTable();
void dimmerMappingInfo_WriteToEEPRom(uint16 index);
uint8 getDimmerMappingTableSize();
DimmerMappingInfo_st * getDimmerMappingInfoByIndex(uint8 index);
DimmerMappingInfo_st *addDimmerMappingTable(uint16 dimmerId, uint8 channelId, uint8 circuitId);
int updateDimmerMappingTable(uint16 dimmerId, uint8 channelId, uint8 circuitId);
int findDimmerMappingInfoByPair(uint16 dimmerId, uint8 channelId, uint8 circuitId);
int findDimmerMappingInfoById(uint16 dimmerId, uint8 channelId);

void printMappingInfoTable();

#endif

