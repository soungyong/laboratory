#ifndef __PLCS_NCP_PROTOCOL_H__
#define __PLCS_NCP_PROTOCOL_H__

#include "NCProtocol.h"
#include "util.h"

// ---------------------------------------------------------------------------------------------- //
#define PLCS_NCP_PROTOCOL_ID						(0x01)
#define PLCS_GCP_PROTOCOL_ID						(0x21)
#define PLCS_LC_FW_VER								0x08

#define PLCS_GCP_REQ_NUMOFDEVICE					(0x20)
#define PLCS_GCP_RES_NUMOFDEVICE					(0x21)
#define PLCS_GCP_REQ_DEVICEINFO						(0x22)
#define PLCS_GCP_RES_DEVICEINFO						(0x23)
#define PLCS_GCP_REQ_STATEINFO						(0x24)
#define PLCS_GCP_RES_STATEINFO						(0x25)

#define PLCS_GCP_REQ_CONTROL_CIRCUIT				(0x30)
#define PLCS_GCP_RES_CONTROL_CIRCUIT				(0x31)
#define PLCS_GCP_REQ_CONTROL_SENSOR					(0x32)
#define PLCS_GCP_RES_CONTROL_SENSOR					(0x33)
#define PLCS_GCP_REQ_CONTROL_SENSORSHARING			(0x34)
#define PLCS_GCP_RES_CONTROL_SENSORSHARING			(0x35)
#define PLCS_GCP_REQ_CONTROL_GATEWAY				(0x36)
#define PLCS_GCP_RES_CONTROL_GATEWAY				(0x37)
#define PLCS_GCP_REQ_CONTROL_REBOOT					(0x3A)

#define PLCS_GCP_REQ_POWERMETER						(0x40)
#define PLCS_GCP_RES_POWERMETER						(0x41)
#define PLCS_GCP_REQ_UPDATEPOWERMETER_LC			(0x42)
#define PLCS_GCP_REQ_UPDATEPOWERMETER				(0x44)

#define PLCS_GCP_REQ_SENSORMAPPINGSIZE				(0x50)
#define PLCS_GCP_RES_SENSORMAPPINGSIZE				(0x51)
#define PLCS_GCP_REQ_DIMMERMAPPINGSIZE				(0x52)
#define PLCS_GCP_RES_DIMMERMAPPINGSIZE				(0x53)
#define PLCS_GCP_REQ_SCHEDULESIZE					(0x54)
#define PLCS_GCP_RES_SCHEDULESIZE					(0x55)
#define PLCS_GCP_REQ_SENSORMAPPINGINFO				(0x56)
#define PLCS_GCP_RES_SENSORMAPPINGINFO				(0x57)
#define PLCS_GCP_REQ_DIMMERMAPPINGINFO				(0x58)
#define PLCS_GCP_RES_DIMMERMAPPINGINFO				(0x59)
#define PLCS_GCP_REQ_SCHEDULEINFO					(0x5A)
#define PLCS_GCP_RES_SCHEDULEINFO					(0x5B)
#define PLCS_GCP_REQ_MAPPINGSENSOR					(0x5C)
#define PLCS_GCP_RES_MAPPINGSENSOR					(0x5D)
#define PLCS_GCP_REQ_MAPPINGSENSORLIST				(0x5E)
#define PLCS_GCP_RES_MAPPINGSENSORLIST				(0x5F)
#define PLCS_GCP_REQ_RESETMAPPINGSENSOR				(0x60)
#define PLCS_GCP_RES_RESETMAPPINGSENSOR				(0x61)
#define PLCS_GCP_REQ_MAPPINGDIMMER					(0x62)
#define PLCS_GCP_RES_MAPPINGDIMMER					(0x63)
#define PLCS_GCP_REQ_MAPPINGDIMMERLIST				(0x64)
#define PLCS_GCP_RES_MAPPINGDIMMERLIST				(0x65)
#define PLCS_GCP_REQ_RESETMAPPINGDIMMER				(0x66)
#define PLCS_GCP_RES_RESETMAPPINGDIMMER				(0x67)
#define PLCS_GCP_REQ_ADDSCHEDULE					(0x68)
#define PLCS_GCP_RES_ADDSCHEDULE					(0x69)
#define PLCS_GCP_REQ_ADDSCHEDULELIST				(0x6A)
#define PLCS_GCP_RES_ADDSCHEDULELIST				(0x6B)
#define PLCS_GCP_REQ_RESETSCHEDULE					(0x6C)
#define PLCS_GCP_RES_RESETSCHEDULE					(0x6D)

#define PLCS_GCP_NOTICEEVENT						(0x70)
#define PLCS_GCP_NOTICEEVENT_NEIGHBOR				(0x71)

#define PLCS_GCP_REQ_SETSENSINGLEVEL				(0x80)
#define PLCS_GCP_RES_SETSENSINGLEVEL				(0x81)

#define PLCS_GCP_REQ_DEBUG_LOG						(0x90)
#define PLCS_GCP_RES_DEBUG_LOG						(0x91)
#define PLCS_GCP_REQ_RESET_DEBUG_LOG				(0x92)
#define PLCS_GCP_REQ_DEBUG_LOG_LC					(0x93)
#define PLCS_GCP_RES_DEBUG_LOG_LC					(0x94)
#define PLCS_GCP_REQ_RESET_DEBUG_LOG_LC				(0x95)

#define PLCS_GW_TYPE 								(0x2000)
#define PLCS_LC_100Z_TYPE							(0x2010)
#define PLCS_ZDIMMER_TYPE							(0x2040)
#define PLCS_ZSENSOR_TYPE							(0x2050)
#define PLCS_ZMDF_TYPE								(0x2060)
#define PLCS_ZSW_TYPE								(0x2080)

uint16 getSeqNumGenerator();
uint8 plcs_GetNCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len);
uint8 plcs_GetNCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum);
uint8 plcs_GetGCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len);
uint8 plcs_GetGCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum);

extern uint8 ncp_NetState;
uint8 ncp_ConnState();

// ---------------------------------------------------------------------------------------------- //
extern char token;
extern uint8 Min;
extern uint8 Hour;
extern uint8 applyingSchedule;
// ---------------------------------------------------------------------------------------------- //

void NCP_ProcessMessage(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_ProcessMessage(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

void ncp_ProcessRegisterReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void ncp_ProcessPingReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void ncp_ProcessRegisterRes(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void ncp_ProcessPingRes(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void npc_ProcessSendTimeInfo(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

void ncp_SendPingReq(uint16 srcNetAddr);
void ncp_SendRegisterReq(uint16 srcNetAddr, uint16 nodeId);
void npc_SendRegisterNodeReq(uint16 srcNetAddr, uint16 node_Id, uint16 device_Type,
		uint16 nwk_addr);
void ncp_SendRegisterNodeReq(uint16 srcNetAddr, uint16 deviceId, uint16 deviceType,
		uint8 deviceVersion, uint8 fwVersion, uint16 netAddr);

void ncp_SendPingRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint16 device_Type, uint8 flag, uint8 timeOut);
void ncp_SendRegisterRes(uint16 srcNetAddr, uint16 seqNum, uint16 node_Id,
		uint16 deviceType, uint16 result);

void registerAllNodetoServer();

char hasToken();
void plcs_NCP_ProcessRegisterBroadcastReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_NCP_ProcessAssignToken(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_NCP_ProcessReleaseTokenRes(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_NCP_Send_NCPReqReleaseToken(uint16 srcNetAddr);

//GCP Handle
void plcs_GCP_HandleNumOfDeviceReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleDeviceInfoReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleStateInfoReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleControlRebootReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleControlCircuitReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleControlSensorReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandlePowermeterReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleUpdatePowermeterLCReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleUpdatePowermeterReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleSensorMappingSizeReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleDimmerMappingSizeReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleScheduleSizeReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleSensorMappingInfoReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleDimmerMappingInfoReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleScheduleInfoReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleMappingSensorReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleMappingSensorListReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleResetSensorMappingReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleMappingDimmerReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleMappingDimmerListReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleResetDimmerMappingReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleAddScheduleReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleAddScheduleListReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleResetScheduleReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleNoticeEventNeighbor(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleDebugLogLCReq(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleDebugLogLCReset(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

void plcs_GCP_HandleSensorEvent(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

void handleSensorEvent(uint16 lcId, uint16 sensorId);
void handleSwitchEvent(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

//GCP Send
void plcs_GCP_SendNumOfDeviceRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint8 size);
void plcs_GCP_SendDeviceInfoRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint8 index);
void plcs_GCP_SendStateInfoRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint16 deviceId);
void plcs_GCP_SendControlCircuitRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint8 result);
void plcs_GCP_SendControlSensorRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint8 result);
void plcs_GCP_SendPowermeterRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint32 powermeterValue);
void plcs_GCP_SendSensorMappingSizeRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint8 size);
void plcs_GCP_SendDimmerMappingSizeRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint8 size);
void plcs_GCP_SendScheduleSizeRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint8 scheduleType,
		uint8 size);
void plcs_GCP_SendSensorMappingInfoRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint8 index);
void plcs_GCP_SendDimmerMappingInfoRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint8 index);
void plcs_GCP_SendScheduleInfoRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint8 scheduleType,
		uint8 index);
void plcs_GCP_SendMappingSensorRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint8 result);
void plcs_GCP_SendMappingSensorListRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint8 result);
void plcs_GCP_SendResetSensorMappingRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint8 result);
void plcs_GCP_SendMappingDimmerRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint8 result);
void plcs_GCP_SendMappingDimmerListRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint8 result);
void plcs_GCP_SendResetDimmerMappingRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint8 result);
void plcs_GCP_SendAddScheduleRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint8 result);
void plcs_GCP_SendAddScheduleListRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint8 result);
void plcs_GCP_SendResetScheduleRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId,
		uint8 result);
void plcs_GCP_SendNoticeEvent(uint16 srcNetAddr, uint16 nodeId, uint8 dataFormat, uint16 sensorId);
void plcs_GCP_SendNoticeEventNeightbor(uint16 srcNetAddr, uint16 nodeId, uint8 dataFormat, uint16 sensorId, uint8 sensorState);

void plcs_GCP_SendDebugLogLCRes(uint16 srcNetAddr, uint16 seqNum, uint16 nodeId, uint16 rebootCnt,
		uint8 lastRebootTime[], uint16 serverConnectionCnt,
		uint8 lastServerConnectionTime[]);

void plcs_SendMessage(uint16 dst_Addr, uint8 msg[], int length);

extern volatile uint32 g_PowerMeterValue;

#endif
