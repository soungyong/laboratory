#include "XNetProtocol.h"
#include "TestDeviceProtocol.h"
#include "RFU_Protocol.h"

// --------------------------------------------------------------------------- //
#define MaxMessageListSize 50
uint8 sendMessageList[MaxMessageListSize][XCPS_MAX_PDU];
uint8 messageLength[MaxMessageListSize];
uint8 messageList_Tail = 0;
uint8 messageList_Head = 0;

uint8 getMessageListSize() {
	if (messageList_Tail >= messageList_Head)
		return messageList_Tail - messageList_Head;
	else
		return MaxMessageListSize + messageList_Tail - messageList_Head;
}
// --------------------------------------------------------------------------- //


void XNetHandlerFromZigbee(uint16 srcNetAddr, uint8 msg[], int buff_length) {
	uint8 pid;
	uint8 subPid;
	uint16 seqNum;
	uint16 srcId;
	uint16 destId;

	pid = msg[0];
	subPid = msg[1];
	seqNum = msg[2] << 8 | msg[3];
	destId = msg[4] << 8 | msg[5];
	srcId = msg[6] << 8 | msg[7];

	switch (pid) {
	case NCP_PROTOCOL_ID: {
		switch (subPid) {
		case PLCS_NCP_PROTOCOL_ID:
			NCP_ProcessMessage(srcNetAddr, seqNum, srcId, destId, msg,
					buff_length);
			break;
		case PLCS_GCP_PROTOCOL_ID:
			plcs_GCP_ProcessMessage(srcNetAddr, seqNum, srcId, destId, msg,
					buff_length);
			break;
		case PLCS_RFU_PROTOCOL_ID:
			plcs_RFUP_ProcessMessage(srcNetAddr, seqNum, srcId, destId, msg,
					buff_length);
			break;
		case TEST_DEVICE_PROTOCOL_ID:
			processNCP_TestDevice(srcNetAddr, msg, buff_length);
			break;
		}
	}
		break;
	default:
		break;
	}
}

void XNetCommandFromGateway(uint8 msg[], int buff_length) {
	uint8 pid;
	uint8 subPid;
	uint16 subNodeId;
	uint16 seqNum;
	uint16 srcId;
	uint16 destId;

	pid = msg[0];
	subPid = msg[1];
	seqNum = msg[2] << 8 | msg[3];
	destId = msg[4] << 8 | msg[5];
	srcId = msg[6] << 8 | msg[7];

	switch (pid) {
	case NCP_PROTOCOL_ID: {
		switch (subPid) {
		case PLCS_NCP_PROTOCOL_ID:
			if (destId == zrmpInfo_0.zrm_Id || destId == 0xffff)
				NCP_ProcessMessage(0, seqNum, srcId, destId, msg, buff_length);
			break;
		case PLCS_GCP_PROTOCOL_ID:
			//subNodeId Ȯ��
			subNodeId = msg[9] << 8 | msg[10];
			if (subNodeId == zrmpInfo_0.zrm_Id)
				plcs_GCP_ProcessMessage(0, seqNum, srcId, destId, msg,
						buff_length);
			break;
		case PLCS_RFU_PROTOCOL_ID:
			subNodeId = msg[9] << 8 | msg[10];
			if (subNodeId == zrmpInfo_0.zrm_Id)
				plcs_RFUP_ProcessMessage(0, seqNum, srcId, destId, msg,
						buff_length);
			break;
		}
	}
		break;
	default:
		break;
	}
}

char sendStackedServerMessage() {
	if (getMessageListSize() > 0) {
		messageList_Head += 1;
		xcps_send_rs485(sendMessageList[messageList_Head - 1],
				messageLength[messageList_Head - 1]);
		if (messageList_Head >= MaxMessageListSize)
			messageList_Head = 0;
		return 1;
	}
	return 0;
}

void sendServerMessage(uint8 *msg, uint8 buff_length) {
	if (getMessageListSize() >= (MaxMessageListSize - 2)) {
		return;
	}

	memcpy(sendMessageList[messageList_Tail], msg, buff_length);
	messageLength[messageList_Tail] = (uint8) buff_length;
	messageList_Tail += 1;
	if (messageList_Tail >= MaxMessageListSize)
		messageList_Tail = 0;
}

void sendServerMessageWithoutQueue(uint8 *msg, int buff_length) {
	xcps_send_rs485(msg, buff_length);
}

//end of file

