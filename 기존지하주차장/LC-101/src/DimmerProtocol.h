#ifndef __DIMMERPROTOCOL_H
#define __DIMMERPROTOCOL_H

#include "Util.h"
#include "NCProtocol.h"

#define PLCS_DCP_PROTOCOL_ID 0x11
#define PLCS_DCP_REQ_DIMMING 0x32

void dimmer_SendReqDimming(uint8 circuitId, uint8 DimLEVEL);
void dimmer_SendReqDimmingByNodeId(uint16 nodeId, uint8 channelId, uint8 DimLEVEL);
uint8 dimmer_DimmingConvert(uint8 DimLevel);


#endif
