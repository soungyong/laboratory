#include "ScheduleTable.h"
#include "deviceInfo.h"
#include <avr/wdt.h>

uint8 ee_DefaultScheduleTimeSize[MaxNumOfCircuit] EEMEM;
Schedule_Info ee_DefaultScheduleTime[MaxNumOfCircuit][MaxNumOfScheduleInfo] EEMEM;

void initScheduleTimeTable() {
	uint8 i = 0;
	uint8 j = 0;
	for (i = 0; i < MaxNumOfCircuit; i++) {
		defaultScheduleTable.size[i] = 0;
		todayScheduleTable.size[i] = 0;
		tomorrowScheduleTable.size[i] = 0;
		eeprom_read_block(&(defaultScheduleTable.size[i]),
				&ee_DefaultScheduleTimeSize[i], sizeof(uint8));

		if (defaultScheduleTable.size[i] >= MaxNumOfScheduleInfo)
			defaultScheduleTable.size[i] = 0;
		if (todayScheduleTable.size[i] >= MaxNumOfScheduleInfo)
			todayScheduleTable.size[i] = 0;
		if (tomorrowScheduleTable.size[i] >= MaxNumOfScheduleInfo)
			tomorrowScheduleTable.size[i] = 0;

		for (j = 0; j < defaultScheduleTable.size[i]; j++)
			eeprom_read_block(&(defaultScheduleTable.scheduleInfo[i][j]),
					&(ee_DefaultScheduleTime[i][j]), sizeof(Schedule_Info));
	}
}

// --------------------------------------------------------------------------------------------------------- //
// ------------------ Add Schedule Table ---------------------------------------------------------------------- //
// --------------------------------------------------------------------------------------------------------- //
int addSchedule(uint8 scheduleType, uint8 circuitId, uint8 hour, uint8 minute,
		uint8 ctrlMode) {
	uint8 onTime = 0;
	uint8 index = 255;

	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == TODAY)
		scheduleTable_p = &todayScheduleTable;
	else if (scheduleType == TOMORROW)
		scheduleTable_p = &tomorrowScheduleTable;
	else
		return 0;

	if (scheduleTable_p->size[circuitId] >= MaxNumOfScheduleInfo)
		return 0;
	if (circuitId >= MaxNumOfCircuit)
		return 0;

	if (isContainScheduleAt(scheduleType, circuitId, hour, minute)) {
		onTime = hour * 6 + minute / 10;

		for (index = 0; index < scheduleTable_p->size[circuitId]; index++)
			if (scheduleTable_p->scheduleInfo[circuitId][index].onTime_10m
					== onTime)
				scheduleTable_p->scheduleInfo[circuitId][index].ctrlMode =
						ctrlMode;
	} else {
		scheduleTable_p->scheduleInfo[circuitId][scheduleTable_p->size[circuitId]].onTime_10m =
				hour * 6 + minute / 10;
		scheduleTable_p->scheduleInfo[circuitId][scheduleTable_p->size[circuitId]].ctrlMode =
				ctrlMode;
		(scheduleTable_p->size[circuitId])++;
	}

	schedule_WriteToEEPRom(scheduleType, circuitId,
			(scheduleTable_p->size[circuitId]) - 1);

	return 1;
}

int addScheduleList(uint8 scheduleType, uint8 circuitId, uint8 numOfSchedule,
		uint8 hour[], uint8 minute[], uint8 ctrlMode[]) {
	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == TODAY)
		scheduleTable_p = &todayScheduleTable;
	else if (scheduleType == TOMORROW)
		scheduleTable_p = &tomorrowScheduleTable;
	else
		return 0;

	if (scheduleTable_p->size[circuitId] + numOfSchedule > MaxNumOfScheduleInfo)
		return 0;
	if (circuitId >= MaxNumOfCircuit)
		return 0;

	for (int i = 0; i < numOfSchedule; i++){
		addSchedule(scheduleType, circuitId, hour[i], minute[i], ctrlMode[i]);
		wdt_reset();
	}

	return 1;
}

uint8 getControlModeAt(uint8 scheduleType, uint8 circuitId, uint8 hour,
		uint8 minute) {
	uint8 i = 0;
	uint8 onTime;
	uint8 scheduleTime = 200;
	uint8 resultCtrlMode = ON;

	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == TODAY)
		scheduleTable_p = &todayScheduleTable;
	else if (scheduleType == TOMORROW)
		scheduleTable_p = &tomorrowScheduleTable;
	else
		return ON;

	if (scheduleTable_p->size[circuitId] == 0)
		return ON;
	else if (scheduleTable_p->size[circuitId] == 1)
		return scheduleTable_p->scheduleInfo[circuitId][0].ctrlMode;

	onTime = hour * 6 + minute / 10;

	for (i = 0; i < scheduleTable_p->size[circuitId]; i++) {
		if (scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m <= onTime)
			if (scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m
					>= scheduleTime || scheduleTime==200) {
				scheduleTime =
						scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m;
				resultCtrlMode =
						scheduleTable_p->scheduleInfo[circuitId][i].ctrlMode;
			}
	}

	if (scheduleTime == 200) {
		scheduleTime=0;
		for (i = 0; i < scheduleTable_p->size[circuitId]; i++) {
			if (scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m
					>= scheduleTime) {
				scheduleTime =
						scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m;
				resultCtrlMode =
						scheduleTable_p->scheduleInfo[circuitId][i].ctrlMode;
			}
		}

		return resultCtrlMode;
	}

	return resultCtrlMode;
}

uint8 isContainScheduleAt(uint8 scheduleType, uint8 circuitId, uint8 hour,
		uint8 minute) {
	uint8 i = 0;
	uint8 onTime;
	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == TODAY)
		scheduleTable_p = &todayScheduleTable;
	else if (scheduleType == TOMORROW)
		scheduleTable_p = &tomorrowScheduleTable;
	else
		return 0;

	onTime = hour * 6 + minute / 10;

	for (i = 0; i < scheduleTable_p->size[circuitId]; i++)
		if (scheduleTable_p->scheduleInfo[circuitId][i].onTime_10m == onTime)
			return 1;

	return 0;
}

void schedule_WriteToEEPRom(uint8 scheduleType, uint8 circuitId, uint8 index) {
	if (scheduleType == DEFAULT) {
		eeprom_write_block(&(defaultScheduleTable.size[circuitId]),
				&ee_DefaultScheduleTimeSize[circuitId], sizeof(uint8));
		eeprom_write_block(
				&(defaultScheduleTable.scheduleInfo[circuitId][index]),
				&(ee_DefaultScheduleTime[circuitId][index]),
				sizeof(Schedule_Info));
	}
}

void resetScheduleTimeTable(uint8 scheduleType, uint8 circuitId) {
	Schedule_Table_st * scheduleTable_p = &defaultScheduleTable;

	if (scheduleType == DEFAULT)
		scheduleTable_p = &defaultScheduleTable;
	else if (scheduleType == TODAY)
		scheduleTable_p = &todayScheduleTable;
	else if (scheduleType == TOMORROW)
		scheduleTable_p = &tomorrowScheduleTable;

	scheduleTable_p->size[circuitId] = 0;
	schedule_WriteToEEPRom(scheduleType, circuitId, 0);
}
// End of File
