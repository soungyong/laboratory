#include "RFU_Protocol.h"
#include "NCProtocol.h"
#include "PLCS_Protocol.h"
#include "xcps.h"
#include "flash.h"

#include <avr/eeprom.h>
// --------------------------------------------------------------------------- //

uint8 tmp_BuffForRFUP[64];

void plcs_RFUP_ProcessMessage(uint16 srcNetAddr, uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint8 msgType;
	msgType = msg[8];

	switch (msgType) { // Msg Type of MDFP
	case PLCS_RFU_REQ_UPGRADE:
		plcs_RFUP_HandleUpgradeFirmwareReq(seqNum, srcId, destId, msg, length);
		break;
	case PLCS_RFU_REQ_SEND_DATA:
		plcs_RFUP_HandleSendDataReq(seqNum, srcId, destId, msg, length);
		break;
	case PLCS_RFU_RES_UPGRADE:
	case PLCS_RFU_RES_SEND_DATA:

		msg[11] = zrmpInfo_0.zrm_Id >> 8;
		msg[12] = zrmpInfo_0.zrm_Id;
		plcs_SendMessage(0, msg, length);

		{
			uint8 i = 0;
			for (i = 0; i < deviceInfoTable.size; i++)
				if (deviceInfoTable.deviceInfo[i].deviceType == 0x2090
						&& deviceInfoTable.deviceInfo[i].cnt < 200)
					plcs_SendMessage(deviceInfoTable.deviceInfo[i].nwkAddr, msg,
							length);
		}
		break;
	default:
		break;
	}
}

void plcs_RFUP_HandleUpgradeFirmwareReq(uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint16 deviceType;
	uint16 lcId;
	uint16 deviceId;

	deviceType = msg[9] << 8 | msg[10];
	lcId = msg[11] << 8 | msg[12];
	deviceId = msg[13] << 8 | msg[14];

	if (deviceType != PLCS_LC_100Z_TYPE) {
		device_Info* deviceInfo_P;
		deviceInfo_P = findNodeById(deviceId);
		if (deviceInfo_P != NULL)
			plcs_SendMessage(deviceInfo_P->nwkAddr, msg, length);
		return;
	}

	if (plcs_RFUP_CheckDownloadedFirmware()) {
		plcs_RFUP_SetReadyToUpgradeFirmware(1);
		plcs_RFUP_SendUpgradeFirmwareRes(seqNum, 0x00);
		plcs_RFUP_Reboot();
	} else {
		plcs_RFUP_SendUpgradeFirmwareRes(seqNum, 0x10);
	}

}

void plcs_RFUP_HandleSendDataReq(uint16 seqNum, uint16 srcId, uint16 destId,
		uint8 msg[], int length) {
	uint16 deviceType;
	uint16 lcId;
	uint16 deviceId;
	uint8 dataLen = 0;
	uint8 data[44];
	uint8 i = 0;
	uint16 totalLen = 0;
	uint16 index = 0;

	deviceType = msg[9] << 8 | msg[10];
	lcId = msg[11] << 8 | msg[12];
	deviceId = msg[13] << 8 | msg[14];
	totalLen = msg[15] << 8 | msg[16];
	index = msg[17] << 8 | msg[18];
	dataLen = msg[19];

	if (dataLen > 44)
		return;
	for (i = 0; i < dataLen; i++)
		data[i] = msg[20 + i];

	if (deviceType != PLCS_LC_100Z_TYPE) {
		device_Info* deviceInfo_P;
		deviceInfo_P = findNodeById(deviceId);
		if (deviceInfo_P != NULL)
			plcs_SendMessage(deviceInfo_P->nwkAddr, msg, length);
		return;
	}

	plcs_RFUP_WriteFirmwareData(index, dataLen, data);

	plcs_RFUP_SendDataRes(seqNum, 0x00);

}
////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
/////////////////////////////////////////////////////////////////////////////////////////////////////////
void plcs_RFUP_SendUpgradeFirmwareRes(uint16 seqNum, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_RFU_RES_UPGRADE;
	payload[len++] = PLCS_LC_100Z_TYPE >> 8;
	payload[len++] = (uint8) PLCS_LC_100Z_TYPE;
	payload[len++] = 0;
	payload[len++] = 0;
	payload[len++] = zrmpInfo_0.zrm_Id >> 8;
	payload[len++] = zrmpInfo_0.zrm_Id;
	payload[len++] = result;

	resultLen = plcs_GetRFUPResMessage(tmp_BuffForRFUP, 0, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(0, tmp_BuffForRFUP, resultLen);
}

void plcs_RFUP_SendDataRes(uint16 seqNum, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_RFU_RES_UPGRADE;
	payload[len++] = PLCS_LC_100Z_TYPE >> 8;
	payload[len++] = (uint8) PLCS_LC_100Z_TYPE;
	payload[len++] = 0;
	payload[len++] = 0;
	payload[len++] = zrmpInfo_0.zrm_Id >> 8;
	payload[len++] = zrmpInfo_0.zrm_Id;
	payload[len++] = result;

	resultLen = plcs_GetRFUPResMessage(tmp_BuffForRFUP, 0, zrmpInfo_0.zrm_Id,
			payload, len, seqNum);

	sendToZigbee(0, tmp_BuffForRFUP, resultLen);
}

////////////////////////////////////////////

uint8 plcs_RFUP_CheckDownloadedFirmware() {
	return 1;
}
void plcs_RFUP_SetReadyToUpgradeFirmware(uint8 value) {
	flash_writeUint8(256, value);
}
void plcs_RFUP_Reboot() {
	while (1)
		;
}
//////////////

uint16 seqNumGeneratorForRFUP = 0;
uint16 getSeqNumGeneratorForRFUP() {
	return seqNumGeneratorForRFUP++;
}

uint8 plcs_GetRFUPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGeneratorForRFUP();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_RFU_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetRFUPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_RFU_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

void plcs_RFUP_WriteFirmwareData(uint16 index, uint8 len, uint8* data) {

}
