#include "DeviceInfo.h"
#include "SSRControl.h"
#include "ScheduleTable.h"

SSR_State_Info ee_SSRInfo EEMEM;
extern uint8 applyingSchedule;

void handleDeviceControl(uint16 nodeId, int circuitId, uint8 ctrlMode) {

	tmp_SSRInfo.ctrlMode[circuitId] = ctrlMode;
	tmp_SSRInfo.state[circuitId] = ctrlMode;
	tmp_SSRInfo.expirationTime[circuitId] = 0xffff;
	switch (ctrlMode) {
	case OFF:
		break;
	case ON:
		break;
	case SENSOR:
		tmp_SSRInfo.onCtrlMode[circuitId] = ON;
		tmp_SSRInfo.offCtrlMode[circuitId] = OFF;
		break;
	case DIMMING_0:
	case DIMMING_1:
	case DIMMING_2:
	case DIMMING_3:
	case DIMMING_4:
	case DIMMING_5:
	case DIMMING_6:
	case DIMMING_7:
	case DIMMING_8:
	case DIMMING_9:
	case DIMMING_10:
		break;
	case SCHEDULE:
		applyingSchedule = TODAY;
		break;
	default:
		break;
	}
	//eeprom에 설정 정보 저장.
	ssrInfo_WriteToEEPRomOfIndex(circuitId);
}

void ssrInfo_WriteToEEPRom() {
	eeprom_write_block(&tmp_SSRInfo, &ee_SSRInfo, sizeof(SSR_State_Info));
}

void ssrInfo_WriteToEEPRomOfIndex(uint8 ssrId) {
	eeprom_write_block(&(tmp_SSRInfo.ctrlMode[ssrId]),
			&(ee_SSRInfo.ctrlMode[ssrId]), sizeof(uint8));
	eeprom_write_block(&(tmp_SSRInfo.expirationTime[ssrId]),
			&(ee_SSRInfo.expirationTime[ssrId]), sizeof(uint16));
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Initialize Mapping Table  ------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void initDeviceTable() {
	uint8 i = 0;
	deviceInfoTable.size = 0;
	/*deviceInfoTable.deviceInfo = NULL;
	 while (1) {
	 deviceInfoTable.deviceInfo = (device_Info*) malloc(
	 sizeof(device_Info) * 40);
	 if (deviceInfoTable.deviceInfo != NULL)
	 break;
	 }*/

	eeprom_read_block(&tmp_SSRInfo, &ee_SSRInfo, sizeof(SSR_State_Info));

	for (i = 0; i < NumOfChannels; i++) {
		if (tmp_SSRInfo.ctrlMode[i] == 0xff)
			tmp_SSRInfo.ctrlMode[i] = 0x01;
		tmp_SSRInfo.timerCnt[i] = 0;
	}
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Find Node Mapping Table  ------------------------------------------------------ //
// ---------------------------------------------------------------------------------------------- //
device_Info *findNodeById(uint16 node_Id) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++) {
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id)
			return &(deviceInfoTable.deviceInfo[i]);
	}
	return NULL;
}

int isContainNodeOfId(uint16 Ieee_Addr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].ieeeId == Ieee_Addr)
			return 1;
	return 0;
}

int isContainNode(uint16 node_Id, uint16 nwkAddr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].nwkAddr == nwkAddr
				&& deviceInfoTable.deviceInfo[i].ieeeId == node_Id)
			return 1;

	return 0;
}

int isContainNodeOfNwkAddr(uint16 nwk_Addr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].nwkAddr == nwk_Addr)
			return 1;

	return 0; // 찾는 값이 없으면 '0'을 리턴
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
device_Info *addDeviceTable(uint16 node_Id, uint16 nwkAddr, uint16 node_Type,
		uint8 device_version, uint8 fw_version) {
	uint8 i = 0;

	if(deviceInfoTable.size >= MaxNumOfDevice)return NULL;

	deviceInfoTable.deviceInfo[deviceInfoTable.size].ieeeId = node_Id;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].nwkAddr = nwkAddr;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].deviceType = node_Type;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].deviceVersion =
			device_version;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].fwVersion = fw_version;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].cnt = 0;

	deviceInfoTable.size++;

	return NULL;
}

int updateDeviceTable(uint16 node_Id, uint16 nwkAddr, uint16 node_Type,
		uint8 device_version, uint8 fw_version) {
	device_Info *pUpdate;

	if ((pUpdate = findNodeById(node_Id)) != NULL) {
		pUpdate->nwkAddr = nwkAddr;
		pUpdate->deviceType = node_Type;
		pUpdate->deviceVersion = device_version;
		pUpdate->fwVersion = fw_version;
		pUpdate->cnt = 0;
		return 0;
	} else {
		addDeviceTable(node_Id, nwkAddr, node_Type, device_version, fw_version);
	}
	return 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Remove Node & Mapping Table  ------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void removeNode(uint16 node_Id) {
	uint8 i = 0, j = 0;

	for (i = 0; i < deviceInfoTable.size; i++) {
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id) {
			for (j = i; j < deviceInfoTable.size - 1; j++)
				deviceInfoTable.deviceInfo[j] =
						deviceInfoTable.deviceInfo[j + 1];
			deviceInfoTable.size--;
			break;
		}
	}
}

void removeDeviceTable() {
	deviceInfoTable.size = 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Count Node & Device Table  ---------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint8 countDeviceTable(uint16 node_Type) {
	uint8 device_count = 0;
	uint8 i = 0;

	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].deviceType == node_Type)
			device_count++;
	return device_count;

}

uint8 countDevice() {
	return deviceInfoTable.size;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Get Node ID Table  ----------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint16 getNetAddr(uint16 node_Id) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id)
			return deviceInfoTable.deviceInfo[i].nwkAddr;
	return 0;
}

uint16 getNodeId(uint16 nwk_Addr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].nwkAddr == nwk_Addr)
			return deviceInfoTable.deviceInfo[i].ieeeId;
	return 0;

}

void deviceInfo_UpdateSSRInfo() {
	tmp_PrevSSRInfo = tmp_SSRInfo;
}

int deviceInfo_IsUpdatedSSRInfo() {
	uint8 i = 0;
	for (i = 0; i < NumOfChannels; i++) {
		if (tmp_PrevSSRInfo.state[i] != tmp_SSRInfo.state[i])
			return 1;
		if (tmp_PrevSSRInfo.expirationTime[i] != tmp_SSRInfo.expirationTime[i])
			return 1;
	}

	return 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Print Node & Mapping Table  ---------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void printDeviceTable() {
	uint16 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		printf_P(PSTR("\n%d: %02X-%02X"), ++i,
				deviceInfoTable.deviceInfo[i].ieeeId,
				deviceInfoTable.deviceInfo[i].nwkAddr);
}

// Enf of File
