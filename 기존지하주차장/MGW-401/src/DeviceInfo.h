#ifndef __DEVICE_INFO_H__
#define __DEVICE_INFO_H__


#include <stdio.h>
#include <string.h>  		// strcmp(), strcpy(), strlen() 함수등 주로 문자열과 관련된 함수사용시
#include <stdlib.h>  		// malloc() 함수 사용
#include <avr/pgmspace.h>
#include "Util.h"
#include "Uart.h"

#define DEVICETYPE_LC100		0x2010

//typedef struct DeviceInfo *PDEVICE_INFO;

typedef struct DeviceInfo
{	
	uint16 nodeId;
	uint16 deviceType;	
	uint8 deviceVersion;
	uint8 fwVersion;	
}device_Info;

typedef struct {
	device_Info deviceInfo[20];
	uint8 size;
} DeviceInfoTable_st;


DeviceInfoTable_st deviceInfoTable;

//--------------------------------------------------------------------------------------------//
void initDeviceTable();

device_Info *findNodeById(uint16 node_Id);
int isContainNodeOfId(uint16 node_Id);

device_Info *addDeviceTable(uint16 node_Id, uint8 device_version, uint8 fw_version);
int updateDeviceTable(uint16 node_Id, uint8 device_version, uint8 fw_version);


void removeNode(uint16 node_Id);
void removeDeviceTable();


uint8 countDeviceTable(uint16 node_Type);
uint8 countDevice();


uint16 getzigbeeID(uint32 node_Id);
uint32 getnodeID(uint16 zigbee_Id);

void printDeviceTable();

#endif

