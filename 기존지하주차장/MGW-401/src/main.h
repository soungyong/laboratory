#ifndef __MAIN_H__
#define __MAIN_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/iom128.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>


#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "../iinchip/types.h"
#include "../iinchip/w5100.h"
#include "../iinchip/socket.h"
#include "Util.h"
#include "Uart.h"
#include "Xcps.h"
#include "Xcpsnet.h"
#include "NCProtocol.h"
#include "XNetProtocol.h"
#include "DeviceInfo.h"
#include "DS1307_i2c.h"
#include "Timer.h"



/********/
//#include <avrx.h>
//#include <avrx-io.h>
//#include <avrx-signal.h>
/********/


/**/

#define STATE_LED_ON()   	(PORTD &= ~0x10)
#define STATE_LED_OFF()  	(PORTD |=  0x10)

/**/


#if (__DEF_IINCHIP_BUS__ == __DEF_IINCHIP_SPI_MODE__)
#include "iinchip/spi.h"
#endif

void InitMCU();

int hexToint(uint8 tmp_hex);

void Before_Config_Display();

void After_Config_Display();

void parsing(char *String, uint8 *data);

void GateWaySET();

void WDT_INIT();

#endif

