#ifndef __SCHEDULE_TIME_H__
#define __SCHEDULE_TIME_H__


#include <string.h>  		// strcmp(), strcpy(), strlen() 함수등 주로 문자열과 관련된 함수사용시
#include <stdlib.h>  		// malloc() 함수 사용
#include <stdio.h>
#include <avr/pgmspace.h>

#include "../iinchip/types.h"
#include "Uart.h"


typedef struct TIMETABLE
{
	uint8 On_StartTime;
	uint8 On_EndTime;

}Schedule_Table;

Schedule_Table *pNodeTime;

int addScheduleTable(uint8 tmp_TimeCnt, uint8 *msg, int tmp_length);

void printScheduleTable();

uint8 Find_StartTime(uint8 startTime);

uint8 Find_EndTime(uint8 endTime);

uint8 Get_ScheduleCount();

#endif
