#include "PLCS_Protocol_LC100.h"
#include "PLCS_Protocol_Server.h"
#include "XNetProtocol.h"

/////////////////
uint8 tmp_Buff[64];
///////////////////
uint16 token = -1; //-1: 할당된 LC 없음. 0: GW가 사용중. 1~0xffff: 해당 ID의 LC가 사용중.

// --------------------------------------------------------------------------- //
// --------------------- Process PLCS (Gateway <-> LC100)  ---------------------- //
// --------------------------------------------------------------------------- //
void plcs_GCP_LC100_ProcessMessage(uint16 seqNum, uint16 srcId, uint16 destId,
		uint8 msg[], int length) {
	uint8 msgType;
	msgType = msg[8];

	switch (msgType) {
	case PLCS_GCP_NOTICEEVENT:
		if (isOnSensorSharing)
			plcs_GCP_LC100_HandleNoticeEventNeighbor(seqNum, srcId, destId, msg,
					length);

		msg[6] = ncp_GW_Id >> 8;
		msg[7] = ncp_GW_Id;

		sendToServer(msg, length);
		break;
	default:
		msg[6] = ncp_GW_Id >> 8;
		msg[7] = ncp_GW_Id;

		sendToServer(msg, length);
		break;
	}
}

void plcs_NCP_LC100_ProcessMessage(uint16 seqNum, uint16 srcId, uint16 destId,
		uint8 msg[], int length) {
	uint8 msgType;
	msgType = msg[8];

	switch (msgType) {
	case NCP_REQ_REGISTER:
		plcs_NCP_LC100_HandleRegisterReq(seqNum, srcId, destId, msg, length);
		break;
	case NCP_REQ_PING:
		plcs_NCP_LC100_HandlePingReq(seqNum, srcId, destId, msg, length);
		break;
	case NCP_REQ_REGISTER_NODE:
		plcs_NCP_LC100_HandleRegisterNodeReq(seqNum, srcId, destId, msg,
				length);
		break;
	case NCP_REQ_TOKEN_RELEASE:
		plcs_NCP_LC100_HandleReleaseTokenReq(seqNum, srcId, destId, msg,
				length);
		break;
	default:
		break;
	}
}

void plcs_NCP_LC100_HandleRegisterReq(uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint16 deviceType;
	uint8 device_Ver;
	uint8 fw_Ver;
	uint8 min, hour;

	//msg[8] = messageType
	min = ds1307_read(1);
	hour = ds1307_read(2);

	deviceType = (uint16) ((msg[9] << 8) | msg[10]);
	device_Ver = msg[11];
	fw_Ver = msg[12];

	updateDeviceTable(srcId, device_Ver, fw_Ver);
	plcs_NCP_LC100_SendTimeInfo(srcId, hour, min);
	plcs_NCP_SERVER_SendRegisterNodeReq(srcId, deviceType, srcId, device_Ver,
			fw_Ver, srcId);

	plcs_NCP_LC100_SendRegisterRes(seqNum, srcId, 0, 30);

}

void plcs_NCP_LC100_HandlePingReq(uint16 seqNum, uint16 srcId, uint16 destId,
		uint8 msg[], int length) {
	uint8 protocol_Ver;
	uint8 flag = 0;

	//msg[8] = messageType
	protocol_Ver = msg[9];
	flag = msg[10];

	// Send Response Ping
	if (isContainNodeOfId(srcId))
		plcs_NCP_LC100_SendPingRes(seqNum, srcId, flag, PING_TIME);

	//timer_set(NCP_PING_TIMER_ID, 1000);
}

void plcs_NCP_LC100_HandleRegisterNodeReq(uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint16 subNodeId;
	uint16 deviceType;
	uint16 deviceId;
	uint8 deviceVersion;
	uint8 firmVersion;
	uint16 netAddr;

	//msg[8] = messageType
	subNodeId = msg[9] << 8 | msg[10];
	deviceType = msg[11] << 8 | msg[12];
	deviceId = msg[13] << 8 | msg[14];
	deviceVersion = msg[15];
	firmVersion = msg[16];
	netAddr = msg[17] << 8 | msg[18];

	plcs_NCP_SERVER_SendRegisterNodeReq(subNodeId, deviceType, deviceId,
			deviceVersion, firmVersion, netAddr);
}

void plcs_GCP_LC100_HandleNoticeEventNeighbor(uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 sensorId;
	uint8 state;
	uint8 i;

	//msg[8] = messageType
	nodeId = msg[9] << 8 | msg[10];
	sensorId = msg[12] << 8 | msg[13];

	if (sensorId == 0)
		return;

	//if(nodeId==0x000d) return;

	if (isOnSensorSharing)
		for (i = 0; i < deviceInfoTable.size; i++)
			if (nodeId != deviceInfoTable.deviceInfo[i].nodeId) {
				plcs_GCP_LC100_SendNoticeEventNeighbor(
						deviceInfoTable.deviceInfo[i].nodeId, nodeId, sensorId);
			}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//SendProtocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void plcs_NCP_LC100_SendPingRes(uint16 seqNum, uint16 nodeId, uint8 flag,
		uint16 pingInterval) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_RES_PING;
	payload[len++] = flag;
	payload[len++] = pingInterval >> 8;
	payload[len++] = pingInterval;

	resultLen = plcs_GetNCPResMessage(tmp_Buff, nodeId, ncp_GW_Id, payload, len,
			seqNum);
	sendToController(tmp_Buff, resultLen);
	MSLEEP(3);
}

void plcs_NCP_LC100_SendRegisterRes(uint16 seqNum, uint16 nodeId, uint16 result,
		uint16 timeout) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_RES_REGISTER;
	payload[len++] = (uint8) (result >> 8);
	payload[len++] = (uint8) (result);
	payload[len++] = (uint8) (timeout >> 8);
	payload[len++] = (uint8) (timeout);

	resultLen = plcs_GetNCPResMessage(tmp_Buff, nodeId, ncp_GW_Id, payload, len,
			seqNum);
	sendToController(tmp_Buff, resultLen);

	MSLEEP(3);
}

void plcs_NCP_LC100_SendTimeInfo(uint16 nodeId, uint8 hour, uint8 min) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_SEND_TIME_INFO;
	payload[len++] = hour;
	payload[len++] = min;

	resultLen = plcs_GetNCPMessage(tmp_Buff, nodeId, ncp_GW_Id, payload, len);
	sendToController(tmp_Buff, resultLen);

	MSLEEP(3);
}

uint16 plcs_NCP_LC100_GetToken() {
	return token;
}

char plcs_NCP_LC100_AssignToken(uint16 nodeId) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	if (token != -1)
		return -1; //token 이미 할당된후 해제 되지 않음.
	if (nodeId == token) { //이미 할당된 노드. 할당 해제.
		plcs_NCP_LC100_SendReleaseTokenRes(nodeId, 0);
		return -1;
	}

	MSLEEP(5);
	payload[len++] = NCP_ASSIGN_TOKEN;
	payload[len++] = nodeId >> 8;
	payload[len++] = nodeId;

	resultLen = plcs_GetNCPMessage(tmp_Buff, nodeId, ncp_GW_Id, payload, len);
	sendToController(tmp_Buff, resultLen);
	return 0;
}

void plcs_NCP_LC100_HandleReleaseTokenReq(uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	plcs_NCP_LC100_SendReleaseTokenRes(srcId, seqNum);

	token = -1;
}

void plcs_NCP_LC100_SendReleaseTokenRes(uint16 nodeId, uint16 seqNum) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_RES_TOKEN_RELEASE;
	payload[len++] = nodeId >> 8;
	payload[len++] = nodeId;

	resultLen = plcs_GetNCPResMessage(tmp_Buff, nodeId, ncp_GW_Id, payload, len,
			seqNum);
	sendToController(tmp_Buff, resultLen);

}
void plcs_NCP_LC100_SendRegisterBroadcastReq(char flag) {
	int len = 0;
	uint8 payload[60];
	uint8 i;
	uint8 resultLen;

	payload[len++] = NCP_REQ_REGISTER_BROADCAST;
	payload[len++] = 0xff;
	payload[len++] = 0xff;
	payload[len++] = flag;
	payload[len++] = deviceInfoTable.size;
	for (i = 0; i < deviceInfoTable.size; i++) {
		payload[len++] = deviceInfoTable.deviceInfo[i].nodeId >> 8;
		payload[len++] = deviceInfoTable.deviceInfo[i].nodeId;
	}

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0xffff, ncp_GW_Id, payload, len);
	sendToController(tmp_Buff, resultLen);
}

void plcs_GCP_LC100_SendNoticeEventNeighbor(uint16 destNodeId, uint16 lcId,
		uint16 sensorId) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen;

	payload[len++] = PLCS_GCP_NOTICEEVENT_NEIGHBOR;
	payload[len++] = destNodeId >> 8;
	payload[len++] = destNodeId;
	payload[len++] = lcId >> 8;
	payload[len++] = lcId;
	payload[len++] = sensorId >> 8;
	payload[len++] = sensorId;

	resultLen = plcs_GetGCPMessage(tmp_Buff, destNodeId, ncp_GW_Id, payload, len);
	sendToController(tmp_Buff, resultLen);
}

void plcs_GCP_LC100_SendControlNoticeMode(uint16 destNodeId, uint8 enable) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen;

	payload[len++] = PLCS_GCP_REQ_CONTROL_NOTICEMODE;
	payload[len++] = destNodeId >> 8;
	payload[len++] = destNodeId;
	payload[len++] = enable;

	resultLen = plcs_GetGCPMessage(tmp_Buff, destNodeId, ncp_GW_Id, payload, len);
	sendToController(tmp_Buff, resultLen);
}

