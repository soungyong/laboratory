
#include "ScheduleTime.h"


static uint8 Schedule_Count;

// --------------------------------------------------------------------------------------------------------- //
// ------------------ Add Schedule Table ---------------------------------------------------------------------- //
// --------------------------------------------------------------------------------------------------------- //
int addScheduleTable(uint8 tmp_TimeCnt, uint8 *msg, int tmp_length)
{
	uint8 i = 0;
	
	pNodeTime = (struct TIMETABLE *)malloc(sizeof(struct TIMETABLE) * tmp_TimeCnt);

	if(pNodeTime == NULL)
		return 0;
	
	for(i = 0; i < tmp_TimeCnt; i ++)
	{
		pNodeTime[i].On_StartTime = msg[(i * 2)];
		pNodeTime[i].On_EndTime = msg[1+(i * 2)];
	}

	Schedule_Count = tmp_TimeCnt;

	printScheduleTable();

	return 0;
}


void printScheduleTable()
{
	Schedule_Table *printTable = pNodeTime;

	uint8 i = 0;

	for(i = 0; i < Get_ScheduleCount(); i++)
	{	
		printf_P(PSTR("\n Start Time : %d:%02d, End Time : %d:%02d"), (printTable[i].On_StartTime / 6), (printTable[i].On_StartTime % 6) * 10, (printTable[i].On_EndTime / 6), (printTable[i].On_EndTime % 6) * 10);
		printf_P(PSTR("\n Start Time : %02x, End Time : %02x"), printTable[i].On_StartTime, printTable[i].On_EndTime);
	}
}


uint8 Find_StartTime(uint8 startTime)
{
	uint8 i = 0;
	
	Schedule_Table *pFindStart = pNodeTime;

	for(i = 0; i < Get_ScheduleCount(); i++)
	{
		if(pFindStart[i].On_StartTime == startTime)
			return 1;
	}

	return 0;
}


uint8 Find_EndTime(uint8 endTime)
{
	uint8 i = 0;
	
	Schedule_Table *pFindEnd = pNodeTime;

	for(i = 0; i < Get_ScheduleCount(); i++)
	{
		if(pFindEnd[i].On_EndTime == endTime)
			return 1;
	}

	return 0;
}


uint8 Get_ScheduleCount()
{
	return Schedule_Count;
}


// End of File
