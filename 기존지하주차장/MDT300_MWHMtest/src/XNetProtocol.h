#ifndef __XNET_PROTOCOL_H__
#define __XNET_PROTOCOL_H__
#include "Util.h"

// --------------- XNetMessage Handler --------------- //
uint8 XNetHandlerFromZigbee(uint16 srcNetAddr, uint8 buff[], int buff_length);
#endif

