#include "RFU_Protocol.h"
#include "GC_Protocol.h"
#include "xcps.h"
#include "flash.h"
#include <avr/wdt.h>

#include <avr/eeprom.h>
// --------------------------------------------------------------------------- //

uint8 tmp_BuffForRFUP[64];

void plcs_RFUP_ProcessMessage(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId,
		uint8 msg[], int length) {
	uint8 msgType;

	msgType = msg[8];

	switch (msgType) { // Msg Type of MDFP
	case PLCS_RFU_REQ_UPGRADE:
		plcs_RFUP_HandleUpgradeFirmwareReq(seqNum, srcId, destId, msg, length);
		break;
	case PLCS_RFU_REQ_SEND_DATA:
		plcs_RFUP_HandleSendDataReq(seqNum, srcId, destId, msg, length);
		break;
	default:
		break;
	}
}

void plcs_RFUP_HandleUpgradeFirmwareReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[],
		int length) {
	uint16 deviceType;
	uint16 lcId;
	uint16 deviceId;

	deviceType = msg[9] << 8 | msg[10];
	lcId = msg[11] << 8 | msg[12];
	deviceId = msg[13] << 8 | msg[14];

	if (plcs_RFUP_CheckDownloadedFirmware(DATA_PAGE_ADDR)) {
		plcs_RFUP_SetReadyToUpgradeFirmware(1);
		plcs_RFUP_SendUpgradeFirmwareRes(seqNum, 0x00);
		//plcs_RFUP_Reboot();
	} else {
		plcs_RFUP_SetReadyToUpgradeFirmware(0);
		plcs_RFUP_SendUpgradeFirmwareRes(seqNum, 0x10);
	}

}

void plcs_RFUP_HandleSendDataReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[],
		int length) {
	uint16 deviceType;
	uint16 lcId;
	uint16 deviceId;
	uint8 dataLen = 0;
	uint8 data[21];
	uint8 i = 0;
	uint16 totalLen = 0;
	uint16 index = 0;

	deviceType = msg[9] << 8 | msg[10];
	lcId = msg[11] << 8 | msg[12];
	deviceId = msg[13] << 8 | msg[14];
	totalLen = msg[15] << 8 | msg[16];
	index = msg[17] << 8 | msg[18];
	dataLen = msg[19];

	if (dataLen > 21)
		return;

	for (i = 0; i < 21; i++)
		data[i] = 0;

	for (i = 0; i < dataLen; i++)
		data[i] = msg[20 + i];

	if (plcs_RFUP_WriteFirmwareData(totalLen, index, 21, data)) {
		if (plcs_RFUP_CheckDownloadedFirmwareAt(index))
			plcs_RFUP_SendDataRes(seqNum, 0x00);
		else
			plcs_RFUP_SendDataRes(seqNum, 0x10);

	} else
		plcs_RFUP_SendDataRes(seqNum, 0x10);

}
////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
/////////////////////////////////////////////////////////////////////////////////////////////////////////
void plcs_RFUP_SendUpgradeFirmwareRes(uint16 seqNum, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_RFU_RES_UPGRADE;
	payload[len++] = PLCS_ZDIMMER_TYPE >> 8;
	payload[len++] = (uint8) PLCS_ZDIMMER_TYPE;
	payload[len++] = 0;
	payload[len++] = 0;
	payload[len++] = tmp_zrmp.zrm_Id >> 8;
	payload[len++] = tmp_zrmp.zrm_Id;
	payload[len++] = result;

	resultLen = plcs_GetRFUPResMessage(tmp_BuffForRFUP, 0, tmp_zrmp.zrm_Id, payload, len, seqNum);

	sendToZigbee(0, tmp_BuffForRFUP, resultLen);
}

void plcs_RFUP_SendDataRes(uint16 seqNum, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_RFU_RES_SEND_DATA;
	payload[len++] = PLCS_ZDIMMER_TYPE >> 8;
	payload[len++] = (uint8) PLCS_ZDIMMER_TYPE;
	payload[len++] = 0;
	payload[len++] = 0;
	payload[len++] = tmp_zrmp.zrm_Id >> 8;
	payload[len++] = tmp_zrmp.zrm_Id;
	payload[len++] = result;

	resultLen = plcs_GetRFUPResMessage(tmp_BuffForRFUP, 0, tmp_zrmp.zrm_Id, payload, len, seqNum);

	sendToZigbee(0, tmp_BuffForRFUP, resultLen);
}

////////////////////////////////////////////

uint8 plcs_RFUP_CheckDownloadedFirmware() {
	uint8 data[256];
	uint16 i = 0;
	uint8 j = 0;
	uint8 k = 0;
	uint8 len;
	uint16 addrOffset;
	uint8 dataType;
	uint8 checksumTemp = 0;

	for (i = 0; i < 32; i++) {
		data[i] = 0;
	}

	for (i = DATA_PAGE_ADDR; i < 512; i++) {
		wdt_reset();
		flash_readBytes(i, data, 255);

		for (j = 0; j < 252; j += 21) {
			wdt_reset();
			len = data[0 + j];
			addrOffset = data[1 + j] << 8 | data[2 + j];
			dataType = data[3 + j];
			//verification process 필요.

			if (len == 0 && dataType == 0x01)
				return 1; //정상 종료

			//if(dataType!=0x00) return 0;

			checksumTemp = 0;
			for (k = 0; k < 21; k++)
				checksumTemp += data[j + k];

			if (checksumTemp != 0)
				return 0;
		}
	}
	return 1;
}
uint8 plcs_RFUP_CheckDownloadedFirmwareAt(uint16 index) {
	uint8 data[256];
	uint16 i = 0;
	uint8 j = 0;
	uint8 k = 0;
	uint8 len;
	uint16 addrOffset;
	uint8 dataType;
	uint8 checksumTemp = 0;

	flash_readBytes(DATA_PAGE_ADDR + index / 12, data, 255);

	for (j = 21 * (index % 12); j < (21 * (index % 12 + 1)); j += 21) {
		wdt_reset();
		len = data[0 + j];
		addrOffset = data[1 + j] << 8 | data[2 + j];
		dataType = data[3 + j];
		//verification process 필요.

		if (len == 0 && dataType == 0x01)
			return 1; //정상 종료

		//if(dataType!=0x00) return 0;

		checksumTemp = 0;
		for (k = 0; k < 21; k++)
			checksumTemp += data[j + k];

		if (checksumTemp != 0)
			return 0;
	}
	return 1;
}
void plcs_RFUP_SetReadyToUpgradeFirmware(uint8 value) {
	flash_writeUint8(FLAG_PAGE_ADDR, value);
}
void plcs_RFUP_Reboot() {
	while (1)
		;
}
//////////////

uint16 seqNumGeneratorForRFUP = 0;
uint16 getSeqNumGeneratorForRFUP() {
	return seqNumGeneratorForRFUP++;
}

uint8 plcs_GetRFUPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[],
		uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGeneratorForRFUP();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_RFU_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetRFUPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[],
		uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_RFU_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_RFUP_WriteFirmwareData(uint16 totalLen, uint16 index, uint8 len, uint8* data) {
	uint8 data2[252];
	uint8 i = 0;
	uint8 flashIndex = 0;

	if (index > totalLen)
		return 0;

	flashIndex = index % 12;

	flash_readBytes(DATA_PAGE_ADDR + (index / 12), data2, 252);

	for (i = 0; i < len; i++)
		data2[21 * flashIndex + i] = data[i];

	flash_writeBytes(DATA_PAGE_ADDR + (index / 12), data2, 252);

	flash_readBytes(DATA_PAGE_ADDR + (index / 12), data2, 252);

	for (i = 0; i < len; i++)
		if (data[i] != data2[i + 21 * flashIndex])
			return 0;

	return 1;
}
