/*
 * Debug.h
 *
 *  Created on: 2012. 7. 25.
 *      Author: GoldSunda
 */

#ifndef MYDEBUG_H_
#define MYDEBUG_H_

#include <avr/pgmspace.h>
#include <stdio.h>

#define DEBUG(FORMAT,args...) printf_P(PSTR(FORMAT),##args)
//#define DEBUG(...) ;
#define SPRINTF(target,FORMAT,args...) sprintf_P(target,PSTR(FORMAT),##args)
#define STRNCMP(target,FORMAT,args...) strncmp_P(target,PSTR(FORMAT),##args)

#endif /* DEBUG_H_ */
