#include "GC_Protocol.h"
#include "NCProtocol.h"
#include "xcps.h"
#include "flash.h"
#include "timer.h"

uint8 ncp_NetState = NCP_NET_NOT_CONNECT;
// --------------------------------------------------------------------------- //

uint8 tmp_Buff[64];

void plcs_GCP_ProcessMessageFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[],
		int length) {
	uint8 msgType;
	uint16 seqNum;

	seqNum = msg[2] << 8 | msg[3];
	msgType = msg[8];

	switch (msgType) { // Msg Type of MDFP
	case PLCS_GCP_REQ_POWERMETER:
		plcs_GCP_HandlePowermeterReq(seqNum, srcId, dstId, msg, length);
		break;
	case PLCS_GCP_REQ_UPDATEPOWERMETER:
		plcs_GCP_HandleUpdatePowermeterReq(seqNum, srcId, dstId, msg, length);
		break;
	case PLCS_GCP_REQ_STATEINFO:
		plcs_GCP_HandleStateInfoReq(seqNum, srcId, dstId, msg, length);
		break;
	case PLCS_GCP_REQ_CONTROL_REBOOT:
		while(1);
		break;
	default:

		break;
	}
}

void NCP_ProcessMessageFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[], int length) {
	uint8 msgType;

	msgType = msg[8];

	switch (msgType) { // Msg Type of NCP
	case NCP_RES_PING:
		ncp_ProcessPingResFromZigbee(srcId, dstId, msg, length);
		break;
	case NCP_RES_REGISTER:
		ncp_ProcessRegisterResFromZigbee(srcId, dstId, msg, length);
		break;
	}
}

void ncp_ProcessRegisterResFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[],
		int length) {
	int i = 0;
	uint16 nodeId;
	uint16 seqNum;
	uint16 result = 0;
	// Request Register

	//msg[8] == messageType
	seqNum = msg[2] << 8 | msg[3];
	nodeId = (uint16) ((msg[6] << 8) | (msg[7]));
	result = (uint16) ((msg[9] << 8) | (msg[10]));

	if (result == 0)
		ncp_NetState = NCP_NET_REGISTER;
	else
		ncp_NetState = NCP_NET_NOT_CONNECT;
}

void ncp_ProcessPingResFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[], int length) {
	uint16 seqNum;
	uint8 flag = 0;

	//msg[8] = messageType
	seqNum = msg[2] << 8 | msg[3];
	flag = msg[10];

	if (flag == 0x01)
		ncp_NetState = NCP_NET_NOT_CONNECT;
}

void ncp_SendPingReq() {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_REQ_PING;
	payload[len++] = 0;
	payload[len++] = 0;

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, tmp_zrmp.zrm_Id, payload,
			len);

	sendToZigbee(0, tmp_Buff, resultLen);
}

void ncp_SendRegisterReq() {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_REQ_REGISTER;
	payload[len++] = (uint8) (PLCS_MDF_TYPE >> 8);
	payload[len++] = (uint8) PLCS_MDF_TYPE;
	payload[len++] = PLCS_MDF_FW_VER;
	payload[len++] = PLCS_MDF_FW_VER;

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, tmp_zrmp.zrm_Id, payload,
			len);
	sendToZigbee(0, tmp_Buff, resultLen);
}

uint8 ncp_ConnState() {
	return ncp_NetState;
}
void ncp_setNCPState(uint8 state){
	ncp_NetState = state;
}

void plcs_GCP_HandlePowermeterReq(uint16 seqNum, uint16 srcId, uint16 destId,
		uint8 msg[], int length) {
	uint8 channel = 0;

	channel = msg[13];

	if (channel < 4)
		plcs_GCP_SendPowermeterRes(seqNum, tmp_zrmp.zrm_Id, channel,
				g_PowerMeterValue[channel]);
}

void plcs_GCP_HandleUpdatePowermeterReq(uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 channel;
	uint32 powerMeterValue;

	//msg[8] = message type
	nodeId = msg[11] << 8 | msg[12];
	channel = msg[13];
	powerMeterValue = ((uint32) msg[14]) << 24 | ((uint32) msg[15]) << 16
			| ((uint32) msg[16]) << 8 | ((uint32) msg[17]);

	if (channel < 4) {
		g_PowerMeterValue[channel] = powerMeterValue;
		flash_writeUint32(channel, g_PowerMeterValue[channel]);

		plcs_GCP_SendPowermeterRes(0, tmp_zrmp.zrm_Id, channel, powerMeterValue);
	}

}

void plcs_GCP_HandleStateInfoReq(uint16 seqNum, uint16 srcId, uint16 destId,
		uint8 msg[], int length) {

	plcs_GCP_SendStateInfoRes(seqNum, tmp_zrmp.zrm_Id, g_Temperature,
			g_Illumination);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
/////////////////////////////////////////////////////////////////////////////////////////////////////////
uint16 seqNumGenerator = 0;
uint16 getSeqNumGenerator() {
	return seqNumGenerator++;
}

uint8 plcs_GetNCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGenerator();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetNCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetGCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGenerator();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_GCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetGCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;
	uint8 i=0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_GCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

void plcs_GCP_SendPowermeterRes(uint16 seqNum, uint16 nodeId, uint8 channel,
		uint32 powermeterValue) {
	int len = 0;
	uint8 payload[20];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_POWERMETER;
	payload[len++] = 0;
	payload[len++] = 0;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) channel;
	payload[len++] = (uint8) (powermeterValue >> 24);
	payload[len++] = (uint8) (powermeterValue >> 16);
	payload[len++] = (uint8) (powermeterValue >> 8);
	payload[len++] = (uint8) (powermeterValue);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, tmp_zrmp.zrm_Id, payload,
			len, seqNum);

	sendToZigbee(0, tmp_Buff, resultLen);
}

void plcs_GCP_SendStateInfoRes(uint16 seqNum, uint16 nodeId, uint16 temperature,
		uint16 illuminance) {
	int len = 0;
	uint8 payload[20];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_STATEINFO;
	payload[len++] = (uint8) 0;
	payload[len++] = (uint8) 0;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (PLCS_MDF_TYPE >> 8);
	payload[len++] = (uint8) PLCS_MDF_TYPE;
	payload[len++] = (uint8) (temperature >> 8);
	payload[len++] = (uint8) (temperature);
	payload[len++] = (uint8) (illuminance >> 8);
	payload[len++] = (uint8) (illuminance);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, tmp_zrmp.zrm_Id, payload,
			len, seqNum);

	sendToZigbee(0, tmp_Buff, resultLen);
}
