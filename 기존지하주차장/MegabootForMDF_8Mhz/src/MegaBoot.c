#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <avr/boot.h>
#include <avr/wdt.h>
#include <avr/delay.h>

#include "Flash.h"

#define FLAG_PAGE_ADDR 16
#define DATA_PAGE_ADDR 17

#define  FALSE		0
#define  TRUE		1

#define  OFF		0
#define  ON			1

//*****************************************************************************
// MCU selection
//*****************************************************************************

#ifdef __AVR_ATmega8__
#define MEGATYPE	8		// ATmega8
#endif

#ifdef __AVR_ATmega328P__
#define MEGATYPE	9		// ATmega8
#endif

#ifdef __AVR_ATmega16__
#define MEGATYPE  16		// ATmega16
#endif

#ifdef __AVR_ATmega32__
#define MEGATYPE  32		// ATmega32
#endif

#ifdef __AVR_ATmega48__
#define MEGATYPE  48		// ATmega48
#endif

#ifdef __AVR_ATmega64__
#define MEGATYPE  64		// ATmega64
#endif

#ifdef __AVR_ATmega88__
#define MEGATYPE  88		// ATmega88
#endif

#ifdef __AVR_ATmega128__
#define MEGATYPE  128		// ATmega128
#endif

#ifdef __AVR_ATmega162__
#define MEGATYPE  162		// ATmega162
#endif

#ifdef __AVR_ATmega165__
#define MEGATYPE  165		// ATmega165
#endif

#ifdef __AVR_ATmega168__
#define MEGATYPE  168		// ATmega168
#endif

#ifdef __AVR_ATmega169__
#define MEGATYPE  169		// ATmega169
#endif

#ifdef __AVR_ATmega8515__
#define MEGATYPE  8515		// ATmega8515
#endif

#ifdef __AVR_ATmega8535__
#define MEGATYPE  8535		// ATmega8535
#endif

#ifdef __AVR_ATmega640__
#define MEGATYPE  640		// ATmega640
#endif

#ifdef __AVR_ATmega1280__
#define MEGATYPE  1280		// ATmega1280
#endif

#ifdef __AVR_ATmega1281__
#define MEGATYPE  1281		// ATmega1281
#endif

#ifdef __AVR_ATmega2560__
#define MEGATYPE  2560		// ATmega2560
#endif

#ifdef __AVR_ATmega2561__
#define MEGATYPE  2561		// ATmega2561
#endif

//*****************************************************************************
// Crystal speed
// frequancy of your MCU speed
//*****************************************************************************
//#define CPU_CLK			16000000L		// Clock 16Mhz
#define CPU_CLK			8000000L		// Clock 8Mhz
//*****************************************************************************
// Bootload on UART x	(사용할 UART 포트 선택)
// ATmega64, 128, 162, 1281, 2561 = UART 2PORT
// ATmega640, 1280, 2560 = UART 4PORT
//
// 이외의 나머지 디바이스들은 포트가 하나이므로 설정을 할 필요없다.
//*****************************************************************************
#define UART        0
//#define UART       1
//#define UART       2
//#define UART       3

//*****************************************************************************
// BaudRate
// If you don't specify the baudrate divisor the bootloader
// will automaticaly be in AutoBaudRate mode
//*****************************************************************************

// 지정 보레이트
// 직접 지정할수있으며 리마크시는 AutoBaudRate를 실행한다.
// 16Mhz
#define BAUDRATE     8		// 115200bps
//#define BAUDRATE     16		// 57600bps
//#define BAUDRATE     25		// 38400bps
//#define BAUDRATE     51		// 19200bps
//#define BAUDRATE     103		// 9600bps

//8Mhz
//#define BAUDRATE     3		// 115200bps
//#define BAUDRATE     8		// 57600bps
//#define BAUDRATE     12		// 38400bps
//#define BAUDRATE     25		// 19200bps
//#define BAUDRATE     51		// 9600bps

// 자동 보레이트
#if (CPU_CLK == 16000000L)
#define  B9600		103
#define  B19200		51
#define  B38400		25
#define  B57600		16
#define  B115200	8
#endif

#if (CPU_CLK == 8000000L)
#define  B9600		51
#define  B19200		25
#define  B38400		12
#define  B57600		8
#define  B115200	3
#endif

/*
 // UART Definitions
 //these two defines establish the baud rate
 // quartz crystal frequency [Hz]
 // Baud rate
 #define baud 19200
 #define UBRRLO_VALUE ((CPU_CLK/16/baud-1)&0xFF)
 #define UBRRHI_VALUE ((CPU_CLK/16/baud-1)>>8)
 */

//*****************************************************************************
// EEprom programming
// enable EEprom programing via bootloader
// YOU MUST SET TO 1024 WORD THE BOOTLOADER SIZE IF EEPROM
// PROGRAMMING IS USE
//*****************************************************************************
#define EEPROM

#define SETTINGBIT	// FUSESBIT , LOCKBIT 읽기
//*****************************************************************************
// LockBit programming
// enable LOCKBIT programing via bootloader
//*****************************************************************************
#define LOCKBIT

//*****************************************************************************
// FusesBit programming
// enable FUSESBIT programing via bootloader
//*****************************************************************************
#define FUSESBIT

// LockBit에 사용
#define	BLB12	5
#define	BLB11	4
#define	BLB02	3
#define	BLB01	2
#define LB2		1
#define LB1		0

//*****************************************************************************
// Device ID
//*****************************************************************************

#define Mega8           'A'
#define Mega16          'B'
#define Mega32          'E'
#define Mega48          'L'
#define Mega64          'C'
#define Mega88          'M'
#define Mega128         'D'
#define Mega162         'F'
#define Mega165         'G'
#define Mega168         'H'
#define Mega169         'I'
#define Mega8515        'J'
#define Mega8535        'K'
#define Mega640			0x81
#define Mega1280        0x82
#define Mega1281        0x83
#define Mega2560        0x84
#define Mega2561        0x85

#define Flash1k         'g'
#define Flash2k         'h'
#define Flash4k         'i'
#define Flash8k         'l'
#define Flash16k        'm'
#define Flash32k        'n'
#define Flash64k        'o'
#define Flash128k       'p'
#define Flash256k       'q'
#define Flash40k        'r'

#define EEprom64        '.'
#define EEprom128       '/'
#define EEprom256       '0'
#define EEprom512       '1'
#define EEprom1024      '2'
#define EEprom2048      '3'
#define EEprom4096      '4'

#define Boot128         'a'
#define Boot256         'b'
#define Boot512         'c'
#define Boot1024        'd'
#define Boot2048        'e'
#define Boot4096        'f'

#define Page32          'Q'
#define Page64          'R'
#define Page128         'S'
#define Page256         'T'
#define Page512         'V'

#ifdef __AVR_ATmega328P__
#define  DeviceID       Mega8
#define  FlashSize      Flash32k
#define  PageSize       Page256
#define  EEpromSize     EEprom1024
#define  PageByte       128
#define  NSHIFTPAGE     7
#define  INTVECREG      MCUCR
#define  PULLUPPORT     PORTD
#define  PULLUPPIN      0x01
// SELECTION DU BOOT SIZE
#define  BootSize       Boot1024	// (0xC00) = 0x1800
#endif

#if ((MEGATYPE==9))
#if (UART == 0)
#define  UCSRA          UCSR0A
#define  UCSRB          UCSR0B
#define  UCSRC          UCSR0C
#define  UBRRL          UBRR0L
#define  UBRRH          UBRR0H
#define  UDR            UDR0
#endif
#endif
/*
 #if (!(defined UART) || (UART == 0))
 #define _RX_INLINE
 #endif


 #ifdef  _RX_INLINE
 #define     IsChar()    (UCSRA & 0x80)
 #define     RxChar()    UDR
 #endif
 */

/*****************************************************************************/
// Host
// define jump to bootloader command
#define BOOTLOADER_JUMP_CHAR '$'

/*****************************************************************************/
// BootLoader
// define flash program
#define FLASH_PROGRAM_CHAR '('
// define eeprom program
#define EEPROM_PROGRAM_CHAR ')'
// define bootloader ready for file character
#define READY_CHAR '?'
// define bootloader active char
#define BOOTLOADER_ACTIVE_CHAR '^'
// define line complete with no error character
#define LINE_COMPLETE_CHAR '~'	// 오류없는 행을 수신함
// define checksum error character
#define CS_ERROR_CHAR '-'		// 수신된 행에서 체크섬 오류 검출됨
// define check flash error character
#define CF_ERROR_CHAR '%'		// 오류없는 행을 수신함, 그러나 플래시 오류를 가질수 있다.
// define file complete, no errors character
#define FILE_COMPLETE_CHAR '@'
// define file complete, with errors character
#define FILE_ERROR_CHAR '#'

// define lock bit program
#define LOCKBIT_PROGRAM_CHAR '&'

// define AUTO BAUDRATE PING
#define AUTO_BAUDRATE_PING_CHAR 0x55

/*****************************************************************************/
/*                        P R O T O T Y P E                                  */
/*****************************************************************************/

void Wait(unsigned int loop);
uint8 FlashLoad(void);
void WriteFlash(void);
char CheckFlash(void);

void LockBit(void);
void ExecCode(void);

/*****************************************************************************/
/*                G L O B A L    V A R I A B L E S                           */
/*****************************************************************************/
unsigned char PageBuffer[PageByte];

unsigned char RampzFlag;
#ifdef RAMPZ_FLAG
unsigned long PageAddress;
#else
unsigned int PageAddress;
#endif

unsigned int RealPageAddress;

/*****************************************************************************
 SUB-RUTINE..: Wait
 Function....: 일정 시간 대기
 ******************************************************************************/
void Wait(unsigned int loop) {
	unsigned long i;
	unsigned int j;

	/*
	 for(j=0;j<loop;j++)
	 {
	 #if(CPU_CLK < 8000000)
	 for (i=0;i<32000;i++)
	 {	//250ns
	 asm volatile(" PUSH  R0 ");
	 asm volatile(" POP   R0 ");
	 }
	 #else
	 for (i=0;i<64000;i++)
	 {	//250ns
	 asm volatile(" PUSH  R0 ");
	 asm volatile(" POP   R0 ");
	 }
	 #endif
	 }
	 */

	// WinAVR 2007년버전에 적용
	for (j = 0; j < loop; j++) {
#if(CPU_CLK < 8000000)
		for (i=0;i<1600;i++) asm volatile("");
#else
		for (i = 0; i < 3200; i++)
			asm volatile("");
#endif
	}

	/*
	 // 이전 버전에서 사용됨
	 for(j=0;j<loop;j++)
	 {
	 #if(CPU_CLK < 8000000)
	 for (i=0;i<32000;i++);
	 #else
	 for (i=0;i<64000;i++);
	 #endif
	 }
	 */

}

int USART_Transmit(uint8 buff) {
	while ((UCSR0A & (1 << UDRE0)) == 0)
		;
	// tx isr.

	UDR0 = buff;
	return 1;
}

/****************************************************************************
 RUTINE......: FlashLoad
 Function....: Flash Programing Code
 ****************************************************************************/
uint8 FlashLoad(void) {
	uint16 i = 0;
	uint8 j = 0;
	uint16 k = 0;
	uint8 data[255];
	uint16 addrOffset;
	uint8 dataType;
	uint8 len;
	uint8 buffIndex = 0;

	for (i = DATA_PAGE_ADDR; i < 512; i++) {
		flash_readBytes(i, data, 255);
		for (k = 0; k < 252; k++)
			USART_Transmit(data[k]);

		for (j = 0; j < 252; j += 21) {
			len = data[0 + j];
			addrOffset = data[1 + j] << 8 | data[2 + j];
			dataType = data[3 + j];

			if (len == 0 && dataType == 0x01) {
				if (buffIndex != 0) {
					WriteFlash();
					if (CheckFlash() == 0) //실패시 처음부터 다시.
						return 0;
					for (k = 0; k < PageByte; k++)
						PageBuffer[k] = 0xff;
					PageAddress += buffIndex;
				}

				USART_Transmit('c');
				USART_Transmit('c');
				USART_Transmit('c');
				USART_Transmit('c');
				return 1; //정상 종료
			}

			/*if (buffIndex == 0) {
			 RealPageAddress = (unsigned int) addrOffset;
			 PageAddress = (unsigned long) RealPageAddress;
			 }*/
			for (k = 0; k < len; k++) {
				PageBuffer[buffIndex++] = data[j + k + 4];

				if (buffIndex == PageByte) {
					WriteFlash();
					if (CheckFlash() == 0) //실패시 처음부터 다시.
						return 0;

					PageAddress += buffIndex;
					buffIndex = 0;
					for (k = 0; k < PageByte; k++)
						PageBuffer[k] = 0xff;
				}
			}
		}
	}
}

/*****************************************************************************
 SUB-RUTINE..: WriteFlash
 Function....:
 ******************************************************************************/
void WriteFlash(void) {
	unsigned int i;
	unsigned int TempInt;

// 2007-10-02 수정
	eeprom_busy_wait ();

	boot_page_erase(PageAddress);
// 한 페이지를 삭제.
//while(boot_rww_busy()) boot_rww_enable();		// 삭제 될때까지 대기.
	boot_spm_busy_wait (); // Wait until the memory is erased.

	for (i = 0; i < PageByte; i += 2) {
		TempInt = PageBuffer[i] + ((unsigned int) (PageBuffer[i + 1] << 8));

#ifdef RAMPZ_FLAG
		boot_page_fill((unsigned long)i, TempInt);
#else
		boot_page_fill(i, TempInt);
#endif
	}

	boot_page_write(PageAddress);
// Store buffer in flash page. (페이지 번호 Writing)
//while(boot_rww_busy()) boot_rww_enable();		// 데이터가 모두 Writing될때까지 대기.
	boot_spm_busy_wait(); // Wait until the memory is written.
// Reenable RWW-section again. We need this if we want to jump back
// to the application after bootloading.
	boot_rww_enable ();
}

/*****************************************************************************
 SUB-RUTINE..: CheckFlash
 Function....: flash에 Writ한 데이터를 read하여 이상없이 Writ가 되었는지 확인
 ******************************************************************************/
char CheckFlash(void) {
	unsigned int i;
	unsigned int TempInt = 0;
	unsigned int TempInt2 = 0;
	uint16 k = 0;

	for (i = 0; i < PageByte; i += 2) {

#ifdef RAMPZ_FLAG
		TempInt = pgm_read_word_far(PageAddress + (unsigned long)i);
#else
		TempInt = pgm_read_word(PageAddress + i);
#endif
		TempInt2 = PageBuffer[i] + ((unsigned int) (PageBuffer[i + 1] << 8));

		if (TempInt != TempInt2) {
			USART_Transmit(0xff);
			USART_Transmit(0xff);
			USART_Transmit(PageAddress >> 8);
			USART_Transmit(PageAddress);
			for (k = 0; k < PageByte; k++)
				USART_Transmit(PageBuffer[k]);
			return 0;
		}
	}
	return 1;

}

/*****************************************************************************
 RUTINE......: LockBit
 Function....: LockBit Code
 ****************************************************************************/
#ifdef LOCKBIT
void LockBit(void) {
	unsigned char LockSet, CheckBuff;

//LockSet = RxChar(); // LockBit 데이타

//CheckBuff = RxChar();
	if (LockSet == 0xFF && CheckBuff == 0xFF)
		return;
	CheckBuff = ~CheckBuff;
//if (LockSet == CheckBuff) write_lock_bits(LockSet);
	if (LockSet == CheckBuff)
		boot_lock_bits_set(LockSet);
}
#endif

/*****************************************************************************
 RUTINE......: ExecCode
 Function....: 프로그램 실행..
 ****************************************************************************/
void ExecCode(void) {
#ifdef RAMPZ_FLAG
	RAMPZ = 0;
#endif

#ifdef INTVECREG
	INTVECREG = 0x01; // Enable interrupt vector select
	INTVECREG = 0x00; // Move interrupt vector to flash
#endif

//asm("jmp 0x0000");	// Run application code

#ifdef __AVR_MEGA__
#define XJMP "jmp"
#else
#define XJMP "rjmp"
#endif
	asm(XJMP" 0x0000");
// Run application code

}
void WDT_INIT() {
	MCUSR &= ~(1 << WDRF); // WatchDog Init(Low)
	wdt_enable(WDTO_2S);
// WatchDog Reset Time(High)
}

int main(void) {
	uint16 i = 0;
	uint8 isReadyToUpgradeFirmware = 0;

	WDT_INIT();
	wdt_disable();

	flash_init();

	UCSR0A = 0x02; // U2X = 0
	UCSR0B = 0x98;
	UCSR0C = 0x06;
	UBRR0H = 0x00;
	UBRR0L = 0x08; // 115200

	isReadyToUpgradeFirmware = flash_readUint8(FLAG_PAGE_ADDR);

	asm("cli");
// disable all interrupts

	Wait(100);
	//DDRB |= 0x02;
	for (i = 0; i < 2; i++) {
		USART_Transmit('a');
		//PORTB ^= 0x02;
		MSLEEP(200);
		//PORTB ^= 0x02;
		MSLEEP(200);
	}
	MSLEEP(500);

	if (isReadyToUpgradeFirmware == 1) {
		if (FlashLoad() == 0) {
			WDT_INIT();
			while (1)
				;
		}
		flash_writeUint8(FLAG_PAGE_ADDR, 0);
		//flash data 모두 삭제.
		for (i = DATA_PAGE_ADDR; i < 512; i++)
			flash_eraseBytes(i, 255);
	}

	for (i = 0; i < 3; i++) {
		USART_Transmit('b');
		//PORTB ^= 0x02;
		MSLEEP(200);
		//PORTB ^= 0x02;
		MSLEEP(200);
	}

	//WDT_INIT();
	asm("sei");

	ExecCode();
}
/* End of main program */

