#ifndef FLASH__H
#define FLASH__H

typedef unsigned char uint8;
typedef unsigned int uint16;
typedef unsigned long uint32;

#define		MSLEEP(x)		_delay_ms(x)
#define		USLEEP(x)		_delay_us(x)


void flash_init();

void flash_writeUint8(uint16 address, uint8 data);
uint8 flash_readUint8(uint16 address);

void flash_writeUint16(uint16 address, uint16 data);
uint16 flash_readUint16(uint16 address);

void flash_writeUint32(uint16 address, uint32 data);
uint32 flash_readUint32(uint16 address);

void flash_writeBytes(uint16 address, uint8 *data, uint8 length);
uint8 flash_readBytes(uint16 address, uint8 *data, uint8 length);

void flash_eraseBytes(uint16 address, uint8 length);

uint8 flash_bufferReadUint8(uint16 address);

uint8 flash_readStatus();

#endif
