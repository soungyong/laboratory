#define ADC_VREF_TYPE 0x00
#include "util.h"
#include <avr/io.h>

void initADC() {
	ADMUX = ADC_VREF_TYPE & 0xff;
	ADCSRA = 0x84;
}

uint16 adc_getValue(uint8 channel) {
	ADMUX = channel | (ADC_VREF_TYPE & 0xff);
	// Delay needed for the stabilization of the ADC input voltage
	MSLEEP(20);
	// Start the AD conversion
	ADCSRA |= 0x40;
	// Wait for the AD conversion to complete
	while ((ADCSRA & 0x10) == 0)
		;
	ADCSRA |= 0x10;
	return ADCW;
}
