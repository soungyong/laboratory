#ifndef __MAIN_H__
#define __MAIN_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "XNet.h"
#include "Util.h"
#include "Uart.h"
#include "Xcps.h"
#include "ZRMProtocol.h"
#include "NCProtocol.h"
#include "XNetProtocol.h"
#include "Timer.h"

void InitMCU();
void WDT_INIT();

#endif

