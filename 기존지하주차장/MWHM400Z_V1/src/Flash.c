#include "Flash.h"
#include <avr/io.h>

#define SET_NCS() (PORTC |= 0x01)
#define CLEAR_NCS() (PORTC &= ~(0x01))

#define SET_CLK() (PORTC |= 0x02)
#define CLEAR_CLK() (PORTC &= ~(0x02))

#define SET_SI() (PORTC |= 0x04)
#define CLEAR_SI() (PORTC &= ~(0x04))

#define GET_SO() ((PINC & 0x08)>>2)

void flash_init() {
	DDRC |= 0x07;
	DDRC &= ~(0x08);

	//DDRC |= 0x60;
	//PORTC |= (0x04);

	SET_NCS();
}

void clk() {
	USLEEP(1);
	SET_CLK();
	USLEEP(1);
	// MIN: 7ns
	CLEAR_CLK();
	USLEEP(1);
	//MIN: 7ns
}

void flash_writeByteToSI(uint8 data) {
	int i = 0;
	for (i = 7; i >= 0; i--) {
		if ((data & (1 << i)) != 0)
			SET_SI();
		else
			CLEAR_SI();
		clk();
	}
}

uint8 flash_readByteFromSO() {
	int i = 0;
	uint8 data = 0;

	for (i = 7; i >= 0; i--) {
		if (GET_SO() == 0)
			data |= 0;
		else
			data |= (0x01 << i);
		clk();
	}

	return data;
}

void flash_bufferWrite(uint16 address, uint8 size, uint8 *data) {
	uint8 i = 0;
	uint8 cmd = 0x84;

	CLEAR_NCS();
	MSLEEP(1);

	flash_writeByteToSI(cmd);
	flash_writeByteToSI(0);
	flash_writeByteToSI(address >> 8);
	flash_writeByteToSI(address);

	for (i = 0; i < size; i++) {
		flash_writeByteToSI(data[i]);
	}
	MSLEEP(1);
	SET_NCS();
}

void flash_bufferToMainMemory(uint16 address) {
	uint8 cmd = 0x83;

	CLEAR_NCS();
	MSLEEP(1);

	flash_writeByteToSI(cmd);
	flash_writeByteToSI(address >> 7);
	flash_writeByteToSI(address << 1);
	flash_writeByteToSI(0);

	MSLEEP(1);
	SET_NCS();
}

void flash_writeUint8(uint16 address, uint8 data) {
	flash_bufferWrite(0, 1, &data);
	flash_bufferToMainMemory(address);

	MSLEEP(10);
}

uint8 flash_bufferReadUint8(uint16 address) {
	uint8 data = 0x00;
	uint8 cmd = 0xd4;
	uint8 i = 0;

	CLEAR_NCS();
	clk();

	flash_writeByteToSI(cmd);
	flash_writeByteToSI(0);
	flash_writeByteToSI(address >> 8);
	flash_writeByteToSI(address);
	flash_writeByteToSI(0);

	data = flash_readByteFromSO();

	SET_NCS();
	return data;
}

uint8 flash_readUint8(uint16 address) {
	uint8 data = 0x00;
	uint8 cmd = 0xd2;
	uint8 i = 0;

	CLEAR_NCS();
	MSLEEP(1);

	flash_writeByteToSI(cmd);
	flash_writeByteToSI(address >> 7);
	flash_writeByteToSI(address << 1 | 0);
	flash_writeByteToSI(0);

	for (i = 0; i < 4; i++)
		flash_writeByteToSI(0);

	data = flash_readByteFromSO();

	MSLEEP(1);
	SET_NCS();
	return data;
}

void flash_writeUint16(uint16 address, uint16 data) {
	flash_bufferWrite(0, 2, &data);
	flash_bufferToMainMemory(address);

	MSLEEP(10);

}
uint16 flash_readUint16(uint16 address) {
	uint16 data = 0x00;
	uint8 cmd = 0xd2;
	uint8 i = 0;

	CLEAR_NCS();
	MSLEEP(1);

	flash_writeByteToSI(cmd);
	flash_writeByteToSI(address >> 7);
	flash_writeByteToSI(address << 1 | 0);
	flash_writeByteToSI(0);

	for (i = 0; i < 4; i++)
		flash_writeByteToSI(0);

	data = flash_readByteFromSO();
	data |= flash_readByteFromSO() << 8;

	MSLEEP(1);
	SET_NCS();
	return data;
}

void flash_writeUint32(uint16 address, uint32 data) {
	flash_bufferWrite(0, 4, &data);
	flash_bufferToMainMemory(address);

	MSLEEP(10);
}

uint32 flash_readUint32(uint16 address) {
	uint32 data = 0;
	uint8 cmd = 0xd2;
	uint8 i = 0;

	CLEAR_NCS();
	MSLEEP(1);

	flash_writeByteToSI(cmd);
	flash_writeByteToSI(address >> 7);
	flash_writeByteToSI(address << 1 | 0);
	flash_writeByteToSI(0);

	for (i = 0; i < 4; i++)
		flash_writeByteToSI(0);

	data = flash_readByteFromSO();
	data |= (flash_readByteFromSO() << 8);
	data |= ((uint32) flash_readByteFromSO()) << 16;
	data |= ((uint32) flash_readByteFromSO()) << 24;

	MSLEEP(1);
	SET_NCS();
	return data;
}

void flash_writeBytes(uint16 address, uint8* data, uint8 length) {
	flash_bufferWrite(0, length, data);
	flash_bufferToMainMemory(address);

	MSLEEP(10);
}
uint8 flash_readBytes(uint16 address, uint8 *data, uint8 length) {
	uint8 cmd = 0xd2;
	uint8 i = 0;

	CLEAR_NCS();
	MSLEEP(1);

	flash_writeByteToSI(cmd);
	flash_writeByteToSI(address >> 7);
	flash_writeByteToSI(address << 1 | 0);
	flash_writeByteToSI(0);

	for (i = 0; i < 4; i++)
		flash_writeByteToSI(0);

	for (i = 0; i < length; i++)
		data[i] = flash_readByteFromSO();

	MSLEEP(1);
	SET_NCS();

	return length;
}

uint8 flash_readStatus() {
	uint8 cmd = 0xd7;
	uint8 data = 0;

	CLEAR_NCS();
	MSLEEP(1);

	flash_writeByteToSI(cmd);
	data = flash_readByteFromSO();

	MSLEEP(1);
	SET_NCS();

	return data;
}
