#ifndef __PLCS_GC_PROTOCOL_H__
#define __PLCS_GC_PROTOCOL_H__

#include "NCProtocol.h"
#include "util.h"

// ---------------------------------------------------------------------------------------------- //
#define PLCS_GCP_PROTOCOL_ID							(0x21)
#define PLCS_MDF_FW_VER								0x10

#define PLCS_GCP_REQ_POWERMETER						(0x40)
#define PLCS_GCP_RES_POWERMETER						(0x41)
#define PLCS_GCP_REQ_UPDATEPOWERMETER_LC			(0x42)
#define PLCS_GCP_REQ_UPDATEPOWERMETER				(0x44)

#define PLCS_GCP_REQ_STATEINFO						(0x24)
#define PLCS_GCP_RES_STATEINFO						(0x25)

#define PLCS_GCP_REQ_CONTROL_REBOOT					(0x3A)

#define PLCS_MDF_TYPE								(0x2060)

// ---------------------------------------------------------------------------------------------- //
extern volatile uint32 g_PowerMeterValue[4];
extern volatile uint16 g_Temperature;
extern volatile uint16 g_Illumination;
// ---------------------------------------------------------------------------------------------- //

uint16 getSeqNumGenerator();
uint8 plcs_GetNCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len);
uint8 plcs_GetNCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum);
uint8 plcs_GetGCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len);
uint8 plcs_GetGCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum);

extern uint8 ncp_NetState;
uint8 ncp_ConnState();
void ncp_setNCPState(uint8 state);

void NCP_ProcessMessageFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[], int length);
void plcs_GCP_ProcessMessageFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[], int length);

void ncp_ProcessRegisterResFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[], int length);
void ncp_ProcessPingResFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[], int length);

void ncp_SendPingReq();
void ncp_SendRegisterReq();

//MDFP Handle
void plcs_GCP_HandlePowermeterReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleUpdatePowermeterReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_HandleStateInfoReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

//MDFP Send
void plcs_GCP_SendPowermeterRes(uint16 seqNum, uint16 nodeId, uint8 channel,
		uint32 powermeterValue);

void plcs_GCP_SendStateInfoRes(uint16 seqNum, uint16 nodeId,
		uint16 temperature, uint16 illuminance);

#endif
