#include "Debug.h"
//#include <avr/eeprom.h>
#include "Flash.h"

//DebugInfo_st debugInfo EEMEM;

/*
 typedef struct {
 uint16 rebootCnt;
 uint8 lastRebootTime[2];
 uint16 serverConnectionCnt;
 uint8 lastServerConnectionTime[2];
 }DebugInfo_st;
 */
uint8 tempBuffForDebug[10];

void debug_UpdateLog() {
	//eeprom_write_block(&debug_DebugInfo, &debugInfo, sizeof(DebugInfo_st));

	tempBuffForDebug[0] = debug_DebugInfo.rebootCnt >> 8;
	tempBuffForDebug[1] = debug_DebugInfo.rebootCnt;

	tempBuffForDebug[2] = debug_DebugInfo.lastRebootTime[0];
	tempBuffForDebug[3] = debug_DebugInfo.lastRebootTime[1];

	tempBuffForDebug[4] = (debug_DebugInfo.serverConnectionCnt) >> 8;
	tempBuffForDebug[5] = (debug_DebugInfo.serverConnectionCnt);

	tempBuffForDebug[6] = debug_DebugInfo.lastServerConnectionTime[0];
	tempBuffForDebug[7] = debug_DebugInfo.lastServerConnectionTime[1];

	flash_writeBytes(FLASH_ADDRESS_DEBUG, tempBuffForDebug, 8);
}

void debug_ReadLog() {
//	eeprom_read_block(&debug_DebugInfo, &debugInfo, sizeof(DebugInfo_st));

	flash_readBytes(FLASH_ADDRESS_DEBUG, tempBuffForDebug, 10);

	debug_DebugInfo.rebootCnt = tempBuffForDebug[0] << 8 | tempBuffForDebug[1];

	debug_DebugInfo.lastRebootTime[0] = tempBuffForDebug[2];
	debug_DebugInfo.lastRebootTime[1] = tempBuffForDebug[3];

	debug_DebugInfo.serverConnectionCnt = tempBuffForDebug[4] << 8 | tempBuffForDebug[5];

	debug_DebugInfo.lastServerConnectionTime[0] = tempBuffForDebug[6];
	debug_DebugInfo.lastServerConnectionTime[1] = tempBuffForDebug[7];
}

void debug_Reset() {
	uint8 i = 0;
	debug_DebugInfo.rebootCnt = 0;
	debug_DebugInfo.serverConnectionCnt = 0;
	for (i = 0; i < 5; i++) {
		debug_DebugInfo.lastRebootTime[i] = 0;
		debug_DebugInfo.lastServerConnectionTime[i] = 0;
	}

	debug_UpdateLog();
}

void debug_UpdateReboot() {
	debug_DebugInfo.rebootCnt++;

	debug_DebugInfo.lastRebootTime[0] = Hour;
	debug_DebugInfo.lastRebootTime[1] = Min;

	debug_UpdateLog();
}

void debug_UpdateConnection() {
	//debug_DebugInfo.serverConnectionCnt++;

	debug_DebugInfo.lastServerConnectionTime[0] = Hour;
	debug_DebugInfo.lastServerConnectionTime[1] = Min;

	debug_UpdateLog();
}
