#include "DimmerProtocol.h"
#include "ZRMProtocol.h"
#include "MappingTable.h"
////////
uint8 tmpBuffForDimmer[32];
///////
static uint16 seqNumGeneratorForDimmer = 0;
uint16 getSeqNumGeneratorForDimmer() {
	return seqNumGeneratorForDimmer++;
}

void initDimmerControlInfoList() {
	dimmerControlInfoList.size = 0;
}

void updateDimmerControlInfoList() {
	uint8 i = 0;
	uint8 j = 0;
	uint8 isContainDimmerInfo = 0;

	initDimmerControlInfoList();

	for (i = 0; i < getDimmerMappingTableSize(); i++) {
		isContainDimmerInfo = 0;
		DimmerMappingInfo_st *info = getDimmerMappingInfoByIndex(i);
		if (info == NULL)
			continue;
		for (j = 0; j < dimmerControlInfoList.size; j++)
			if (dimmerControlInfoList.info[j].dimmerId == info->dimmerId) {

				dimmerControlInfoList.info[j].channelState[info->channelId] = &(tmp_CircuitInfo.state[info->circuitId]);
				isContainDimmerInfo = 1;
				break;
			}
		if (isContainDimmerInfo == 0) {
			dimmerControlInfoList.info[dimmerControlInfoList.size].dimmerId = info->dimmerId;

			dimmerControlInfoList.info[dimmerControlInfoList.size].channelState[0] = NULL;
			dimmerControlInfoList.info[dimmerControlInfoList.size].channelState[1] = NULL;
			dimmerControlInfoList.info[dimmerControlInfoList.size].channelState[2] = NULL;
			dimmerControlInfoList.info[dimmerControlInfoList.size].channelState[3] = NULL;

			dimmerControlInfoList.info[dimmerControlInfoList.size].channelState[info->channelId] = &(tmp_CircuitInfo.state[info->circuitId]);
			dimmerControlInfoList.size++;
		}
	}
}

uint8 plcs_GetDCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGeneratorForDimmer();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_DCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

void dimmer_SendReqDimming(dimmerControl_Info_st *dimmerControlInfo) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen;
	device_Info* deviceInfo_P;

	if (dimmerControlInfo == NULL)
		return;

	len = 0;
	payload[len++] = PLCS_DCP_REQ_DIMMING;
	payload[len++] = dimmerControlInfo->dimmerId >> 8;
	payload[len++] = dimmerControlInfo->dimmerId;
	payload[len++] = dimmer_DimmingConvert(*(dimmerControlInfo->channelState[0]));
	payload[len++] = dimmer_DimmingConvert(*(dimmerControlInfo->channelState[1]));
	payload[len++] = dimmer_DimmingConvert(*(dimmerControlInfo->channelState[2]));
	payload[len++] = dimmer_DimmingConvert(*(dimmerControlInfo->channelState[3]));

	dimmerControlInfo->prevChannelState[0] = *(dimmerControlInfo->channelState[0]);
	dimmerControlInfo->prevChannelState[1] = *(dimmerControlInfo->channelState[1]);
	dimmerControlInfo->prevChannelState[2] = *(dimmerControlInfo->channelState[2]);
	dimmerControlInfo->prevChannelState[3] = *(dimmerControlInfo->channelState[3]);

	resultLen = plcs_GetDCPMessage(tmpBuffForDimmer, dimmerControlInfo->dimmerId, zrmpInfo_0.zrm_Id, payload, len);

	deviceInfo_P = findNodeById(dimmerControlInfo->dimmerId);
	if (deviceInfo_P != NULL) {
		sendToZigbee(ZIGBEE_1, deviceInfo_P->netAddr, tmpBuffForDimmer, resultLen);
		sendMessage(MPR, dimmerControlInfo->dimmerId, tmpBuffForDimmer, resultLen);
	}
}

uint8 dimmer_DimmingConvert(uint8 DimLevel) {
	switch (DimLevel) {
	case DIMMING_0:
		return 0x00;
		break;

	case DIMMING_1:
		return 0x20;
		break;

	case DIMMING_2:
		return 0x34;
		break;

	case DIMMING_3:
		return 0x4D;
		break;

	case DIMMING_4:
		return 0x67;
		break;

	case DIMMING_5:
		return 0x80;
		break;

	case DIMMING_6:
		return 0x9A;
		break;

	case DIMMING_7:
		return 0xB3;
		break;

	case DIMMING_8:
		return 0xCD;
		break;

	case DIMMING_9:
		return 0xE6;
		break;

	case DIMMING_10:
		return 0xFE;
		break;
	case ON:
		return 0xFE;
		break;
	case OFF:
		return 0x00;
		break;

	default:
		return 0xFE;
		break;
	}
	return 0xFE;
}
