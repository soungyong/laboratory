
#include <stdio.h>
#include <string.h>
#include "Xcps.h"
#include "Uart.h"


// protocol message format start/end flag.
#define	ZS_SFLAG	0xFA
#define	ZS_EFLAG	0xAF


/**
	temporary rx packet.
*/
uint8 xcps_rx_packet_zigbee[XCPS_MAX_PDU];


// 03/01. xcp zigbee uart transport receive state.
int xcps_rxlen_zigbee = 0;							// total length received.
int xcps_pdu_len_zigbee = 0;

/**
	uart interface to use.
*/

static usart_getter	xcps_getter_zigbee = (usart_getter)0;
static usart_putter	xcps_putter_zigbee = (usart_putter)0;


/**

**/
uint8 xcps_init_zigbee(usart_getter getter, usart_putter putter)
{
	xcps_getter_zigbee = getter;
	xcps_putter_zigbee = putter;

	
	return 0;
}


/**

*/
uint8 xcps_send_zigbee(const uint8 *data, uint8 length) {
	uint8 i=0;
	uint8 checkSum = 0;

	// check putter.
	if (!xcps_putter_zigbee)
		return -1;

	// calc checksum.
	for (i = 0; i < length; i++)
		checkSum += ((uint8) data[i] & 0xFF);

	xcps_putter_zigbee(ZS_SFLAG);
	xcps_putter_zigbee(length);
	for (i = 0; i < length; i++)
		xcps_putter_zigbee(data[i]);
	xcps_putter_zigbee(checkSum);
	xcps_putter_zigbee(ZS_EFLAG);

	return 0;
}

uint8 xcps_recv_zigbee(uint8 *buff, uint8 buff_length) {
	uint8 temp;
	int i;
	uint8 checkSum = 0;

	// check getter.
	if (!xcps_getter_zigbee)
		return -1;

	if (xcps_getter_zigbee(&temp) < 1)
		return 0;


	if (xcps_rxlen_zigbee >= buff_length)
		xcps_rxlen_zigbee = 0;

	xcps_rx_packet_zigbee[xcps_rxlen_zigbee++] = temp;

	if (temp == ZS_SFLAG) {
		if(xcps_rx_packet_zigbee[0]!=ZS_SFLAG){
			xcps_rxlen_zigbee = 0;
			xcps_rx_packet_zigbee[xcps_rxlen_zigbee++] = temp;
			return 0;
		}
	} else if (temp == ZS_EFLAG){
		if(xcps_rxlen_zigbee < 6) return 0;
		if(xcps_rxlen_zigbee - 4 < xcps_rx_packet_zigbee[1]) return 0;
		if(xcps_rxlen_zigbee - 4 > xcps_rx_packet_zigbee[1]) {
			xcps_rxlen_zigbee=0;
			return 0;
		}

		uint8 resultLen=0;
		checkSum = 0;


		//xcps_rx_packet_zigbee contains fa~af.

		//length.
		if (xcps_rxlen_zigbee - 4 != xcps_rx_packet_zigbee[1]){
			xcps_rxlen_zigbee=0;
			return 0;
		}

		//checksum
		for (i = 2; i < xcps_rxlen_zigbee - 2; i++)
			checkSum += ((uint8) xcps_rx_packet_zigbee[i] & 0xFF);

		if (checkSum != xcps_rx_packet_zigbee[xcps_rxlen_zigbee - 2])
			return 0;

		for (i = 2; i < xcps_rxlen_zigbee - 2; i++) {
			buff[resultLen++] = xcps_rx_packet_zigbee[i];
		}
		xcps_rxlen_zigbee=0;

		return resultLen;
	}

	return 0;
}

//end of file

