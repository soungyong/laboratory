#ifndef _ADC__H
#define _ADC__H

#define NumOFHistory 20

void initADC();
uint16 adc_getValue(uint8 channel);

#endif
