################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
S_SRCS += \
../avrx/2.6e/avrx_semaphores.s \
../avrx/2.6e/avrx_tasking.s 

OBJS += \
./avrx/2.6e/avrx_semaphores.o \
./avrx/2.6e/avrx_tasking.o 

S_DEPS += \
./avrx/2.6e/avrx_semaphores.d \
./avrx/2.6e/avrx_tasking.d 


# Each subdirectory must supply rules for building sources it contributes
avrx/2.6e/%.o: ../avrx/2.6e/%.s
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Assembler'
	avr-gcc -x assembler-with-cpp -g2 -gstabs -mmcu=atmega128 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


