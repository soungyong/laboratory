#include "PLCS_Protocol_Server.h"
#include "PLCS_Protocol_LC100.h"
#include "XNetProtocol.h"
#include "debug.h"

/////////////////
uint8 tmp_Buff[64];
///////////////////

extern uint8 Server_Ip[4];
extern uint8 my_Ip[4];
extern uint8 gw_Ip[4];
extern uint8 sub_Net[4];
extern uint16 Server_Port;
extern uint8 isOnSensorSharing;

// --------------------------------------------------------------------------- //
// --------------------- Process PLCS (Gateway <-> Server)  ---------------------- //
// --------------------------------------------------------------------------- //
//uint16 cntForNCPTest=0;
void plcs_GCP_Server_ProcessMessage(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint8 msgType;
	msgType = msg[8];

	switch (msgType) {
	case PLCS_GCP_REQ_STATEINFO:
		plcs_GCP_SERVER_HandleStateInfoReq(seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_NUMOFDEVICE:
		plcs_GCP_SERVER_HandleNumOfDeviceReq(seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_DEVICEINFO:
		plcs_GCP_SERVER_HandleDeviceInfoReq(seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_CONTROL_SENSORSHARING:
		plcs_GCP_SERVER_HandleControlSensorSharingReq(seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_CONTROL_GATEWAY:
		plcs_GCP_SERVER_HandleControlGatewayReq(seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_CONTROL_REBOOT:
		plcs_GCP_SERVER_HandleControlRebootReq(seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_DEBUG_LOG:
		plcs_GCP_SERVER_HandleDebugLogReq(seqNum, srcId, destId, msg, length);
		break;
	case PLCS_GCP_REQ_RESET_DEBUG_LOG:
		plcs_GCP_SERVER_HandleResetDebugLogReq(seqNum, srcId, destId, msg, length);
		break;
	default:
		sendToController(msg, length);
		break;
	}

}
void plcs_NCP_Server_ProcessMessage(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint8 msgType;
	msgType = msg[8];

	switch (msgType) {
	case NCP_RES_REGISTER:
		plcs_NCP_SERVER_HandleRegisterRes(seqNum, srcId, destId, msg, length);
		break;
	case NCP_RES_PING:
		plcs_NCP_SERVER_HandlePingRes(seqNum, srcId, destId, msg, length);
		break;
	case NCP_RES_REGISTER_NODE:
		plcs_NCP_SERVER_HandleRegisterNodeRes(seqNum, srcId, destId, msg, length);
		break;
	case NCP_SEND_TIME_INFO:
		plcs_NCP_SERVER_HandleSendTimeInfo(seqNum, srcId, destId, msg, length);
		break;
	default:
		break;
	}
}

void plcs_NCP_SERVER_HandleRegisterRes(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 timeOut;
	uint16 result;
	uint8 i;

	//msg[8] == msgType
	result = (uint16) ((msg[9] << 8) | msg[10]);
	timeOut = (uint16) ((msg[11] << 8) | msg[12]);

	if (result == 0) {
		if (plcs_NCP_SERVER_NetState != NET_REGISTER) {
			debug_UpdateConnection();
		}

		plcs_NCP_SERVER_NetState = NET_REGISTER;
		plcs_NCP_SERVER_SendPingReq();

		//하위 장치들에게 재 등록 요청.
		for (i = 0; i < deviceInfoTable.size; i++) {
			plcs_NCP_LC100_SendRegisterRes(0, deviceInfoTable.deviceInfo[i].nodeId, 0x01, PING_TIME);
		}
	} else {
		plcs_NCP_SERVER_SendRegisterReq();
		for (i = 0; i < deviceInfoTable.size; i++) {
			plcs_NCP_LC100_SendRegisterRes(0, deviceInfoTable.deviceInfo[i].nodeId, 0x01, PING_TIME);
		}
	}
}

void plcs_NCP_SERVER_HandlePingRes(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint8 flag = 0;
	uint8 protocolVer;
	uint16 pingInterval;

	//msg[8] == msgType
	protocolVer = msg[9];
	flag = msg[10];
	pingInterval = (uint16) ((msg[11] << 8) | msg[12]);

	plcs_NCP_SERVER_Ping_Count = 0;

	// Add Function Timer + Ping Interval
	if (pingInterval == 0)
		pingInterval = 0xffff;
	plcs_NCP_SERVER_PingInterval = pingInterval;

	if (flag == 0x01) { //재 등록.
		uint16 i = 0;
		plcs_NCP_SERVER_SendRegisterReq();
		//하위 노드들도 재 등록 요청.
		for (i = 0; i < deviceInfoTable.size; i++) {
			plcs_NCP_LC100_SendPingRes(0, deviceInfoTable.deviceInfo[i].nodeId, 0x01, PING_TIME);
		}
	}
}

void plcs_NCP_SERVER_HandleRegisterNodeRes(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 deviceType;
	uint8 result;

	//msg[8] == msgType
	nodeId = msg[9] << 8 | msg[10];
	deviceType = msg[11] << 8 | msg[12];
	result = (uint16) ((msg[13] << 8) | msg[14]);

	if (deviceType == DEVICETYPE_LC100) {
		uint8 min, hour;
		min = ds1307_read(1);
		hour = ds1307_read(2);

		plcs_NCP_LC100_SendRegisterRes(seqNum, nodeId, result, 0x0030);
		plcs_NCP_LC100_SendTimeInfo(nodeId, hour, min);
	}
}

void plcs_NCP_SERVER_HandleSendTimeInfo(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint8 i;
	uint8 dataFormat;
	uint8 year, month;
	uint8 date, day, hour, min, sec;

	//msg[8] == msgType
	dataFormat = msg[9];
	year = 10 * (msg[12] - '0') + (msg[13] - '0');
	month = 10 * (msg[14] - '0') + (msg[15] - '0');
	date = 10 * (msg[16] - '0') + (msg[17] - '0');
	day = msg[18] - '0';
	hour = 10 * (msg[19] - '0') + (msg[20] - '0');
	min = 10 * (msg[21] - '0') + (msg[22] - '0');
	sec = 10 * (msg[23] - '0') + (msg[24] - '0');

	ds1307_initial_config(sec, min, hour, day, date, month, year);
	for (i = 0; i < deviceInfoTable.size; i++) {
		plcs_NCP_LC100_SendTimeInfo(deviceInfoTable.deviceInfo[i].nodeId, hour, min);
	}
}

void plcs_GCP_SERVER_HandleStateInfoReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 subNodeId;
	uint16 deviceId;

	//msg[8] = msgtype

	subNodeId = msg[9] << 8 | msg[10];
	deviceId = msg[11] << 8 | msg[12];

	//printf_P(PSTR("\nsuNodeId (0x%04X)"), subNodeId);
	//printf_P(PSTR("\ndeviceId (0x%04X)"), deviceId);

	if (subNodeId == 0 && deviceId == 0)
		plcs_GCP_SERVER_SendStateInfoRes(seqNum);
	else
		sendToController(msg, length);

	//plcs_NCP_LC100_SendStateInfoReq(nodeId, subNodeId, deviceType);
}

void plcs_GCP_SERVER_HandleNumOfDeviceReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 subNodeId;
	//msg[8] = msgtype

	subNodeId = msg[9] << 8 | msg[10];

	if (subNodeId == 0)
		plcs_GCP_SERVER_SendNumOfDeviceRes(seqNum);
	else
		sendToController(msg, length);
}
void plcs_GCP_SERVER_HandleDeviceInfoReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 subNodeId;
	uint8 index;

	//msg[8] = msgtype

	subNodeId = msg[9] << 8 | msg[10];
	index = msg[11];

	if (subNodeId == 0)
		plcs_GCP_SERVER_SendDeviceInfoRes(seqNum, index);
	else
		sendToController(msg, length);
}

void plcs_GCP_SERVER_HandleControlSensorSharingReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint8 value;

	//msg[8] = msgtype
	value = msg[9];

	isOnSensorSharing = value;

	plcs_GCP_SERVER_SendControlSensorSharingRes(seqNum, 0x00);
}

void plcs_GCP_SERVER_HandleControlGatewayReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint8 deviceIp[4];
	uint8 serverIp[4];
	uint8 defaultGatewayIp[4];
	uint8 netmask[4];
	uint16 port;
	uint8 sensorSharingMode = 0;
	uint8 i;

	//msg[8] = msgtype

	for (i = 0; i < 4; i++)
		deviceIp[i] = msg[9 + i];
	for (i = 0; i < 4; i++)
		serverIp[i] = msg[13 + i];
	for (i = 0; i < 4; i++)
		defaultGatewayIp[i] = msg[17 + i];
	for (i = 0; i < 4; i++)
		netmask[i] = msg[21 + i];

	port = msg[25] << 8 | msg[26];

	sensorSharingMode = msg[27];

	for (i = 0; i < 4; i++)
		Server_Ip[i] = serverIp[i];
	for (i = 0; i < 4; i++)
		my_Ip[i] = deviceIp[i];
	for (i = 0; i < 4; i++)
		gw_Ip[i] = defaultGatewayIp[i];
	for (i = 0; i < 4; i++)
		sub_Net[i] = netmask[i];
	Server_Port = port;
	isOnSensorSharing = sensorSharingMode;

	//eeprom에 저장.
	eeprom_write_byte((uint8_t*) 20, Server_Ip[0]);
	eeprom_write_byte((uint8_t*) 21, Server_Ip[1]);
	eeprom_write_byte((uint8_t*) 22, Server_Ip[2]);
	eeprom_write_byte((uint8_t*) 23, Server_Ip[3]);

	eeprom_write_byte((uint8_t*) 30, my_Ip[0]);
	eeprom_write_byte((uint8_t*) 31, my_Ip[1]);
	eeprom_write_byte((uint8_t*) 32, my_Ip[2]);
	eeprom_write_byte((uint8_t*) 33, my_Ip[3]);

	eeprom_write_byte((uint8_t*) 40, gw_Ip[0]);
	eeprom_write_byte((uint8_t*) 41, gw_Ip[1]);
	eeprom_write_byte((uint8_t*) 42, gw_Ip[2]);
	eeprom_write_byte((uint8_t*) 43, gw_Ip[3]);

	eeprom_write_byte((uint8_t*) 50, sub_Net[0]);
	eeprom_write_byte((uint8_t*) 51, sub_Net[1]);
	eeprom_write_byte((uint8_t*) 52, sub_Net[2]);
	eeprom_write_byte((uint8_t*) 53, sub_Net[3]);

	eeprom_write_word((uint16_t*) 60, Server_Port);

	eeprom_write_byte((uint8_t*) 70, isOnSensorSharing);

	plcs_GCP_SERVER_SendControlGatewayRes(seqNum, 0x00);
}

void plcs_GCP_SERVER_HandleControlRebootReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 subNodeId;
	uint16 deviceId;

	//msg[8] = msgtype
	subNodeId = msg[9] << 8 | msg[10];
	deviceId = msg[11] << 8 | msg[12];
	if (subNodeId == 0 && deviceId == 0) {
		while (1)
			;
	} else
		sendToController(msg, length);
}

void plcs_GCP_SERVER_HandleDebugLogReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	plcs_GCP_SERVER_SendDebugLogRes(seqNum);
}

void plcs_GCP_SERVER_HandleResetDebugLogReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length) {
	debug_Reset();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
////////////////////////////////////////////////////////////////////////////////////////////////////////////
void plcs_GCP_SERVER_SendControlSensorSharingRes(uint16 seqNum, uint8 result) {

	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	//printf_P(PSTR("\nplcs_GCP_SERVER_SendStateInfoRes"));

	payload[len++] = PLCS_GCP_RES_CONTROL_SENSORSHARING;
	payload[len++] = result;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, NCP_GW_ID, payload, len, seqNum);

	sendToServer(tmp_Buff, resultLen);
	MSLEEP(3);
}

void plcs_GCP_SERVER_SendDeviceInfoRes(uint16 seqNum, uint8 index) {

	int len = 0;
	int i = 0;
	uint16 subNodeId;
	uint16 deviceId;
	uint16 deviceType;
	uint8 deviceVersion;
	uint8 payload[20];
	uint8 resultLen = 0;

	//printf_P(PSTR("\nplcs_GCP_SERVER_SendStateInfoRes"));
	subNodeId = 0;

	if (deviceInfoTable.size <= index)
		return;

	deviceId = deviceInfoTable.deviceInfo[index].nodeId;
	deviceType = deviceInfoTable.deviceInfo[index].deviceType;
	deviceVersion = deviceInfoTable.deviceInfo[index].deviceVersion;

	payload[len++] = PLCS_GCP_RES_DEVICEINFO;
	payload[len++] = subNodeId >> 8;
	payload[len++] = subNodeId;
	payload[len++] = deviceId >> 8;
	payload[len++] = deviceId;
	payload[len++] = deviceType >> 8;
	payload[len++] = deviceType;
	payload[len++] = deviceVersion;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, NCP_GW_ID, payload, len, seqNum);

	sendToServer(tmp_Buff, resultLen);
	MSLEEP(3);
}
void plcs_GCP_SERVER_SendNumOfDeviceRes(uint16 seqNum) {

	int len = 0;
	uint16 subNodeId;
	uint8 numOfDevice;
	uint8 payload[10];
	uint8 resultLen = 0;

	//printf_P(PSTR("\nplcs_GCP_SERVER_SendStateInfoRes"));
	subNodeId = 0;
	numOfDevice = deviceInfoTable.size;

	payload[len++] = PLCS_GCP_RES_NUMOFDEVICE;
	payload[len++] = subNodeId >> 8;
	payload[len++] = subNodeId;
	payload[len++] = numOfDevice;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, NCP_GW_ID, payload, len, seqNum);

	sendToServer(tmp_Buff, resultLen);
	MSLEEP(3);
}

void plcs_GCP_SERVER_SendStateInfoRes(uint16 seqNum) {

	int len = 0;
	int i = 0;
	uint16 subNodeId;
	uint16 deviceId;
	uint16 deviceType;
	uint8 payload[40];
	uint8 resultLen = 0;

	//printf_P(PSTR("\nplcs_GCP_SERVER_SendStateInfoRes"));
	subNodeId = 0;
	deviceId = 0;
	deviceType = PLCS_GW_TYPE;

	payload[len++] = PLCS_GCP_RES_STATEINFO;
	payload[len++] = subNodeId >> 8;
	payload[len++] = subNodeId;
	payload[len++] = deviceId >> 8;
	payload[len++] = deviceId;
	payload[len++] = deviceType >> 8;
	payload[len++] = deviceType;

	for (i = 0; i < 4; i++)
		payload[len++] = my_Ip[i];
	for (i = 0; i < 4; i++)
		payload[len++] = Server_Ip[i];
	for (i = 0; i < 4; i++)
		payload[len++] = gw_Ip[i];
	for (i = 0; i < 4; i++)
		payload[len++] = sub_Net[i];

	payload[len++] = Server_Port >> 8;
	payload[len++] = Server_Port;
	payload[len++] = isOnSensorSharing;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, NCP_GW_ID, payload, len, seqNum);

	sendToServer(tmp_Buff, resultLen);
	MSLEEP(3);
}

void plcs_NCP_SERVER_SendPingReq() {
	uint8 len = 0;
	uint8 resultLen = 0;
	uint8 payload[20];

	payload[len++] = NCP_REQ_PING;
	payload[len++] = 0;
	payload[len++] = 0;

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, NCP_GW_ID, payload, len);

	sendToServer(tmp_Buff, resultLen);
	MSLEEP(3);
}

void plcs_NCP_SERVER_SendRegisterNodeReq(uint16 subNodeId, uint16 deviceType, uint16 deviceId, uint8 deviceVersion, uint8 firmwareVersion, uint16 netAddr) {
	uint8 len = 0;
	uint8 resultLen = 0;

	uint8 payload[20];

	payload[len++] = NCP_REQ_REGISTER_NODE;
	payload[len++] = (uint8) (subNodeId >> 8);
	payload[len++] = (uint8) (subNodeId);
	payload[len++] = (uint8) (deviceType >> 8);
	payload[len++] = (uint8) (deviceType);
	if (deviceType == 0x2010) {
		payload[len++] = deviceVersion;
		payload[len++] = firmwareVersion;
	} else {
		payload[len++] = (uint8) (deviceId >> 8);
		payload[len++] = (uint8) (deviceId);
		payload[len++] = deviceVersion;
		payload[len++] = firmwareVersion;
		payload[len++] = netAddr;
		payload[len++] = netAddr;
	}

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, NCP_GW_ID, payload, len);

	sendToServer(tmp_Buff, resultLen);
	MSLEEP(3);
}

void plcs_NCP_SERVER_SendRegisterReq() {
	uint8 len = 0;
	uint8 resultLen = 0;

	uint8 payload[20];

	payload[len++] = NCP_REQ_REGISTER;
	payload[len++] = (uint8) (PLCS_GW_TYPE >> 8);
	payload[len++] = (uint8) (PLCS_GW_TYPE);
	payload[len++] = NCP_FW_VER;
	payload[len++] = NCP_FW_VER;

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, NCP_GW_ID, payload, len);

	sendToServer(tmp_Buff, resultLen);
	MSLEEP(3);
}

void plcs_GCP_SERVER_SendControlGatewayRes(uint16 seqNum, uint8 result) {

	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	//printf_P(PSTR("\nplcs_GCP_SERVER_SendStateInfoRes"));

	payload[len++] = PLCS_GCP_RES_CONTROL_GATEWAY;
	payload[len++] = result;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, NCP_GW_ID, payload, len, seqNum);

	sendToServer(tmp_Buff, resultLen);
	MSLEEP(3);
}

void plcs_GCP_SERVER_SendDebugLogRes(uint16 seqNum) {
	int len = 0;
	uint8 payload[40];
	uint8 resultLen = 0;
	uint8 i;

	payload[len++] = PLCS_GCP_RES_DEBUG_LOG;
	payload[len++] = ds1307_read(6);
	payload[len++] = ds1307_read(5);
	payload[len++] = ds1307_read(4);
	payload[len++] = ds1307_read(2);
	payload[len++] = ds1307_read(1);
	payload[len++] = ds1307_read(0);
	payload[len++] = (uint8) (debug_DebugInfo.rebootCnt >> 8);
	payload[len++] = (uint8) debug_DebugInfo.rebootCnt;
	for (i = 0; i < 5; i++)
		payload[len++] = debug_DebugInfo.lastRebootTime[i];
	payload[len++] = (uint8) (debug_DebugInfo.serverConnectionCnt >> 8);
	payload[len++] = (uint8) (debug_DebugInfo.serverConnectionCnt);
	for (i = 0; i < 5; i++)
		payload[len++] = debug_DebugInfo.lastServerConnectionTime[i];

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, NCP_GW_ID, payload, len, seqNum);

	sendToServer(tmp_Buff, resultLen);
	MSLEEP(3);
}

uint8 plcs_NCP_SERVER_GetNetworkState() {
	return plcs_NCP_SERVER_NetState;
}

uint16 plcs_NCP_SERVER_GetPingIntervalTime() {
	return plcs_NCP_SERVER_PingInterval;
}

uint8 plcs_NCP_SERVER_GetPingCount() {
	return plcs_NCP_SERVER_Ping_Count;
}

