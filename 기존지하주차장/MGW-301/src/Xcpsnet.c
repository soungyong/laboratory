#include "Xcpsnet.h"
#include "XNetProtocol.h"

// protocol message format start/end flag.
#define	NET_SFLAG		0xFA
#define	NET_EFLAG		0xAF

/**
 temporary rx packet.
 */
uint8 packet_buffer[XCPSNET_MAX_PDU];

static int packet_state = 0; //
int packet_Rx_Length = 0; // RX Buffer length
int packet_length = 0; // packet payload length.

//-----------------------------------------------------------------------------
uint8 Server_Ip[4] = MY_SERVER_IP;
uint8 my_Ip[4] = MY_SOURCEIP;
uint8 mac_Addr[6] = MY_NET_MAC;
uint8 gw_Ip[4] = MY_NET_GWIP;
uint8 sub_Net[4] = MY_SUBNET;
//-----------------------------------------------------------------------------

void InitNET(void) {
	printf("\nInit InitNET");
	//W5100 Chip Init
	iinchip_init();

	//Set MAC Address
	setSHAR(mac_Addr);

	//Set Gateway
	setGAR(gw_Ip);

	//Set Subnet Mask
	setSUBR(sub_Net);

	//Set My IP
	setSIPR(my_Ip);

#ifdef __DEF_IINCHIP_INT__
	setIMR(0xEF);
#endif

	sysinit(MY_NET_MEMALLOC, MY_NET_MEMALLOC);
}

int xcpsnet_send(const uint8 *data, int length) {
	int i, msgLen;
	uint8 buff[XCPSNET_MAX_PDU];
	int checkSum = 0;

	msgLen = 0;

	buff[msgLen++] = NET_SFLAG; //Start Byte_1
	//buff[msgLen++] = (uint8)(length >> 8);
	buff[msgLen++] = (uint8) (length);

	for (i = 0; i < length; i++)
		buff[msgLen++] = data[i];

	// calc checksum.
	for (i = 2; i < msgLen; i++)
		checkSum += ((uint8) buff[i] & 0xFF);

	buff[msgLen++] = (uint8) (checkSum & 0xFF);

	// end flag.
	buff[msgLen++] = NET_EFLAG;

	send(SOCK_TCPC, buff, (uint16) msgLen);

//	printf("\nG->S :");
	//for(i = 0; i < msgLen; i++)
	//	printf_P(PSTR("%02X."), buff[i]);

	return 0;
}

int xcpsnet_recv(uint8 *buff, int buff_length) {
	int pi;

	for (pi = 0; pi < buff_length; pi++) {
		switch (packet_state) {
		case 0:
			if (buff[pi] == NET_SFLAG) {
				packet_Rx_Length = 0;
				packet_buffer[packet_Rx_Length++] = buff[pi];
				packet_state = 1;
			} else if (buff[pi] == NET_EFLAG) {
				// Nothing TODO:
			}
			break;

		case 1:
			packet_buffer[packet_Rx_Length++] = buff[pi];
			packet_length = ((buff[pi]) & 0xFF);
			packet_state = 3;
			break;

		case 2:
			packet_buffer[packet_Rx_Length++] = buff[pi];
			packet_length |= ((buff[pi]) & 0xFF);

			if (packet_length >= 128)
				packet_state = 0;

			packet_state = 3;

			break;

		case 3:
			packet_buffer[packet_Rx_Length++] = buff[pi];

			if (packet_Rx_Length >= (packet_length + 2))
				packet_state = 4;

			break;

		case 4:
			packet_buffer[packet_Rx_Length++] = buff[pi];
			// TODO: check sum
			packet_state = 5;

			break;

		case 5:
			if (buff[pi] == NET_EFLAG) {
				packet_state = 0;

				packet_buffer[packet_Rx_Length++] = buff[pi];

				// Send PacketMessage
				XNetPacketFromServer(&packet_buffer[2], packet_length);
			}

			break;

		default:
			packet_state = 0;
		}
	}

	return 0;
}

//end of file

