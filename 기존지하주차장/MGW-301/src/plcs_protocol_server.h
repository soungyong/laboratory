#ifndef __PLCS_NCP_SERVER_PROTOCOL_H__
#define __PLCS_NCP_SERVER_PROTOCOL_H__

#include "PLCS_Protocol.h"

#define NET_NOT_CONNECT				0	
#define NET_REGISTER				1

extern uint8 plcs_NCP_SERVER_NetState;
extern uint16 plcs_NCP_SERVER_PingInterval;
extern uint8 plcs_NCP_SERVER_Ping_Count;

void plcs_NCP_SERVER_ProcessMessage(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_Server_ProcessMessage(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

void plcs_NCP_SERVER_HandlePingRes(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_NCP_SERVER_HandleRegisterRes(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_NCP_SERVER_HandleRegisterNodeRes(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_NCP_SERVER_HandleSendTimeInfo(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

void plcs_GCP_SERVER_HandleStateInfoReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_SERVER_HandleNumOfDeviceReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_SERVER_HandleDeviceInfoReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_SERVER_HandleControlSensorSharingReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_SERVER_HandleControlGatewayReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_SERVER_HandleControlRebootReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_SERVER_HandleDebugLogReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_GCP_SERVER_HandleResetDebugLogReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);


void plcs_NCP_SERVER_SendPingReq();
void plcs_NCP_SERVER_SendRegisterReq();
void plcs_NCP_SERVER_SendRegisterNodeReq(uint16 subNodeId, uint16 deviceType,
		uint16 deviceId, uint8 deviceVersion, uint8 firmwareVersion,
		uint16 netAddr);

void plcs_GCP_SERVER_SendStateInfoRes(uint16 seqNum); //gateway.
void plcs_GCP_SERVER_SendNumOfDeviceRes(uint16 seqNum); //gateway.
void plcs_GCP_SERVER_SendDeviceInfoRes(uint16 seqNum, uint8 index); //gateway.
void plcs_GCP_SERVER_SendControlSensorSharingRes(uint16 seqNum, uint8 result); //gateway.
void plcs_GCP_SERVER_SendControlGatewayRes(uint16 seqNum, uint8 result);
void plcs_GCP_SERVER_SendDebugLogRes(uint16 seqNum);




uint8 plcs_NCP_SERVER_GetNetworkState();
uint16 plcs_NCP_SERVER_GetPingIntervalTime();
uint8 plcs_NCP_SERVER_GetPingCount();

#endif
