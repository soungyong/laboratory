#ifndef __PLCS_NCP_LC100_PROTOCOL_H__
#define __PLCS_NCP_LC100_PROTOCOL_H__

#include "PLCS_Protocol.h"
#include "../iinchip/types.h"

void plcs_NCP_LC100_ProcessMessage(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

void plcs_NCP_LC100_HandlePingReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_NCP_LC100_HandleRegisterReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_NCP_LC100_HandleRegisterNodeReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

void plcs_GCP_LC100_HandleNoticeEventNeighbor(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);

void plcs_NCP_LC100_SendRegisterRes(uint16 seqNum, uint16 nodeId, uint16 result, uint16 timeout);
void plcs_NCP_LC100_SendPingRes(uint16 seqNum, uint16 nodeID, uint8 flag, uint16 pingInterval);
void plcs_NCP_LC100_SendTimeInfo(uint16 nodeId, uint8 hour, uint8 min);

void plcs_GCP_LC100_SendNoticeEventNeighbor(uint16 nodeId, uint16 lcId, uint16 sensorId);

uint16 plcs_NCP_LC100_GetToken();
char plcs_NCP_LC100_AssignToken(uint16 nodeId);
void plcs_NCP_LC100_HandleReleaseTokenReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_NCP_LC100_SendReleaseTokenRes(uint16 nodeId, uint16 seqNum);
void plcs_NCP_LC100_SendRegisterBroadcastReq(char flag);

extern uint8 isOnSensorSharing;

#endif
