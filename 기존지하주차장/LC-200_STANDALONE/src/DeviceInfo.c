#include "DeviceInfo.h"
#include "SSRControl.h"
#include "ScheduleTable.h"
#include "Flash.h"
#include <avr/wdt.h>

//Circuit_State_Info ee_SSRInfo EEMEM;
uint8 tempBuffForDeviceInfo[255];

void handleDeviceControl(uint16 nodeId, int circuitId, uint8 ctrlMode) {

	tmp_CircuitInfo.ctrlMode[circuitId] = ctrlMode;
	tmp_CircuitInfo.state[circuitId] = ctrlMode;
	tmp_CircuitInfo.expirationTime[circuitId] = 0xffff;
	switch (ctrlMode) {
	case OFF:
		break;
	case ON:
		break;
	case SENSOR:
		tmp_CircuitInfo.onCtrlMode[circuitId] = ON;
		tmp_CircuitInfo.offCtrlMode[circuitId] = OFF;
		break;
	case DIMMING_0:
	case DIMMING_1:
	case DIMMING_2:
	case DIMMING_3:
	case DIMMING_4:
	case DIMMING_5:
	case DIMMING_6:
	case DIMMING_7:
	case DIMMING_8:
	case DIMMING_9:
	case DIMMING_10:
		break;
	case ILLU_DIMMING_0:
	case ILLU_DIMMING_1:
	case ILLU_DIMMING_2:
	case ILLU_DIMMING_3:
	case ILLU_DIMMING_4:
	case ILLU_DIMMING_5:
	case ILLU_DIMMING_6:
	case ILLU_DIMMING_7:
	case ILLU_DIMMING_8:
	case ILLU_DIMMING_9:
	case ILLU_DIMMING_10:
		tmp_CircuitInfo.prevIlluState[circuitId][ctrlMode - ILLU_DIMMING_0] = ctrlMode - ILLU_DIMMING_0 + DIMMING_0;
		tmp_CircuitInfo.state[circuitId] = tmp_CircuitInfo.prevIlluState[circuitId][ctrlMode - ILLU_DIMMING_0];
		break;
	case SCHEDULE:
		break;
	default:
		break;
	}
	//eeprom에 설정 정보 저장.
	ssrInfo_WriteToEEPRomOfIndex(circuitId);
}

void ssrInfo_WriteToEEPRom() {
	uint8 i, j;
//	eeprom_write_block(&tmp_CircuitInfo, &ee_SSRInfo, sizeof(Circuit_State_Info));

	for (i = 0; i < NumOfChannels; i++) {
		wdt_reset();
		tempBuffForDeviceInfo[0 + i] = tmp_CircuitInfo.ctrlMode[i];
		tempBuffForDeviceInfo[NumOfChannels + i] = tmp_CircuitInfo.state[i];
		tempBuffForDeviceInfo[NumOfChannels * 2 + i * 2] = tmp_CircuitInfo.expirationTime[i] >> 8;
		tempBuffForDeviceInfo[NumOfChannels * 2 + i * 2 + 1] = tmp_CircuitInfo.expirationTime[i];
		tempBuffForDeviceInfo[NumOfChannels * 4 + i * 2] = tmp_CircuitInfo.timerCnt[i] >> 8;
		tempBuffForDeviceInfo[NumOfChannels * 4 + i * 2 + 1] = tmp_CircuitInfo.timerCnt[i];
		tempBuffForDeviceInfo[NumOfChannels * 6 + i] = tmp_CircuitInfo.onCtrlMode[i];
		tempBuffForDeviceInfo[NumOfChannels * 7 + i] = tmp_CircuitInfo.offCtrlMode[i];
	}
	flash_writeBytes(FLASH_ADDRESS_DEVICEINFO, tempBuffForDeviceInfo, 255);

	for (i = 0; i < NumOfChannels; i++) {
		wdt_reset();
		for (j = 0; j < 11; j++)
			tempBuffForDeviceInfo[i * 11 + j] = tmp_CircuitInfo.prevIlluState[i][j];
	}
	flash_writeBytes(FLASH_ADDRESS_DEVICEINFO + 1, tempBuffForDeviceInfo, 255);

	for (i = 0; i < NumOfChannels; i++) {
		wdt_reset();
		for (j = 0; j < 11; j++)
			tempBuffForDeviceInfo[i * 11 + j] = tmp_CircuitInfo.prevIlluStateAge[i][j];
	}
	flash_writeBytes(FLASH_ADDRESS_DEVICEINFO + 2, tempBuffForDeviceInfo, 255);
}

void ssrInfo_WriteToEEPRomOfIndex(uint8 circuitId) {
	uint8 j;

	/* eeprom_write_block(&(tmp_CircuitInfo.ctrlMode[circuitId]), &(ee_SSRInfo.ctrlMode[circuitId]),
	 sizeof(uint8));
	 eeprom_write_block(&(tmp_CircuitInfo.expirationTime[circuitId]),
	 &(ee_SSRInfo.expirationTime[circuitId]), sizeof(uint16));*/

	flash_readBytes(FLASH_ADDRESS_DEVICEINFO, tempBuffForDeviceInfo, 255);
	tempBuffForDeviceInfo[0 + circuitId] = tmp_CircuitInfo.ctrlMode[circuitId];
	tempBuffForDeviceInfo[NumOfChannels + circuitId] = tmp_CircuitInfo.state[circuitId];
	tempBuffForDeviceInfo[NumOfChannels * 2 + circuitId * 2] = tmp_CircuitInfo.expirationTime[circuitId] >> 8;
	tempBuffForDeviceInfo[NumOfChannels * 2 + circuitId * 2 + 1] = tmp_CircuitInfo.expirationTime[circuitId];
	tempBuffForDeviceInfo[NumOfChannels * 4 + circuitId * 2] = tmp_CircuitInfo.timerCnt[circuitId] >> 8;
	tempBuffForDeviceInfo[NumOfChannels * 4 + circuitId * 2 + 1] = tmp_CircuitInfo.timerCnt[circuitId];
	tempBuffForDeviceInfo[NumOfChannels * 6 + circuitId] = tmp_CircuitInfo.onCtrlMode[circuitId];
	tempBuffForDeviceInfo[NumOfChannels * 7 + circuitId] = tmp_CircuitInfo.offCtrlMode[circuitId];
	flash_writeBytes(FLASH_ADDRESS_DEVICEINFO, tempBuffForDeviceInfo, 255);

	flash_readBytes(FLASH_ADDRESS_DEVICEINFO + 1, tempBuffForDeviceInfo, 255);
	for (j = 0; j < 11; j++)
		tempBuffForDeviceInfo[circuitId * 11 + j] = tmp_CircuitInfo.prevIlluState[circuitId][j];
	flash_writeBytes(FLASH_ADDRESS_DEVICEINFO + 1, tempBuffForDeviceInfo, 255);

	flash_readBytes(FLASH_ADDRESS_DEVICEINFO + 2, tempBuffForDeviceInfo, 255);
	for (j = 0; j < 11; j++)
		tempBuffForDeviceInfo[circuitId * 11 + j] = tmp_CircuitInfo.prevIlluStateAge[circuitId][j];
	flash_writeBytes(FLASH_ADDRESS_DEVICEINFO + 2, tempBuffForDeviceInfo, 255);
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Initialize Mapping Table  ------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
/*
 typedef struct CircuitInfo
 {
 uint8 ctrlMode[NumOfChannels];
 uint8 state[NumOfChannels];
 uint16 expirationTime[NumOfChannels]; //timer_cnt가 expirationTime이 되면 꺼짐.0: 시간 증가 안함. 항상 꺼짐. 0xffff 항상 켜짐. 시간 증가 안함.else 시간 증가.
 uint16 timerCnt[NumOfChannels];
 uint8 onCtrlMode[NumOfChannels];
 uint8 offCtrlMode[NumOfChannels];
 uint8 prevIlluState[NumOfChannels][11];
 uint8 prevIlluStateAge[NumOfChannels][11];
 }Circuit_State_Info;
 */

void initDeviceTable() {
	uint8 i = 0;
	uint8 j = 0;
	deviceInfoTable.size = 0;

	//eeprom_read_block(&tmp_CircuitInfo, &ee_SSRInfo, sizeof(Circuit_State_Info));

	flash_readBytes(FLASH_ADDRESS_DEVICEINFO, tempBuffForDeviceInfo, 255);
	for (i = 0; i < NumOfChannels; i++) {
		wdt_reset();
		tmp_CircuitInfo.ctrlMode[i] = tempBuffForDeviceInfo[0 + i];
		tmp_CircuitInfo.state[i] = tempBuffForDeviceInfo[NumOfChannels + i];
		tmp_CircuitInfo.expirationTime[i] = tempBuffForDeviceInfo[NumOfChannels * 2 + i * 2] << 8 | tempBuffForDeviceInfo[NumOfChannels * 2 + i * 2 + 1];
		tmp_CircuitInfo.timerCnt[i] = tempBuffForDeviceInfo[NumOfChannels * 4 + i * 2] << 8 | tempBuffForDeviceInfo[NumOfChannels * 4 + i * 2 + 1];
		tmp_CircuitInfo.onCtrlMode[i] = tempBuffForDeviceInfo[NumOfChannels * 6 + i];
		tmp_CircuitInfo.offCtrlMode[i] = tempBuffForDeviceInfo[NumOfChannels * 7 + i];
	}

	flash_readBytes(FLASH_ADDRESS_DEVICEINFO + 1, tempBuffForDeviceInfo, 255);
	for (i = 0; i < NumOfChannels; i++) {
		wdt_reset();
		for (j = 0; j < 11; j++)
			wdt_reset();
		tmp_CircuitInfo.prevIlluState[i][j] = tempBuffForDeviceInfo[i * 11 + j];
	}
	flash_readBytes(FLASH_ADDRESS_DEVICEINFO + 2, tempBuffForDeviceInfo, 255);
	for (i = 0; i < NumOfChannels; i++) {
		wdt_reset();
		for (j = 0; j < 11; j++)
			wdt_reset();
		tmp_CircuitInfo.prevIlluStateAge[i][j] = tempBuffForDeviceInfo[i * 11 + j];
	}

	for (i = 0; i < NumOfChannels; i++) {
		if (tmp_CircuitInfo.ctrlMode[i] == 0xff)
			tmp_CircuitInfo.ctrlMode[i] = ON;
		tmp_CircuitInfo.timerCnt[i] = 0;
		for (j = 0; j < 11; j++) {
			tmp_CircuitInfo.prevIlluState[i][j] = DIMMING_0 + j;
			tmp_CircuitInfo.prevIlluStateAge[i][j] = 0;
		}
	}
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Find Node Mapping Table  ------------------------------------------------------ //
// ---------------------------------------------------------------------------------------------- //
device_Info *findNodeById(uint16 node_Id) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++) {
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id)
			return &(deviceInfoTable.deviceInfo[i]);
	}
	return NULL;
}

int isContainNodeOfId(uint16 nodeId) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].ieeeId == nodeId)
			return 1;
	return 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
device_Info *addDeviceTable(uint16 node_Id, uint16 node_Type, uint8 device_version, uint8 fw_version, uint16 netAddr) {
	uint8 i = 0;

	if (deviceInfoTable.size >= MaxNumOfDevice)
		return NULL;

	deviceInfoTable.deviceInfo[deviceInfoTable.size].ieeeId = node_Id;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].deviceType = node_Type;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].deviceVersion = device_version;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].fwVersion = fw_version;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].cnt = 0;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].connectionState = ONLINE;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].resendCnt = 0;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].luxAvr = 0;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].netAddr = netAddr;

	deviceInfoTable.size++;

	return NULL;
}

int updateDeviceTable(uint16 node_Id, uint16 node_Type, uint8 device_version, uint8 fw_version) {
	device_Info *pUpdate;

	if ((pUpdate = findNodeById(node_Id)) != NULL) {
		DEBUG("\n    NOT NULL");
		pUpdate->cnt = 0;
		pUpdate->connectionState = ONLINE;
		pUpdate->resendCnt = 0;
		pUpdate->deviceVersion = device_version;
		pUpdate->fwVersion = fw_version;
		pUpdate->deviceType = node_Type;
	} else {
		addDeviceTable(node_Id, node_Type, device_version, fw_version, 0);
	}
	return 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Remove Node & Mapping Table  ------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void removeNode(uint16 node_Id) {
	uint8 i = 0, j = 0;

	for (i = 0; i < deviceInfoTable.size; i++) {
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id) {
			for (j = i; j < deviceInfoTable.size - 1; j++)
				deviceInfoTable.deviceInfo[j] = deviceInfoTable.deviceInfo[j + 1];
			deviceInfoTable.size--;
			break;
		}
	}
}

void removeDeviceTable() {
	deviceInfoTable.size = 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Count Node & Device Table  ---------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint8 countDeviceTable(uint16 node_Type) {
	uint8 device_count = 0;
	uint8 i = 0;

	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].deviceType == node_Type)
			device_count++;
	return device_count;

}

uint8 countDevice() {
	return deviceInfoTable.size;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Get Node ID Table  ----------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //

void deviceInfo_UpdateSSRInfo() {
	tmp_PrevCircuitInfo = tmp_CircuitInfo;
}

int deviceInfo_IsUpdatedSSRInfo() {
	uint8 i = 0;
	for (i = 0; i < NumOfChannels; i++) {
		if (tmp_PrevCircuitInfo.state[i] != tmp_CircuitInfo.state[i])
			return 1;
		if (tmp_PrevCircuitInfo.expirationTime[i] != tmp_CircuitInfo.expirationTime[i])
			return 1;
	}

	return 0;
}

// Enf of File
