#include "rotary.h"
#include <avr/io.h>


void initRotary(){
    DDRH &= ~(0xF0);
    DDRE &= ~(0xF0);

	PORTH |= 0xF0;
	PORTE |= 0xF0;
}

uint8 rotary_GetValue() {
    uint8 id=0;

	id = (PINH & 0xF0) | (PINE>>4);
    
    return 0xff-id;    
}
