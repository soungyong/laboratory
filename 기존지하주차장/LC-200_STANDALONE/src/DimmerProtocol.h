#ifndef __DIMMERPROTOCOL_H
#define __DIMMERPROTOCOL_H

#include "Util.h"
#include "NCProtocol.h"

#define PLCS_DCP_PROTOCOL_ID 0x11
#define PLCS_DCP_REQ_DIMMING 0x32




typedef struct DimmerControlInfo
{
	uint16 dimmerId;
	uint8 prevChannelState[4];
	uint8 *channelState[4];
}dimmerControl_Info_st;

typedef struct DimmerControlInfoList
{
	dimmerControl_Info_st info[20];
	uint8 size;
}dimmerControlInfoList_st;
dimmerControlInfoList_st dimmerControlInfoList;

void initDimmerControlInfoList();
void updateDimmerControlInfoList();


void dimmer_SendReqDimming(dimmerControl_Info_st *dimmerControlInfo);
uint8 dimmer_DimmingConvert(uint8 DimLevel);

void dimmer_control(dimmerControl_Info_st *dimmerControlInfo);

#endif
