#ifndef __MAIN_H__
#define __MAIN_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>


#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#include "XNet.h"
#include "Util.h"
#include "Uart.h"
#include "Xcps.h"
#include "DeviceInfo.h"
#include "ZRMProtocol.h"
#include "NCProtocol.h"
#include "XNetProtocol.h"
#include "Timer.h"
#include "ScheduleTable.h"
#include "MappingTable.h"


/********/
//#include <avrx.h>
//#include <avrx-io.h>
//#include <avrx-signal.h>
/********/


// YELLOW LED(LOW ACTIVE) : LC-100Z State Check LED
#define STATE_LED_ON()		(PORTG &= ~0x20)			// Low
#define STATE_LED_OFF()		(PORTG |= 0x20)				// High
#define STATE_LED_TOGGLE()	(PORTG ^= 0x20)				// TOGGLING


void InitMCU();
void xmem_enable();
void Start_LED();
void WDT_INIT();

#endif

