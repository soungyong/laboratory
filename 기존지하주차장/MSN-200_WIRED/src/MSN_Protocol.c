#include "MSN_Protocol.h"
#include "xcps.h"
#include "nc_protocol.h"
#include "zrmProtocol.h"
// --------------------------------------------------------------------------- //

uint8 isNormalMode=1;
uint8 Sensing_Threshold_Avr_Diff=80;

uint16 temperature=0;
uint16 illumination=0;

uint8 tmp_BuffforMSN[64];
volatile uint8 payloadForMSN[30];

////////////

void GCP_ProcessMessageFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[], int length) {
	uint8 msgType;

	msgType = msg[8];

	switch (msgType) { // Msg Type of NCP
	case PLCS_GCP_REQ_STATE_INFO:
		GCP_ProcessStateInfoReqFromZigbee(srcId, dstId, msg, length);
		break;
	case PLCS_GCP_REQ_REBOOT:
		GCP_ProcessRebootReqFromZigbee(srcId, dstId, msg, length);
		break;
	case PLCS_GCP_REQ_SET_SENSINGLEVEL:
		GCP_ProcessSetSensingLevelReqFromZigbee(srcId, dstId, msg, length);
		break;
	case PLCS_GCP_REQ_SET_MODE:
		GCP_ProcessSetModeReqFromZigbee(srcId, dstId, msg, length);
		break;
	default:
		break;
	}
}
void GCP_ProcessStateInfoReqFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[], int length) {
	GCP_SendStateInfoRes();
}

void GCP_ProcessRebootReqFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[], int length) {
	while (1)
		;
}
void GCP_ProcessSetSensingLevelReqFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[],
		int length) {
	uint16 seqNum = 0;
	uint16 sensingLevel = 0;
	seqNum = msg[2] << 8 | msg[3];
	sensingLevel = msg[13];
	Sensing_Threshold_Avr_Diff = sensingLevel;
	GCP_SendSetSensingLevelRes(seqNum);
}
void GCP_ProcessSetModeReqFromZigbee(uint16 srcId, uint16 dstId, uint8 msg[], int length) {
	uint8 mode;
	mode = msg[13];
	if (mode == 0x01)
		isNormalMode = 0;
	else
		isNormalMode = 1;
	GCP_SendSetModeRes();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
/////////////////////////////////////////////////////////////////////////////////////////////////////////

void GCP_SendStateInfoRes() {
	uint8 len = 0;
	uint8 resultLen = 0;

	payloadForMSN[len++] = PLCS_GCP_RES_STATE_INFO;
	payloadForMSN[len++] = 0;
	payloadForMSN[len++] = 0;
	payloadForMSN[len++] = tmp_zrmp.zrm_Id >> 8;
	payloadForMSN[len++] = tmp_zrmp.zrm_Id;
	payloadForMSN[len++] = PLCS_ZSENSOR_TYPE >> 8;
	payloadForMSN[len++] = PLCS_ZSENSOR_TYPE;
	payloadForMSN[len++] = Sensing_Threshold_Avr_Diff;
	payloadForMSN[len++] = temperature >>8;
	payloadForMSN[len++] = temperature;
	payloadForMSN[len++] = illumination >>8;
	payloadForMSN[len++] = illumination;

	resultLen = plcs_GetGCPMessage(tmp_BuffforMSN, 0, tmp_zrmp.zrm_Id, payloadForMSN,
			len);

	sendToZigbee(0, tmp_BuffforMSN, resultLen);
}
void GCP_SendNoticeEvent() {
	int len = 0;
	uint8 resultLen = 0;

	payloadForMSN[len++] = PLCS_GCP_NOTICE_EVENT;
	payloadForMSN[len++] = 0;
	payloadForMSN[len++] = 0;
	payloadForMSN[len++] = 0x01;
	payloadForMSN[len++] = tmp_zrmp.zrm_Id >> 8;
	payloadForMSN[len++] = tmp_zrmp.zrm_Id;

	resultLen = plcs_GetGCPMessage(tmp_BuffforMSN, 0, tmp_zrmp.zrm_Id, payloadForMSN,
			len);

	sendToZigbee(0, tmp_BuffforMSN, resultLen);

}
void GCP_SendSetSensingLevelRes(uint16 seqNum) {
	int len = 0;
	uint8 resultLen = 0;

	payloadForMSN[len++] = PLCS_GCP_RES_SET_SENSINGLEVEL;
	payloadForMSN[len++] = 0;
	payloadForMSN[len++] = 0;
	payloadForMSN[len++] = tmp_zrmp.zrm_Id >> 8;
	payloadForMSN[len++] = tmp_zrmp.zrm_Id;
	payloadForMSN[len++] = 0;

	resultLen = plcs_GetGCPResMessage(tmp_BuffforMSN, 0, tmp_zrmp.zrm_Id,
			payloadForMSN, len, seqNum);

	sendToZigbee(0, tmp_BuffforMSN, resultLen);
}
void GCP_SendSetModeRes() {
	int len = 0;
	uint8 resultLen = 0;

	payloadForMSN[len++] = PLCS_GCP_RES_SET_MODE;
	payloadForMSN[len++] = 0;
	payloadForMSN[len++] = 0;
	payloadForMSN[len++] = tmp_zrmp.zrm_Id >> 8;
	payloadForMSN[len++] = tmp_zrmp.zrm_Id;
	payloadForMSN[len++] = 0;

	resultLen = plcs_GetGCPMessage(tmp_BuffforMSN, 0, tmp_zrmp.zrm_Id, payloadForMSN,
			len);

	sendToZigbee(0, tmp_BuffforMSN, resultLen);
}

void GCP_SendNoticeEvent_Debug(uint8 debugSize, uint16 data[], uint16 standardAvr, uint8 avrDiff, uint8 isSensoredAvr, uint8 isSensoredAvr2) {
	int len = 0;
	uint8 i = 0;

	uint8 resultLen = 0;

	payloadForMSN[len++] = PLCS_GCP_NOTICE_EVENT_DEBUG;
	payloadForMSN[len++] = 0;
	payloadForMSN[len++] = 0;
	payloadForMSN[len++] = tmp_zrmp.zrm_Id >> 8;
	payloadForMSN[len++] = tmp_zrmp.zrm_Id;
	payloadForMSN[len++] = ((int) standardAvr + avrDiff)
			>> 8;
	payloadForMSN[len++] = ((int) standardAvr + avrDiff);
	payloadForMSN[len++] = isSensoredAvr * 10;
	payloadForMSN[len++] = isSensoredAvr2 * 10;
	payloadForMSN[len++] = debugSize;
	for (i = 0; i < debugSize; i++) {
		payloadForMSN[len++] = data[i] >> 8;
		payloadForMSN[len++] = data[i];
	}

	resultLen = plcs_GetGCPMessage(tmp_BuffforMSN, 0, tmp_zrmp.zrm_Id, payloadForMSN,
			len);

	sendToZigbee(0, tmp_BuffforMSN, resultLen);

}
/////////////////////////////////////////////////////////////////////////////////////////////////////////
//util
/////////////////////////////////////////////////////////////////////////////////////////////////////////
uint16 seqNumGeneratorForGCP = 0;
uint16 getGCPSeqNumGenerator() {
	return seqNumGeneratorForGCP++;
}

uint8 plcs_GetGCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getNCPSeqNumGenerator();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_GC_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetGCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_GC_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 MSN_GetSensorDiff(){
	return Sensing_Threshold_Avr_Diff;
}
uint8 MSN_IsNormalMode(){
	return isNormalMode;
}

void MSN_SetSensorDiff(uint8 diff) {
	Sensing_Threshold_Avr_Diff = diff;
}
void MSN_SetNormalMode(uint8 isNormal) {
	isNormalMode = isNormal;
}

uint16 MSN_GetTemperature(){
	return temperature;
}
uint16 MSN_GetIllumination(){
	return illumination;
}

void MSN_SetTemperature(uint16 value){
	temperature = value;
}
void MSN_SetIllumination(uint16 value){
	illumination = value;
}
