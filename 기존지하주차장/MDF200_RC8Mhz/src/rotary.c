#include "rotary.h"
#include <avr/io.h>

//H_8: PD5
//H_4: PB2
//H_2: PD6
//H_1: PB1

//L_8: PC5
//L_4: PD7
//L_2: PC4
//L_1: PD4



void initRotary(){
	DDRD &= ~0xf0;
    DDRB &= ~0x06;
    DDRC &= ~0x30;

	PORTD |= 0xF0;
	PORTB |= 0x06;
	PORTC |= 0x30;
}

uint8 rotary_GetValue() {
    uint8 id=0;
    
    if((PINC & 0x20)==0) id |= (0x01 << 7);
    if((PIND & 0x80)==0) id |= (0x01 << 6);
    if((PINC & 0x10)==0) id |= (0x01 << 5);
    if((PIND & 0x10)==0) id |= (0x01 << 4);

    if((PIND & 0x20)==0) id |= (0x01 << 3);
    if((PINB & 0x04)==0) id |= (0x01 << 2);
    if((PIND & 0x40)==0) id |= (0x01 << 1);
    if((PINB & 0x02)==0) id |= (0x01 << 0);

    return id;
}
