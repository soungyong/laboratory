#include "main.h"
#include "Rotary.h"
#include "xcps.h"
#include "flash.h"
#include "GC_Protocol.h"
#include "adc.h"
#include "MyDebug.h"

#define DISABLE_INTERRUPT()     asm("cli")
#define ENABLE_INTERRUPT()      asm("sei")

PowerMeter pm_value;

PowerMeter pm_value_eep EEMEM;

// Avrx hw Peripheral initialization
#define CPUCLK 		16000000L     		// CPU xtal
#define TICKRATE 	1000				// AvrX timer queue Tick rate
#define TCNT0_INIT 	(CPUCLK/128/TICKRATE)

// NB: Clock divisor bits are different between classic and mega103 chips!
// NB: IAR chooses to use different CPU identifiers for their C compiler
// NB: GCC changes the IO bit names just for fun...

#define TCCR0_INIT	((1<<CS02) | (0<<CS01) | (1<<CS00))

volatile uint32 g_PowerMeterValue[4] = { 0x00, 0x00, 0x00, 0x00 };
volatile uint16 g_Temperature = 0;
volatile uint16 g_Illumination = 0;

//-----------------------------------------------------------------------------
//MCU Initialize
void InitMCU() {
	DDRB |= 0x01;
	PORTB |= 0x01;
}

void save_PowerMeter_Value() {

	DISABLE_INTERRUPT();
	eeprom_busy_wait();
	eeprom_write_block(&pm_value, &pm_value_eep, sizeof(PowerMeter));
	ENABLE_INTERRUPT();
	DEBUG("Save_PowerMeter!\n\r");
}

void load_PowerMeter_Value() {
	DISABLE_INTERRUPT();
	eeprom_busy_wait();
	eeprom_read_block(&pm_value, &pm_value_eep, sizeof(PowerMeter));
	ENABLE_INTERRUPT();

	if (pm_value.pm_value_0 == pm_value.pm_value_1) {
		g_PowerMeterValue[0] = pm_value.pm_value_0;
	} else if (pm_value.pm_value_0 == pm_value.pm_value_2) {
		g_PowerMeterValue[0] = pm_value.pm_value_0;
		//	g_PowerMeterValue[1] = 0;
		//	g_PowerMeterValue[2] = 0;
		//	g_PowerMeterValue[3] = 0;
	} else if (pm_value.pm_value_1 == pm_value.pm_value_2) {
		g_PowerMeterValue[0] = pm_value.pm_value_1;
		//	g_PowerMeterValue[1] = 0;
		//	g_PowerMeterValue[2] = 0;
		//	g_PowerMeterValue[3] = 0;

	}

}

void InitExternalInterrupt() {
	EICRA |= 0x0f;
	EIMSK |= 0x03;
}

void resetZigbee() {
	PORTB &= ~(0x01);
	MSLEEP(100);
	PORTB |= 0x01;
}

//----------------------------------------------------------------------//

void WDT_INIT() {
	MCUSR &= ~(1 << WDRF); // WatchDog Init(Low)
	wdt_enable(WDTO_2S);
// WatchDog Reset Time(High)
}
void WTD_Task() {
	if (timer_isfired(ON_WTD_TIMER_ID)) {

		wdt_reset();
		timer_clear(ON_WTD_TIMER_ID);
		timer_set(ON_WTD_TIMER_ID, 100);
	}
}

int i = 0;
void Power_Chack_Task() {
	if (timer_isfired(POWER_CHECK_ID)) {
//		flash_writeUint32(1, 1111);

//		if (adc_getValue(7) < 400) {
//			save_PowerMeter_Value();
//			i++;
//		}

		timer_clear(POWER_CHECK_ID);
		timer_set(POWER_CHECK_ID, 10000);
	}
}

void EEPROM_Task() {
	if (timer_isfired(EEPROM_SAVE_ID)) {
		pm_value.pm_value_0 = g_PowerMeterValue[0];
		pm_value.pm_value_1 = g_PowerMeterValue[0];
		pm_value.pm_value_2 = g_PowerMeterValue[0];

		_delay_ms(1);
		save_PowerMeter_Value();
		DEBUG("\n\r PowerMeterValue = %d\n\r", g_PowerMeterValue[0]);
		timer_clear(EEPROM_SAVE_ID);
//		timer_set(EEPROM_SAVE_ID, 1000);
		timer_set(EEPROM_SAVE_ID, 600000);
	}
}

void ZigbeeSend_Task() {

	if (timer_isfired(ZIGBEE_SEND_ID)) {
		timer_clear(ZIGBEE_SEND_ID);
		timer_set(ZIGBEE_SEND_ID, 100);

		sendQueueingMessage();
	}
}

uint8 ZigbeePacket[64];
uint8 msgFromZigbee[64];
void ZigbeeUsart_Task() {
	static int Recvlen = 0;
	static uint8 zigbeeHWResetCount = 0;

	if (timer_isfired(ON_ZIGBEE_PING_TIMER_ID)) {
		ZRMsendPing();
		zigbeeHWResetCount++;
		if (getZigbeeState() == ZRM_CONNECT) {
			timer_clear(ON_ZIGBEE_PING_TIMER_ID);
			timer_set(ON_ZIGBEE_PING_TIMER_ID, 10000);
		} else {
			timer_clear(ON_ZIGBEE_PING_TIMER_ID);
			timer_set(ON_ZIGBEE_PING_TIMER_ID, 1000);
		}
	}

	if (zigbeeHWResetCount > 5) {
		resetZigbee();
		zigbeeHWResetCount = 0;
	}

	if ((Recvlen = xcps_recv_zigbee(ZigbeePacket, 64)) > 0) {
		uint8 buff_len = 0;
		uint16 Src_Addr;
		uint16 Dst_Addr;
		uint16 destId = 0;

		switch (ZigbeePacket[0]) {
		case 0x00: // Send to GMProtocols (Gateway <-> RFM)
			buff_len = (Recvlen - 1);
			zigbeeHWResetCount = 0;
			ZRMPMessage(&ZigbeePacket[1], buff_len);
			break;
		case 0x10:
			Dst_Addr = (uint16) (ZigbeePacket[1] << 8) | (ZigbeePacket[2]);
			Src_Addr = (uint16) (ZigbeePacket[3] << 8) | (ZigbeePacket[4]);

			buff_len = ZRM_getOriginalCode(msgFromZigbee, &(ZigbeePacket[5]), Recvlen - 5);

			destId = (uint16) (msgFromZigbee[4] << 8) | (msgFromZigbee[5]);

			if (destId == tmp_zrmp.zrm_Id) {
				zigbeeHWResetCount = 0;
				XNetHandlerFromZigbee(Src_Addr, msgFromZigbee, buff_len);
			}
			break;

		default: // Error Mesaage
			break;
		}
	}
}

void NCP_Task() {
	if (timer_isfired(NCP_TIMER_ID)) {
		if (ncp_ConnState() == NCP_NET_NOT_CONNECT) {
			if (getZigbeeState() == ZRM_CONNECT)
				ncp_SendRegisterReq();

			timer_clear(NCP_TIMER_ID);
			timer_set(NCP_TIMER_ID, 10000);
		} else {
			if (getZigbeeState() == ZRM_CONNECT && ncp_ConnState() == NCP_NET_REGISTER)
				ncp_SendPingReq();

			timer_clear(NCP_TIMER_ID);
			timer_set(NCP_TIMER_ID, 10000);
		}
	}
}
void ADC_Task() {

	if (timer_isfired(ADC_TIMER_ID)) {
		g_Temperature = adc_getValue(6);
		g_Illumination = adc_getValue(7);

		if (ncp_ConnState() == NCP_NET_REGISTER) {
			plcs_GCP_SendStateInfoRes(0, tmp_zrmp.zrm_Id, g_Temperature, g_Illumination);
		}
		timer_clear(ADC_TIMER_ID);
		timer_set(ADC_TIMER_ID, 10000);
	}
}

void Powermeter_Task() {
	uint8 i = 0;
	if (timer_isfired(NOTIC_POWERMETER_TIMER_ID)) {
		for (i = 0; i < 4; i++) {
			plcs_GCP_SendPowermeterRes(0, tmp_zrmp.zrm_Id, i, g_PowerMeterValue[i]);
		}
		timer_clear(NOTIC_POWERMETER_TIMER_ID);
		timer_set(NOTIC_POWERMETER_TIMER_ID, 10000);
	}
}

ISR(INT0_vect) {
	g_PowerMeterValue[0]++;
	DEBUG("\n\r%ld\n\r", g_PowerMeterValue[0]);
	flash_writeUint32(0, g_PowerMeterValue[0]);

//plcs_MDFP_SendPowermeterRes(0, 0, g_PowerMeterValue);
}

//ISR(INT1_vect) {
//	g_PowerMeterValue[1]++;
//
//	flash_writeUint32(1, g_PowerMeterValue[1]);
//	//plcs_MDFP_SendPowermeterRes(0, 0, g_PowerMeterValue);
//}

//-----------------------------------------------------------------------------
// Main function
//-----------------------------------------------------------------------------
int main(void) {
	InitMCU();

	initRotary();
	InitUART();
	timer_init();
//	flash_init();
	initADC();
	wdt_reset();
	MSLEEP(1000);
	wdt_reset();
	xcps_init_zigbee(USART_Receive, USART_Transmit);
	WDT_INIT();

	timer_set(ON_WTD_TIMER_ID, 100);
	timer_set(NCP_TIMER_ID, 10000);
	timer_set(NOTIC_POWERMETER_TIMER_ID, 1000);
	timer_set(ADC_TIMER_ID, 10000);
	timer_set(ZIGBEE_SEND_ID, 10000);
//	timer_set(EEPROM_SAVE_ID, 600000);
	timer_set(POWER_CHECK_ID, 100);

	timer_set(ON_ZIGBEE_PING_TIMER_ID, 1000);

	g_PowerMeterValue[0] = flash_readUint32(0);
	g_PowerMeterValue[1] = flash_readUint32(1);
	g_PowerMeterValue[2] = flash_readUint32(2);
	g_PowerMeterValue[3] = flash_readUint32(3);
	InitExternalInterrupt();

	FILE mystdout = FDEV_SETUP_STREAM((void *)USART_Transmit, NULL, _FDEV_SETUP_WRITE);
	stdout = &mystdout;
//		load_PowerMeter_Value();

	DEBUG("==============START============\n\r");
	while (1) {
		WTD_Task();
//		EEPROM_Task();
		ZigbeeUsart_Task();
		NCP_Task();
		Powermeter_Task();
		ADC_Task();
		ZigbeeSend_Task();
		Power_Chack_Task();
	}
	return 0;
}

