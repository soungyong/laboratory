#ifndef __MAIN_H__
#define __MAIN_H__

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/eeprom.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "XNet.h"
#include "Util.h"
#include "Uart.h"
#include "Xcps.h"
#include "ZRMProtocol.h"
#include "NCProtocol.h"
#include "XNetProtocol.h"
#include "Timer.h"

void InitMCU();
void WDT_INIT();

struct _powermeter;
typedef struct _powermeter PowerMeter;

struct _powermeter {
	unsigned long pm_value_0; // 초기화 여부
	unsigned long pm_value_1; // 초기화 여부
	unsigned long pm_value_2; // 초기화 여부
};

#endif

