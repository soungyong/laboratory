#ifndef __PLCS_RFU_PROTOCOL_H__
#define __PLCS_RFU_PROTOCOL_H__

#include "NCProtocol.h"
#include "util.h"

// ---------------------------------------------------------------------------------------------- //
#define PLCS_RFU_PROTOCOL_ID						(0xA0)

#define PLCS_RFU_REQ_UPGRADE						(0x01)
#define PLCS_RFU_RES_UPGRADE						(0x02)
#define PLCS_RFU_REQ_SEND_DATA						(0x03)
#define PLCS_RFU_RES_SEND_DATA						(0x04)

#define FLAG_PAGE_ADDR 16
#define DATA_PAGE_ADDR 17


uint16 getSeqNumGeneratorForRFUP();

uint8 plcs_GetRFUPPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len);
uint8 plcs_GetRFUPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum);

void plcs_RFUP_ProcessMessage(uint16 srcNetAddr, uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[],
		int length);

//RFUP Handle
void plcs_RFUP_HandleUpgradeFirmwareReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);
void plcs_RFUP_HandleSendDataReq(uint16 seqNum, uint16 srcId, uint16 destId, uint8 msg[], int length);


//RFUP Send
void plcs_RFUP_SendUpgradeFirmwareRes(uint16 seqNum, uint8 result);
void plcs_RFUP_SendDataRes(uint16 seqNum, uint8 result);


////////////////////////////////////////
uint8 plcs_RFUP_CheckDownloadedFirmware();
uint8 plcs_RFUP_CheckDownloadedFirmwareAt(uint16 index);
void plcs_RFUP_SetReadyToUpgradeFirmware(uint8 value);
void plcs_RFUP_Reboot();
uint8 plcs_RFUP_WriteFirmwareData(uint16 totalLen, uint16 index, uint8 len, uint8* data);

#endif
