#include "MappingTable.h"
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include "Flash.h"

//SensorSSRMappingTable_st ee_SensorMappingInfo EEMEM;
SensorMappingInfo_st ee_SensorMappingInfo[MaxNumOfSensorMappingInfo] EEMEM;
uint16 ee_SensorMappingTableSize EEMEM;

//DimmerSSRMappingTable_st ee_DimmerMappingInfo EEMEM;
DimmerMappingInfo_st ee_DimmerSSRMappingInfo[MaxNumOfDimmerMappingInfo] EEMEM;
uint16 ee_DimmerMappingTableSize EEMEM;

// ---------------------------------------------------------------------------------------------- //
// ------------------ Initialize Mapping Table  ------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint8 tempBuffForMappingInfo[255];
void initMappingTable() {
	uint16 i = 0;
	uint8 j = 0;

	//Page당 28개 mapping 정보 기록. 총 252바이트.
	for (i = 0; i < MaxNumOfSensorMappingInfo; i += 28) {
		wdt_reset();
		flash_readBytes(FLASH_ADDR_SENSORMAPPING_INFO + i / 28, tempBuffForMappingInfo, 255);
		for (j = 0; j < 28; j++) {
			sensorMappingTable.mappingInfo[i + j].circuitId = tempBuffForMappingInfo[0 + 9 * j];
			sensorMappingTable.mappingInfo[i + j].isEnable = tempBuffForMappingInfo[1 + 9 * j];
			sensorMappingTable.mappingInfo[i + j].lcId = (tempBuffForMappingInfo[2 + 9 * j] << 8) | tempBuffForMappingInfo[3 + 9 * j];
			sensorMappingTable.mappingInfo[i + j].offCtrlMode = tempBuffForMappingInfo[4 + 9 * j];
			sensorMappingTable.mappingInfo[i + j].onCtrlMode = tempBuffForMappingInfo[5 + 9 * j];
			sensorMappingTable.mappingInfo[i + j].onTime = tempBuffForMappingInfo[6 + 9 * j];
			sensorMappingTable.mappingInfo[i + j].sensorId = (tempBuffForMappingInfo[7 + 9 * j] << 8) | tempBuffForMappingInfo[8 + 9 * j];
		}
	}

	/*for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
	 eeprom_read_block(&(sensorMappingTable.mappingInfo[i]),
	 &(ee_SensorMappingInfo[i]), sizeof(SensorMappingInfo_st));
	 */
}

void sensorMappingInfo_WriteAll() {
	uint16 i = 0;
	uint8 j = 0;

	//Page당 28개 mapping 정보 기록. 총 252바이트.
	for (i = 0; i < MaxNumOfSensorMappingInfo; i += 28) {
		wdt_reset();
		for (j = 0; j < 28; j++) {
			tempBuffForMappingInfo[0 + 9 * j] = sensorMappingTable.mappingInfo[i + j].circuitId;
			tempBuffForMappingInfo[1 + 9 * j] = sensorMappingTable.mappingInfo[i + j].isEnable;
			tempBuffForMappingInfo[2 + 9 * j] = sensorMappingTable.mappingInfo[i + j].lcId >> 8;
			tempBuffForMappingInfo[3 + 9 * j] = sensorMappingTable.mappingInfo[i + j].lcId;
			tempBuffForMappingInfo[4 + 9 * j] = sensorMappingTable.mappingInfo[i + j].offCtrlMode;
			tempBuffForMappingInfo[5 + 9 * j] = sensorMappingTable.mappingInfo[i + j].onCtrlMode;
			tempBuffForMappingInfo[6 + 9 * j] = sensorMappingTable.mappingInfo[i + j].onTime;
			tempBuffForMappingInfo[7 + 9 * j] = sensorMappingTable.mappingInfo[i + j].sensorId >> 8;
			tempBuffForMappingInfo[8 + 9 * j] = sensorMappingTable.mappingInfo[i + j].sensorId;
		}

		flash_writeBytes(FLASH_ADDR_SENSORMAPPING_INFO + i / 28, tempBuffForMappingInfo, 255);
	}
}
void sensorMappingInfo_WriteToEEPRom(uint16 index) {
	uint8 pageAddr = FLASH_ADDR_SENSORMAPPING_INFO + index / 28;
	flash_readBytes(pageAddr, tempBuffForMappingInfo, 255);
	uint8 slotIndex = (index % 28) * 9;
	;

	//Page당 28개 mapping 정보 기록. 총 252바이트.

	tempBuffForMappingInfo[0 + slotIndex] = sensorMappingTable.mappingInfo[index].circuitId;
	tempBuffForMappingInfo[1 + slotIndex] = sensorMappingTable.mappingInfo[index].isEnable;
	tempBuffForMappingInfo[2 + slotIndex] = sensorMappingTable.mappingInfo[index].lcId >> 8;
	tempBuffForMappingInfo[3 + slotIndex] = sensorMappingTable.mappingInfo[index].lcId;
	tempBuffForMappingInfo[4 + slotIndex] = sensorMappingTable.mappingInfo[index].offCtrlMode;
	tempBuffForMappingInfo[5 + slotIndex] = sensorMappingTable.mappingInfo[index].onCtrlMode;
	tempBuffForMappingInfo[6 + slotIndex] = sensorMappingTable.mappingInfo[index].onTime;
	tempBuffForMappingInfo[7 + slotIndex] = sensorMappingTable.mappingInfo[index].sensorId >> 8;
	tempBuffForMappingInfo[8 + slotIndex] = sensorMappingTable.mappingInfo[index].sensorId;

	flash_writeBytes(pageAddr, tempBuffForMappingInfo, 255);

	//eeprom_write_block(&(sensorMappingTable.mappingInfo[index]), &(ee_SensorMappingInfo[index]),
	//	sizeof(SensorMappingInfo_st));

}
// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint16 getSensorMappingTableSize() {
	uint16 i;
	uint16 size = 0;
	for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
		if (sensorMappingTable.mappingInfo[i].isEnable == 1)
			size++;
	return size;
}

SensorMappingInfo_st * getSensorMappingInfoByIndex(uint16 index) {
	uint16 cnt = 0;
	uint16 i = 0;
	for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
		if (sensorMappingTable.mappingInfo[i].isEnable == 1) {
			if (cnt == index) {
				return &(sensorMappingTable.mappingInfo[i]);
			}
			cnt++;
		}
	return NULL;
}

SensorMappingInfo_st *addSensorMappingTable(uint16 lcId, uint16 sensorId, uint8 circuitId, uint8 onTime, uint8 onCtrlMode, uint8 offCtrlMode) {
	uint16 i = 0;
	if (getSensorMappingTableSize() >= MaxNumOfSensorMappingInfo)
		return NULL;

	for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
		if (sensorMappingTable.mappingInfo[i].isEnable != 1) {
			sensorMappingTable.mappingInfo[i].lcId = lcId;
			sensorMappingTable.mappingInfo[i].sensorId = sensorId;
			sensorMappingTable.mappingInfo[i].circuitId = circuitId;
			sensorMappingTable.mappingInfo[i].onTime = onTime;
			sensorMappingTable.mappingInfo[i].onCtrlMode = onCtrlMode;
			sensorMappingTable.mappingInfo[i].offCtrlMode = offCtrlMode;
			sensorMappingTable.mappingInfo[i].isEnable = 1;
			break;
		}

	if (i == MaxNumOfSensorMappingInfo) {
		return NULL;
	}

	return &(sensorMappingTable.mappingInfo[i]);
}

int updateSensorMappingTableByCircuit(uint8 circuitId, uint8 onTime, uint8 onCtrlMode, uint8 offCtrlMode) {
	int index;
	int result = 0;
	uint16 i = 0;

	for (i = 0; i < MaxNumOfSensorMappingInfo; i++)
		if (circuitId == sensorMappingTable.mappingInfo[i].circuitId && sensorMappingTable.mappingInfo[i].isEnable == 1) {
			sensorMappingTable.mappingInfo[i].onTime = onTime;
			sensorMappingTable.mappingInfo[i].onCtrlMode = onCtrlMode;
			sensorMappingTable.mappingInfo[i].offCtrlMode = offCtrlMode;
			sensorMappingInfo_WriteToEEPRom(i);
			wdt_reset();
		}

	return result;
}

int updateSensorMappingTable(uint16 lcId, uint16 sensorId, uint8 circuitId, uint8 onTime, uint8 onCtrlMode, uint8 offCtrlMode) {
	int index;
	int result = 0;
	if ((index = findSensorMappingInfoById(lcId, sensorId, circuitId)) != -1) {
		sensorMappingTable.mappingInfo[index].onTime = onTime;
		sensorMappingTable.mappingInfo[index].onCtrlMode = onCtrlMode;
		sensorMappingTable.mappingInfo[index].offCtrlMode = offCtrlMode;
		result = 2;
	} else if (addSensorMappingTable(lcId, sensorId, circuitId, onTime, onCtrlMode, offCtrlMode) != NULL) {
		result = 1;
	} else {
		return 0;
	}

	index = findSensorMappingInfoById(lcId, sensorId, circuitId);

	if (index != -1) {
		sensorMappingInfo_WriteToEEPRom(index);
	}

	return result;
}

int findSensorMappingInfoById(uint16 lcId, uint16 sensorId, uint8 circuitId) {
	uint16 i = 0;
	for (i = 0; i < MaxNumOfSensorMappingInfo; i++) {
		if (sensorId == sensorMappingTable.mappingInfo[i].sensorId && circuitId == sensorMappingTable.mappingInfo[i].circuitId && sensorMappingTable.mappingInfo[i].isEnable == 1) {
			return i;
		}
	}
	return -1; // 찾는 값이 없으면 '-1'을 리턴
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //

uint8 getDimmerMappingTableSize() {
	uint8 i;
	uint8 size = 0;
	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++)
		if (dimmerMappingTable.mappingInfo[i].isEnable == 1)
			size++;
	return size;
}

DimmerMappingInfo_st * getDimmerMappingInfoByIndex(uint8 index) {
	uint8 cnt = 0;
	uint8 i = 0;
	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++)
		if (dimmerMappingTable.mappingInfo[i].isEnable == 1) {
			if (cnt == index) {
				return &(dimmerMappingTable.mappingInfo[i]);
			}
			cnt++;
		}
	return NULL;
}

DimmerMappingInfo_st *addDimmerMappingTable(uint16 dimmerId, uint8 channelId, uint8 circuitId) {
	uint16 i = 0;

	if (getDimmerMappingTableSize() >= MaxNumOfDimmerMappingInfo)
		return NULL;

	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++) {
		if (dimmerMappingTable.mappingInfo[i].isEnable != 1) {
			dimmerMappingTable.mappingInfo[i].dimmerId = dimmerId;
			dimmerMappingTable.mappingInfo[i].channelId = channelId;
			dimmerMappingTable.mappingInfo[i].circuitId = circuitId;
			dimmerMappingTable.mappingInfo[i].isEnable = 1;
			break;
		}
	}

	if (i == MaxNumOfDimmerMappingInfo)
		return NULL;

	return &(dimmerMappingTable.mappingInfo[i]);
}

int updateDimmerMappingTable(uint16 dimmerId, uint8 channelId, uint8 circuitId) {
	int index;
	int result;

	if ((index = findDimmerMappingInfoById(dimmerId, channelId)) != -1) {
		return 0;
	} else {
		if (addDimmerMappingTable(dimmerId, channelId, circuitId) == NULL)
			return 2;
		else
			result = 1;
	}

	index = findDimmerMappingInfoById(dimmerId, channelId);
	if (index != -1)
		dimmerMappingInfo_WriteToEEPRom(index);
	return result;
}

int findDimmerMappingInfoByPair(uint16 dimmerId, uint8 channelId, uint8 circuitId) {
	uint16 i = 0;
	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++)
		if (dimmerId == dimmerMappingTable.mappingInfo[i].dimmerId && channelId == dimmerMappingTable.mappingInfo[i].channelId && circuitId == dimmerMappingTable.mappingInfo[i].circuitId)
			return i;
	return -1; // 찾는 값이 없으면 '-1'을 리턴
}

int findDimmerMappingInfoById(uint16 dimmerId, uint8 channelId) {
	uint16 i = 0;
	for (i = 0; i < MaxNumOfDimmerMappingInfo; i++)
		if (dimmerId == dimmerMappingTable.mappingInfo[i].dimmerId && channelId == dimmerMappingTable.mappingInfo[i].channelId && dimmerMappingTable.mappingInfo[i].isEnable == 1)
			return i;
	return -1; // 찾는 값이 없으면 '-1'을 리턴
}

void initDimmerMappingTable() {
	uint16 i = 0;
	uint8 j = 0;

	//Page당 51개 mapping 정보 기록. 총 255바이트.
	for (i = 0; i < MaxNumOfDimmerMappingInfo; i += 5) {
		wdt_reset();
		flash_readBytes(FLASH_ADDR_DIMMERMAPPING_INFO + i / 5, tempBuffForMappingInfo, 255);
		for (j = 0; j < 5; j++) {
			dimmerMappingTable.mappingInfo[i + j].channelId = tempBuffForMappingInfo[0 + 5 * j];
			dimmerMappingTable.mappingInfo[i + j].circuitId = tempBuffForMappingInfo[1 + 5 * j];
			dimmerMappingTable.mappingInfo[i + j].dimmerId = (tempBuffForMappingInfo[2 + 5 * j] << 8) | tempBuffForMappingInfo[3 + 5 * j];
			dimmerMappingTable.mappingInfo[i + j].isEnable = tempBuffForMappingInfo[4 + 5 * j];
		}
	}

	/*for (i = 0; i < MaxNumOfDimmerMappingInfo; i++)
	 eeprom_read_block(&(dimmerMappingTable.mappingInfo[i]), &(ee_DimmerSSRMappingInfo[i]),
	 sizeof(DimmerMappingInfo_st));*/
}

void dimmerMappingInfo_WriteToEEPRom(uint16 index) {
	uint8 pageAddr = FLASH_ADDR_DIMMERMAPPING_INFO + index / 5;
	flash_readBytes(pageAddr, tempBuffForMappingInfo, 255);
	uint8 slotIndex = (index % 5) * 5;
	;

	//Page당 28개 mapping 정보 기록. 총 252바이트.

	tempBuffForMappingInfo[0 + slotIndex] = dimmerMappingTable.mappingInfo[index].channelId;
	tempBuffForMappingInfo[1 + slotIndex] = dimmerMappingTable.mappingInfo[index].circuitId;
	tempBuffForMappingInfo[2 + slotIndex] = dimmerMappingTable.mappingInfo[index].dimmerId >> 8;
	tempBuffForMappingInfo[3 + slotIndex] = dimmerMappingTable.mappingInfo[index].dimmerId;
	tempBuffForMappingInfo[4 + slotIndex] = dimmerMappingTable.mappingInfo[index].isEnable;

	flash_writeBytes(pageAddr, tempBuffForMappingInfo, 255);

	/*eeprom_write_block(&(dimmerMappingTable.mappingInfo[index]), &(ee_DimmerSSRMappingInfo[index]),
	 sizeof(DimmerMappingInfo_st));*/
}

void dimmerMappingInfo_WriteAll() {
	uint16 i = 0;
	uint8 j = 0;

	//Page당 51개 mapping 정보 기록. 총 255바이트.
	for (i = 0; i < MaxNumOfDimmerMappingInfo; i += 5) {
		wdt_reset();
		for (j = 0; j < 5; j++) {
			tempBuffForMappingInfo[0 + 5 * j] = dimmerMappingTable.mappingInfo[i + j].channelId;
			tempBuffForMappingInfo[1 + 5 * j] = dimmerMappingTable.mappingInfo[i + j].circuitId;
			tempBuffForMappingInfo[2 + 5 * j] = dimmerMappingTable.mappingInfo[i + j].dimmerId >> 8;
			tempBuffForMappingInfo[3 + 5 * j] = dimmerMappingTable.mappingInfo[i + j].dimmerId;
			tempBuffForMappingInfo[4 + 5 * j] = dimmerMappingTable.mappingInfo[i + j].isEnable;

		}
		flash_writeBytes(FLASH_ADDR_DIMMERMAPPING_INFO + i / 5, tempBuffForMappingInfo, 255);
	}
}

// Enf of File
