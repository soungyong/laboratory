#ifndef __SSRCONTROL_H
#define __SSRCONTROL_H

#include "util.h"
#include "deviceinfo.h"
#include <avr/io.h>

// GREEN LED(LOW ACTIVE) : SSR State(ON/OFF)
#define LATCH_ON(x)						(PORTL |= (x))			// Low
#define LATCH_OFF(x) 						(PORTL &= ~(x))			// High

#define SSR_ON(x)						(PORTK |= (x))			// High
#define SSR_OFF(x) 						(PORTK &= ~(x))			// Low

uint8 GetSSRState(uint8 ssrID);

void SSR_init();
void SSR_On_Index(int);
void SSR_Off_Index(int);
#endif
