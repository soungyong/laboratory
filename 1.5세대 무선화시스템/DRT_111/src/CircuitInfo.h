#ifndef __CIRCUIT_INFO_H__
#define __CIRCUIT_INFO_H__


#include <stdio.h>
#include <string.h>  		// strcmp(), strcpy(), strlen() 함수등 주로 문자열과 관련된 함수사용시
#include <stdlib.h>  		// malloc() 함수 사용
#include <avr/pgmspace.h>
#include "Util.h"

#define NumOfChannels 4
#define OFF								0x00
#define ON								0x01
#define SENSOR							0x02
#define SCHEDULE						0x03
#define DIMMING_0						0x10
#define DIMMING_1						0x11
#define DIMMING_2						0x12
#define DIMMING_3						0x13
#define DIMMING_4						0x14
#define DIMMING_5						0x15
#define DIMMING_6						0x16
#define DIMMING_7						0x17
#define DIMMING_8						0x18
#define DIMMING_9						0x19
#define DIMMING_10						0x1A
#define ILLU_DIMMING_0					0x20
#define ILLU_DIMMING_1					0x21
#define ILLU_DIMMING_2					0x22
#define ILLU_DIMMING_3					0x23
#define ILLU_DIMMING_4					0x24
#define ILLU_DIMMING_5					0x25
#define ILLU_DIMMING_6					0x26
#define ILLU_DIMMING_7					0x27
#define ILLU_DIMMING_8					0x28
#define ILLU_DIMMING_9					0x29
#define ILLU_DIMMING_10					0x2A

typedef struct CircuitInfo
{
	uint8 ctrlMode[NumOfChannels];
	uint8 state[NumOfChannels];
	uint16 expirationTime[NumOfChannels]; //timer_cnt가 expirationTime이 되면 꺼짐.0: 시간 증가 안함. 항상 꺼짐. 0xffff 항상 켜짐. 시간 증가 안함.else 시간 증가.
	uint16 timerCnt[NumOfChannels];
	uint8 onCtrlMode[NumOfChannels];
	uint8 offCtrlMode[NumOfChannels];
	uint8 prevIlluState[NumOfChannels][11];
	uint8 prevIlluStateAge[NumOfChannels][11];
}Circuit_State_Info;

Circuit_State_Info tmp_CircuitInfo;
Circuit_State_Info tmp_PrevCircuitInfo;

void initCircuit();
void handleCircuitControl(uint16 nodeId,  int circuitId, uint8 ctrlMode);


#endif

