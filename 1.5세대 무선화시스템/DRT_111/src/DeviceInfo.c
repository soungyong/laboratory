#include "DeviceInfo.h"

// ---------------------------------------------------------------------------------------------- //
// ------------------ Initialize Mapping Table  ------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void initDeviceTable() {
	uint8 i = 0;
	deviceInfoTable.size = 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Find Node Mapping Table  ------------------------------------------------------ //
// ---------------------------------------------------------------------------------------------- //
device_Info *findNodeById(uint16 node_Id) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++) {
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id)
			return &(deviceInfoTable.deviceInfo[i]);
	}
	return NULL;
}

int isContainNodeOfId(uint16 Ieee_Addr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].ieeeId == Ieee_Addr)
			return 1;
	return 0;
}

int isContainNode(uint16 node_Id, uint16 nwkAddr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].nwkAddr == nwkAddr
				&& deviceInfoTable.deviceInfo[i].ieeeId == node_Id)
			return 1;

	return 0;
}

int isContainNodeOfNwkAddr(uint16 nwk_Addr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].nwkAddr == nwk_Addr)
			return 1;

	return 0; // 찾는 값이 없으면 '0'을 리턴
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
device_Info *addDeviceTable(uint16 node_Id, uint16 nwkAddr, uint16 node_Type, uint8 device_version,
		uint8 fw_version) {
	deviceInfoTable.deviceInfo[deviceInfoTable.size].ieeeId = node_Id;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].nwkAddr = nwkAddr;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].deviceType = node_Type;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].deviceVersion = device_version;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].fwVersion = fw_version;

	deviceInfoTable.size++;

	return NULL;
}

int updateDeviceTable(uint16 node_Id, uint16 node_Type, uint8 device_version, uint8 fw_version) {
	device_Info *pUpdate;

	if ((pUpdate = findNodeById(node_Id)) != NULL) {
		pUpdate->deviceType = node_Type;
		pUpdate->deviceVersion = device_version;
		pUpdate->fwVersion = fw_version;
		return 0;
	} else {
		if (isContainNodeOfId(node_Id) == 0)
			addDeviceTable(node_Id, 0, node_Type, device_version, fw_version);
	}
	return 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Remove Node & Mapping Table  ------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void removeNode(uint16 node_Id) {
	uint8 i = 0, j = 0;

	for (i = 0; i < deviceInfoTable.size; i++) {
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id) {
			for (j = i; j < deviceInfoTable.size - 1; j++)
				deviceInfoTable.deviceInfo[j] = deviceInfoTable.deviceInfo[j + 1];
			deviceInfoTable.size--;
			break;
		}
	}
}

void removeDeviceTable() {
	deviceInfoTable.size = 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Count Node & Device Table  ---------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint8 countDeviceTable(uint16 node_Type) {
	uint8 device_count = 0;
	uint8 i = 0;

	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].deviceType == node_Type)
			device_count++;
	return device_count;

}

uint8 countDevice() {
	return deviceInfoTable.size;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Get Node ID Table  ----------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint16 getNetAddr(uint16 node_Id) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id)
			return deviceInfoTable.deviceInfo[i].nwkAddr;
	return 0;
}

uint16 getNodeId(uint16 nwk_Addr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].nwkAddr == nwk_Addr)
			return deviceInfoTable.deviceInfo[i].ieeeId;
	return 0;

}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Print Node & Mapping Table  ---------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void printDeviceTable() {
	uint16 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		printf_P(PSTR("\n%d: %02X-%02X"), ++i, deviceInfoTable.deviceInfo[i].ieeeId,
				deviceInfoTable.deviceInfo[i].nwkAddr);
}

// Enf of File
