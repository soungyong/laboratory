#include "CircuitInfo.h"
#include <avr/wdt.h>

void initCircuit(){
	uint8 i=0;
	for(i=0; i < NumOfChannels; i++){
		tmp_CircuitInfo.ctrlMode[i] = SENSOR;
		tmp_CircuitInfo.expirationTime[i]=0xffff;
		tmp_CircuitInfo.offCtrlMode[i]=ON;
		tmp_CircuitInfo.onCtrlMode[i]=ON;
		tmp_CircuitInfo.state[i]=ON;
		tmp_CircuitInfo.timerCnt[i]=0;
	}
}

void handleCircuitControl(uint16 nodeId, int circuitId, uint8 ctrlMode) {
	if(circuitId >= NumOfChannels) return;

	tmp_CircuitInfo.ctrlMode[circuitId] = ctrlMode;
	tmp_CircuitInfo.state[circuitId] = ctrlMode;
	tmp_CircuitInfo.expirationTime[circuitId] = 0xffff;
	switch (ctrlMode) {
	case OFF:
		break;
	case ON:
		break;
	case SENSOR:
		tmp_CircuitInfo.onCtrlMode[circuitId] = ON;
		tmp_CircuitInfo.offCtrlMode[circuitId] = OFF;
		break;
	case DIMMING_0:
	case DIMMING_1:
	case DIMMING_2:
	case DIMMING_3:
	case DIMMING_4:
	case DIMMING_5:
	case DIMMING_6:
	case DIMMING_7:
	case DIMMING_8:
	case DIMMING_9:
	case DIMMING_10:
		break;
	case ILLU_DIMMING_0:
	case ILLU_DIMMING_1:
	case ILLU_DIMMING_2:
	case ILLU_DIMMING_3:
	case ILLU_DIMMING_4:
	case ILLU_DIMMING_5:
	case ILLU_DIMMING_6:
	case ILLU_DIMMING_7:
	case ILLU_DIMMING_8:
	case ILLU_DIMMING_9:
	case ILLU_DIMMING_10:
		tmp_CircuitInfo.prevIlluState[circuitId][ctrlMode - ILLU_DIMMING_0] = ctrlMode
				- ILLU_DIMMING_0 + DIMMING_0;
		tmp_CircuitInfo.state[circuitId] = tmp_CircuitInfo.prevIlluState[circuitId][ctrlMode
				- ILLU_DIMMING_0];
		break;
	case SCHEDULE:
		break;
	default:
		break;
	}
}
// Enf of File
