#ifndef __DEVICE_INFO_H__
#define __DEVICE_INFO_H__


#include <stdio.h>
#include <string.h>  		// strcmp(), strcpy(), strlen() 함수등 주로 문자열과 관련된 함수사용시
#include <stdlib.h>  		// malloc() 함수 사용
#include <avr/pgmspace.h>
#include "Util.h"
#include <avr/eeprom.h>

typedef struct DeviceInfo
{
	uint16 nwkAddr;					// nwk_id : Zigbee_id (Gateway)
	uint16 deviceType;
	uint16 ieeeId;					// IEEE	: Node_id (Gateway)	//
	uint8 deviceVersion;
	uint8 fwVersion;
	uint8 cnt;
}device_Info;

typedef struct {	
	device_Info deviceInfo[40];
	uint8 size;
} DeviceInfoTable_st;

DeviceInfoTable_st deviceInfoTable;

//--------------------------------------------------------------------------------------------//
void initDeviceTable();

device_Info *findNodeById(uint16 node_Id);
int isContainNodeOfNwkAddr(uint16 nwkAddr);
int isContainNodeOfId(uint16 node_Id);
int isContainNode(uint16 node_Id, uint16 nwkAddr);

device_Info *addDeviceTable(uint16 node_Id, uint16 nwkAddr, uint16 node_Type, uint8 device_version, uint8 fw_version);
int updateDeviceTable(uint16 node_Id, uint16 node_Type, uint8 device_version, uint8 fw_version);


void removeNode(uint16 node_Id);
void removeDeviceTable();

uint8 countDeviceTable(uint16 node_Type);
uint8 countDevice();

uint16 getNetAddr(uint16 node_Id);
uint16 getNodeId(uint16 net_Addr);

void printDeviceTable();


#endif

