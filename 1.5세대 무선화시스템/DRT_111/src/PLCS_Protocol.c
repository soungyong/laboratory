#include "PLCS_Protocol.h"
#include "NCProtocol.h"
#include <avr/wdt.h>
#include "DeviceInfo.h"
#include "CircuitInfo.h"
#include "Rotary.h"

// --------------------------------------------------------------------------- //
device_Info *tmp_DevInfo_P;
// --------------------------------------------------------------------------- //

uint8 dev_Count;
uint16 ctrl_Cmd;
uint16 device_Type;
uint32 z_ieee_Id;

uint8 ncp_NetState = NCP_NET_NOT_CONNECT;

char token = 0;
// --------------------------------------------------------------------------- //

uint8 tmp_Buff[64];

void plcs_GCP_ProcessMessage(uint8 srcNodeType, uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint8 msgType;

	msgType = msg[8];

	switch (msgType) { // Msg Type of NCP
	case PLCS_GCP_REQ_NUMOFDEVICE:
		plcs_GCP_HandleNumOfDeviceReq(srcNodeType, seqNum, srcId, destId, msg,
				length);
		break;
	case PLCS_GCP_REQ_DEVICEINFO:
		plcs_GCP_HandleDeviceInfoReq(srcNodeType, seqNum, srcId, destId, msg,
				length);
		break;
	case PLCS_GCP_REQ_STATEINFO:
		plcs_GCP_HandleStateInfoReq(srcNodeType, seqNum, srcId, destId, msg,
				length);
		break;
	case PLCS_GCP_REQ_CONTROL_REBOOT:
		plcs_GCP_HandleControlRebootReq(srcNodeType, seqNum, srcId, destId, msg,
				length);
		break;
	case PLCS_GCP_REQ_CONTROL_CIRCUIT:
		plcs_GCP_HandleControlCircuitReq(srcNodeType, seqNum, srcId, destId,
				msg, length);
		break;
	case PLCS_GCP_REQ_SENSORMAPPINGSIZE:
		plcs_GCP_HandleSensorMappingSizeReq(srcNodeType, seqNum, srcId, destId,
				msg, length);
		break;
	case PLCS_GCP_REQ_DIMMERMAPPINGSIZE:
		plcs_GCP_HandleDimmerMappingSizeReq(srcNodeType, seqNum, srcId, destId,
				msg, length);
		break;
	case PLCS_GCP_REQ_SENSORMAPPINGINFO:
		plcs_GCP_HandleSensorMappingInfoReq(srcNodeType, seqNum, srcId, destId,
				msg, length);
		break;
	case PLCS_GCP_REQ_DIMMERMAPPINGINFO:
		plcs_GCP_HandleDimmerMappingInfoReq(srcNodeType, seqNum, srcId, destId,
				msg, length);
		break;
	case PLCS_GCP_REQ_SCHEDULESIZE:
	case PLCS_GCP_REQ_CONTROL_SENSOR:
	case PLCS_GCP_REQ_POWERMETER:
	case PLCS_GCP_REQ_UPDATEPOWERMETER_LC:
	case PLCS_GCP_REQ_UPDATEPOWERMETER:
	case PLCS_GCP_REQ_SCHEDULEINFO:
	case PLCS_GCP_REQ_MAPPINGSENSOR:
	case PLCS_GCP_REQ_MAPPINGSENSORLIST:
	case PLCS_GCP_REQ_RESETMAPPINGSENSOR:
	case PLCS_GCP_REQ_MAPPINGDIMMER:
	case PLCS_GCP_REQ_MAPPINGDIMMERLIST:
	case PLCS_GCP_REQ_RESETMAPPINGDIMMER:
	case PLCS_GCP_REQ_ADDSCHEDULE:
	case PLCS_GCP_REQ_ADDSCHEDULELIST:
	case PLCS_GCP_REQ_RESETSCHEDULE:
	case PLCS_GCP_NOTICEEVENT_NEIGHBOR:
	case PLCS_GCP_REQ_DEBUG_LOG_LC:
	case PLCS_GCP_REQ_RESET_DEBUG_LOG_LC:
		break;
	case PLCS_GCP_REQ_SETSENSINGLEVEL:
		sendMessage(ZIGBEE, msg[11] << 8 | msg[12], msg, length);
		break;
	default:
		break;
	}

	//Response Message
	switch (msgType) { // Msg Type of NCP
	case PLCS_GCP_NOTICEEVENT:
		msg[9] = zrmpInfo.zrm_Id >> 8;
		msg[10] = zrmpInfo.zrm_Id;
		if (msg[11] == 0x01) {
			plcs_GCP_HandleSensorEvent(0, seqNum, srcId, destId, msg, length);
		}
		{
			uint8 i = 0;
			for (i = 0; i < deviceInfoTable.size; i++)
				if (deviceInfoTable.deviceInfo[i].deviceType == 0x2090
						&& deviceInfoTable.deviceInfo[i].cnt < 200)
					sendMessage(ZIGBEE, deviceInfoTable.deviceInfo[i].ieeeId,
							msg, length);
		}
		break;
	case PLCS_GCP_RES_STATEINFO:
	case PLCS_GCP_RES_SETSENSINGLEVEL:
	case PLCS_GCP_RES_POWERMETER:
		msg[9] = zrmpInfo.zrm_Id >> 8;
		msg[10] = zrmpInfo.zrm_Id;
		{
			uint8 i = 0;
			for (i = 0; i < deviceInfoTable.size; i++)
				if (deviceInfoTable.deviceInfo[i].deviceType == 0x2090
						&& deviceInfoTable.deviceInfo[i].cnt < 200)
					sendToZigbee(ZIGBEE_0,
							deviceInfoTable.deviceInfo[i].nwkAddr, msg, length);
		}
		break;
	}
}

void NCP_ProcessMessage(uint8 srcNodeType, uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint8 msgType;
	msgType = msg[8];

	//Request Message
	switch (msgType) { // Msg Type of NCP
	case NCP_REQ_PING:
		ncp_ProcessPingReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	case NCP_REQ_REGISTER:
		ncp_ProcessRegisterReq(srcNodeType, seqNum, srcId, destId, msg, length);
		break;
	}
}

void ncp_ProcessRegisterReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint16 deviceType;
	uint8 deviceVersion;
	uint8 fwVersion;
	// Request Register

	//msg[8] == messageType
	deviceType = (uint16) ((msg[9] << 8) | (msg[10]));
	deviceVersion = (uint8) msg[11];
	fwVersion = (uint8) msg[12];

	updateDeviceTable(srcId, deviceType, deviceVersion, fwVersion);
	ncp_SendRegisterRes(srcNodeType, srcId, seqNum, srcId, deviceType, 0x0000);
}

void ncp_ProcessPingReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {

	device_Info *pDeviceInfo;
	//msg[8] = messageType
	pDeviceInfo = findNodeById(srcId);

	if (pDeviceInfo != NULL) {
		if (pDeviceInfo->deviceType == 0)
			ncp_SendRegisterRes(srcNodeType, srcId, 0, srcId, device_Type, 1);
		else
			ncp_SendPingRes(srcNodeType, srcId, seqNum, srcId, device_Type,
					0x00, 30);

	} else {
		ncp_SendRegisterRes(srcNodeType, srcId, 0, srcId, device_Type, 1);
	}
}

void npc_ProcessSendTimeInfo(uint8 srcNodeType, uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint8 hour;
	uint8 min;
	hour = msg[9];
	min = msg[10];
}

/////////////////////////////////////////////////
void ncp_SendPingRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum,
		uint16 nodeId, uint16 device_Type, uint8 flag, uint8 timeOut) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_RES_PING;
	payload[len++] = FW_VERSION;
	payload[len++] = (uint8) (flag);
	payload[len++] = (uint8) (timeOut >> 8);
	payload[len++] = (uint8) (timeOut);

	resultLen = plcs_GetNCPResMessage(tmp_Buff, nodeId, zrmpInfo.zrm_Id,
			payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void ncp_SendRegisterRes(uint8 dstNodeType, uint16 dstNodeId, uint16 seqNum,
		uint16 node_Id, uint16 deviceType, uint16 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_RES_REGISTER;
	payload[len++] = (uint8) (result >> 8);
	payload[len++] = (uint8) (result);
	payload[len++] = 0x00;
	payload[len++] = 0x1e;

	resultLen = plcs_GetNCPResMessage(tmp_Buff, dstNodeId, zrmpInfo.zrm_Id,
			payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void npc_SendRegisterNodeReq(uint8 dstNodeType, uint16 dstNodeId,
		uint16 node_Id, uint16 device_Type) {
	int len = 0;
	uint8 payload[20];
	uint8 resultLen = 0;

	payload[len++] = NCP_REQ_REGISTER_NODE;
	payload[len++] = zrmpInfo.zrm_Id >> 8;
	payload[len++] = zrmpInfo.zrm_Id;
	payload[len++] = (uint8) (device_Type >> 8);
	payload[len++] = (uint8) (device_Type);

	switch (device_Type) {
	case PLCS_LC_100Z_TYPE:
		payload[len++] = (uint8) (PLCS_LC_FW_VER);
		payload[len++] = (uint8) (PLCS_LC_FW_VER);
		break;
	case PLCS_ZDIMMER_TYPE:
	case PLCS_ZSENSOR_TYPE:
	case PLCS_ZMDF_TYPE:
	case PLCS_SSG_TYPE:
		tmp_DevInfo_P = findNodeById(node_Id);
		if (tmp_DevInfo_P == NULL)
			return;
		if (tmp_DevInfo_P->deviceType != device_Type)
			return;
		payload[len++] = tmp_DevInfo_P->ieeeId >> 8;
		payload[len++] = tmp_DevInfo_P->ieeeId;
		payload[len++] = tmp_DevInfo_P->deviceVersion;
		payload[len++] = tmp_DevInfo_P->fwVersion;
		payload[len++] = tmp_DevInfo_P->ieeeId >> 8;
		payload[len++] = tmp_DevInfo_P->ieeeId;

		break;
	default:
		return;
	}

	resultLen = plcs_GetNCPMessage(tmp_Buff, 0, zrmpInfo.zrm_Id, payload, len);
	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);

}

void registerAllNodetoServer() {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		npc_SendRegisterNodeReq(0, deviceInfoTable.deviceInfo[i].ieeeId,
				deviceInfoTable.deviceInfo[i].deviceType,
				deviceInfoTable.deviceInfo[i].ieeeId);

}
uint8 ncp_ConnState() {
	return ncp_NetState;
}

char hasToken() {
	if (token == 0)
		return 0;

	return 1;
}

void plcs_NCP_ProcessRegisterBroadcastReq(uint8 srcNodeType, uint16 seqNum,
		uint16 srcId, uint16 destId, uint8 msg[], int length) {
	char numOfNode;
	char flag;
	uint16 nodeId;
	char isRegistered = 0;
	char i;

	//msg[9] == msgType
	//msg[9, 10] = nodeId

	flag = msg[11];
	numOfNode = msg[12];
	for (i = 0; i < numOfNode; i++) {
		nodeId = msg[13 + i * 2] << 8 | msg[14 + i * 2];
		if (nodeId == zrmpInfo.zrm_Id)
			isRegistered = 1;
	}

	if (isRegistered == 0) {
		switch (flag) {
		case 0:
			MSLEEP(zrmpInfo.zrm_Id % 17 * 70);
			break;
		case 1:
			MSLEEP(zrmpInfo.zrm_Id % 13 * 70);
			break;
		case 2:
			MSLEEP(zrmpInfo.zrm_Id % 11 * 70);
			break;
		case 3:
			MSLEEP(zrmpInfo.zrm_Id % 7 * 70);
			break;
		case 4:
			MSLEEP(zrmpInfo.zrm_Id % 5 * 70);
			break;
		case 5:
			MSLEEP(zrmpInfo.zrm_Id % 3 * 70);
			break;
		}
	}
}

void plcs_NCP_ProcessAssignToken(uint8 srcNodeType, uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	token = 1;
}

void plcs_GCP_HandleNumOfDeviceReq(uint8 srcNodeType, uint16 seqNum,
		uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 size = 0;
	uint8 i = 0;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];

	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].deviceType == PLCS_ZSENSOR_TYPE)
			size++;

	plcs_GCP_SendNumOfDeviceRes(srcNodeType, srcId, seqNum, nodeId, size);
}

void plcs_GCP_HandleDeviceInfoReq(uint8 srcNodeType, uint16 seqNum,
		uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 index;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	index = msg[11];

	plcs_GCP_SendDeviceInfoRes(srcNodeType, srcId, seqNum, nodeId, index);
}

void plcs_GCP_HandleStateInfoReq(uint8 srcNodeType, uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 deviceId;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	deviceId = msg[11] << 8 | msg[12];

	if (deviceId == 0)
		plcs_GCP_SendStateInfoRes(srcNodeType, srcId, seqNum, nodeId, deviceId);
	else { //deviceId!=0 서브 노드에게 전달.
		sendMessage(ZIGBEE, deviceId, msg, length);
	}
}

void plcs_GCP_HandleControlRebootReq(uint8 srcNodeType, uint16 seqNum,
		uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 deviceId;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	deviceId = msg[11] << 8 | msg[12];

	if (deviceId == 0) {
		while (1)
			;
	} else { //deviceId!=0 서브 노드에게 전달.
		sendMessage(ZIGBEE, deviceId, msg, length);
	}
	//deviceId!=0 서브 노드에게 전달.

}

void plcs_GCP_HandleControlCircuitReq(uint8 srcNodeType, uint16 seqNum,
		uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	int circuitId;
	uint8 ctrlMode;

	//msg[8] = message type
	nodeId = msg[9] << 8 | msg[10];
	circuitId = msg[11];
	ctrlMode = msg[12];

	handleCircuitControl(nodeId, circuitId, ctrlMode);

	if (circuitId < NumOfChannels)
		plcs_GCP_SendControlCircuitRes(srcNodeType, srcId, seqNum, nodeId,
				0x00);
	else
		plcs_GCP_SendControlCircuitRes(srcNodeType, srcId, seqNum, nodeId,
				0x10);
}

void plcs_GCP_HandleSensorMappingSizeReq(uint8 srcNodeType, uint16 seqNum,
		uint16 srcId, uint16 destId, uint8 msg[], int length) {

	uint8 size = 0;
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++) {
		if (deviceInfoTable.deviceInfo[i].deviceType == PLCS_ZSENSOR_TYPE)
			size++;
	}

	plcs_GCP_SendSensorMappingSizeRes(srcNodeType, srcId, seqNum,
			zrmpInfo.zrm_Id, size * NumOfChannels);
}

void plcs_GCP_HandleDimmerMappingSizeReq(uint8 srcNodeType, uint16 seqNum,
		uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];

	uint8 size = 0;
	size = NumOfChannels;

	plcs_GCP_SendDimmerMappingSizeRes(srcNodeType, srcId, seqNum,
			zrmpInfo.zrm_Id, size);
}

void plcs_GCP_HandleSensorMappingInfoReq(uint8 srcNodeType, uint16 seqNum,
		uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 index;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	index = msg[11];

	plcs_GCP_SendSensorMappingInfoRes(srcNodeType, srcId, seqNum, nodeId,
			index);
}

void plcs_GCP_HandleDimmerMappingInfoReq(uint8 srcNodeType, uint16 seqNum,
		uint16 srcId, uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint8 index;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	index = msg[11];

	plcs_GCP_SendDimmerMappingInfoRes(srcNodeType, srcId, seqNum, nodeId,
			index);
}

void plcs_GCP_HandleSensorEvent(uint8 srcNodeType, uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
	uint16 nodeId;
	uint16 sensorId;

	//msg[8]=messagetype
	nodeId = msg[9] << 8 | msg[10];
	sensorId = msg[12] << 8 | msg[13];

	handleSensorEvent(nodeId, sensorId);
}

void handleSensorEvent(uint16 lcId, uint16 sensorId) {
	uint16 onTime = 0;
	uint8 i = 0;

	for (i = 0; i < NumOfChannels; i++)
		tmp_CircuitInfo.timerCnt[i] = 0;

	if ((rotary_GetValue() & 0xf0) >= 0x10
			&& (rotary_GetValue() & 0xf0) <= 0xA0)
		onTime = ((rotary_GetValue() & 0xf0) >> 4) * 10;
	else if ((rotary_GetValue() & 0xf0) >= 0xB0
			&& (rotary_GetValue() & 0xf0) <= 0xE0)
		onTime = (((rotary_GetValue() & 0xf0) >> 4) - 10 + 1) * 60;

	tmp_CircuitInfo.expirationTime[0] = onTime;
	tmp_CircuitInfo.offCtrlMode[0] = OFF;
	tmp_CircuitInfo.onCtrlMode[0] = DIMMING_10;

	tmp_CircuitInfo.expirationTime[1] = onTime;
	tmp_CircuitInfo.offCtrlMode[1] = DIMMING_1;
	tmp_CircuitInfo.onCtrlMode[1] = DIMMING_10;

	tmp_CircuitInfo.expirationTime[2] = onTime;
	tmp_CircuitInfo.offCtrlMode[2] = DIMMING_3;
	tmp_CircuitInfo.onCtrlMode[2] = DIMMING_8;

	tmp_CircuitInfo.expirationTime[3] = onTime;
	tmp_CircuitInfo.offCtrlMode[3] = OFF;
	tmp_CircuitInfo.onCtrlMode[3] = DIMMING_5;
}

void handleSwitchEvent(uint16 srcNetAddr, uint16 seqNum, uint16 srcId,
		uint16 destId, uint8 msg[], int length) {
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
/////////////////////////////////////////////////////////////////////////////////////////////////////////
uint16 seqNumGenerator = 0;
uint16 getSeqNumGenerator() {
	return seqNumGenerator++;
}

uint8 plcs_GetNCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGenerator();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetNCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetGCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGenerator();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_GCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetGCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_GCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

/////////send gcp message
void plcs_GCP_SendNumOfDeviceRes(uint8 dstNodeType, uint16 dstNodeId,
		uint16 seqNum, uint16 nodeId, uint8 size) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_NUMOFDEVICE;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (size);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo.zrm_Id, payload,
			len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendDeviceInfoRes(uint8 dstNodeType, uint16 dstNodeId,
		uint16 seqNum, uint16 nodeId, uint8 index) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;
	uint16 deviceId;
	uint16 deviceType;
	uint8 firmVersion;

	if (deviceInfoTable.size < index && index != 200)
		return;
	else if (index == 200) {
		nodeId = 0;
		deviceId = nodeId;
		deviceType = PLCS_LC_100Z_TYPE;
		firmVersion = (uint8) PLCS_LC_FW_VER;
	} else {
		device_Info* deviceInfo_P = NULL;
		uint8 i = 0;
		uint8 cnt = 0;
		for (i = 0; i < deviceInfoTable.size; i++) {
			if (deviceInfoTable.deviceInfo[i].deviceType == PLCS_ZSENSOR_TYPE)
				cnt++;

			deviceInfo_P = &(deviceInfoTable.deviceInfo[i]);
			if ((cnt - 1) == index)
				break;

		}
		nodeId = zrmpInfo.zrm_Id;
		deviceId = deviceInfo_P->ieeeId;
		deviceType = deviceInfo_P->deviceType;
		firmVersion = deviceInfo_P->fwVersion;
	}

	if (deviceType == 0)
		return;

	payload[len++] = PLCS_GCP_RES_DEVICEINFO;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (deviceId >> 8);
	payload[len++] = (uint8) deviceId;
	payload[len++] = (uint8) (deviceType >> 8);
	payload[len++] = (uint8) deviceType;
	payload[len++] = (uint8) firmVersion;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo.zrm_Id, payload,
			len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendStateInfoRes(uint8 dstNodeType, uint16 dstNodeId,
		uint16 seqNum, uint16 nodeId, uint16 deviceId) {
	uint8 len = 0;
	uint8 payload[60];
	uint8 resultLen = 0;
	uint16 deviceType = PLCS_LC_100Z_TYPE;
	uint8 i;

	payload[len++] = PLCS_GCP_RES_STATEINFO;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (deviceId >> 8);
	payload[len++] = (uint8) deviceId;
	payload[len++] = (uint8) (deviceType >> 8);
	payload[len++] = (uint8) deviceType;

	payload[len++] = tmp_CircuitInfo.state[0];
	payload[len++] = tmp_CircuitInfo.state[1];
	payload[len++] = tmp_CircuitInfo.state[2];
	payload[len++] = tmp_CircuitInfo.state[3];
	for (i = 4; i < 16; i++)
		payload[len++] = 0x00;
	payload[len++] = tmp_CircuitInfo.ctrlMode[0];
	payload[len++] = tmp_CircuitInfo.ctrlMode[1];
	payload[len++] = tmp_CircuitInfo.ctrlMode[2];
	payload[len++] = tmp_CircuitInfo.ctrlMode[3];
	for (i = 4; i < 16; i++)
		payload[len++] = 0x00;

	payload[len++] = (uint8) (zrmpInfo.zrm_Panid >> 8);
	payload[len++] = (uint8) zrmpInfo.zrm_Panid;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, deviceId, zrmpInfo.zrm_Id,
			payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendControlCircuitRes(uint8 dstNodeType, uint16 dstNodeId,
		uint16 seqNum, uint16 nodeId, uint8 result) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_CONTROL_CIRCUIT;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) result;

	resultLen = plcs_GetGCPResMessage(tmp_Buff, dstNodeId, zrmpInfo.zrm_Id,
			payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendSensorMappingSizeRes(uint8 dstNodeType, uint16 dstNodeId,
		uint16 seqNum, uint16 nodeId, uint8 size) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_SENSORMAPPINGSIZE;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (size);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, dstNodeId, zrmpInfo.zrm_Id,
			payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendDimmerMappingSizeRes(uint8 dstNodeType, uint16 dstNodeId,
		uint16 seqNum, uint16 nodeId, uint8 size) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_RES_DIMMERMAPPINGSIZE;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (size);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, dstNodeId, zrmpInfo.zrm_Id,
			payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendSensorMappingInfoRes(uint8 dstNodeType, uint16 dstNodeId,
		uint16 seqNum, uint16 nodeId, uint8 index) {
	int len = 0;
	uint8 payload[20];
	uint8 resultLen = 0;
	uint8 size = 0;
	uint8 i = 0;
	uint16 sensorId = 0;
	uint8 circuitId;
	uint8 onTime;
	uint8 onCtrlMode;
	uint8 offCtrlMode;

	//index/4 = indexofSensor in deviceInfo

	for (i = 0; i < deviceInfoTable.size; i++) {
		if (deviceInfoTable.deviceInfo[i].deviceType == PLCS_ZSENSOR_TYPE) {
			if (index / NumOfChannels == size)
				sensorId = deviceInfoTable.deviceInfo[i].ieeeId;
			size++;
		}
	}

	circuitId = index % 4;

	if ((rotary_GetValue() & 0xf0) >= 0x10
			&& (rotary_GetValue() & 0xf0) <= 0xA0)
		onTime = ((rotary_GetValue() & 0xf0) >> 4) * 10;
	else if ((rotary_GetValue() & 0xf0) >= 0xB0
			&& (rotary_GetValue() & 0xf0) <= 0xE0)
		onTime = (((rotary_GetValue() & 0xf0) >> 4) - 10 + 1) * 60;

	onCtrlMode = tmp_CircuitInfo.onCtrlMode[circuitId];
	offCtrlMode = tmp_CircuitInfo.offCtrlMode[circuitId];

	if (index >= size * NumOfChannels)
		return;

	payload[len++] = PLCS_GCP_RES_SENSORMAPPINGINFO;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (zrmpInfo.zrm_Id >> 8);
	payload[len++] = (uint8) (zrmpInfo.zrm_Id);
	payload[len++] = (uint8) (sensorId >> 8);
	payload[len++] = (uint8) (sensorId);
	payload[len++] = (uint8) (circuitId);
	payload[len++] = (uint8) (onTime);
	payload[len++] = (uint8) (onCtrlMode);
	payload[len++] = (uint8) (offCtrlMode);
	resultLen = plcs_GetGCPResMessage(tmp_Buff, dstNodeId, zrmpInfo.zrm_Id,
			payload, len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendDimmerMappingInfoRes(uint8 dstNodeType, uint16 dstNodeId,
		uint16 seqNum, uint16 nodeId, uint8 index) {
	int len = 0;
	uint8 payload[20];
	uint8 resultLen = 0;

	if (index >= NumOfChannels)
		return;

	payload[len++] = PLCS_GCP_RES_DIMMERMAPPINGINFO;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (zrmpInfo.zrm_Id >> 8);
	payload[len++] = (uint8) (zrmpInfo.zrm_Id);
	payload[len++] = (uint8) (index);
	payload[len++] = (uint8) (index);

	resultLen = plcs_GetGCPResMessage(tmp_Buff, 0, zrmpInfo.zrm_Id, payload,
			len, seqNum);

	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}

void plcs_GCP_SendNoticeEvent(uint8 dstNodeType, uint16 dstNodeId,
		uint16 nodeId, uint8 dataFormat, uint16 sensorId) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = PLCS_GCP_NOTICEEVENT;
	payload[len++] = (uint8) (nodeId >> 8);
	payload[len++] = (uint8) nodeId;
	payload[len++] = (uint8) (dataFormat);
	payload[len++] = (uint8) (sensorId >> 8);
	payload[len++] = (uint8) sensorId;

	resultLen = plcs_GetGCPMessage(tmp_Buff, 0, zrmpInfo.zrm_Id, payload, len);
	sendMessage(dstNodeType, dstNodeId, tmp_Buff, resultLen);
}
