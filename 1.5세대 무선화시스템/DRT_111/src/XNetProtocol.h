#ifndef __XNET_PROTOCOL_H__
#define __XNET_PROTOCOL_H__


#include <string.h>
#include "Util.h"
#include "NCProtocol.h"
#include "plcs_protocol.h"
#include "Xcps.h"
#include "ZRMProtocol.h"

#define RS485		0
#define ZIGBEE_0	1
#define ZIGBEE_1	2
#define ZIGBEE		3

// --------------- XNetMessage Handler --------------- //
void XNetHandler(uint16 zigbee_Id, uint8 buff[], int buff_length);

void XNetCommandFromRS485(uint8 msg[], int buff_length);
void XNetHandlerFromZigbee(uint16 srcNetAddr, uint8 msg[], int buff_length);

void sendServerMessage(uint8 *msg, uint8 buff_length);

char sendStackedServerMessage();
void sendMessage(uint8 dstNodeType, uint16 dstId, uint8 msg[], uint8 length);


#endif

