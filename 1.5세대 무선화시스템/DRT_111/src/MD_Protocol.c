#include "MD_Protocol.h"
#include "NCProtocol.h"
#include "luminature.h"
#include "CircuitInfo.h"
#include "NRF24L01/NRF24L01.h"
uint8 tmpBuffForMDP[64];

void plcs_MDP_ProcessMessageFromZigbee(uint16 srcNetAddr, uint8 msg[],
		int length) {
	switch (msg[8]) { // Msg Type of NCP
	case PLCS_DP_REQ_DIMMING:
		MDP_HandleDimmingReq(msg, length);
		break;
	}
}

uint8 lastSendDimmingLevel[4] = { 0xff, 0xff, 0xff, 0xff };

void MDP_HandleDimmingReq(uint8 msg[], int length) {
	uint8 channelId = 0;
	uint8 dimmingLevel = 0;

	channelId = msg[11];
	dimmingLevel = msg[12];

	if (channelId < 4) {
		lastSendDimmingLevel[channelId] = dimmingLevel;
//		MDP_SendDimmingReqToMDP(channelId, dimmingLevel);
		luminature_ControlLuminatureDimming(dimmingLevel);
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
/////////////////////////////////////////////////////////////////////////////////////////////////////////

void MDP_SendDimmingReqToMDP(uint8 channel, uint8 dimmingLevel0,
		uint8 dimmingLevel1, uint8 dimmingLevel2, uint8 dimmingLevel3) {
	int len = 0;
	uint8 payload[20];
	uint8 resultLen;

	len = 0;
	payload[len++] = PLCS_DP_REQ_DIMMING;
	payload[len++] = zrmpInfo.zrm_Id >> 8;
	payload[len++] = zrmpInfo.zrm_Id;
	payload[len++] = 0x00;
	payload[len++] = MDP_DimmingConvert(dimmingLevel0);
	payload[len++] = 0x01;
	payload[len++] = MDP_DimmingConvert(dimmingLevel1);
	payload[len++] = 0x02;
	payload[len++] = MDP_DimmingConvert(dimmingLevel2);
	payload[len++] = 0x03;
	payload[len++] = MDP_DimmingConvert(dimmingLevel3);
	payload[len++] = rotary__GetValue();
	resultLen = plcs_GetMDPMessage(tmpBuffForMDP, 0, 0, payload, len);

	xcps_send_NRF24L01(tmpBuffForMDP, resultLen);
	_delay_ms(1);
	xcps_send_rs485(tmpBuffForMDP, resultLen);
}
//void MDP_SendDimmingReqToMDP(uint8 channel, uint8 dimmingLevel) {
//	int len = 0;
//	uint8 payload[15];
//	uint8 resultLen;
//
//	len = 0;
//	payload[len++] = PLCS_DP_REQ_DIMMING;
//	payload[len++] = zrmpInfo.zrm_Id >> 8;
//	payload[len++] = zrmpInfo.zrm_Id;
//	payload[len++] = 0x00;
//	payload[len++] = MDP_DimmingConvert(lastSendDimmingLevel[0]);
//	payload[len++] = 0x01;
//	payload[len++] = MDP_DimmingConvert(lastSendDimmingLevel[1]);
//	payload[len++] = 0x02;
//	payload[len++] = MDP_DimmingConvert(lastSendDimmingLevel[2]);
//	payload[len++] = 0x03;
//	payload[len++] = MDP_DimmingConvert(lastSendDimmingLevel[3]);
//	payload[len++] = rotary__GetValue();
//
//	resultLen = plcs_GetMDPMessage(tmpBuffForMDP, 0, 0, payload, len);
//
////	xcps_send_NRF24L01(tmpBuffForMDP, resultLen);
////	_delay_ms(1);
//	xcps_send_rs485(tmpBuffForMDP, resultLen);
//}
void MDP_SendSetWatchdogReqToMDP(uint8 isEnable) {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen;

	len = 0;
	payload[len++] = PLCS_DP_REQ_WATCHDOG;
	payload[len++] = zrmpInfo.zrm_Id >> 8;
	payload[len++] = zrmpInfo.zrm_Id;
	payload[len++] = isEnable;

	resultLen = plcs_GetMDPMessage(tmpBuffForMDP, 0, 0, payload, len);

	xcps_send_rs485(tmpBuffForMDP, resultLen);
}

////////////////////////////////////

uint16 seqNumGeneratorForMDP = 0;
uint16 getSeqNumGeneratorForMDP() {
	return seqNumGeneratorForMDP++;
}

uint8 plcs_GetMDPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getSeqNumGeneratorForMDP();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_DP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 plcs_GetMDPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId,
		uint8 payload[], uint8 len, uint16 seqNum) {
	uint8 resultLen = 0;

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_DP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

uint8 MDP_GetLastSendDimmingLevel(uint8 channelNum) {
	return lastSendDimmingLevel[channelNum];
}

uint8 MDP_DimmingConvert(uint8 DimLevel) {
	switch (DimLevel) {
	case DIMMING_0:
		return 0x00;
		break;

	case DIMMING_1:
		return 0x20;
		break;

	case DIMMING_2:
		return 0x34;
		break;

	case DIMMING_3:
		return 0x4D;
		break;

	case DIMMING_4:
		return 0x67;
		break;

	case DIMMING_5:
		return 0x80;
		break;

	case DIMMING_6:
		return 0x9A;
		break;

	case DIMMING_7:
		return 0xB3;
		break;

	case DIMMING_8:
		return 0xCD;
		break;

	case DIMMING_9:
		return 0xE6;
		break;

	case DIMMING_10:
		return 0xFE;
		break;
	case ON:
		return 0xFE;
		break;
	case OFF:
		return 0x00;
		break;

	default:
		return 0xFE;
		break;
	}
	return 0xFE;
}
