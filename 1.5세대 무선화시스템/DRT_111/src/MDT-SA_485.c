#include "MDT-SA_485.h"
#include "Rotary.h"
#include "NCProtocol.h"
#include "DeviceInfo.h"
#include "luminature.h"
#include "MD_Protocol.h"
#include "CircuitInfo.h"
#include "Debug.h"
#include "NRF24L01/NRF24L01.h"
// Avrx hw Peripheral initialization
#define CPUCLK 		16000000L     		// CPU xtal
#define TICKRATE 	1000				// AvrX timer queue Tick rate
#define TCNT0_INIT 	(CPUCLK/128/TICKRATE)

// NB: Clock divisor bits are different between classic and mega103 chips!
// NB: IAR chooses to use different CPU identifiers for their C compiler
// NB: GCC changes the IO bit names just for fun...

#define TCCR0_INIT	((1<<CS02) | (0<<CS01) | (1<<CS00))

uint8 ZigbeePacket[64];

uint8 targetDimmingLevel = 0xff;
uint8 currentDimmingLevel = 0xff;

uint8 dimmingLevel[4] = { 0xff, 0xff, 0xff, 0xff };

#define SSR_OFF	PORTC|=0x01
#define SSR_ON	PORTC &=~(0x01);

//-----------------------------------------------------------------------------
//MCU Initialize
void InitMCU() {
	PORTA = 0x00;
	DDRA = 0xFF;
	PORTC = 0x0F;
	DDRC = 0xFF;
	PORTD = 0x9F;
	DDRD = 0xFF;
	PORTF = 0xFF;
	DDRF = 0xFF;
	PORTG = 0xFF;
	DDRG = 0xFF;

	DDRE |= 0x0C;
	PORTE &= ~0x0C;
}

//----------------------------------------------------------------------//

void resetZigbee() {
	PORTD &= ~(0x80);
	MSLEEP(100);
	PORTD |= 0x80;
}

/***** Task Led(Toggle) *****/
void ledTask() {
	static uint8 mode = 0;
	if (timer_isfired(ON_WTD_TIMER_ID)) {
		wdt_reset();
		if (PIND & 0x10)
			PORTD &= ~(0x10);
		else
			PORTD |= 0x10;

		timer_clear(ON_WTD_TIMER_ID);
		timer_set(ON_WTD_TIMER_ID, 500);
	}

	if (timer_isfired(ON_TEST_TIMER_ID)) {
		if (rotary_GetValue() == 0x00) {
			if (mode == 0) {
				mode++;
				MDP_SendDimmingReqToMDP(0xff, 0x12, 0, 0, 0);
//				luminature_ControlLuminatureDimming(0x33);
			} else if (mode == 1) {
				mode++;
				MDP_SendDimmingReqToMDP(0xff, 0x14, 0, 0, 0);
//				luminature_ControlLuminatureDimming(0x4c);
			} else if (mode == 2) {
				mode++;
				MDP_SendDimmingReqToMDP(0xff, 0x16, 0, 0, 0);
//				luminature_ControlLuminatureDimming(0x7f);
			} else if (mode == 3) {
				mode++;
				MDP_SendDimmingReqToMDP(0xff, 0x18, 0, 0, 0);
//				luminature_ControlLuminatureDimming(0xcc);
			} else if (mode == 4) {
				mode = 0;
				MDP_SendDimmingReqToMDP(0xff, 0x1a, 0, 0, 0);
//				luminature_ControlLuminatureDimming(0xfe);
			} else {
				mode = 0;
			}
//			MDP_SendSetWatchdogReqToMDP(0);
		} else {
//			MDP_SendSetWatchdogReqToMDP(1);
		}
		timer_clear(ON_TEST_TIMER_ID);
		timer_set(ON_TEST_TIMER_ID, 3000);
	}
}
uint8 msgFromZigbee[90];
void ZigbeeUsartTask() {
	static int Recvlen = 0;
	static uint8 zigbeeHWResetCount = 0;

	if (timer_isfired(ON_ZIGBEE_PING_TIMER_ID)) {
		ZRMsendPing(ZIGBEE_0);
		zigbeeHWResetCount++;

		timer_clear(ON_ZIGBEE_PING_TIMER_ID);
		timer_set(ON_ZIGBEE_PING_TIMER_ID, 10000);
	}

	if (getZigbeeState(ZIGBEE_0) > 1) {
		zigbeeHWResetCount = 0;
	} else if (zigbeeHWResetCount > 5) {
		resetZigbee();
		zigbeeHWResetCount = 0;
	}

	if ((Recvlen = xcps_recv_zigbee(ZigbeePacket, 64)) > 0) {
		uint8 buff_len = 0;
		uint16 Src_Addr;
		uint16 Dst_Addr;

		switch (ZigbeePacket[0]) {
		case 0x00: // Send to GMProtocols (Gateway <-> RFM)
			buff_len = (Recvlen - 1);
			ZRMPMessage(ZIGBEE_0, &ZigbeePacket[1], buff_len);
			break;
		case 0x10:
			Dst_Addr = (uint16) (ZigbeePacket[1] << 8) | (ZigbeePacket[2]);
			Src_Addr = (uint16) (ZigbeePacket[3] << 8) | (ZigbeePacket[4]);

			buff_len = ZRM_getOriginalCode(msgFromZigbee, &(ZigbeePacket[5]), Recvlen - 5);

			XNetHandlerFromZigbee(Src_Addr, msgFromZigbee, buff_len);
			break;

		default: // Error Mesaage
			break;
		}
	}
}

void CircuitControlTask() {
	static uint8 i = 0;
	if (timer_isfired(CONTROL_CIRCUIT_TIMER_ID)) {
		timer_clear(CONTROL_CIRCUIT_TIMER_ID);
		timer_set(CONTROL_CIRCUIT_TIMER_ID, 100);

		for (i = 0; i < NumOfChannels; i++) {
			if (tmp_CircuitInfo.ctrlMode[i] == SENSOR) {
				if (tmp_CircuitInfo.expirationTime[i] == 0x00)
					tmp_CircuitInfo.state[i] = tmp_CircuitInfo.offCtrlMode[i];
				else if (tmp_CircuitInfo.expirationTime[i] == 0xffff) {
					tmp_CircuitInfo.state[i] = tmp_CircuitInfo.onCtrlMode[i];
				} else {
					tmp_CircuitInfo.timerCnt[i] = tmp_CircuitInfo.timerCnt[i] + 1;
					if (tmp_CircuitInfo.expirationTime[i] * 10 <= tmp_CircuitInfo.timerCnt[i]) {
						tmp_CircuitInfo.state[i] = tmp_CircuitInfo.offCtrlMode[i];
						tmp_CircuitInfo.timerCnt[i] = 0;
						tmp_CircuitInfo.expirationTime[i] = 0;
					} else {
						tmp_CircuitInfo.state[i] = tmp_CircuitInfo.onCtrlMode[i];
					}
				}
			} else if (tmp_CircuitInfo.ctrlMode[i] == SCHEDULE || tmp_CircuitInfo.ctrlMode[i] >= ILLU_DIMMING_0) {
				tmp_CircuitInfo.state[i] = ON;
			} else
				tmp_CircuitInfo.state[i] = tmp_CircuitInfo.ctrlMode[i];
		}

		//state에 따라 회로 제어.
		for (i = 0; i < 1; i++) {
			if (tmp_CircuitInfo.state[i] == OFF) {
				MDP_SendDimmingReqToMDP(i, 0, 0, 0, 0);
				if (i == 0) {
//					luminature_ControlLuminatureDimming(0);
					SSR_OFF;
				}
			} else if (tmp_CircuitInfo.state[i] == ON) {
				MDP_SendDimmingReqToMDP(i, ON, 0, 0, 0);
				if (i == 0) {
//					luminature_ControlLuminatureDimming(MDP_DimmingConvert(ON));
					SSR_ON;
				}
			} else {

				MDP_SendDimmingReqToMDP(i, tmp_CircuitInfo.state[0], tmp_CircuitInfo.state[1], tmp_CircuitInfo.state[2], tmp_CircuitInfo.state[3]);
				if (i == 0) {
//					luminature_ControlLuminatureDimming(
//							MDP_DimmingConvert(tmp_CircuitInfo.state[0]));
					if (tmp_CircuitInfo.state[0] != DIMMING_0) {
						SSR_ON;
					} else {
						SSR_OFF;
					}
				}
			}
		}
	}
}

void WDT_INIT() {
	MCUCSR &= ~(1 << WDRF); // WatchDog Init(Low)
	wdt_enable(WDTO_2S);
	// WatchDog Reset Time(High)
}

void ZigbeeSendTask() {
	if (timer_isfired(ZIGBEE_SEND_TIMER_ID)) {
		timer_clear(ZIGBEE_SEND_TIMER_ID);
		timer_set(ZIGBEE_SEND_TIMER_ID, 50);

		sendQueueingMessage(ZIGBEE_0);
	}
}

//-----------------------------------------------------------------------------
// Main function
//-----------------------------------------------------------------------------
int main(void) {

	InitMCU();
	initRotary();
	InitUART();
	timer_init();
	luminature_Init();
	initCircuit();
	rotary__GetValue();
	nRF24L01_Initial();

	/*	while(1) {
	 MSLEEP(500);
	 resetZigbee();
	 }
	 */
	WDT_INIT();

	initDeviceTable();

	// initialize dmx driver
	//MDP_SendDimmingReqToMDP(0xff, 0xfe);

	PORTC = 0x00;

	TCCR1A = 0x00;
	TCCR1B = 0x06;
	TCNT1H = 0xFF;
	TCNT1L = 0xFF;

	TIMSK |= 0x04;

	ZRMSendSetPreconfig(ZIGBEE_0, 0x13);
	MSLEEP(100);
	ZRMSendReset(ZIGBEE_0);
	MSLEEP(1000);

	timer_set(ON_WTD_TIMER_ID, 500);
	timer_set(ON_TEST_TIMER_ID, 3000);

	timer_set(ON_ZIGBEE_PING_TIMER_ID, 3000);
	timer_set(CONTROL_CIRCUIT_TIMER_ID, 100);
	timer_set(ZIGBEE_SEND_TIMER_ID, 1000);

	zrmpInfo.zrm_State = ZRM_INIT;

	xcps_init_zigbee(USART0_Receive, USART0_Transmit);
	xcps_init_rs485(USART1_Receive, USART1_Transmit);

	TX_Mode(0);
//	FILE mystdout =
//			FDEV_SETUP_STREAM((void *)USART0_Transmit, NULL, _FDEV_SETUP_WRITE);
//	stdout = &mystdout;
//
//	DEBUG("\n\r=========START PROGRAM ===========\n\r");

	while (1) {
		ledTask();
		if (rotary_GetValue() != 0x00) {
			ZigbeeUsartTask();
			CircuitControlTask();
			ZigbeeSendTask();
		}
	}

	return 0;
}

