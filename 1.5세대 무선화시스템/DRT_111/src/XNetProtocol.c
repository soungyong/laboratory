#include "XNetProtocol.h"
#include "DeviceInfo.h"
// --------------------------------------------------------------------------- //
// --------------------------------------------------------------------------- //

void XNetHandlerFromZigbee(uint16 srcNetAddr, uint8 msg[], int buff_length) {
	uint8 pid;
	uint8 subPid;
	uint16 seqNum;
	uint16 srcId;
	uint16 destId;
	device_Info *deviceInfo_P = NULL;

	pid = msg[0];
	subPid = msg[1];
	seqNum = msg[2] << 8 | msg[3];
	destId = msg[4] << 8 | msg[5];
	srcId = msg[6] << 8 | msg[7];

	deviceInfo_P = findNodeById(srcId);
	if (deviceInfo_P != NULL)
		deviceInfo_P->nwkAddr = srcNetAddr;
	else
		addDeviceTable(srcId, 0, 0, 0, srcNetAddr);

	switch (pid) {
	case NCP_PROTOCOL_ID:
		switch (subPid) {
		case PLCS_NCP_PROTOCOL_ID:
			NCP_ProcessMessage(ZIGBEE, seqNum, srcId, destId, msg, buff_length);
			break;
		case PLCS_GCP_PROTOCOL_ID:
			plcs_GCP_ProcessMessage(ZIGBEE, seqNum, srcId, destId, msg, buff_length);
			break;
		}
		break;
	default:
		break;
	}

}

void XNetCommandFromRS485(uint8 msg[], int buff_length) {
	uint8 pid;
	uint8 subPid;
	uint16 subNodeId;
	uint16 seqNum;
	uint16 srcId;
	uint16 destId;

	pid = msg[0];
	subPid = msg[1];
	seqNum = msg[2] << 8 | msg[3];
	destId = msg[4] << 8 | msg[5];
	srcId = msg[6] << 8 | msg[7];

	switch (pid) {
	case NCP_PROTOCOL_ID:
		switch (subPid) {
		case PLCS_NCP_PROTOCOL_ID:
			if (destId == zrmpInfo.zrm_Id || destId == 0xffff)
				NCP_ProcessMessage(RS485, seqNum, srcId, destId, msg, buff_length);
			break;
		case PLCS_GCP_PROTOCOL_ID:
			//subNodeId Ȯ��
			subNodeId = msg[9] << 8 | msg[10];
			if (subNodeId == zrmpInfo.zrm_Id)
				plcs_GCP_ProcessMessage(RS485, seqNum, srcId, destId, msg, buff_length);
			break;
		}
		break;
	default:
		break;
	}
}

void sendMessage(uint8 dstNodeType, uint16 dstId, uint8 msg[], uint8 length) {
	msg[4] = dstId >> 8;
	msg[5] = dstId;
	msg[6] = zrmpInfo.zrm_Id >> 8;
	msg[7] = zrmpInfo.zrm_Id;

	if (dstNodeType == RS485) {
		xcps_send_rs485(msg, length);
	} else {
		device_Info* deviceInfo_P = NULL;

		deviceInfo_P = findNodeById(dstId);
		if (deviceInfo_P != NULL) {
			sendToZigbee(ZIGBEE_0, deviceInfo_P->nwkAddr, msg, length);
		}
	}
}

//end of file

