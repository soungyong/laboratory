################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/CircuitInfo.c \
../src/DeviceInfo.c \
../src/MDT-SA_485.c \
../src/MD_Protocol.c \
../src/PLCS_Protocol.c \
../src/Timer.c \
../src/Uart.c \
../src/XNetProtocol.c \
../src/Xcps.c \
../src/ZRMProtocol.c \
../src/luminature.c \
../src/rotary.c 

OBJS += \
./src/CircuitInfo.o \
./src/DeviceInfo.o \
./src/MDT-SA_485.o \
./src/MD_Protocol.o \
./src/PLCS_Protocol.o \
./src/Timer.o \
./src/Uart.o \
./src/XNetProtocol.o \
./src/Xcps.o \
./src/ZRMProtocol.o \
./src/luminature.o \
./src/rotary.o 

C_DEPS += \
./src/CircuitInfo.d \
./src/DeviceInfo.d \
./src/MDT-SA_485.d \
./src/MD_Protocol.d \
./src/PLCS_Protocol.d \
./src/Timer.d \
./src/Uart.d \
./src/XNetProtocol.d \
./src/Xcps.d \
./src/ZRMProtocol.d \
./src/luminature.d \
./src/rotary.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega128 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


