#define ADC_VREF_TYPE 0x00
#include "util.h"
#include <avr/io.h>
#include "adc.h"

void initADC() {
//	ADMUX = 0x00; //select adc input 0
//	ACSR = 0x80;
//	ADCSRA = 0xA5;
//	ADCSRB &= 0xf8;
//	DIDR0 = 0x01;
	ADMUX = 0x00; //select adc input 0
	ACSR = 0x80;
	ADCSRA = 0xA5;
	ADCSRB &= 0xf8;
	DIDR0 = 0xC1;

}

