#include "NC_Protocol.h"
#include "xcps.h"

uint8 ncp_NetState = NCP_NET_NOT_CONNECT;
// --------------------------------------------------------------------------- //

uint8 tmp_Buff2[64];
uint8 pingCount = 0;

////////////

void NCP_ProcessMessageFromZigbee(uint16 srcId, uint8 msg[], int length) {
	uint8 msgType;
	msgType = msg[8];

	switch (msgType) { // Msg Type of NCP
	case NCP_RES_PING:
		ncp_ProcessPingResFromZigbee(srcId, msg, length);
		break;
	case NCP_RES_REGISTER:
		ncp_ProcessRegisterResFromZigbee(srcId, msg, length);
		break;
	}
}

void ncp_ProcessRegisterResFromZigbee(uint16 srcId, uint8 msg[], int length) {
	int i = 0;
	uint16 nodeId;
	uint16 seqNum;
	uint16 result = 0;
	// Request Register

	//msg[8] == messageType
	seqNum = msg[2] << 8 | msg[3];
	nodeId = (uint16) ((msg[6] << 8) | (msg[7]));
	result = (uint16) ((msg[9] << 8) | (msg[10]));

	if (result == 0)
		ncp_NetState = NCP_NET_REGISTER;
	else
		ncp_NetState = NCP_NET_NOT_CONNECT;
}

void ncp_ProcessPingResFromZigbee(uint16 srcId, uint8 msg[], int length) {
	uint16 seqNum;
	uint8 flag = 0;

	//msg[8] = messageType
	seqNum = msg[2] << 8 | msg[3];
	flag = msg[10];

	if (flag == 0x01) {
		ncp_NetState = NCP_NET_NOT_CONNECT;
		pingCount = 0;
	} else {
		pingCount = 0;
	}
}

void ncp_SendPingReq() {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	pingCount++;

	payload[len++] = NCP_REQ_PING;
	payload[len++] = 0;
	payload[len++] = 0;

	resultLen = plcs_GetNCPMessage(tmp_Buff2, 0, tmp_zrmp.zrm_Id, payload, len);

	if (pingCount > 3) {
		ncp_NetState = NCP_NET_NOT_CONNECT;
		pingCount = 0;
	}

	sendData(0, tmp_Buff2, resultLen);

}
void ncp_SendPingReq_second() {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	pingCount++;

	payload[len++] = NCP_REQ_PING;
	payload[len++] = 0;
	payload[len++] = 0;

	resultLen = plcs_GetNCPMessage(tmp_Buff2, 0, tmp_zrmp.zrm_Id + 1, payload, len);

	if (pingCount > 3) {
		ncp_NetState = NCP_NET_NOT_CONNECT;
		pingCount = 0;
	}

	sendData(0, tmp_Buff2, resultLen);

}

void ncp_SendRegisterReq() {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_REQ_REGISTER;
	payload[len++] = (uint8) (PLCS_ZSENSOR_TYPE >> 8);
	payload[len++] = (uint8) PLCS_ZSENSOR_TYPE;
	payload[len++] = PLCS_FW_VER;
	payload[len++] = PLCS_FW_VER;

	resultLen = plcs_GetNCPMessage(tmp_Buff2, 0, tmp_zrmp.zrm_Id, payload, len);
	sendData(0, tmp_Buff2, resultLen);
}
void ncp_SendRegisterReq_Second() {
	int len = 0;
	uint8 payload[10];
	uint8 resultLen = 0;

	payload[len++] = NCP_REQ_REGISTER;
	payload[len++] = (uint8) (PLCS_ZSENSOR_TYPE >> 8);
	payload[len++] = (uint8) PLCS_ZSENSOR_TYPE;
	payload[len++] = PLCS_FW_VER;
	payload[len++] = PLCS_FW_VER;

	resultLen = plcs_GetNCPMessage_Second(tmp_Buff2, 0, tmp_zrmp.zrm_Id + 1, payload, len);
	sendData(0, tmp_Buff2, resultLen);
}

uint8 ncp_ConnState() {
	return ncp_NetState;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
/////////////////////////////////////////////////////////////////////////////////////////////////////////
uint16 seqNumGenerator = 0;
uint16 getNCPSeqNumGenerator() {
	return seqNumGenerator++;
}

uint8 plcs_GetNCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getNCPSeqNumGenerator();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}
uint8 plcs_GetNCPMessage_Second(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[], uint8 len) {
	uint8 resultLen = 0;
	uint16 seqNum = getNCPSeqNumGenerator();

	resultMsg[resultLen++] = NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = PLCS_NCP_PROTOCOL_ID;
	resultMsg[resultLen++] = seqNum >> 8;
	resultMsg[resultLen++] = seqNum;
	resultMsg[resultLen++] = destId >> 8;
	resultMsg[resultLen++] = destId;
	resultMsg[resultLen++] = srcId >> 8;
	resultMsg[resultLen++] = srcId;
	for (int i = 0; i < len; i++)
		resultMsg[resultLen++] = payload[i];
	return resultLen;
}

