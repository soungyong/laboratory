#ifndef __MSN_PROTOCOL_H__
#define __MSN_PROTOCOL_H__

#include <stdio.h>
#include "Util.h"
#include "ZRMProtocol.h"
#include "XNetProtocol.h"

#define PLCS_GC_PROTOCOL_ID 			0x21

#define PLCS_GCP_REQ_STATE_INFO    		(0x24)
#define PLCS_GCP_RES_STATE_INFO     	(0x25)
#define PLCS_GCP_REQ_REBOOT         	(0x3A)
#define PLCS_GCP_NOTICE_EVENT       	(0x70)
#define PLCS_GCP_NOTICE_EVENT_DEBUG 	(0x7A)

#define PLCS_GCP_REQ_SET_SENSINGLEVEL   (0x80)
#define PLCS_GCP_RES_SET_SENSINGLEVEL   (0x81)
#define PLCS_GCP_REQ_SET_MODE           (0x82)
#define PLCS_GCP_RES_SET_MODE           (0x83)

///////////////////////////////////////////////////////////////////////////////////////////

uint16 getGCPSeqNumGenerator();
uint8 plcs_GetGCPMessage(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[], uint8 len);
uint8 plcs_GetGCPResMessage(uint8 resultMsg[], uint16 destId, uint16 srcId, uint8 payload[], uint8 len, uint16 seqNum);

void GCP_ProcessMessageFromZigbee(uint16 srcId, uint8 msg[], int length);
void GCP_ProcessMessageFromZigbee_second(uint16 srcId, uint8 msg[], int length);

void GCP_ProcessStateInfoReqFromZigbee(uint16 srcId, uint8 msg[], int length);
void GCP_ProcessStateInfoReqFromZigbee_second(uint16 srcId, uint8 msg[], int length);
void GCP_ProcessRebootReqFromZigbee(uint16 srcId, uint8 msg[], int length);
void GCP_ProcessSetSensingLevelReqFromZigbee(uint16 srcId, uint8 msg[], int length);
void GCP_ProcessSetSensingLevelReqFromZigbee_second(uint16 srcId, uint8 msg[], int length);
void GCP_ProcessSetModeReqFromZigbee(uint16 srcId, uint8 msg[], int length);

void GCP_SendStateInfoRes();
void GCP_SendStateInfoRes_second();
void GCP_SendNoticeEvent();
void GCP_SendNoticeEvent_Second();
void GCP_SendSetSensingLevelRes(uint16 seqNum);
void GCP_SendSetSensingLevelRes_second(uint16 seqNum);

void GCP_SendSetModeRes();

void GCP_SendNoticeEvent_Debug(uint8 debugSize, uint16 data[], uint16 standardAvr, uint8 avrDiff, uint8 isSensoredAvr, uint8 isSensoredAvr2);

uint8 MSN_GetSensorDiff();
uint8 MSN_GetSensorDiff_second();
uint8 MSN_IsNormalMode();
void MSN_SetSensorDiff(uint8 diff);
void MSN_SetSensorDiff_second(uint8 diff);
void MSN_SetNormalMode(uint8 mode);

#endif

