/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/26.
*/
package m2m.util;


import java.util.Calendar;

/**
 * Date to String formatting utility
 * 
 * - toString(date) added
 */
public class DateFormatter
{
	/**
	 * 17 char format : YYYYMMDDhhmmss000 format
	 * @return
	 */
	public static String toString17()
	{
		return toString17(Calendar.getInstance().getTime());
	}
	
	public static String toString17(java.util.Date date)
	{
		Object param[] = new Object[1];
		param[0] = date;

		return String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS000", param);
	}
	
	/**
	 * String value of date with 14 char format : "YYYYMMDDhhmmss" format
	 * @return
	 */
	public static String toString14()
	{
		return toString14(Calendar.getInstance().getTime());
	}

	/**
	 * String value of date with 14 char format : "YYYYMMDDhhmmss" format
	 * @return
	 */
	public static String toString14(java.util.Date date)
	{
		Object param[] = new Object[1];
		param[0] = date;

		return String.format("%1$tY%1$tm%1$td%1$tH%1$tM%1$tS", param);
	}
	
	/**
	 * String value of date with 10 char format : "YYYY-MM-DD" format
	 * @return
	 */
	public static String toStringE10()
	{
		return toStringE10(Calendar.getInstance().getTime());
	}

	public static String toStringE10(java.util.Date date)
	{
		Object param[] = new Object[1];
		param[0] = date;

		String s= String.format("%1$tY-%1$tm-%1$td", param);
		return s;
	}

	/**
	 * String value of date with 10 char format : "YYYY-MM-DD hh:mm:ss" format
	 * @return
	 */
	public static String toStringE19()
	{
		return toStringE19(Calendar.getInstance().getTime());
	}

	public static String toStringE19(java.util.Date date)
	{
		Object param[] = new Object[1];
		param[0] = date;

		return String.format("%1$tY-%1$tm-%1$td %1$tH:%1$tM:%1$tS", param);
	}
}
