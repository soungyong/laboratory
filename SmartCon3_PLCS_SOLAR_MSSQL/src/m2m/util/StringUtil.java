/**
	String manipulation util.

	- make HEX string with binary(byte) data.
 */
package m2m.util;

import java.util.StringTokenizer;

//import	java.lang.Integer;

public class StringUtil
{
	static final char _hexString[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	
	
	//OutputStream out;
	//Sy
	
	public StringUtil()
	{
	}
	
	/*
	public static String makeHexString(byte data[])
	{
		String str;
		str.
		printHex(data, data.length);
	}
	
	
	public static void printHex(byte data[], int length)
	{
		for (int i=0; i<length-1; i++)
		{
			printHex(data[i]);
			System.out.print(",");
		}		
		printHex(data[length-1]);
	}
	*/
	
	public static String makeHexString(byte data)
	{
		Object args[] = new Object[1];
		args[0] = new Byte(data);
		return String.format("%1$02X", args);	//(int)data);
		/*
		str
		
		System.out.print(_hexString[(data>>4) & 0x0F]);
		System.out.print(_hexString[data & 0x0F]);
		*/
	}
	
	public static String toHexString(byte data)
	{
		Object args[] = new Object[1];
		args[0] = new Byte(data);
		return String.format("%1$02X", args);
	}
	
	public static String toHexString(short data)
	{
		Object args[] = new Object[1];
		args[0] = new Short(data);
		return String.format("%1$04X", args);
	}

	public static String toHexString(int data)
	{
		Object args[] = new Object[1];
		args[0] = new Integer(data);
		return String.format("%1$08X", args);
	}

	public static String toHexString(byte[] data)
	{
		String tmp = "";
		if (data.length < 1) return tmp;
		
		tmp += toHexString(data[0]);

		for (int i=1; i<data.length; i++)
		{
			tmp += ","+toHexString(data[i]);
		}

		return tmp;
	}

	public static String toHexString(byte[] data, int offset)
	{
		String tmp = "";
		if ((data.length - offset) < 1) return tmp;
		
		tmp += toHexString(data[offset]);

		for (int i=offset+1; i<data.length; i++)
		{
			tmp += ","+toHexString(data[i]);
		}

		return tmp;
	}
	
	/**
	 * Parse hex string to byte[]
	 * @param data "12 98 fa 34 56 98 78 a";
	 * 				"12,98,fa,34,56,98,78,a";
	 * @return
	 */
	public static byte[] parseHexString(String data)
	{
		byte txData[] = new byte[1024];
		
		StringTokenizer tokenizer = new StringTokenizer(data, " ,\n\r");
		int len = 0; 
		while (tokenizer.hasMoreTokens())
		{
			String token = tokenizer.nextToken();
			
			if (token.length() < 1) continue;
			
			try
			{
				short ab = Short.parseShort(token, 16);
				txData[len++] = (byte)(ab&0xFF);
			}
			catch (Exception e)
			{
				//
			}
			//Byte ab = Byte.valueOf(token, 16);
		}
		
		byte rb[] = new byte[len];
		for (int i=0; i<len; i++)
			rb[i] = txData[i];
		return rb;
	}
}
