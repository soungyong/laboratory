package m2m.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class MyLog {
	public static final int INFO=2;
	public static final int WARNING=1;
	public static final int ERROR=0;
	
	private static MyLog _instance = null;
	private File file = null;
	FileWriter fw = null;
	static int debugiingLevel = 1;

	static SimpleDateFormat simpleFormatter = new SimpleDateFormat(
			"yyyy-MM-dd HH_mm_ss");

	static Calendar date;

	private MyLog() {
		// 디렉토리 생성
		File f = new File("log");
		if (!f.mkdirs())
			System.err.println("디렉토리 생성 실패");

		// 오래된 로그 삭제
		removeOldHistory();
		date = Calendar.getInstance();

		String s = simpleFormatter.format(date.getTime());

		file = new File("./log/Log_" + s + ".txt");
		try {
			fw = new FileWriter(file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void removeOldHistory() {
		String folterName = "./log";
		List<File> dirList = getDirFileList(folterName);
		int year;
		int month;
		String fileName;
		for (File file : dirList) {
			fileName = file.getName();
			if (fileName.length() == 27) {
				try {
					year = Integer.parseInt(fileName.substring(4, 8));
					month = Integer.parseInt(fileName.substring(9, 11));
					if (year * 12 + month + 2 < Calendar.getInstance().get(
							Calendar.YEAR)
							* 12
							+ Calendar.getInstance().get(Calendar.MONDAY)
							+ 1) {
						fileDelete(folterName + "/" + fileName);
						System.out.println("remove log: " + fileName);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	// 파일을 삭제하는 메소드
	public void fileDelete(String deleteFileName) {
		File I = new File(deleteFileName);
		if (I.delete() == false) {
			System.out.println("file 삭제 실패: " + deleteFileName);
		}

	}

	public List<File> getDirFileList(String dirPath) {
		// 디렉토리 파일 리스트
		List<File> dirFileList = null;

		// 파일 목록을 요청한 디렉토리를 가지고 파일 객체를 생성함
		File dir = new File(dirPath);

		// 디렉토리가 존재한다면
		if (dir.exists()) {
			// 파일 목록을 구함
			File[] files = dir.listFiles();

			// 파일 배열을 파일 리스트로 변화함
			dirFileList = Arrays.asList(files);
		}

		return dirFileList;
	}

	public synchronized static MyLog getInstance() {
		if (_instance == null)
			_instance = new MyLog();
		else if (date.get(Calendar.DAY_OF_MONTH) != Calendar.getInstance().get(
				Calendar.DAY_OF_MONTH)) {
			// _instance.terminate();
			_instance = new MyLog();
		}

		return _instance;
	}

	public static void e(String str) {
		try {
			System.out.println("e: " + str);
			getInstance().fw.append("\r\ne_"
					+ simpleFormatter.format(Calendar.getInstance().getTime())
					+ ":\t" + str);
			getInstance().fw.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void i(String str) {
		try {
			if (debugiingLevel >= 2) {
				System.out.println("i: " + str);
				getInstance().fw.append("\r\ne_"
						+ simpleFormatter.format(Calendar.getInstance()
								.getTime()) + ":\t" + str);
				getInstance().fw.flush();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void lWithSysOut(String str) {
		try {
			System.out.println("l: " + str);
			getInstance().fw.append("\r\nl_"
					+ simpleFormatter.format(Calendar.getInstance().getTime())
					+ ":\t" + str);
			getInstance().fw.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void l(String str) {
		try {
			if (debugiingLevel >= 1)
				System.out.println("l: " + str);
			getInstance().fw.append("\r\nl_"
					+ simpleFormatter.format(Calendar.getInstance().getTime())
					+ ":\t" + str);
			getInstance().fw.flush();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void setDebuggingLevel(int level) {
		debugiingLevel = level;
	}

	public void terminate() {
		try {
			getInstance().fw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
