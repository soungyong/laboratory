/**
 * Time of day
 * HHMMSS format.
 */
package m2m.util;

//import	java.lang.Integer;

/**
 * Time of the day.
 * 
 * @author tchan@m2mkorea.co.kr
 */
public class Time
{
	byte hh;		// 0 .. 23
	byte mm;		// 0 .. 59
	byte ss;		// 0 .. 59

	/**
	 * Make time (of day), seconds is set to zero.
	 * @param h	0..23
	 * @param m	0..59
	 */
	public Time(int h, int m)
	{
		this(h, m, 0);
	}

	/**
	 * Make time of day.
	 * @param h	0..23
	 * @param m	0..59
	 * @param s	0..59
	 */
	public Time(int h, int m, int s)
	{
		if ((h<0) || (h>23)) h = 0;
		if ((m<0) || (m>59)) m = 0;
		if ((s<0) || (s>59)) s = 0;

		this.hh = (byte)h;
		this.mm =(byte)m;
		this.ss = (byte)s;
	}
	
	/**
	 * Get hour of day 0..23
	 * @return	hour of day 0..23
	 */
	public int getHour()
	{
		return hh;
	}
	
	/**
	 * Get minute of time 0..59
	 * @return	minute 0..59
	 */
	public int getMinute()
	{
		return mm;
	}
	
	/**
	 * Get second of time 0..59
	 * @return second 0..59
	 */
	public int getSecond()
	{
		return ss;
	}
}
