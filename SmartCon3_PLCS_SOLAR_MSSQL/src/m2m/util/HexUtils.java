package m2m.util;
/**
 * ?ì??????Hex String?¼ë¡ ë³??ê³ , Hex String???ì??????ë³??ë Utility
 */
public class HexUtils {

	private static final char hexchar[] = { '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	
	/**
	 * byte ?°ì´?°ë? Hex String??ë³?
	 * 
	 * @param b Hex String?¼ë¡ ë³???byte ?°ì´??
	 * @return Hex String
	 */
	public static String toHexString(byte b) {
		StringBuffer buffer = new StringBuffer();

		buffer.append(hexchar[( b >>> 4) & 0xf]);
		buffer.append(hexchar[b & 0xf]);
		
		return buffer.toString();
	}
	
	/**
	 * char ?°ì´?°ë? Hex String??ë³?
	 * 
	 * @param c Hex String?¼ë¡ ë³???char ?°ì´??
	 * @return HexString
	 */
	public static String toHexString(char c) {
		StringBuffer buffer = new StringBuffer();

		buffer.append(hexchar[( c >>> 12) & 0xf]);
		buffer.append(hexchar[( c >>> 8) & 0xf]);
		buffer.append(hexchar[( c >>> 4) & 0xf]);
		buffer.append(hexchar[c & 0xf]);
		
		return buffer.toString();
	}
	
	/**
	 * short ?°ì´?°ë? Hex String??ë³?
	 * 
	 * @param s Hex String?¼ë¡ ë³???short ?°ì´??
	 * @return HexString
	 */
	public static String toHexString(short s) {
		StringBuffer buffer = new StringBuffer();

		buffer.append(hexchar[( s >>> 12) & 0xf]);
		buffer.append(hexchar[( s >>> 8) & 0xf]);
		buffer.append(hexchar[( s >>> 4) & 0xf]);
		buffer.append(hexchar[s & 0xf]);
		
		return buffer.toString();
	}
	
	/**
	 * int ?°ì´?°ë? Hex String??ë³?
	 * 
	 * @param i Hex String?¼ë¡ ë³???int ?°ì´??
	 * @return HexString
	 */
	public static String toHexString(int i) {
		StringBuffer buffer = new StringBuffer();

		buffer.append(hexchar[( i >>> 28) & 0xf]);
		buffer.append(hexchar[( i >>> 24) & 0xf]);
		buffer.append(hexchar[( i >>> 20) & 0xf]);
		buffer.append(hexchar[( i >>> 16) & 0xf]);
		buffer.append(hexchar[( i >>> 12) & 0xf]);
		buffer.append(hexchar[( i >>> 8) & 0xf]);
		buffer.append(hexchar[( i >>> 4) & 0xf]);
		buffer.append(hexchar[i & 0xf]);
		
		return buffer.toString();
	}
	
	/**
	 * long ?°ì´?°ë? Hex String??ë³?
	 * 
	 * @param l Hex String?¼ë¡ ë³???long ?°ì´??
	 * @return HexString
	 */
	public static String toHexString(long l){
		StringBuffer buffer = new StringBuffer();

		buffer.append(hexchar[(int)(( l >>> 60) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 56) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 52) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 48) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 44) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 40) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 36) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 32) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 28) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 24) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 20) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 16) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 12) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 8) & 0xf)]);
		buffer.append(hexchar[(int)(( l >>> 4) & 0xf)]);
		buffer.append(hexchar[(int)(l & 0xf)]);
		
		return buffer.toString();
	}
	
	/**
	 * Hex String??byteë¡?ë³?
	 * 
	 * @param hexStr
	 * @return byte
	 */
	public static byte toByte(String hexStr){
		byte result = 0;
		String hex = hexStr.toUpperCase();
		
		for(int i = 0 ; i < hex.length() ; i++){
			char c = hex.charAt(hex.length() - i - 1);
			byte b = toByte(c);
			result |= (b & 0x0f) << (i*4); 
		}
		
		return result;
	}
	
	/**
	 * Hex String??char ê°ì¼ë¡?ë³?
	 * 
	 * @param hexStr
	 * @return char
	 */
	public static char toChar(String hexStr){
		char result = 0;
		String hex = hexStr.toUpperCase();
		
		for(int i = 0 ; i < hex.length() ; i++){
			char c = hex.charAt(hex.length() - i - 1);
			byte b = toByte(c);
			result |= (b & 0x0f) << (i*4); 
		}
		
		return result;
	}
	
	/**
	 * Hex String??short?¼ë¡ ë³?
	 * 
	 * @param hexStr
	 * @return short ê°?
	 */
	public static short toShort(String hexStr){
		short result = 0;
		String hex = hexStr.toUpperCase();
		
		for(int i = 0 ; i < hex.length() ; i++){
			char c = hex.charAt(hex.length() - i - 1);
			byte b = toByte(c);
			result |= (b & 0x0f) << (i*4); 
		}
		
		return result;
	}
	
	/**
	 * Hex String??intë¡?ë³?
	 * 
	 * @param hexStr
	 * @return int ê°?
	 */
	public static int toInt(String hexStr){
		int result = 0;
		String hex = hexStr.toUpperCase();

		for(int i = 0 ; i < hex.length() ; i++){
			char c = hex.charAt(hex.length() - i - 1);
			byte b = toByte(c);
			result |= (b & 0x0f) << (i*4); 
		}
		
		return result;
	}
	
	/**
	 * Hex String??long?¼ë¡ ë³?
	 * 
	 * @param hexStr
	 * @return long ê°?
	 */
	public static long toLong(String hexStr){
		long result = 0;
		String hex = hexStr.toUpperCase();

		for(int i = 0 ; i < hex.length() ; i++){
			char c = hex.charAt(hex.length() - i - 1);
			byte b = toByte(c);
			result |= (long)(b & 0x0f) << (i*4); 
		}
		
		return result;
	}
	
	private static byte toByte(char c){
		switch(c){
		case '0':
			return 0;
		case '1':
			return 1;
		case '2':
			return 2;
		case '3':
			return 3;
		case '4':
			return 4;
		case '5':
			return 5;
		case '6':
			return 6;
		case '7':
			return 7;
		case '8':
			return 8;
		case '9':
			return 9;
		case 'A':
			return 10;
		case 'B':
			return 11;
		case 'C':
			return 12;
		case 'D':
			return 13;
		case 'E':
			return 14;
		case 'F':
			return 15;
		}
		
		return 0;
	}
}