/**
		Debug print utility.
		- printing as HEX string.
 */
package m2m.util;

//import	java.lang.Integer;

public class Printer
{
	static final char _hexString[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
	
	
	//OutputStream out;
	//Sy
	
	public Printer()
	{
	}
	
	
	public static void printHex(byte data[])
	{
		printHex(data, data.length);
	}
	
	
	public static void printHex(byte data[], int length)
	{
		for (int i=0; i<length-1; i++)
		{
			printHex(data[i]);
			System.out.print(",");
		}		
		printHex(data[length-1]);
	}
	
	public static void printHex(byte data)
	{
		System.out.print(_hexString[(data>>4) & 0x0F]);
		System.out.print(_hexString[data & 0x0F]);
	}

	public static void printHex(short data)
	{
		
		printHex((byte)((data>>8) & 0xFF));
		System.out.print(",");
		printHex((byte)(data & 0xFF));
	}

	public static void printHex(int data)
	{
		printHex((byte)((data>>24) & 0xFF));
		System.out.print(",");
		printHex((byte)((data>>16) & 0xFF));
		System.out.print(",");
		printHex((byte)((data>> 8) & 0xFF));
		System.out.print(",");
		printHex((byte)(data & 0xFF));
	}
}
