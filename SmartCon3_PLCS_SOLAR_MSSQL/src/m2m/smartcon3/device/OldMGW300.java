package m2m.smartcon3.device;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;

import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.devicenetwork.ResponseWaitMessage;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_ControlCircuitReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_ControlCircuitRes;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_DebugLogLCReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_DebugLogLCRes;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_DebugLogReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_DebugLogRes;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_MappingDimmerReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_MappingDimmerRes;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_MappingSensorReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_MappingSensorRes;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_NoticeEvent;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_PowermeterReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_PowermeterRes;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_ResetDebugLogLCReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_ResetDebugLogReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_ResetMappingDimmerReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_ResetMappingSensorReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_SetSensingLevelReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_SetSensingLevelRes;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_StateInfoReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_StateInfoRes;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_UpdatePowermeterReq;
import m2m.smartcon3.protocol.gcp1_4.I_GCP_Message;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.smartcon3.protocol.ncp1_0.I_NCP_Message;
import m2m.smartcon3.protocol.ncp1_0.NCP_Message_PingReq;
import m2m.smartcon3.protocol.ncp1_0.NCP_Message_PingRes;
import m2m.smartcon3.protocol.ncp1_0.NCP_Message_RegisterNodeReq;
import m2m.smartcon3.protocol.ncp1_0.NCP_Message_RegisterNodeRes;
import m2m.smartcon3.protocol.ncp1_0.NCP_Message_RegisterReq;
import m2m.smartcon3.protocol.ncp1_0.NCP_Message_RegisterRes;
import m2m.smartcon3.protocol.ncp1_0.NCP_Message_SendTimeInfo;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class OldMGW300 implements I_Device, Runnable {
	I_XNetHandler xnetHandler = null;
	short deviceId = 0x00;
	E_DeviceType deviceType;

	HashMap<Short, I_Device> lcMap = new HashMap<Short, I_Device>();

	String ipAddress = "192.168.0.50";
	String serverIpAddress = "192.168.0.10";
	String defaultGateway = "192.168.0.1";
	String netMask = "255.255.255.0";
	int port = 0;
	boolean isSensorSharingMode = false;
	int deviceVersion;
	int firmwareVersion;

	Calendar currentTime = Calendar.getInstance();
	int rebootCount = 0;
	Calendar lastRebootTime = Calendar.getInstance();
	int serverConnectionCount = 0;
	Calendar lastServerConnectionTime = Calendar.getInstance();
	
	boolean isTerminated=false;

	boolean isReceiveRegReq = false;
	LinkedList<ResponseWaitMessage> _responseWaitMessageQueue;

	public OldMGW300(short deviceId) {
		this();
		this.deviceId = deviceId;
		new Thread(this).start();
		
		MyLog.lWithSysOut("Create OldMGW300: " + HexUtils.toHexString(deviceId));
	}

	public OldMGW300() {
		deviceType = E_DeviceType.MGW300;		
		new DeviceStateChangedEvent(this, "", null);
	}

	void addMessageToResponseWaitMessageQueue(I_Message msg) {
		_responseWaitMessageQueue.add(new ResponseWaitMessage(msg, 5));
	}

	synchronized I_Message removceMessageInResponseWaitMessageQeueueByIndex(
			int i) {
		return _responseWaitMessageQueue.remove(i).getMsg();
	}

	public void addLC(OldLC100 device) {
		device.setXNetHandler(xnetHandler);
		device.setParent(this);
		lcMap.put(device.getId(), device);
	}

	public boolean removeLC(OldLC100 device) {
		if (lcMap.containsValue(device)){
			device.terminate();
			lcMap.remove(device.getId());
			new DeviceStateChangedEvent(this, "", null);
		}
		else
			return false;
		return true;
	}

	public OldLC100 getLC(short id) {
		return (OldLC100) lcMap.get(id);
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		if (xnetHandler == null)
			return NETWORK_STATE.OFFLINE;
		else if (xnetHandler.isConnected() == false)
			return NETWORK_STATE.OFFLINE;
		return NETWORK_STATE.ONLINE;
	}

	@Override
	public E_DeviceType getDeviceType() {
		// TODO Auto-generated method stub
		return deviceType;
	}

	public void handleMessage(I_Message msg) {
		if (msg instanceof I_NCP_Message) {
			I_NCP_Message ncpMsg = (I_NCP_Message) msg;
			switch (ncpMsg.getMessageType()) {
			case REQ_REGISTER:
				handleRegisterReq(ncpMsg);
				break;
			case REQ_PING:
				handlePingReq(ncpMsg);
				break;
			case REQ_REGISTER_NODE:
				handleRegisterNodeReq(ncpMsg);
				break;
			case SEND_TIME_INFO:
				handleSendTimeInfo(ncpMsg);
				break;
			}
		} else if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			switch (gcpMsg.getMessageType()) {
			case RES_STATEINFO:
				handleStateInfoRes(gcpMsg);
				break;
			case REQ_STATEINFO:
				handleStateInfoReq(gcpMsg);
				break;
			case RES_CONTROL_CIRCUIT:
				handleControlCircuitRes(gcpMsg);
				break;
			case REQ_CONTROL_CIRCUIT:
				handleControlCircuitReq(gcpMsg);
				break;
			case RES_POWERMETER:
				handlePowermeterRes(gcpMsg);
				break;
			case REQ_POWERMETER:
				handlePowermeterReq(gcpMsg);
				break;
			case REQ_UPDATEPOWERMETER:
				handleUpdatePowermeter(gcpMsg);
				break;
			case RES_MAPPING_SENSOR:
				handleMappingSensorRes(gcpMsg);
				break;
			case REQ_MAPPING_SENSOR:
				handleMappingSensorReq(gcpMsg);
				break;
			case RES_MAPPING_DIMMER:
				handleMappingDimmerRes(gcpMsg);
				break;
			case REQ_MAPPING_DIMMER:
				handleMappingDimmerReq(gcpMsg);
				break;
			case REQ_RESET_MAPPING_SENSOR:
				handleResetMappingSensorReq(gcpMsg);
				break;
			case REQ_RESET_MAPPING_DIMMER:
				handleResetMappingDimmerReq(gcpMsg);
				break;
			case NOTICE_EVENT:
				handleNoticeEvent(gcpMsg);
				break;
			case RES_SET_SENSING_LEVEL:
				handleSetSensingLevelRes(gcpMsg);
				break;
			case REQ_SET_SENSING_LEVEL:
				handleSetSensingLevelReq(gcpMsg);
				break;
			case RES_DEBUG_LOG:
				handleDebugLogRes(gcpMsg);
				break;
			case REQ_DEBUG_LOG:
				handleDebugLogReq(gcpMsg);
				break;
			case RES_DEBUG_LOG_LC:
				handleDebugLogLCRes(gcpMsg);
				break;
			case REQ_DEBUG_LOG_LC:
				handleDebugLogLCReq(gcpMsg);
				break;
			case REQ_RESET_DEBUG_LOG:
				handleResetDebugLogReq(gcpMsg);
				break;
			case REQ_RESET_DEBUG_LOG_LC:
				handleResetDebugLogLCReq(gcpMsg);
				break;
			}
		}
	}

	private void handleSendTimeInfo(I_NCP_Message ncpMsg) {
		MyLog.i("handleSendTimeInfo");
		NCP_Message_SendTimeInfo msg = (NCP_Message_SendTimeInfo) ncpMsg;
		MyLog.i("TimeInfo: " + msg.getDateTimeString());
	}

	private void handleResetDebugLogLCReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleDebugLogLCReq");
		GCP_Message_ResetDebugLogLCReq reqMsg = (GCP_Message_ResetDebugLogLCReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		OldLC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);

	}

	private void handleResetDebugLogReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetDebugLogReq");

		this.currentTime = Calendar.getInstance();
		this.rebootCount = 0;
		this.serverConnectionCount = 0;
		this.lastRebootTime = Calendar.getInstance();
		this.lastServerConnectionTime = Calendar.getInstance();
	}

	private void handleDebugLogLCReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleDebugLogLCReq");
		GCP_Message_DebugLogLCReq reqMsg = (GCP_Message_DebugLogLCReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		OldLC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleDebugLogReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleDebugLogReq");
		GCP_Message_DebugLogReq reqMsg = (GCP_Message_DebugLogReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		byte[] currentTime = new byte[6];
		int rebootCount = 0;
		byte[] lastRebootTime = new byte[5];
		int serverConnectionCount = 0;
		byte[] lastServerConnectionTime = new byte[5];

		this.currentTime = Calendar.getInstance();

		currentTime[0] = new Integer(this.currentTime.get(Calendar.YEAR) % 100)
				.byteValue();
		currentTime[1] = new Integer(this.currentTime.get(Calendar.MONTH) + 1)
				.byteValue();
		currentTime[2] = new Integer(
				this.currentTime.get(Calendar.DAY_OF_MONTH)).byteValue();
		currentTime[3] = new Integer(this.currentTime.get(Calendar.HOUR))
				.byteValue();
		currentTime[4] = new Integer(this.currentTime.get(Calendar.MINUTE))
				.byteValue();
		currentTime[4] = new Integer(this.currentTime.get(Calendar.SECOND))
				.byteValue();

		rebootCount = this.rebootCount;
		serverConnectionCount = this.serverConnectionCount;

		lastRebootTime[0] = new Integer(
				this.lastRebootTime.get(Calendar.YEAR) % 100).byteValue();
		lastRebootTime[1] = new Integer(
				this.lastRebootTime.get(Calendar.MONTH) + 1).byteValue();
		lastRebootTime[2] = new Integer(
				this.lastRebootTime.get(Calendar.DAY_OF_MONTH)).byteValue();
		lastRebootTime[3] = new Integer(this.lastRebootTime.get(Calendar.HOUR))
				.byteValue();
		lastRebootTime[4] = new Integer(
				this.lastRebootTime.get(Calendar.MINUTE)).byteValue();

		lastServerConnectionTime[0] = new Integer(
				this.lastServerConnectionTime.get(Calendar.YEAR) % 100)
				.byteValue();
		lastServerConnectionTime[1] = new Integer(
				this.lastServerConnectionTime.get(Calendar.MONTH) + 1)
				.byteValue();
		lastServerConnectionTime[2] = new Integer(
				this.lastServerConnectionTime.get(Calendar.DAY_OF_MONTH))
				.byteValue();
		lastServerConnectionTime[3] = new Integer(
				this.lastServerConnectionTime.get(Calendar.HOUR)).byteValue();
		lastServerConnectionTime[4] = new Integer(
				this.lastServerConnectionTime.get(Calendar.MINUTE)).byteValue();

		GCP_Message_DebugLogRes resMsg = new GCP_Message_DebugLogRes(destId,
				currentTime, rebootCount, lastRebootTime,
				serverConnectionCount, lastServerConnectionTime);
		resMsg.setSeqNum((byte) reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleSetSensingLevelReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleSetSensingLevelReq");
		GCP_Message_SetSensingLevelReq reqMsg = (GCP_Message_SetSensingLevelReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = reqMsg.getNodeId();

		MyLog.i("nodeId: " + nodeId);

		OldLC100 lc = getLC(nodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleResetMappingDimmerReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetMappingDimmerReq");
		GCP_Message_ResetMappingDimmerReq reqMsg = (GCP_Message_ResetMappingDimmerReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		OldLC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleResetMappingSensorReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetMappingSensorReq");
		GCP_Message_ResetMappingSensorReq reqMsg = (GCP_Message_ResetMappingSensorReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		OldLC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleMappingDimmerReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingDimmerReq");
		GCP_Message_MappingDimmerReq reqMsg = (GCP_Message_MappingDimmerReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = reqMsg.getNodeId();

		MyLog.i("subNodeId: " + nodeId);

		OldLC100 lc = getLC(nodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);

	}

	private void handleMappingSensorReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingSensorReq");
		GCP_Message_MappingSensorReq reqMsg = (GCP_Message_MappingSensorReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = reqMsg.getNodeId();

		OldLC100 lc = getLC(nodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleUpdatePowermeter(I_GCP_Message gcpMsg) {
		MyLog.i("handleUpdatePowermeter");
		GCP_Message_UpdatePowermeterReq reqMsg = (GCP_Message_UpdatePowermeterReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short nodeId = reqMsg.getNodeId();

		OldLC100 lc = getLC(nodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handlePowermeterReq(I_GCP_Message gcpMsg) {
		MyLog.i("handlePowermeterReq");
		GCP_Message_PowermeterReq reqMsg = (GCP_Message_PowermeterReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = reqMsg.getNodeId();

		MyLog.i("nodeId: " + nodeId);
		OldLC100 lc = getLC(nodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleControlCircuitReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlCircuitReq");
		GCP_Message_ControlCircuitReq reqMsg = (GCP_Message_ControlCircuitReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleStateInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleStateInfoReq");
		GCP_Message_StateInfoReq reqMsg = (GCP_Message_StateInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short nodeId = reqMsg.getNodeId();

		GCP_Message_StateInfoRes resMsg = null;

		OldLC100 device = getLC(nodeId);
		if (device == null)
			return;
		device.handleMessage((I_Message) gcpMsg);
	}

	public I_Device getLCByIndex(int index) {
		if (lcMap.size() <= index)
			return null;
		int cnt = 0;
		for (I_Device device : lcMap.values()) {
			if (cnt == index)
				return device;
			cnt++;
		}
		return null;
	}

	private void handleDebugLogLCRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleDebugLogLCRes");
		GCP_Message_DebugLogLCRes resMsg = (GCP_Message_DebugLogLCRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleDebugLogRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleDebugLogRes");
		GCP_Message_DebugLogRes resMsg = (GCP_Message_DebugLogRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		byte[] currentTime = resMsg.getCurrentTime();
		int rebootCount = resMsg.getRebootCount();
		byte[] lastRebootTime = resMsg.getLastRebootTime();
		int serverConnectionCount = resMsg.getServerConnectionCount();
		byte[] lastServerConnectionTime = resMsg.getLastServerConnectionTime();

		String str = "";
		for (int i = 0; i < currentTime.length; i++)
			str += currentTime[i];
		MyLog.i("currentTime: " + str);
		MyLog.i("rebootCount: " + rebootCount);
		str = "";
		for (int i = 0; i < lastRebootTime.length; i++)
			str += lastRebootTime[i];
		MyLog.i("lastRebootTime: " + str);
		MyLog.i("serverConnectionCount: " + serverConnectionCount);
		str = "";
		for (int i = 0; i < lastServerConnectionTime.length; i++)
			str += lastServerConnectionTime[i];
		MyLog.i("lastServerConnectionTime: " + str);

		this.rebootCount = rebootCount;
		this.serverConnectionCount = serverConnectionCount;
		this.currentTime.set(Calendar.YEAR, 2000 + new Integer(currentTime[0]));
		this.currentTime.set(Calendar.MONTH, new Integer(currentTime[1]) - 1);
		this.currentTime.set(Calendar.DAY_OF_MONTH, 2000 + new Integer(
				currentTime[2]));
		this.currentTime.set(Calendar.HOUR_OF_DAY, 2000 + new Integer(
				currentTime[3]));
		this.currentTime.set(Calendar.MINUTE,
				2000 + new Integer(currentTime[4]));
		this.currentTime.set(Calendar.SECOND,
				2000 + new Integer(currentTime[5]));

		this.lastRebootTime.set(Calendar.YEAR, 2000 + new Integer(
				lastRebootTime[0]));
		this.lastRebootTime.set(Calendar.MONTH,
				new Integer(lastRebootTime[1]) - 1);
		this.lastRebootTime.set(Calendar.DAY_OF_MONTH, 2000 + new Integer(
				lastRebootTime[2]));
		this.lastRebootTime.set(Calendar.HOUR_OF_DAY, 2000 + new Integer(
				lastRebootTime[3]));
		this.lastRebootTime.set(Calendar.MINUTE, 2000 + new Integer(
				lastRebootTime[4]));

		this.lastServerConnectionTime.set(Calendar.YEAR, 2000 + new Integer(
				lastServerConnectionTime[0]));
		this.lastServerConnectionTime.set(Calendar.MONTH, new Integer(
				lastServerConnectionTime[1]) - 1);
		this.lastServerConnectionTime.set(Calendar.DAY_OF_MONTH,
				2000 + new Integer(lastServerConnectionTime[2]));
		this.lastServerConnectionTime.set(Calendar.HOUR_OF_DAY,
				2000 + new Integer(lastServerConnectionTime[3]));
		this.lastServerConnectionTime.set(Calendar.MINUTE, 2000 + new Integer(
				lastServerConnectionTime[4]));		
	}

	private void handleSetSensingLevelRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleSetSensingLevelRes");
		GCP_Message_SetSensingLevelRes resMsg = (GCP_Message_SetSensingLevelRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = resMsg.getNodeId();

		I_Device device = getLC(nodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleMappingDimmerRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingDimmerRes");
		GCP_Message_MappingDimmerRes resMsg = (GCP_Message_MappingDimmerRes) gcpMsg;
		
		short nodeId = resMsg.getNodeId();
		short dimmerId = resMsg.getDimmerId();

		I_Device device = getLC(nodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleMappingSensorRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingSensorRes");
		GCP_Message_MappingSensorRes resMsg = (GCP_Message_MappingSensorRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handlePowermeterRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlGatewayRes");
		GCP_Message_PowermeterRes resMsg = (GCP_Message_PowermeterRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short nodeId = resMsg.getNodeId();

		I_Device device = getLC(nodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleControlCircuitRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlCircuitRes");
		GCP_Message_ControlCircuitRes reqMsg = (GCP_Message_ControlCircuitRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short nodeId = reqMsg.getNodeId();

		I_Device device = getLC(nodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleStateInfoRes(I_GCP_Message gcpMsg) {
		GCP_Message_StateInfoRes resMsg = (GCP_Message_StateInfoRes) gcpMsg;
		
		I_Device device = getLC(resMsg.getNodeId());
		if (device != null)
			device.handleMessage((I_Message) resMsg);

	}

	private void handleNoticeEvent(I_GCP_Message gcpMsg) {
		MyLog.i("handleNoticeEvent");
		GCP_Message_NoticeEvent reqMsg = (GCP_Message_NoticeEvent) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		I_Device device = getLC(reqMsg.getNodeId());
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleRegisterNodeReq(I_NCP_Message ncpMsg) {
		MyLog.i("handleRegisterNodeReq");
		NCP_Message_RegisterNodeReq reqMsg = ((NCP_Message_RegisterNodeReq) ncpMsg);
		short deviceType = reqMsg.getDeviceType();
		OldLC100 lc = getLC(reqMsg.getSubNodeId());
		
		if (lc == null)
			addLC(new OldLC100(reqMsg.getSubNodeId()));
		
		lc = getLC(reqMsg.getSubNodeId());		

		lc.handleMessage((I_Message) ncpMsg);
	}

	private void handlePingReq(I_NCP_Message ncpMsg) {
		MyLog.i("handlePingReq");

		int requestRegister = 0;
		if (isReceiveRegReq == false)
			requestRegister = 1;

		NCP_Message_PingRes resMsg = new NCP_Message_PingRes(
				ncpMsg.getNodeId(), 0x00, requestRegister, 0x20);
		resMsg.setSeqNum(ncpMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleRegisterReq(I_NCP_Message ncpMsg) {
		MyLog.i("handleRegisterReq");
		
		NCP_Message_RegisterReq reqMsg = (NCP_Message_RegisterReq)ncpMsg;
		short srcId = (short) 0x0000;
		short destId = ncpMsg.getNodeId();
		
		
		this.deviceVersion = reqMsg.getDeviceVersion();
		this.firmwareVersion = reqMsg.getFirmwareVersion();
		
		NCP_Message_RegisterRes resMsg = new NCP_Message_RegisterRes(destId,
				0x00, 0x20);
		resMsg.setSeqNum(ncpMsg.getSeqNum());
		sendMessage(resMsg);

		isReceiveRegReq = true;

		try {
			String dateTime = "";
			Calendar cal = Calendar.getInstance();
			dateTime = String.format("%04d%02d%02d%1d%02d%02d%02d",
					cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1,
					cal.get(Calendar.DAY_OF_MONTH),
					cal.get(Calendar.DAY_OF_WEEK),
					cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE),
					cal.get(Calendar.SECOND));

			NCP_Message_SendTimeInfo timeInfoMsg = new NCP_Message_SendTimeInfo(
					destId, 1, dateTime);
			sendMessage(timeInfoMsg);
		} catch (Exception e) {
			MyLog.lWithSysOut(e.getMessage());
		}
	}

	@Override
	public void setXNetHandler(I_XNetHandler xnetHandler) {
		if (this.xnetHandler != null)
			this.xnetHandler.terminate();

		this.xnetHandler = xnetHandler;
		for (I_Device device : lcMap.values())
			device.setXNetHandler(xnetHandler);
		xnetHandler.setParent(this);

		lastServerConnectionTime = Calendar.getInstance();
		serverConnectionCount++;
	}

	@Override
	public void run() {
		Calendar lastSentTime_ReRegisterReq = Calendar.getInstance();
		
		while (isTerminated==false) {
			if (xnetHandler != null) {
				I_Message msg = null;
				while ((msg = xnetHandler.removeMessage()) != null)
					handleMessage(msg);
			}
			if (lastSentTime_ReRegisterReq.getTimeInMillis() + 60000 < Calendar
					.getInstance().getTimeInMillis()) {// 1분에 한번
														// debugLog
														// 가져오기.
				isReceiveRegReq=false;
				lastSentTime_ReRegisterReq = Calendar.getInstance();
			}
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getServerIpAddress() {
		return serverIpAddress;
	}

	public String getDefaultGateway() {
		return defaultGateway;
	}

	public String getNetMask() {
		return netMask;
	}

	public int getPort() {
		return port;
	}

	public boolean isSensorSharingMode() {
		return isSensorSharingMode;
	}

	public int getNumOfSubDevices() {
		return lcMap.size();
	}

	public boolean sendMessage(I_Message msg) {
		if (xnetHandler != null){
			return xnetHandler.sendMessage((I_Message) msg);
		}
		return false;
	}

	public boolean sendRegisterReq() {
		MyLog.i("sendRegisterReq");
		return sendMessage(new NCP_Message_RegisterReq(getId(), getDeviceType()
				.toShort(), 0x00, 0x11));
	}

	public boolean sendPingReq() {
		MyLog.i("sendPingReq");
		return sendMessage(new NCP_Message_PingReq(getId(), 0x00, 0x00));
	}

	public void sendRegisterNodeReq(I_Device device) {
		MyLog.i("sendRegisterNodeReq");
		short srcId = getId();
		short destId = (short) 0x0000;
		short subSrcId = device.getId();
		short deviceType = device.getDeviceType().toShort();
		short deviceId = device.getId();
		int deviceVersion = 0;
		int firmwareVersion = 1;
		int netAddr = 0;

		I_NCP_Message msg = new NCP_Message_RegisterNodeReq(destId, subSrcId,
				deviceType, deviceId, deviceVersion, firmwareVersion, netAddr);
		sendMessage((I_Message) msg);
	}

	private boolean sendMessageWithResponse(I_Message msg) {
		if (xnetHandler != null) {
			addMessageToResponseWaitMessageQueue((I_Message) msg);
			return xnetHandler.sendMessageWithResponse((I_Message) msg);
		}

		addMessageToResponseWaitMessageQueue((I_Message) msg);
		return false;
	}

	@Override
	public I_Device getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	public Calendar getCurrentTime() {
		return currentTime;
	}

	public int getRebootCount() {
		return rebootCount;
	}

	public Calendar getLastRebootTime() {
		return lastRebootTime;
	}

	public int getServerConnectionCount() {
		return serverConnectionCount;
	}

	public Calendar getLastServerConnectionTime() {
		return lastServerConnectionTime;
	}

	public int sendDebugLogReq() throws UnformattedPacketException {
		MyLog.i("sendDebugLogReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_DebugLogReq(srcId);
		if(sendMessageWithResponse((I_Message) msg)==false){			
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public boolean sendResetDebugLogReq() {
		MyLog.i("sendResetDebugLogReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ResetDebugLogReq(srcId);
		return sendMessage((I_Message) msg);
	}

	public int getDeviceVersion() {
		return deviceVersion;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}
	
	public void termiate(){
		this.isTerminated = true;
		xnetHandler.terminate();
		for(I_Device device : lcMap.values()){
			((LC100)device).terminate();
		}		
	}
	@Override
	public I_XNetHandler getXNetHandler() {
		return this.xnetHandler;
	}	
	
}
