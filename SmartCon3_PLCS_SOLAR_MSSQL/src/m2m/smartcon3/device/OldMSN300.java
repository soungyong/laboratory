package m2m.smartcon3.device;

import java.util.Calendar;
import java.util.LinkedList;

import m2m.smartcon3.device.I_Device.NETWORK_STATE;
import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.devicenetwork.ResponseWaitMessage;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.event.SensorEvent;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_NoticeEvent;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_SetSensingLevelReq;
import m2m.smartcon3.protocol.gcp1_4.GCP_Message_SetSensingLevelRes;
import m2m.smartcon3.protocol.gcp1_4.I_GCP_Message;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class OldMSN300 implements I_Device {
	I_XNetHandler xnetHandler = null;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.MSN300;
	private int firmwaraeVersion;
	private int sensingLevel = -1;
	I_Device parent = null;

	LinkedList<ResponseWaitMessage> _responseWaitMessageQueue;

	NETWORK_STATE networkState = NETWORK_STATE.OFFLINE;
	private Calendar lastMessageReceivingTime = Calendar.getInstance();

	public OldMSN300(I_Device parent, short id) {
		this.parent = parent;
		this.deviceId = id;

		_responseWaitMessageQueue = new LinkedList<ResponseWaitMessage>();
		new DeviceStateChangedEvent(this, "", null);
		
		MyLog.lWithSysOut("Create OldMSN300: " + HexUtils.toHexString(deviceId));
	}

	@Override
	public short getId() {
		return this.deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		if (xnetHandler == null)
			return NETWORK_STATE.OFFLINE;
		else if (xnetHandler.isConnected() == false)
			return NETWORK_STATE.OFFLINE;
		return NETWORK_STATE.ONLINE;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}

	@Override
	public void setXNetHandler(I_XNetHandler xnetHandler) {
		this.xnetHandler = xnetHandler;
	}

	private boolean sendMessage(I_Message msg) {
		if (xnetHandler == null)
			return false;
		return xnetHandler.sendMessage(msg);
	}

	@Override
	public void handleMessage(I_Message msg) {
		lastMessageReceivingTime = Calendar.getInstance();
		networkState = NETWORK_STATE.ONLINE;

		if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			switch (gcpMsg.getMessageType()) {
			case RES_SET_SENSING_LEVEL:
				handleSetSensingLevelRes(gcpMsg);
				break;
			case REQ_SET_SENSING_LEVEL:
				handleSetSensingLevelReq(gcpMsg);
				break;
			case NOTICE_EVENT:
				handleNoticeEvent(gcpMsg);
				break;
			}
		}
	}

	private void handleNoticeEvent(I_GCP_Message gcpMsg) {
		MyLog.i("MSN_handleNoticeEvent");
		GCP_Message_NoticeEvent reqMsg = (GCP_Message_NoticeEvent) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short nodeId = reqMsg.getNodeId();

		short sensorId = reqMsg.getSensorId();
		MyLog.i("nodeId: " + nodeId);
		MyLog.i("sensorId: " + sensorId);

		new SensorEvent(this, "SensorEvent: " + sensorId, null);
	}

	private void handleSetSensingLevelReq(I_GCP_Message gcpMsg) {
		MyLog.i("MSN_handleSetSensingLevelReq");
		GCP_Message_SetSensingLevelReq reqMsg = (GCP_Message_SetSensingLevelReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = reqMsg.getNodeId();
		short sensorId = reqMsg.getSensorId();
		int level = reqMsg.getLevel();
		this.sensingLevel = level;

		new DeviceStateChangedEvent(this, "", null);

		MyLog.i("nodeId: " + nodeId);
		MyLog.i("sensorId: " + sensorId);
		MyLog.i("level: " + level);

		GCP_Message_SetSensingLevelRes resMsg = new GCP_Message_SetSensingLevelRes(
				nodeId, sensorId, level);
		resMsg.setSeqNum((byte) reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleSetSensingLevelRes(I_GCP_Message gcpMsg) {
		MyLog.i("MSN_handleSetSensingLevelRes");
		GCP_Message_SetSensingLevelRes resMsg = (GCP_Message_SetSensingLevelRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getNodeId();
		short sensorId = resMsg.getSensorId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("sensorId: " + sensorId);

		this.sensingLevel = resMsg.getLevel();
		new DeviceStateChangedEvent(this, "", null);

		I_Message reqIMsg = getRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_SetSensingLevelReq) {
			GCP_Message_SetSensingLevelReq reqMsg = (GCP_Message_SetSensingLevelReq) reqIMsg;

			new ReceiveResponseMessageEvent(this, reqMsg, (I_Message) gcpMsg,
					"", null);
			this.sensingLevel = reqMsg.getLevel();
			new DeviceStateChangedEvent(this, "", null);
		}

	}

	public int getSensingLevel() {
		return sensingLevel;
	}

	public boolean sendNoticeEvent(short subNodeId, short sensorId) {
		MyLog.i("sendNoticeEvent");

		short srcId = getId();
		short destId = (short) 0x0000;
		E_GCP_ControlMode ctrlMode[] = new E_GCP_ControlMode[8];
		for (int i = 0; i < 8; i++)
			ctrlMode[i] = E_GCP_ControlMode.DEFAULT;

		I_GCP_Message msg = new GCP_Message_NoticeEvent(srcId, 0x01, sensorId,
				ctrlMode);
		return sendMessage((I_Message) msg);
	}

	public int sendSetSensingLevelReq(int level) {
		MyLog.i("sendSetSensingLevelReq");
		short nodeId = getParent().getId();
		short sensorId = getId();

		I_GCP_Message msg = new GCP_Message_SetSensingLevelReq(nodeId,
				sensorId, level);
		sendMessageWithResponse((I_Message) msg);
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));

	}

	public void setFirmwareVersion(int version) {
		this.firmwaraeVersion = version;
	}

	private boolean sendMessageWithResponse(I_Message msg) {
		if (xnetHandler != null) {
			addMessageToResponseWaitMessageQueue((I_Message) msg);
			return xnetHandler.sendMessageWithResponse((I_Message) msg);
		}
		addMessageToResponseWaitMessageQueue((I_Message) msg);
		return false;
	}

	public I_Device getParent() {
		return parent;
	}

	void addMessageToResponseWaitMessageQueue(I_Message msg) {
		_responseWaitMessageQueue.add(new ResponseWaitMessage(msg, 5));
	}

	synchronized I_Message removceMessageInResponseWaitMessageQeueueByIndex(
			int i) {
		return _responseWaitMessageQueue.remove(i).getMsg();
	}

	I_Message getRequestMessageOfResponseMessage(I_Message msg) {
		I_Message msgInQeueue = null;
		for (int i = 0; i < _responseWaitMessageQueue.size(); i++) {
			msgInQeueue = _responseWaitMessageQueue.get(i).getMsg();
			if (msg instanceof I_GCP_Message
					&& msgInQeueue instanceof I_GCP_Message) {
				I_GCP_Message gcpMsg = (I_GCP_Message) msg;
				I_GCP_Message gcpMsgInQueue = (I_GCP_Message) msgInQeueue;

				if (gcpMsg.getSeqNum() == gcpMsgInQueue.getSeqNum()) {
					removceMessageInResponseWaitMessageQeueueByIndex(i);
					return (I_Message) gcpMsgInQueue;
				}
			}
		}
		return null;
	}

	Calendar lastTick = Calendar.getInstance();

	public void tick_1Sec() {
		if (lastTick.getTimeInMillis() + 1000 < System.currentTimeMillis()) {
			lastTick = Calendar.getInstance();
			handleResponseWaitMessageQueue();
			handleStateMonitoring();
			handleNetworkConnection();
			// handleSendSensorEventForTestDummy();
		}
	}

	Calendar lastSendStateMonitoringMessage = Calendar.getInstance();

	private void handleStateMonitoring() {
		if (lastSendStateMonitoringMessage.getTimeInMillis() + 60000 < System
				.currentTimeMillis()) {
			if (sensingLevel >= 0)
				sendSetSensingLevelReq(this.sensingLevel);
			lastSendStateMonitoringMessage = Calendar.getInstance();
		}
	}

	private void handleNetworkConnection() {
		if (lastMessageReceivingTime.getTimeInMillis() + 300000 < System
				.currentTimeMillis()) {
			networkState = networkState.OFFLINE;
		}
	}

	protected void handleResponseWaitMessageQueue() {
		int size = _responseWaitMessageQueue.size();
		for (int i = 0; i < size; i++)
			((ResponseWaitMessage) _responseWaitMessageQueue.get(i))
					.increaseTimer();

		for (int j = 0; j < size; j++)
			for (int i = 0; i < _responseWaitMessageQueue.size(); i++) {
				if (((ResponseWaitMessage) _responseWaitMessageQueue.get(i))
						.isTimeout()) {
					removceMessageInResponseWaitMessageQeueueByIndex(i);
					break;
				}
			}
	}
	@Override
	public I_XNetHandler getXNetHandler() {
		return this.xnetHandler;
	}
}
