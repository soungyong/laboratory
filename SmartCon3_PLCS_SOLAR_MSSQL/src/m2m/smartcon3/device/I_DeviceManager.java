/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
/**
 *	Handling of data/gateways.
 *	- load basic data from databases
 *	- list of gateways and states..
 *	- list of nodes and states.
 *	- schedule..
 *	- ?
 *
 *	// use types..
 *	ctrl_dev, ctrl_cmd type modified from int to string.
 */
package m2m.smartcon3.device;


//import java.lang.Integer;
//import m2m.util.StringUtil;

public interface I_DeviceManager
{
	public I_Device createDevice(short deviceId, short deviceType);
	public I_Device getDevice(short deviceId, short deviceType);
	public void addDevice(I_Device device);
	public void removeDevice(short deviceId, short type);	
	public void removeDevice(I_Device device); 
	public void clear();
}
