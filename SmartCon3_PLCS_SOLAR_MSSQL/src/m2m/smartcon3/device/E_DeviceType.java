package m2m.smartcon3.device;

public enum E_DeviceType {
	MGW300((short) 0x2000),
	SolarSensorGetter((short) 0x2100),

	LC100((short) 0x2010),

	MSN300((short) 0x2050),

	MDT100((short) 0x2040), 
	
	MDF100((short) 0x2060),
	
	MPR200((short) 0x2070),
	
	MIR100((short) 0x2080);

	private short shortValue = 0;

	private E_DeviceType() {
	}

	private E_DeviceType(short value) {
		this.shortValue = value;
	}

	public short toShort() {
		return shortValue;
	}

	public static E_DeviceType getByShort(short deviceType_short) {
		E_DeviceType deviceType = E_DeviceType.LC100;
		switch (deviceType_short) {
		case 0x2000:
			deviceType = E_DeviceType.MGW300;
			break;
		case 0x2010:
			deviceType = E_DeviceType.LC100;
			break;
		case 0x2040:
			deviceType = E_DeviceType.MDT100;
			break;
		case 0x2050:
			deviceType = E_DeviceType.MSN300;
			break;
		case 0x2060:
			deviceType = E_DeviceType.MDF100;
			break;
		case 0x2070:
			deviceType = E_DeviceType.MPR200;
			break;
		case 0x2080:
			deviceType = E_DeviceType.MIR100;
			break;
		case 0x2100:
			deviceType = E_DeviceType.SolarSensorGetter;
			break;
		}
		return deviceType;
	}
}
