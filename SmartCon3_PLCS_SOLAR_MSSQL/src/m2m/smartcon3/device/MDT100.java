package m2m.smartcon3.device;

import java.util.Calendar;
import java.util.Random;

import m2m.smartcon3.device.I_Device.NETWORK_STATE;
import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.event.DeviceDisconnectEvent;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.module.RemoteFirmwareUpdateHelper;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_MessageType;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_RebootReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_StateInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_StateInfoRes;
import m2m.smartcon3.protocol.gcp2_1.I_GCP_Message;
import m2m.smartcon3.protocol.rfup1_0.I_RFUP_Message;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class MDT100 implements I_Device {
	I_XNetHandler xnetHandler = null;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.MDT100;
	private int firmwareVersion;
	I_Device parent = null;

	NETWORK_STATE networkState = NETWORK_STATE.OFFLINE;
	private Calendar lastMessageReceivingTime = Calendar.getInstance();

	RemoteFirmwareUpdateHelper rfuHelper = null;

	public MDT100(I_Device parent, short id) {
		this.parent = parent;
		this.deviceId = id;

		new DeviceStateChangedEvent(this, "", null);

		MyLog.lWithSysOut("Create MDT100: " + HexUtils.toHexString(deviceId));
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		if (xnetHandler == null)
			return NETWORK_STATE.OFFLINE;
		else if (xnetHandler.isConnected() == false)
			return NETWORK_STATE.OFFLINE;
		return networkState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}

	@Override
	public void setXNetHandler(I_XNetHandler xnetHandler) {
		this.xnetHandler = xnetHandler;
	}

	private boolean sendMessage(I_Message msg) {
		if (xnetHandler == null)
			return false;
		return xnetHandler.sendMessage(msg);
	}

	@Override
	public void handleMessage(I_Message msg) {
		if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			if (gcpMsg.getMessageType() != E_GCP_MessageType.RES_DEVICEINFO) {
				lastMessageReceivingTime = Calendar.getInstance();
				networkState = NETWORK_STATE.ONLINE;
			}
		}

		if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			switch (gcpMsg.getMessageType()) {
			case RES_DEVICEINFO:
				handleDeviceInfoRes(gcpMsg);
				break;
			case REQ_DEVICEINFO:
				handleDeviceInfoReq(gcpMsg);
				break;
			case RES_STATEINFO:
				handleStateInfoRes(gcpMsg);
				break;
			case REQ_STATEINFO:
				handleStateInfoReq(gcpMsg);
				break;
			}
		} else if (msg instanceof I_RFUP_Message) {
			I_RFUP_Message rfupMsg = (I_RFUP_Message) msg;
			if (rfuHelper != null && rfuHelper.isComplete() == false)
				rfuHelper.handleResponseMessage(rfupMsg);
		}

	}

	private void handleStateInfoRes(I_GCP_Message gcpMsg) {

	}

	private void handleStateInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("MDT100_handleStateInfoReq");
		GCP_Message_StateInfoReq reqMsg = (GCP_Message_StateInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();
		short deviceId = reqMsg.getDeviceId();

		GCP_Message_StateInfoRes resMsg = null;

		try {
			resMsg = new GCP_Message_StateInfoRes(srcId, destId, subNodeId,
					deviceId, getDeviceType().toShort(), 0);
			resMsg.setSeqNum(gcpMsg.getSeqNum());
			sendMessage((I_Message) resMsg);
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
	}

	private void handleDeviceInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("MDT100_handleDeviceInfoReq");
		GCP_Message_DeviceInfoReq reqMsg = (GCP_Message_DeviceInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();
		short deviceId;
		short deviceType;
		byte deviceVersion = 0;

		I_Device device = this;

		deviceId = device.getId();
		deviceType = device.getDeviceType().toShort();

		GCP_Message_DeviceInfoRes resMsg = new GCP_Message_DeviceInfoRes(srcId,
				destId, subNodeId, deviceId, deviceType, deviceVersion);
		resMsg.setSeqNum(gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);
	}

	private void handleDeviceInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("MDT100_handleDeviceInfoRes");
		GCP_Message_DeviceInfoRes resMsg = (GCP_Message_DeviceInfoRes) gcpMsg;

		short subNodeId = resMsg.getSubNodeId();
		short deviceId = resMsg.getDeviceId();
		short deviceType = resMsg.getDeviceType();
		int firmwareVersion = resMsg.getDeviceVersion();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("deviceId: " + deviceId);
		MyLog.i("deviceType: " + HexUtils.toHexString(deviceType));
		MyLog.i("deviceVersion: " + firmwareVersion);

		this.firmwareVersion = firmwareVersion;
	}

	public void setFirmwareVersion(int version) {
		this.firmwareVersion = version;
	}

	@Override
	public I_Device getParent() {
		return parent;
	}

	public int getFirmwareVersion() {
		return this.firmwareVersion;
	}

	public boolean sendStateInfoReq() {
		MyLog.i("sendStateInfoReq");

		short subNodeId = (short) getParent().getId();
		short deviceId = (short) getId();

		I_GCP_Message msg = new GCP_Message_StateInfoReq((short) 0x00, getId(),
				subNodeId, deviceId);
		return sendMessage((I_Message) msg);
	}

	public boolean sendRebootReq() {
		MyLog.i("sendRebootReq");

		short subNodeId = (short) getParent().getId();
		short deviceId = (short) getId();

		I_GCP_Message msg = new GCP_Message_RebootReq((short) 0x0000, getId(),
				subNodeId, deviceId);
		return sendMessage((I_Message) msg);
	}

	Calendar lastTick = Calendar.getInstance();

	public void tick_1Sec() {
		if (lastTick.getTimeInMillis() + 1000 < System.currentTimeMillis()) {
			lastTick = Calendar.getInstance();
			// handleResponseWaitMessageQueue();
			handleStateMonitoring();
			handleNetworkConnection();
			// handleSendSensorEventForTestDummy();
		}
	}

	private void handleNetworkConnection() {
		if (lastMessageReceivingTime.getTimeInMillis() + 300000 < System
				.currentTimeMillis()) {
			if (networkState != networkState.OFFLINE) {
				networkState = networkState.OFFLINE;
				new DeviceDisconnectEvent(this, "", null);
			}
		}
	}

	Calendar lastSentTime_StateInfoReq = Calendar.getInstance();

	protected void handleStateMonitoring() {
		if (xnetHandler != null) {
			if (xnetHandler.isConnected()) {
				if (lastSentTime_StateInfoReq.getTimeInMillis() + 60000
						+ ((new Random()).nextInt(30000)) < Calendar
						.getInstance().getTimeInMillis()) {// 60초에 한번
															// stateInfo
															// 가져오기.
					sendStateInfoReq();
					lastSentTime_StateInfoReq = Calendar.getInstance();
				}
			}
		}
	}

	@Override
	public I_XNetHandler getXNetHandler() {
		return this.xnetHandler;
	}

	public int sendFirmwareUpgradeReq(byte[] data) {
		MyLog.l("sendFirmwareUpgradeReq");

		short subNodeId = (short) getParent().getId();
		short deviceId = (short) getId();

		if (rfuHelper == null)
			rfuHelper = new RemoteFirmwareUpdateHelper(this, data);
		else if (rfuHelper.isComplete())
			rfuHelper = new RemoteFirmwareUpdateHelper(this, data);

		return -1;
	}

}
