package m2m.smartcon3.device;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.devicenetwork.ResponseWaitMessageQueue;
import m2m.smartcon3.event.DeviceDisconnectEvent;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.module.RemoteFirmwareUpdateHelper;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ScheduleType;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleListReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleListRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ApplyingScheduleReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ApplyingScheduleRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_BoatSensorMessage;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlCircuitReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlCircuitRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlIlluSensorReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlSensorReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlSensorRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DebugLogLCReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DebugLogLCRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingSizeReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingSizeRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_IrControlRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerListReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerListRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorListReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorListRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NoticeEvent;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NoticeSensorValue;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NumOfDeviceReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NumOfDeviceRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_PowermeterRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_Powermeter_LCReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_RebootReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetDebugLogLCReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingDimmerReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingDimmerRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingSensorReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingSensorRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetScheduleReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetScheduleRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleSizeReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleSizeRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingSizeReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingSizeRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SetSensingLevelReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SetSensingLevelRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_StateInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_StateInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_TiltControlReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_TiltMessage;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_UpdatePowermeterReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_UpdatePowermeter_LCReq;
import m2m.smartcon3.protocol.gcp2_1.I_GCP_Message;
import m2m.smartcon3.protocol.ncp2_0.I_NCP_Message;
import m2m.smartcon3.protocol.ncp2_0.NCP_Message_RegisterNodeReq;
import m2m.smartcon3.protocol.ncp2_0.NCP_Message_RegisterNodeRes;
import m2m.smartcon3.protocol.rfup1_0.I_RFUP_Message;
import m2m.smartcon3.protocol.rfup1_0.RFUP_Message_SendDataRes;
import m2m.smartcon3.protocol.rfup1_0.RFUP_Message_UpgradeRes;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class LC100 implements I_Device, Runnable {
	I_XNetHandler xnetHandler = null;
	MGW300 parent = null;
	short deviceId = 0x00;
	E_DeviceType deviceType = E_DeviceType.LC100;
	int firmwareVersion = 0;
	short panId = 0xff;
	E_GCP_ScheduleType applyingSchedule = E_GCP_ScheduleType.DEFAULT;

	Hashtable<Short, I_Device> subDeviceMap = new Hashtable<Short, I_Device>();
	SynchronizedQueue<SensorMappingInfo> sensorMappingList = new SynchronizedQueue<SensorMappingInfo>();
	SynchronizedQueue<DimmerMappingInfo> dimmerMappingList = new SynchronizedQueue<DimmerMappingInfo>();

	SynchronizedQueue<ScheduleInfo> defaultScheduleList = new SynchronizedQueue<ScheduleInfo>();
	SynchronizedQueue<ScheduleInfo> todayScheduleList = new SynchronizedQueue<ScheduleInfo>();
	SynchronizedQueue<ScheduleInfo> tomorrowScheduleList = new SynchronizedQueue<ScheduleInfo>();

	E_GCP_ControlMode controlInfo[] = new E_GCP_ControlMode[16];
	E_GCP_ControlMode stateInfo[] = new E_GCP_ControlMode[16];
	private int powermeterValue = 0;

	private int rebootCount = 0;
	private int connectionCount = 0;
	private Calendar lastRebootTime = Calendar.getInstance();
	private Calendar lastConnectionTime = Calendar.getInstance();

	ResponseWaitMessageQueue responseWaitMessageQueue = new ResponseWaitMessageQueue();

	NETWORK_STATE networkState = NETWORK_STATE.OFFLINE;
	private Calendar lastMessageReceivingTime = Calendar.getInstance();

	boolean isTerminated = false;

	RemoteFirmwareUpdateHelper rfuHelper = null;

	String tiltMessage = "";
	String boatSensorMessage = "";

	public LC100(I_Device parent, short deviceId) {
		this.deviceId = deviceId;
		for (int i = 0; i < 16; i++) {
			controlInfo[i] = E_GCP_ControlMode.DEFAULT;
			stateInfo[i] = E_GCP_ControlMode.DEFAULT;
		}

		new Thread(new Runnable() {
			@Override
			public void run() {
				while (isTerminated == false) {
					if (lastMessageReceivingTime.getTimeInMillis() + 10000 < System.currentTimeMillis()) {
						if (networkState != networkState.OFFLINE) {
							networkState = networkState.OFFLINE;
							new DeviceDisconnectEvent(LC100.this, "", null);
						}
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isTerminated == false) {
					responseWaitMessageQueue.increaseTimerOfMessages();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();

		this.parent = (MGW300) parent;
		new Thread(this).start();

		// 하위 노드들 state monitoring thread
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isTerminated == false) {
					try {
						Set<Short> ids = subDeviceMap.keySet();
						for (Short id : ids) {
							I_Device device = subDeviceMap.get(id);
							if (device instanceof MSN300) {
								((MSN300) device).tick_1Sec();
							} else if (device instanceof MDT100) {
								((MDT100) device).tick_1Sec();
							} else if (device instanceof MDF100) {
								((MDF100) device).tick_1Sec();
							} else if (device instanceof MPR200) {
								((MPR200) device).tick_1Sec();
							} else if (device instanceof SolarSensorGetter) {
								((SolarSensorGetter) device).tick_1Sec();
							}
						}

						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (Exception e) {
						e.printStackTrace();
						MyLog.e(e.getMessage());
					}
				}
			}
		}).start();
		
		final LC100 me = this;

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isTerminated == false) {
					short deviceId;
					short deviceType;
					I_Device device = null;
					device = me;

					deviceId = device.getId();
					deviceType = device.getDeviceType().toShort();

					GCP_Message_DeviceInfoRes resMsg = new GCP_Message_DeviceInfoRes(srcId, destId, subNodeId, deviceId, deviceType, deviceVersion);
					resMsg.setSeqNum(gcpMsg.getSeqNum());
					sendMessage((I_Message) resMsg);
					
					try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();

		MyLog.lWithSysOut("Create LC100: " + HexUtils.toHexString(deviceId));
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		if (xnetHandler == null)
			return NETWORK_STATE.OFFLINE;
		else if (xnetHandler.isConnected() == false)
			return NETWORK_STATE.OFFLINE;
		else
			return networkState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		// TODO Auto-generated method stub
		return deviceType;
	}

	public synchronized void handleMessage(I_Message msg) {
		// 10초이상 message 응답 없으면 network state를 offline로 설정.
		lastMessageReceivingTime.setTimeInMillis(System.currentTimeMillis());
		networkState = NETWORK_STATE.ONLINE;

		if (msg instanceof I_NCP_Message) {
			I_NCP_Message ncpMsg = (I_NCP_Message) msg;
			switch (ncpMsg.getMessageType()) {
			case REQ_REGISTER_NODE:
				handleRegisterNodeReq(ncpMsg);
				break;
			}
		} else if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			switch (gcpMsg.getMessageType()) {
			case RES_NUMOFDEVICE:
				handleNumOfDeviceRes(gcpMsg);
				break;
			case REQ_NUMOFDEVICE:
				handleNumOfDeviceReq(gcpMsg);
				break;
			case RES_DEVICEINFO:
				handleDeviceInfoRes(gcpMsg);
				break;
			case REQ_DEVICEINFO:
				handleDeviceInfoReq(gcpMsg);
				break;
			case RES_STATEINFO:
				handleStateInfoRes(gcpMsg);
				break;
			case REQ_STATEINFO:
				handleStateInfoReq(gcpMsg);
				break;
			case RES_CONTROL_CIRCUIT:
				handleControlCircuitRes(gcpMsg);
				break;
			case REQ_CONTROL_CIRCUIT:
				handleControlCircuitReq(gcpMsg);
				break;
			case RES_CONTROL_SENSOR:
				handleControlSensorRes(gcpMsg);
				break;
			case REQ_CONTROL_SENSOR:
				handleControlSensorReq(gcpMsg);
				break;
			case RES_POWERMETER:
				handlePowermeterRes(gcpMsg);
				break;
			case REQ_POWERMETER:
				handlePowermeterReq(gcpMsg);
				break;
			case REQ_UPDATEPOWERMETER:
				handleUpdatePowermeter(gcpMsg);
				break;
			case RES_SENSOR_MAPPING_SIZE:
				handleSensorMappingSizeRes(gcpMsg);
				break;
			case REQ_SENSOR_MAPPING_SIZE:
				handleSensorMappingSizeReq(gcpMsg);
				break;
			case RES_DIMMER_MAPPING_SIZE:
				handleDimmerMappingSizeRes(gcpMsg);
				break;
			case REQ_DIMMER_MAPPING_SIZE:
				handleDimmerMappingSizeReq(gcpMsg);
				break;
			case RES_SCHEDULE_SIZE:
				handleScheduleSizeRes(gcpMsg);
				break;
			case REQ_SCHEDULE_SIZE:
				handleScheduleSizeReq(gcpMsg);
				break;
			case RES_SENSOR_MAPPING_INFO:
				handleSensorMappingInfoRes(gcpMsg);
				break;
			case REQ_SENSOR_MAPPING_INFO:
				handleSensorMappingInfoReq(gcpMsg);
				break;
			case RES_DIMMER_MAPPING_INFO:
				handleDimmerMappingInfoRes(gcpMsg);
				break;
			case REQ_DIMMER_MAPPING_INFO:
				handleDimmerMappingInfoReq(gcpMsg);
				break;
			case RES_SCHEDULE_INFO:
				handleScheduleInfoRes(gcpMsg);
				break;
			case REQ_SCHEDULE_INFO:
				handleScheduleInfoReq(gcpMsg);
				break;
			case RES_MAPPING_SENSOR:
				handleMappingSensorRes(gcpMsg);
				break;
			case REQ_MAPPING_SENSOR:
				handleMappingSensorReq(gcpMsg);
				break;
			case RES_MAPPING_DIMMER:
				handleMappingDimmerRes(gcpMsg);
				break;
			case REQ_MAPPING_DIMMER:
				handleMappingDimmerReq(gcpMsg);
				break;
			case RES_ADD_SCHEDULE:
				handleAddScheduleRes(gcpMsg);
				break;
			case REQ_ADD_SCHEDULE:
				handleAddScheduleReq(gcpMsg);
				break;
			case RES_MAPPING_SENSOR_LIST:
				handleMappingSensorListRes(gcpMsg);
				break;
			case REQ_MAPPING_SENSOR_LIST:
				handleMappingSensorListReq(gcpMsg);
				break;
			case RES_MAPPING_DIMMER_LIST:
				handleMappingDimmerListRes(gcpMsg);
				break;
			case REQ_MAPPING_DIMMER_LIST:
				handleMappingDimmerListReq(gcpMsg);
				break;
			case RES_ADD_SCHEDULE_LIST:
				handleAddScheduleListRes(gcpMsg);
				break;
			case REQ_ADD_SCHEDULE_LIST:
				handleAddScheduleListReq(gcpMsg);
				break;
			case RES_RESET_MAPPING_SENSOR:
				handleResetMappingSensorRes(gcpMsg);
				break;
			case REQ_RESET_MAPPING_SENSOR:
				handleResetMappingSensorReq(gcpMsg);
				break;
			case RES_RESET_MAPPING_DIMMER:
				handleResetMappingDimmerRes(gcpMsg);
				break;
			case REQ_RESET_MAPPING_DIMMER:
				handleResetMappingDimmerReq(gcpMsg);
				break;
			case RES_RESET_SCHEDULE:
				handleResetScheduleRes(gcpMsg);
				break;
			case REQ_RESET_SCHEDULE:
				handleResetScheduleReq(gcpMsg);
				break;
			case REQ_APPLYING_SCHEDULE:
				handleApplyingScheduleReq(gcpMsg);
				break;
			case RES_APPLYING_SCHEDULE:
				handleApplyingScheduleRes(gcpMsg);
				break;
			case NOTICE_EVENT:
				handleNoticeEvent(gcpMsg);
				break;
			case RES_SET_SENSING_LEVEL:
				handleSetSensingLevelRes(gcpMsg);
				break;
			case REQ_SET_SENSING_LEVEL:
				handleSetSensingLevelReq(gcpMsg);
				break;
			case RES_DEBUG_LOG_LC:
				handleDebugLogLCRes(gcpMsg);
				break;
			case REQ_DEBUG_LOG_LC:
				handleDebugLogLCReq(gcpMsg);
				break;
			case REQ_RESET_DEBUG_LOG_LC:
				handleResetDebugLogLCReq(gcpMsg);
				break;
			case NOTICE_SENSOR_VALUE:
				handleNoticeSensorValue(msg);
				break;
			case REQ_IR_CONTROL:
				// TODO : 내용 없음
				handleIrControlReq(gcpMsg);
				break;
			case RES_IR_CONTROL:
				handleIrControlRes(gcpMsg);
				break;
			case TILT_MESSAGE:
				handleTiltMessage(gcpMsg);
				break;
			case BOAT_SENSOR_MESSAGE:
				handleBoatSensorMessage(gcpMsg);
				break;

			}
		} else if (msg instanceof I_RFUP_Message) {
			I_RFUP_Message rfupMsg = (I_RFUP_Message) msg;
			if (rfuHelper != null && rfuHelper.isComplete() == false)
				rfuHelper.handleResponseMessage(rfupMsg);

			switch (rfupMsg.getMessageType()) {
			case RES_UPGRADE:
				handleUpgradeRes(rfupMsg);
				break;
			case RES_SEND_DATA:
				handleSendData(rfupMsg);
				break;
			}
		}
	}

	private void handleIrControlReq(I_GCP_Message gcpMsg) {
		// TODO Auto-generated method stub

	}

	private void handleIrControlRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleIrControlRes");
		GCP_Message_IrControlRes resMsg = (GCP_Message_IrControlRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();
		short irId = resMsg.getIrId();

		I_Device device = getDeviceById(irId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleTiltMessage(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleTiltMessage");
		GCP_Message_TiltMessage resMsg = (GCP_Message_TiltMessage) gcpMsg;
		this.tiltMessage = resMsg.getMessage();
		System.out.println("TILT MESSAGE : " + tiltMessage);
	}

	private void handleBoatSensorMessage(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleBoatSensorMessage");
		GCP_Message_BoatSensorMessage resMsg = (GCP_Message_BoatSensorMessage) gcpMsg;
		this.boatSensorMessage = resMsg.getMessage();
		System.out.println("BOAT SENSOR MESSAGE : " + boatSensorMessage);
	}

	private void handleApplyingScheduleRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleApplyingScheduleRes");
		GCP_Message_ApplyingScheduleRes resMsg = (GCP_Message_ApplyingScheduleRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null) {

			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

			GCP_Message_ApplyingScheduleReq reqMsg = (GCP_Message_ApplyingScheduleReq) reqIMsg;
			E_GCP_ScheduleType scheduleType = reqMsg.getScheduleType();

			applyingSchedule = scheduleType;
		}

		new DeviceStateChangedEvent(this, "", null);
	}

	private void handleApplyingScheduleReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleApplyingScheduleReq");
		GCP_Message_ApplyingScheduleReq reqMsg = (GCP_Message_ApplyingScheduleReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		E_GCP_ScheduleType scheduleType = reqMsg.getScheduleType();

		if (scheduleType != E_GCP_ScheduleType.DEFAULT)
			applyingSchedule = scheduleType;

		GCP_Message_ApplyingScheduleRes resMsg = new GCP_Message_ApplyingScheduleRes(destId, srcId, subNodeId, applyingSchedule);
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);

	}

	private void handleSendData(I_RFUP_Message rfupMsg) {
		MyLog.i("handleSendData");
		RFUP_Message_SendDataRes recvMsg = (RFUP_Message_SendDataRes) rfupMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		MyLog.i("DeviceType: " + recvMsg.getDeviceType());

		if (recvMsg.getDeviceType() == E_DeviceType.MSN300) {
			MSN300 device = (MSN300) (getDeviceById(recvMsg.getDeviceId()));
			if (device != null)
				device.handleMessage((I_Message) recvMsg);
		} else if (recvMsg.getDeviceType() == E_DeviceType.MDT100) {
			MDT100 device = (MDT100) (getDeviceById(recvMsg.getDeviceId()));
			if (device != null)
				device.handleMessage((I_Message) recvMsg);
		} else if (recvMsg.getDeviceType() == E_DeviceType.MDF100) {
			MDF100 device = (MDF100) (getDeviceById(recvMsg.getDeviceId()));
			if (device != null)
				device.handleMessage((I_Message) recvMsg);
		} else if (recvMsg.getDeviceType() == E_DeviceType.MPR200) {
			MPR200 device = (MPR200) (getDeviceById(recvMsg.getDeviceId()));
			if (device != null)
				device.handleMessage((I_Message) recvMsg);
		} else if (recvMsg.getDeviceType() == E_DeviceType.LC100) {
			//
		}
	}

	private void handleUpgradeRes(I_RFUP_Message rfupMsg) {
		MyLog.i("handleUpgradeRes");
		RFUP_Message_UpgradeRes recvMsg = (RFUP_Message_UpgradeRes) rfupMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		MyLog.i("DeviceType: " + recvMsg.getDeviceType());

		if (recvMsg.getDeviceType() == E_DeviceType.MSN300) {
			MSN300 device = (MSN300) (getDeviceById(recvMsg.getDeviceId()));
			if (device != null)
				device.handleMessage((I_Message) recvMsg);
		} else if (recvMsg.getDeviceType() == E_DeviceType.MDT100) {
			MDT100 device = (MDT100) (getDeviceById(recvMsg.getDeviceId()));
			if (device != null)
				device.handleMessage((I_Message) recvMsg);
		} else if (recvMsg.getDeviceType() == E_DeviceType.MDF100) {
			MDF100 device = (MDF100) (getDeviceById(recvMsg.getDeviceId()));
			if (device != null)
				device.handleMessage((I_Message) recvMsg);
		} else if (recvMsg.getDeviceType() == E_DeviceType.MPR200) {
			MPR200 device = (MPR200) (getDeviceById(recvMsg.getDeviceId()));
			if (device != null)
				device.handleMessage((I_Message) recvMsg);
		} else if (recvMsg.getDeviceType() == E_DeviceType.LC100) {
			//
		}
	}

	private void handleNoticeSensorValue(I_Message msg) {
		MyLog.i("handleNoticeSensorValue");
		GCP_Message_NoticeSensorValue recvMsg = (GCP_Message_NoticeSensorValue) msg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = recvMsg.getSubNodeId();
		short sensorId = recvMsg.getSensorId();

		I_Device device = getDeviceById(sensorId);
		if (device != null)
			device.handleMessage((I_Message) msg);
	}

	private void handleRegisterNodeReq(I_NCP_Message ncpMsg) {
		MyLog.i("LC_handleRegisterNodeReq");
		NCP_Message_RegisterNodeReq reqMsg = ((NCP_Message_RegisterNodeReq) ncpMsg);
		short deviceType = reqMsg.getDeviceType();
		if (deviceType == E_DeviceType.MSN300.toShort()) {
			MSN300 msn = (MSN300) getDeviceById(reqMsg.getDeviceId());
			if (msn == null) {
				msn = new MSN300(this, reqMsg.getDeviceId());
				addDevice(msn);
				msn.setFirmwareVersion(reqMsg.getFirmwareVersion());
			}
			msn.setFirmwareVersion(reqMsg.getFirmwareVersion());
		} else if (deviceType == E_DeviceType.MDT100.toShort()) {
			MDT100 mdt = (MDT100) getDeviceById(reqMsg.getDeviceId());
			if (mdt == null) {
				mdt = new MDT100(this, reqMsg.getDeviceId());
				addDevice(mdt);
				mdt.setFirmwareVersion(reqMsg.getFirmwareVersion());
			}
			mdt.setFirmwareVersion(reqMsg.getFirmwareVersion());
		} else if (deviceType == E_DeviceType.MDF100.toShort()) {
			MDF100 mdf = (MDF100) getDeviceById(reqMsg.getDeviceId());
			if (mdf == null) {
				mdf = new MDF100(this, reqMsg.getDeviceId());
				addDevice(mdf);
				mdf.setFirmwareVersion(reqMsg.getFirmwareVersion());
			}
			mdf.setFirmwareVersion(reqMsg.getFirmwareVersion());
		} else if (deviceType == E_DeviceType.MPR200.toShort()) {
			MPR200 mpr = (MPR200) getDeviceById(reqMsg.getDeviceId());
			if (mpr == null) {
				mpr = new MPR200(this, reqMsg.getDeviceId());
				addDevice(mpr);
				mpr.setFirmwareVersion(reqMsg.getFirmwareVersion());
			}
		} else if (deviceType == E_DeviceType.MIR100.toShort()) {
			MIR100 mir = (MIR100) getDeviceById(reqMsg.getDeviceId());
			if (mir == null) {
				mir = new MIR100(this, reqMsg.getDeviceId());
				addDevice(mir);
				mir.setFirmwareVersion(reqMsg.getFirmwareVersion());
			}
		} else if (deviceType == E_DeviceType.SolarSensorGetter.toShort()) {
			SolarSensorGetter ssg = (SolarSensorGetter) getDeviceById(reqMsg.getDeviceId());
			if (ssg == null) {
				ssg = new SolarSensorGetter(this, reqMsg.getDeviceId());
				addDevice(ssg);
				ssg.setFirmwareVersion(reqMsg.getFirmwareVersion());
			}
			ssg.setFirmwareVersion(reqMsg.getFirmwareVersion());
		} else if (deviceType == E_DeviceType.LC100.toShort()) {
			this.firmwareVersion = reqMsg.getFirmwareVersion();
			new DeviceStateChangedEvent(this, "", null);

			NCP_Message_RegisterNodeRes resMsg = new NCP_Message_RegisterNodeRes(ncpMsg.getDestId(), ncpMsg.getSrcId(), reqMsg.getSubNodeId(), deviceType, 0x00);
			resMsg.setSeqNum(ncpMsg.getSeqNum());
			sendMessage(resMsg);

		}
	}

	private void handleResetDebugLogLCReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleResetDebugLogReq");
		this.rebootCount = 0;
		this.connectionCount = 0;
		this.lastRebootTime = Calendar.getInstance();
		this.lastConnectionTime = Calendar.getInstance();
	}

	private void handleDebugLogLCReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleDebugLogLCReq");
		GCP_Message_DebugLogLCReq reqMsg = (GCP_Message_DebugLogLCReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		MyLog.i("subNodeId: " + subNodeId);

		int rebootCount = 0;
		byte[] lastRebootTime = new byte[5];
		int serverConnectionCount = 0;
		byte[] lastServerConnectionTime = new byte[5];

		rebootCount = this.rebootCount;
		serverConnectionCount = this.connectionCount;

		lastRebootTime[0] = new Integer(this.lastRebootTime.get(Calendar.HOUR)).byteValue();
		lastRebootTime[1] = new Integer(this.lastRebootTime.get(Calendar.MINUTE)).byteValue();

		lastServerConnectionTime[0] = new Integer(this.lastConnectionTime.get(Calendar.HOUR)).byteValue();
		lastServerConnectionTime[1] = new Integer(this.lastConnectionTime.get(Calendar.MINUTE)).byteValue();

		GCP_Message_DebugLogLCRes resMsg = new GCP_Message_DebugLogLCRes(destId, srcId, subNodeId, rebootCount, lastRebootTime, serverConnectionCount, lastServerConnectionTime);
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleDebugLogLCRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleDebugLogLCRes");
		GCP_Message_DebugLogLCRes resMsg = (GCP_Message_DebugLogLCRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		int rebootCount = resMsg.getRebootCount();
		byte[] lastRebootTime = resMsg.getLastRebootTime();
		int serverConnectionCount = resMsg.getServerConnectionCount();
		byte[] lastServerConnectionTime = resMsg.getLastServerConnectionTime();

		String str = "";
		MyLog.i("rebootCount: " + rebootCount);
		str = "";
		for (int i = 0; i < lastRebootTime.length; i++)
			str += lastRebootTime[i];
		MyLog.i("lastRebootTime: " + str);
		MyLog.i("serverConnectionCount: " + serverConnectionCount);
		str = "";
		for (int i = 0; i < lastServerConnectionTime.length; i++)
			str += lastServerConnectionTime[i];
		MyLog.i("lastServerConnectionTime: " + str);

		this.rebootCount = rebootCount;
		this.connectionCount = serverConnectionCount;
		;
		this.lastRebootTime.set(Calendar.HOUR_OF_DAY, new Integer(lastRebootTime[0]));
		this.lastRebootTime.set(Calendar.MINUTE, new Integer(lastRebootTime[1]));

		this.lastConnectionTime.set(Calendar.HOUR_OF_DAY, new Integer(lastServerConnectionTime[0]));
		this.lastConnectionTime.set(Calendar.MINUTE, new Integer(lastServerConnectionTime[1]));

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null)
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

	}

	private void handleSetSensingLevelReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleSetSensingLevelReq");
		GCP_Message_SetSensingLevelReq reqMsg = (GCP_Message_SetSensingLevelReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		short sensorId = reqMsg.getSensorId();

		I_Device device = getDeviceById(sensorId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleSetSensingLevelRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleSetSensingLevelRes");
		GCP_Message_SetSensingLevelRes resMsg = (GCP_Message_SetSensingLevelRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();
		short sensorId = resMsg.getSensorId();

		I_Device device = getDeviceById(sensorId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleNoticeEvent(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleNoticeEvent");
		GCP_Message_NoticeEvent reqMsg = (GCP_Message_NoticeEvent) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();

		short sensorId = reqMsg.getSensorId();
		// event 생성 필요.
		I_Device device = getDeviceById(sensorId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleResetScheduleReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetScheduleReq");
		GCP_Message_ResetScheduleReq reqMsg = (GCP_Message_ResetScheduleReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		byte circuitId = reqMsg.getCircuitId();
		E_GCP_ScheduleType scheduleType = reqMsg.getScheduleType();

		SynchronizedQueue<ScheduleInfo> scheduleInfoList = null;
		if (scheduleType == E_GCP_ScheduleType.DEFAULT)
			scheduleInfoList = this.defaultScheduleList;
		else if (scheduleType == E_GCP_ScheduleType.TODAY)
			scheduleInfoList = this.todayScheduleList;
		else if (scheduleType == E_GCP_ScheduleType.TOMORROW)
			scheduleInfoList = this.tomorrowScheduleList;

		if (circuitId == (byte) 0xff)
			scheduleInfoList.clear();
		else {
			for (int i = 0; i < scheduleInfoList.getSize(); i++) {
				ScheduleInfo info = scheduleInfoList.get(i);
				if (info != null)
					if (new Integer(info.getCircuitId()).byteValue() == circuitId) {
						scheduleInfoList.remove(info);
						i--;
					}
			}
		}

		GCP_Message_ResetScheduleRes resMsg = new GCP_Message_ResetScheduleRes(destId, srcId, subNodeId, E_GCP_Result.SUCCESS);
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);

	}

	private void handleResetScheduleRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetScheduleRes");
		GCP_Message_ResetScheduleRes resMsg = (GCP_Message_ResetScheduleRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null) {

			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

			GCP_Message_ResetScheduleReq reqMsg = (GCP_Message_ResetScheduleReq) reqIMsg;
			byte circuitId = reqMsg.getCircuitId();
			E_GCP_ScheduleType scheduleType = reqMsg.getScheduleType();

			SynchronizedQueue<ScheduleInfo> scheduleInfoList = null;

			if (scheduleType == E_GCP_ScheduleType.DEFAULT)
				scheduleInfoList = this.defaultScheduleList;
			else if (scheduleType == E_GCP_ScheduleType.TODAY)
				scheduleInfoList = this.todayScheduleList;
			else if (scheduleType == E_GCP_ScheduleType.TOMORROW)
				scheduleInfoList = this.tomorrowScheduleList;

			if (circuitId == (byte) 0xff)
				scheduleInfoList.clear();
			else {
				for (int i = 0; i < scheduleInfoList.getSize(); i++) {
					ScheduleInfo info = scheduleInfoList.get(i);
					if (info != null)
						if (new Integer(info.getCircuitId()).byteValue() == circuitId) {
							scheduleInfoList.remove(info);
							i--;
						}
				}
			}
		}

		new DeviceStateChangedEvent(this, "", null);
	}

	private void handleResetMappingDimmerReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleResetMappingDimmerReq");
		GCP_Message_ResetMappingDimmerReq reqMsg = (GCP_Message_ResetMappingDimmerReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		byte circuitId = reqMsg.getCircuitId();

		if (circuitId == (byte) 0xff)
			dimmerMappingList.clear();
		else {
			for (int i = 0; i < dimmerMappingList.getSize(); i++) {
				DimmerMappingInfo info = dimmerMappingList.get(i);
				if (info != null)
					if (new Integer(info.getCircuitId()).byteValue() == circuitId) {
						dimmerMappingList.remove(info);
						i--;
					}
			}
		}

		GCP_Message_ResetMappingDimmerRes resMsg = new GCP_Message_ResetMappingDimmerRes(destId, srcId, subNodeId, E_GCP_Result.SUCCESS);
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);

	}

	private void handleResetMappingDimmerRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleResetMappingDimmerRes");
		GCP_Message_ResetMappingDimmerRes resMsg = (GCP_Message_ResetMappingDimmerRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null) {
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

			GCP_Message_ResetMappingDimmerReq reqMsg = (GCP_Message_ResetMappingDimmerReq) reqIMsg;
			byte circuitId = reqMsg.getCircuitId();

			if (circuitId == (byte) 0xff)
				dimmerMappingList.clear();
			else {
				for (int i = 0; i < dimmerMappingList.getSize(); i++) {
					DimmerMappingInfo info = dimmerMappingList.get(i);
					if (info != null)
						if (new Integer(info.getCircuitId()).byteValue() == circuitId) {
							dimmerMappingList.remove(info);
							i--;
						}
				}
			}
		}

		new DeviceStateChangedEvent(this, "", null);
	}

	private void handleResetMappingSensorReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleResetMappingSensorReq");
		GCP_Message_ResetMappingSensorReq reqMsg = (GCP_Message_ResetMappingSensorReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		byte circuitId = reqMsg.getCircuitId();

		if (circuitId == (byte) 0xff)
			sensorMappingList.clear();
		else {
			for (int i = 0; i < sensorMappingList.getSize(); i++) {
				SensorMappingInfo info = sensorMappingList.get(i);
				if (info != null)
					if (new Integer(info.getCircuitId()).byteValue() == circuitId) {
						sensorMappingList.remove(info);
						i--;
					}
			}
		}

		GCP_Message_ResetMappingSensorRes resMsg = new GCP_Message_ResetMappingSensorRes(destId, srcId, subNodeId, E_GCP_Result.SUCCESS);
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleResetMappingSensorRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleResetMappingSensorRes");
		GCP_Message_ResetMappingSensorRes resMsg = (GCP_Message_ResetMappingSensorRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null) {
			GCP_Message_ResetMappingSensorReq reqMsg = (GCP_Message_ResetMappingSensorReq) reqIMsg;
			byte circuitId = reqMsg.getCircuitId();

			if (circuitId == (byte) 0xff)
				sensorMappingList.clear();
			else {
				for (int i = 0; i < sensorMappingList.getSize(); i++) {
					SensorMappingInfo info = sensorMappingList.get(i);
					if (info != null)
						if (new Integer(info.getCircuitId()).byteValue() == circuitId) {
							sensorMappingList.remove(info);
							i--;
						}
				}
			}

			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);
		}
		new DeviceStateChangedEvent(this, "", null);
	}

	private void handleMappingDimmerListReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingDimmerListReq");
		GCP_Message_MappingDimmerListReq reqMsg = (GCP_Message_MappingDimmerListReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		int numOfDimmers = reqMsg.getNumOfDimmers();
		short dimmerId[] = reqMsg.getDimmerIds();
		int channelId[] = reqMsg.getChannelIds();
		int circuitId = reqMsg.getCircuitId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("numOfDimmers: " + numOfDimmers);
		MyLog.i("circuitId: " + circuitId);

		E_GCP_Result result = E_GCP_Result.SUCCESS;
		for (int i = 0; i < numOfDimmers; i++)
			if (isContainDimmerInMappingTable(dimmerId[i], channelId[i]))
				result = E_GCP_Result.FAIL_ALREADYMAPPED;

		if (result == E_GCP_Result.SUCCESS) {
			for (int i = 0; i < numOfDimmers; i++)
				addDimmerMappingInfo(new DimmerMappingInfo(dimmerId[i], channelId[i], circuitId));

			GCP_Message_MappingDimmerListRes resMsg = new GCP_Message_MappingDimmerListRes(destId, srcId, subNodeId, result);
			resMsg.setSeqNum(reqMsg.getSeqNum());
			sendMessage(resMsg);
		}
	}

	private void handleMappingDimmerListRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingDimmerListRes");
		GCP_Message_MappingDimmerListRes resMsg = (GCP_Message_MappingDimmerListRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_MappingDimmerListReq) {
			GCP_Message_MappingDimmerListReq reqMsg = (GCP_Message_MappingDimmerListReq) reqIMsg;
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);
			short dimmerId[] = reqMsg.getDimmerIds();
			int channelId[] = reqMsg.getChannelIds();
			int numOfDimmers = reqMsg.getNumOfDimmers();
			int circuitId = reqMsg.getCircuitId();

			if (resMsg.getResult() == E_GCP_Result.SUCCESS) {
				for (int i = 0; i < numOfDimmers; i++)
					addDimmerMappingInfo(new DimmerMappingInfo(dimmerId[i], channelId[i], circuitId));
				new DeviceStateChangedEvent(this, "", null);
			}
		}

	}

	private void handleMappingSensorListReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingSensorListReq");
		GCP_Message_MappingSensorListReq reqMsg = (GCP_Message_MappingSensorListReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short lcId = reqMsg.getLcId();
		short subNodeId = reqMsg.getSubNodeId();
		int numOfSensors = reqMsg.getNumOfSensors();
		short sensorId[] = reqMsg.getSensorIds();
		int circuitId = reqMsg.getCircuitId();
		int onTime = reqMsg.getOnTime();
		E_GCP_ControlMode onCtrlMode = reqMsg.getOnDimmingCtrl();
		E_GCP_ControlMode offCtrlMode = reqMsg.getOffDimmingCtrl();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("numOfSensors: " + numOfSensors);
		MyLog.i("circuitId: " + circuitId);
		MyLog.i("onTime: " + onTime);
		MyLog.i("onCtrlMode: " + onCtrlMode);
		MyLog.i("offCtrlMode: " + offCtrlMode);

		GCP_Message_MappingSensorListRes resMsg = null;
		E_GCP_Result result = E_GCP_Result.SUCCESS;

		if (getSensorMappingTableSize() + numOfSensors >= 256)
			result = E_GCP_Result.FAIL;
		else
			for (int i = 0; i < numOfSensors; i++) {
				if (updateSensorMappingInfo(new SensorMappingInfo(lcId, sensorId[i], circuitId, onTime, onCtrlMode, offCtrlMode)) == true) {
					result = E_GCP_Result.UPDATE;
				} else {
					addSensorMappingInfo(new SensorMappingInfo(lcId, sensorId[i], circuitId, onTime, onCtrlMode, offCtrlMode));
				}
			}

		resMsg = new GCP_Message_MappingSensorListRes(destId, srcId, subNodeId, result);
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleMappingSensorListRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingSensorListRes");
		GCP_Message_MappingSensorListRes resMsg = (GCP_Message_MappingSensorListRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_MappingSensorListReq) {
			GCP_Message_MappingSensorListReq reqMsg = (GCP_Message_MappingSensorListReq) reqIMsg;
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);
			short lcId = reqMsg.getLcId();
			short sensorId[] = reqMsg.getSensorIds();
			int numOfSensors = reqMsg.getNumOfSensors();
			int onTime = reqMsg.getOnTime();
			int circuitId = reqMsg.getCircuitId();
			E_GCP_ControlMode onCtrlMode = reqMsg.getOnDimmingCtrl();
			E_GCP_ControlMode offCtrlMode = reqMsg.getOffDimmingCtrl();

			if (resMsg.getResult() == E_GCP_Result.UPDATE || resMsg.getResult() == E_GCP_Result.SUCCESS) {
				for (int i = 0; i < numOfSensors; i++)
					if (updateSensorMappingInfo(new SensorMappingInfo(lcId, sensorId[i], circuitId, onTime, onCtrlMode, offCtrlMode)) == false)
						addSensorMappingInfo(new SensorMappingInfo(lcId, sensorId[i], circuitId, onTime, onCtrlMode, offCtrlMode));
				new DeviceStateChangedEvent(this, "", null);
			}
		}
	}

	private void handleAddScheduleListRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleAddScheduleListRes");
		GCP_Message_AddScheduleListRes resMsg = (GCP_Message_AddScheduleListRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_AddScheduleListReq) {
			GCP_Message_AddScheduleListReq reqMsg = (GCP_Message_AddScheduleListReq) reqIMsg;
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

			if (resMsg.getResult() == E_GCP_Result.SUCCESS) {
				int numOfSchedule = reqMsg.getNumOfSchedule();
				int circuitId = reqMsg.getCircuitId();
				E_GCP_ScheduleType scheduleType = reqMsg.getScheduleType();

				List<ScheduleInfo> newInfoList = new ArrayList<ScheduleInfo>();
				for (int i = 0; i < numOfSchedule; i++) {
					newInfoList.add(new ScheduleInfo(circuitId, reqMsg.getOnTimeList().get(i), reqMsg.getCtrlModeList().get(i)));
				}
				for (int i = 0; i < numOfSchedule; i++) {
					ScheduleInfo newInfo = newInfoList.get(i);
					if (scheduleType == E_GCP_ScheduleType.DEFAULT) {
						addDefaultSchedule(newInfo);
					} else if (scheduleType == E_GCP_ScheduleType.TODAY) {

						addTodaySchedule(newInfo);
					} else if (scheduleType == E_GCP_ScheduleType.TOMORROW) {

						addTomorrowSchedule(newInfo);
					}
				}
				new DeviceStateChangedEvent(this, "", null);
			}
		}

	}

	private void handleAddScheduleListReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleAddScheduleListReq");
		GCP_Message_AddScheduleListReq reqMsg = (GCP_Message_AddScheduleListReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		E_GCP_ScheduleType scheduleType = reqMsg.getScheduleType();
		int circuitId = reqMsg.getCircuitId();
		int numOfSchedule = reqMsg.getNumOfSchedule();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("scheduleType: " + scheduleType);
		MyLog.i("circuitId: " + circuitId);

		List<ScheduleInfo> newInfoList = new ArrayList<ScheduleInfo>();
		for (int i = 0; i < numOfSchedule; i++) {
			newInfoList.add(new ScheduleInfo(circuitId, reqMsg.getOnTimeList().get(i), reqMsg.getCtrlModeList().get(i)));
		}
		boolean isUpdated = false;
		boolean result = true;

		if (scheduleType == E_GCP_ScheduleType.DEFAULT) {
			if (defaultScheduleList.getSize() + numOfSchedule >= 256)
				result = false;
		} else if (scheduleType == E_GCP_ScheduleType.TODAY) {
			if (todayScheduleList.getSize() + numOfSchedule >= 256)
				result = false;
		} else if (scheduleType == E_GCP_ScheduleType.TOMORROW) {
			if (tomorrowScheduleList.getSize() + numOfSchedule >= 256)
				result = false;
		}

		if (result == true)
			for (int i = 0; i < numOfSchedule; i++) {
				ScheduleInfo newInfo = newInfoList.get(i);
				if (scheduleType == E_GCP_ScheduleType.DEFAULT) {
					if (isContainDefaultSchedule(newInfo) == true)
						isUpdated = true;
					addDefaultSchedule(newInfo);
				} else if (scheduleType == E_GCP_ScheduleType.TODAY) {
					if (isContainTodaySchedule(newInfo) == true)
						isUpdated = true;
					addTodaySchedule(newInfo);
				} else if (scheduleType == E_GCP_ScheduleType.TOMORROW) {
					if (isContainTomorrowSchedule(newInfo) == true)
						isUpdated = true;
					addTomorrowSchedule(newInfo);
				}
			}

		if (result) {
			GCP_Message_AddScheduleListRes resMsg = null;
			if (isUpdated)
				resMsg = new GCP_Message_AddScheduleListRes(destId, srcId, subNodeId, E_GCP_Result.UPDATE);
			else
				resMsg = new GCP_Message_AddScheduleListRes(destId, srcId, subNodeId, E_GCP_Result.SUCCESS);
			resMsg.setSeqNum(reqMsg.getSeqNum());
			sendMessage(resMsg);
		} else {
			GCP_Message_AddScheduleListRes resMsg = new GCP_Message_AddScheduleListRes(destId, srcId, subNodeId, E_GCP_Result.FAIL);
			resMsg.setSeqNum(reqMsg.getSeqNum());
			sendMessage(resMsg);
		}

	}

	private void handleAddScheduleReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleAddScheduleReq");
		GCP_Message_AddScheduleReq reqMsg = (GCP_Message_AddScheduleReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		E_GCP_ScheduleType scheduleType = reqMsg.getScheduleType();
		int circuitId = reqMsg.getCircuitId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("scheduleType: " + scheduleType);
		MyLog.i("circuitId: " + circuitId);

		ScheduleInfo newInfo = new ScheduleInfo(circuitId, reqMsg.getOnTime(), reqMsg.getCtrlMode());
		boolean isUpdated = false;
		boolean result = false;

		if (scheduleType == E_GCP_ScheduleType.DEFAULT) {
			if (isContainDefaultSchedule(newInfo) == true)
				isUpdated = true;
			result = addDefaultSchedule(newInfo);
		} else if (scheduleType == E_GCP_ScheduleType.TODAY) {
			if (isContainTodaySchedule(newInfo) == true)
				isUpdated = true;
			result = addTodaySchedule(newInfo);
		} else if (scheduleType == E_GCP_ScheduleType.TOMORROW) {
			if (isContainTomorrowSchedule(newInfo) == true)
				isUpdated = true;
			result = addTomorrowSchedule(newInfo);
		}

		if (result) {
			GCP_Message_AddScheduleRes resMsg = null;
			if (isUpdated)
				resMsg = new GCP_Message_AddScheduleRes(destId, srcId, subNodeId, E_GCP_Result.UPDATE);
			else
				resMsg = new GCP_Message_AddScheduleRes(destId, srcId, subNodeId, E_GCP_Result.SUCCESS);
			resMsg.setSeqNum(reqMsg.getSeqNum());
			sendMessage(resMsg);
		} else {
			GCP_Message_AddScheduleRes resMsg = new GCP_Message_AddScheduleRes(destId, srcId, subNodeId, E_GCP_Result.FAIL);
			resMsg.setSeqNum(reqMsg.getSeqNum());
			sendMessage(resMsg);
		}

	}

	private void handleAddScheduleRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleAddScheduleRes");
		GCP_Message_AddScheduleRes resMsg = (GCP_Message_AddScheduleRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_AddScheduleReq) {
			GCP_Message_AddScheduleReq reqMsg = (GCP_Message_AddScheduleReq) reqIMsg;
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

			if (resMsg.getResult() == E_GCP_Result.SUCCESS) {
				E_GCP_ScheduleType scheduleType = reqMsg.getScheduleType();
				ScheduleInfo newInfo = new ScheduleInfo(reqMsg.getCircuitId(), reqMsg.getOnTime(), reqMsg.getCtrlMode());

				if (scheduleType == E_GCP_ScheduleType.DEFAULT) {
					addDefaultSchedule(newInfo);
				} else if (scheduleType == E_GCP_ScheduleType.TODAY) {
					addTodaySchedule(newInfo);
				} else if (scheduleType == E_GCP_ScheduleType.TOMORROW) {
					addTomorrowSchedule(newInfo);
				}
				new DeviceStateChangedEvent(this, "", null);
			}
		}
	}

	private void handleMappingDimmerReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingDimmerReq");
		GCP_Message_MappingDimmerReq reqMsg = (GCP_Message_MappingDimmerReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		short dimmerId = reqMsg.getDimmerId();
		int channelId = reqMsg.getChannelId();
		int circuitId = reqMsg.getCircuitId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("sensorId: " + dimmerId);
		MyLog.i("circuitId: " + circuitId);

		if (isContainDimmerInMappingTable(dimmerId, channelId)) {
			GCP_Message_MappingDimmerRes resMsg = new GCP_Message_MappingDimmerRes(destId, srcId, subNodeId, E_GCP_Result.FAIL_ALREADYMAPPED);
			resMsg.setSeqNum(reqMsg.getSeqNum());
			sendMessage(resMsg);
			return;
		}

		boolean result = addDimmerMappingInfo(new DimmerMappingInfo(dimmerId, channelId, circuitId));

		if (result) {
			GCP_Message_MappingDimmerRes resMsg = new GCP_Message_MappingDimmerRes(destId, srcId, subNodeId, E_GCP_Result.SUCCESS);
			resMsg.setSeqNum(reqMsg.getSeqNum());
			sendMessage(resMsg);
		} else {
			GCP_Message_MappingDimmerRes resMsg = new GCP_Message_MappingDimmerRes(destId, srcId, subNodeId, E_GCP_Result.FAIL);
			resMsg.setSeqNum(reqMsg.getSeqNum());
			sendMessage(resMsg);
		}

	}

	private void handleMappingDimmerRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingDimmerRes");
		GCP_Message_MappingDimmerRes resMsg = (GCP_Message_MappingDimmerRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_MappingDimmerReq) {
			GCP_Message_MappingDimmerReq reqMsg = (GCP_Message_MappingDimmerReq) reqIMsg;
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);
			if (resMsg.getResult() == E_GCP_Result.SUCCESS) {
				addDimmerMappingInfo(new DimmerMappingInfo(reqMsg.getDimmerId(), reqMsg.getChannelId(), reqMsg.getCircuitId()));
				new DeviceStateChangedEvent(this, "", null);
			}
		}
	}

	private void handleMappingSensorReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingSensorReq");
		GCP_Message_MappingSensorReq reqMsg = (GCP_Message_MappingSensorReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short lcId = reqMsg.getLcId();
		short subNodeId = reqMsg.getSubNodeId();
		short sensorId = reqMsg.getSensorId();
		int circuitId = reqMsg.getCircuitId();
		int onTime = reqMsg.getOnTime();
		E_GCP_ControlMode onCtrlMode = reqMsg.getOnDimmingCtrl();
		E_GCP_ControlMode offCtrlMode = reqMsg.getOffDimmingCtrl();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("sensorId: " + sensorId);
		MyLog.i("circuitId: " + circuitId);
		MyLog.i("onTime: " + onTime);
		MyLog.i("onCtrlMode: " + onCtrlMode);
		MyLog.i("offCtrlMode: " + offCtrlMode);
		if (updateSensorMappingInfo(new SensorMappingInfo(lcId, sensorId, circuitId, onTime, onCtrlMode, offCtrlMode)) == false) {
			boolean result = addSensorMappingInfo(new SensorMappingInfo(lcId, sensorId, circuitId, onTime, onCtrlMode, offCtrlMode));
			if (result) {
				GCP_Message_MappingSensorRes resMsg = new GCP_Message_MappingSensorRes(destId, srcId, subNodeId, E_GCP_Result.SUCCESS);
				resMsg.setSeqNum(reqMsg.getSeqNum());
				sendMessage(resMsg);
			} else {
				GCP_Message_MappingSensorRes resMsg = new GCP_Message_MappingSensorRes(destId, srcId, subNodeId, E_GCP_Result.FAIL);
				resMsg.setSeqNum(reqMsg.getSeqNum());
				sendMessage(resMsg);
			}
		} else {
			GCP_Message_MappingSensorRes resMsg = new GCP_Message_MappingSensorRes(destId, srcId, subNodeId, E_GCP_Result.UPDATE);
			resMsg.setSeqNum(reqMsg.getSeqNum());
			sendMessage(resMsg);
		}
	}

	private void handleMappingSensorRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingSensorRes");
		GCP_Message_MappingSensorRes resMsg = (GCP_Message_MappingSensorRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());
		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_MappingSensorReq) {
			GCP_Message_MappingSensorReq reqMsg = (GCP_Message_MappingSensorReq) reqIMsg;
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);
			if (resMsg.getResult() == E_GCP_Result.UPDATE || resMsg.getResult() == E_GCP_Result.SUCCESS) {
				SensorMappingInfo info = new SensorMappingInfo(reqMsg.getLcId(), reqMsg.getSensorId(), reqMsg.getCircuitId(), reqMsg.getOnTime(), reqMsg.getOnDimmingCtrl(), reqMsg.getOffDimmingCtrl());
				if (updateSensorMappingInfo(info) == false)
					addSensorMappingInfo(info);

				new DeviceStateChangedEvent(this, "", null);
			}
		}
	}

	private void handleScheduleInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleScheduleInfoReq");
		GCP_Message_ScheduleInfoReq reqMsg = (GCP_Message_ScheduleInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		E_GCP_ScheduleType scheduleType = reqMsg.getScheduleType();
		int index = reqMsg.getIndex();

		ScheduleInfo scheduleInfo = null;
		if (scheduleType == E_GCP_ScheduleType.DEFAULT)
			scheduleInfo = getDefaultSchedule(index);
		else if (scheduleType == E_GCP_ScheduleType.TODAY)
			scheduleInfo = getTodaySchedule(index);
		else if (scheduleType == E_GCP_ScheduleType.TOMORROW)
			scheduleInfo = getTomorrowSchedule(index);

		if (scheduleInfo == null)
			return;

		GCP_Message_ScheduleInfoRes resMsg = new GCP_Message_ScheduleInfoRes(destId, srcId, subNodeId, scheduleType, scheduleInfo.getCircuitId(), scheduleInfo.getOnTime(), scheduleInfo.getCtrlMode());
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	public ScheduleInfo getTomorrowSchedule(int index) {
		return tomorrowScheduleList.get(index);
	}

	public int getTomorrowScheduleSize() {
		return this.tomorrowScheduleList.getSize();
	}

	public ScheduleInfo getTodaySchedule(int index) {
		return todayScheduleList.get(index);
	}

	public int getTodayScheduleSize() {
		return this.todayScheduleList.getSize();
	}

	public ScheduleInfo getDefaultSchedule(int index) {
		return defaultScheduleList.get(index);
	}

	private void handleScheduleInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleScheduleInfoRes");
		GCP_Message_ScheduleInfoRes resMsg = (GCP_Message_ScheduleInfoRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();
		int circuitId = resMsg.getCircuitId();
		E_GCP_ScheduleType scheduleType = resMsg.getScheduleType();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("circuitId: " + circuitId);
		MyLog.i("scheduleType: " + scheduleType);

		if (scheduleType == E_GCP_ScheduleType.DEFAULT)
			addDefaultSchedule(new ScheduleInfo(resMsg.getCircuitId(), resMsg.getOnTime(), resMsg.getCtrlMode()));
		else if (scheduleType == E_GCP_ScheduleType.TODAY)
			addTodaySchedule(new ScheduleInfo(resMsg.getCircuitId(), resMsg.getOnTime(), resMsg.getCtrlMode()));
		else if (scheduleType == E_GCP_ScheduleType.TOMORROW)
			addTomorrowSchedule(new ScheduleInfo(resMsg.getCircuitId(), resMsg.getOnTime(), resMsg.getCtrlMode()));
	}

	boolean isContainDefaultSchedule(ScheduleInfo scheduleInfo) {
		Calendar calendar_Info = Calendar.getInstance();
		Calendar calendar_ScheduleInfo = Calendar.getInstance();
		calendar_ScheduleInfo.setTime(scheduleInfo.getOnTime());

		for (int i = 0; i < defaultScheduleList.getSize(); i++) {
			ScheduleInfo info = defaultScheduleList.get(i);
			if (info == null)
				continue;
			calendar_Info.setTime(info.getOnTime());
			if (info.getCircuitId() == scheduleInfo.getCircuitId())
				if (calendar_Info.get(Calendar.HOUR_OF_DAY) == calendar_ScheduleInfo.get(Calendar.HOUR_OF_DAY))
					if (calendar_Info.get(Calendar.MINUTE) == calendar_ScheduleInfo.get(Calendar.MINUTE)) {
						return true;
					}
		}
		return false;
	}

	boolean isContainTodaySchedule(ScheduleInfo scheduleInfo) {
		Calendar calendar_Info = Calendar.getInstance();
		Calendar calendar_ScheduleInfo = Calendar.getInstance();
		calendar_ScheduleInfo.setTime(scheduleInfo.getOnTime());

		for (int i = 0; i < todayScheduleList.getSize(); i++) {
			ScheduleInfo info = todayScheduleList.get(i);
			if (info == null)
				continue;
			calendar_Info.setTime(info.getOnTime());
			if (info.getCircuitId() == scheduleInfo.getCircuitId())
				if (calendar_Info.get(Calendar.HOUR_OF_DAY) == calendar_ScheduleInfo.get(Calendar.HOUR_OF_DAY))
					if (calendar_Info.get(Calendar.MINUTE) == calendar_ScheduleInfo.get(Calendar.MINUTE)) {
						return true;
					}
		}
		return false;
	}

	boolean isContainTomorrowSchedule(ScheduleInfo scheduleInfo) {
		Calendar calendar_Info = Calendar.getInstance();
		Calendar calendar_ScheduleInfo = Calendar.getInstance();
		calendar_ScheduleInfo.setTime(scheduleInfo.getOnTime());

		for (int i = 0; i < tomorrowScheduleList.getSize(); i++) {
			ScheduleInfo info = tomorrowScheduleList.get(i);
			calendar_Info.setTime(info.getOnTime());
			if (info.getCircuitId() == scheduleInfo.getCircuitId())
				if (calendar_Info.get(Calendar.HOUR_OF_DAY) == calendar_ScheduleInfo.get(Calendar.HOUR_OF_DAY))
					if (calendar_Info.get(Calendar.MINUTE) == calendar_ScheduleInfo.get(Calendar.MINUTE)) {
						return true;
					}
		}
		return false;
	}

	private boolean addDefaultSchedule(ScheduleInfo scheduleInfo) {
		Calendar calendar_ScheduleInfo = Calendar.getInstance();
		calendar_ScheduleInfo.setTime(scheduleInfo.getOnTime());

		if (isContainDefaultSchedule(scheduleInfo) == false) {
			if (defaultScheduleList.getSize() >= 255) {
				return false;
			}
			defaultScheduleList.add(scheduleInfo);
		}
		return true;
	}

	private boolean addTodaySchedule(ScheduleInfo scheduleInfo) {
		Calendar calendar_ScheduleInfo = Calendar.getInstance();
		calendar_ScheduleInfo.setTime(scheduleInfo.getOnTime());

		if (isContainTodaySchedule(scheduleInfo) == false) {
			if (todayScheduleList.getSize() >= 255) {
				return false;
			}
			todayScheduleList.add(scheduleInfo);
		}
		return true;
	}

	private boolean addTomorrowSchedule(ScheduleInfo scheduleInfo) {
		Calendar calendar_ScheduleInfo = Calendar.getInstance();
		calendar_ScheduleInfo.setTime(scheduleInfo.getOnTime());

		if (isContainTomorrowSchedule(scheduleInfo) == false) {
			if (tomorrowScheduleList.getSize() >= 255) {
				return false;
			}
			tomorrowScheduleList.add(scheduleInfo);
		}
		return true;
	}

	private void handleDimmerMappingInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleDimmerMappingInfoReq");
		GCP_Message_DimmerMappingInfoReq reqMsg = (GCP_Message_DimmerMappingInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		int index = reqMsg.getIndex();

		DimmerMappingInfo mappingInfo = getDimmerMappingInfoByIndex(index);
		if (mappingInfo == null)
			return;
		short dimmerId = mappingInfo.getDimmerId();
		int channelId = mappingInfo.getChannelId();
		int circuitId = mappingInfo.getCircuitId();

		GCP_Message_DimmerMappingInfoRes resMsg = new GCP_Message_DimmerMappingInfoRes(destId, srcId, subNodeId, dimmerId, channelId, circuitId);
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleDimmerMappingInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingDimmerReq");
		GCP_Message_DimmerMappingInfoRes resMsg = (GCP_Message_DimmerMappingInfoRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();
		short dimmerId = resMsg.getDimmerId();
		int channelId = resMsg.getChannleId();
		int circuitId = resMsg.getCircuitId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("dimmerId: " + dimmerId);
		MyLog.i("circuitId: " + circuitId);

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_DimmerMappingInfoReq)
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

		if (isContainDimmerInMappingTable(dimmerId, channelId) == false)
			addDimmerMappingInfo(new DimmerMappingInfo(dimmerId, channelId, circuitId));
	}

	private void handleSensorMappingInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleSensorMappingInfoReq");
		GCP_Message_SensorMappingInfoReq reqMsg = (GCP_Message_SensorMappingInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		int index = reqMsg.getIndex();

		SensorMappingInfo mappingInfo = getSensorMappingInfoByIndex(index);
		if (mappingInfo == null)
			return;
		short lcId = mappingInfo.getLcId();
		short sensorId = mappingInfo.getSensorId();
		int circuitId = mappingInfo.getCircuitId();
		int onTime = mappingInfo.getOnTime();
		E_GCP_ControlMode onCtrlMode = mappingInfo.getOnCtrlMode();
		E_GCP_ControlMode offCtrlMode = mappingInfo.getOffCtrlMode();

		GCP_Message_SensorMappingInfoRes resMsg = new GCP_Message_SensorMappingInfoRes(destId, srcId, subNodeId, lcId, sensorId, circuitId, onTime, onCtrlMode, offCtrlMode);
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	public SensorMappingInfo getSensorMappingInfoByIndex(int index) {
		SensorMappingInfo info = null;
		if (this.sensorMappingList.getSize() <= index)
			info = null;
		else
			info = this.sensorMappingList.get(index);
		return info;
	}

	public DimmerMappingInfo getDimmerMappingInfoByIndex(int index) {
		DimmerMappingInfo info = null;

		if (this.dimmerMappingList.getSize() <= index)
			info = null;
		else
			info = this.dimmerMappingList.get(index);
		return info;
	}

	private void handleSensorMappingInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleSensorMappingInfoRes");
		GCP_Message_SensorMappingInfoRes resMsg = (GCP_Message_SensorMappingInfoRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short lcId = resMsg.getLcId();
		short subNodeId = resMsg.getSubNodeId();
		short sensorId = resMsg.getSensorId();
		int circuitId = resMsg.getCircuitId();
		int onTime = resMsg.getOnTime();
		E_GCP_ControlMode onCtrlMode = resMsg.getOnDimmingCtrl();
		E_GCP_ControlMode offCtrlMode = resMsg.getOffDimmingCtrl();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("sensorId: " + sensorId);
		MyLog.i("circuitId: " + circuitId);
		MyLog.i("onTime: " + onTime);
		MyLog.i("onCtrlMode: " + onCtrlMode);
		MyLog.i("offCtrlMode: " + offCtrlMode);

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_SensorMappingInfoReq)
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

		if (updateSensorMappingInfo(new SensorMappingInfo(lcId, sensorId, circuitId, onTime, onCtrlMode, offCtrlMode)) == false)
			addSensorMappingInfo(new SensorMappingInfo(lcId, sensorId, circuitId, onTime, onCtrlMode, offCtrlMode));
	}

	private void handleScheduleSizeReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleScheduleSizeReq");
		GCP_Message_ScheduleSizeReq reqMsg = (GCP_Message_ScheduleSizeReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		GCP_Message_ScheduleSizeRes resMsg = new GCP_Message_ScheduleSizeRes(destId, srcId, subNodeId, reqMsg.getScheduleType(), getDefaultScheduleSize());
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleScheduleSizeRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleScheduleSizeRes");
		GCP_Message_ScheduleSizeRes resMsg = (GCP_Message_ScheduleSizeRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();
		int numOfSchedule = resMsg.getNumOfSchedule();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("numOfSchedule: " + numOfSchedule);

		if (resMsg.getScheduleType() == E_GCP_ScheduleType.DEFAULT) {
			if (numOfSchedule != defaultScheduleList.getSize())
				defaultScheduleList.clear();
		} else if (resMsg.getScheduleType() == E_GCP_ScheduleType.TODAY) {
			if (numOfSchedule != todayScheduleList.getSize())
				todayScheduleList.clear();
		} else if (resMsg.getScheduleType() == E_GCP_ScheduleType.TOMORROW) {
			if (numOfSchedule != tomorrowScheduleList.getSize())
				tomorrowScheduleList.clear();
		}

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null)
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

		for (int i = 0; i < numOfSchedule; i++) {
			GCP_Message_ScheduleInfoReq reqMsg = new GCP_Message_ScheduleInfoReq(destId, srcId, subNodeId, resMsg.getScheduleType(), i);
			sendMessageWithResponse(reqMsg, 3, 10);
		}
	}

	private void handleDimmerMappingSizeReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleDimmerMappingSizeReq");
		GCP_Message_DimmerMappingSizeReq reqMsg = (GCP_Message_DimmerMappingSizeReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		GCP_Message_DimmerMappingSizeRes resMsg = new GCP_Message_DimmerMappingSizeRes(destId, srcId, subNodeId, getDimmerMappingTableSize());
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleDimmerMappingSizeRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleDimmerMappingSizeRes");
		GCP_Message_DimmerMappingSizeRes resMsg = (GCP_Message_DimmerMappingSizeRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();
		int dimmerMappingTableSize = resMsg.getNumOfMapping();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("dimmerMappingTableSize: " + dimmerMappingTableSize);

		if (dimmerMappingTableSize != dimmerMappingList.getSize()) {
			// if (dimmerMappingList.getSize() == 0) {
			dimmerMappingList.clear();
			for (int i = 0; i < dimmerMappingTableSize; i++) {
				GCP_Message_DimmerMappingInfoReq reqMsg = new GCP_Message_DimmerMappingInfoReq(getId(), (short) 0, getId(), i);
				sendMessageWithResponse(reqMsg, 3, 10);
			}
			// } else {
			// GCP_Message_ResetMappingDimmerReq reqMsg = new
			// GCP_Message_ResetMappingDimmerReq(
			// getId(), (short) 0, getId(), (byte) 0xff);
			// sendMessageWithResponse(reqMsg, 3, 0);
			// List<DimmerMappingInfo> infoList = dimmerMappingList.getAll();
			// for (DimmerMappingInfo info : infoList) {
			// GCP_Message_MappingDimmerReq req2Msg = new
			// GCP_Message_MappingDimmerReq(
			// getId(), (short) 0, getId(), info.getDimmerId(),
			// info.getChannelId(), info.getCircuitId());
			// sendMessageWithResponse(req2Msg, 3, 3);
			// }
			// }
		}
	}

	private void handleSensorMappingSizeReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleSensorMappingSizeReq");
		GCP_Message_SensorMappingSizeReq reqMsg = (GCP_Message_SensorMappingSizeReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		GCP_Message_SensorMappingSizeRes resMsg = new GCP_Message_SensorMappingSizeRes(destId, srcId, subNodeId, getSensorMappingTableSize());
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleSensorMappingSizeRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleSensorMappingSizeRes");
		GCP_Message_SensorMappingSizeRes resMsg = (GCP_Message_SensorMappingSizeRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();
		int sensorMappingTableSize = resMsg.getNumOfMapping();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("sensorMappingTableSize: " + sensorMappingTableSize);

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null)
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

		if (sensorMappingTableSize != sensorMappingList.getSize()) {
			// if (sensorMappingList.getSize() == 0) {
			sensorMappingList.clear();

			for (int i = 0; i < sensorMappingTableSize; i++) {
				GCP_Message_SensorMappingInfoReq reqMsg = new GCP_Message_SensorMappingInfoReq(getId(), (short) 0, getId(), i);
				sendMessageWithResponse(reqMsg, 3, 10);
			}
			// } else {
			// GCP_Message_ResetMappingSensorReq reqMsg = new
			// GCP_Message_ResetMappingSensorReq(
			// getId(), (short) 0, getId(), (byte) 0xff);
			// sendMessageWithResponse(reqMsg, 3, 0);
			//
			// List<SensorMappingInfo> infoList = sensorMappingList.getAll();
			//
			// for (SensorMappingInfo info : infoList) {
			// GCP_Message_MappingSensorReq req2Msg = new
			// GCP_Message_MappingSensorReq(
			// getId(), (short) 0, getId(), info.getLcId(),
			// info.getSensorId(), info.getCircuitId(),
			// info.getOnTime(), info.getOnCtrlMode(),
			// info.getOffCtrlMode());
			// sendMessageWithResponse(req2Msg, 3, 3);
			// }
			// }
		}
	}

	private void handleUpdatePowermeter(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleUpdatePowermeter");
		GCP_Message_UpdatePowermeterReq reqMsg = (GCP_Message_UpdatePowermeterReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		int powermeterValue = reqMsg.getEnergyPulse();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("powermeterValue: " + powermeterValue);

		this.powermeterValue = powermeterValue;
	}

	private void handlePowermeterReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handlePowermeterReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		// GCP_Message_PowermeterRes resMsg = new
		// GCP_Message_PowermeterRes(srcId,
		// destId, getId(), powermeterValue);
		// resMsg.setSeqNum(gcpMsg.getSeqNum());
		// sendMessage(resMsg);
	}

	private void handlePowermeterRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handlePowermeterRes");
		GCP_Message_PowermeterRes resMsg = (GCP_Message_PowermeterRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;
		short mdfId = resMsg.getMdfId();
		int channelId = resMsg.getChannelId();
		int powermeterValue = resMsg.getEnergyPulse();

		if (mdfId == 0x0000) {
			I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
			if (reqIMsg != null)
				new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);
			MyLog.i("" + mdfId);
			MyLog.i("powermeterValue: " + powermeterValue);
			if (this.powermeterValue != powermeterValue) {
				this.powermeterValue = powermeterValue;
				new DeviceStateChangedEvent(this, "Powermeter: " + powermeterValue, null);
			}
		} else {
			I_Device device = getDeviceById(mdfId);
			if (device != null)
				device.handleMessage((I_Message) resMsg);
		}
	}

	private void handleControlSensorReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleControlSensorReq");
		GCP_Message_ControlSensorReq reqMsg = (GCP_Message_ControlSensorReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();

		int circuitId = reqMsg.getCircuitId();
		int onTime = reqMsg.getOnTime();
		E_GCP_ControlMode ctrlModeOn = reqMsg.getOnDimmingLevel();
		E_GCP_ControlMode ctrlModeOff = reqMsg.getOffDimmingLevel();

		updateSensorMappingTable(circuitId, onTime, ctrlModeOn, ctrlModeOff);

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("CircuitId: " + circuitId);
		MyLog.i("onTime: " + onTime);
		MyLog.i("ctrlModeOn: " + ctrlModeOn);
		MyLog.i("ctrlModeOff: " + ctrlModeOff);

		GCP_Message_ControlSensorRes resMsg = new GCP_Message_ControlSensorRes(srcId, destId, subNodeId, E_GCP_Result.SUCCESS);

		resMsg.setSeqNum(gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);
	}

	private void updateSensorMappingTable(int circuitId, int onTime, E_GCP_ControlMode ctrlModeOn, E_GCP_ControlMode ctrlModeOff) {

		for (int i = 0; i < sensorMappingList.getSize(); i++) {
			SensorMappingInfo mappingInfo = sensorMappingList.get(i);
			if (mappingInfo.getCircuitId() == circuitId) {
				mappingInfo.setOnTime(onTime);
				mappingInfo.setOffCtrlMode(ctrlModeOn);
				mappingInfo.setOnCtrlMode(ctrlModeOff);
			}
		}
	}

	private void handleControlSensorRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlSensorRes");
		GCP_Message_ControlSensorRes resMsg = (GCP_Message_ControlSensorRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_ControlSensorReq) {
			GCP_Message_ControlSensorReq reqMsg = (GCP_Message_ControlSensorReq) reqIMsg;
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);
			if (resMsg.getResult() == E_GCP_Result.SUCCESS) {
				boolean stateChanged = false;
				for (int i = 0; i < sensorMappingList.getSize(); i++) {
					SensorMappingInfo info = sensorMappingList.get(i);
					if (info.getCircuitId() == reqMsg.getCircuitId()) {
						if (info.getOffCtrlMode() != reqMsg.getOffDimmingLevel()) {
							info.setOffCtrlMode(reqMsg.getOffDimmingLevel());
							stateChanged = true;
						}
						if (info.getOnCtrlMode() != reqMsg.getOnDimmingLevel()) {
							info.setOnCtrlMode(reqMsg.getOnDimmingLevel());
							stateChanged = true;
						}
						if (info.getOnTime() != reqMsg.getOnTime()) {
							info.setOnTime(reqMsg.getOnTime());
							stateChanged = true;
						}
					}
				}
				if (stateChanged)
					new DeviceStateChangedEvent(this, "", null);
			}
		}
	}

	private void handleControlCircuitReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleControlCircuitReq");
		GCP_Message_ControlCircuitReq reqMsg = (GCP_Message_ControlCircuitReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();

		int circuitId = reqMsg.getCircuitId();
		E_GCP_ControlMode ctrlMode = reqMsg.getCtrlMode();
		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("CircuitId: " + circuitId);
		MyLog.i("ctrlMode: " + ctrlMode);

		controlInfo[circuitId] = ctrlMode;

		GCP_Message_ControlCircuitRes resMsg = new GCP_Message_ControlCircuitRes(srcId, destId, subNodeId, E_GCP_Result.SUCCESS);

		resMsg.setSeqNum(gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);

	}

	private void handleControlCircuitRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleControlCircuitRes");
		GCP_Message_ControlCircuitRes resMsg = (GCP_Message_ControlCircuitRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);

		if (reqIMsg instanceof GCP_Message_ControlCircuitReq) {
			GCP_Message_ControlCircuitReq reqMsg = (GCP_Message_ControlCircuitReq) reqIMsg;
			if (resMsg.getResult() == E_GCP_Result.SUCCESS) {
				if (controlInfo[reqMsg.getCircuitId()] != reqMsg.getCtrlMode()) {
					controlInfo[reqMsg.getCircuitId()] = reqMsg.getCtrlMode();
					new DeviceStateChangedEvent(this, "", null);
				}
			}

			new ReceiveResponseMessageEvent(this, reqMsg, resMsg, "", null);
		}
	}

	private void handleStateInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleStateInfoReq");
		GCP_Message_StateInfoReq reqMsg = (GCP_Message_StateInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();
		short deviceId = reqMsg.getDeviceId();

		GCP_Message_StateInfoRes resMsg = null;

		if (deviceId == 0) {
			try {
				resMsg = new GCP_Message_StateInfoRes(srcId, destId, subNodeId, deviceId, getDeviceType().toShort(), getStateInfo(), getControlInfo(), getPanId());
				resMsg.setSeqNum(gcpMsg.getSeqNum());
				sendMessage((I_Message) resMsg);
			} catch (UnformattedPacketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				MyLog.e(e.getMessage());
			}
		} else {
			I_Device device = getDeviceById(deviceId);
			if (device == null)
				return;
			device.handleMessage((I_Message) gcpMsg);
		}
	}

	private void handleStateInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleStateInfoRes");

		GCP_Message_StateInfoRes resMsg = (GCP_Message_StateInfoRes) gcpMsg;

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null)
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

		if (resMsg.getDeviceId() == (short) 0x0000 && resMsg.getDeviceType() == E_DeviceType.LC100.toShort()) { // LC
			// info
			boolean stateChanged = false;
			E_GCP_ControlMode controlInfo[] = resMsg.getControlInfo();
			E_GCP_ControlMode stateInfo[] = resMsg.getStateInfo();
			if (controlInfo != null && stateInfo != null && controlInfo.length == 16 && stateInfo.length == 16) {
				if (this.controlInfo != null && this.stateInfo != null && this.controlInfo.length == 16 && this.stateInfo.length == 16)
					for (int i = 0; i < 16; i++) {
						if (controlInfo[i].toByte() != this.controlInfo[i].toByte())
							stateChanged = true;
						if (stateInfo[i].toByte() != this.stateInfo[i].toByte())
							stateChanged = true;

						if (resMsg.getPanId() != this.panId)
							stateChanged = true;
					}
				else
					stateChanged = true;
			}
			if (stateChanged) {
				for (int i = 0; i < 16; i++) {
					this.controlInfo[i] = controlInfo[i];
					this.stateInfo[i] = stateInfo[i];
				}
				this.panId = resMsg.getPanId();
				new DeviceStateChangedEvent(this, "", null);
				// System.out.println(Calendar.getInstance().getTime());
			}
		} else {
			I_Device device = getDeviceById(resMsg.getDeviceId());
			if (device != null)
				device.handleMessage((I_Message) resMsg);
		}
	}

	private void handleDeviceInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleDeviceInfoReq");
		GCP_Message_DeviceInfoReq reqMsg = (GCP_Message_DeviceInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();
		short deviceId;
		short deviceType;
		int index = reqMsg.getIndex();
		byte deviceVersion = 0;

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("index: " + index);

		I_Device device = null;
		if (subNodeId == 0)
			device = this;
		else {
			device = getDeviceByIndex(index);
			if (device == null)
				return;
			device.handleMessage((I_Message) gcpMsg);
		}
		if (device == null)
			return;

		deviceId = device.getId();
		deviceType = device.getDeviceType().toShort();

		GCP_Message_DeviceInfoRes resMsg = new GCP_Message_DeviceInfoRes(srcId, destId, subNodeId, deviceId, deviceType, deviceVersion);
		resMsg.setSeqNum(gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);
	}

	private void handleDeviceInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleDeviceInfoRes");
		GCP_Message_DeviceInfoRes resMsg = (GCP_Message_DeviceInfoRes) gcpMsg;

		short subNodeId = resMsg.getSubNodeId();
		short deviceId = resMsg.getDeviceId();
		short deviceType = resMsg.getDeviceType();
		int deviceVersion = resMsg.getDeviceVersion();
		MyLog.i("subNodeId: " + HexUtils.toHexString(subNodeId));
		MyLog.i("deviceId: " + HexUtils.toHexString(deviceId));
		MyLog.i("deviceType: " + HexUtils.toHexString(deviceType));
		MyLog.i("deviceVersion: " + deviceVersion);

		if (subNodeId == 0 && deviceType == (short) 0x2010) {
			boolean stateChanged = false;
			if (this.firmwareVersion != deviceVersion)
				stateChanged = true;
			if (stateChanged) {
				this.firmwareVersion = deviceVersion;
				new DeviceStateChangedEvent(this, "", null);
			}
		} else {
			I_Device device = getDeviceById(deviceId);
			if (device == null) {
				if (E_DeviceType.getByShort(deviceType) == E_DeviceType.MSN300)
					device = new MSN300(this, deviceId);
				else if (E_DeviceType.getByShort(deviceType) == E_DeviceType.MDT100)
					device = new MDT100(this, deviceId);
				else if (E_DeviceType.getByShort(deviceType) == E_DeviceType.MDF100)
					device = new MDF100(this, deviceId);
				else if (E_DeviceType.getByShort(deviceType) == E_DeviceType.MPR200)
					device = new MPR200(this, deviceId);
				else if (E_DeviceType.getByShort(deviceType) == E_DeviceType.SolarSensorGetter)
					device = new SolarSensorGetter(this, deviceId);

				if (device != null) {
					addDevice(device);
					device.handleMessage((I_Message) gcpMsg);
				}
			} else
				device.handleMessage((I_Message) gcpMsg);
		}
	}

	public I_Device getDeviceById(short deviceId) {
		return subDeviceMap.get(deviceId);
	}

	private void handleNumOfDeviceRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleNumOfDeviceRes");
		GCP_Message_NumOfDeviceRes resMsg = (GCP_Message_NumOfDeviceRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();
		int numOfDevice = resMsg.getNumOfDevice();

		MyLog.i("subNodeId: " + HexUtils.toHexString(subNodeId));
		MyLog.i("numOfDevice: " + numOfDevice);

		if (subNodeId != getId())
			return;

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null)
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);
		if (getNumOfSubDevices() != numOfDevice) {
			for (int i = 0; i < numOfDevice; i++) {
				GCP_Message_DeviceInfoReq reqMsg = new GCP_Message_DeviceInfoReq(srcId, destId, subNodeId, i);
				sendMessageWithResponse((I_Message) reqMsg);
			}
		}
	}

	private void handleNumOfDeviceReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleNumOfDeviceReq");
		GCP_Message_NumOfDeviceReq reqMsg = (GCP_Message_NumOfDeviceReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();
		int numOfDevice = 0;
		if (subNodeId != getId())
			return;

		MyLog.i("subNodeId: " + subNodeId);

		numOfDevice = getNumOfSubDevices();

		GCP_Message_NumOfDeviceRes resMsg = new GCP_Message_NumOfDeviceRes(srcId, destId, subNodeId, numOfDevice);
		resMsg.setSeqNum(gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);
	}

	@Override
	public void setXNetHandler(I_XNetHandler xnetHandler) {
		this.xnetHandler = xnetHandler;
		Collection<I_Device> devices = subDeviceMap.values();
		for (I_Device device : devices)
			device.setXNetHandler(xnetHandler);

		sendDimmerMappingSizeReq();
		sendSensorMappingSizeReq();
		sendScheduleSizeReq(E_GCP_ScheduleType.DEFAULT);
		sendScheduleSizeReq(E_GCP_ScheduleType.TODAY);
		sendScheduleSizeReq(E_GCP_ScheduleType.TOMORROW);
	}

	public void addDevice(I_Device device) {
		device.setXNetHandler(xnetHandler);
		subDeviceMap.put(device.getId(), device);
	}

	public boolean removeDevice(I_Device device) {
		if (subDeviceMap.remove(device.getId()) != null) {
			new DeviceStateChangedEvent(this, "", null);
			return true;
		}
		return false;
	}

	public int getNumOfSubDevices() {
		return subDeviceMap.size();
	}

	public I_Device getDeviceByIndex(int index) {
		if (index >= getNumOfSubDevices())
			return null;
		int cnt = 0;
		Collection<I_Device> devices = subDeviceMap.values();
		for (I_Device device : devices) {
			if (index == cnt)
				return device;
			cnt++;
		}

		return null;
	}

	public E_GCP_ControlMode[] getStateInfo() {
		return stateInfo;
	}

	public E_GCP_ControlMode[] getControlInfo() {
		return controlInfo;
	}

	private boolean sendMessage(I_Message msg) {
		if (xnetHandler == null)
			return false;
		return xnetHandler.sendMessage(msg);
	}

	public void setParent(MGW300 parent) {
		this.parent = parent;
	}

	public int getSensorMappingTableSize() {
		int size = 0;
		size = this.sensorMappingList.getSize();
		return size;
	}

	public int getDimmerMappingTableSize() {
		return this.dimmerMappingList.getSize();
	}

	public int getDefaultScheduleSize() {
		return this.defaultScheduleList.getSize();
	}

	public synchronized boolean addSensorMappingInfo(SensorMappingInfo mappingInfo) {

		if (this.sensorMappingList.getSize() < 256) {
			boolean isContains = false;
			for (int i = 0; i < sensorMappingList.getSize(); i++) {
				SensorMappingInfo info = sensorMappingList.get(i);
				if (info.getCircuitId() == mappingInfo.getCircuitId())
					if (info.getSensorId() == mappingInfo.getSensorId())
						isContains = true;
			}
			if (isContains == false) {
				this.sensorMappingList.add(mappingInfo);

			}
			return true;
		}
		return false;
	}

	public boolean updateSensorMappingInfo(SensorMappingInfo mappingInfo) {

		for (int i = 0; i < sensorMappingList.getSize(); i++) {
			SensorMappingInfo info = sensorMappingList.get(i);
			if (info.getCircuitId() == mappingInfo.getCircuitId())
				if (info.getSensorId() == mappingInfo.getSensorId()) {
					info.onCtrlMode = mappingInfo.onCtrlMode;
					info.offCtrlMode = mappingInfo.offCtrlMode;
					info.onTime = mappingInfo.onTime;
					return true;
				}
		}
		return false;
	}

	public synchronized boolean addDimmerMappingInfo(DimmerMappingInfo mappingInfo) {

		if (this.dimmerMappingList.getSize() < 256) {
			if (isContainDimmerInMappingTable(mappingInfo.getDimmerId(), mappingInfo.getChannelId()) == false) {
				this.dimmerMappingList.add(mappingInfo);

				return true;
			}
		}

		return false;
	}

	public boolean isContainDimmerInMappingTable(short dimmerId, int channelId) {
		for (int i = 0; i < dimmerMappingList.getSize(); i++) {
			DimmerMappingInfo mappingInfo = dimmerMappingList.get(i);
			if (mappingInfo.getDimmerId() == dimmerId && mappingInfo.getChannelId() == channelId) {
				return true;
			}
		}
		return false;
	}

	public I_XNetHandler getXnetHandler() {
		return xnetHandler;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public int getPowermeterValue() {
		int value = 0;
		I_Device device = null;
		for (int i = 0; i < getNumOfSubDevices(); i++) {
			device = getDeviceByIndex(i);
			if (device instanceof MDF100) {
				for (int j = 0; j < 4; j++)
					value += ((MDF100) device).getPowermeterValue(j);
			}
		}
		MyLog.i("Value: " + value + "\n" + "PowermeterValue: " + this.powermeterValue);

		value += this.powermeterValue;
		return value;
	}

	public int getRebootCount() {
		return rebootCount;
	}

	public int getConnectionCount() {
		return connectionCount;
	}

	public Calendar getLastRebootTime() {
		return lastRebootTime;
	}

	public Calendar getLastConnectionTime() {
		return lastConnectionTime;
	}

	public int sendDebugLogLCReq() throws UnformattedPacketException {
		MyLog.i("sendDebugLogLCReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_DebugLogLCReq(srcId, destId, getId());
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public boolean sendResetDebugLCLogReq() {
		MyLog.i("sendResetDebugLCLogReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ResetDebugLogLCReq(srcId, destId, getId());
		boolean result = sendMessage((I_Message) msg);
		try {
			sendDebugLogLCReq();
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
		return result;
	}

	public int sendMappingSensorReq(short lcId, short sensorId, int circuitId, int onTime, E_GCP_ControlMode onCtrlMode, E_GCP_ControlMode offCtrlMode) {
		MyLog.i("sendMappingSensorReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_MappingSensorReq(srcId, destId, getId(), lcId, sensorId, circuitId, onTime, onCtrlMode, offCtrlMode);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendMappingDimmerReq(short dimmerId, int channelId, int circuitId) {
		MyLog.i("sendMappingSensorReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_MappingDimmerReq(srcId, destId, getId(), dimmerId, channelId, circuitId);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendAddScheduleReq(E_GCP_ScheduleType scheduleType, int circuitId, Date onTime, E_GCP_ControlMode ctrlMode) {
		MyLog.i("sendAddScheduleReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_AddScheduleReq(srcId, destId, getId(), scheduleType, circuitId, onTime, ctrlMode);

		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendMappingSensorListReq(short lcId, short sensorId[], int circuitId, int onTime, E_GCP_ControlMode onCtrlMode, E_GCP_ControlMode offCtrlMode) throws UnformattedPacketException {
		MyLog.i("sendMappingSensorListReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_MappingSensorListReq(srcId, destId, getId(), lcId, sensorId.length, sensorId, circuitId, onTime, onCtrlMode, offCtrlMode);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendMappingDimmerListReq(short dimmerId[], int channelIds[], int circuitId) throws UnformattedPacketException {
		MyLog.i("sendMappingDimmerListReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_MappingDimmerListReq(srcId, destId, getId(), dimmerId.length, dimmerId, channelIds, circuitId);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));

	}

	public int sendAddScheduleListReq(E_GCP_ScheduleType scheduleType, int circuitId, Date onTime[], E_GCP_ControlMode ctrlMode[]) throws UnformattedPacketException {
		MyLog.i("sendAddScheduleListReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_AddScheduleListReq(srcId, destId, getId(), scheduleType, circuitId, onTime.length, onTime, ctrlMode);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));

	}

	public int sendPowermeterReq() {
		MyLog.i("sendPowermeterReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_Powermeter_LCReq(srcId, destId, getId());
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public boolean sendUpdatePowermeter(int powermeterValue) {
		MyLog.i("sendUpdatePowermeter");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_UpdatePowermeter_LCReq(srcId, destId, getId(), (short) 0, powermeterValue);
		boolean result = sendMessage((I_Message) msg);
		sendPowermeterReq();

		return result;
	}

	public boolean sendSensorMappingSizeReq() {
		MyLog.i("sendSensorMappingSizeReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_SensorMappingSizeReq(srcId, destId, getId());
		return sendMessageWithResponse((I_Message) msg);
	}

	public boolean sendDimmerMappingSizeReq() {
		MyLog.i("sendDimmerMappingSizeReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_DimmerMappingSizeReq(srcId, destId, getId());
		return sendMessageWithResponse((I_Message) msg);
	}

	public boolean sendSensorMappingInfoReq(short subNodeId, int index) {
		MyLog.i("sendSensorMappingInfoReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_SensorMappingInfoReq(srcId, destId, subNodeId, index);
		return sendMessageWithResponse((I_Message) msg);
	}

	public boolean sendDimmerMappingInfoReq(short subNodeId, int index) {
		MyLog.i("sendDimmerMappingInfoReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_DimmerMappingInfoReq(srcId, destId, subNodeId, index);
		return sendMessageWithResponse((I_Message) msg);
	}

	public boolean sendScheduleInfoReq(E_GCP_ScheduleType scheduleType, int index) {
		MyLog.i("sendScheduleInfoReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ScheduleInfoReq(srcId, destId, getId(), scheduleType, index);
		return sendMessageWithResponse((I_Message) msg);
	}

	public boolean sendScheduleSizeReq(E_GCP_ScheduleType scheduleType) {
		MyLog.i("sendScheduleSizeReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ScheduleSizeReq(srcId, destId, getId(), scheduleType);
		return sendMessageWithResponse((I_Message) msg);
	}

	// public boolean sendSensorMappingReq(short sensorId, int circuitId,
	// int onTime, E_GCP_ControlMode ctrlModeOn,
	// E_GCP_ControlMode ctrlModeOff) {
	// MyLog.i("sendSensorMappingReq");
	//
	// short srcId = getId();
	// short destId = (short) 0x0000;
	//
	// I_GCP_Message msg = new GCP_Message_MappingSensorReq(srcId, destId,
	// getId(), getId(), sensorId, circuitId, onTime, ctrlModeOn, ctrlModeOff);
	// return sendMessageWithResponse((I_Message) msg);
	//
	// }
	//
	// public boolean sendSensorMappingListReq(int numOfSensors,
	// short sensorIds[], int circuitId, int onTime,
	// E_GCP_ControlMode ctrlModeOn, E_GCP_ControlMode ctrlModeOff)
	// throws UnformattedPacketException {
	// MyLog.i("sendSensorMappingReq");
	//
	// short srcId = getId();
	// short destId = (short) 0x0000;
	//
	// I_GCP_Message msg = new GCP_Message_MappingSensorListReq(srcId, destId,
	// getId(), getId(), numOfSensors, sensorIds, circuitId, onTime,
	// ctrlModeOn, ctrlModeOff);
	// return sendMessageWithResponse((I_Message) msg);
	//
	// }

	public boolean sendDimmerMappingReq(short dimmerId, int channelId, int circuitId) {
		MyLog.i("sendDimmerMappingReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_MappingDimmerReq(srcId, destId, getId(), dimmerId, channelId, circuitId);
		return sendMessageWithResponse((I_Message) msg);

	}

	public boolean sendDimmerMappingListReq(int numOfDimmer, short dimmerId[], int channelIds[], int circuitId) throws UnformattedPacketException {
		MyLog.i("sendDimmerMappingListReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_MappingDimmerListReq(srcId, destId, getId(), numOfDimmer, dimmerId, channelIds, circuitId);
		return sendMessageWithResponse((I_Message) msg);

	}

	public int sendResetSensorMappingReq(byte circuitId) {
		MyLog.i("sendResetSensorMappingReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ResetMappingSensorReq(srcId, destId, getId(), circuitId);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));

	}

	public int sendApplyingScheduleReq(E_GCP_ScheduleType scheduleType) {
		MyLog.i("sendResetSensorMappingReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ApplyingScheduleReq(srcId, destId, getId(), scheduleType);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendResetDimmerMappingReq(byte circuitId) {
		MyLog.i("sendResetDimmerMappingReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ResetMappingDimmerReq(srcId, destId, getId(), circuitId);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));

	}

	public int sendResetScheduleReq(E_GCP_ScheduleType scheduleType, byte circuitId) {
		MyLog.i("sendResetScheduleReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ResetScheduleReq(srcId, destId, getId(), scheduleType, circuitId);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));

	}

	public int sendControlCircuit(int circuitId, E_GCP_ControlMode ctrlMode) {
		MyLog.i("sendControlCircuit");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ControlCircuitReq(srcId, destId, getId(), circuitId, ctrlMode);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));

	}

	public int sendControlSensor(int circuitId, int onTime, E_GCP_ControlMode ctrlModeOn, E_GCP_ControlMode ctrlModeOff) {
		MyLog.i("sendControlSensor");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ControlSensorReq(srcId, destId, getId(), circuitId, onTime, ctrlModeOn, ctrlModeOff);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendControlIlluSensor(int circuitId, int onTime, short onIlluLevel, short offIlluLevel) {
		MyLog.i("sendControlSensor");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ControlIlluSensorReq(srcId, destId, getId(), circuitId, onTime, onIlluLevel, offIlluLevel);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));

	}

	public boolean sendStateInfoReq() {
		MyLog.i("sendStateInfoReq");

		short srcId = 0;
		short destId = (short) getId();

		I_GCP_Message msg = new GCP_Message_StateInfoReq(srcId, destId, (short) deviceId, (short) 0);
		return sendMessageWithResponse((I_Message) msg);
	}

	public boolean sendNumOfDeviceReq() {
		MyLog.i("sendNumOfDeviceReq");

		short srcId = getId();
		short destId = (short) 0x0000;
		I_GCP_Message msg = new GCP_Message_NumOfDeviceReq(srcId, destId, getId());
		return sendMessageWithResponse((I_Message) msg);
	}

	public boolean sendDeviceInfoReq(short subNodeId, int index) {
		MyLog.i("sendDeviceInfoReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_DeviceInfoReq(srcId, destId, subNodeId, index);
		return sendMessageWithResponse((I_Message) msg);
	}

	public boolean sendRegisterNodeReq(I_Device device) {
		MyLog.i("sendRegisterNodeReq");
		short srcId = getId();
		short destId = (short) 0x0000;
		short subSrcId = getId();
		short deviceType = device.getDeviceType().toShort();
		short deviceId = device.getId();
		int deviceVersion = 0;
		int firmwareVersion = 1;
		int netAddr = 0;

		I_NCP_Message msg = new NCP_Message_RegisterNodeReq(srcId, destId, subSrcId, deviceType, deviceId, deviceVersion, firmwareVersion, netAddr);
		return sendMessageWithResponse((I_Message) msg);
	}

	private boolean sendMessageWithResponse(I_Message msg) {
		return sendMessageWithResponse(msg, 5, 0);
	}

	private boolean sendMessageWithResponse(I_Message msg, int timeout, int maxRetry) {
		responseWaitMessageQueue.addMessageToResponseWaitMessageQueue((I_Message) msg);
		if (xnetHandler != null) {
			return xnetHandler.sendMessageWithResponse((I_Message) msg, timeout, maxRetry);
		}
		return false;
	}

	@Override
	public I_Device getParent() {
		// TODO Auto-generated method stub
		return parent;
	}

	public DimmerMappingInfo getDimmerMappingInfo(int i) {
		DimmerMappingInfo info = null;
		info = dimmerMappingList.get(i);
		return info;
	}

	public SensorMappingInfo getSensorMappingInfo(int i) {
		SensorMappingInfo info = sensorMappingList.get(i);
		return info;
	}

	public boolean sendRebootReq() {
		MyLog.i("sendRebootReq");

		short srcId = (short) 0x0000;
		short destId = getId();
		short subNodeId = (short) getId();
		short deviceId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_RebootReq(srcId, destId, subNodeId, deviceId);
		boolean result = sendMessage((I_Message) msg);
		return result;
	}

	@Override
	public void run() {
		Calendar lastSentTime_NumOfDevReq = Calendar.getInstance();
		Calendar lastSentTime_DebugLogReq = Calendar.getInstance();
		Calendar lastSentTime_DimmerMappingTableReq = Calendar.getInstance();
		Calendar lastSentTime_SensorMappingTableReq = Calendar.getInstance();
		Calendar lastSentTime_ScheduleReq = Calendar.getInstance();
		while (isTerminated == false) {
			try {
				if (xnetHandler != null) {
					if (xnetHandler.isConnected()) {
						if (lastSentTime_DebugLogReq.getTimeInMillis() + 60000 < Calendar.getInstance().getTimeInMillis()) {// 1분에
							// 한번
							// debugLog
							// 가져오기.
							try {
								sendDebugLogLCReq();
							} catch (UnformattedPacketException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
								MyLog.e(e.toString());
							}
							lastSentTime_DebugLogReq = Calendar.getInstance();
						}
						if (lastSentTime_DimmerMappingTableReq.getTimeInMillis() + 300000 < Calendar.getInstance().getTimeInMillis()) {// 5분에
							// 한번
							// dimmerMappingTable
							// 가져오기.
							sendDimmerMappingSizeReq();
							lastSentTime_DimmerMappingTableReq = Calendar.getInstance();
						}
						if (lastSentTime_SensorMappingTableReq.getTimeInMillis() + 300000 < Calendar.getInstance().getTimeInMillis()) {// 5분에
							// 한번
							// sensorMappingTable
							// 가져오기.
							sendSensorMappingSizeReq();
							lastSentTime_SensorMappingTableReq = Calendar.getInstance();
						}
						if (lastSentTime_ScheduleReq.getTimeInMillis() + 300000 < Calendar.getInstance().getTimeInMillis()) {// 5분에
							// 한번
							// Schedule Info
							// 가져오기.
							sendScheduleSizeReq(E_GCP_ScheduleType.DEFAULT);
							sendScheduleSizeReq(E_GCP_ScheduleType.TODAY);
							sendScheduleSizeReq(E_GCP_ScheduleType.TOMORROW);
							lastSentTime_ScheduleReq = Calendar.getInstance();
						}
						if (lastSentTime_NumOfDevReq.getTimeInMillis() + 120000 < Calendar.getInstance().getTimeInMillis()) {// 2분에
							// 한번
							// NumOfDevice
							// 가져오기.
							sendNumOfDeviceReq();
							lastSentTime_NumOfDevReq = Calendar.getInstance();
						}
					}
				}
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
				MyLog.e(e.getMessage());
			}
		}
	}

	public short getPanId() {
		return panId;
	}

	public void terminate() {
		this.isTerminated = true;
	}

	@Override
	public I_XNetHandler getXNetHandler() {
		return this.xnetHandler;
	}

	public int sendFirmwareUpgradeReq(byte[] data) {
		MyLog.l("sendFirmwareUpgradeReq");

		short subNodeId = (short) getParent().getId();
		short deviceId = (short) getId();

		if (rfuHelper == null)
			rfuHelper = new RemoteFirmwareUpdateHelper(this, data);
		else if (rfuHelper.isComplete())
			rfuHelper = new RemoteFirmwareUpdateHelper(this, data);

		return -1;
	}

	public E_GCP_ScheduleType getApplyingSchedule() {
		return applyingSchedule;
	}

	public int sendTiltControlCommand(int len, String command) {
		MyLog.i("sendTiltControl");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_TiltControlReq(srcId, destId, getId(), len, command);
		sendMessage((I_Message) msg);
		return 0;
	}

	public String getTiltMessage() {
		return this.tiltMessage;
	}

	public void setTiltMessage(String string) {
		this.tiltMessage = string;
	}

	public String getZyroMessage() {
		return this.boatSensorMessage;
	}

	public void setZyroMessage(String string) {
		this.boatSensorMessage = string;
	}
}
