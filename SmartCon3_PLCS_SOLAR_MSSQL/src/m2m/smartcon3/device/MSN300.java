package m2m.smartcon3.device;

import java.util.Calendar;
import java.util.Random;

import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.devicenetwork.ResponseWaitMessageQueue;
import m2m.smartcon3.event.DeviceDisconnectEvent;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.event.SensorEvent;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.module.RemoteFirmwareUpdateHelper;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_MessageType;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NoticeEvent;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_RebootReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SetSensingLevelReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SetSensingLevelRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_StateInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_StateInfoRes;
import m2m.smartcon3.protocol.gcp2_1.I_GCP_Message;
import m2m.smartcon3.protocol.rfup1_0.I_RFUP_Message;
import m2m.smartcon3.protocol.rfup1_0.RFUP_Message_SendDataRes;
import m2m.smartcon3.protocol.rfup1_0.RFUP_Message_UpgradeRes;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class MSN300 implements I_Device {
	I_XNetHandler xnetHandler = null;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.MSN300;
	private int firmwaraeVersion;
	private int sensingLevel = -1;
	private int illumination = -1;
	private int temperature = -1;
	I_Device parent = null;

	NETWORK_STATE networkState = NETWORK_STATE.OFFLINE;
	private Calendar lastMessageReceivingTime = Calendar.getInstance();

	ResponseWaitMessageQueue responseWaitMessageQueue = new ResponseWaitMessageQueue();

	RemoteFirmwareUpdateHelper rfuHelper = null;

	public MSN300(I_Device parent, short id) {
		this.parent = parent;
		this.deviceId = id;

		new DeviceStateChangedEvent(this, "", null);

		MyLog.lWithSysOut("Create MSN300: " + HexUtils.toHexString(deviceId));
	}

	@Override
	public short getId() {
		return this.deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		if (xnetHandler == null)
			return NETWORK_STATE.OFFLINE;
		else if (xnetHandler.isConnected() == false)
			return NETWORK_STATE.OFFLINE;
		return networkState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}

	@Override
	public void setXNetHandler(I_XNetHandler xnetHandler) {
		this.xnetHandler = xnetHandler;
	}

	private boolean sendMessage(I_Message msg) {
		if (xnetHandler == null)
			return false;
		return xnetHandler.sendMessage(msg);
	}

	@Override
	public void handleMessage(I_Message msg) {

		// 120초이상 message 응답 없으면 network state를 offline로 설정.

		if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			if (gcpMsg.getMessageType() != E_GCP_MessageType.RES_DEVICEINFO) {
				lastMessageReceivingTime = Calendar.getInstance();
				networkState = NETWORK_STATE.ONLINE;
			}
		}

		if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			switch (gcpMsg.getMessageType()) {
			case RES_DEVICEINFO:
				handleDeviceInfoRes(gcpMsg);
				break;
			case REQ_DEVICEINFO:
				handleDeviceInfoReq(gcpMsg);
				break;
			case RES_STATEINFO:
				handleStateInfoRes(gcpMsg);
				break;
			case REQ_STATEINFO:
				handleStateInfoReq(gcpMsg);
				break;
			case RES_SET_SENSING_LEVEL:
				handleSetSensingLevelRes(gcpMsg);
				break;
			case REQ_SET_SENSING_LEVEL:
				handleSetSensingLevelReq(gcpMsg);
				break;
			case NOTICE_EVENT:
				handleNoticeEvent(gcpMsg);
				break;
			}
		} else if (msg instanceof I_RFUP_Message) {
			I_RFUP_Message rfupMsg = (I_RFUP_Message) msg;
			if (rfuHelper != null && rfuHelper.isComplete() == false)
				rfuHelper.handleResponseMessage(rfupMsg);
		}
	}

	private void handleNoticeEvent(I_GCP_Message gcpMsg) {
		MyLog.i("MSN_handleNoticeEvent");
		GCP_Message_NoticeEvent reqMsg = (GCP_Message_NoticeEvent) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();

		short sensorId = reqMsg.getSensorId();
		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("sensorId: " + sensorId);

		new SensorEvent(this, "SensorEvent: " + sensorId, null);
	}

	private void handleSetSensingLevelReq(I_GCP_Message gcpMsg) {
		MyLog.i("MSN_handleSetSensingLevelReq");
		GCP_Message_SetSensingLevelReq reqMsg = (GCP_Message_SetSensingLevelReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		short sensorId = reqMsg.getSensorId();
		int level = reqMsg.getLevel();
		this.sensingLevel = level;

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("sensorId: " + sensorId);
		MyLog.i("level: " + level);

		GCP_Message_SetSensingLevelRes resMsg = new GCP_Message_SetSensingLevelRes(
				destId, srcId, subNodeId, sensorId, E_GCP_Result.SUCCESS);
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleSetSensingLevelRes(I_GCP_Message gcpMsg) {
		MyLog.i("MSN_handleSetSensingLevelRes");
		GCP_Message_SetSensingLevelRes resMsg = (GCP_Message_SetSensingLevelRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();
		short sensorId = resMsg.getSensorId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("sensorId: " + sensorId);
		MyLog.i("result: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue
				.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_SetSensingLevelReq) {
			GCP_Message_SetSensingLevelReq reqMsg = (GCP_Message_SetSensingLevelReq) reqIMsg;

			new ReceiveResponseMessageEvent(this, reqMsg, (I_Message) gcpMsg,
					"", null);
			if (resMsg.getResult() == E_GCP_Result.SUCCESS) {
				this.sensingLevel = reqMsg.getLevel();
				new DeviceStateChangedEvent(this, "", null);
			}
		}

	}

	private void handleStateInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("MSN300_handleStateInfoReq");
		GCP_Message_StateInfoReq reqMsg = (GCP_Message_StateInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();
		short deviceId = reqMsg.getDeviceId();

		GCP_Message_StateInfoRes resMsg = null;

		try {
			resMsg = new GCP_Message_StateInfoRes(srcId, destId, subNodeId,
					deviceId, getDeviceType().toShort(), getSensingLevel());
			resMsg.setSeqNum(gcpMsg.getSeqNum());
			sendMessage((I_Message) resMsg);
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
	}

	public int getSensingLevel() {
		return sensingLevel;
	}

	private void handleStateInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("MSN_handleStateInfoRes");

		GCP_Message_StateInfoRes resMsg = (GCP_Message_StateInfoRes) gcpMsg;

		if (this.sensingLevel != resMsg.getSensingLevel()) {
			this.sensingLevel = resMsg.getSensingLevel();
			new DeviceStateChangedEvent(this, "SensingLevel: " + sensingLevel,
					null);
		}
		this.illumination = resMsg.getIllumination();
		this.temperature = resMsg.getTemperature();
		MyLog.i("sensingLevel: " + sensingLevel);
		// if(this.temperature>0)System.out.println("Temp: "+temperature);
		// if(this.temperature>0)System.out.println("Lux: "+illumination);
	}

	private void handleDeviceInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("MSN300_handleDeviceInfoReq");
		GCP_Message_DeviceInfoReq reqMsg = (GCP_Message_DeviceInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();
		short deviceId;
		short deviceType;
		byte deviceVersion = 0;

		I_Device device = this;

		deviceId = device.getId();
		deviceType = device.getDeviceType().toShort();

		GCP_Message_DeviceInfoRes resMsg = new GCP_Message_DeviceInfoRes(srcId,
				destId, subNodeId, deviceId, deviceType, deviceVersion);
		resMsg.setSeqNum(gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);
	}

	private void handleDeviceInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("MSN_handleDeviceInfoRes");
		GCP_Message_DeviceInfoRes resMsg = (GCP_Message_DeviceInfoRes) gcpMsg;

		short subNodeId = resMsg.getSubNodeId();
		short deviceId = resMsg.getDeviceId();
		short deviceType = resMsg.getDeviceType();
		int deviceVersion = resMsg.getDeviceVersion();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("deviceId: " + deviceId);
		MyLog.i("deviceType: " + HexUtils.toHexString(deviceType));
		MyLog.i("deviceVersion: " + deviceVersion);

		this.firmwaraeVersion = deviceVersion;
	}

	public boolean sendNoticeEvent(short subNodeId, short sensorId) {
		MyLog.i("sendNoticeEvent");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_NoticeEvent((short) 0x0000,
				getId(), subNodeId, 0x01, sensorId);
		return sendMessage((I_Message) msg);
	}

	public int sendSetSensingLevelReq(int level) {
		MyLog.i("sendSetSensingLevelReq");

		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = getParent().getId();
		short sensorId = getId();

		I_GCP_Message msg = new GCP_Message_SetSensingLevelReq((short) 0x0000,
				getId(), subNodeId, sensorId, level);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));

	}

	public boolean sendStateInfoReq() {
		MyLog.i("sendStateInfoReq");

		short subNodeId = getParent().getId();
		short sensorId = getId();

		I_GCP_Message msg = new GCP_Message_StateInfoReq((short) 0x0000,
				getId(), subNodeId, sensorId);
		return sendMessageWithResponse((I_Message) msg);
	}

	public void setFirmwareVersion(int version) {
		this.firmwaraeVersion = version;
	}

	private boolean sendMessageWithResponse(I_Message msg) {
		responseWaitMessageQueue.addMessageToResponseWaitMessageQueue(msg);
		if (xnetHandler != null) {
			return xnetHandler.sendMessageWithResponse((I_Message) msg);
		}
		return false;
	}

	public I_Device getParent() {
		return parent;
	}

	public boolean sendRebootReq() {
		MyLog.i("sendRebootReq");

		short subNodeId = (short) getParent().getId();
		short deviceId = (short) getId();

		I_GCP_Message msg = new GCP_Message_RebootReq((short) 0x0000, getId(),
				subNodeId, deviceId);
		return sendMessage((I_Message) msg);
	}

	public int sendFirmwareUpgradeReq(byte[] data) {
		MyLog.l("sendFirmwareUpgradeReq");

		short subNodeId = (short) getParent().getId();
		short deviceId = (short) getId();

		if (rfuHelper == null)
			rfuHelper = new RemoteFirmwareUpdateHelper(this, data);
		else if (rfuHelper.isComplete())
			rfuHelper = new RemoteFirmwareUpdateHelper(this, data);

		return -1;
	}

	public int getFirmwareVersion() {
		return firmwaraeVersion;
	}

	Calendar lastTick = Calendar.getInstance();

	public void tick_1Sec() {
		if (lastTick.getTimeInMillis() + 1000 < System.currentTimeMillis()) {
			lastTick = Calendar.getInstance();
			responseWaitMessageQueue.increaseTimerOfMessages();
			handleStateMonitoring();
			handleNetworkConnection();
			// handleSendSensorEventForTestDummy();
		}
	}

	private void handleNetworkConnection() {
		if (lastMessageReceivingTime.getTimeInMillis() + 600000 < System
				.currentTimeMillis()) {
			if (networkState != networkState.OFFLINE) {
				networkState = networkState.OFFLINE;
				new DeviceDisconnectEvent(this, "", null);
			}
		}
	}

	Calendar lastSentTime_SensorEvent = Calendar.getInstance();

	private void handleSendSensorEventForTestDummy() {
		if (xnetHandler != null) {
			if (xnetHandler.isConnected()) {
				if (lastSentTime_SensorEvent.getTimeInMillis() + 1000
						+ new Random().nextInt(500) < Calendar.getInstance()
						.getTimeInMillis()) {// 1초+a에 한번 event 발생.
					sendNoticeEvent(getParent().getId(), getId());
					lastSentTime_SensorEvent = Calendar.getInstance();
				}
			}
		}
	}

	Calendar lastSentTime_StateInfoReq = Calendar.getInstance();

	protected void handleStateMonitoring() {
		if (xnetHandler != null) {
			if (xnetHandler.isConnected()) {
				if (lastSentTime_StateInfoReq.getTimeInMillis() + 60000
						+ ((new Random()).nextInt(30000)) < Calendar
						.getInstance().getTimeInMillis()) {// 60초에 한번
															// stateInfo
															// 가져오기.
					sendStateInfoReq();
					lastSentTime_StateInfoReq = Calendar.getInstance();
				}
			}
		}
	}

	public int getIllumination() {
		return this.illumination;
	}

	public int getTemperature() {
		return this.temperature;
	}

	@Override
	public I_XNetHandler getXNetHandler() {
		return this.xnetHandler;
	}
}
