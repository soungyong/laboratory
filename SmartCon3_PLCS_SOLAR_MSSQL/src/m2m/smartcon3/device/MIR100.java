package m2m.smartcon3.device;

import java.util.Calendar;

import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.devicenetwork.ResponseWaitMessageQueue;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_IrButtonSamsung;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_IrControlCompany;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_MessageType;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_IrControlReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_IrControlRes;
import m2m.smartcon3.protocol.gcp2_1.I_GCP_Message;
import m2m.smartcon3.protocol.rfup1_0.I_RFUP_Message;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class MIR100 implements I_Device {
	
	I_XNetHandler xnetHandler = null;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.MIR100;
	private int firmwareVersion;
	I_Device parent = null;
	
	NETWORK_STATE networkState = NETWORK_STATE.OFFLINE;
	private Calendar lastMessageReceivingTime = Calendar.getInstance();
	
	public static final int MODE_COOLING = 0;
	public static final int MODE_AUTO = 1;
	public static final int MODE_DEHUMIDIFY = 2;
	public static final int MODE_AIR = 3;
	
	private boolean power = false;
	private int mode = MODE_COOLING;
	private int windPower = 0;
	private int windPowerAir = 0;
	private int temperature = 18;
	private int temperatureDehumidify = 18;
	private boolean isTurbo = false;
	private boolean savingMode = false;
	private boolean naturalWind = false;
	private boolean isSound = true;
	
	ResponseWaitMessageQueue responseWaitMessageQueue = new ResponseWaitMessageQueue();

	public MIR100(I_Device parent, short id) {
		this.parent = parent;
		this.deviceId = id;
		
		new DeviceStateChangedEvent(this, "", null);
		
		MyLog.lWithSysOut("Create MIR100: "+ HexUtils.toHexString(deviceId));
	}

	@Override
	public short getId() {
		return this.deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		if(xnetHandler == null) {
			return NETWORK_STATE.OFFLINE;
		} else if (xnetHandler.isConnected() == false) {
			return NETWORK_STATE.OFFLINE;
		}
		return networkState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}

	@Override
	public I_XNetHandler getXNetHandler() {
		return this.xnetHandler;
	}

	@Override
	public void setXNetHandler(I_XNetHandler xnetHandler) {
		this.xnetHandler = xnetHandler;
	}

	@Override
	public void handleMessage(I_Message msg) {
		if(msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			if(gcpMsg.getMessageType() != E_GCP_MessageType.RES_DEVICEINFO) {
				lastMessageReceivingTime = Calendar.getInstance();
				networkState = NETWORK_STATE.ONLINE;
			}
		}
		
		if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			switch (gcpMsg.getMessageType()) {
			case RES_DEVICEINFO:
				break;
			case REQ_DEVICEINFO:
				break;
			case RES_STATEINFO:
				break;
			case REQ_STATEINFO:
				break;
			case REQ_IR_CONTROL:
				break;
			case RES_IR_CONTROL:
				handleIrControlRes(gcpMsg);
				break;
			default:
				break;
			}
		} else if (msg instanceof I_RFUP_Message) {
			I_RFUP_Message rfupMsg = (I_RFUP_Message) msg;
			//TODO : 원격 펌웨어 업데이트 부분 해야됨
		}
	}

	private void handleIrControlRes(I_GCP_Message gcpMsg) {
		MyLog.i("MIR_handleIrControlRes");
		GCP_Message_IrControlRes resMsg = (GCP_Message_IrControlRes) gcpMsg;
		
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();
		short irId = resMsg.getIrId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("sensorId: " + irId);
		MyLog.i("result: " + resMsg.getResult());

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if(reqIMsg instanceof GCP_Message_IrControlReq) {
			GCP_Message_IrControlReq reqMsg = (GCP_Message_IrControlReq) reqIMsg;
			
			new ReceiveResponseMessageEvent(this, reqMsg, (I_Message) gcpMsg, "", null);
			if(resMsg.getResult() == E_GCP_Result.SUCCESS) {
				new DeviceStateChangedEvent(this, "", null);
			}
		}
	}

	@Override
	public I_Device getParent() {
		return parent;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public void setFirmwareVersion(int version) {
		this.firmwareVersion = version;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public int sendIrControlReq(E_GCP_IrControlCompany company, E_GCP_IrButtonSamsung button) {
		MyLog.i("sendIrControlReq");
		
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = getParent().getId();
		short irId = getId();
		
		byte[] data = irApplyButton(button);
		
		if(data == null) {
			return -1;
		}
		
		if(data.length != 7) { // 종료는 2개 보내야 한다.
			byte[] data1 = new byte[7];
			byte[] data2 = new byte[7];
			for(int i=0 ; i<data.length ; i++) {
				if(i<7) { 
					data1[i] = data[i];
				} else {
					data2[i%7] = data[i];
				}
			}
			
			I_GCP_Message msg = new GCP_Message_IrControlReq((short) 0x0000, getId(), subNodeId, irId, company, data1);
			I_GCP_Message msg2 = new GCP_Message_IrControlReq((short) 0x0000, getId(), subNodeId, irId, company, data2);
			
			if( sendMessageWithResponse((I_Message) msg) == false) {
				new FailToSendMessageEvent(this, (I_Message) msg, "", null);
			}
			
			if( sendMessageWithResponse((I_Message) msg2) == false) {
				new FailToSendMessageEvent(this, (I_Message) msg2, "", null);
			}
			
			return HexUtils.toInt(HexUtils.toHexString(msg2.getSeqNum()));
		} else {
			I_GCP_Message msg = new GCP_Message_IrControlReq((short) 0x0000, getId(), subNodeId, irId, company, data);
			
			if( sendMessageWithResponse((I_Message) msg) == false) {
				new FailToSendMessageEvent(this, (I_Message) msg, "", null);
			}
			
			System.out.println("====================================================");
			for(int i=0; i<data.length ; i++) {
				System.out.print(HexUtils.toHexString(data[i])+" ");
			}
			System.out.println();
			System.out.println(HexUtils.toHexString(msg.getSeqNum()));
			System.out.println("====================================================");
			
			return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));			
		}
	}


	private boolean sendMessageWithResponse(I_Message msg) {
		responseWaitMessageQueue.addMessageToResponseWaitMessageQueue(msg);
		if (xnetHandler != null) {
			return xnetHandler.sendMessageWithResponse((I_Message) msg);
		}
		return false;
	}

	private byte[] irApplyButton(E_GCP_IrButtonSamsung button) {
		byte[] commandData = null;
		
		switch (button) {
		case DEFAULT:
			commandData = getCommandData();
			break;
		case POWER:
			selectedPower();
			commandData = getCommandData();
			break;
		case TEMPURATURE_UP:
			selectedTemperatureUp();
			commandData = getCommandData();
			break;
		case TEMPURATURE_DOWN:
			selectedTemperatureDown();
			commandData = getCommandData();
			break;
		case MODE:
			selectedMode();
			commandData = getCommandData();
			break;
		case WINDPOWER:
			selectedWindPower();
			commandData = getCommandData();
			break;
		case TURBO:
			selectedTurbo();
			commandData = getCommandData();
			break;
		case SAVING:
			selectedSaving();
			commandData = getCommandData();
			break;
		default:
			break;
		}
		
		return commandData;
	}

	private void selectedPower() {
		this.power = !this.power;
	}

	private void selectedTemperatureUp() {
		if(power) {
			if(this.mode == MODE_AUTO || this.mode == MODE_AIR) {
				this.mode = MODE_COOLING;
				return;
			} 
			
			if(this.isTurbo) {
				return;
			}
			
			if(this.mode == MODE_DEHUMIDIFY) {
				if(this.temperatureDehumidify < 30) {
					this.temperatureDehumidify += 1;
				}
				return;
			}
				
			if(this.temperature < 30) {
				this.temperature += 1;
				return;
			}
		}
	}
	
	private void selectedTemperatureDown() {
		if(power) {
			if(this.mode == MODE_AUTO || this.mode == MODE_AIR) {
				this.mode = MODE_COOLING;
				return;
			}
			
			if(this.isTurbo) {
				return;
			}
			
			if(this.mode == MODE_DEHUMIDIFY) {
				if(this.temperatureDehumidify > 18) {
					this.temperatureDehumidify -= 1;
				}
				return;
			}
			
			if(this.savingMode) {
				if(this.temperature > 26) {
					this.temperature -= 1;
				}
				return;
			}
			
			if(this.temperature > 18) {
				this.temperature -= 1;
				return;
			}
		}
	}

	private void selectedMode() {
		if(this.power) {
			if(this.isTurbo) {
				this.isTurbo = false;
				return;
			}
			
			if(this.savingMode) {
				this.savingMode = false;
				return;
			}
			
			this.mode = (this.mode + 1) % 4; 
		}
	}

	private void selectedWindPower() {
		if(this.power) {			
			if(this.isTurbo) {
				return;
			}
			
			if(this.mode == MODE_COOLING) {
				windPower = (windPower +1)%5;
				return;
			}
			
			if(this.mode == MODE_AIR) {
				windPowerAir = (windPowerAir+1)%4; 
				return;
			}
		}
	}
	
	private void selectedTurbo() {
		if(this.power) {
			if(this.savingMode) {
				this.savingMode = false;
			}
			
			if(this.isTurbo) {
				if(this.mode != MODE_COOLING) {
					this.mode = MODE_AUTO;
				}
				this.isTurbo = false;
				return;				
			}
			if(!this.isTurbo) {
				if(this.mode != MODE_COOLING) {
					this.mode = MODE_AUTO;
				}
				this.isTurbo = true;
				return;
			}
		}
	}
	
	private void selectedSaving() {
		if(this.power) {
			if(this.mode == MODE_COOLING) {
				this.savingMode = !this.savingMode;
				this.isTurbo = false;
				if(this.temperature < 26) {
					temperature = 26;
				}
				return;
			}
			
			if(this.mode == MODE_AUTO) {
				this.savingMode = !this.savingMode;
				this.isTurbo = false;
				return;
			}
		}
	}
	
	private byte[] getCommandData() {
		byte[] commandData = null;
		
		if(!power) {
//			commandData = new byte[14];
//			commandData[0] = (byte) 0x80;
//			commandData[1] = (byte) 0x41;
//			commandData[2] = (byte) 0xF0;
//			commandData[3] = (byte) 0x00;
//			commandData[4] = (byte) 0x08;
//			commandData[5] = (byte) 0x0E;
//			commandData[6] = (byte) 0x80;
//			commandData[7] = (byte) 0x80;
//			commandData[8] = (byte) 0x4B;
//			commandData[9] = (byte) 0x7F;
//			commandData[10] = (byte) 0x8E;
//			commandData[11] = (byte) 0x0C;
//			commandData[12] = (byte) 0xD8;
//			commandData[13] = (byte) 0x03;
			commandData = new byte[7];
			commandData[0] = (byte) 0x80;
			commandData[1] = (byte) 0x4B;
			commandData[2] = (byte) 0x7F;
			commandData[3] = (byte) 0x8E;
			commandData[4] = (byte) 0x0C;
			commandData[5] = (byte) 0xD8;
			commandData[6] = (byte) 0x03;
			return commandData;
		} else if(isTurbo) { 
			commandData = new byte[7];
			commandData[0] = (byte) 0x80;
			commandData[1] = (byte) 0x43;
			commandData[2] = (byte) 0x7F;
			commandData[3] = (byte) 0xEE;
			commandData[4] = (byte) 0x09;
			commandData[5] = (byte) 0x80;
			commandData[6] = (byte) 0x0F;
			return commandData;
		} else if(mode == MODE_AIR) { // 송풍
			commandData = new byte[7];
			commandData[0] = (byte) 0x80;
			commandData[1] = (byte) 0x45;
			commandData[2] = (byte) 0x7F;
			commandData[3] = (byte) 0x8E;
			commandData[4] = (byte) 0x01;
			switch (windPowerAir) {
			case 0:
				commandData[5] = (byte) 0xAC;
				break;
			case 1:
				commandData[5] = (byte) 0x9C;
				break;
			case 2:
				commandData[5] = (byte) 0xDC;
				break;
			case 3:
				commandData[5] = (byte) 0xFC;
				break;
			default:
				commandData[5] = (byte) 0xAC;
				break;
			}
			commandData[6] = (byte) 0x0F;
			return commandData;
		} else if(mode == MODE_AUTO) { // 자동
			commandData = new byte[7];
			commandData[0] = (byte) 0x80;
			commandData[1] = (byte) 0x43;
			commandData[2] = (byte) 0x7F;
			commandData[3] = (byte) 0x8E;
			commandData[4] = (byte) 0x09;
			commandData[5] = (byte) 0xB0;
			commandData[6] = (byte) 0x0F;
			return commandData;
		} else {
			commandData = new byte[7];
			commandData[0] = (byte) 0x80;
			commandData[1] = (byte) 0x00;
			commandData[2] = (byte) 0x7F;
			
			byte temp = (byte) 0x00;
			if(savingMode) {
				temp = (byte) 0xF0;
			} else {
				temp = (byte) 0x80;
			}
			if(isSound) {
				temp += (byte) 0x0E;
			} else {
				temp += (byte) 0x0F;
			}
			commandData[3] = temp;
			
			if(mode == MODE_COOLING) { //냉방 일때
				temp = (byte) (temperature - 16);	
			} else { // 제습 일때
				temp = (byte) (temperatureDehumidify - 16);
			}
			byte lbs = 0;
			for (int i = 0; i < 4; i++) {
				if ((0x01 & temp >> i) == 0x01) {
					lbs += Math.pow(2, 3 - i);
				}
			}
			commandData[4] = (byte) (0x00+lbs);
			
			if(mode == MODE_DEHUMIDIFY) {
				commandData[5] = (byte) 0x84;
			} else {
				if(naturalWind) {
					commandData[5] = (byte) 0x88;
				} else {
					switch (windPower) {
					case 0:
						commandData[5] = (byte) 0xA8;
						break;
					case 1:
						commandData[5] = (byte) 0x98;
						break;
					case 2:
						commandData[5] = (byte) 0xD8;
						break;
					case 3:
						commandData[5] = (byte) 0xF8;
						break;
					default:
						break;
					}
				}
			}
			
			commandData[6] = (byte) 0x0F;
			
			commandData[1] = getCs(commandData);
			
			return commandData;
		}
	}
	
	private static byte getCs(byte[] data) {
		int highBits = 0;

		for (int i = 0; i < data.length; i++) {
			if (i == 1) {
				continue;
			}

			byte command = data[i];

			for (int j = 0; j < 8; j++) {
				if ((0x01 & (command >> j)) == 0x01) {
					highBits++;
				}
			}
		}

		byte CS = (byte) ((highBits - 3) % 15);

		byte NewCS = 0;
		for (int i = 0; i < 4; i++) {
			if ((0x01 & CS >> i) == 0x00) {
				NewCS += Math.pow(2, i);
			}
		}

		byte FinalCS = 0;
		for (int i = 0; i < 4; i++) {
			if ((0x01 & NewCS >> i) == 0x01) {
				FinalCS += Math.pow(2, 3 - i);
				Object args[] = new Object[1];
				args[0] = new Byte(FinalCS);
			}
		}

		FinalCS += 0x40;
		return FinalCS;
	}

	public boolean isPower() {
		return power;
	}

	public int getMode() {
		return mode;
	}

	public int getWindPower() {
		return windPower;
	}

	public int getWindPowerAir() {
		return windPowerAir;
	}

	public int getTemperature() {
		return temperature;
	}

	public int getTemperatureDehumidify() {
		return temperatureDehumidify;
	}

	public boolean isTurbo() {
		return isTurbo;
	}

	public boolean isSavingMode() {
		return savingMode;
	}

	public boolean isNaturalWind() {
		return naturalWind;
	}
}
