package m2m.smartcon3.device;

import java.io.Serializable;
import java.util.Date;

import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;

public class ScheduleInfo implements Serializable{	
	int circuitId;
	Date onTime;
	E_GCP_ControlMode ctrlMode;

	public ScheduleInfo(int circuitId, Date onTime, E_GCP_ControlMode ctrlMode) {
		this.circuitId = circuitId;
		this.onTime = onTime;
		this.ctrlMode = ctrlMode;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public void setCircuitId(int circuitId) {
		this.circuitId = circuitId;
	}

	public Date getOnTime() {
		return onTime;
	}

	public void setOnTime(Date onTime) {
		this.onTime = onTime;
	}

	public E_GCP_ControlMode getCtrlMode() {
		return ctrlMode;
	}

	public void setCtrlMode(E_GCP_ControlMode ctrlMode) {
		this.ctrlMode = ctrlMode;
	}
}
