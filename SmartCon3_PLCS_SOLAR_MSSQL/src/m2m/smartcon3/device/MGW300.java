package m2m.smartcon3.device;

import java.util.Calendar;
import java.util.Collection;
import java.util.Hashtable;

import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.devicenetwork.ResponseWaitMessageQueue;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.module.RemoteFirmwareUpdateHelper;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleListReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleListRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ApplyingScheduleReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ApplyingScheduleRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_BoatSensorMessage;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlCircuitReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlCircuitRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlGatewayReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlGatewayRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlNoticeModeReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlNoticeModeRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlSensorReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlSensorRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlSensorSharingReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlSensorSharingRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DebugLogLCReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DebugLogLCRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DebugLogReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DebugLogRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingSizeReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingSizeRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_IrControlRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerListReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerListRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorListReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorListRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NoticeEvent;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NoticeSensorValue;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NumOfDeviceReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NumOfDeviceRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_PowermeterReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_PowermeterRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_RebootReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetDebugLogLCReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetDebugLogReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingDimmerReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingDimmerRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingSensorReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingSensorRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetScheduleReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetScheduleRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleSizeReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleSizeRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingSizeReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingSizeRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SetSensingLevelReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SetSensingLevelRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_StateInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_StateInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_TiltMessage;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_UpdatePowermeterReq;
import m2m.smartcon3.protocol.gcp2_1.I_GCP_Message;
import m2m.smartcon3.protocol.ncp2_0.I_NCP_Message;
import m2m.smartcon3.protocol.ncp2_0.NCP_Message_PingReq;
import m2m.smartcon3.protocol.ncp2_0.NCP_Message_PingRes;
import m2m.smartcon3.protocol.ncp2_0.NCP_Message_RegisterNodeReq;
import m2m.smartcon3.protocol.ncp2_0.NCP_Message_RegisterReq;
import m2m.smartcon3.protocol.ncp2_0.NCP_Message_RegisterRes;
import m2m.smartcon3.protocol.ncp2_0.NCP_Message_SendTimeInfo;
import m2m.smartcon3.protocol.rfup1_0.I_RFUP_Message;
import m2m.smartcon3.protocol.rfup1_0.RFUP_Message_SendDataRes;
import m2m.smartcon3.protocol.rfup1_0.RFUP_Message_UpgradeRes;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class MGW300 implements I_Device, Runnable {
	protected I_XNetHandler xnetHandler = null;
	protected short deviceId = 0x00;
	E_DeviceType deviceType;

	Hashtable<Short, I_Device> lcMap = new Hashtable<Short, I_Device>();

	int firmwareVersion = 0;
	int deviceVersion = 0;
	String ipAddress = "192.168.0.50";
	String serverIpAddress = "192.168.0.10";
	String defaultGateway = "192.168.0.1";
	String netMask = "255.255.255.0";
	int port = 0;
	boolean isSensorSharingMode = false;

	protected Calendar currentTime = Calendar.getInstance();
	int rebootCount = 0;
	Calendar lastRebootTime = Calendar.getInstance();
	int serverConnectionCount = 0;
	Calendar lastServerConnectionTime = Calendar.getInstance();

	boolean isReceiveRegReq = false;
	ResponseWaitMessageQueue responseWaitMessageQueue = new ResponseWaitMessageQueue();
	boolean isTerminated = false;

	RemoteFirmwareUpdateHelper rfuHelper = null;

	public MGW300(short deviceId) {
		this();
		this.deviceId = deviceId;
		new Thread(this).start();

		MyLog.lWithSysOut("Create MGW300: " + HexUtils.toHexString(deviceId));
	}

	public MGW300() {
		deviceType = E_DeviceType.MGW300;

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isTerminated == false) {
					responseWaitMessageQueue.increaseTimerOfMessages();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();

		new DeviceStateChangedEvent(this, "", null);
	}

	public void addLC(LC100 device) {
		device.setXNetHandler(xnetHandler);
		device.setParent(this);
		lcMap.put(device.getId(), device);

		new DeviceStateChangedEvent(device, "", null);
	}

	public boolean removeLC(LC100 device) {
		if (lcMap.containsValue(device)) {
			device.terminate();
			lcMap.remove(device.getId());
			new DeviceStateChangedEvent(this, "", null);
		} else
			return false;
		return true;
	}

	public LC100 getLC(short id) {
		return (LC100) lcMap.get(id);
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		if (xnetHandler == null)
			return NETWORK_STATE.OFFLINE;
		else if (xnetHandler.isConnected() == false)
			return NETWORK_STATE.OFFLINE;
		return NETWORK_STATE.ONLINE;
	}

	@Override
	public E_DeviceType getDeviceType() {
		// TODO Auto-generated method stub
		return deviceType;
	}

	public void handleMessage(I_Message msg) {
		if (msg instanceof I_NCP_Message) {
			I_NCP_Message ncpMsg = (I_NCP_Message) msg;
			switch (ncpMsg.getMessageType()) {
			case REQ_REGISTER:
				handleRegisterReq(ncpMsg);
				break;
			case REQ_PING:
				handlePingReq(ncpMsg);
				break;
			case REQ_REGISTER_NODE:
				handleRegisterNodeReq(ncpMsg);
				break;
			case SEND_TIME_INFO:
				handleSendTimeInfo(ncpMsg);
				break;
			}
		} else if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			switch (gcpMsg.getMessageType()) {
			case RES_NUMOFDEVICE:
				handleNumOfDeviceRes(gcpMsg);
				break;
			case REQ_NUMOFDEVICE:
				handleNumOfDeviceReq(gcpMsg);
				break;
			case RES_DEVICEINFO:
				handleDeviceInfoRes(gcpMsg);
				break;
			case REQ_DEVICEINFO:
				handleDeviceInfoReq(gcpMsg);
				break;
			case RES_STATEINFO:
				handleStateInfoRes(gcpMsg);
				break;
			case REQ_STATEINFO:
				handleStateInfoReq(gcpMsg);
				break;
			case RES_CONTROL_CIRCUIT:
				handleControlCircuitRes(gcpMsg);
				break;
			case REQ_CONTROL_CIRCUIT:
				handleControlCircuitReq(gcpMsg);
				break;
			case RES_CONTROL_SENSOR:
				handleControlSensorRes(gcpMsg);
				break;
			case REQ_CONTROL_SENSOR:
				handleControlSensorReq(gcpMsg);
				break;
			case RES_CONTROL_SENSORSHARING:
				handleControlSensorSharingRes(gcpMsg);
				break;
			case REQ_CONTROL_SENSORSHARING:
				handleControlSensorSharingReq(gcpMsg);
				break;
			case RES_CONTROL_NOTICEMODE:
				handleControlNoticeModeRes(gcpMsg);
				break;
			case RES_CONTROL_GATEWAY:
				handleControlGatewayRes(gcpMsg);
				break;
			case REQ_CONTROL_GATEWAY:
				handleControlGatewayReq(gcpMsg);
				break;
			case RES_POWERMETER:
				handlePowermeterRes(gcpMsg);
				break;
			case REQ_POWERMETER:
				handlePowermeterReq(gcpMsg);
				break;
			case REQ_UPDATEPOWERMETER:
				handleUpdatePowermeter(gcpMsg);
				break;
			case RES_SENSOR_MAPPING_SIZE:
				handleSensorMappingSizeRes(gcpMsg);
				break;
			case REQ_SENSOR_MAPPING_SIZE:
				handleSensorMappingSizeReq(gcpMsg);
				break;
			case RES_DIMMER_MAPPING_SIZE:
				handleDimmerMappingSizeRes(gcpMsg);
				break;
			case REQ_DIMMER_MAPPING_SIZE:
				handleDimmerMappingSizeReq(gcpMsg);
				break;
			case RES_SCHEDULE_SIZE:
				handleScheduleSizeRes(gcpMsg);
				break;
			case REQ_SCHEDULE_SIZE:
				handleScheduleSizeReq(gcpMsg);
				break;
			case RES_SENSOR_MAPPING_INFO:
				handleSensorMappingInfoRes(gcpMsg);
				break;
			case REQ_SENSOR_MAPPING_INFO:
				handleSensorMappingInfoReq(gcpMsg);
				break;
			case RES_DIMMER_MAPPING_INFO:
				handleDimmerMappingInfoRes(gcpMsg);
				break;
			case REQ_DIMMER_MAPPING_INFO:
				handleDimmerMappingInfoReq(gcpMsg);
				break;
			case RES_SCHEDULE_INFO:
				handleScheduleInfoRes(gcpMsg);
				break;
			case REQ_SCHEDULE_INFO:
				handleScheduleInfoReq(gcpMsg);
				break;
			case RES_MAPPING_SENSOR:
				handleMappingSensorRes(gcpMsg);
				break;
			case REQ_MAPPING_SENSOR:
				handleMappingSensorReq(gcpMsg);
				break;
			case RES_MAPPING_DIMMER:
				handleMappingDimmerRes(gcpMsg);
				break;
			case REQ_MAPPING_DIMMER:
				handleMappingDimmerReq(gcpMsg);
				break;
			case RES_ADD_SCHEDULE:
				handleAddScheduleRes(gcpMsg);
				break;
			case REQ_ADD_SCHEDULE:
				handleAddScheduleReq(gcpMsg);
				break;
			case RES_MAPPING_SENSOR_LIST:
				handleMappingSensorListRes(gcpMsg);
				break;
			case REQ_MAPPING_SENSOR_LIST:
				handleMappingSensorListReq(gcpMsg);
				break;
			case RES_MAPPING_DIMMER_LIST:
				handleMappingDimmerListRes(gcpMsg);
				break;
			case REQ_MAPPING_DIMMER_LIST:
				handleMappingDimmerListReq(gcpMsg);
				break;
			case RES_ADD_SCHEDULE_LIST:
				handleAddScheduleListRes(gcpMsg);
				break;
			case REQ_ADD_SCHEDULE_LIST:
				handleAddScheduleListReq(gcpMsg);
				break;
			case RES_RESET_MAPPING_SENSOR:
				handleResetMappingSensorRes(gcpMsg);
				break;
			case REQ_RESET_MAPPING_SENSOR:
				handleResetMappingSensorReq(gcpMsg);
				break;
			case RES_RESET_MAPPING_DIMMER:
				handleResetMappingDimmerRes(gcpMsg);
				break;
			case REQ_RESET_MAPPING_DIMMER:
				handleResetMappingDimmerReq(gcpMsg);
				break;
			case RES_RESET_SCHEDULE:
				handleResetScheduleRes(gcpMsg);
				break;
			case REQ_RESET_SCHEDULE:
				handleResetScheduleReq(gcpMsg);
				break;
			case REQ_APPLYING_SCHEDULE:
				handleApplyingScheduleReq(gcpMsg);
				break;
			case RES_APPLYING_SCHEDULE:
				handleApplyingScheduleRes(gcpMsg);
				break;
			case NOTICE_EVENT:
				handleNoticeEvent(gcpMsg);
				break;
			case RES_SET_SENSING_LEVEL:
				handleSetSensingLevelRes(gcpMsg);
				break;
			case REQ_SET_SENSING_LEVEL:
				handleSetSensingLevelReq(gcpMsg);
				break;
			case RES_DEBUG_LOG:
				handleDebugLogRes(gcpMsg);
				break;
			case REQ_DEBUG_LOG:
				handleDebugLogReq(gcpMsg);
				break;
			case RES_DEBUG_LOG_LC:
				handleDebugLogLCRes(gcpMsg);
				break;
			case REQ_DEBUG_LOG_LC:
				handleDebugLogLCReq(gcpMsg);
				break;
			case REQ_RESET_DEBUG_LOG:
				handleResetDebugLogReq(gcpMsg);
				break;
			case REQ_RESET_DEBUG_LOG_LC:
				handleResetDebugLogLCReq(gcpMsg);
				break;
			case NOTICE_SENSOR_VALUE:
				handleNoticeSensorValue(gcpMsg);
				break;
			case REQ_IR_CONTROL:
				// TODO : 내용 없음
				handleIrControlReq(gcpMsg);
				break;
			case RES_IR_CONTROL:
				handleIrControlRes(gcpMsg);
				break;
			case TILT_MESSAGE:
				handleTiltMessage(gcpMsg);
				break;
			case BOAT_SENSOR_MESSAGE:
				handleBoatSensorMessage(gcpMsg);
				break;
			}
		} else if (msg instanceof I_RFUP_Message) {
			I_RFUP_Message rfupMsg = (I_RFUP_Message) msg;
			if (rfuHelper != null && rfuHelper.isComplete() == false)
				rfuHelper.handleResponseMessage(rfupMsg);
			switch (rfupMsg.getMessageType()) {
			case RES_UPGRADE:
				handleUpgradeRes(rfupMsg);
				break;
			case RES_SEND_DATA:
				handleSendData(rfupMsg);
				break;
			}

		}
	}

	private void handleIrControlRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleIrControlRes");
		GCP_Message_IrControlRes resMsg = (GCP_Message_IrControlRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleTiltMessage(I_GCP_Message gcpMsg) {
		MyLog.i("handleTiltMessage");
		GCP_Message_TiltMessage msg = (GCP_Message_TiltMessage) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = msg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleBoatSensorMessage(I_GCP_Message gcpMsg) {
		MyLog.i("handleTiltMessage");
		GCP_Message_BoatSensorMessage msg = (GCP_Message_BoatSensorMessage) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = msg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleIrControlReq(I_GCP_Message gcpMsg) {
		// TODO Auto-generated method stub

	}

	private void handleApplyingScheduleRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleApplyingScheduleRes");
		GCP_Message_ApplyingScheduleRes resMsg = (GCP_Message_ApplyingScheduleRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleApplyingScheduleReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleApplyingScheduleReq");
		GCP_Message_ApplyingScheduleReq resMsg = (GCP_Message_ApplyingScheduleReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleSendData(I_RFUP_Message rfupMsg) {
		MyLog.i("handleSendData");
		RFUP_Message_SendDataRes recvMsg = (RFUP_Message_SendDataRes) rfupMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		MyLog.i("DeviceType: " + recvMsg.getDeviceType());

		if (recvMsg.getDeviceType() != E_DeviceType.MGW300) {
			LC100 lc = getLC(recvMsg.getLcId());
			if (lc != null)
				lc.handleMessage((I_Message) recvMsg);
		}

	}

	private void handleUpgradeRes(I_RFUP_Message rfupMsg) {
		MyLog.i("handleUpgradeRes");
		RFUP_Message_UpgradeRes recvMsg = (RFUP_Message_UpgradeRes) rfupMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		MyLog.i("DeviceType: " + recvMsg.getDeviceType());

		if (recvMsg.getDeviceType() != E_DeviceType.MGW300) {
			LC100 lc = getLC(recvMsg.getLcId());
			if (lc != null)
				lc.handleMessage((I_Message) recvMsg);
		}
	}

	private void handleNoticeSensorValue(I_GCP_Message ssgcpMsg) {
		MyLog.i("handleNoticeSensorValue");
		GCP_Message_NoticeSensorValue recvMsg = (GCP_Message_NoticeSensorValue) ssgcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = recvMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) recvMsg);
	}

	private void handleResetScheduleReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetScheduleReq");
		GCP_Message_ResetScheduleReq reqMsg = (GCP_Message_ResetScheduleReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);

	}

	private void handleResetScheduleRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetScheduleRes");
		GCP_Message_ResetScheduleRes resMsg = (GCP_Message_ResetScheduleRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleAddScheduleListReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleAddScheduleListReq");
		GCP_Message_AddScheduleListReq reqMsg = (GCP_Message_AddScheduleListReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);

	}

	private void handleAddScheduleListRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleAddScheduleListRes");
		GCP_Message_AddScheduleListRes resMsg = (GCP_Message_AddScheduleListRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleAddScheduleReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleAddScheduleReq");
		GCP_Message_AddScheduleReq reqMsg = (GCP_Message_AddScheduleReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleAddScheduleRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleAddScheduleRes");
		GCP_Message_AddScheduleRes resMsg = (GCP_Message_AddScheduleRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleScheduleInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleScheduleInfoReq");
		GCP_Message_ScheduleInfoReq reqMsg = (GCP_Message_ScheduleInfoReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);

	}

	private void handleScheduleInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleScheduleInfoRes");
		GCP_Message_ScheduleInfoRes resMsg = (GCP_Message_ScheduleInfoRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);

	}

	private void handleScheduleSizeReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleScheduleSizeReq");
		GCP_Message_ScheduleSizeReq reqMsg = (GCP_Message_ScheduleSizeReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleScheduleSizeRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleScheduleSizeRes");
		GCP_Message_ScheduleSizeRes resMsg = (GCP_Message_ScheduleSizeRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleSendTimeInfo(I_NCP_Message ncpMsg) {
		MyLog.i("handleSendTimeInfo");
		NCP_Message_SendTimeInfo msg = (NCP_Message_SendTimeInfo) ncpMsg;
		MyLog.i("TimeInfo: " + msg.getDateTimeString());
		this.currentTime = Calendar.getInstance();
	}

	private void handleResetDebugLogLCReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleDebugLogLCReq");
		GCP_Message_ResetDebugLogLCReq reqMsg = (GCP_Message_ResetDebugLogLCReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);

	}

	private void handleResetDebugLogReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetDebugLogReq");

		this.currentTime = Calendar.getInstance();
		this.rebootCount = 0;
		this.serverConnectionCount = 0;
		this.lastRebootTime = Calendar.getInstance();
		this.lastServerConnectionTime = Calendar.getInstance();
	}

	private void handleDebugLogLCReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleDebugLogLCReq");
		GCP_Message_DebugLogLCReq reqMsg = (GCP_Message_DebugLogLCReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleDebugLogReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleDebugLogReq");
		GCP_Message_DebugLogReq reqMsg = (GCP_Message_DebugLogReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		byte[] currentTime = new byte[6];
		int rebootCount = 0;
		byte[] lastRebootTime = new byte[5];
		int serverConnectionCount = 0;
		byte[] lastServerConnectionTime = new byte[5];

		this.currentTime = Calendar.getInstance();

		currentTime[0] = new Integer(this.currentTime.get(Calendar.YEAR) % 100).byteValue();
		currentTime[1] = new Integer(this.currentTime.get(Calendar.MONTH) + 1).byteValue();
		currentTime[2] = new Integer(this.currentTime.get(Calendar.DAY_OF_MONTH)).byteValue();
		currentTime[3] = new Integer(this.currentTime.get(Calendar.HOUR_OF_DAY)).byteValue();
		currentTime[4] = new Integer(this.currentTime.get(Calendar.MINUTE)).byteValue();
		currentTime[4] = new Integer(this.currentTime.get(Calendar.SECOND)).byteValue();

		rebootCount = this.rebootCount;
		serverConnectionCount = this.serverConnectionCount;

		lastRebootTime[0] = new Integer(this.lastRebootTime.get(Calendar.YEAR) % 100).byteValue();
		lastRebootTime[1] = new Integer(this.lastRebootTime.get(Calendar.MONTH) + 1).byteValue();
		lastRebootTime[2] = new Integer(this.lastRebootTime.get(Calendar.DAY_OF_MONTH)).byteValue();
		lastRebootTime[3] = new Integer(this.lastRebootTime.get(Calendar.HOUR)).byteValue();
		lastRebootTime[4] = new Integer(this.lastRebootTime.get(Calendar.MINUTE)).byteValue();

		lastServerConnectionTime[0] = new Integer(this.lastServerConnectionTime.get(Calendar.YEAR) % 100).byteValue();
		lastServerConnectionTime[1] = new Integer(this.lastServerConnectionTime.get(Calendar.MONTH) + 1).byteValue();
		lastServerConnectionTime[2] = new Integer(this.lastServerConnectionTime.get(Calendar.DAY_OF_MONTH)).byteValue();
		lastServerConnectionTime[3] = new Integer(this.lastServerConnectionTime.get(Calendar.HOUR)).byteValue();
		lastServerConnectionTime[4] = new Integer(this.lastServerConnectionTime.get(Calendar.MINUTE)).byteValue();

		GCP_Message_DebugLogRes resMsg = new GCP_Message_DebugLogRes(destId, srcId, currentTime, rebootCount, lastRebootTime, serverConnectionCount,
				lastServerConnectionTime);
		resMsg.setSeqNum(reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleSetSensingLevelReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleSetSensingLevelReq");
		GCP_Message_SetSensingLevelReq reqMsg = (GCP_Message_SetSensingLevelReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleResetMappingDimmerReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetMappingDimmerReq");
		GCP_Message_ResetMappingDimmerReq reqMsg = (GCP_Message_ResetMappingDimmerReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleResetMappingSensorReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetMappingSensorReq");
		GCP_Message_ResetMappingSensorReq reqMsg = (GCP_Message_ResetMappingSensorReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleMappingDimmerListReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingDimmerListReq");
		GCP_Message_MappingDimmerListReq reqMsg = (GCP_Message_MappingDimmerListReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleMappingSensorListReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingSensorListReq");
		GCP_Message_MappingSensorListReq reqMsg = (GCP_Message_MappingSensorListReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleMappingDimmerReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingDimmerReq");
		GCP_Message_MappingDimmerReq reqMsg = (GCP_Message_MappingDimmerReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);

	}

	private void handleMappingSensorReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingSensorReq");
		GCP_Message_MappingSensorReq reqMsg = (GCP_Message_MappingSensorReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleDimmerMappingInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleDimmerMappingInfoReq");
		GCP_Message_DimmerMappingInfoReq reqMsg = (GCP_Message_DimmerMappingInfoReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);

	}

	private void handleSensorMappingInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleSensorMappingInfoReq");
		GCP_Message_SensorMappingInfoReq reqMsg = (GCP_Message_SensorMappingInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleDimmerMappingSizeReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleDimmerMappingSizeReq");
		GCP_Message_DimmerMappingSizeReq reqMsg = (GCP_Message_DimmerMappingSizeReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleSensorMappingSizeReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleSensorMappingSizeReq");
		GCP_Message_SensorMappingSizeReq reqMsg = (GCP_Message_SensorMappingSizeReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleUpdatePowermeter(I_GCP_Message gcpMsg) {
		MyLog.i("handleUpdatePowermeter");
		GCP_Message_UpdatePowermeterReq reqMsg = (GCP_Message_UpdatePowermeterReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();

		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handlePowermeterReq(I_GCP_Message gcpMsg) {
		MyLog.i("handlePowermeterReq");
		GCP_Message_PowermeterReq reqMsg = (GCP_Message_PowermeterReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		LC100 lc = getLC(subNodeId);
		if (lc != null)
			lc.handleMessage((I_Message) gcpMsg);
	}

	private void handleControlGatewayReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlGatewayReq");
		GCP_Message_ControlGatewayReq reqMsg = (GCP_Message_ControlGatewayReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		String ipAddress = reqMsg.getDeviceIp();
		String serverIpAddress = reqMsg.getServerIp();
		String defaultGateway = reqMsg.getDefaultGatewayIp();
		String netmask = reqMsg.getNetmask();
		int port = reqMsg.getPort();
		boolean isSensorSharingMode = reqMsg.isSensorSharingMode();

		MyLog.i("ipAddress: " + ipAddress);
		MyLog.i("serverIpAddress: " + serverIpAddress);
		MyLog.i("defaultGateway: " + defaultGateway);
		MyLog.i("netmask: " + netmask);
		MyLog.i("port: " + port);
		MyLog.i("isSensorSharingMode: " + isSensorSharingMode);

		this.ipAddress = ipAddress;
		this.serverIpAddress = serverIpAddress;
		this.defaultGateway = defaultGateway;
		this.netMask = netmask;
		this.port = port;
		this.isSensorSharingMode = isSensorSharingMode;

		GCP_Message_ControlGatewayRes resMsg = new GCP_Message_ControlGatewayRes(srcId, destId, E_GCP_Result.SUCCESS);

		resMsg.setSeqNum(gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);
	}

	private void handleControlSensorSharingReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlSensorSharingReq");
		GCP_Message_ControlSensorSharingReq reqMsg = (GCP_Message_ControlSensorSharingReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		boolean enable = reqMsg.isEnable();

		MyLog.i("enable: " + enable);

		this.isSensorSharingMode = enable;

		GCP_Message_ControlSensorSharingRes resMsg = new GCP_Message_ControlSensorSharingRes(srcId, destId, E_GCP_Result.SUCCESS);

		resMsg.setSeqNum(gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);
	}

	private void handleControlSensorReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlSensorReq");
		GCP_Message_ControlSensorReq reqMsg = (GCP_Message_ControlSensorReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleControlCircuitReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlCircuitReq");
		GCP_Message_ControlCircuitReq reqMsg = (GCP_Message_ControlCircuitReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleStateInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleStateInfoReq");
		GCP_Message_StateInfoReq reqMsg = (GCP_Message_StateInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();
		short deviceId = reqMsg.getDeviceId();

		GCP_Message_StateInfoRes resMsg = null;

		if (subNodeId == 0 && deviceId == 0) {
			try {
				resMsg = new GCP_Message_StateInfoRes(srcId, destId, subNodeId, deviceId, getDeviceType().toShort(), ipAddress, serverIpAddress,
						defaultGateway, netMask, port, isSensorSharingMode);
				resMsg.setSeqNum(gcpMsg.getSeqNum());
				sendMessage((I_Message) resMsg);
			} catch (UnformattedPacketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				MyLog.e(e.getMessage());
			}
		} else if (subNodeId == 0) {
			LC100 device = getLC(deviceId);
			if (device == null)
				return;
			device.handleMessage((I_Message) gcpMsg);
		} else {
			LC100 device = getLC(subNodeId);
			if (device == null)
				return;
			device.handleMessage((I_Message) gcpMsg);
		}
	}

	private void handleDeviceInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleDeviceInfoReq");
		GCP_Message_DeviceInfoReq reqMsg = (GCP_Message_DeviceInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();
		int index = reqMsg.getIndex();

		I_Device device = null;
		device = getLC(subNodeId);
		if (device == null)
			return;
		device.handleMessage((I_Message) gcpMsg);
	}

	public I_Device getLCByIndex(int index) {
		if (lcMap.size() <= index)
			return null;
		int cnt = 0;
		Collection<I_Device> devices = lcMap.values();
		for (I_Device device : devices) {
			if (cnt == index)
				return device;
			cnt++;
		}
		return null;
	}

	private void handleNumOfDeviceReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleNumOfDeviceReq");
		GCP_Message_NumOfDeviceReq reqMsg = (GCP_Message_NumOfDeviceReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		int numOfDevice = 0;
		if (subNodeId == 0)
			numOfDevice = lcMap.size();
		else if (getLC(subNodeId) != null) {
			getLC(subNodeId).handleMessage((I_Message) gcpMsg);
			return;
		} else
			return;

		GCP_Message_NumOfDeviceRes resMsg = new GCP_Message_NumOfDeviceRes(srcId, destId, subNodeId, numOfDevice);
		resMsg.setSeqNum(gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);
	}

	private void handleDebugLogLCRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleDebugLogLCRes");
		GCP_Message_DebugLogLCRes resMsg = (GCP_Message_DebugLogLCRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleDebugLogRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleDebugLogRes");
		GCP_Message_DebugLogRes resMsg = (GCP_Message_DebugLogRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		byte[] currentTime = resMsg.getCurrentTime();
		int rebootCount = resMsg.getRebootCount();
		byte[] lastRebootTime = resMsg.getLastRebootTime();
		int serverConnectionCount = resMsg.getServerConnectionCount();
		byte[] lastServerConnectionTime = resMsg.getLastServerConnectionTime();

		String str = "";
		for (int i = 0; i < currentTime.length; i++)
			str += currentTime[i] + ", ";
		MyLog.i("currentTime: " + str);
		MyLog.i("rebootCount: " + rebootCount);
		str = "";
		for (int i = 0; i < lastRebootTime.length; i++)
			str += lastRebootTime[i];
		MyLog.i("lastRebootTime: " + str);
		MyLog.i("serverConnectionCount: " + serverConnectionCount);
		str = "";
		for (int i = 0; i < lastServerConnectionTime.length; i++)
			str += lastServerConnectionTime[i];
		MyLog.i("lastServerConnectionTime: " + str);

		this.rebootCount = rebootCount;
		this.serverConnectionCount = serverConnectionCount;
		this.currentTime.set(Calendar.YEAR, 2000 + HexUtils.toInt(HexUtils.toHexString(currentTime[0])));
		this.currentTime.set(Calendar.MONTH, HexUtils.toInt(HexUtils.toHexString(currentTime[1])) - 1);
		this.currentTime.set(Calendar.DAY_OF_MONTH, HexUtils.toInt(HexUtils.toHexString(currentTime[2])));
		this.currentTime.set(Calendar.HOUR_OF_DAY, HexUtils.toInt(HexUtils.toHexString(currentTime[3])));
		this.currentTime.set(Calendar.MINUTE, HexUtils.toInt(HexUtils.toHexString(currentTime[4])));
		this.currentTime.set(Calendar.SECOND, HexUtils.toInt(HexUtils.toHexString(currentTime[5])));

		this.lastRebootTime.set(Calendar.YEAR, 2000 + HexUtils.toInt(HexUtils.toHexString(lastRebootTime[0])));
		this.lastRebootTime.set(Calendar.MONTH, HexUtils.toInt(HexUtils.toHexString(lastRebootTime[1])) - 1);
		this.lastRebootTime.set(Calendar.DAY_OF_MONTH, HexUtils.toInt(HexUtils.toHexString(lastRebootTime[2])));
		this.lastRebootTime.set(Calendar.HOUR_OF_DAY, HexUtils.toInt(HexUtils.toHexString(lastRebootTime[3])));
		this.lastRebootTime.set(Calendar.MINUTE, HexUtils.toInt(HexUtils.toHexString(lastRebootTime[4])));

		this.lastServerConnectionTime.set(Calendar.YEAR, 2000 + HexUtils.toInt(HexUtils.toHexString(lastServerConnectionTime[0])));
		this.lastServerConnectionTime.set(Calendar.MONTH, HexUtils.toInt(HexUtils.toHexString(lastServerConnectionTime[1])) - 1);
		this.lastServerConnectionTime.set(Calendar.DAY_OF_MONTH, HexUtils.toInt(HexUtils.toHexString(lastServerConnectionTime[2])));
		this.lastServerConnectionTime.set(Calendar.HOUR_OF_DAY, HexUtils.toInt(HexUtils.toHexString(lastServerConnectionTime[3])));
		this.lastServerConnectionTime.set(Calendar.MINUTE, HexUtils.toInt(HexUtils.toHexString(lastServerConnectionTime[4])));

		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null)
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

		new DeviceStateChangedEvent(this, "", null);
	}

	private void handleSetSensingLevelRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleSetSensingLevelRes");
		GCP_Message_SetSensingLevelRes resMsg = (GCP_Message_SetSensingLevelRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleResetMappingDimmerRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetMappingDimmerRes");
		GCP_Message_ResetMappingDimmerRes resMsg = (GCP_Message_ResetMappingDimmerRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleResetMappingSensorRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleResetMappingSensorRes");
		GCP_Message_ResetMappingSensorRes resMsg = (GCP_Message_ResetMappingSensorRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleMappingDimmerListRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingDimmerListRes");
		GCP_Message_MappingDimmerListRes resMsg = (GCP_Message_MappingDimmerListRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleMappingSensorListRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingDimmerRes");
		GCP_Message_MappingSensorListRes resMsg = (GCP_Message_MappingSensorListRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleMappingDimmerRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingDimmerRes");
		GCP_Message_MappingDimmerRes resMsg = (GCP_Message_MappingDimmerRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleMappingSensorRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleMappingSensorRes");
		GCP_Message_MappingSensorRes resMsg = (GCP_Message_MappingSensorRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleDimmerMappingInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleDimmerMappingInfoRes");
		GCP_Message_DimmerMappingInfoRes resMsg = (GCP_Message_DimmerMappingInfoRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleSensorMappingInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleSensorMappingInfoRes");
		GCP_Message_SensorMappingInfoRes resMsg = (GCP_Message_SensorMappingInfoRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleDimmerMappingSizeRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleDimmerMappingSizeRes");
		GCP_Message_DimmerMappingSizeRes resMsg = (GCP_Message_DimmerMappingSizeRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleSensorMappingSizeRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleSensorMappingSizeRes");
		GCP_Message_SensorMappingSizeRes resMsg = (GCP_Message_SensorMappingSizeRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handlePowermeterRes(I_GCP_Message gcpMsg) {
		MyLog.i("handlePowermeterRes");
		GCP_Message_PowermeterRes resMsg = (GCP_Message_PowermeterRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleControlGatewayRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlGatewayRes");
		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_ControlGatewayReq) {
			GCP_Message_ControlGatewayReq reqMsg = (GCP_Message_ControlGatewayReq) reqIMsg;
			GCP_Message_ControlGatewayRes resMsg = (GCP_Message_ControlGatewayRes) gcpMsg;

			new ReceiveResponseMessageEvent(this, reqMsg, resMsg, "", null);

			if (resMsg.getResult() == E_GCP_Result.SUCCESS) {
				boolean stateChanged = false;
				if (this.ipAddress.equals(reqMsg.getDeviceIp()) == false) {
					this.ipAddress = reqMsg.getDeviceIp();
					stateChanged = true;
				}
				if (this.defaultGateway.equals(reqMsg.getDefaultGatewayIp()) == false) {
					this.defaultGateway = reqMsg.getDefaultGatewayIp();
					stateChanged = true;
				}
				if (this.netMask.equals(reqMsg.getNetmask()) == false) {
					this.netMask = reqMsg.getNetmask();
					stateChanged = true;
				}
				if (this.serverIpAddress.equals(reqMsg.getServerIp()) == false) {
					this.serverIpAddress = reqMsg.getServerIp();
					stateChanged = true;
				}
				if (this.port != reqMsg.getPort()) {
					this.port = reqMsg.getPort();
					stateChanged = true;
				}
				if (this.isSensorSharingMode != reqMsg.isSensorSharingMode()) {
					this.isSensorSharingMode = reqMsg.isSensorSharingMode();
					stateChanged = true;
				}

				if (stateChanged)
					new DeviceStateChangedEvent(this, "", null);
			}
		}
	}

	private void handleControlSensorSharingRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlSensorSharingRes");
		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg instanceof GCP_Message_ControlSensorSharingReq) {
			GCP_Message_ControlSensorSharingReq reqMsg = (GCP_Message_ControlSensorSharingReq) reqIMsg;
			GCP_Message_ControlSensorSharingRes resMsg = (GCP_Message_ControlSensorSharingRes) gcpMsg;
			if (resMsg.getResult() == E_GCP_Result.SUCCESS) {
				if (this.isSensorSharingMode != reqMsg.isEnable()) {
					this.isSensorSharingMode = reqMsg.isEnable();
					new DeviceStateChangedEvent(this, "", null);
				}
			}

			new ReceiveResponseMessageEvent(this, reqMsg, resMsg, "", null);
		}
	}

	private void handleControlNoticeModeRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlNoticeModeRes");
		I_Message reqIMsg = responseWaitMessageQueue.removeRequestMessageOfResponseMessage((I_Message) gcpMsg);

		if (reqIMsg instanceof GCP_Message_ControlNoticeModeReq) {
			GCP_Message_ControlNoticeModeReq reqMsg = (GCP_Message_ControlNoticeModeReq) reqIMsg;
			GCP_Message_ControlNoticeModeRes resMsg = (GCP_Message_ControlNoticeModeRes) gcpMsg;

			new ReceiveResponseMessageEvent(this, reqMsg, resMsg, "", null);
		}
	}

	private void handleControlSensorRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlSensorRes");
		GCP_Message_ControlSensorRes resMsg = (GCP_Message_ControlSensorRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleControlCircuitRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleControlCircuitRes");
		GCP_Message_ControlCircuitRes reqMsg = (GCP_Message_ControlCircuitRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleStateInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleStateInfoRes");
		GCP_Message_StateInfoRes resMsg = (GCP_Message_StateInfoRes) gcpMsg;

		MyLog.i("subNodeId: " + resMsg.getSubNodeId());
		MyLog.i("deviceId: " + resMsg.getDeviceId());

		if (resMsg.getSubNodeId() == (short) 0x0000 && resMsg.getDeviceType() == getDeviceType().toShort()) {// Gateway
			// info
			boolean stateChanged = false;
			if (this.ipAddress.equals(resMsg.getDeviceIp()) == false) {
				this.ipAddress = resMsg.getDeviceIp();
				stateChanged = true;
			}
			if (this.defaultGateway.equals(resMsg.getDefaultGatewayIp()) == false) {
				this.defaultGateway = resMsg.getDefaultGatewayIp();
				stateChanged = true;
			}
			if (this.netMask.equals(resMsg.getNetmask()) == false) {
				this.netMask = resMsg.getNetmask();
				stateChanged = true;
			}
			if (this.serverIpAddress.equals(resMsg.getServerIp()) == false) {
				this.serverIpAddress = resMsg.getServerIp();
				stateChanged = true;
			}
			if (this.port != resMsg.getPort()) {
				this.port = resMsg.getPort();
				stateChanged = true;
			}
			if (this.isSensorSharingMode != resMsg.isSensorSharingMode()) {
				this.isSensorSharingMode = resMsg.isSensorSharingMode();
				stateChanged = true;
			}

			if (stateChanged)
				new DeviceStateChangedEvent(this, "", null);

			MyLog.i("IpAddress: " + ipAddress);
			MyLog.i("defaultGateway: " + defaultGateway);
			MyLog.i("serverIpAddress: " + serverIpAddress);
			MyLog.i("netMask: " + netMask);
			MyLog.i("port: " + port);
			MyLog.i("isSensorSharingMode: " + isSensorSharingMode);
		} else {
			I_Device device = getLC(resMsg.getSubNodeId());

			if (device == null)
				addLC(new LC100(this, resMsg.getSubNodeId()));
			device = getLC(resMsg.getSubNodeId());

			if (device != null)
				device.handleMessage((I_Message) resMsg);
		}
	}

	private void handleDeviceInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleDeviceInfoRes");
		GCP_Message_DeviceInfoRes resMsg = (GCP_Message_DeviceInfoRes) gcpMsg;

		short subNodeId = resMsg.getSubNodeId();
		short deviceId = resMsg.getDeviceId();
		short deviceType = resMsg.getDeviceType();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("deviceId: " + deviceId);
		MyLog.i("deviceType: " + HexUtils.toHexString(deviceType));

		if (resMsg.getSubNodeId() == (short) 0x0000 && resMsg.getDeviceType() == E_DeviceType.LC100.toShort()) {
			LC100 lc = getLC(resMsg.getDeviceId());
			if (lc == null)
				addLC(new LC100(this, resMsg.getDeviceId()));
			lc = getLC(resMsg.getDeviceId());
			lc.handleMessage((I_Message) gcpMsg);
		} else if (resMsg.getSubNodeId() != (short) 0x0000) {
			LC100 lc = getLC(resMsg.getSubNodeId());
			if (lc == null) {
				addLC(new LC100(this, resMsg.getSubNodeId()));
			}
			lc = getLC(resMsg.getSubNodeId());
			lc.handleMessage((I_Message) gcpMsg);
		}
	}

	private void handleNumOfDeviceRes(I_GCP_Message gcpMsg) {
		MyLog.i("handleNumOfDeviceRes");
		GCP_Message_NumOfDeviceRes resMsg = (GCP_Message_NumOfDeviceRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getSubNodeId();
		int numOfDevice = resMsg.getNumOfDevice();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("numOfDevice: " + numOfDevice);

		if (subNodeId == 0) {

			for (int i = 0; i < numOfDevice; i++) {
				GCP_Message_DeviceInfoReq reqMsg = new GCP_Message_DeviceInfoReq(srcId, destId, subNodeId, i);
				sendMessage((I_Message) reqMsg);
			}
		} else if (getLC(subNodeId) != null) {
			getLC(subNodeId).handleMessage((I_Message) gcpMsg);
			return;
		} else
			return;
	}

	private void handleNoticeEvent(I_GCP_Message gcpMsg) {
		MyLog.i("handleNoticeEvent");
		GCP_Message_NoticeEvent reqMsg = (GCP_Message_NoticeEvent) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();

		I_Device device = getLC(subNodeId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleRegisterNodeReq(I_NCP_Message ncpMsg) {
		MyLog.i("handleRegisterNodeReq");
		NCP_Message_RegisterNodeReq reqMsg = ((NCP_Message_RegisterNodeReq) ncpMsg);
		LC100 lc = getLC(reqMsg.getSubNodeId());
		if (lc == null)
			addLC(new LC100(this, reqMsg.getSubNodeId()));
		lc = getLC(reqMsg.getSubNodeId());

		lc.handleMessage((I_Message) ncpMsg);
	}

	private void handlePingReq(I_NCP_Message ncpMsg) {
		MyLog.i("handlePingReq");

		int requestRegister = 0;
		if (isReceiveRegReq == false)
			requestRegister = 1;

		NCP_Message_PingRes resMsg = new NCP_Message_PingRes((short) 0x0000, ncpMsg.getSrcId(), 0x00, requestRegister, 0x0A);
		resMsg.setSeqNum(ncpMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleRegisterReq(I_NCP_Message ncpMsg) {
		MyLog.i("handleRegisterReq");
		NCP_Message_RegisterReq reqMsg = (NCP_Message_RegisterReq) ncpMsg;
		short srcId = (short) 0x0000;
		short destId = ncpMsg.getSrcId();

		this.firmwareVersion = reqMsg.getFirmwareVersion();
		this.deviceVersion = reqMsg.getDeviceVersion();

		NCP_Message_RegisterRes resMsg = new NCP_Message_RegisterRes(srcId, destId, 0x0000, 0x20);
		resMsg.setSeqNum(ncpMsg.getSeqNum());
		sendMessage(resMsg);

		isReceiveRegReq = true;

		try {
			String dateTime = "";
			Calendar cal = Calendar.getInstance();
			dateTime = String.format("%04d%02d%02d%1d%02d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), cal
					.get(Calendar.DAY_OF_WEEK), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

			NCP_Message_SendTimeInfo timeInfoMsg = new NCP_Message_SendTimeInfo(srcId, destId, 1, dateTime);
			sendMessage(timeInfoMsg);
		} catch (Exception e) {
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
	}

	@Override
	public synchronized void setXNetHandler(I_XNetHandler xnetHandler) {
		if (this.xnetHandler != null && this.xnetHandler != xnetHandler) {
			this.xnetHandler.terminate();
		}

		try {
			Thread.sleep(3000);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		isReceiveRegReq = false;
		MyLog.lWithSysOut("setXNetHandler: id(" + getId() + ")");

		this.xnetHandler = xnetHandler;
		xnetHandler.setParent(this);
		Collection<I_Device> devices = lcMap.values();
		for (I_Device device : devices)
			device.setXNetHandler(xnetHandler);

		sendStateInfoReq();
		sendTimeInfo();
		try {
			sendDebugLogReq();
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
	}

	@Override
	public void run() {
		Calendar lastSentTime_TimeInfo = Calendar.getInstance();

		while (isTerminated == false) {
			try {
				if (xnetHandler != null) {
					I_Message msg = null;
					while ((msg = xnetHandler.removeMessage()) != null) {
						handleMessage(msg);
						/*
						 * try { Thread.sleep(5); } catch (InterruptedException
						 * e) { // TODO Auto-generated catch block
						 * e.printStackTrace(); }
						 */
					}
				}
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
				MyLog.e(e.getMessage());
			}

			if (lastSentTime_TimeInfo.getTimeInMillis() + 600000 < Calendar.getInstance().getTimeInMillis()) {// 10분에
				// 한번

				sendTimeInfo();
				lastSentTime_TimeInfo = Calendar.getInstance();
			}
		}
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getServerIpAddress() {
		return serverIpAddress;
	}

	public String getDefaultGateway() {
		return defaultGateway;
	}

	public String getNetMask() {
		return netMask;
	}

	public int getPort() {
		return port;
	}

	public boolean isSensorSharingMode() {
		return isSensorSharingMode;
	}

	public int getNumOfSubDevices() {
		return lcMap.size();
	}

	public boolean sendMessage(I_Message msg) {
		if (xnetHandler != null)
			return xnetHandler.sendMessage((I_Message) msg);
		return false;
	}

	public boolean sendRegisterReq() {
		MyLog.i("sendRegisterReq");
		return sendMessage(new NCP_Message_RegisterReq(getId(), (short) 0x0000, getDeviceType().toShort(), 0x00, 0x11));
	}

	public boolean sendPingReq() {
		MyLog.i("sendPingReq");
		return sendMessage(new NCP_Message_PingReq(getId(), (short) 0x0000, 0x00, 0x00));
	}

	public boolean sendTimeInfo() {
		MyLog.i("sendTimeInfo");
		try {
			String dateTime = "";
			Calendar cal = Calendar.getInstance();
			dateTime = String.format("%04d%02d%02d%1d%02d%02d%02d", cal.get(Calendar.YEAR), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), cal
					.get(Calendar.DAY_OF_WEEK), cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));

			NCP_Message_SendTimeInfo timeInfoMsg = new NCP_Message_SendTimeInfo(getId(), getId(), 1, dateTime);
			sendMessage(timeInfoMsg);
		} catch (Exception e) {
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
		return true;
	}

	public void sendRegisterNodeReq(I_Device device) {
		MyLog.i("sendRegisterNodeReq");
		short srcId = getId();
		short destId = (short) 0x0000;
		short subSrcId = device.getId();
		short deviceType = device.getDeviceType().toShort();
		short deviceId = device.getId();
		int deviceVersion = 0;
		int firmwareVersion = 1;
		int netAddr = 0;

		I_NCP_Message msg = new NCP_Message_RegisterNodeReq(srcId, destId, subSrcId, deviceType, deviceId, deviceVersion, firmwareVersion, netAddr);
		sendMessage((I_Message) msg);
	}

	public boolean sendNumOfDeviceReq() {
		MyLog.i("sendNumOfDeviceReq");

		short srcId = getId();
		short destId = (short) 0x0000;
		I_GCP_Message msg = new GCP_Message_NumOfDeviceReq(srcId, destId, (short) 0x0000);
		return sendMessageWithResponse((I_Message) msg);
	}

	private boolean sendMessageWithResponse(I_Message msg) {
		responseWaitMessageQueue.addMessageToResponseWaitMessageQueue((I_Message) msg);
		if (xnetHandler != null) {

			return xnetHandler.sendMessageWithResponse((I_Message) msg);
		}

		return false;
	}

	public boolean sendDeviceInfoReq(short subNodeId, int index) {
		MyLog.i("sendDeviceInfoReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_DeviceInfoReq(srcId, destId, subNodeId, index);
		return sendMessageWithResponse((I_Message) msg);
	}

	public boolean sendStateInfoReq() {
		MyLog.i("sendStateInfoReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_StateInfoReq(srcId, destId, (short) 0x0000, (short) 0x0000);
		return sendMessageWithResponse((I_Message) msg);
	}

	public int sendControlSensorSharing(boolean enable) {
		MyLog.i("sendControlSensorSharing");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ControlSensorSharingReq(srcId, destId, enable);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendControlGateway(String deviceIp, String serverIp, String defaultGatewayIp, String netmask, int port, boolean isSensorSharingMode)
			throws UnformattedPacketException {
		MyLog.i("sendControlGateway");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ControlGatewayReq(srcId, destId, deviceIp, serverIp, defaultGatewayIp, netmask, port, isSensorSharingMode);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendDebugLogReq() throws UnformattedPacketException {
		MyLog.i("sendDebugLogReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_DebugLogReq(srcId, destId);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public boolean sendRebootReq() {
		MyLog.i("sendRebootReq");

		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = (short) 0x0000;
		short deviceId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_RebootReq(srcId, destId, subNodeId, deviceId);
		return sendMessage((I_Message) msg);
	}

	public boolean sendResetDebugLogReq() {
		MyLog.i("sendResetDebugLogReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ResetDebugLogReq(srcId, destId);
		boolean result = sendMessage((I_Message) msg);
		try {
			sendDebugLogReq();
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
		return result;
	}

	@Override
	public I_Device getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	public Calendar getCurrentTime() {
		return currentTime;
	}

	public int getRebootCount() {
		return rebootCount;
	}

	public Calendar getLastRebootTime() {
		return lastRebootTime;
	}

	public int getServerConnectionCount() {
		return serverConnectionCount;
	}

	public Calendar getLastServerConnectionTime() {
		return lastServerConnectionTime;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public int getDeviceVersion() {
		return deviceVersion;
	}

	public void termiate() {
		this.isTerminated = true;
		xnetHandler.terminate();
		for (I_Device device : lcMap.values()) {
			((LC100) device).terminate();
		}
	}

	@Override
	public I_XNetHandler getXNetHandler() {
		return this.xnetHandler;
	}

	public int sendFirmwareUpgradeReq(byte[] data) {
		MyLog.l("sendFirmwareUpgradeReq");

		if (rfuHelper == null)
			rfuHelper = new RemoteFirmwareUpdateHelper(this, data);
		else if (rfuHelper.isComplete())
			rfuHelper = new RemoteFirmwareUpdateHelper(this, data);

		return -1;
	}

	public int sendControlNoticeMode(boolean enable) {
		MyLog.i("sendControlNoticeMode");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ControlNoticeModeReq(srcId, destId, enable);
		if (sendMessageWithResponse((I_Message) msg) == false) {
			new FailToSendMessageEvent(this, (I_Message) msg, "", null);
		}
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

}
