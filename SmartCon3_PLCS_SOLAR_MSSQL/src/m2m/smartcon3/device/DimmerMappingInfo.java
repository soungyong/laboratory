package m2m.smartcon3.device;

import java.io.Serializable;

public class DimmerMappingInfo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1319386046219889251L;
	short dimmerId;
	int channelId;
	int circuitId;

	public DimmerMappingInfo(short dimmerId, int channelId, int circuitId) {
		this.dimmerId = dimmerId;
		this.channelId = channelId;
		this.circuitId = circuitId;
	}

	public short getDimmerId() {
		return dimmerId;
	}
	
	public void setDimmerId(short dimmerId) {
		this.dimmerId = dimmerId;
	}
	
	public int getChannelId() {
		return channelId;
	}

	public void setChannelId(int channelId) {
		this.channelId = channelId;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public void setCircuitId(int circuitId) {
		this.circuitId = circuitId;
	}
}
