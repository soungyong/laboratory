package m2m.smartcon3.device;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import m2m.smartcon3.device.I_Device.NETWORK_STATE;
import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.devicenetwork.ResponseWaitMessage;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp1_4.*;
import m2m.smartcon3.protocol.ncp1_0.*;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class OldLC100 implements I_Device, Runnable {
	I_XNetHandler xnetHandler = null;
	OldMGW300 parent = null;
	short deviceId = 0x00;
	E_DeviceType deviceType = E_DeviceType.LC100;
	int firmwareVersion = 0;
	short panId = 0xff;
	HashMap<Short, I_Device> subDeviceMap = new HashMap<Short, I_Device>();
	List<SensorMappingInfo> sensorMappingList = new LinkedList<SensorMappingInfo>();
	List<DimmerMappingInfo> dimmerMappingList = new LinkedList<DimmerMappingInfo>();

	E_GCP_ControlMode controlInfo[] = new E_GCP_ControlMode[16];
	E_GCP_ControlMode stateInfo[] = new E_GCP_ControlMode[16];
	private int powermeterValue = 0;

	boolean isTerminated = false;

	private int rebootCount = 0;
	private int serverConnectionCount = 0;
	private Calendar lastRebootTime = Calendar.getInstance();
	private Calendar lastServerConnectionTime = Calendar.getInstance();

	LinkedList<ResponseWaitMessage> _responseWaitMessageQueue;

	NETWORK_STATE networkState = NETWORK_STATE.OFFLINE;
	private Calendar lastMessageReceivingTime = Calendar.getInstance();

	public OldLC100(short deviceId) {
		this.deviceId = deviceId;
		for (int i = 0; i < 16; i++) {
			stateInfo[i] = E_GCP_ControlMode.DEFAULT;
			controlInfo[i] = E_GCP_ControlMode.DEFAULT;
		}

		_responseWaitMessageQueue = new LinkedList<ResponseWaitMessage>();

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isTerminated == false) {
					if (lastMessageReceivingTime.getTimeInMillis() + 10000 < System
							.currentTimeMillis()) {
						networkState = networkState.OFFLINE;
					}

					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}).start();

		new Thread(this).start();

		// 하위 노드들 state monitoring thread
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isTerminated == false) {
					try {
						Set<Short> ids = subDeviceMap.keySet();
						for (Short id : ids) {
							I_Device device = subDeviceMap.get(id);
							if (device instanceof MSN300) {
								((OldMSN300) device).tick_1Sec();
							}
						}

						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					} catch (Exception e) {
						MyLog.lWithSysOut(e.getMessage());
					}
				}
			}
		}).start();
		new DeviceStateChangedEvent(this, "", null);
		MyLog.lWithSysOut("Create OldLC100: " + HexUtils.toHexString(deviceId));
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		if (xnetHandler == null)
			return NETWORK_STATE.OFFLINE;
		else if (xnetHandler.isConnected() == false)
			return NETWORK_STATE.OFFLINE;
		return networkState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		// TODO Auto-generated method stub
		return deviceType;
	}

	public void handleMessage(I_Message msg) {

		// 10초이상 message 응답 없으면 network state를 offline로 설정.
		lastMessageReceivingTime = Calendar.getInstance();
		networkState = NETWORK_STATE.ONLINE;

		if (msg instanceof I_NCP_Message) {
			I_NCP_Message ncpMsg = (I_NCP_Message) msg;
			switch (ncpMsg.getMessageType()) {
			case REQ_REGISTER_NODE:
				handleRegisterNodeReq(ncpMsg);
				break;
			}
		} else if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			switch (gcpMsg.getMessageType()) {
			case RES_STATEINFO:
				handleStateInfoRes(gcpMsg);
				break;
			case REQ_STATEINFO:
				handleStateInfoReq(gcpMsg);
				break;
			case RES_CONTROL_CIRCUIT:
				handleControlCircuitRes(gcpMsg);
				break;
			case REQ_CONTROL_CIRCUIT:
				handleControlCircuitReq(gcpMsg);
				break;
			case RES_POWERMETER:
				handlePowermeterRes(gcpMsg);
				break;
			case REQ_POWERMETER:
				handlePowermeterReq(gcpMsg);
				break;
			case REQ_UPDATEPOWERMETER:
				handleUpdatePowermeter(gcpMsg);
				break;
			case RES_MAPPING_SENSOR:
				handleMappingSensorRes(gcpMsg);
				break;
			case REQ_MAPPING_SENSOR:
				handleMappingSensorReq(gcpMsg);
				break;
			case RES_MAPPING_DIMMER:
				handleMappingDimmerRes(gcpMsg);
				break;
			case REQ_MAPPING_DIMMER:
				handleMappingDimmerReq(gcpMsg);
				break;
			case REQ_RESET_MAPPING_SENSOR:
				handleResetMappingSensorReq(gcpMsg);
				break;			
			case REQ_RESET_MAPPING_DIMMER:
				handleResetMappingDimmerReq(gcpMsg);
				break;
			case NOTICE_EVENT:
				handleNoticeEvent(gcpMsg);
				break;
			case RES_SET_SENSING_LEVEL:
				handleSetSensingLevelRes(gcpMsg);
				break;
			case REQ_SET_SENSING_LEVEL:
				handleSetSensingLevelReq(gcpMsg);
				break;
			case RES_DEBUG_LOG_LC:
				handleDebugLogLCRes(gcpMsg);
				break;
			case REQ_DEBUG_LOG_LC:
				handleDebugLogLCReq(gcpMsg);
				break;
			case REQ_RESET_DEBUG_LOG_LC:
				handleResetDebugLogLCReq(gcpMsg);
				break;
			}
		}
	}

	private void handleRegisterNodeReq(I_NCP_Message ncpMsg) {
		MyLog.i("LC_handleRegisterNodeReq");
		NCP_Message_RegisterNodeReq reqMsg = ((NCP_Message_RegisterNodeReq) ncpMsg);
		short deviceType = reqMsg.getDeviceType();
		if (deviceType == E_DeviceType.MSN300.toShort()) {
			OldMSN300 msn = (OldMSN300) getDeviceById(reqMsg.getDeviceId());
			if (msn == null) {
				msn = new OldMSN300(this, reqMsg.getDeviceId());
				msn.setFirmwareVersion(reqMsg.getFirmwareVersion());
				addDevice(msn);
			}
		} else if (deviceType == E_DeviceType.MDT100.toShort()) {
			OldMDT100 mdt = (OldMDT100) getDeviceById(reqMsg.getDeviceId());
			if (mdt == null) {
				mdt = new OldMDT100(this, reqMsg.getDeviceId());
				mdt.setFirmwareVersion(reqMsg.getFirmwareVersion());
				addDevice(mdt);
			}
		} else if (deviceType == E_DeviceType.LC100.toShort()) {
			this.firmwareVersion = reqMsg.getFirmwareVersion();
			new DeviceStateChangedEvent(this, "", null);
		}

		NCP_Message_RegisterNodeRes resMsg = new NCP_Message_RegisterNodeRes(
				ncpMsg.getNodeId(), reqMsg.getSubNodeId(), deviceType, 0x00);
		resMsg.setSeqNum(ncpMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleResetDebugLogLCReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleResetDebugLogReq");
		this.rebootCount = 0;
		this.serverConnectionCount = 0;
		this.lastRebootTime = Calendar.getInstance();
		this.lastServerConnectionTime = Calendar.getInstance();
	}

	private void handleDebugLogLCReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleDebugLogLCReq");
		GCP_Message_DebugLogLCReq reqMsg = (GCP_Message_DebugLogLCReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();
		MyLog.i("subNodeId: " + subNodeId);

		int rebootCount = 0;
		byte[] lastRebootTime = new byte[5];
		int serverConnectionCount = 0;
		byte[] lastServerConnectionTime = new byte[5];

		rebootCount = this.rebootCount;
		serverConnectionCount = this.serverConnectionCount;

		lastRebootTime[0] = new Integer(this.lastRebootTime.get(Calendar.HOUR))
				.byteValue();
		lastRebootTime[1] = new Integer(
				this.lastRebootTime.get(Calendar.MINUTE)).byteValue();

		lastServerConnectionTime[0] = new Integer(
				this.lastServerConnectionTime.get(Calendar.HOUR)).byteValue();
		lastServerConnectionTime[1] = new Integer(
				this.lastServerConnectionTime.get(Calendar.MINUTE)).byteValue();

		GCP_Message_DebugLogLCRes resMsg = new GCP_Message_DebugLogLCRes(srcId,
				subNodeId, rebootCount, lastRebootTime, serverConnectionCount,
				lastServerConnectionTime);
		resMsg.setSeqNum((byte) reqMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handleDebugLogLCRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleDebugLogLCRes");
		GCP_Message_DebugLogLCRes resMsg = (GCP_Message_DebugLogLCRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		int rebootCount = resMsg.getRebootCount();
		byte[] lastRebootTime = resMsg.getLastRebootTime();
		int serverConnectionCount = resMsg.getServerConnectionCount();
		byte[] lastServerConnectionTime = resMsg.getLastServerConnectionTime();

		String str = "";
		MyLog.i("rebootCount: " + rebootCount);
		str = "";
		for (int i = 0; i < lastRebootTime.length; i++)
			str += lastRebootTime[i];
		MyLog.i("lastRebootTime: " + str);
		MyLog.i("serverConnectionCount: " + serverConnectionCount);
		str = "";
		for (int i = 0; i < lastServerConnectionTime.length; i++)
			str += lastServerConnectionTime[i];
		MyLog.i("lastServerConnectionTime: " + str);

		this.rebootCount = rebootCount;
		this.serverConnectionCount = serverConnectionCount;
		;
		this.lastRebootTime.set(Calendar.HOUR_OF_DAY, 2000 + new Integer(
				lastRebootTime[0]));
		this.lastRebootTime.set(Calendar.MINUTE, 2000 + new Integer(
				lastRebootTime[1]));

		this.lastServerConnectionTime.set(Calendar.HOUR_OF_DAY,
				2000 + new Integer(lastServerConnectionTime[0]));
		this.lastServerConnectionTime.set(Calendar.MINUTE, 2000 + new Integer(
				lastServerConnectionTime[1]));

		I_Message reqIMsg = getRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null)
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);
	}

	private void handleSetSensingLevelReq(I_GCP_Message gcpMsg) {
		MyLog.i("handleSetSensingLevelReq");
		GCP_Message_SetSensingLevelReq reqMsg = (GCP_Message_SetSensingLevelReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = reqMsg.getNodeId();
		short sensorId = reqMsg.getSensorId();

		I_Device device = getDeviceById(sensorId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);

	}

	private void handleSetSensingLevelRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleSetSensingLevelRes");
		GCP_Message_SetSensingLevelRes resMsg = (GCP_Message_SetSensingLevelRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = resMsg.getNodeId();
		short sensorId = resMsg.getSensorId();

		I_Device device = getDeviceById(sensorId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
	}

	private void handleNoticeEvent(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleNoticeEvent");
		GCP_Message_NoticeEvent reqMsg = (GCP_Message_NoticeEvent) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getNodeId();

		short sensorId = reqMsg.getSensorId();
		// event 생성 필요.
		I_Device device = getDeviceById(sensorId);
		if (device != null)
			device.handleMessage((I_Message) gcpMsg);
		boolean isChanged = false;
		E_GCP_ControlMode[] stateTemp = reqMsg.getControlState();
		for (int i = 0; i < 8; i++)
			if (stateTemp[i] != this.stateInfo[i])
				isChanged = true;
		if (isChanged == true) {
			for (int i = 0; i < 8; i++)
				this.stateInfo[i] = stateTemp[i];
			new DeviceStateChangedEvent(this, "", null);
		}
	}

	private void handleResetMappingDimmerReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleResetMappingDimmerReq");
		GCP_Message_ResetMappingDimmerReq reqMsg = (GCP_Message_ResetMappingDimmerReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		dimmerMappingList.clear();
	}

	private void handleResetMappingSensorReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleResetMappingSensorReq");
		GCP_Message_ResetMappingSensorReq reqMsg = (GCP_Message_ResetMappingSensorReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short subNodeId = reqMsg.getSubNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		sensorMappingList.clear();
	}

	private void handleMappingDimmerReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingDimmerReq");
		GCP_Message_MappingDimmerReq reqMsg = (GCP_Message_MappingDimmerReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = reqMsg.getNodeId();
		short dimmerId = reqMsg.getDimmerId();
		int circuitId = reqMsg.getCircuitId();

		MyLog.i("subNodeId: " + nodeId);
		MyLog.i("sensorId: " + dimmerId);
		MyLog.i("circuitId: " + circuitId);

		if (isContainDimmerInMappingTable(dimmerId)) {
			GCP_Message_MappingDimmerRes resMsg = new GCP_Message_MappingDimmerRes(
					nodeId, dimmerId, circuitId,
					E_GCP_Result.FAIL_ALREADYMAPPED);
			sendMessage(resMsg);
			return;
		}

		boolean result = addDimmerMappingInfo(new DimmerMappingInfo(dimmerId,
				0, circuitId));

		if (result) {
			GCP_Message_MappingDimmerRes resMsg = new GCP_Message_MappingDimmerRes(
					nodeId, dimmerId, circuitId, E_GCP_Result.SUCCESS);
			sendMessage(resMsg);
		} else {
			GCP_Message_MappingDimmerRes resMsg = new GCP_Message_MappingDimmerRes(
					nodeId, dimmerId, circuitId, E_GCP_Result.FAIL);
			sendMessage(resMsg);
		}

	}

	private void handleMappingDimmerRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingDimmerRes");
		GCP_Message_MappingDimmerRes resMsg = (GCP_Message_MappingDimmerRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short nodeId = resMsg.getNodeId();

		MyLog.i("subNodeId: " + nodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		if (resMsg.getResult() == E_GCP_Result.SUCCESS) {
			addDimmerMappingInfo(new DimmerMappingInfo(resMsg.getDimmerId(), 0,
					resMsg.getCircuitId()));
			new DeviceStateChangedEvent(this, "", null);
		}
	}

	private void handleMappingSensorReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingSensorReq");
		GCP_Message_MappingSensorReq reqMsg = (GCP_Message_MappingSensorReq) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = reqMsg.getNodeId();
		short sensorId = reqMsg.getSensorId();
		int circuitId = reqMsg.getCircuitId();
		int onTime = reqMsg.getOnTime();
		E_GCP_ControlMode offCtrlMode = reqMsg.getOffDimmingCtrl();

		MyLog.i("subNodeId: " + nodeId);
		MyLog.i("sensorId: " + sensorId);
		MyLog.i("circuitId: " + circuitId);
		MyLog.i("onTime: " + onTime);
		MyLog.i("offCtrlMode: " + offCtrlMode);
		if (updateSensorMappingInfo(new SensorMappingInfo(getId(), sensorId,
				circuitId, onTime, E_GCP_ControlMode.ON, offCtrlMode)) == false) {
			boolean result = addSensorMappingInfo(new SensorMappingInfo(
					getId(), sensorId, circuitId, onTime, E_GCP_ControlMode.ON,
					offCtrlMode));
			if (result) {
				GCP_Message_MappingSensorRes resMsg = new GCP_Message_MappingSensorRes(
						nodeId, sensorId, circuitId, onTime, offCtrlMode,
						E_GCP_Result.SUCCESS);
				sendMessage(resMsg);
			} else {
				GCP_Message_MappingSensorRes resMsg = new GCP_Message_MappingSensorRes(
						nodeId, sensorId, circuitId, onTime, offCtrlMode,
						E_GCP_Result.FAIL);
				sendMessage(resMsg);
			}
		} else {
			GCP_Message_MappingSensorRes resMsg = new GCP_Message_MappingSensorRes(
					nodeId, sensorId, circuitId, onTime, offCtrlMode,
					E_GCP_Result.UPDATE);
			sendMessage(resMsg);
		}
	}

	private void handleMappingSensorRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleMappingSensorRes");
		GCP_Message_MappingSensorRes resMsg = (GCP_Message_MappingSensorRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getNodeId();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("Resut: " + resMsg.getResult());

		if (resMsg.getResult() == E_GCP_Result.UPDATE
				|| resMsg.getResult() == E_GCP_Result.SUCCESS) {
			SensorMappingInfo info = new SensorMappingInfo(getId(),
					resMsg.getSensorId(), resMsg.getCircuitId(),
					resMsg.getOnTime(), E_GCP_ControlMode.ON,
					resMsg.getOffCtrlMode());
			if (updateSensorMappingInfo(info) == false)
				addSensorMappingInfo(info);
			new DeviceStateChangedEvent(this, "", null);
		}
	}

	private SensorMappingInfo getSensorMappingInfoByIndex(int index) {
		if (this.sensorMappingList.size() <= index)
			return null;
		return this.sensorMappingList.get(index);
	}

	private DimmerMappingInfo getDimmerMappingInfoByIndex(int index) {
		if (this.dimmerMappingList.size() <= index)
			return null;
		return this.dimmerMappingList.get(index);
	}

	private void handleUpdatePowermeter(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleUpdatePowermeter");
		GCP_Message_UpdatePowermeterReq reqMsg = (GCP_Message_UpdatePowermeterReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;

		short nodeId = reqMsg.getNodeId();
		int powermeterValue = reqMsg.getEnergyPulse();

		MyLog.i("nodeId: " + nodeId);
		MyLog.i("powermeterValue: " + powermeterValue);

		this.powermeterValue = powermeterValue;
	}

	private void handlePowermeterReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handlePowermeterReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		GCP_Message_PowermeterRes resMsg = new GCP_Message_PowermeterRes(
				getId(), powermeterValue);
		resMsg.setSeqNum((byte) gcpMsg.getSeqNum());
		sendMessage(resMsg);
	}

	private void handlePowermeterRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handlePowermeterRes");
		GCP_Message_PowermeterRes resMsg = (GCP_Message_PowermeterRes) gcpMsg;

		short srcId = getId();
		short destId = (short) 0x0000;
		int powermeterValue = resMsg.getEnergyPulse();

		I_Message reqIMsg = getRequestMessageOfResponseMessage((I_Message) gcpMsg);
		if (reqIMsg != null)
			new ReceiveResponseMessageEvent(this, reqIMsg, resMsg, "", null);

		MyLog.i("powermeterValue: " + powermeterValue);
		if (this.powermeterValue != powermeterValue) {
			this.powermeterValue = powermeterValue;
			new DeviceStateChangedEvent(this, "Powermeter: " + powermeterValue,
					null);
		}
	}

	private void updateSensorMappingTable(int circuitId, int onTime,
			E_GCP_ControlMode ctrlModeOn, E_GCP_ControlMode ctrlModeOff) {
		for (SensorMappingInfo mappingInfo : sensorMappingList) {
			if (mappingInfo.getCircuitId() == circuitId) {
				mappingInfo.setOnTime(onTime);
				mappingInfo.setOffCtrlMode(ctrlModeOn);
				mappingInfo.setOnCtrlMode(ctrlModeOff);
			}
		}
	}

	private void handleControlCircuitReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleControlCircuitReq");
		GCP_Message_ControlCircuitReq reqMsg = (GCP_Message_ControlCircuitReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getNodeId();

		int circuitId = reqMsg.getCircuitId();
		E_GCP_ControlMode ctrlMode = reqMsg.getCtrlMode();
		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("CircuitId: " + circuitId);
		MyLog.i("ctrlMode: " + ctrlMode);

		controlInfo[circuitId] = ctrlMode;

		GCP_Message_ControlCircuitRes resMsg = new GCP_Message_ControlCircuitRes(
				srcId, circuitId, ctrlMode);

		resMsg.setSeqNum((byte) gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);

	}

	private void handleControlCircuitRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleControlCircuitRes");
		GCP_Message_ControlCircuitRes resMsg = (GCP_Message_ControlCircuitRes) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = resMsg.getNodeId();

		MyLog.i("subNodeId: " + subNodeId);

		if (controlInfo[resMsg.getCircuitId()] != resMsg.getCtrlMode()) {
			controlInfo[resMsg.getCircuitId()] = resMsg.getCtrlMode();
			new DeviceStateChangedEvent(this, "", null);
		}
	}

	private void handleStateInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleStateInfoReq");
		GCP_Message_StateInfoReq reqMsg = (GCP_Message_StateInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short nodeId = reqMsg.getNodeId();
		short deviceType = reqMsg.getDeviceType();

		GCP_Message_StateInfoRes resMsg = null;

		try {
			resMsg = new GCP_Message_StateInfoRes(nodeId, getDeviceType()
					.toShort(), getStateInfo());
			resMsg.setSeqNum((byte) gcpMsg.getSeqNum());
			sendMessage((I_Message) resMsg);
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void handleStateInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("LC_handleStateInfoRes");

		GCP_Message_StateInfoRes resMsg = (GCP_Message_StateInfoRes) gcpMsg;

		boolean stateChanged = false;
		E_GCP_ControlMode controlInfo[] = resMsg.getStateInfo();
		E_GCP_ControlMode stateInfo[] = resMsg.getStateInfo();
		for (int i = 0; i < 16; i++) {
			if (controlInfo[i].toByte() != this.controlInfo[i].toByte())
				stateChanged = true;
			if (stateInfo[i].toByte() != this.stateInfo[i].toByte())
				stateChanged = true;
		}

		if (stateChanged) {
			this.controlInfo = controlInfo;
			this.stateInfo = stateInfo;
			new DeviceStateChangedEvent(this, "", null);
		}
	}

	public I_Device getDeviceById(short deviceId) {
		return subDeviceMap.get(deviceId);
	}

	@Override
	public void setXNetHandler(I_XNetHandler xnetHandler) {
		this.xnetHandler = xnetHandler;
		for (I_Device device : subDeviceMap.values())
			device.setXNetHandler(xnetHandler);
	}

	public void addDevice(I_Device device) {
		device.setXNetHandler(xnetHandler);
		subDeviceMap.put(device.getId(), device);
	}

	public boolean removeDevice(I_Device device) {
		if (subDeviceMap.remove(device.getId()) != null) {
			new DeviceStateChangedEvent(this, "", null);
			return true;
		}
		return false;
	}

	public int getNumOfSubDevices() {
		return subDeviceMap.size();
	}

	public I_Device getDeviceByIndex(int index) {
		if (index >= getNumOfSubDevices())
			return null;
		int cnt = 0;
		for (I_Device device : subDeviceMap.values()) {
			if (index == cnt)
				return device;
			cnt++;
		}

		return null;
	}

	public E_GCP_ControlMode[] getStateInfo() {
		return stateInfo;
	}

	public E_GCP_ControlMode[] getControlInfo() {
		return controlInfo;
	}

	private boolean sendMessage(I_Message msg) {
		if (xnetHandler == null)
			return false;
		return xnetHandler.sendMessage(msg);
	}

	public void setParent(OldMGW300 parent) {
		this.parent = parent;
	}

	public int getSensorMappingTableSize() {
		return this.sensorMappingList.size();
	}

	public int getDimmerMappingTableSize() {
		return this.dimmerMappingList.size();
	}

	public boolean addSensorMappingInfo(SensorMappingInfo mappingInfo) {
		if (this.sensorMappingList.size() < 256) {
			this.sensorMappingList.add(mappingInfo);
			return true;
		}
		return false;
	}

	public boolean updateSensorMappingInfo(SensorMappingInfo mappingInfo) {
		for (SensorMappingInfo info : sensorMappingList) {
			if (info.getCircuitId() == mappingInfo.getCircuitId())
				if (info.getSensorId() == mappingInfo.getSensorId()) {
					sensorMappingList.remove(info);
					sensorMappingList.add(mappingInfo);
					return true;
				}
		}
		return false;
	}

	public boolean addDimmerMappingInfo(DimmerMappingInfo mappingInfo) {
		if (this.dimmerMappingList.size() < 256) {
			this.dimmerMappingList.add(mappingInfo);
			return true;
		}
		return false;
	}

	public boolean isContainDimmerInMappingTable(short dimmerId) {
		for (DimmerMappingInfo mappingInfo : dimmerMappingList) {
			if (mappingInfo.getDimmerId() == dimmerId)
				return true;
		}
		return false;
	}

	public I_XNetHandler getXnetHandler() {
		return xnetHandler;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public int getPowermeterValue() {
		return powermeterValue;
	}

	public int getRebootCount() {
		return rebootCount;
	}

	public int getServerConnectionCount() {
		return serverConnectionCount;
	}

	public Calendar getLastRebootTime() {
		return lastRebootTime;
	}

	public Calendar getLastServerConnectionTime() {
		return lastServerConnectionTime;
	}

	public int sendDebugLogLCReq() throws UnformattedPacketException {
		MyLog.i("sendDebugLogLCReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_DebugLogLCReq(srcId, getId());
		sendMessage((I_Message) msg);

		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public boolean sendResetDebugLCLogReq() {
		MyLog.i("sendResetDebugLCLogReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ResetDebugLogLCReq(srcId, getId());
		boolean result = sendMessage((I_Message) msg);

		try {
			sendDebugLogLCReq();
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	public int sendMappingSensorReq(short sensorId, int circuitId, int onTime,
			E_GCP_ControlMode onCtrlMode, E_GCP_ControlMode offCtrlMode) {
		MyLog.i("sendMappingSensorReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_MappingSensorReq(srcId, sensorId,
				circuitId, onTime, offCtrlMode);
		sendMessage((I_Message) msg);
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendMappingDimmerReq(short dimmerId, int circuitId) {
		MyLog.i("sendMappingSensorReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_MappingDimmerReq(srcId, dimmerId,
				circuitId);
		sendMessage((I_Message) msg);
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendPowermeterReq() {
		MyLog.i("sendPowermeterReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_PowermeterReq(getId());
		sendMessage((I_Message) msg);
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public boolean sendUpdatePowermeter(int powermeterValue) {
		MyLog.i("sendUpdatePowermeter");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_UpdatePowermeterReq(getId(),
				powermeterValue);
		return sendMessage((I_Message) msg);
	}

	public boolean sendSensorMappingReq(short sensorId, int circuitId,
			int onTime, E_GCP_ControlMode ctrlModeOn,
			E_GCP_ControlMode ctrlModeOff) {
		MyLog.i("sendSensorMappingReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_MappingSensorReq(srcId, sensorId,
				circuitId, onTime, ctrlModeOff);
		return sendMessage((I_Message) msg);

	}

	public int sendResetSensorMappingReq() {
		MyLog.i("sendResetSensorMappingReq");

		short srcId = getId();
		short destId = (short) 0x0000;
		
		sensorMappingList.clear();

		I_GCP_Message msg = new GCP_Message_ResetMappingSensorReq(srcId,
				getId());
		sendMessage((I_Message) msg);
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendResetDimmerMappingReq() {
		MyLog.i("sendResetDimmerMappingReq");

		short srcId = getId();
		short destId = (short) 0x0000;
		
		dimmerMappingList.clear();
		I_GCP_Message msg = new GCP_Message_ResetMappingDimmerReq(srcId,
				getId());
		sendMessage((I_Message) msg);
		
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));
	}

	public int sendControlCircuit(int circuitId, E_GCP_ControlMode ctrlMode) {
		MyLog.i("sendControlCircuit");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_ControlCircuitReq(srcId, circuitId,
				ctrlMode);

		sendMessage((I_Message) msg);
		return HexUtils.toInt(HexUtils.toHexString(msg.getSeqNum()));

	}

	public boolean sendStateInfoReq() {
		MyLog.i("sendStateInfoReq");

		short srcId = getId();
		short destId = (short) 0x0000;

		I_GCP_Message msg = new GCP_Message_StateInfoReq(srcId, getDeviceType()
				.toShort());
		return sendMessage((I_Message) msg);
	}

	public boolean sendRegisterNodeReq(I_Device device) {
		MyLog.i("sendRegisterNodeReq");
		short srcId = getId();
		short destId = (short) 0x0000;
		short subSrcId = getId();
		short deviceType = device.getDeviceType().toShort();
		short deviceId = device.getId();
		int deviceVersion = 0;
		int firmwareVersion = 1;
		int netAddr = 0;

		I_NCP_Message msg = new NCP_Message_RegisterNodeReq(srcId, subSrcId,
				deviceType, deviceId, deviceVersion, firmwareVersion, netAddr);
		return sendMessage((I_Message) msg);
	}

	@Override
	public I_Device getParent() {
		// TODO Auto-generated method stub
		return parent;
	}

	public DimmerMappingInfo getDimmerMappingInfo(int i) {
		return dimmerMappingList.get(i);
	}

	public SensorMappingInfo getSensorMappingInfo(int i) {
		return sensorMappingList.get(i);
	}

	void addMessageToResponseWaitMessageQueue(I_Message msg) {
		_responseWaitMessageQueue.add(new ResponseWaitMessage(msg, 5));
	}

	synchronized I_Message removceMessageInReponseWaitMessageQeueueByIndex(int i) {
		return _responseWaitMessageQueue.remove(i).getMsg();
	}

	I_Message getRequestMessageOfResponseMessage(I_Message msg) {
		I_Message msgInQeueue = null;
		for (int i = 0; i < _responseWaitMessageQueue.size(); i++) {
			msgInQeueue = _responseWaitMessageQueue.get(i).getMsg();
			if (msg instanceof I_GCP_Message
					&& msgInQeueue instanceof I_GCP_Message) {
				I_GCP_Message gcpMsg = (I_GCP_Message) msg;
				I_GCP_Message gcpMsgInQueue = (I_GCP_Message) msgInQeueue;

				if (gcpMsg.getSeqNum() == gcpMsgInQueue.getSeqNum()) {
					removceMessageInReponseWaitMessageQeueueByIndex(i);
					return (I_Message) gcpMsgInQueue;
				}
			}
		}
		return null;
	}

	@Override
	public void run() {
		Calendar lastSentTime_DebugLogReq = Calendar.getInstance();
		while (isTerminated == false) {
			if (xnetHandler != null) {
				if (xnetHandler.isConnected()) {
					if (lastSentTime_DebugLogReq.getTimeInMillis() + 60000 < Calendar
							.getInstance().getTimeInMillis()) {// 1분에 한번
																// debugLog
																// 가져오기.
						try {
							sendDebugLogLCReq();
						} catch (UnformattedPacketException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						lastSentTime_DebugLogReq = Calendar.getInstance();
					}
				}
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public short getPanId() {
		return panId;
	}

	public void terminate() {
		this.isTerminated = true;
	}
	
	@Override
	public I_XNetHandler getXNetHandler() {
		return this.xnetHandler;
	}

	public int sendTiltControlCommand(int len, String command) {
		return 0;
	}

	public String getTiltMessage() {
		return "";
	}

	public void setTiltMessage(String string) {
	}

	public String getZyroMessage() {
		return "";
	}

	public void setZyroMessage(String string) {
		// TODO Auto-generated method stub
		
	}
}
