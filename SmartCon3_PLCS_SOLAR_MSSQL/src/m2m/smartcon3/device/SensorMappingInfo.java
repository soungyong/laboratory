package m2m.smartcon3.device;

import java.io.Serializable;

import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;

public class SensorMappingInfo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 6303432085963061952L;
	short lcId;
	short sensorId;
	int circuitId;
	int onTime;
	E_GCP_ControlMode onCtrlMode;
	E_GCP_ControlMode offCtrlMode;
	
	public SensorMappingInfo(short lcId, short sensorId, int circuitId, int onTime, E_GCP_ControlMode onCtrlMode,
	E_GCP_ControlMode offCtrlMode){
		this.lcId = lcId;
		this.sensorId = sensorId;
		this.circuitId = circuitId;
		this.onTime = onTime;
		this.onCtrlMode = onCtrlMode;
		this.offCtrlMode = offCtrlMode;
	}
	
	public short getSensorId() {
		return sensorId;
	}
	public void setSensorId(short sensorId) {
		this.sensorId = sensorId;
	}
	public int getCircuitId() {
		return circuitId;
	}
	public void setCircuitId(int circuitId) {
		this.circuitId = circuitId;
	}
	public int getOnTime() {
		return onTime;
	}
	public void setOnTime(int onTime) {
		this.onTime = onTime;
	}
	public E_GCP_ControlMode getOnCtrlMode() {
		return onCtrlMode;
	}
	public void setOnCtrlMode(E_GCP_ControlMode onCtrlMode) {
		this.onCtrlMode = onCtrlMode;
	}
	public E_GCP_ControlMode getOffCtrlMode() {
		return offCtrlMode;
	}
	public void setOffCtrlMode(E_GCP_ControlMode offCtrlMode) {
		this.offCtrlMode = offCtrlMode;
	}

	public short getLcId() {
		return lcId;
	}

	public void setLcId(short lcId) {
		this.lcId = lcId;
	}
	
	
}
