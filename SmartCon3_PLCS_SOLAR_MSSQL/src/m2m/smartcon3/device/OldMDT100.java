package m2m.smartcon3.device;

import java.io.Serializable;

import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_RebootReq;
import m2m.smartcon3.protocol.gcp2_1.I_GCP_Message;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class OldMDT100 implements I_Device{
	I_XNetHandler xnetHandler = null;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.MDT100;	
	private int firmwareVersion;
	I_Device parent =null;
	
	public OldMDT100(I_Device parent, short id) {
		this.parent = parent;
		this.deviceId = id;
		new DeviceStateChangedEvent(this, "", null);
		MyLog.lWithSysOut("Create OldMDT100: " + HexUtils.toHexString(deviceId));
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		if (xnetHandler == null)
			return NETWORK_STATE.OFFLINE;
		else if (xnetHandler.isConnected() == false)
			return NETWORK_STATE.OFFLINE;
		return NETWORK_STATE.ONLINE;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}

	@Override
	public void setXNetHandler(I_XNetHandler xnetHandler) {
		this.xnetHandler = xnetHandler;
	}
	
	private boolean sendMessage(I_Message msg) {
		if (xnetHandler == null)
			return false;
		return xnetHandler.sendMessage(msg);
	}

	@Override
	public void handleMessage(I_Message msg) {
		if (msg instanceof I_GCP_Message) {
			I_GCP_Message gcpMsg = (I_GCP_Message) msg;
			switch (gcpMsg.getMessageType()) {			
			case RES_DEVICEINFO:
				handleDeviceInfoRes(gcpMsg);
				break;
			case REQ_DEVICEINFO:
				handleDeviceInfoReq(gcpMsg);
				break;
			case RES_STATEINFO:
				handleStateInfoRes(gcpMsg);
				break;
			case REQ_STATEINFO:
				handleStateInfoReq(gcpMsg);
				break;			
			}
		}	
		
	}
	
	private void handleStateInfoRes(I_GCP_Message gcpMsg) {
		// TODO Auto-generated method stub
		
	}

	private void handleStateInfoReq(I_GCP_Message gcpMsg) {
		// TODO Auto-generated method stub
		
	}

	private void handleDeviceInfoReq(I_GCP_Message gcpMsg) {
		MyLog.i("MDT100_handleDeviceInfoReq");
		GCP_Message_DeviceInfoReq reqMsg = (GCP_Message_DeviceInfoReq) gcpMsg;
		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = reqMsg.getSubNodeId();
		short deviceId;
		short deviceType;		
		byte deviceVersion = 0;

		I_Device device = this;		
		
		deviceId = device.getId();
		deviceType = device.getDeviceType().toShort();

		GCP_Message_DeviceInfoRes resMsg = new GCP_Message_DeviceInfoRes(srcId,
				destId, subNodeId, deviceId, deviceType, deviceVersion);
		resMsg.setSeqNum(gcpMsg.getSeqNum());
		sendMessage((I_Message) resMsg);		
	}

	private void handleDeviceInfoRes(I_GCP_Message gcpMsg) {
		MyLog.i("MDT100_handleDeviceInfoRes");
		GCP_Message_DeviceInfoRes resMsg = (GCP_Message_DeviceInfoRes) gcpMsg;

		short subNodeId = resMsg.getSubNodeId();
		short deviceId = resMsg.getDeviceId();
		short deviceType = resMsg.getDeviceType();
		int firmwareVersion = resMsg.getDeviceVersion();

		MyLog.i("subNodeId: " + subNodeId);
		MyLog.i("deviceId: " + deviceId);
		MyLog.i("deviceType: " + HexUtils.toHexString(deviceType));
		MyLog.i("deviceVersion: " + firmwareVersion);

		this.firmwareVersion = firmwareVersion;		
	}
	
	public void setFirmwareVersion(int version){
		this.firmwareVersion = version;
	}

	@Override
	public I_Device getParent() {
		return parent;
	}

	public int getFrimwareVersion() {
		return this.firmwareVersion;
	}
	
	public boolean sendRebootReq(){
		MyLog.i("sendRebootReq");

		short srcId = getId();
		short destId = (short) 0x0000;
		short subNodeId = (short)getParent().getId();
		short deviceId = (short)getId();

		I_GCP_Message msg = new GCP_Message_RebootReq(srcId, destId, subNodeId, deviceId);
		return sendMessage((I_Message) msg);
	}
	
	@Override
	public I_XNetHandler getXNetHandler() {
		return this.xnetHandler;
	}
}
