/**
	SMART-CON 2.0

	@author	tchan@m2mkorea.co.kr
	@date 2009/09/24.
 */
package m2m.smartcon3.device;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

/**
 * SC2 Device definitions
 * 
 * 장치들의 정의에 관한 상수들....
 */
public class DefaultDeviceManager implements I_DeviceManager {
	public static final Logger logger = Logger.getLogger("DefaultDeviceFactory");
	// device id, device type.
	List<I_Device> deviceList;

	private static I_DeviceManager _instance;

	public static I_DeviceManager getInstance() {
		if (_instance == null) {
			_instance = new DefaultDeviceManager();
		}
		return _instance;
	}

	private DefaultDeviceManager() {
		deviceList = new LinkedList<I_Device>();
	}

	public Collection<I_Device> getDevices() {
		return deviceList;
	}

	public void addDevice(I_Device dev) {
		if (find(dev.getId(), dev.getDeviceType()) == null)
			deviceList.add(dev);
	}

	public I_Device find(short deviceId, E_DeviceType deviceType) {
		for (I_Device device : deviceList)
			if (deviceId == device.getId() && deviceType == device.getDeviceType())
				return device;
		return null;
	}

	@Override
	public I_Device createDevice(short deviceId, short deviceType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public I_Device getDevice(short deviceId, short deviceType) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeDevice(short deviceId, short type) {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeDevice(I_Device device) {
		// TODO Auto-generated method stub

	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
	}
}
