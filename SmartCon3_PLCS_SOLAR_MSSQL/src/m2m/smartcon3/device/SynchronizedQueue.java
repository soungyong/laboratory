package m2m.smartcon3.device;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import m2m.util.MyLog;

public class SynchronizedQueue<T> {

	Semaphore semaphore = new Semaphore(1);
	List<T> queue;

	public SynchronizedQueue() {
		queue = Collections.synchronizedList(new LinkedList<T>());
	}

	public void add(T object) {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		queue.add(object);
		semaphore.release();

	}

	public T removeByIndex(int i) {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		T object = null;
		try {
			object = queue.remove(i);

		} catch (Exception e) {
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
		semaphore.release();
		return object;
	}

	public void remove(T object) {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		queue.remove(object);

		semaphore.release();
	}

	public T get(int index) {
		T object = null;
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			object = queue.get(index);
		} catch (Exception e) {
			e.printStackTrace();
		}
		semaphore.release();
		return object;
	}

	public List<T> getAll() {
		List<T> objectList = new ArrayList<T>();
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			for (int i = 0; i < queue.size(); i++)
				objectList.add(queue.get(i));
		} catch (Exception e) {
			e.printStackTrace();
		}
		semaphore.release();
		return objectList;
	}

	public int getSize() {
		return queue.size();
	}

	public void clear() {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		queue.clear();
		semaphore.release();
	}
}
