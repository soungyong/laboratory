/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/24.
*/

package m2m.smartcon3.device;

import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.protocol.I_Message;

/**
 * An interface to accommodate Nodes and Gateways in SMARTCON network.
 */
public interface I_Device
{
	enum NETWORK_STATE {OFFLINE, ONLINE};
	

	/**
	 * Get device's name
	 * @return
	 */
	public short getId();
	
	public NETWORK_STATE getConnectionState();

	/**
	 * Get device's type
	 */
	public E_DeviceType getDeviceType();
	public I_XNetHandler getXNetHandler();
	public void setXNetHandler(I_XNetHandler xnetHandler);
	public void handleMessage(I_Message msg);
	public I_Device getParent();
		
}
