package m2m.smartcon3.tools.device;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PacketCollector {
	private static PacketCollector _instance = null;

	private PacketCollector() {
	}

	public synchronized static PacketCollector getInstance() {
		if (_instance == null)
			_instance = new PacketCollector();
		return _instance;
	}

	HashMap<Short, List<Packet>> packetMap = new HashMap<Short, List<Packet>>();

	public void addPacket(Packet packet) {
		
		List<Packet> packetList = packetMap.get(packet.getSrcId());
		if (packetList == null) {
			packetList = new ArrayList<Packet>();
			if (packet.getIndex() != 0)
				return;
			packetList.add(packet);
			packetMap.put(packet.getSrcId(), packetList);
		} else {
			if (packet.getIndex() == 0) {
				packetList.clear();
				packetList.add(packet);
			} else if (packetList.size() == packet.getIndex())
				packetList.add(packet);
			else
				packetList.clear();
		}
	}

	public List<Packet> getPacketList(Packet packet) {
		List<Packet> packetList = packetMap.get(packet.getSrcId());
		if (packetList == null)
			return null;

		if (packetList.size() != packet.getTotalLen())
			return null;
		packetMap.remove(packetList);
		return packetList;
	}
}
