package m2m.smartcon3.tools.device;

public class Packet {
	short srcId;
	byte seqId;
	byte totalLen;
	byte index;
	byte[] data;
	
	public Packet(short srcId, byte[] rawData){
		this.srcId = srcId;
		this.seqId = rawData[0];
		this.totalLen = rawData[1];
		this.index = rawData[2];
		
		int len=0;
		data = new byte[rawData.length-3];
		for(int i=3; i < rawData.length; i++)
			data[len++] = rawData[i];
	}

	public short getSrcId() {
		return srcId;
	}

	public void setSrcId(short srcId) {
		this.srcId = srcId;
	}

	public byte getSeqId() {
		return seqId;
	}

	public void setSeqId(byte seqId) {
		this.seqId = seqId;
	}

	public byte getTotalLen() {
		return totalLen;
	}

	public void setTotalLen(byte totalLen) {
		this.totalLen = totalLen;
	}

	public byte getIndex() {
		return index;
	}

	public void setIndex(byte index) {
		this.index = index;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}
}
