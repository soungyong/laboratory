/**
	SMART-CON 2.0

	@author	tchan@m2mkorea.co.kr
	@date 2009/08/25.
 */
/**
 * 	XcpProcessor
 * 		handle GCP message from gateway.
 * 		send GCP message to gateway.
 * 		- handle NCP message from NODE (through gateway)
 * 		- send NCP message to NODE (through gateway)
 * 
 *	- GCP :
 *	- NCP (forwarding) :
 *	- TODO: buffered request (NCP_NS from api, etc...) processing.
 * - 0622. log data by caching...
 * - 2010/06/02 no singleton.
 */
package m2m.smartcon3.tools.devicenetwork;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import javax.comm.CommPortIdentifier;
import javax.comm.NoSuchPortException;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.UnsupportedCommOperationException;

import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.devicenetwork.ResponseWaitMessage;
import m2m.smartcon3.devicenetwork.ResponseWaitMessageQueue;
import m2m.smartcon3.event.DeviceDisconnectEvent;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.exception.UnknownMessageException;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp2_1.I_GCP_Message;
import m2m.smartcon3.protocol.ncp2_0.I_NCP_Message;
import m2m.smartcon3.tools.protocol.gcp2_1.GCPParserForZigbee;
import m2m.smartcon3.tools.protocol.ncp2_0.NCPParserForZigbee;
import m2m.smartcon3.tools.protocol.zrm.radiopulse.I_ZRM_Message;
import m2m.smartcon3.tools.protocol.zrm.radiopulse.ZRMParserForZigbee;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class SerialXNetHandler implements I_XNetHandler, Runnable {
	public static final Logger logger = Logger.getLogger("SerialXNetProcessor");
	public static final Logger dataLogger = Logger.getLogger("SERIAL_NET_DATA");

	SerialPort serialPort;
	I_Device parentDevice = null;
	boolean isConnected = true;
	boolean isTerminated = false;

	BufferedInputStream serialInputStream = null;
	BufferedOutputStream serialOutputStream = null;

	//
	// packet vector will be sent to gw.
	List<I_Message> _sendingMessageQueue; // (gwid, this adapter) map.

	ResponseWaitMessageQueue _responseWaitMessageQueue = new ResponseWaitMessageQueue();

	// packet vector received from gw.
	List<I_Message> _receivedMessageQueue; // (gwid, this adapter) map.

	public SerialXNetHandler(String comPort, I_Device parentDevice) {

		isConnected = openComPort(comPort);

		this.parentDevice = parentDevice;

		_sendingMessageQueue = Collections.synchronizedList(new LinkedList<I_Message>());
		_receivedMessageQueue = Collections.synchronizedList(new LinkedList<I_Message>());

		new Thread(this).start();

		// sendThread for _responseWaitMessageQueue
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (isTerminated == false) {
					_responseWaitMessageQueue.increaseTimerOfMessages();
					// timeout 된 녀석들 resend.
					for (ResponseWaitMessage msg : _responseWaitMessageQueue.getResponseWaitMessageList()) {
						if (msg.isTimeout() == true && msg.isOverRetry() == false) {
							msg.increaseReSentCnt();
							sendMessage(msg.getMsg());
						}
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();

		// sendThread for _sendingMessageQeueue
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (isConnected) {
					try {
						BufferedOutputStream bos = new BufferedOutputStream(SerialXNetHandler.this.serialPort.getOutputStream());
						I_Message msg = null;
						while (_sendingMessageQueue.size() > 0) {
							msg = _sendingMessageQueue.remove(0);
							if (msg instanceof I_NCP_Message || msg instanceof I_GCP_Message) {
								String str = "send: ";

								byte[] packet_byte = null;
								if (msg instanceof I_NCP_Message)
									packet_byte = ((I_NCP_Message) msg).toBytes();
								if (msg instanceof I_GCP_Message)
									packet_byte = ((I_GCP_Message) msg).toBytes();

								if (getParent() == null)
									return;
								packet_byte[8] = (byte) (getParent().getId() >> 8);
								packet_byte[9] = (byte) (getParent().getId());

								byte[] zigbeePacket_byteTemp = new byte[packet_byte.length * 2];
								int zigbeePacketLength = 0;
								zigbeePacket_byteTemp[zigbeePacketLength++] = packet_byte[0];
								zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0x00;// len
								zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0x10;
								zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0x00;
								zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0x00;
								zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0x00;
								zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0x00;
								for (int i = 2; i < packet_byte.length - 1; i++) {
									if (packet_byte[i] == (byte) 0xff) {
										zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0xff;
										zigbeePacket_byteTemp[zigbeePacketLength++] = packet_byte[i];
									} else if (packet_byte[i] == (byte) 0xfa) {
										zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0xff;
										zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0x01;
									} else if (packet_byte[i] == (byte) 0xaf) {
										zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0xff;
										zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0x02;
									} else
										zigbeePacket_byteTemp[zigbeePacketLength++] = packet_byte[i];
								}
								zigbeePacket_byteTemp[zigbeePacketLength++] = (byte) 0xaf;

								zigbeePacket_byteTemp[1] = (byte) (zigbeePacketLength - 4); // len
								byte checksum = 0;
								for (int i = 1; i < zigbeePacketLength - 2; i++)
									checksum += zigbeePacket_byteTemp[i];
								zigbeePacket_byteTemp[zigbeePacketLength - 2] = checksum;

								byte zigbeePacket_byte[] = new byte[zigbeePacketLength];
								for (int i = 0; i < zigbeePacketLength; i++)
									zigbeePacket_byte[i] = zigbeePacket_byteTemp[i];

								for (int i = 0; i < zigbeePacketLength; i++)
									str += HexUtils.toHexString(zigbeePacket_byte[i]) + " ";
								MyLog.l(str);

								bos.write(zigbeePacket_byte);
								bos.flush();
							} else if (msg instanceof I_ZRM_Message) {
								String str = "send: ";
								byte[] packet_byte = ((I_ZRM_Message) msg).toBytes();
								for (int i = 0; i < packet_byte.length; i++)
									str += HexUtils.toHexString(packet_byte[i]) + " ";
								MyLog.l(str);
								bos.write(packet_byte);
								bos.flush();
							}
							try {
								// Thread.sleep(1);
								Thread.sleep(50);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						new DeviceDisconnectEvent(SerialXNetHandler.this.parentDevice, "XnetHandler", null);
						MyLog.e(e.getMessage());
						terminate();
					}
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MyLog.e(e.getMessage());
					}
				}

			}

		}, "send Thread").start();
	}

	public boolean openComPort(String comPort) {
		String comPortUppercase = comPort.toUpperCase();

		CommPortIdentifier ports;
		try {
			ports = CommPortIdentifier.getPortIdentifier(comPortUppercase);
			serialPort = (SerialPort) ports.open("RS232C", 1000);
			serialPort.setSerialPortParams(115200, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
			serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
			InputStream is = serialPort.getInputStream();
			serialInputStream = new BufferedInputStream(is);
			serialOutputStream = new BufferedOutputStream(serialPort.getOutputStream());
		} catch (NoSuchPortException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (UnsupportedCommOperationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} catch (PortInUseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public synchronized boolean sendMessage(I_Message msg) {
		if (isConnected == false)
			return false;

		_sendingMessageQueue.add(msg);
		return true;
	}

	@Override
	public synchronized I_Message getMessage() {

		if (_receivedMessageQueue.size() != 0) {
			I_Message msg = _receivedMessageQueue.get(0);
			return msg;
		}

		return null;
	}

	@Override
	public void run() {
		isConnected = true;
		try {
			BufferedInputStream bis = new BufferedInputStream(serialPort.getInputStream());
			byte[] recvBuf = new byte[20480];
			int len = 0;
			byte[] recvBufTemp = new byte[10240];
			int lenTemp = 0;

			while (isConnected) {
				try {
					lenTemp = bis.read(recvBufTemp);
					if (lenTemp > 0) {
						String str = "Recv1: ";
						for (int i = 0; i < lenTemp; i++) {
							recvBuf[len++] = recvBufTemp[i];
							str += HexUtils.toHexString(recvBufTemp[i]) + " ";
						}
						MyLog.l(str);
						// // /////////////////////////////////////////
						// byte[] byteTemp = new byte[lenTemp];
						// for (int i = 0; i < lenTemp; i++)
						// byteTemp[i] = recvBufTemp[i];
						//
						// String strTemp = new String(byteTemp);
						// int startIndex = strTemp.indexOf("  PanID : ");
						// while (startIndex != -1) {
						// String panIdString = strTemp.substring(
						// startIndex + 10, startIndex + 14);
						// // System.out.println(panIdString);
						// short panId = HexUtils.toShort(panIdString);
						// ZRM_Message_ScanPanIdRes resMsg = new
						// ZRM_Message_ScanPanIdRes(
						// panId);
						// addToRecvMessageQueue(resMsg);
						// strTemp = strTemp.substring(startIndex + 14);
						// startIndex = strTemp.indexOf("  PanID : ");
						// }
						// // /////////////////////////////////////////

					}
					if (len > 0) {
						// 하나의 single Packet
						int index = handlePacket(recvBuf, len);

						if (index != -1) {
							int size = len;
							len = 0;
							for (int i = index + 1; i < size; i++)
								recvBuf[len++] = recvBuf[i];
						} else {
							if (len > 1024) {
								MyLog.e("쓰레기 패킷 잔뜩 쌓임");
								len = 0;
							}
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else {
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (java.net.SocketTimeoutException se) {
					// se.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new DeviceDisconnectEvent(parentDevice, "", null);
			terminate();
			MyLog.e(e.getMessage());
		}
	}

	/**
	 * 입력받은 패킷에서 메시지 추출하여 처리.
	 * 
	 * @param packet
	 * @param len
	 * @return 마지막으로 처리한 패킷 index, or -1: 처리할 패킷 없음.
	 */
	public int handlePacket(byte[] packet, int len) {
		int lastIndex = -1;
		int lenForASingleMessage = 0;
		byte[] buf_temp = new byte[20480];
		byte[] bufForASingleMessage = null;

		for (int i = 0; i < len && i < 20480; i++) {
			if (packet[i] == (byte) 0xfa) {
				lenForASingleMessage = 0;
				buf_temp[lenForASingleMessage++] = packet[i];
			} else if (lenForASingleMessage == 1) {
				buf_temp[lenForASingleMessage++] = packet[i];
			} else if (packet[i] == (byte) 0xaf) {
				lastIndex = i;
				buf_temp[lenForASingleMessage++] = packet[i];
				bufForASingleMessage = new byte[lenForASingleMessage];
				if (lenForASingleMessage < 5)
					continue;

				int lenTemp = 0;
				bufForASingleMessage[lenTemp++] = (byte) 0xfa;
				for (int j = 1; j < lenForASingleMessage - 1; j++) {
					bufForASingleMessage[lenTemp++] = buf_temp[j];
				}
				bufForASingleMessage[lenTemp++] = (byte) 0xaf;

				buf_temp[0] = 0x00;
				byte originalPacket[] = new byte[lenTemp];

				for (int j = 0; j < lenTemp; j++)
					originalPacket[j] = bufForASingleMessage[j];

				int isUnknownMessage = 1;
				try {
					I_Message msg = null;

					try {
						msg = (I_Message) new ZRMParserForZigbee().getMessageFromPacket(originalPacket, lenTemp);
						isUnknownMessage = 0;
					} catch (UnknownMessageException e) {
						// MyLog.i(e.toString());
					}

					lenTemp = 0;
					bufForASingleMessage[lenTemp++] = (byte) 0xfa;
					bufForASingleMessage[lenTemp++] = (byte) 0x00;
					for (int j = 2; j < lenForASingleMessage - 2; j++) {
						if (buf_temp[j] == (byte) 0xff) {
							if (buf_temp[j + 1] == (byte) 0x01)
								bufForASingleMessage[lenTemp++] = (byte) 0xfa;
							else if (buf_temp[j + 1] == (byte) 0x02)
								bufForASingleMessage[lenTemp++] = (byte) 0xaf;
							else if (buf_temp[j + 1] == (byte) 0xff)
								bufForASingleMessage[lenTemp++] = (byte) 0xff;
							j += 1;
							continue;
						} else
							bufForASingleMessage[lenTemp++] = buf_temp[j];
					}
					bufForASingleMessage[lenTemp++] = buf_temp[lenForASingleMessage - 2];
					bufForASingleMessage[lenTemp++] = (byte) 0xaf;

					bufForASingleMessage[1] = (byte) (lenTemp - 4);
					if (msg == null)
						try {
							msg = (I_Message) new NCPParserForZigbee().getMessageFromPacket(bufForASingleMessage, lenTemp);
							isUnknownMessage = 0;
						} catch (UnknownMessageException e) {
							// MyLog.i(e.toString());
						}

					if (msg == null)
						try {
							msg = (I_Message) new GCPParserForZigbee().getMessageFromPacket(bufForASingleMessage, lenTemp);
							isUnknownMessage = 0;
						} catch (UnknownMessageException e) {
							// MyLog.i(e.toString());
						}

					if (msg == null && isUnknownMessage == 1) {
						MyLog.e("UnknownMessageException");
						String str = "";
						for (int j = 0; j < lenTemp; j++) {
							str += HexUtils.toHexString(bufForASingleMessage[j]) + " ";
						}
						MyLog.l(str);
					}

					if (msg != null)
						addToRecvMessageQueue(msg);

				} catch (UnformattedPacketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					MyLog.e(e.toString());
				}
				lenForASingleMessage = 0;

			} else {
				buf_temp[lenForASingleMessage++] = packet[i];
			}
		}
		if (buf_temp[0] != (byte) 0xfa)
			lastIndex = len - 1;
		return lastIndex;
	}

	private void handleResWaitMessageQueue(I_Message msg) {
		_responseWaitMessageQueue.removeRequestMessageOfResponseMessage(msg);
	}

	private void addToRecvMessageQueue(I_Message msg) {
		handleResWaitMessageQueue(msg);
		_receivedMessageQueue.add(msg);
	}

	@Override
	public synchronized I_Message removeMessage() {
		I_Message msg = null;
		if (_receivedMessageQueue.size() != 0)
			msg = _receivedMessageQueue.remove(0);

		return msg;
	}

	@Override
	public boolean hasParent() {
		if (parentDevice == null)
			return false;
		return true;
	}

	@Override
	public void setParent(I_Device parent) {
		this.parentDevice = parent;
	}

	@Override
	public boolean isConnected() {
		// TODO Auto-generated method stub
		return isConnected;
	}

	@Override
	public void terminate() {
		if (isTerminated == false) {
			if (parentDevice != null)
				MyLog.lWithSysOut("SerialXNetHandler terminated: " + parentDevice.getId());
			else
				MyLog.lWithSysOut("SerialXNetHandler terminated: " + null);
			this.isTerminated = true;
			isConnected = false;

			if (serialPort != null) {
				serialPort.close();
			}
		}
	}

	@Override
	public boolean sendMessageWithResponse(I_Message msg) {
		return sendMessageWithResponse(msg, 5, 0);
	}

	@Override
	public boolean sendMessageWithResponse(I_Message msg, int timeout) {
		return sendMessageWithResponse(msg, timeout, 0);
	}

	@Override
	public boolean sendMessageWithResponse(I_Message msg, int timeout, int maxRetryCntToSend) {
		_responseWaitMessageQueue.addMessageToResponseWaitMessageQueue(msg, timeout, maxRetryCntToSend);

		if (isConnected == false)
			return false;

		_sendingMessageQueue.add(msg);
		return true;
	}

	public String getSerialPortString() {
		return serialPort.getName();

	}

	public I_Device getParent() {
		return this.parentDevice;
	}
}
