package m2m.smartcon3.tools.protocol.zrm.radiopulse;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.exception.UnknownMessageException;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class ZRMParserForZigbee {
	/**
	 * Input: FA~AF로 이루어진 하나의 패킷. 여러개 뭉쳐서 오면 처음꺼만 처리한다. Output: 입력된 패킷을 분석하여
	 * message로 반환.
	 * 
	 * @throws UnformattedPacketException
	 * @throws UnknownMessageException
	 */
	public I_ZRM_Message getMessageFromPacket(byte[] packet, int size) throws UnformattedPacketException, UnknownMessageException {
		I_ZRM_Message message = null;

		if (packet.length < size)
			throw new UnformattedPacketException("패킷 길이 잘못됨");
		// 시작 byte, end byte확인.
		if (packet[0] != (byte) 0xfa)
			throw new UnformattedPacketException("시작 문자 잘못됨, " + HexUtils.toHexString(packet[0]));
		if (packet[size - 1] != (byte) 0xaf) {
			String str = "";
			for (int i = 0; i < size; i++)
				str += HexUtils.toHexString(packet[i]) + " ";
			throw new UnformattedPacketException("종료 문자 잘못됨, " + str);
		}
		if (size < 5)
			throw new UnformattedPacketException("패킷 길이 너무 짧음");
		if (packet[2] != (byte) 0x00)
			throw new UnknownMessageException("지그비 제어 패킷이 아님");

		byte length_byte;
		byte checksum_byte;
		byte messageType_byte;

		length_byte = packet[1];
		messageType_byte = packet[3];
		checksum_byte = packet[size - 2];

		// packet 길이 확인.
//		if (length_byte != size - 4)
//			throw new UnformattedPacketException("패킷 길이 틀림");

		// checksum 확인
		byte check_cal = 0;
		for (int i = 2; i < size - 2; i++)
			check_cal += packet[i];
//		if (check_cal != checksum_byte)
//			throw new UnformattedPacketException("checksum 틀림");

		// Message 생성 후 반환.
		if (messageType_byte == E_ZRM_MessageType.RES_PING.toByte())
			message = new ZRM_Message_PingRes(packet, size);
		else if (messageType_byte == E_ZRM_MessageType.RES_CONFIG.toByte())
			message = new ZRM_Message_ConfigRes(packet, size);
		else if (messageType_byte == E_ZRM_MessageType.RES_FAIL.toByte())
			message = new ZRM_Message_Fail(packet, size);
		else if (messageType_byte == E_ZRM_MessageType.RES_SUCCESS.toByte())
			message = new ZRM_Message_Success(packet, size);
		else
			throw new UnknownMessageException("Unknwon MessageType:0x" + HexUtils.toHexString(messageType_byte));

		return message;
	}

	public static void main(String args[]) {
		byte packet[] = new byte[10];
		int size = 20;
		// 1. 패킷 크기 잘못됨.
		MyLog.i("1. 패킷 크기 잘못됨.");
		try {
			new ZRMParserForZigbee().getMessageFromPacket(packet, size);
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 2. 시작 문자 잘못됨.
		MyLog.i("2. 시작 문자 잘못됨.");
		packet = new byte[20];
		packet[0] = (byte) 0xf2;
		try {
			new ZRMParserForZigbee().getMessageFromPacket(packet, size);
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 3. 종료 문자 잘못됨.
		MyLog.i("3. 종료 문자 잘못됨.");
		packet[0] = (byte) 0xfa;
		packet[19] = (byte) 0xae;
		try {
			new ZRMParserForZigbee().getMessageFromPacket(packet, size);
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 4. 패킷 길이 틀림.
		MyLog.i("4. 패킷 길이 틀림.");
		packet[0] = (byte) 0xfa;
		packet[19] = (byte) 0xaf;
		try {
			new ZRMParserForZigbee().getMessageFromPacket(packet, size);
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MyLog.i("5. checksum 잘못됨.");
		// 5. checksum 잘못됨.
		packet[1] = 16;
		packet[18] = 0x11;
		try {
			new ZRMParserForZigbee().getMessageFromPacket(packet, size);
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MyLog.i("6. NCP 패킷 아님.");
		// 6. NCP 패킷 아님.
		packet[1] = 16;
		packet[18] = (byte) 0x81;
		packet[2] = (byte) 0x81;
		try {
			new ZRMParserForZigbee().getMessageFromPacket(packet, size);
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		MyLog.i("7. NCP의 message type이 아님.");
		// 7. NCP의 message type이 아님.

		size = 23;
		packet = new byte[23];
		packet[0] = (byte) 0xfa;
		packet[22] = (byte) 0xaf;
		packet[1] = 19;
		packet[21] = (byte) 0x96;
		packet[2] = (byte) 0x80;
		packet[4] = (byte) 0x16;
		try {
			new ZRMParserForZigbee().getMessageFromPacket(packet, size);
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnknownMessageException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
