package m2m.smartcon3.tools.protocol.zrm.radiopulse;

/**
 * @version v2.0
 * @author funface2
 * 
 */
public enum E_ZRM_MessageType {
	REQ_PING((byte) 0x00), 
	RES_PING((byte) 0x01),	 
	REQ_CONFIG((byte) 0x10),	
	RES_CONFIG((byte) 0x11),
	REQ_SETPANID((byte) 0x30), 
	RES_SETPANID((byte) 0x31),
	REQ_SETCHANNEL((byte) 0x40),
	REQ_SETPRECONFIG((byte) 0x40),
	REQ_RESET((byte) 0x01),
	RES_SUCCESS((byte) 0x02),
	RES_FAIL((byte) 0x03),
	
	RES_SCANPANID((byte) 0xA1),
	;
	

	private byte byteValue=0;

	private E_ZRM_MessageType() {
	}

	private E_ZRM_MessageType(byte value) {
		this.byteValue = value;
	}
	
	public byte toByte() {
		return byteValue;
	}
}
