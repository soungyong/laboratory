package m2m.smartcon3.tools.protocol.zrm.radiopulse;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class ZRM_Message_ScanPanIdRes extends ZRM_Message {	
	short deviceId;
	short panId;
	
	public ZRM_Message_ScanPanIdRes(){
		msgType = E_ZRM_MessageType.RES_SCANPANID;
	}
	
	public ZRM_Message_ScanPanIdRes(short panId){
		this();
		
		this.panId = panId;
		int len=0;
		byte payload_bytes[] = new byte[3];		
		payload_bytes[len++] = msgType.toByte();
		payload_bytes[len++] = (byte) (panId>>8);
		payload_bytes[len++] = (byte) panId;
				
		super.getPacketBytes(payload_bytes);
	}

	public ZRM_Message_ScanPanIdRes(byte[] packet, int size) throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		
		if(payload_bytes.length!=3)throw new UnformattedPacketException("패킷 길이 틀림");
		
		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		panId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
		+HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		
	}

	public short getDeviceId() {
		return this.deviceId;
	}
	
	public short getPanId(){
		return this.panId;
	}

	public static void main(String args[]) {
		ZRM_Message_ScanPanIdRes pingResMsg = new ZRM_Message_ScanPanIdRes((short)0x1002);
		byte[] data = pingResMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");
		
		try {
			pingResMsg = new ZRM_Message_ScanPanIdRes(data, data.length);
			data = pingResMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public short getSeqNum() {
		// TODO Auto-generated method stub
		return 0;
	}	
	
	
}
