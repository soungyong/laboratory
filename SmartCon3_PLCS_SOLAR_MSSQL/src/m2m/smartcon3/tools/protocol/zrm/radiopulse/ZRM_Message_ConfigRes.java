package m2m.smartcon3.tools.protocol.zrm.radiopulse;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class ZRM_Message_ConfigRes extends ZRM_Message {
	short deviceId;
	short panId;
	int channel;

	public ZRM_Message_ConfigRes() {
		msgType = E_ZRM_MessageType.RES_CONFIG;
	}

	public ZRM_Message_ConfigRes(short deviceId, short panId, short netAddr,
			int channel, int tPower) {
		this();

		int len = 0;
		byte payload_bytes[] = new byte[15];
		payload_bytes[len++] = msgType.toByte();
		payload_bytes[len++] = 0x00;
		payload_bytes[len++] = 0x15;
		payload_bytes[len++] = 0x51;
		payload_bytes[len++] = 0x20;
		payload_bytes[len++] = 0x20;
		payload_bytes[len++] = 0x10;
		payload_bytes[len++] = (byte) (deviceId >> 8);
		payload_bytes[len++] = (byte) deviceId;
		payload_bytes[len++] = (byte) channel;
		payload_bytes[len++] = (byte) (panId >> 8);
		payload_bytes[len++] = (byte) panId;
		payload_bytes[len++] = (byte) (netAddr >> 8);
		payload_bytes[len++] = (byte) netAddr;
		payload_bytes[len++] = (byte) tPower;

		super.getPacketBytes(payload_bytes);
	}

	public ZRM_Message_ConfigRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);

		if (payload_bytes.length != 15)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		deviceId = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[7])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[8])));

		channel = HexUtils.toInt(HexUtils.toHexString(payload_bytes[9]));

		panId = (short) (HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[10])) * 256 + HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[11])));

	}

	public short getDeviceId() {
		return this.deviceId;
	}

	public short getPanId() {
		return this.panId;
	}
	
	public int getChannel(){
		return this.channel;
	}

	public static void main(String args[]) {
		ZRM_Message_ConfigRes pingResMsg = new ZRM_Message_ConfigRes((short) 0x0002,
				(short) 0x01f5, (short) 0001, 0, 0);
		byte[] data = pingResMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			pingResMsg = new ZRM_Message_ConfigRes(data, data.length);
			data = pingResMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public short getSeqNum() {
		// TODO Auto-generated method stub
		return 0;
	}

}
