package m2m.smartcon3.tools.protocol.zrm.radiopulse;

import m2m.smartcon3.tools.protocol.zrm.radiopulse.E_ZRM_MessageType;

public interface I_ZRM_Message {	
	public byte[] toBytes();
	public E_ZRM_MessageType getMessageType();
	public int getLength();
	public byte getChecksum();	
}
