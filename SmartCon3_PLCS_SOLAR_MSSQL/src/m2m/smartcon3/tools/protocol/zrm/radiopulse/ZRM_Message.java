package m2m.smartcon3.tools.protocol.zrm.radiopulse;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.I_Message;

public abstract class ZRM_Message implements I_ZRM_Message, I_Message {
	E_ZRM_MessageType msgType;
	int length;
	byte checksum;	
	byte[] payload_byte;

	public ZRM_Message() {		
	}

	public ZRM_Message(byte[] packet, int size) throws UnformattedPacketException {		
		throw new UnformattedPacketException("패킷 길이 틀림, size: "+size);		
	}
	
	public byte[] getPayload(byte[] packet_bytes){		
		this.length = new Integer(packet_bytes[1]);
		this.checksum = packet_bytes[packet_bytes.length-2];
		
		payload_byte = new byte[packet_bytes.length-5];
		
		for(int i=0; i < payload_byte.length; i++)
			payload_byte[i] = packet_bytes[i+3];
		
		return payload_byte;
	}
		
	public byte[] getPacketBytes(byte[] payload){
		if(payload==null){
			this.payload_byte = null;
			this.length = 1;
		}else{
			this.payload_byte = payload;
			this.length = payload.length+1;
		}
		
		return toBytes();
	}
	
	public byte[] getPacketBytes(){
		if(payload_byte!=null)
			this.length = payload_byte.length+1;
		else
			this.length = 1;
		
		return toBytes();
	}
		
	@Override
	public byte[] toBytes() {		
		int len=0;
		byte checksum=0;	
		byte packet_byte[] = new byte[length+4];
		
		packet_byte[len++] = (byte) 0xfa;
		packet_byte[len++] = new Integer(this.length).byteValue();
		packet_byte[len++] = (byte) 0x00;
		for(int i=0; i < this.length-1; i++)
			packet_byte[len++] = payload_byte[i];
		for(int i=2; i < len; i++)
			checksum+=packet_byte[i];
		packet_byte[len++] = checksum;
		packet_byte[len++] = (byte) 0xaf;
				 
		return packet_byte;
	}

	@Override
	public E_ZRM_MessageType getMessageType() {
		// TODO Auto-generated method stub
		return msgType;
	}

	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		return length;
	}	

	@Override
	public byte getChecksum() {
		// TODO Auto-generated method stub
		return checksum;
	}
}
