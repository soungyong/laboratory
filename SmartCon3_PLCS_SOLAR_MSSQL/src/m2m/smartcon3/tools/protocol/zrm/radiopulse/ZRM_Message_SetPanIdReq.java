package m2m.smartcon3.tools.protocol.zrm.radiopulse;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class ZRM_Message_SetPanIdReq extends ZRM_Message {
	short panId=0;
	
	public ZRM_Message_SetPanIdReq(){
		msgType = E_ZRM_MessageType.REQ_SETPANID;
	}
	public ZRM_Message_SetPanIdReq(short panId){
		this();		
		
		int len = 0;
		byte payload_byte[] = new byte[3];
		payload_byte[len++] = msgType.toByte();
		payload_byte[len++] = (byte) (panId>>8);
		payload_byte[len++] = (byte) panId;
		
		getPacketBytes(payload_byte);
	}

	public ZRM_Message_SetPanIdReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload = super.getPayload(packet);
		if (payload.length != 3)
			throw new UnformattedPacketException("패킷 길이 틀림, size: " + size);
				
		byte messageType = payload[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
	}
	

	public static void main(String args[]) {
		ZRM_Message_SetPanIdReq pingReqMsg = new ZRM_Message_SetPanIdReq();
		byte[] data = pingReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			pingReqMsg = new ZRM_Message_SetPanIdReq(data, data.length);
			data = pingReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public short getSeqNum() {
		// TODO Auto-generated method stub
		return 0;
	}
}
