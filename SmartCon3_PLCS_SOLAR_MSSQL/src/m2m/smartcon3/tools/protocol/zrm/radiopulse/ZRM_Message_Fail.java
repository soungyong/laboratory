package m2m.smartcon3.tools.protocol.zrm.radiopulse;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class ZRM_Message_Fail extends ZRM_Message {
	
	public ZRM_Message_Fail(){
		msgType = E_ZRM_MessageType.RES_FAIL;		
		
		int len = 0;
		byte payload_byte[] = new byte[1];
		payload_byte[len++] = msgType.toByte();
		
		getPacketBytes(payload_byte);
	}

	public ZRM_Message_Fail(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload = super.getPayload(packet);
		if (payload.length != 1)
			throw new UnformattedPacketException("패킷 길이 틀림, size: " + size);
				
		byte messageType = payload[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));		
	}
	

	public static void main(String args[]) {
		ZRM_Message_Fail pingReqMsg = new ZRM_Message_Fail();
		byte[] data = pingReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			pingReqMsg = new ZRM_Message_Fail(data, data.length);
			data = pingReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public short getSeqNum() {
		// TODO Auto-generated method stub
		return 0;
	}
}
