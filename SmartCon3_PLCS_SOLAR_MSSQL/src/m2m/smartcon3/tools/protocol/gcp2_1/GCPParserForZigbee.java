package m2m.smartcon3.tools.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.exception.UnknownMessageException;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_MessageType;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleListReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleListRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_AddScheduleRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ApplyingScheduleReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ApplyingScheduleRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlCircuitReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlCircuitRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlGatewayReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlGatewayRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlSensorReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlSensorRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlSensorSharingReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ControlSensorSharingRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DebugLogLCReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DebugLogLCRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DebugLogReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DebugLogRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DeviceInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingSizeReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_DimmerMappingSizeRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerListReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerListRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingDimmerRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorListReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorListRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_MappingSensorRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NoticeEvent;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NumOfDeviceReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_NumOfDeviceRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_PowermeterReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_PowermeterRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_RebootReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetDebugLogLCReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetDebugLogReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingDimmerReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingDimmerRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingSensorReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetMappingSensorRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetScheduleReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ResetScheduleRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleSizeReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_ScheduleSizeRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingSizeReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SensorMappingSizeRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SetSensingLevelReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_SetSensingLevelRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_StateInfoReq;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_StateInfoRes;
import m2m.smartcon3.protocol.gcp2_1.GCP_Message_UpdatePowermeterReq;
import m2m.smartcon3.protocol.gcp2_1.I_GCP_Message;
import m2m.util.HexUtils;

public class GCPParserForZigbee {

	public GCPParserForZigbee() {
	}

	public void init() {
	}

	public I_GCP_Message getMessageFromPacket(byte[] packet, int size)
			throws UnformattedPacketException, UnknownMessageException {
		I_GCP_Message message = null;

		if (packet.length < size)
			throw new UnformattedPacketException("패킷 길이 잘못됨");
		// 시작 byte, end byte확인.
		if (packet[0] != (byte) 0xfa)
			throw new UnformattedPacketException("시작 문자 잘못됨, "
					+ HexUtils.toHexString(packet[0]));
		if (packet[size - 1] != (byte) 0xaf)
			throw new UnformattedPacketException("종료 문자 잘못됨");
		if (size < 15)
			throw new UnformattedPacketException("패킷 길이 너무 짧음");
		if(packet[2]!= (byte)0x10)
			throw new UnknownMessageException("지그비 응용 패킷이 아님");

		byte length_byte;
		byte checksum_byte;
		byte pid_byte;
		byte subPid_byte;
		short seqNum_byte;
		byte srcId_byte[] = new byte[2];
		byte destId_byte[] = new byte[2];
		byte messageType_byte;

		length_byte = packet[1];
		pid_byte = packet[7];
		subPid_byte = packet[8];
		seqNum_byte = (short) (packet[9]<<8 | packet[10]);
		destId_byte[0] = packet[11];
		destId_byte[1] = packet[12];
		srcId_byte[0] = packet[13];
		srcId_byte[1] = packet[14];
		messageType_byte = packet[15];		
		checksum_byte = packet[size - 2];

		// packet 길이 확인.
		if (length_byte != size - 4)
			throw new UnformattedPacketException("패킷 길이 틀림");

		// checksum 확인
		byte check_cal = 0;
		for (int i = 2; i < size - 2; i++)
			check_cal += packet[i];
		//if (check_cal != checksum_byte)
			//throw new UnformattedPacketException("checksum 틀림");

		// NCP 패킷 확인
		if (pid_byte != (byte) 0x80)
			throw new UnknownMessageException("NCP 패킷 아님");

		// GCP 버전 확인.
		if (subPid_byte != I_GCP_Message.GCP2_1) {
			throw new UnknownMessageException("GCP2_1 버전 다름");
		}
		
		byte packet2[] = new byte[size-5];
		int len=0;
		for(int i=0; i < size; i++){
			if(i==1)
				packet2[len++] = (byte) (packet[i]-5);
			else if(i >=2 && i <=6)
				continue;
			else
				packet2[len++] = packet[i];
		}
		packet = packet2;
		size = len;
		
		// Message 생성 후 반환.
		if (messageType_byte == E_GCP_MessageType.REQ_NUMOFDEVICE.toByte())
			message = new GCP_Message_NumOfDeviceReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_NUMOFDEVICE.toByte())
			message = new GCP_Message_NumOfDeviceRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_DEVICEINFO.toByte())
			message = new GCP_Message_DeviceInfoReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_DEVICEINFO.toByte())
			message = new GCP_Message_DeviceInfoRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_STATEINFO.toByte())
			message = new GCP_Message_StateInfoReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_STATEINFO.toByte())
			message = new GCP_Message_StateInfoRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_CONTROL_CIRCUIT
				.toByte())
			message = new GCP_Message_ControlCircuitReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_CONTROL_CIRCUIT
				.toByte())
			message = new GCP_Message_ControlCircuitRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_CONTROL_SENSOR
				.toByte())
			message = new GCP_Message_ControlSensorReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_CONTROL_SENSOR
				.toByte())
			message = new GCP_Message_ControlSensorRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_CONTROL_SENSORSHARING
				.toByte())
			message = new GCP_Message_ControlSensorSharingReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_CONTROL_SENSORSHARING
				.toByte())
			message = new GCP_Message_ControlSensorSharingRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_CONTROL_GATEWAY
				.toByte())
			message = new GCP_Message_ControlGatewayReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_CONTROL_GATEWAY
				.toByte())
			message = new GCP_Message_ControlGatewayRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_REBOOT
				.toByte())
			message = new GCP_Message_RebootReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_POWERMETER.toByte())
			message = new GCP_Message_PowermeterReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_POWERMETER.toByte())
			message = new GCP_Message_PowermeterRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_UPDATEPOWERMETER
				.toByte())
			message = new GCP_Message_UpdatePowermeterReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_SENSOR_MAPPING_SIZE
				.toByte())
			message = new GCP_Message_SensorMappingSizeReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_SENSOR_MAPPING_SIZE
				.toByte())
			message = new GCP_Message_SensorMappingSizeRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_DIMMER_MAPPING_SIZE
				.toByte())
			message = new GCP_Message_DimmerMappingSizeReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_DIMMER_MAPPING_SIZE
				.toByte())
			message = new GCP_Message_DimmerMappingSizeRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_SCHEDULE_SIZE
				.toByte())
			message = new GCP_Message_ScheduleSizeReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_SCHEDULE_SIZE
				.toByte())
			message = new GCP_Message_ScheduleSizeRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_SENSOR_MAPPING_INFO
				.toByte())
			message = new GCP_Message_SensorMappingInfoReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_SENSOR_MAPPING_INFO
				.toByte())
			message = new GCP_Message_SensorMappingInfoRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_DIMMER_MAPPING_INFO
				.toByte())
			message = new GCP_Message_DimmerMappingInfoReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_DIMMER_MAPPING_INFO
				.toByte())
			message = new GCP_Message_DimmerMappingInfoRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_SCHEDULE_INFO
				.toByte())
			message = new GCP_Message_ScheduleInfoReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_SCHEDULE_INFO
				.toByte())
			message = new GCP_Message_ScheduleInfoRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_MAPPING_SENSOR
				.toByte())
			message = new GCP_Message_MappingSensorReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_MAPPING_SENSOR
				.toByte())
			message = new GCP_Message_MappingSensorRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_MAPPING_SENSOR_LIST
				.toByte())
			message = new GCP_Message_MappingSensorListReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_MAPPING_SENSOR_LIST
				.toByte())
			message = new GCP_Message_MappingSensorListRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_MAPPING_DIMMER
				.toByte())
			message = new GCP_Message_MappingDimmerReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_MAPPING_DIMMER
				.toByte())
			message = new GCP_Message_MappingDimmerRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_MAPPING_DIMMER_LIST
				.toByte())
			message = new GCP_Message_MappingDimmerListReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_MAPPING_DIMMER_LIST
				.toByte())
			message = new GCP_Message_MappingDimmerListRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_RESET_MAPPING_SENSOR
				.toByte())
			message = new GCP_Message_ResetMappingSensorReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_RESET_MAPPING_SENSOR
				.toByte())
			message = new GCP_Message_ResetMappingSensorRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_RESET_MAPPING_DIMMER
				.toByte())
			message = new GCP_Message_ResetMappingDimmerReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_RESET_MAPPING_DIMMER
				.toByte())
			message = new GCP_Message_ResetMappingDimmerRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_ADD_SCHEDULE
				.toByte())
			message = new GCP_Message_AddScheduleReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_ADD_SCHEDULE
				.toByte())
			message = new GCP_Message_AddScheduleRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_ADD_SCHEDULE_LIST
				.toByte())
			message = new GCP_Message_AddScheduleListReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_ADD_SCHEDULE_LIST
				.toByte())
			message = new GCP_Message_AddScheduleListRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_RESET_SCHEDULE
				.toByte())
			message = new GCP_Message_ResetScheduleReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_RESET_SCHEDULE
				.toByte())
			message = new GCP_Message_ResetScheduleRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_APPLYING_SCHEDULE
				.toByte())
			message = new GCP_Message_ApplyingScheduleReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_APPLYING_SCHEDULE
				.toByte())
			message = new GCP_Message_ApplyingScheduleRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.NOTICE_EVENT.toByte())
			message = new GCP_Message_NoticeEvent(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_SET_SENSING_LEVEL
				.toByte())
			message = new GCP_Message_SetSensingLevelReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_SET_SENSING_LEVEL
				.toByte())
			message = new GCP_Message_SetSensingLevelRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_DEBUG_LOG.toByte())
			message = new GCP_Message_DebugLogReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_DEBUG_LOG.toByte())
			message = new GCP_Message_DebugLogRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_DEBUG_LOG_LC
				.toByte())
			message = new GCP_Message_DebugLogLCReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.RES_DEBUG_LOG_LC
				.toByte())
			message = new GCP_Message_DebugLogLCRes(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_RESET_DEBUG_LOG
				.toByte())
			message = new GCP_Message_ResetDebugLogReq(packet, size);
		else if (messageType_byte == E_GCP_MessageType.REQ_RESET_DEBUG_LOG_LC
				.toByte())
			message = new GCP_Message_ResetDebugLogLCReq(packet, size);
		else
			throw new UnknownMessageException("Unknwon MessageType:0x"
					+ HexUtils.toHexString(messageType_byte));

		return message;
	}
}
