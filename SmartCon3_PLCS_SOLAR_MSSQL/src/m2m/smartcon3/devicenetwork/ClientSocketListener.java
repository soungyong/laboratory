package m2m.smartcon3.devicenetwork;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import m2m.util.MyLog;

public class ClientSocketListener implements Runnable {
	private static ClientSocketListener _instance = null;
	private int port = 30011;
	private ServerSocket serverSocket = null;

	public static ClientSocketListener getInstance() {
		return _instance;
	}

	public static ClientSocketListener getInstance(int port) throws IOException {
		if (_instance == null) {
			_instance = new ClientSocketListener(port);
		}
		return _instance;
	}

	private ClientSocketListener() {
	}

	private ClientSocketListener(int port) throws IOException {
		serverSocket = new ServerSocket(port);

		new Thread(this).start();
	}

	@Override
	public void run() {		
		while (true) {
			try {
				Socket socket = serverSocket.accept();
				System.out.println("Accept: "+socket.getInetAddress());
				XNetManager.getInstance().acceptNewClient(socket);
				
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				MyLog.e(e1.getMessage());
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
