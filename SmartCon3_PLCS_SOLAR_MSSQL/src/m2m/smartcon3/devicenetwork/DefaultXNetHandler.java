/**
	SMART-CON 2.0

	@author	tchan@m2mkorea.co.kr
	@date 2009/08/25.
 */
/**
 * 	XcpProcessor
 * 		handle GCP message from gateway.
 * 		send GCP message to gateway.
 * 		- handle NCP message from NODE (through gateway)
 * 		- send NCP message to NODE (through gateway)
 * 
 *	- GCP :
 *	- NCP (forwarding) :
 *	- TODO: buffered request (NCP_NS from api, etc...) processing.
 * - 0622. log data by caching...
 * - 2010/06/02 no singleton.
 */
package m2m.smartcon3.devicenetwork;

//import java.net.ServerSocket;
//import java.util.Calendar;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.MGW300;
import m2m.smartcon3.device.OldMGW300;
import m2m.smartcon3.event.DeviceDisconnectEvent;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.exception.UnknownMessageException;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp2_1.GCPParser;
import m2m.smartcon3.protocol.gcp2_1.I_GCP_Message;
import m2m.smartcon3.protocol.ncp2_0.I_NCP_Message;
import m2m.smartcon3.protocol.ncp2_0.NCPParser;
import m2m.smartcon3.protocol.rfup1_0.I_RFUP_Message;
import m2m.smartcon3.protocol.rfup1_0.RFUPParser;
import m2m.util.HexUtils;
import m2m.util.MyLog;

import org.apache.log4j.Logger;

public class DefaultXNetHandler implements I_XNetHandler, Runnable {
	public static final Logger logger = Logger.getLogger("XNetProcessor");
	public static final Logger dataLogger = Logger.getLogger("NET_DATA");

	Socket clientSocket;
	I_Device parentDevice = null;
	boolean isConnected = true;
	boolean isTerminated = false;

	//
	// packet vector will be sent to gw.
	List<I_Message> _sendingMessageQueue; // (gwid, this adapter) map.

	ResponseWaitMessageQueue _responseWaitMessageQueue = new ResponseWaitMessageQueue();

	// packet vector received from gw.
	List<I_Message> _receivedMessageQueue; // (gwid, this adapter) map.

	public DefaultXNetHandler(Socket clientSocket, I_Device parentDevice) {
		this.clientSocket = clientSocket;
		this.parentDevice = parentDevice;

		_sendingMessageQueue = Collections.synchronizedList(new LinkedList<I_Message>());
		_receivedMessageQueue = Collections.synchronizedList(new LinkedList<I_Message>());
		try {
			clientSocket.setSoTimeout(5000);
		} catch (SocketException e) {
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}

		new Thread(this).start();

		// sendThread for _responseWaitMessageQueue
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (isTerminated == false) {
					_responseWaitMessageQueue.increaseTimerOfMessages();
					// timeout 된 녀석들 resend.
					List<ResponseWaitMessage> list = _responseWaitMessageQueue.getResponseWaitMessageList();
					for (ResponseWaitMessage msg : list) {
						if (msg.isTimeout() == true && msg.isOverRetry() == false) {
							msg.increaseReSentCnt();
							sendMessage(msg.getMsg());
						}
					}
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();

		// sendThread for _sendingMessageQeueue
		new Thread(new Runnable() {
			@Override
			public void run() {
				// int testCnt=0;
				int deviceVersion = 0;

				while (isConnected) {
					try {
						BufferedOutputStream bos = new BufferedOutputStream(DefaultXNetHandler.this.clientSocket.getOutputStream());
						I_Message msg = null;
						while (_sendingMessageQueue.size() > 0) {
							msg = _sendingMessageQueue.remove(0);
							if (msg instanceof I_NCP_Message) {
								String str = "send: ";
								byte[] packet_byte = ((I_NCP_Message) msg).toBytes();
								for (int i = 0; i < packet_byte.length; i++)
									str += HexUtils.toHexString(packet_byte[i]) + " ";
								MyLog.l(str);
								bos.write(packet_byte);
								bos.flush();
							} else if (msg instanceof m2m.smartcon3.protocol.ncp1_0.I_NCP_Message) {
								String str = "send: ";
								byte[] packet_byte = ((m2m.smartcon3.protocol.ncp1_0.I_NCP_Message) msg).toBytes();
								for (int i = 0; i < packet_byte.length; i++)
									str += HexUtils.toHexString(packet_byte[i]) + " ";
								MyLog.l(str);
								bos.write(packet_byte);
								bos.flush();
							} else if (msg instanceof I_GCP_Message) {
								String str = "send: ";
								byte[] packet_byte = ((I_GCP_Message) msg).toBytes();
								for (int i = 0; i < packet_byte.length; i++)
									str += HexUtils.toHexString(packet_byte[i]) + " ";
								MyLog.l(str);

								bos.write(packet_byte);
								bos.flush();
							} else if (msg instanceof m2m.smartcon3.protocol.gcp1_4.I_GCP_Message) {
								String str = "send: ";
								byte[] packet_byte = ((m2m.smartcon3.protocol.gcp1_4.I_GCP_Message) msg).toBytes();
								for (int i = 0; i < packet_byte.length; i++)
									str += HexUtils.toHexString(packet_byte[i]) + " ";
								MyLog.l(str);

								bos.write(packet_byte);
								bos.flush();
							} else if (msg instanceof I_RFUP_Message) {
								String str = "send: ";
								byte[] packet_byte = ((I_RFUP_Message) msg).toBytes();
								for (int i = 0; i < packet_byte.length; i++)
									str += HexUtils.toHexString(packet_byte[i]) + " ";
								MyLog.l(str);

								bos.write(packet_byte);
								bos.flush();
							}
							try {

								// Thread.sleep(1);
								if (deviceVersion == 0) {
									I_Device device = getParentDevice();
									if (device instanceof MGW300) {
										deviceVersion = ((MGW300) device).getFirmwareVersion();
									} else if (device instanceof OldMGW300) {
										deviceVersion = 1;
									}
								}
								if (deviceVersion >= 6)
									Thread.sleep(10);
								else
									Thread.sleep(50);

							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						new DeviceDisconnectEvent(DefaultXNetHandler.this.parentDevice, "XnetHandler", null);
						MyLog.e(e.getMessage());
						terminate();
					}
					/*
					 * testCnt++; if(testCnt>10){ testCnt=0;
					 * System.out.println("No Send Waiting Message: "
					 * +getParentDevice().getId()); }
					 */
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						MyLog.e(e.getMessage());
					}
				}

			}

		}).start();
	}

	public I_Device getParentDevice() {
		return parentDevice;
	}

	@Override
	public synchronized boolean sendMessage(I_Message msg) {
		if (isConnected == false)
			return false;

		_sendingMessageQueue.add(msg);
		return true;
	}

	@Override
	public synchronized I_Message getMessage() {

		if (_receivedMessageQueue.size() != 0) {
			I_Message msg = _receivedMessageQueue.get(0);
			return msg;
		}

		return null;
	}

	@Override
	public void run() {
		isConnected = true;
		try {
			BufferedInputStream bis = new BufferedInputStream(clientSocket.getInputStream());
			byte[] recvBuf = new byte[20480];
			int len = 0;
			byte[] recvBufTemp = new byte[10240];
			int lenTemp = 0;

			while (isConnected) {
				try {
					lenTemp = bis.read(recvBufTemp);
					if (lenTemp > 0) {
						String str = "Recv: ";
						for (int i = 0; i < lenTemp; i++) {
							recvBuf[len++] = recvBufTemp[i];
							str += HexUtils.toHexString(recvBufTemp[i]) + " ";
						}
						MyLog.l(str);
					}
					if (len > 0) {
						// 하나의 single Packet
						int index = handlePacket(recvBuf, len);

						if (index != -1) {
							int size = len - index;
							len = 0;
							for (int i = index; i < size; i++)
								recvBuf[len++] = recvBuf[i];
						} else {
							if (len > 1024) {
								MyLog.e("쓰레기 패킷 잔뜩 쌓임");
								len = 0;
							}
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					} else {
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				} catch (java.net.SocketTimeoutException se) {
					// se.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			new DeviceDisconnectEvent(parentDevice, "", null);
			terminate();
			MyLog.e(e.getMessage());
		}
	}

	/**
	 * 입력받은 패킷에서 메시지 추출하여 처리.
	 * 
	 * @param packet
	 * @param len
	 * @return 마지막으로 처리한 패킷 index, or -1: 처리할 패킷 없음.
	 */
	public int handlePacket(byte[] packet, int len) {
		int lastIndex = -1;
		int lenForASingleMessage = 0;
		int lenTemp = 0;
		byte[] buf_temp = new byte[20480];
		byte[] bufForASingleMessage = null;
		for (int i = 0; i < len && i < 20480; i++) {
			if (packet[i] == (byte) 0xfa && lenForASingleMessage == 0) {
				lenForASingleMessage = 0;
				buf_temp[lenForASingleMessage++] = packet[i];
			} else if (lenForASingleMessage == 1) {
				lenTemp = HexUtils.toInt(HexUtils.toHexString(packet[i]));
				buf_temp[lenForASingleMessage++] = packet[i];
			} else if (lenTemp + 3 == lenForASingleMessage && packet[i] == (byte) 0xaf) {
				lastIndex = i + 1;
				buf_temp[lenForASingleMessage++] = packet[i];
				bufForASingleMessage = new byte[lenForASingleMessage];
				for (int j = 0; j < lenForASingleMessage; j++)
					bufForASingleMessage[j] = buf_temp[j];

				int isUnknownMessage = 1;
				try {
					I_Message msg = null;
					try {
						msg = (I_Message) new NCPParser().getMessageFromPacket(bufForASingleMessage, lenForASingleMessage);
						isUnknownMessage = 0;
					} catch (UnknownMessageException e) {
						// MyLog.i(e.toString());
						// e.printStackTrace();
					}
					if (msg == null)
						try {
							msg = (I_Message) new m2m.smartcon3.protocol.ncp1_0.NCPParser().getMessageFromPacket(bufForASingleMessage, lenForASingleMessage);
							isUnknownMessage = 0;
						} catch (UnknownMessageException e) {
							// MyLog.i(e.toString());
							// e.printStackTrace();
						}
					if (msg == null)
						try {
							msg = (I_Message) new m2m.smartcon3.protocol.gcp1_4.GCPParser().getMessageFromPacket(bufForASingleMessage, lenForASingleMessage);
							isUnknownMessage = 0;
						} catch (UnknownMessageException e) {
							// MyLog.i(e.toString());
							// e.printStackTrace();
						}

					if (msg == null)
						try {
							msg = (I_Message) new GCPParser().getMessageFromPacket(bufForASingleMessage, lenForASingleMessage);
							isUnknownMessage = 0;
						} catch (UnknownMessageException e) {
							// MyLog.i(e.toString());
							// e.printStackTrace();
						}

					if (msg == null)
						try {
							msg = (I_Message) new RFUPParser().getMessageFromPacket(bufForASingleMessage, lenForASingleMessage);
							isUnknownMessage = 0;
						} catch (UnknownMessageException e) {
							// MyLog.i(e.toString());
							// e.printStackTrace();
						}

					if (msg == null && isUnknownMessage == 1) {
						MyLog.e("UnknownMessageException: " + msg);
					}

					if (msg != null)
						addToRecvMessageQueue(msg);

				} catch (UnformattedPacketException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					MyLog.e(e.toString());
				}
				lenForASingleMessage = 0;

			} else if (lenForASingleMessage < lenTemp + 3) {
				buf_temp[lenForASingleMessage++] = packet[i];
			} else {
				lenForASingleMessage = 0;
			}
		}
		return lastIndex;
	}

	private void handleResWaitMessageQueue(I_Message msg) {
		_responseWaitMessageQueue.removeRequestMessageOfResponseMessage(msg);
	}

	private void addToRecvMessageQueue(I_Message msg) {
		handleResWaitMessageQueue(msg);
		_receivedMessageQueue.add(msg);
	}

	@Override
	public synchronized I_Message removeMessage() {
		I_Message msg = null;
		if (_receivedMessageQueue.size() != 0)
			msg = _receivedMessageQueue.remove(0);

		return msg;
	}

	@Override
	public boolean hasParent() {
		if (parentDevice == null)
			return false;
		return true;
	}

	@Override
	public void setParent(I_Device parent) {
		this.parentDevice = parent;
	}

	@Override
	public boolean isConnected() {
		// TODO Auto-generated method stub
		return isConnected;
	}

	@Override
	public void terminate() {
		if (isTerminated == false) {
			if (parentDevice != null)
				MyLog.lWithSysOut("DefaultXNetHandler terminated: " + parentDevice.getId());
			else
				MyLog.lWithSysOut("DefaultXNetHandler terminated: " + null);
			this.isTerminated = true;
			isConnected = false;

			if (clientSocket != null) {
				if (clientSocket.isClosed() == false) {
					try {
						clientSocket.close();
					} catch (IOException e) {
						e.printStackTrace();
						MyLog.e(e.getMessage());
					}
				}
			}
		}
	}

	@Override
	public boolean sendMessageWithResponse(I_Message msg) {
		return sendMessageWithResponse(msg, 5, 1);
	}

	@Override
	public boolean sendMessageWithResponse(I_Message msg, int timeout) {
		return sendMessageWithResponse(msg, timeout, 0);
	}

	@Override
	public boolean sendMessageWithResponse(I_Message msg, int timeout, int maxRetryCntToSend) {
		_responseWaitMessageQueue.addMessageToResponseWaitMessageQueue(msg, timeout, maxRetryCntToSend);

		if (isConnected == false)
			return false;

		_sendingMessageQueue.add(msg);
		return true;
	}
}
