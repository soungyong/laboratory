package m2m.smartcon3.devicenetwork;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.protocol.I_Message;
import m2m.util.MyLog;

public class ResponseWaitMessageQueue {

	Semaphore semaphore = new Semaphore(1);
	List<ResponseWaitMessage> _responseWaitMessageQueue = new LinkedList<ResponseWaitMessage>();
	I_Device parentDevice = null;

	public ResponseWaitMessageQueue() {
	}

	public ResponseWaitMessageQueue(I_Device parentDevice) {
		this.parentDevice = parentDevice;
	}

	public void addMessageToResponseWaitMessageQueue(I_Message msg) {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_responseWaitMessageQueue.add(new ResponseWaitMessage(msg, 3));
		semaphore.release();

	}

	public void addMessageToResponseWaitMessageQueue(I_Message msg, int timeout) {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_responseWaitMessageQueue.add(new ResponseWaitMessage(msg, timeout));
		semaphore.release();
	}

	public void addMessageToResponseWaitMessageQueue(I_Message msg,
			int timeout, int maxResentCnt) {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_responseWaitMessageQueue.add(new ResponseWaitMessage(msg, timeout,
				maxResentCnt));
		semaphore.release();

	}

	public void removeMessageInResponseWaitMessageQeueue(ResponseWaitMessage msg) {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		_responseWaitMessageQueue.remove(msg);

		semaphore.release();
	}

	public int getResponseWaitMessageListSize() {
		return _responseWaitMessageQueue.size();
	}

	public List<ResponseWaitMessage> getResponseWaitMessageList() {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<ResponseWaitMessage> result = new LinkedList<ResponseWaitMessage>();
		for (ResponseWaitMessage msg : _responseWaitMessageQueue)
			result.add(msg);
		semaphore.release();
		return result;
	}

	public void removeGarbageMessage() {
		for (ResponseWaitMessage msgTemp : getResponseWaitMessageList()) {
			if (msgTemp == null)
				continue;
			if (msgTemp.isTimeout() && msgTemp.isOverRetry()) {
				removeMessageInResponseWaitMessageQeueue(msgTemp);
				if (parentDevice != null)
					new FailToSendMessageEvent(parentDevice, msgTemp, "", null);
			}
		}
	}

	public I_Message removeRequestMessageOfResponseMessage(I_Message msg) {

		if (msg == null)
			return null;

		I_Message msgInQeueue = null;
		for (ResponseWaitMessage responseWaitMsg : getResponseWaitMessageList()) {
			if (responseWaitMsg == null)
				continue;
			msgInQeueue = responseWaitMsg.getMsg();

			if (msg.getSeqNum() == msgInQeueue.getSeqNum()) {
				removeMessageInResponseWaitMessageQeueue(responseWaitMsg);
				return (I_Message) msgInQeueue;
			}
		}
		return null;
	}

	public void increaseTimerOfMessages() {
		removeGarbageMessage();

		for (ResponseWaitMessage responseWaitMsg : getResponseWaitMessageList()) {
			if (responseWaitMsg == null)
				continue;
			responseWaitMsg.increaseTimer();
		}
	}
}
