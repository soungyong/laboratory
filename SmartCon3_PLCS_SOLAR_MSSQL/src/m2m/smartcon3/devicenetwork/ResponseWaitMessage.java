package m2m.smartcon3.devicenetwork;

import m2m.smartcon3.protocol.I_Message;

public class ResponseWaitMessage implements I_Message {
	I_Message msg;
	int timeout = 0;
	int timer = 0;
	int maxRetryToSend=0;
	int retryCnt=0;

	public ResponseWaitMessage(I_Message msg, int timeout) {
		this.msg = msg;
		this.timeout = timeout;
		timer = 0;		
	}
	
	public ResponseWaitMessage(I_Message msg, int timeout, int maxRetryToSend){
		this(msg, timeout);
		this.maxRetryToSend = maxRetryToSend;
	}

	public I_Message getMsg() {
		return msg;
	}

	public int getTimeout() {
		return timeout;
	}

	public boolean isTimeout() {
		if (timer >= timeout)
			return true;
		return false;
	}

	public boolean increaseTimer() {
		timer += 1;
		return isTimeout();
	}
	
	public boolean increaseReSentCnt(){
		retryCnt+=1;
		return isOverRetry();
	}
	
	public boolean isOverRetry(){
		if(retryCnt>=maxRetryToSend)
			return true;
		return false;
	}

	@Override
	public short getSeqNum() {
		// TODO Auto-generated method stub
		return msg.getSeqNum();
	}
}
