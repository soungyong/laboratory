package m2m.smartcon3.devicenetwork;

import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import m2m.smartcon3.core.DeviceManager;
import m2m.util.MyLog;

public class XNetManager implements Runnable {
	private static XNetManager _instance = null;

	List<I_XNetHandler> xnetHandlerList;

	public static XNetManager getInstance() {
		return _instance;
	}

	public synchronized static XNetManager getInstance(int port)
			throws IOException {
		if (_instance == null) {
			_instance = new XNetManager();
			ClientSocketListener.getInstance(port);
		}
		return _instance;
	}

	private XNetManager() {
		xnetHandlerList = new LinkedList<I_XNetHandler>();
		new Thread(this).start();
	}

	/**
	 * I_XnetHandler에서 받은 메시지들 모아다가 해당 deviceManager로 전달.
	 */
	@Override
	public void run() {
		while (true) {
			try {
				int size = xnetHandlerList.size();
				for (int i = 0; i < size; i++) {
					I_XNetHandler xnetHandler = xnetHandlerList.get(i);
					if (xnetHandler.hasParent() == false) {						
						DeviceManager.getInstance().AssignXNetHandler(
								xnetHandler);
					}
				}

				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
				MyLog.e(e.getMessage());
			}
		}
	}

	public synchronized void acceptNewClient(Socket socket) {
		I_XNetHandler xnetHandler = new DefaultXNetHandler(socket, null);
		this.xnetHandlerList.add(xnetHandler);
	}

	public static void main(String args[]) {
		try {
			XNetManager.getInstance(30011);
			while (true) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
