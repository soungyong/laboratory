package m2m.smartcon3.provider;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SocketProviderForAndroid implements Runnable{
	private static SocketProviderForAndroid _instance = null;
	ServerSocket socket=null;
	
	private SocketProviderForAndroid(){		
		try {
			socket = new ServerSocket(30012);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		new Thread(this).start();
		
		
	}
	
	public synchronized static SocketProviderForAndroid getInstance(){
		if(_instance==null) {
			_instance = new SocketProviderForAndroid();
		}
		return _instance;
	}

	@Override
	public void run() {
		while(true){
			try {
				Socket clientSocket = socket.accept();
				new ClientSocketHandler(clientSocket);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
}
