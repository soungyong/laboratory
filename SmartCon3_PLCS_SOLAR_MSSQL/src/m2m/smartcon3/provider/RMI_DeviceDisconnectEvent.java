package m2m.smartcon3.provider;

import java.io.Serializable;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.event.DeviceDisconnectEvent;

public class RMI_DeviceDisconnectEvent extends RMI_EventImplBase implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	short deviceId = 0;
	short lcId = 0;
	short gatewayId = 0;
	
	E_DeviceType deviceType;
	

	public RMI_DeviceDisconnectEvent() {
		super("");
	}

	public RMI_DeviceDisconnectEvent(DeviceDisconnectEvent event) {
		super(event.getName());
		
		if(event.getDevice()!=null){
			this.deviceType = event.getDevice().getDeviceType();
			this.deviceId = event.getDevice().getId();
		}
		else{
			this.deviceType = null;
			this.deviceId = 0;
		}
		
		
		if(deviceType == E_DeviceType.LC100){
			this.gatewayId = event.getDevice().getParent()
			.getId();
		}else if(deviceType == E_DeviceType.MSN300){			
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent()
			.getId();
		}else if(deviceType == E_DeviceType.MDT100){
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent()
			.getId();
		}else if(deviceType == E_DeviceType.MDF100){
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent()
			.getId();
		}
		
		this.date = event.getDate();		
	}

	public short getDeviceId() {
		return deviceId;
	}

	public short getLcId() {
		return lcId;
	}

	public short getGatewayId() {
		return gatewayId;
	}
	
	public E_DeviceType getDeviceType(){
		return deviceType;
	}
}
