/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/24.
*/

package m2m.smartcon3.provider;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;

/**
 * An interface to accommodate Nodes and Gateways in SMARTCON network.
 */
public interface I_RMI_Device
{
	enum NETWORK_STATE {OFFLINE, ONLINE};
	

	/**
	 * Get device's name
	 * @return
	 */
	public short getId();
	
	public NETWORK_STATE getConnectionState();

	/**
	 * Get device's type
	 */
	public E_DeviceType getDeviceType();
	public void update(I_Device device);
}
