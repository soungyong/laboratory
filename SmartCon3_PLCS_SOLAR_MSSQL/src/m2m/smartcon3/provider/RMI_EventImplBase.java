/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
package m2m.smartcon3.provider;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import m2m.smartcon3.event.I_Event;

/**
 * ISmartEvent 인터페이스는 모든 이벤트가 구현해야되는 인터페이스이다.
 * 
 * 05/26 날짜/시간 추가.
 */
public class RMI_EventImplBase implements I_RMI_Event, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4L;
	Date date;
	String name;
	
	public RMI_EventImplBase(){		
		name="";
	}
	public RMI_EventImplBase(String name){
		this.name = name;		
	}
	
	public RMI_EventImplBase(I_Event event){
		this(event.getName());		
	}
		
	@Override
	public Date getDate() {		
		return date;
	}

	@Override
	public String getName() {
		return name;
	}	
}
