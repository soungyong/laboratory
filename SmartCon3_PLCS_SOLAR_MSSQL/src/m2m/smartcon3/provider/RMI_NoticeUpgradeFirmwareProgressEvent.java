package m2m.smartcon3.provider;

import java.io.Serializable;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.event.NoticeUpgradeFirmwareProgressEvent;
import m2m.smartcon3.protocol.rfup1_0.E_RFUP_Result;

public class RMI_NoticeUpgradeFirmwareProgressEvent extends RMI_EventImplBase implements
		Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 26L;
	short deviceId = 0;
	short lcId = 0;
	short gatewayId = 0;
	E_DeviceType deviceType;
	int totalNumOfPacket;
	int currentIndex;

	public RMI_NoticeUpgradeFirmwareProgressEvent() {
		super("");
	}

	public RMI_NoticeUpgradeFirmwareProgressEvent(NoticeUpgradeFirmwareProgressEvent event) {
		super(event.getName());
		this.deviceId = event.getDevice().getId();
		this.deviceType = event.getDevice().getDeviceType();
		this.totalNumOfPacket = event.getTotalNumOfPacket();
		this.currentIndex = event.getCurrentIndex();

		if (deviceType == E_DeviceType.LC100) {
			this.gatewayId = event.getDevice().getParent().getId();
		} else if (deviceType == E_DeviceType.MSN300) {
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent()
			.getId();
		} else if (deviceType == E_DeviceType.MDT100) {
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent()
			.getId();
		} else if (deviceType == E_DeviceType.MDF100) {
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent()
			.getId();
		} else if (deviceType == E_DeviceType.MPR200) {
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent().getId();
		}

		this.date = event.getDate();
	}

	public short getDeviceId() {
		return deviceId;
	}

	public short getLcId() {
		return lcId;
	}

	public short getGatewayId() {
		return gatewayId;
	}

	public E_DeviceType getDeviceType() {
		return deviceType;
	}

	public int getTotalNumOfPacket() {
		return totalNumOfPacket;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	
}
