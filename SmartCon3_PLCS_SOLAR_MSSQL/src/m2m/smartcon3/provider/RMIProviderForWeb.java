package m2m.smartcon3.provider;

import java.io.FileInputStream;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import m2m.smartcon3.core.DeviceManager;
import m2m.smartcon3.core.EventManager;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.LC100;
import m2m.smartcon3.device.MDF100;
import m2m.smartcon3.device.MDT100;
import m2m.smartcon3.device.MGW300;
import m2m.smartcon3.device.MSN300;
import m2m.smartcon3.device.OldLC100;
import m2m.smartcon3.device.OldMGW300;
import m2m.smartcon3.device.OldMSN300;
import m2m.smartcon3.event.DeviceDisconnectEvent;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.event.I_Event;
import m2m.smartcon3.event.NoticeUpgradeFirmwareEvent;
import m2m.smartcon3.event.NoticeUpgradeFirmwareProgressEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.event.SensorEvent;
import m2m.smartcon3.event.SynchronizedMessageEventHandler;
import m2m.smartcon3.exception.NullResultException;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.module.SensorEventLogger;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ScheduleType;
import m2m.util.MyLog;

public class RMIProviderForWeb implements I_RMIProviderForWeb {

	/**
	 * 
	 */
	private static final long serialVersionUID = 14L;
	private static RMIProviderForWeb _instance = null;

	public static RMIProviderForWeb getInstance() throws RemoteException {
		if (_instance == null) {
			_instance = new RMIProviderForWeb();
		}
		return _instance;
	}

	public RMIProviderForWeb() throws RemoteException {
		super();
		try {
			Properties props = new Properties();
			FileInputStream fis = new FileInputStream(
					"./smartcon3_plcs.properties");
			props.load(fis);
			System.setProperty("java.rmi.server.hostname",
					props.getProperty("publicIP"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		I_RMIProviderForWeb stub = (I_RMIProviderForWeb) UnicastRemoteObject
				.exportObject(this, 1099);
		Registry registry = LocateRegistry.createRegistry(1099);
		registry.rebind("smartcon3_plcs", stub);
	}

	public boolean test(String str) throws RemoteException {
		return true;
	}

	@Override
	public synchronized I_RMI_Event getEvent(int eventLoggerId)
			throws RemoteException {
		I_Event event = EventManager.getInstance().getEvent(eventLoggerId);
		I_RMI_Event rmiEvent = null;
		if (event instanceof SensorEvent)
			rmiEvent = new RMI_SensorEvent((SensorEvent) event);
		else if (event instanceof DeviceDisconnectEvent)
			rmiEvent = new RMI_DeviceDisconnectEvent(
					(DeviceDisconnectEvent) event);
		else if (event instanceof DeviceStateChangedEvent)
			rmiEvent = new RMI_DeviceStateChangedEvent(
					(DeviceStateChangedEvent) event);
		else if (event instanceof FailToSendMessageEvent)
			rmiEvent = new RMI_FailToSendMessageEvent(
					(FailToSendMessageEvent) event);
		else if (event instanceof ReceiveResponseMessageEvent)
			rmiEvent = new RMI_SuccessToSendMessageEvent(
					(ReceiveResponseMessageEvent) event);
		else if (event instanceof NoticeUpgradeFirmwareEvent)
			rmiEvent = new RMI_NoticeUpgradeFirmwareEvent(
					(NoticeUpgradeFirmwareEvent) event);
		else if (event instanceof NoticeUpgradeFirmwareProgressEvent)
			rmiEvent = new RMI_NoticeUpgradeFirmwareProgressEvent(
					(NoticeUpgradeFirmwareProgressEvent) event);

		return rmiEvent;
	}

	@Override
	public int getNumOfEvent(int eventLoggerId) throws RemoteException {
		return EventManager.getInstance().getNumOfEvent(eventLoggerId);
	}

	@Override
	public synchronized RMI_DeviceManager getDeviceManager()
			throws RemoteException {
		DeviceManager deviceManager = DeviceManager.getInstance();
		RMI_DeviceManager rmiDeviceManager = new RMI_DeviceManager();
		for (I_Device device : deviceManager.getGatewayList()) {
			if (device instanceof MGW300) {
				MGW300 gateway = (MGW300) device;
				RMI_MGW300 rmiGateway = (RMI_MGW300) rmiDeviceManager
						.getGatewayById(gateway.getId());
				if (rmiGateway == null) {
					rmiDeviceManager.addGateway(new RMI_MGW300(gateway));
				} else {
					rmiGateway.update(gateway);
				}
			} else if (device instanceof OldMGW300) {
				OldMGW300 gateway = (OldMGW300) device;
				RMI_MGW300 rmiGateway = (RMI_MGW300) rmiDeviceManager
						.getGatewayById(gateway.getId());
				if (rmiGateway == null) {
					rmiDeviceManager.addGateway(new RMI_MGW300(gateway));
				} else {
					rmiGateway.update(gateway);
				}
			}
		}
		return rmiDeviceManager;
	}

	@Override
	public int sendPowermeterReq(RMI_LC100 rmiLC) {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendPowermeterReq();
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendPowermeterReq();
		}
		return -1;
	}

	@Override
	public boolean sendDeviceInfoReq(RMI_MGW300 rmiGateway)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());
		return gateway.sendNumOfDeviceReq();
	}

	@Override
	public boolean sendStateInfoReq(RMI_MGW300 rmiGateway)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());
		return gateway.sendStateInfoReq();
	}

	@Override
	public int sendControlSensorSharing(RMI_MGW300 rmiGateway, boolean enable)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());
		return gateway.sendControlSensorSharing(enable);
	}

	@Override
	public int sendControlGateway(RMI_MGW300 rmiGateway, String deviceIp,
			String serverIp, String defaultGatewayIp, String netmask, int port,
			boolean isSensorSharingMode) throws RemoteException,
			UnformattedPacketException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());

		return gateway.sendControlGateway(deviceIp, serverIp, defaultGatewayIp,
				netmask, port, isSensorSharingMode);
	}

	@Override
	public int sendDebugLogReq(RMI_MGW300 rmiGateway) throws RemoteException,
			UnformattedPacketException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			return gateway.sendDebugLogReq();
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			return gateway.sendDebugLogReq();
		}
		return -1;
	}

	@Override
	public boolean sendResetDebugLogReq(RMI_MGW300 rmiGateway)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			return gateway.sendResetDebugLogReq();
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			return gateway.sendResetDebugLogReq();
		}
		return false;
	}

	@Override
	public int sendSetSensingLevelReq(RMI_MSN300 rmiMsn, int level)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) rmiMsn.getParent()).getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = (LC100) gateway.getLC(rmiMsn.getParent().getId());
			MSN300 msn = (MSN300) lc.getDeviceById(rmiMsn.getId());
			return msn.sendSetSensingLevelReq(level);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = (OldLC100) gateway.getLC(rmiMsn.getParent().getId());
			OldMSN300 msn = (OldMSN300) lc.getDeviceById(rmiMsn.getId());
			return msn.sendSetSensingLevelReq(level);
		}
		return -1;
	}

	@Override
	public boolean sendStateInfoReq(RMI_MSN300 rmiMsn) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) rmiMsn.getParent()).getParent().getId());
		LC100 lc = (LC100) gateway.getLC(rmiMsn.getParent().getId());
		MSN300 msn = (MSN300) lc.getDeviceById(rmiMsn.getId());
		return msn.sendStateInfoReq();
	}

	@Override
	public boolean sendStateInfoReq(RMI_MDT100 rmiMdt) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) (rmiMdt.getParent())).getParent().getId());
		LC100 lc = (LC100) gateway.getLC(rmiMdt.getParent().getId());
		MDT100 mdt = (MDT100) lc.getDeviceById(rmiMdt.getId());
		return mdt.sendStateInfoReq();
	}

	@Override
	public int sendDebugLogLCReq(RMI_LC100 rmiLC) throws RemoteException,
			UnformattedPacketException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendDebugLogLCReq();
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendDebugLogLCReq();
		}
		return -1;
	}

	@Override
	public boolean sendResetDebugLCLogReq(RMI_LC100 rmiLC)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendResetDebugLCLogReq();
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendResetDebugLCLogReq();
		}
		return false;
	}

	@Override
	public int sendMappingSensorReq(RMI_LC100 rmiLC, short lcId,
			short sensorId, int circuitId, int onTime,
			E_GCP_ControlMode onCtrlMode, E_GCP_ControlMode offCtrlMode)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendMappingSensorReq(lcId, sensorId, circuitId, onTime,
					onCtrlMode, offCtrlMode);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendMappingSensorReq(sensorId, circuitId, onTime,
					onCtrlMode, offCtrlMode);
		}

		return -1;
	}

	@Override
	public int sendMappingDimmerReq(RMI_LC100 rmiLC, short dimmerId,
			int channelId, int circuitId) throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendMappingDimmerReq(dimmerId, channelId, circuitId);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendMappingDimmerReq(dimmerId, circuitId);
		}
		return -1;
	}

	@Override
	public int sendMappingSensorListReq(RMI_LC100 rmiLC, short lcId,
			short[] sensorId, int circuitId, int onTime,
			E_GCP_ControlMode onCtrlMode, E_GCP_ControlMode offCtrlMode)
			throws RemoteException, UnformattedPacketException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		return lc.sendMappingSensorListReq(lcId, sensorId, circuitId, onTime,
				onCtrlMode, offCtrlMode);
	}

	@Override
	public int sendMappingDimmerListReq(RMI_LC100 rmiLC, short[] dimmerId,
			int[] channelIds, int circuitId) throws RemoteException,
			UnformattedPacketException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		return lc.sendMappingDimmerListReq(dimmerId, channelIds, circuitId);

	}

	@Override
	public boolean sendUpdatePowermeter(RMI_LC100 rmiLC, int powermeterValue)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendUpdatePowermeter(powermeterValue);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendUpdatePowermeter(powermeterValue);
		}
		return false;
	}

	@Override
	public boolean sendSensorMappingInfoReq(RMI_LC100 rmiLC)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		return lc.sendSensorMappingSizeReq();
	}

	@Override
	public boolean sendDimmerMappingInfoReq(RMI_LC100 rmiLC)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		return lc.sendDimmerMappingSizeReq();
	}

	@Override
	public int sendResetSensorMappingReq(RMI_LC100 rmiLC, byte circuitId)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendResetSensorMappingReq(circuitId);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendResetSensorMappingReq();
		}
		return -1;

	}

	@Override
	public int sendResetDimmerMappingReq(RMI_LC100 rmiLC, byte circuitId)
			throws RemoteException {

		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendResetDimmerMappingReq(circuitId);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendResetDimmerMappingReq();
		}
		return -1;

	}

	@Override
	public int sendControlCircuit(RMI_LC100 rmiLC, int circuitId,
			E_GCP_ControlMode ctrlMode) throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendControlCircuit(circuitId, ctrlMode);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendControlCircuit(circuitId, ctrlMode);
		}

		return -1;
	}

	@Override
	public int sendControlSensor(RMI_LC100 rmiLC, int circuitId, int onTime,
			E_GCP_ControlMode ctrlModeOn, E_GCP_ControlMode ctrlModeOff)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		return lc.sendControlSensor(circuitId, onTime, ctrlModeOn, ctrlModeOff);

	}

	@Override
	public boolean sendStateInfoReq(RMI_LC100 rmiLC) throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendStateInfoReq();
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			return lc.sendStateInfoReq();
		}
		return false;
	}

	@Override
	public boolean sendDeviceInfoReq(RMI_LC100 rmiLC) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		return lc.sendNumOfDeviceReq();
	}

	@Override
	public boolean sendRebootReq(RMI_MGW300 rmiGateway) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());

		return gateway.sendRebootReq();
	}

	@Override
	public boolean sendRebootReq(RMI_LC100 rmiLC) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());

		LC100 lc = gateway.getLC(rmiLC.getId());

		return lc.sendRebootReq();
	}

	@Override
	public boolean sendRebootReq(RMI_MSN300 rmiMsn) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) (rmiMsn.getParent())).getParent().getId());

		LC100 lc = gateway.getLC(rmiMsn.getParent().getId());

		MSN300 msn = (MSN300) lc.getDeviceById(rmiMsn.getId());

		return msn.sendRebootReq();
	}

	@Override
	public boolean sendRebootReq(RMI_MDT100 rmiMdt) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) (rmiMdt.getParent())).getParent().getId());

		LC100 lc = gateway.getLC(rmiMdt.getParent().getId());

		MDT100 mdt = (MDT100) lc.getDeviceById(rmiMdt.getId());

		return mdt.sendRebootReq();
	}

	@Override
	public List<I_RMI_Event> getEventAll(int eventLoggerId)
			throws RemoteException {
		I_Event event = null;
		I_RMI_Event rmiEvent = null;
		List<I_RMI_Event> eventList = new ArrayList<I_RMI_Event>();
		for (int i = 0; i < EventManager.getInstance().getNumOfEvent(
				eventLoggerId); i++) {
			event = EventManager.getInstance().getEvent(eventLoggerId);
			if (event instanceof SensorEvent)
				rmiEvent = new RMI_SensorEvent((SensorEvent) event);
			else if (event instanceof DeviceDisconnectEvent)
				rmiEvent = new RMI_DeviceDisconnectEvent(
						(DeviceDisconnectEvent) event);
			else if (event instanceof DeviceStateChangedEvent)
				rmiEvent = new RMI_DeviceStateChangedEvent(
						(DeviceStateChangedEvent) event);
			else if (event instanceof FailToSendMessageEvent)
				rmiEvent = new RMI_FailToSendMessageEvent(
						(FailToSendMessageEvent) event);
			else if (event instanceof ReceiveResponseMessageEvent)
				rmiEvent = new RMI_SuccessToSendMessageEvent(
						(ReceiveResponseMessageEvent) event);
			else if (event instanceof NoticeUpgradeFirmwareEvent)
				rmiEvent = new RMI_NoticeUpgradeFirmwareEvent(
						(NoticeUpgradeFirmwareEvent) event);
			else if (event instanceof NoticeUpgradeFirmwareProgressEvent)
				rmiEvent = new RMI_NoticeUpgradeFirmwareProgressEvent(
						(NoticeUpgradeFirmwareProgressEvent) event);
			if (rmiEvent != null)
				eventList.add(rmiEvent);
		}
		return eventList;
	}

	@Override
	public int getNewEventLoggerId() throws RemoteException {
		int loggerId=0;
		loggerId = EventManager.getInstance().getNewEventLoggerId();
		MyLog.e("Allocate EventLoggerId at "+Calendar.getInstance().getTime()+"\nLoggerId: "+loggerId);
		return loggerId;
	}

	@Override
	public int sendAddScheduleReq(RMI_LC100 rmiLC,
			E_GCP_ScheduleType scheduleType, int circuitId, Date onTime,
			E_GCP_ControlMode ctrlMode) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		return lc.sendAddScheduleReq(scheduleType, circuitId, onTime, ctrlMode);
	}

	@Override
	public int sendAddScheduleListReq(RMI_LC100 rmiLC,
			E_GCP_ScheduleType scheduleType, int circuitId, Date[] onTime,
			E_GCP_ControlMode[] ctrlMode) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		try {
			return lc.sendAddScheduleListReq(scheduleType, circuitId, onTime,
					ctrlMode);
		} catch (UnformattedPacketException e) {
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
		return -1;
	}

	@Override
	public boolean sendScheduleInfoReq(RMI_LC100 rmiLC,
			E_GCP_ScheduleType scheduleType) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		return lc.sendScheduleSizeReq(scheduleType);
	}

	@Override
	public int sendResetScheduleReq(RMI_LC100 rmiLC,
			E_GCP_ScheduleType scheduleType, byte circuitId)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		return lc.sendResetScheduleReq(scheduleType, circuitId);
	}

	@Override
	public boolean sendTimeInfo(RMI_MGW300 rmiGateway) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());
		return gateway.sendTimeInfo();
	}

	@Override
	public boolean sendControlSensorSharing_Synchronized(RMI_MGW300 rmiGateway,
			boolean enable) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());
		int seqNum_Req = gateway.sendControlSensorSharing(enable);

		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendControlGateway_Synchronized(RMI_MGW300 rmiGateway,
			String deviceIp, String serverIp, String defaultGatewayIp,
			String netmask, int port, boolean isSensorSharingMode)
			throws RemoteException, UnformattedPacketException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());

		int seqNum_Req = gateway.sendControlGateway(deviceIp, serverIp,
				defaultGatewayIp, netmask, port, isSensorSharingMode);

		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendDebugLogReq_Synchronized(RMI_MGW300 rmiGateway)
			throws RemoteException, UnformattedPacketException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());
		int seqNum_Req = -1;
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			seqNum_Req = gateway.sendDebugLogReq();
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			seqNum_Req = gateway.sendDebugLogReq();
		}
		if (seqNum_Req != -1)
			return getSynchronizedMessageResult(seqNum_Req);
		return true;
	}

	@Override
	public boolean sendPowermeterReq_Synchronized(RMI_LC100 rmiLC)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		int seqNum_Req = -1;
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendPowermeterReq();
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendPowermeterReq();
		}
		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendDebugLogLCReq_Synchronized(RMI_LC100 rmiLC)
			throws RemoteException, UnformattedPacketException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		int seqNum_Req = -1;
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendDebugLogLCReq();
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendDebugLogLCReq();
		}

		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendMappingSensorReq_Synchronized(RMI_LC100 rmiLC,
			short lcId, short sensorId, int circuitId, int onTime,
			E_GCP_ControlMode onCtrlMode, E_GCP_ControlMode offCtrlMode)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		int seqNum_Req = -1;
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendMappingSensorReq(lcId, sensorId, circuitId,
					onTime, onCtrlMode, offCtrlMode);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendMappingSensorReq(sensorId, circuitId, onTime,
					onCtrlMode, offCtrlMode);
			return true;
		}

		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendMappingDimmerReq_Synchronized(RMI_LC100 rmiLC,
			short dimmerId, int channelId, int circuitId)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		int seqNum_Req = -1;
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc
					.sendMappingDimmerReq(dimmerId, channelId, circuitId);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendMappingDimmerReq(dimmerId, circuitId);
			return true;
		}

		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendAddScheduleReq_Synchronized(RMI_LC100 rmiLC,
			E_GCP_ScheduleType scheduleType, int circuitId, Date onTime,
			E_GCP_ControlMode ctrlMode) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		int seqNum_Req = -1;
		seqNum_Req = lc.sendAddScheduleReq(scheduleType, circuitId, onTime,
				ctrlMode);

		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendMappingSensorListReq_Synchronized(RMI_LC100 rmiLC,
			short lcId, short[] sensorId, int circuitId, int onTime,
			E_GCP_ControlMode onCtrlMode, E_GCP_ControlMode offCtrlMode)
			throws RemoteException, UnformattedPacketException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());

		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			int seqNum_Req = lc.sendMappingSensorListReq(lcId, sensorId,
					circuitId, onTime, onCtrlMode, offCtrlMode);
			return getSynchronizedMessageResult(seqNum_Req);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			for (int i = 0; i < sensorId.length; i++)
				lc.sendMappingSensorReq(sensorId[i], circuitId, onTime,
						onCtrlMode, offCtrlMode);
			return true;
		}
		return false;
	}

	@Override
	public boolean sendMappingDimmerListReq_Synchronized(RMI_LC100 rmiLC,
			short[] dimmerId, int[] channelIds, int circuitId)
			throws RemoteException, UnformattedPacketException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());

		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			int seqNum_Req = lc.sendMappingDimmerListReq(dimmerId, channelIds,
					circuitId);
			return getSynchronizedMessageResult(seqNum_Req);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			for (int i = 0; i < dimmerId.length; i++)
				lc.sendMappingDimmerReq(dimmerId[i], circuitId);
			return true;
		}
		return false;
	}

	@Override
	public boolean sendAddScheduleListReq_Synchronized(RMI_LC100 rmiLC,
			E_GCP_ScheduleType scheduleType, int circuitId, Date[] onTime,
			E_GCP_ControlMode[] ctrlMode) throws RemoteException {

		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		int seqNum_Req = -1;
		try {
			seqNum_Req = lc.sendAddScheduleListReq(scheduleType, circuitId,
					onTime, ctrlMode);
		} catch (UnformattedPacketException e) {
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendResetSensorMappingReq_Synchronized(RMI_LC100 rmiLC,
			byte circuitId) throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		int seqNum = -1;
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			seqNum = lc.sendResetSensorMappingReq(circuitId);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			seqNum = lc.sendResetSensorMappingReq();
			return true;
		}
		return getSynchronizedMessageResult(seqNum);
	}

	@Override
	public boolean sendResetDimmerMappingReq_Synchronized(RMI_LC100 rmiLC,
			byte circuitId) throws RemoteException {

		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		int seqNum_Req = -1;
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendResetDimmerMappingReq(circuitId);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendResetDimmerMappingReq();
			return true;
		}

		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendResetScheduleReq_Synchronized(RMI_LC100 rmiLC,
			E_GCP_ScheduleType scheduleType, byte circuitId)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		int seqNum_Req = lc.sendResetScheduleReq(scheduleType, circuitId);

		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendControlCircuit_Synchronized(RMI_LC100 rmiLC,
			int circuitId, E_GCP_ControlMode ctrlMode) throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		int seqNum_Req = -1;
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendControlCircuit(circuitId, ctrlMode);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = gateway.getLC(rmiLC.getId());
			seqNum_Req = lc.sendControlCircuit(circuitId, ctrlMode);
			return true;
		}

		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendControlSensor_Synchronized(RMI_LC100 rmiLC,
			int circuitId, int onTime, E_GCP_ControlMode ctrlModeOn,
			E_GCP_ControlMode ctrlModeOff) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiLC.getParent().getId());
		LC100 lc = gateway.getLC(rmiLC.getId());
		int seqNum_Req = lc.sendControlSensor(circuitId, onTime, ctrlModeOn,
				ctrlModeOff);

		return getSynchronizedMessageResult(seqNum_Req);
	}

	@Override
	public boolean sendSetSensingLevelReq_Synchronized(RMI_MSN300 rmiMsn,
			int level) throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) rmiMsn.getParent()).getParent().getId());
		int seqNum_Req = -1;
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = (LC100) gateway.getLC(rmiMsn.getParent().getId());
			MSN300 msn = (MSN300) lc.getDeviceById(rmiMsn.getId());
			seqNum_Req = msn.sendSetSensingLevelReq(level);
		} else if (device instanceof OldMGW300) {
			OldMGW300 gateway = (OldMGW300) device;
			OldLC100 lc = (OldLC100) gateway.getLC(rmiMsn.getParent().getId());
			OldMSN300 msn = (OldMSN300) lc.getDeviceById(rmiMsn.getId());
			seqNum_Req = msn.sendSetSensingLevelReq(level);
			return true;
		}

		return getSynchronizedMessageResult(seqNum_Req);
	}

	public boolean getSynchronizedMessageResult(int seqNum_Req) {
		try {
			Thread.sleep(200);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		for (int i = 0; i < 10; i++) {
			try {
				if (SynchronizedMessageEventHandler.getInstance().getResult(
						seqNum_Req))
					return true;
				else
					return false;
			} catch (NullResultException e) {
				// e.printStackTrace();
				try {
					Thread.sleep(200);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
			}
		}
		MyLog.e("NullResultException at getSynchronizedMessageResult: "
				+ seqNum_Req);
		return false;
	}

	@Override
	public boolean removeDevice(I_RMI_Device rmiDevice) throws RemoteException {
		I_Device device = null;
		if (rmiDevice instanceof RMI_MGW300)
			device = DeviceManager.getInstance().getGatewayById(
					rmiDevice.getId());
		else if (rmiDevice instanceof RMI_LC100) {
			I_Device gateway = DeviceManager.getInstance().getGatewayById(
					((RMI_LC100) rmiDevice).getParent().getId());
			if (gateway instanceof MGW300)
				device = ((MGW300) gateway).getLC(rmiDevice.getId());
			else if (gateway instanceof OldMGW300)
				device = ((OldMGW300) gateway).getLC(rmiDevice.getId());
		} else if (rmiDevice instanceof RMI_MSN300) {
			RMI_LC100 rmiLc = (RMI_LC100) ((RMI_MSN300) rmiDevice).getParent();
			I_Device gateway = DeviceManager.getInstance().getGatewayById(
					rmiLc.getParent().getId());
			if (gateway instanceof MGW300) {
				LC100 lc = ((MGW300) gateway).getLC(rmiLc.getId());
				device = lc.getDeviceById(rmiDevice.getId());
			} else if (gateway instanceof OldMGW300) {
				OldLC100 lc = ((OldMGW300) gateway).getLC(rmiLc.getId());
				device = lc.getDeviceById(rmiDevice.getId());
			}
		} else if (rmiDevice instanceof RMI_MDT100) {
			RMI_LC100 rmiLc = (RMI_LC100) ((RMI_MDT100) rmiDevice).getParent();
			I_Device gateway = DeviceManager.getInstance().getGatewayById(
					rmiLc.getParent().getId());
			if (gateway instanceof MGW300) {
				LC100 lc = ((MGW300) gateway).getLC(rmiLc.getId());
				device = lc.getDeviceById(rmiDevice.getId());
			} else if (gateway instanceof OldMGW300) {
				OldLC100 lc = ((OldMGW300) gateway).getLC(rmiLc.getId());
				device = lc.getDeviceById(rmiDevice.getId());
			}
		} else if (rmiDevice instanceof RMI_MDF100) {
			RMI_LC100 rmiLc = (RMI_LC100) ((RMI_MDF100) rmiDevice).getParent();
			I_Device gateway = DeviceManager.getInstance().getGatewayById(
					rmiLc.getParent().getId());
			if (gateway instanceof MGW300) {
				LC100 lc = ((MGW300) gateway).getLC(rmiLc.getId());
				device = lc.getDeviceById(rmiDevice.getId());
			} else if (gateway instanceof OldMGW300) {
				OldLC100 lc = ((OldMGW300) gateway).getLC(rmiLc.getId());
				device = lc.getDeviceById(rmiDevice.getId());
			}
		}

		if (device != null)
			return DeviceManager.getInstance().removeDevice(device);
		else
			return false;
	}

	public int sendPowermeterReq(RMI_MDF100 rmiMdf, int channel)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				((RMI_MGW300) ((RMI_LC100) rmiMdf.getParent()).getParent())
						.getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiMdf.getParent().getId());
			MDF100 mdf = (MDF100) (lc.getDeviceById(rmiMdf.getId()));
			return mdf.sendPowermeterReq(channel);
		}
		return -1;
	}

	public boolean sendPowermeterReq_Synchronized(RMI_MDF100 rmiMdf, int channel)
			throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				((RMI_MGW300) ((RMI_LC100) rmiMdf.getParent()).getParent())
						.getId());
		int seqNum_Req = -1;
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiMdf.getParent().getId());
			MDF100 mdf = (MDF100) (lc.getDeviceById(rmiMdf.getId()));
			seqNum_Req = mdf.sendPowermeterReq(channel);
		}
		return getSynchronizedMessageResult(seqNum_Req);
	}

	public boolean sendUpdatePowermeter(RMI_MDF100 rmiMdf, int channel,
			int powermeterValue) throws RemoteException {
		I_Device device = DeviceManager.getInstance().getGatewayById(
				((RMI_MGW300) ((RMI_LC100) rmiMdf.getParent()).getParent())
						.getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = gateway.getLC(rmiMdf.getParent().getId());
			MDF100 mdf = (MDF100) (lc.getDeviceById(rmiMdf.getId()));
			return mdf.sendUpdatePowermeter(channel, powermeterValue);
		}
		return false;
	}

	public boolean sendStateInfoReq(RMI_MDF100 rmiMdf) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) (rmiMdf.getParent())).getParent().getId());
		LC100 lc = (LC100) gateway.getLC(rmiMdf.getParent().getId());
		MDF100 mdf = (MDF100) lc.getDeviceById(rmiMdf.getId());
		return mdf.sendStateInfoReq();
	}

	public boolean sendRebootReq(RMI_MDF100 rmiMdf) throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) (rmiMdf.getParent())).getParent().getId());

		LC100 lc = gateway.getLC(rmiMdf.getParent().getId());

		MDF100 mdf = (MDF100) lc.getDeviceById(rmiMdf.getId());

		return mdf.sendRebootReq();
	}

	@Override
	public long getSensorEventCount(RMI_MSN300 rmiMsn, long dateTimeInMinute)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) (rmiMsn.getParent())).getParent().getId());

		LC100 lc = gateway.getLC(rmiMsn.getParent().getId());

		MSN300 msn = (MSN300) lc.getDeviceById(rmiMsn.getId());

		return SensorEventLogger.getInstance().getEventCount(msn,
				dateTimeInMinute);
	}

	@Override
	public List<Long> getSensorEventCount(RMI_MSN300 rmiMsn,
			long fromDateTimeInMinute, long toDateTimeInMinute)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) (rmiMsn.getParent())).getParent().getId());

		LC100 lc = gateway.getLC(rmiMsn.getParent().getId());

		MSN300 msn = (MSN300) lc.getDeviceById(rmiMsn.getId());

		ArrayList<Long> list = new ArrayList<Long>();
		for (long i = fromDateTimeInMinute; i < toDateTimeInMinute; i++)
			list.add(SensorEventLogger.getInstance().getEventCount(msn, i));

		return list;
	}

	@Override
	public List<Integer> getSensorLevel(RMI_MSN300 rmiMsn,
			long fromDateTimeInMinute, long toDateTimeInMinute)
			throws RemoteException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) (rmiMsn.getParent())).getParent().getId());

		LC100 lc = gateway.getLC(rmiMsn.getParent().getId());

		MSN300 msn = (MSN300) lc.getDeviceById(rmiMsn.getId());

		ArrayList<Integer> list = new ArrayList<Integer>();
		for (long i = fromDateTimeInMinute; i < toDateTimeInMinute; i++)
			list.add(SensorEventLogger.getInstance().getSensorLevel(msn, i));

		return list;
	}

	@Override
	public int sendControlNoticeModeReq(RMI_MGW300 rmiGateway,
			boolean enable) throws RemoteException, UnformattedPacketException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());
		return gateway.sendControlNoticeMode(enable);
	}

	@Override
	public boolean sendControlNoticeModeReq_Synchronized(RMI_MGW300 rmiGateway,
			boolean enable) throws RemoteException, UnformattedPacketException {
		MGW300 gateway = (MGW300) DeviceManager.getInstance().getGatewayById(
				rmiGateway.getId());
		int seqNum_Req = gateway.sendControlNoticeMode(enable);

		return getSynchronizedMessageResult(seqNum_Req);
	}
}
