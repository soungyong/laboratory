package m2m.smartcon3.provider;

import java.io.Serializable;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.MIR100;

public class RMI_MIR100 implements I_RMI_Device, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3147150688070005461L;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.MIR100;
	private int firmwareVersion;
	private boolean power;
	private int mode;
	private int windPower;
	private int windPowerAir;
	private int temperature;
	private int temperatureDehumidify;
	private boolean isTurbo;
	private boolean savingMode;
	private boolean naturalWind;
	I_RMI_Device parent = null;
	
	NETWORK_STATE connectionState = NETWORK_STATE.OFFLINE;

	public RMI_MIR100(MIR100 device, I_RMI_Device parent) {
		this.deviceId = device.getId();
		this.parent = parent;
		if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE) {
			connectionState = NETWORK_STATE.ONLINE;
		}
		this.firmwareVersion = device.getFirmwareVersion();
		
		this.power = device.isPower();
		this.mode = device.getMode();
		this.windPower = device.getWindPower();
		this.windPowerAir = device.getWindPowerAir();
		this.temperature = device.getTemperature();
		this.temperatureDehumidify = device.getTemperatureDehumidify();
		this.isTurbo = device.isTurbo();
		this.savingMode = device.isSavingMode();
		this.naturalWind = device.isNaturalWind();
	}

	@Override
	public short getId() {
		return this.deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		return connectionState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}
	
	public I_RMI_Device getParent() {
		return parent;
	}

	@Override
	public void update(I_Device dev) {
		if(dev instanceof MIR100) {
			MIR100 device = (MIR100) dev;
			this.deviceId = device.getId();
			if(device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE) {
				connectionState = NETWORK_STATE.ONLINE;
			} else {
				connectionState = NETWORK_STATE.OFFLINE;
			}
			this.firmwareVersion = device.getFirmwareVersion();
			
			this.power = device.isPower();
			this.mode = device.getMode();
			this.windPower = device.getWindPower();
			this.windPowerAir = device.getWindPowerAir();
			this.temperature = device.getTemperature();
			this.temperatureDehumidify = device.getTemperatureDehumidify();
			this.isTurbo = device.isTurbo();
			this.savingMode = device.isSavingMode();
			this.naturalWind = device.isNaturalWind();
		}
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public void setFirmwareVersion(int firmwareVersion) {
		this.firmwareVersion = firmwareVersion;
	}

	public boolean isPower() {
		return power;
	}

	public int getMode() {
		return mode;
	}

	public int getWindPower() {
		return windPower;
	}

	public int getWindPowerAir() {
		return windPowerAir;
	}

	public int getTemperature() {
		return temperature;
	}

	public int getTemperatureDehumidify() {
		return temperatureDehumidify;
	}

	public boolean isTurbo() {
		return isTurbo;
	}

	public boolean isSavingMode() {
		return savingMode;
	}

	public boolean isNaturalWind() {
		return naturalWind;
	}
	
	

}
