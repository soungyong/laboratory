package m2m.smartcon3.provider;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.module.SensorEventLog;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ScheduleType;

public interface I_RMIProviderForRemoteFirmwareUpgrade extends Remote {
	boolean test(String str) throws RemoteException;
	
	RMI_DeviceManager getDeviceManager() throws RemoteException;

	public int upgradeFirmwareReq(RMI_MGW300 rmiMgw, byte[]data)
	throws RemoteException;
	
	public int upgradeFirmwareReq(RMI_LC100 rmiLc, byte[]data)
	throws RemoteException;
	
	public int upgradeFirmwareReq(RMI_MSN300 msn, byte[]data)
			throws RemoteException;
	
	public int upgradeFirmwareReq(RMI_MDT100 rmiMdt, byte[]data)
	throws RemoteException;
	
	public int upgradeFirmwareReq(RMI_MDF100 rmiMdf, byte[]data)
	throws RemoteException;
	
	public int upgradeFirmwareReq(RMI_MPR200 rmiMpr, byte[]data)
	throws RemoteException;
}
