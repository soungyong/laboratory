package m2m.smartcon3.provider;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.LC100;
import m2m.smartcon3.device.MGW300;
import m2m.smartcon3.device.OldLC100;
import m2m.smartcon3.device.OldMGW300;

public class RMI_MGW300 implements I_RMI_Device, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9L;
	short deviceId = 0x00;
	E_DeviceType deviceType = E_DeviceType.MGW300;

	HashMap<Short, I_RMI_Device> lcMap = new HashMap<Short, I_RMI_Device>();

	String ipAddress = "192.168.0.50";
	String serverIpAddress = "192.168.0.10";
	String defaultGateway = "192.168.0.1";
	String netMask = "255.255.255.0";
	int port = 0;
	boolean isSensorSharingMode = false;

	Calendar currentTime = Calendar.getInstance();
	int rebootCount = 0;
	Calendar lastRebootTime = Calendar.getInstance();
	int serverConnectionCount = 0;
	Calendar lastServerConnectionTime = Calendar.getInstance();
	private NETWORK_STATE connectionState = NETWORK_STATE.OFFLINE;
	int firmwareVersion=0;
	int deviceVersion=0;

	public RMI_MGW300(MGW300 device) {
		this();
		this.deviceId = device.getId();
		this.ipAddress = device.getIpAddress();
		this.serverIpAddress = device.getServerIpAddress();
		this.defaultGateway = device.getDefaultGateway();
		this.netMask = device.getNetMask();
		this.port = device.getPort();
		this.isSensorSharingMode = device.isSensorSharingMode();
		this.currentTime = device.getCurrentTime();
		this.rebootCount = device.getRebootCount();
		this.lastRebootTime = device.getLastRebootTime();
		this.serverConnectionCount = device.getServerConnectionCount();
		this.lastServerConnectionTime = device.getLastServerConnectionTime();
		if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		this.deviceVersion = device.getDeviceVersion();
		this.firmwareVersion = device.getFirmwareVersion();

		for (int i = 0; i < device.getNumOfSubDevices(); i++) {
			RMI_LC100 rmiLC = new RMI_LC100((LC100) device.getLCByIndex(i),
					this);
			addLC(rmiLC);
		}
	}

	public RMI_MGW300(OldMGW300 device) {
		this();
		this.deviceId = device.getId();
		this.ipAddress = device.getIpAddress();
		this.serverIpAddress = device.getServerIpAddress();
		this.defaultGateway = device.getDefaultGateway();
		this.netMask = device.getNetMask();
		this.port = device.getPort();
		this.isSensorSharingMode = device.isSensorSharingMode();
		this.currentTime = device.getCurrentTime();
		this.rebootCount = device.getRebootCount();
		this.lastRebootTime = device.getLastRebootTime();
		this.serverConnectionCount = device.getServerConnectionCount();
		this.lastServerConnectionTime = device.getLastServerConnectionTime();
		if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		this.deviceVersion = device.getDeviceVersion();
		this.firmwareVersion = device.getFirmwareVersion();

		for (int i = 0; i < device.getNumOfSubDevices(); i++) {
			RMI_LC100 rmiLC = new RMI_LC100((OldLC100) device.getLCByIndex(i),
					this);
			addLC(rmiLC);
		}
	}

	public RMI_MGW300() {
	}

	public void addLC(RMI_LC100 device) {
		device.setParent(this);
		lcMap.put(device.getId(), device);
	}

	public boolean removeLC(RMI_LC100 device) {
		if (lcMap.containsValue(device))
			lcMap.remove(device.getId());
		else
			return false;
		return true;
	}

	public RMI_LC100 getLC(short id) {
		return (RMI_LC100) lcMap.get(id);
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		return this.connectionState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		// TODO Auto-generated method stub
		return deviceType;
	}

	public I_RMI_Device getLCByIndex(int index) {
		if (lcMap.size() <= index)
			return null;
		int cnt = 0;
		for (I_RMI_Device device : lcMap.values()) {
			if (cnt == index)
				return device;
			cnt++;
		}
		return null;
	}

	public String getIpAddress() {
		return ipAddress;
	}

	public String getServerIpAddress() {
		return serverIpAddress;
	}

	public String getDefaultGateway() {
		return defaultGateway;
	}

	public String getNetMask() {
		return netMask;
	}

	public int getPort() {
		return port;
	}

	public boolean isSensorSharingMode() {
		return isSensorSharingMode;
	}

	public int getNumOfSubDevices() {
		return lcMap.size();
	}

	@Override
	public void update(I_Device dev) {
		if (dev instanceof MGW300) {
			MGW300 device = (MGW300) dev;

			this.firmwareVersion = device.getFirmwareVersion();
			this.deviceId = device.getId();
			this.ipAddress = device.getIpAddress();
			this.serverIpAddress = device.getServerIpAddress();
			this.defaultGateway = device.getDefaultGateway();
			this.netMask = device.getNetMask();
			this.port = device.getPort();
			this.isSensorSharingMode = device.isSensorSharingMode();
			this.currentTime = device.getCurrentTime();
			this.rebootCount = device.getRebootCount();
			this.lastRebootTime = device.getLastRebootTime();
			this.serverConnectionCount = device.getServerConnectionCount();
			this.lastServerConnectionTime = device
					.getLastServerConnectionTime();
			if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;

			for (int i = 0; i < device.getNumOfSubDevices(); i++) {
				LC100 lc = (LC100) device.getLCByIndex(i);
				RMI_LC100 rmiLC = getLC(lc.getId());
				if (rmiLC == null)
					addLC(new RMI_LC100(lc, this));
				else
					rmiLC.update(lc);
			}
		}else if (dev instanceof OldMGW300) {
			OldMGW300 device = (OldMGW300) dev;

			this.firmwareVersion = device.getFirmwareVersion();
			this.deviceId = device.getId();
			this.ipAddress = device.getIpAddress();
			this.serverIpAddress = device.getServerIpAddress();
			this.defaultGateway = device.getDefaultGateway();
			this.netMask = device.getNetMask();
			this.port = device.getPort();
			this.isSensorSharingMode = device.isSensorSharingMode();
			this.currentTime = device.getCurrentTime();
			this.rebootCount = device.getRebootCount();
			this.lastRebootTime = device.getLastRebootTime();
			this.serverConnectionCount = device.getServerConnectionCount();
			this.lastServerConnectionTime = device
					.getLastServerConnectionTime();
			if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;

			for (int i = 0; i < device.getNumOfSubDevices(); i++) {
				OldLC100 lc = (OldLC100) device.getLCByIndex(i);
				RMI_LC100 rmiLC = getLC(lc.getId());
				if (rmiLC == null)
					addLC(new RMI_LC100(lc, this));
				else
					rmiLC.update(lc);
			}
		}

	}

	public short getDeviceId() {
		return deviceId;
	}

	public HashMap<Short, I_RMI_Device> getLcMap() {
		return lcMap;
	}

	public Calendar getCurrentTime() {
		return currentTime;
	}

	public int getRebootCount() {
		return rebootCount;
	}

	public Calendar getLastRebootTime() {
		return lastRebootTime;
	}

	public int getServerConnectionCount() {
		return serverConnectionCount;
	}

	public Calendar getLastServerConnectionTime() {
		return lastServerConnectionTime;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public int getDeviceVersion() {
		return deviceVersion;
	}
	
	
	
}
