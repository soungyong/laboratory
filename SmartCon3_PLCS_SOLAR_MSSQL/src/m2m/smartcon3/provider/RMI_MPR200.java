package m2m.smartcon3.provider;

import java.io.Serializable;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.MDT100;
import m2m.smartcon3.device.MPR200;
import m2m.smartcon3.device.OldMDT100;
import m2m.smartcon3.module.RemoteFirmwareUpdateHelper;
import m2m.util.MyLog;

public class RMI_MPR200 implements I_RMI_Device, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3216060291914084052L;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.MPR200;
	private int firmwareVersion;
	I_RMI_Device parent = null;
	NETWORK_STATE connectionState = NETWORK_STATE.OFFLINE;
	private int temperature;
	private int current;
	

	public RMI_MPR200(MPR200 device, I_RMI_Device parent) {
		this.parent = parent;
		this.deviceId = device.getId();
		this.firmwareVersion = device.getFirmwareVersion();
		if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		else
			connectionState = NETWORK_STATE.OFFLINE;
		
		this.temperature = device.getTemperature();
		this.current = device.getCurrent();
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		return connectionState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}
	
	

	public short getDeviceId() {
		return deviceId;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public I_RMI_Device getParent() {
		return parent;
	}

	public int getTemperature() {
		return temperature;
	}

	public int getCurrent() {
		return current;
	}

	@Override
	public void update(I_Device dev) {
		if (dev instanceof MPR200) {
			MPR200 device = (MPR200) dev;
			this.deviceId = device.getId();
			this.firmwareVersion = device.getFirmwareVersion();
			if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;
			this.temperature = device.getTemperature();
			this.current = device.getCurrent();
		}
	}
	
}
