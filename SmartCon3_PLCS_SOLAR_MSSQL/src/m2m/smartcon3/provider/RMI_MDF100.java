package m2m.smartcon3.provider;

import java.io.Serializable;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.MDF100;
import m2m.smartcon3.device.MDT100;
import m2m.smartcon3.device.OldMDT100;

public class RMI_MDF100 implements I_RMI_Device, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7L;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.MDF100;
	private int firmwareVersion;
	private int powermeterValue[] = new int[4];
	private int temperature = 0;
	private int illumination = 0;

	I_RMI_Device parent = null;
	NETWORK_STATE connectionState = NETWORK_STATE.OFFLINE;

	public RMI_MDF100(MDF100 device, I_RMI_Device parent) {
		this.parent = parent;
		this.deviceId = device.getId();
		this.firmwareVersion = device.getFirmwareVersion();
		for (int i = 0; i < 4; i++)
			this.powermeterValue[i] = device.getPowermeterValue(i);
		this.illumination = device.getIllumination();
		this.temperature = device.getTemperature();

		if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		else
			connectionState = NETWORK_STATE.OFFLINE;
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		return connectionState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}

	public short getDeviceId() {
		return deviceId;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public I_RMI_Device getParent() {
		return parent;
	}

	public int getPowermeterValue(int channel) {
		if (channel < 4)
			return this.powermeterValue[channel];
		return -1;
	}

	public int[] getPowermeterValue() {
		return powermeterValue;
	}

	public int getTemperature() {
		return temperature;
	}

	public int getIllumination() {
		return illumination;
	}

	@Override
	public void update(I_Device dev) {
		if (dev instanceof MDF100) {
			MDF100 device = (MDF100) dev;
			this.deviceId = device.getId();
			this.firmwareVersion = device.getFirmwareVersion();
			for (int i = 0; i < 4; i++)
				this.powermeterValue[i] = device.getPowermeterValue(i);
			this.illumination = device.getIllumination();
			this.temperature = device.getTemperature();

			if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;
		}
	}
}
