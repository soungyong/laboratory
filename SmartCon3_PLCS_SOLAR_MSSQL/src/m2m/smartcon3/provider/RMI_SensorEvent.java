package m2m.smartcon3.provider;

import java.io.Serializable;
import java.util.Date;

import m2m.smartcon3.event.SensorEvent;

public class RMI_SensorEvent extends RMI_EventImplBase implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 11L;
	short sensorId = 0;
	short lcId = 0;
	short gatewayId = 0;

	public RMI_SensorEvent() {
		super("");
	}

	public RMI_SensorEvent(SensorEvent sensorEvent) {
		super(sensorEvent.getName());
		this.sensorId = sensorEvent.getDevice().getId();
		this.lcId = sensorEvent.getDevice().getParent().getId();
		this.gatewayId = sensorEvent.getDevice().getParent().getParent()
				.getId();
		this.date = sensorEvent.getDate();		
	}

	public short getSensorId() {
		return sensorId;
	}

	public short getLcId() {
		return lcId;
	}

	public short getGatewayId() {
		return gatewayId;
	}	
}
