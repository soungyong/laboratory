package m2m.smartcon3.provider;

import java.io.Serializable;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.event.DeviceStateChangedEvent;

public class RMI_DeviceStateChangedEvent extends RMI_EventImplBase implements Serializable {
	short deviceId = 0;
	short lcId = 0;
	short gatewayId = 0;
	E_DeviceType deviceType;
	private static final long serialVersionUID = 3L;
	public RMI_DeviceStateChangedEvent() {
		super("");
	}

	public RMI_DeviceStateChangedEvent(DeviceStateChangedEvent event) {
		super(event.getName());
		this.deviceId = event.getDevice().getId();
		this.deviceType = event.getDevice().getDeviceType();
		if(deviceType == E_DeviceType.LC100){
			this.gatewayId = event.getDevice().getParent()
			.getId();
			this.lcId = deviceId;
		}else if(deviceType == E_DeviceType.MSN300){
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent().getId();
		}else if(deviceType == E_DeviceType.MDT100){
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent().getId();
		}else if(deviceType == E_DeviceType.MDF100){
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent().getId();
		}else if(deviceType == E_DeviceType.MPR200){
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent().getId();
		}else if(deviceType == E_DeviceType.MIR100){
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent().getId();
		} else if(deviceType == E_DeviceType.SolarSensorGetter){
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent().getId();
		}
		this.date = event.getDate();		
	}

	public short getDeviceId() {
		return deviceId;
	}

	public short getLcId() {
		return lcId;
	}

	public short getGatewayId() {
		return gatewayId;
	}

	public E_DeviceType getDeviceType() {
		return deviceType;
	}	
	
	
}
