package m2m.smartcon3.provider;

import java.io.Serializable;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.util.HexUtils;

public class RMI_SuccessToSendMessageEvent extends RMI_EventImplBase implements
		Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 13L;
	short deviceId = 0;
	short lcId = 0;
	short gatewayId = 0;
	E_DeviceType deviceType;
	int seqNum;

	public RMI_SuccessToSendMessageEvent() {
		super("");
	}

	public RMI_SuccessToSendMessageEvent(ReceiveResponseMessageEvent event) {
		super(event.getName());
		this.deviceId = event.getDevice().getId();
		this.deviceType = event.getDevice().getDeviceType();
		this.seqNum = HexUtils.toInt(HexUtils.toHexString(event.getResMessage().getSeqNum()));

		if (deviceType == E_DeviceType.LC100) {
			this.gatewayId = event.getDevice().getParent().getId();
		} else if (deviceType == E_DeviceType.MSN300) {
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent()
			.getId();
		} else if (deviceType == E_DeviceType.MDT100) {
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent()
			.getId();
		}else if (deviceType == E_DeviceType.MDF100) {
			this.lcId = event.getDevice().getParent().getId();
			this.gatewayId = event.getDevice().getParent().getParent()
			.getId();
		}

		this.date = event.getDate();
	}

	public short getDeviceId() {
		return deviceId;
	}

	public short getLcId() {
		return lcId;
	}

	public short getGatewayId() {
		return gatewayId;
	}

	public E_DeviceType getDeviceType() {
		return deviceType;
	}
	
	
}
