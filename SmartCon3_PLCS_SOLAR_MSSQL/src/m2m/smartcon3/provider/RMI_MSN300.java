package m2m.smartcon3.provider;

import java.io.Serializable;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.LC100;
import m2m.smartcon3.device.MSN300;
import m2m.smartcon3.device.OldMSN300;

public class RMI_MSN300 implements I_RMI_Device, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 10L;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.MSN300;
	private int firmwaraeVersion;
	private int sensingLevel;
	I_RMI_Device parent = null;
	private int illumination;
	private int temperature;

	NETWORK_STATE connectionState = NETWORK_STATE.OFFLINE;

	public RMI_MSN300(MSN300 device, I_RMI_Device parent) {
		this.deviceId = device.getId();
		this.parent = parent;
		if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		this.sensingLevel = device.getSensingLevel();
		this.firmwaraeVersion = device.getFirmwareVersion();
		this.illumination = device.getIllumination();
		this.temperature = device.getTemperature();
	}

	public RMI_MSN300(OldMSN300 device, I_RMI_Device parent) {
		this.deviceId = device.getId();
		this.parent = parent;
		if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		this.sensingLevel = device.getSensingLevel();
	}

	@Override
	public short getId() {
		return this.deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		return connectionState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}

	public int getSensingLevel() {
		return sensingLevel;
	}

	public int getFirmwareVersion() {
		return this.firmwaraeVersion;
	}

	@Override
	public void update(I_Device dev) {
		if (dev instanceof MSN300) {
			MSN300 device = (MSN300) dev;
			this.deviceId = device.getId();
			if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;
			this.sensingLevel = device.getSensingLevel();
			this.firmwaraeVersion = device.getFirmwareVersion();
			this.illumination = device.getIllumination();
			this.temperature = device.getTemperature();
		} else if (dev instanceof OldMSN300) {
			OldMSN300 device = (OldMSN300) dev;
			this.deviceId = device.getId();
			if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;
			this.sensingLevel = device.getSensingLevel();
		}
	}

	public I_RMI_Device getParent() {
		return this.parent;
	}

	public int getIllumination() {
		return illumination;
	}

	public int getTemperature() {
		return temperature;
	}
	
	
}
