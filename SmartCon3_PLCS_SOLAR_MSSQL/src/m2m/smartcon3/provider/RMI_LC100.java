package m2m.smartcon3.provider;

import java.io.Serializable;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import m2m.smartcon3.device.DimmerMappingInfo;
import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.LC100;
import m2m.smartcon3.device.MDF100;
import m2m.smartcon3.device.MDT100;
import m2m.smartcon3.device.MIR100;
import m2m.smartcon3.device.MPR200;
import m2m.smartcon3.device.MSN300;
import m2m.smartcon3.device.OldLC100;
import m2m.smartcon3.device.OldMDT100;
import m2m.smartcon3.device.OldMSN300;
import m2m.smartcon3.device.ScheduleInfo;
import m2m.smartcon3.device.SensorMappingInfo;
import m2m.smartcon3.device.SolarSensorGetter;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;

public class RMI_LC100 implements I_RMI_Device, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6L;
	RMI_MGW300 parent = null;
	short deviceId = 0x00;
	short panId = 0xFF;

	E_DeviceType deviceType = E_DeviceType.LC100;
	int firmwareVersion = 0;
	HashMap<Short, I_RMI_Device> subDeviceMap = new HashMap<Short, I_RMI_Device>();
	List<SensorMappingInfo> sensorMappingList = new LinkedList<SensorMappingInfo>();
	List<DimmerMappingInfo> dimmerMappingList = new LinkedList<DimmerMappingInfo>();
	List<ScheduleInfo> defaultScheduleList = new LinkedList<ScheduleInfo>();
	List<ScheduleInfo> todayScheduleList = new LinkedList<ScheduleInfo>();
	List<ScheduleInfo> tomorrowScheduleList = new LinkedList<ScheduleInfo>();

	E_GCP_ControlMode controlInfo[] = new E_GCP_ControlMode[16];
	E_GCP_ControlMode stateInfo[] = new E_GCP_ControlMode[16];

	NETWORK_STATE connectionState = NETWORK_STATE.OFFLINE;

	private int powermeterValue = 0;

	private int rebootCount = 0;
	private int connectionCount = 0;
	private Calendar lastRebootTime = Calendar.getInstance();
	private Calendar lastConnectionTime = Calendar.getInstance();

	public RMI_LC100(LC100 lc100, I_RMI_Device parent) {
		this.deviceId = lc100.getId();
		if (lc100.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		this.controlInfo = lc100.getControlInfo();
		this.stateInfo = lc100.getStateInfo();

		for (int i = 0; i < lc100.getDimmerMappingTableSize(); i++)
			this.dimmerMappingList.add(lc100.getDimmerMappingInfo(i));
		for (int i = 0; i < lc100.getSensorMappingTableSize(); i++)
			this.sensorMappingList.add(lc100.getSensorMappingInfo(i));
		for (int i = 0; i < lc100.getDefaultScheduleSize(); i++)
			this.defaultScheduleList.add(lc100.getDefaultSchedule(i));
		for (int i = 0; i < lc100.getTodayScheduleSize(); i++)
			this.todayScheduleList.add(lc100.getTodaySchedule(i));
		for (int i = 0; i < lc100.getTomorrowScheduleSize(); i++)
			this.tomorrowScheduleList.add(lc100.getTomorrowSchedule(i));

		for (int i = 0; i < lc100.getNumOfSubDevices(); i++) {
			I_Device dev = lc100.getDeviceByIndex(i);
			if (dev instanceof MSN300) {
				subDeviceMap.put(dev.getId(),
						new RMI_MSN300((MSN300) dev, this));
			} else if (dev instanceof MDT100) {
				subDeviceMap.put(dev.getId(),
						new RMI_MDT100((MDT100) dev, this));
			} else if (dev instanceof MDF100) {
				subDeviceMap.put(dev.getId(),
						new RMI_MDF100((MDF100) dev, this));
			} else if (dev instanceof MPR200) { 
				subDeviceMap.put(dev.getId(), 
						new RMI_MPR200((MPR200)dev, this));
			} else if (dev instanceof MIR100) { 
				subDeviceMap.put(dev.getId(), new RMI_MIR100((MIR100)dev, this));
			}
			else if (dev instanceof SolarSensorGetter) {
				subDeviceMap.put(dev.getId(), new RMI_SolarSensorGetter(
						(SolarSensorGetter) dev, this));
			}
		}
		this.firmwareVersion = lc100.getFirmwareVersion();
		this.powermeterValue = lc100.getPowermeterValue();
		this.rebootCount = lc100.getRebootCount();
		this.connectionCount = lc100.getConnectionCount();
		this.lastRebootTime = lc100.getLastRebootTime();
		this.lastConnectionTime = lc100.getLastConnectionTime();
		this.parent = (RMI_MGW300) parent;
		this.panId = lc100.getPanId();
	}

	public RMI_LC100(OldLC100 lc100, I_RMI_Device parent) {
		this.deviceId = lc100.getId();
		if (lc100.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		this.controlInfo = lc100.getControlInfo();
		this.stateInfo = lc100.getStateInfo();
		for (int i = 0; i < lc100.getDimmerMappingTableSize(); i++)
			this.dimmerMappingList.add(lc100.getDimmerMappingInfo(i));
		for (int i = 0; i < lc100.getSensorMappingTableSize(); i++)
			this.sensorMappingList.add(lc100.getSensorMappingInfo(i));

		// System.out.println("Id: "+HexUtils.toHexString(deviceId));
		// System.out.println("NumOfSubDevice: "+lc100.getNumOfSubDevices());

		for (int i = 0; i < lc100.getNumOfSubDevices(); i++) {
			I_Device dev = lc100.getDeviceByIndex(i);
			if (dev instanceof OldMSN300) {
				subDeviceMap.put(dev.getId(), new RMI_MSN300((OldMSN300) dev,
						this));
			} else if (dev instanceof OldMDT100) {
				subDeviceMap.put(dev.getId(), new RMI_MDT100((OldMDT100) dev,
						this));
			}
		}
		this.powermeterValue = lc100.getPowermeterValue();
		this.rebootCount = lc100.getRebootCount();
		this.connectionCount = lc100.getServerConnectionCount();
		this.lastRebootTime = lc100.getLastRebootTime();
		this.lastConnectionTime = lc100.getLastServerConnectionTime();
		this.parent = (RMI_MGW300) parent;
		this.panId = lc100.getPanId();
	}

	public RMI_LC100(short deviceId) {
		this.deviceId = deviceId;
		for (int i = 0; i < 16; i++)
			controlInfo[i] = stateInfo[i] = E_GCP_ControlMode.DEFAULT;
	}

	@Override
	public short getId() {
		// TODO Auto-generated method stub
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		return connectionState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		// TODO Auto-generated method stub
		return deviceType;
	}

	public SensorMappingInfo getSensorMappingInfoByIndex(int index) {
		if (this.sensorMappingList.size() <= index)
			return null;
		return this.sensorMappingList.get(index);
	}

	public DimmerMappingInfo getDimmerMappingInfoByIndex(int index) {
		if (this.dimmerMappingList.size() <= index)
			return null;
		return this.dimmerMappingList.get(index);
	}

	public I_RMI_Device getDeviceById(short deviceId) {
		return subDeviceMap.get(deviceId);
	}

	public int getNumOfSubDevices() {
		return subDeviceMap.size();
	}

	public I_RMI_Device getDeviceByIndex(int index) {
		if (index >= getNumOfSubDevices())
			return null;
		int cnt = 0;
		for (I_RMI_Device device : subDeviceMap.values()) {
			if (index == cnt)
				return device;
			cnt++;
		}

		return null;
	}

	public E_GCP_ControlMode[] getStateInfo() {
		return stateInfo;
	}

	public E_GCP_ControlMode[] getControlInfo() {
		return controlInfo;
	}

	public int getSensorMappingTableSize() {
		return this.sensorMappingList.size();
	}

	public int getDimmerMappingTableSize() {
		return this.dimmerMappingList.size();
	}

	public ScheduleInfo getTomorrowSchedule(int index) {
		return tomorrowScheduleList.get(index);
	}

	public int getTomorrowScheduleSize() {
		return this.tomorrowScheduleList.size();
	}

	public ScheduleInfo getTodaySchedule(int index) {
		return todayScheduleList.get(index);
	}

	public int getTodayScheduleSize() {
		return this.todayScheduleList.size();
	}

	public ScheduleInfo getDefaultSchedule(int index) {
		return defaultScheduleList.get(index);
	}

	public int getDefaultScheduleSize() {
		return this.defaultScheduleList.size();
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public int getPowermeterValue() {
		return powermeterValue;
	}

	public int getRebootCount() {
		return rebootCount;
	}

	public int getConnectionCount() {
		return connectionCount;
	}

	public Calendar getLastRebootTime() {
		return lastRebootTime;
	}

	public Calendar getLastConnectionTime() {
		return lastConnectionTime;
	}

	public void setParent(RMI_MGW300 rmi_MGW300) {
		this.parent = rmi_MGW300;

	}

	@Override
	public void update(I_Device device) {
		if (device instanceof LC100) {
			LC100 lc100 = (LC100) device;
			this.deviceId = lc100.getId();
			if (lc100.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;
			this.controlInfo = lc100.getControlInfo();
			this.stateInfo = lc100.getStateInfo();

			dimmerMappingList.clear();
			sensorMappingList.clear();
			defaultScheduleList.clear();
			todayScheduleList.clear();
			tomorrowScheduleList.clear();
			for (int i = 0; i < lc100.getDimmerMappingTableSize(); i++)
				this.dimmerMappingList.add(lc100.getDimmerMappingInfo(i));
			for (int i = 0; i < lc100.getSensorMappingTableSize(); i++)
				this.sensorMappingList.add(lc100.getSensorMappingInfo(i));
			for (int i = 0; i < lc100.getDefaultScheduleSize(); i++)
				this.defaultScheduleList.add(lc100.getDefaultSchedule(i));
			for (int i = 0; i < lc100.getTodayScheduleSize(); i++)
				this.todayScheduleList.add(lc100.getTodaySchedule(i));
			for (int i = 0; i < lc100.getTomorrowScheduleSize(); i++)
				this.tomorrowScheduleList.add(lc100.getTomorrowSchedule(i));

			this.powermeterValue = lc100.getPowermeterValue();
			this.rebootCount = lc100.getRebootCount();
			this.connectionCount = lc100.getConnectionCount();
			this.lastRebootTime = lc100.getLastRebootTime();
			this.lastConnectionTime = lc100.getLastConnectionTime();
			this.firmwareVersion = lc100.getFirmwareVersion();
			this.panId = lc100.getPanId();

			I_Device dev;
			I_RMI_Device rmiDevice;
			for (int i = 0; i < lc100.getNumOfSubDevices(); i++) {
				dev = lc100.getDeviceByIndex(i);
				rmiDevice = this.getDeviceById(dev.getId());
				if (rmiDevice == null) {
					if (dev instanceof MSN300) {
						subDeviceMap.put(dev.getId(), new RMI_MSN300(
								(MSN300) dev, this));
					} else if (dev instanceof MDT100) {
						subDeviceMap.put(dev.getId(), new RMI_MDT100(
								(MDT100) dev, this));
					} else if (dev instanceof MDF100) {
						subDeviceMap.put(dev.getId(), new RMI_MDF100(
								(MDF100) dev, this));
					} else if (dev instanceof MPR200) { 
						subDeviceMap.put(dev.getId(), new RMI_MPR200(
								(MPR200) dev, this));
					} else if (dev instanceof MIR100) { 
						subDeviceMap.put(dev.getId(), new RMI_MIR100( (MIR100) dev, this));
					}
					else if (dev instanceof SolarSensorGetter) {
						subDeviceMap.put(dev.getId(),
								new RMI_SolarSensorGetter(
										(SolarSensorGetter) dev, this));
					}
				} else
					rmiDevice.update(dev);
			}
		} else if (device instanceof OldLC100) {
			OldLC100 lc100 = (OldLC100) device;
			this.deviceId = lc100.getId();
			if (lc100.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;
			this.controlInfo = lc100.getControlInfo();
			this.stateInfo = lc100.getStateInfo();
			for (int i = 0; i < lc100.getDimmerMappingTableSize(); i++)
				if (this.dimmerMappingList.contains(lc100
						.getDimmerMappingInfo(i)) == false)
					this.dimmerMappingList.add(lc100.getDimmerMappingInfo(i));
			for (int i = 0; i < lc100.getSensorMappingTableSize(); i++)
				if (this.sensorMappingList.contains(lc100
						.getSensorMappingInfo(i)) == false)
					this.sensorMappingList.add(lc100.getSensorMappingInfo(i));
			this.powermeterValue = lc100.getPowermeterValue();
			this.rebootCount = lc100.getRebootCount();
			this.connectionCount = lc100.getServerConnectionCount();
			this.lastRebootTime = lc100.getLastRebootTime();
			this.lastConnectionTime = lc100.getLastServerConnectionTime();
			this.firmwareVersion = lc100.getFirmwareVersion();
			this.panId = lc100.getPanId();

			I_Device dev;
			I_RMI_Device rmiDevice;
			for (int i = 0; i < lc100.getNumOfSubDevices(); i++) {
				dev = lc100.getDeviceByIndex(i);
				rmiDevice = this.getDeviceById(dev.getId());
				if (rmiDevice == null) {
					if (dev instanceof OldMSN300) {
						subDeviceMap.put(dev.getId(), new RMI_MSN300(
								(OldMSN300) dev, this));
					} else if (dev instanceof OldMDT100) {
						subDeviceMap.put(dev.getId(), new RMI_MDT100(
								(OldMDT100) dev, this));
					}
				} else
					rmiDevice.update(dev);
			}
		}

	}

	public RMI_MGW300 getParent() {
		return this.parent;
	}

	public short getPanId() {
		return panId;
	}

	public void setPanId(short panId) {
		this.panId = panId;
	}
}
