package m2m.smartcon3.provider;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class ClientSocketHandler implements Runnable {
	Socket clientSocket = null;
	BufferedInputStream bis = null;
	BufferedOutputStream bos = null;

	public ClientSocketHandler(Socket socket) {
		this.clientSocket = socket;

		new Thread(this).start();
	}

	@Override
	public void run() {

		try {
			bis = new BufferedInputStream(clientSocket.getInputStream());
			bos = new BufferedOutputStream(clientSocket.getOutputStream());
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		while (true) {
			byte[] tempBuf = new byte[1024];
			try {
				int tempLen = bis.read(tempBuf);
				if (tempLen > 0)
					handleMessage(tempBuf, tempLen);
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				break;
			}
			try {
				Thread.sleep(10);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void handleMessage(byte[] buff, int size) {
		if(new String(buff).substring(0, size).compareTo("DeviceManager")==0){
			try {
				int port = 30013+(new Random()).nextInt(1000);
				ServerSocket socket = new ServerSocket(port);
				
				bos.write((""+port).getBytes());
				bos.flush();
				
				Socket clientSocket = socket.accept();
				RMI_DeviceManager deviceManager = RMIProviderForWeb.getInstance().getDeviceManager();
				
				ObjectOutputStream oos = new ObjectOutputStream(clientSocket.getOutputStream());
				oos.writeObject(deviceManager);
				oos.flush();
				clientSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
