package m2m.smartcon3.provider;

import java.io.Serializable;
import java.util.Calendar;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.MDF100;
import m2m.smartcon3.device.MDT100;
import m2m.smartcon3.device.OldMDT100;
import m2m.smartcon3.device.SolarSensorGetter;

public class RMI_SolarSensorGetter implements I_RMI_Device, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 12L;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.SolarSensorGetter;
	private int firmwareVersion;
	int numOfSensor=0;
	int sensorValue[] = new int[16];
	
	Calendar lastSensorValueReceivedTime= Calendar.getInstance();

	I_RMI_Device parent = null;
	NETWORK_STATE connectionState = NETWORK_STATE.OFFLINE;

	public RMI_SolarSensorGetter(SolarSensorGetter device, I_RMI_Device parent) {
		this.parent = parent;
		this.deviceId = device.getId();
		this.firmwareVersion = device.getFirmwareVersion();
		for(int i=0; i < 16; i++)
			this.sensorValue = device.getSensorValue();
		this.numOfSensor = device.getNumOfSensor();
		this.lastSensorValueReceivedTime = device.getLastSensorValueReceivedTime();

		if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		else
			connectionState = NETWORK_STATE.OFFLINE;
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		return connectionState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}

	public short getDeviceId() {
		return deviceId;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public I_RMI_Device getParent() {
		return parent;
	}
	
	

	public int getNumOfSensor() {
		return numOfSensor;
	}

	public int[] getSensorValue() {
		return sensorValue;
	}

	@Override
	public void update(I_Device dev) {
		if (dev instanceof SolarSensorGetter) {
			SolarSensorGetter device = (SolarSensorGetter) dev;
			this.deviceId = device.getId();
			this.firmwareVersion = device.getFirmwareVersion();
			for(int i=0; i < 16; i++)
				this.sensorValue = device.getSensorValue();
			this.numOfSensor = device.getNumOfSensor();
			
			this.lastSensorValueReceivedTime = device.getLastSensorValueReceivedTime();

			if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;
		}
	}

	public Calendar getLastSensorValueReceivedTime() {
		return lastSensorValueReceivedTime;
	}
	
	
}
