package m2m.smartcon3.provider;

import java.io.Serializable;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.MDT100;
import m2m.smartcon3.device.OldMDT100;
import m2m.smartcon3.module.RemoteFirmwareUpdateHelper;
import m2m.util.MyLog;

public class RMI_MDT100 implements I_RMI_Device, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8L;
	short deviceId;
	E_DeviceType deviceType = E_DeviceType.MDT100;
	private int firmwareVersion;
	I_RMI_Device parent = null;
	NETWORK_STATE connectionState = NETWORK_STATE.OFFLINE;
	

	public RMI_MDT100(MDT100 device, I_RMI_Device parent) {
		this.parent = parent;
		this.deviceId = device.getId();
		this.firmwareVersion = device.getFirmwareVersion();
		if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		else
			connectionState = NETWORK_STATE.OFFLINE;
	}

	public RMI_MDT100(OldMDT100 device, I_RMI_Device parent) {
		this.parent = parent;
		this.deviceId = device.getId();
		this.firmwareVersion = device.getFrimwareVersion();
		if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
			connectionState = NETWORK_STATE.ONLINE;
		else
			connectionState = NETWORK_STATE.OFFLINE;
	}

	@Override
	public short getId() {
		return deviceId;
	}

	@Override
	public NETWORK_STATE getConnectionState() {
		return connectionState;
	}

	@Override
	public E_DeviceType getDeviceType() {
		return this.deviceType;
	}
	
	

	public short getDeviceId() {
		return deviceId;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public I_RMI_Device getParent() {
		return parent;
	}

	@Override
	public void update(I_Device dev) {
		if (dev instanceof MDT100) {
			MDT100 device = (MDT100) dev;
			this.deviceId = device.getId();
			this.firmwareVersion = device.getFirmwareVersion();
			if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;
		} else if (dev instanceof OldMDT100) {
			OldMDT100 device = (OldMDT100) dev;
			this.deviceId = device.getId();
			this.firmwareVersion = device.getFrimwareVersion();
			if (device.getConnectionState() == I_Device.NETWORK_STATE.ONLINE)
				connectionState = NETWORK_STATE.ONLINE;
			else
				connectionState = NETWORK_STATE.OFFLINE;
		}

	}
	
}
