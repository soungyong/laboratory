/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
package m2m.smartcon3.provider;

import java.util.Date;

/**
 * ISmartEvent 인터페이스는 모든 이벤트가 구현해야되는 인터페이스이다.
 * 
 * 05/26 날짜/시간 추가.
 */
public interface I_RMI_Event
{
	public Date getDate();
	public String getName();	
}
