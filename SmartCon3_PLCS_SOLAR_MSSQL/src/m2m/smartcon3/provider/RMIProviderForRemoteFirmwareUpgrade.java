package m2m.smartcon3.provider;

import java.io.FileInputStream;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import m2m.smartcon3.core.DeviceManager;
import m2m.smartcon3.core.EventManager;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.LC100;
import m2m.smartcon3.device.MDF100;
import m2m.smartcon3.device.MDT100;
import m2m.smartcon3.device.MGW300;
import m2m.smartcon3.device.MPR200;
import m2m.smartcon3.device.MSN300;
import m2m.smartcon3.device.OldLC100;
import m2m.smartcon3.device.OldMGW300;
import m2m.smartcon3.device.OldMSN300;
import m2m.smartcon3.event.DeviceDisconnectEvent;
import m2m.smartcon3.event.DeviceStateChangedEvent;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.event.I_Event;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.event.SensorEvent;
import m2m.smartcon3.event.SynchronizedMessageEventHandler;
import m2m.smartcon3.exception.NullResultException;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.module.SensorEventLog;
import m2m.smartcon3.module.SensorEventLogger;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ScheduleType;
import m2m.util.MyLog;

public class RMIProviderForRemoteFirmwareUpgrade implements I_RMIProviderForRemoteFirmwareUpgrade {

	/**
	 * 
	 */
	private static final long serialVersionUID = 15L;
	private static RMIProviderForRemoteFirmwareUpgrade _instance = null;

	public static RMIProviderForRemoteFirmwareUpgrade getInstance() throws RemoteException {
		if (_instance == null) {
			_instance = new RMIProviderForRemoteFirmwareUpgrade();
		}
		return _instance;
	}

	public RMIProviderForRemoteFirmwareUpgrade() throws RemoteException {
		super();
		try {
			Properties props = new Properties();
			FileInputStream fis = new FileInputStream(
					"./smartcon3_plcs.properties");
			props.load(fis);
			System.setProperty("java.rmi.server.hostname",
					props.getProperty("publicIP"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		I_RMIProviderForRemoteFirmwareUpgrade stub = (I_RMIProviderForRemoteFirmwareUpgrade) UnicastRemoteObject
				.exportObject(this, 1099);
		Registry registry = LocateRegistry.getRegistry(1099);
		registry.rebind("smartcon3_plcs_rfu", stub);
	}

	@Override
	public boolean test(String str) throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public RMI_DeviceManager getDeviceManager() throws RemoteException {
		DeviceManager deviceManager = DeviceManager.getInstance();
		RMI_DeviceManager rmiDeviceManager = new RMI_DeviceManager();
		for (I_Device device : deviceManager.getGatewayList()) {
			if (device instanceof MGW300) {
				MGW300 gateway = (MGW300) device;
				RMI_MGW300 rmiGateway = (RMI_MGW300) rmiDeviceManager
						.getGatewayById(gateway.getId());
				if (rmiGateway == null) {
					rmiDeviceManager.addGateway(new RMI_MGW300(gateway));
				} else {
					rmiGateway.update(gateway);
				}
			} else if (device instanceof OldMGW300) {
				OldMGW300 gateway = (OldMGW300) device;
				RMI_MGW300 rmiGateway = (RMI_MGW300) rmiDeviceManager
						.getGatewayById(gateway.getId());
				if (rmiGateway == null) {
					rmiDeviceManager.addGateway(new RMI_MGW300(gateway));
				} else {
					rmiGateway.update(gateway);
				}
			}
		}
		return rmiDeviceManager;
	}

	
	
	@Override
	public int upgradeFirmwareReq(RMI_MGW300 rmiMgw, byte[] data)
			throws RemoteException {
		// TODO Auto-generated method stub
		I_Device device = DeviceManager.getInstance().getGatewayById(
				rmiMgw.getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			
			return gateway.sendFirmwareUpgradeReq(data);
		} 
		return -1;
	}
	
	@Override
	public int upgradeFirmwareReq(RMI_LC100 rmiLc, byte[] data)
			throws RemoteException {
		// TODO Auto-generated method stub
		I_Device device = DeviceManager.getInstance().getGatewayById(
				(rmiLc.getParent()).getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = (LC100) gateway.getLC(rmiLc.getId());
			return lc.sendFirmwareUpgradeReq(data);
		} 
		return -1;
	}
	
	@Override
	public int upgradeFirmwareReq(RMI_MSN300 rmiMsn, byte[] data)
			throws RemoteException {
		// TODO Auto-generated method stub
		I_Device device = DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) rmiMsn.getParent()).getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = (LC100) gateway.getLC(rmiMsn.getParent().getId());
			MSN300 msn = (MSN300) lc.getDeviceById(rmiMsn.getId());
			return msn.sendFirmwareUpgradeReq(data);
		} 
		return -1;
	}
	
	@Override
	public int upgradeFirmwareReq(RMI_MDT100 rmiMdt, byte[] data)
			throws RemoteException {
		// TODO Auto-generated method stub
		I_Device device = DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) rmiMdt.getParent()).getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = (LC100) gateway.getLC(rmiMdt.getParent().getId());
			MDT100 mdt = (MDT100) lc.getDeviceById(rmiMdt.getId());
			return mdt.sendFirmwareUpgradeReq(data);
		} 
		return -1;
	}
	
	@Override
	public int upgradeFirmwareReq(RMI_MDF100 rmiMdf, byte[] data)
			throws RemoteException {
		// TODO Auto-generated method stub
		I_Device device = DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) rmiMdf.getParent()).getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = (LC100) gateway.getLC(rmiMdf.getParent().getId());
			MDF100 mdf = (MDF100) lc.getDeviceById(rmiMdf.getId());
			return mdf.sendFirmwareUpgradeReq(data);
		} 
		return -1;
	}
	
	@Override
	public int upgradeFirmwareReq(RMI_MPR200 rmiMpr, byte[] data)
			throws RemoteException {
		// TODO Auto-generated method stub
		I_Device device = DeviceManager.getInstance().getGatewayById(
				((RMI_LC100) rmiMpr.getParent()).getParent().getId());
		if (device instanceof MGW300) {
			MGW300 gateway = (MGW300) device;
			LC100 lc = (LC100) gateway.getLC(rmiMpr.getParent().getId());
			MPR200 mpr = (MPR200) lc.getDeviceById(rmiMpr.getId());
			return mpr.sendFirmwareUpgradeReq(data);
		} 
		return -1;
	}

}
