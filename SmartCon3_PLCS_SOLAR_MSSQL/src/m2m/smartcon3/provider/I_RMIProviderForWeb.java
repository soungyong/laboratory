package m2m.smartcon3.provider;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.module.SensorEventLog;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ScheduleType;

public interface I_RMIProviderForWeb extends Remote {
	boolean test(String str) throws RemoteException;

	I_RMI_Event getEvent(int eventLoggerId) throws RemoteException;

	List<I_RMI_Event> getEventAll(int eventLoggerId) throws RemoteException;

	int getNewEventLoggerId() throws RemoteException;

	int getNumOfEvent(int eventLoggerId) throws RemoteException;

	long getSensorEventCount(RMI_MSN300 msn, long dateTimeInMinute)
			throws RemoteException;

	List<Long> getSensorEventCount(RMI_MSN300 msn, long fromDateTimeInMinute,
			long toDateTimeInMinute) throws RemoteException;

	List<Integer> getSensorLevel(RMI_MSN300 msn, long fromDateTimeInMinute,
			long toDateTimeInMinute) throws RemoteException;

	RMI_DeviceManager getDeviceManager() throws RemoteException;

	boolean removeDevice(I_RMI_Device device) throws RemoteException;

	// asynchronized MGW
	public boolean sendDeviceInfoReq(RMI_MGW300 gateway) throws RemoteException;

	public boolean sendStateInfoReq(RMI_MGW300 gateway) throws RemoteException;

	public int sendControlSensorSharing(RMI_MGW300 gateway, boolean enable)
			throws RemoteException;

	public int sendControlGateway(RMI_MGW300 gateway, String deviceIp,
			String serverIp, String defaultGatewayIp, String netmask, int port,
			boolean isSensorSharingMode) throws RemoteException,
			UnformattedPacketException;

	public int sendDebugLogReq(RMI_MGW300 gateway) throws RemoteException,
			UnformattedPacketException;

	public boolean sendResetDebugLogReq(RMI_MGW300 gateway)
			throws RemoteException;

	public boolean sendRebootReq(RMI_MGW300 gateway) throws RemoteException;

	public boolean sendTimeInfo(RMI_MGW300 gateway) throws RemoteException;

	public int sendControlNoticeModeReq(RMI_MGW300 gateway, boolean enable)
			throws RemoteException, UnformattedPacketException;

	// synchronized mgw
	public boolean sendControlSensorSharing_Synchronized(RMI_MGW300 gateway,
			boolean enable) throws RemoteException;;

	public boolean sendControlGateway_Synchronized(RMI_MGW300 gateway,
			String deviceIp, String serverIp, String defaultGatewayIp,
			String netmask, int port, boolean isSensorSharingMode)
			throws RemoteException, UnformattedPacketException;

	public boolean sendDebugLogReq_Synchronized(RMI_MGW300 gateway)
			throws RemoteException, UnformattedPacketException;

	public boolean sendControlNoticeModeReq_Synchronized(RMI_MGW300 gateway, boolean enable)
			throws RemoteException, UnformattedPacketException;

	// asynchronized LC
	public int sendPowermeterReq(RMI_LC100 lc) throws RemoteException;

	public int sendDebugLogLCReq(RMI_LC100 lc) throws RemoteException,
			UnformattedPacketException;

	public boolean sendResetDebugLCLogReq(RMI_LC100 lc) throws RemoteException;

	public int sendMappingSensorReq(RMI_LC100 lc, short lcId, short sensorId,
			int circuitId, int onTime, E_GCP_ControlMode onCtrlMode,
			E_GCP_ControlMode offCtrlMode) throws RemoteException;

	public int sendMappingDimmerReq(RMI_LC100 lc, short dimmerId,
			int channelId, int circuitId) throws RemoteException;

	public int sendAddScheduleReq(RMI_LC100 lc,
			E_GCP_ScheduleType scheduleType, int circuitId, Date onTime,
			E_GCP_ControlMode ctrlMode) throws RemoteException;

	public int sendMappingSensorListReq(RMI_LC100 lc, short lcId,
			short sensorId[], int circuitId, int onTime,
			E_GCP_ControlMode onCtrlMode, E_GCP_ControlMode offCtrlMode)
			throws RemoteException, UnformattedPacketException;

	public int sendMappingDimmerListReq(RMI_LC100 lc, short dimmerId[],
			int channelIds[], int circuitId) throws RemoteException,
			UnformattedPacketException;

	public int sendAddScheduleListReq(RMI_LC100 lc,
			E_GCP_ScheduleType scheduleType, int circuitId, Date onTime[],
			E_GCP_ControlMode ctrlMode[]) throws RemoteException;

	public boolean sendUpdatePowermeter(RMI_LC100 lc, int powermeterValue)
			throws RemoteException;

	public boolean sendSensorMappingInfoReq(RMI_LC100 lc)
			throws RemoteException;

	public boolean sendDimmerMappingInfoReq(RMI_LC100 lc)
			throws RemoteException;

	public boolean sendScheduleInfoReq(RMI_LC100 lc,
			E_GCP_ScheduleType scheduleType) throws RemoteException;

	public int sendResetSensorMappingReq(RMI_LC100 lc, byte circuitId)
			throws RemoteException;

	public int sendResetDimmerMappingReq(RMI_LC100 lc, byte circuitId)
			throws RemoteException;

	public int sendResetScheduleReq(RMI_LC100 lc,
			E_GCP_ScheduleType scheduleType, byte circuitId)
			throws RemoteException;

	public int sendControlCircuit(RMI_LC100 lc, int circuitId,
			E_GCP_ControlMode ctrlMode) throws RemoteException;

	public int sendControlSensor(RMI_LC100 lc, int circuitId, int onTime,
			E_GCP_ControlMode ctrlModeOn, E_GCP_ControlMode ctrlModeOff)
			throws RemoteException;

	public boolean sendStateInfoReq(RMI_LC100 lc) throws RemoteException;

	public boolean sendDeviceInfoReq(RMI_LC100 lc) throws RemoteException;

	public boolean sendRebootReq(RMI_LC100 lc) throws RemoteException;;

	// synchronized lc
	public boolean sendPowermeterReq_Synchronized(RMI_LC100 lc)
			throws RemoteException;

	public boolean sendDebugLogLCReq_Synchronized(RMI_LC100 lc)
			throws RemoteException, UnformattedPacketException;

	public boolean sendMappingSensorReq_Synchronized(RMI_LC100 lc, short lcId,
			short sensorId, int circuitId, int onTime,
			E_GCP_ControlMode onCtrlMode, E_GCP_ControlMode offCtrlMode)
			throws RemoteException;

	public boolean sendMappingDimmerReq_Synchronized(RMI_LC100 lc,
			short dimmerId, int channelId, int circuitId)
			throws RemoteException;

	public boolean sendAddScheduleReq_Synchronized(RMI_LC100 lc,
			E_GCP_ScheduleType scheduleType, int circuitId, Date onTime,
			E_GCP_ControlMode ctrlMode) throws RemoteException;

	public boolean sendMappingSensorListReq_Synchronized(RMI_LC100 lc,
			short lcId, short sensorId[], int circuitId, int onTime,
			E_GCP_ControlMode onCtrlMode, E_GCP_ControlMode offCtrlMode)
			throws RemoteException, UnformattedPacketException;

	public boolean sendMappingDimmerListReq_Synchronized(RMI_LC100 lc,
			short dimmerId[], int channelIds[], int circuitId)
			throws RemoteException, UnformattedPacketException;

	public boolean sendAddScheduleListReq_Synchronized(RMI_LC100 lc,
			E_GCP_ScheduleType scheduleType, int circuitId, Date onTime[],
			E_GCP_ControlMode ctrlMode[]) throws RemoteException;

	public boolean sendResetSensorMappingReq_Synchronized(RMI_LC100 lc,
			byte circuitId) throws RemoteException;

	public boolean sendResetDimmerMappingReq_Synchronized(RMI_LC100 lc,
			byte circuitId) throws RemoteException;

	public boolean sendResetScheduleReq_Synchronized(RMI_LC100 lc,
			E_GCP_ScheduleType scheduleType, byte circuitId)
			throws RemoteException;

	public boolean sendControlCircuit_Synchronized(RMI_LC100 lc, int circuitId,
			E_GCP_ControlMode ctrlMode) throws RemoteException;

	public boolean sendControlSensor_Synchronized(RMI_LC100 lc, int circuitId,
			int onTime, E_GCP_ControlMode ctrlModeOn,
			E_GCP_ControlMode ctrlModeOff) throws RemoteException;

	// asynchronized MSN
	public int sendSetSensingLevelReq(RMI_MSN300 msn, int level)
			throws RemoteException;

	public boolean sendStateInfoReq(RMI_MSN300 msn) throws RemoteException;

	public boolean sendRebootReq(RMI_MSN300 msn) throws RemoteException;;

	// synchronized MSN
	public boolean sendSetSensingLevelReq_Synchronized(RMI_MSN300 msn, int level)
			throws RemoteException;

	// MDT
	public boolean sendStateInfoReq(RMI_MDT100 msn) throws RemoteException;

	public boolean sendRebootReq(RMI_MDT100 mdt) throws RemoteException;

	// MDF
	public int sendPowermeterReq(RMI_MDF100 mdf, int channel)
			throws RemoteException;

	public boolean sendPowermeterReq_Synchronized(RMI_MDF100 mdf, int channel)
			throws RemoteException;

	public boolean sendUpdatePowermeter(RMI_MDF100 mdf, int channel,
			int powermeterValue) throws RemoteException;

	public boolean sendStateInfoReq(RMI_MDF100 mdf) throws RemoteException;

	public boolean sendRebootReq(RMI_MDF100 mdf) throws RemoteException;

}
