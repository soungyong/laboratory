package m2m.smartcon3.provider;

import java.io.Serializable;
import java.util.HashMap;

import m2m.smartcon3.device.MGW300;

public class RMI_DeviceManager implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	HashMap<Short, I_RMI_Device> deviceMap;
	
	public RMI_DeviceManager() {
		deviceMap = new HashMap<Short, I_RMI_Device>();
	}

	public void addGateway(RMI_MGW300 device) {
		deviceMap.put(device.getId(), device);
	}
	
	public I_RMI_Device getGatewayById(short deviceId) {
		return deviceMap.get(deviceId);
	}

	public int getNumOfGateway() {
		return deviceMap.size();
	}

	public I_RMI_Device getGatewayByIndex(int index) {
		int cnt = 0;
		for (I_RMI_Device mgw : deviceMap.values()) {
			if (cnt == index)
				return mgw;
			cnt++;
		}
		return null;
	}	
}
