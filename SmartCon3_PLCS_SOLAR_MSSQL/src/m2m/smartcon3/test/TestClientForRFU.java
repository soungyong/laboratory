package m2m.smartcon3.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import m2m.smartcon3.provider.I_RMIProviderForRemoteFirmwareUpgrade;
import m2m.smartcon3.provider.I_RMIProviderForWeb;
import m2m.smartcon3.provider.I_RMI_Event;
import m2m.smartcon3.provider.RMI_DeviceManager;
import m2m.smartcon3.provider.RMI_LC100;
import m2m.smartcon3.provider.RMI_MGW300;
import m2m.smartcon3.provider.RMI_MSN300;
import m2m.smartcon3.provider.RMI_NoticeUpgradeFirmwareEvent;
import m2m.smartcon3.provider.RMI_NoticeUpgradeFirmwareProgressEvent;
import m2m.util.HexUtils;

public class TestClientForRFU {
	public static void main(String args[]) {
		int previouseRebootCount = 0;
		Calendar preCal = Calendar.getInstance();
		List<Long> diffList = new ArrayList<Long>();

		try {
			// Registry registry = LocateRegistry.getRegistry("121.152.118.252",
			// 1099);
			Registry registry = LocateRegistry.getRegistry("211.230.56.46",
					1099);
			I_RMIProviderForRemoteFirmwareUpgrade rmiServer = (I_RMIProviderForRemoteFirmwareUpgrade) registry
					.lookup("smartcon3_plcs_rfu");

			// while (true)
			{
				RMI_DeviceManager deviceManager = rmiServer.getDeviceManager();
				System.out.println("NumOfGateway: "
						+ deviceManager.getNumOfGateway());
				RMI_MGW300 gateway = (RMI_MGW300) deviceManager
						.getGatewayById((short) 0x0006);
				if (gateway != null) {
					System.out.println(gateway.getConnectionState());
					System.out.println("NumOfLC: "
							+ gateway.getNumOfSubDevices());

					RMI_LC100 lc = (RMI_LC100) gateway.getLC((short) 0x0ff1);
					RMI_MSN300 msn = (RMI_MSN300) lc
							.getDeviceById((short) 0x0202);

					char data[] = new char[1024 * 500];
					FileReader fr = new FileReader(
							"F:\\workspace2\\MSN200\\Debug\\MSN200.hex");
					BufferedReader br = new BufferedReader(fr);
					int len = br.read(data);
					byte data2[] = new byte[len];
					for (int i = 0; i < len; i++)
						data2[i] = (byte) data[i];

					System.out.println("Len: " + len);
					//rmiServer.upgradeFirmwareReq(msn, data2);
				}

				Thread.sleep(2000);
			}

			Registry registry2 = LocateRegistry.getRegistry("121.152.118.252",
					1099);
			I_RMIProviderForWeb rmiServer2 = (I_RMIProviderForWeb) registry
					.lookup("smartcon3_plcs");

			int loggerId = rmiServer2.getNewEventLoggerId();
			System.out.println("LoggerId: " + loggerId);
			while (true) {
				List<I_RMI_Event> eventList = rmiServer2.getEventAll(loggerId);
				if(eventList.size()>0){
					for(I_RMI_Event event : eventList)
						if(event instanceof RMI_NoticeUpgradeFirmwareProgressEvent){
							RMI_NoticeUpgradeFirmwareProgressEvent ne = (RMI_NoticeUpgradeFirmwareProgressEvent)event;
							System.out.println("TotalLen, current("+HexUtils.toHexString(ne.getDeviceId())+"): "+ne.getTotalNumOfPacket()+", "+ne.getCurrentIndex());
						}else if(event instanceof RMI_NoticeUpgradeFirmwareEvent){
							System.out.println(((RMI_NoticeUpgradeFirmwareEvent)event).getResult());
							//return;
						}
				}
			}

		} catch (Exception e) {
			System.err.println("ComputePi exception:");
			e.printStackTrace();
		}
	}
}
