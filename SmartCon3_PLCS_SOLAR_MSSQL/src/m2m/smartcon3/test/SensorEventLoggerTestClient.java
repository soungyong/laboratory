package m2m.smartcon3.test;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import m2m.smartcon3.provider.I_RMIProviderForWeb;
import m2m.smartcon3.provider.RMI_LC100;
import m2m.smartcon3.provider.RMI_MGW300;
import m2m.smartcon3.provider.RMI_MSN300;

public class SensorEventLoggerTestClient {

	public static void main(String args[]) {
		int previouseRebootCount = 0;
		Calendar preCal = Calendar.getInstance();
		List<Long> diffList = new ArrayList<Long>();

		try {
			Registry registry = LocateRegistry.getRegistry("211.230.56.46",
					1099);
			I_RMIProviderForWeb rmiServer = (I_RMIProviderForWeb) registry
					.lookup("smartcon3_plcs");

			long dateTimeInMinute = 0;
			while (true) {
				dateTimeInMinute = System.currentTimeMillis() / 1000 / 60;
				RMI_MGW300 mgw = (RMI_MGW300) rmiServer.getDeviceManager()
						.getGatewayById((short) 1);
				RMI_LC100 lc = (RMI_LC100) mgw.getLC((short) 0x72a3);
				RMI_MSN300 msn = (RMI_MSN300) lc.getDeviceById((short) 0x728b);
				System.out.println(rmiServer.getSensorEventCount(msn,
						dateTimeInMinute));
				Thread.sleep(2000);
			}

		} catch (Exception e) {
			System.err.println("ComputePi exception:");
			e.printStackTrace();
		}
	}
}
