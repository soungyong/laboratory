package m2m.smartcon3.test;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import m2m.smartcon3.provider.RMI_DeviceManager;
import m2m.smartcon3.provider.RMI_LC100;
import m2m.smartcon3.provider.RMI_MGW300;

public class TestClientForSocket {
	public static void main(String args[]) {
		try {
			Socket socket = new Socket("localhost", 30012);
			BufferedOutputStream bos = new BufferedOutputStream(
					socket.getOutputStream());
			BufferedInputStream bis = new BufferedInputStream(
					socket.getInputStream());

			bos.write("DeviceManager".getBytes());
			bos.flush();

			byte[] recvBuf = new byte[1024];
			int len = bis.read(recvBuf);
			if(len > 0){
				System.out.println();
				int port = Integer.parseInt(new String(recvBuf).substring(0, len));
				Socket socket2 = new Socket("localhost", port);
				ObjectInputStream ois = new ObjectInputStream(socket2.getInputStream());
				RMI_DeviceManager deviceManager = (RMI_DeviceManager)ois.readObject();
				
				System.out.println(deviceManager.getNumOfGateway());
				RMI_MGW300 mgw = (RMI_MGW300) deviceManager.getGatewayByIndex(0);
				RMI_LC100 lc = (RMI_LC100) mgw.getLCByIndex(1);
				System.out.println(lc.getNumOfSubDevices());
				System.out.println(lc.getDeviceByIndex(0).getId());
				
				System.out.println();
				
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
