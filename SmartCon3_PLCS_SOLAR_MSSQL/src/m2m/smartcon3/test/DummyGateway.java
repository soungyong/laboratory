package m2m.smartcon3.test;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.LC100;
import m2m.smartcon3.device.MDT100;
import m2m.smartcon3.device.MGW300;
import m2m.smartcon3.device.MSN300;
import m2m.smartcon3.devicenetwork.DefaultXNetHandler;

public class DummyGateway {
	public static void main(String argsp[]) throws UnknownHostException,
			IOException, InterruptedException {
		Socket clientSocket = new Socket("127.0.0.1", 30011);
		DefaultXNetHandler xnetHandler = new DefaultXNetHandler(clientSocket,
				null);
		if(argsp.length!=1)return;
		MGW300 gateway = new MGW300(Short.parseShort(argsp[0]));
		gateway.setXNetHandler(xnetHandler);
		gateway.sendRegisterReq();
		for (int i = 1; i < 2; i++) {
			LC100 lc = new LC100(gateway, (short) i);
			gateway.addLC(lc);
//			for (int j = 1; j < 30; j++) {
//				MSN300 msn = new MSN300(lc, (short) j);
//				lc.addDevice(msn);
//			}
//			for (int j = 1; j < 10; j++) {
//				MDT100 mdt = new MDT100(lc, (short) (j+50));
//				lc.addDevice(mdt);
//			}
			gateway.sendRegisterNodeReq(lc);
//			for (int j = 0; j < lc.getNumOfSubDevices(); j++)
//				lc.sendRegisterNodeReq(lc.getDeviceByIndex(j));
		}

		while (true) {
			if (xnetHandler.isConnected() == false) {
				try {
					clientSocket = new Socket("127.0.0.1", 30011);
					xnetHandler = new DefaultXNetHandler(clientSocket, null);
					gateway.setXNetHandler(xnetHandler);
					gateway.sendRegisterReq();
					for (int i = 1; i < 20; i++) {
						LC100 lc = new LC100(gateway, (short) i);
						gateway.addLC(lc);
						for (int j = 1; j < 30; j++) {
							MSN300 msn = new MSN300(lc, (short) j);
							lc.addDevice(msn);
						}
						for (int j = 1; j < 10; j++) {
							MDT100 mdt = new MDT100(lc, (short) (j+50));
							lc.addDevice(mdt);
						}
						gateway.sendRegisterNodeReq(lc);
						for (int j = 0; j < lc.getNumOfSubDevices(); j++)
							lc.sendRegisterNodeReq(lc.getDeviceByIndex(j));
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			// for(int i=0; i < lc.getNumOfSubDevices(); i++) {
			// I_Device device = lc.getDeviceByIndex(i);
			// if(device instanceof MSN300)
			// ((MSN300)device).sendNoticeEvent(lc.getId(), msn.getId());
			// }
			// gateway.sendRegisterReq();
			// Thread.sleep(1000);
			// gateway.sendPingReq();
			// Thread.sleep(1000);
			// gateway.sendRegisterNodeReq(lc);
			// Thread.sleep(1000);
			
			Thread.sleep(1000);
		}
	}
}
