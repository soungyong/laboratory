package m2m.smartcon3.test;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import m2m.smartcon3.protocol.gcp2_1.E_GCP_ScheduleType;
import m2m.smartcon3.provider.I_RMIProviderForWeb2;
import m2m.smartcon3.provider.RMI_DeviceManager;
import m2m.smartcon3.provider.RMI_LC100;
import m2m.smartcon3.provider.RMI_MGW300;

public class TestClient {
	public static void main(String args[]) {
		int previouseRebootCount = 0;
		Calendar preCal = Calendar.getInstance();
		List<Long> diffList = new ArrayList<Long>();

		try {
			Registry registry = LocateRegistry.getRegistry("121.152.118.252", 1099);
			I_RMIProviderForWeb2 rmiServer = (I_RMIProviderForWeb2) registry.lookup("smartcon3_plcs2");

			int loggerId = rmiServer.getNewEventLoggerId();
			System.out.println("LoggerId: " + loggerId);

			while (true) {
				RMI_DeviceManager deviceManager = rmiServer.getDeviceManager();
				System.out.println("NumOfGateway: "
						+ deviceManager.getNumOfGateway());
				RMI_MGW300 gateway = (RMI_MGW300) deviceManager
						.getGatewayById((short) 0x0002);
				if (gateway != null) {
					System.out.println(gateway.getConnectionState());
					System.out.println("NumOfLC: "
							+ gateway.getNumOfSubDevices());
					
					//System.out.println(rmiServer.getNumOfEvent(loggerId));
					//rmiServer.sendTimeInfo(gateway);
					
					rmiServer.getEventAll(loggerId);
					//rmiServer.sendControlNoticeModeReq(gateway, false);
										
					RMI_LC100 lc = (RMI_LC100) gateway.getLC((short)0x101a);
					//System.out.println(lc.getPowermeterValue());
					//rmiServer.sendApplyingScheduleReq(lc, E_GCP_ScheduleType.TODAY);
					rmiServer.sendScheduleInfoReq(lc, E_GCP_ScheduleType.TOMORROW);
					//rmiServer.sendResetScheduleReq(lc, E_GCP_ScheduleType.TODAY, (byte)0xff);
					
					//System.out.println(lc.getSensorMappingTableSize());
					//RMI_MSN300 msn = (RMI_MSN300) lc.getDeviceById((short) 0x7260);
					//rmiServer.sendControlIlluSensor(lc, 0, 5, (short)700, (short)950);
					//rmiServer.sendControlCircuit(lc, 0, E_GCP_ControlMode.ILLUSENSOR);

					//rmiServer.sendStateInfoReq(msn);
					
					//System.out.println(msn.getTemperature());
					//System.out.println(msn.getIllumination());
				}

				Thread.sleep(2000);
			}

		} catch (Exception e) {
			System.err.println("ComputePi exception:");
			e.printStackTrace();
		}
	}
}
