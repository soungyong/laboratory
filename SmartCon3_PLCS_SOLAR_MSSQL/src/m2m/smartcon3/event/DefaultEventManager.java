/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
 */
package m2m.smartcon3.event;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Event manager/holder
 */
public class DefaultEventManager implements I_EventManager {
	// public static final Logger logger =
	// Logger.getLogger("DefaultEventManager");

	// LinkedList eq;
	LinkedList<I_Event> eq;

	// (eventClassName, eventHandler) map.
	// HashMap eventHandlerMap;
	// 동일 이벤트에 대하여 여러개의 handler 등록이 가능하다.
	HashMap<String, List<I_EventHandler>> eventHandlerMap;

	// LinkedList<WBEvent> eq;
	Thread eventPumpThread;

	boolean eventThreadRunning;

	/**
	 * Singleton.
	 */
	private static DefaultEventManager instance = null;

	public synchronized static DefaultEventManager getInstance() {
		if (instance == null) {
			instance = new DefaultEventManager();
			return instance;
		} else {
			return instance;
		}
	}

	private DefaultEventManager() {
		eq = new LinkedList<I_Event>();

		// eventHandlerMap = new HashMap();
		eventHandlerMap = new HashMap<String, List<I_EventHandler>>();

	}

	/**
	 * Startup event pump.
	 */
	public void startUp() {
		//
		// logger.info("startDefaultEventManager");

		eventPumpThread = new Thread(new Runnable() {
			public void run() {
				while (eventThreadRunning) {
					try {
						I_Event ev = poll(); // and wait..

						if (ev != null) {
							// find event handler and dispatch
							dispatchEvent(ev);

							// logger.debug("polling new Event");
						} else {
							Thread.sleep(10);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});

		eventThreadRunning = true;
		eventPumpThread.start();

		// logger.info("startDefaultEventManager started..");
	}

	/**
	 * Shutdown event pump.
	 */
	public void shutDown() {
		eventThreadRunning = false;

		eventHandlerMap.clear();

		synchronized (eq) {
			eq.notify();
		}
		// this.notify();
	}

	/**
	 * 이벤트를 등록되어 있는 핸들러로 전달함(dispatch).
	 * 
	 * @param ev
	 */
	private void dispatchEvent(I_Event ev) {
		try {
			// 최하위 concrete 클래스 이름이 우선함.
			List<I_EventHandler> ehList = (List<I_EventHandler>) eventHandlerMap.get(ev.getClass().getName());

			if (ehList != null) {
				for (I_EventHandler eh : ehList)
					eh.handleEvent(ev);
			} else {
				// logger.info("Event handler not found for " +
				// ev.getClass().getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void registerEventHandler(String eventClassName, I_EventHandler myhandler)
	// throws SmartException
	{
		List<I_EventHandler> ehList = (List<I_EventHandler>) eventHandlerMap.get(eventClassName);
		if (ehList != null) {
			// throw new SmartException("Duplicated evnet handler");
		} else {
			ehList = new ArrayList<I_EventHandler>();
			eventHandlerMap.put(eventClassName, ehList);
		}
		// else
		if (ehList.contains(myhandler) == false)
			ehList.add(myhandler);
	}

	public I_EventHandler unregisterEventHandler(String eventClassName) // throws
	// SmartException
	{
		return (I_EventHandler) eventHandlerMap.remove(eventClassName);
	}

	public I_EventHandler unregisterEventHandler(I_EventHandler eventHandler) // throws
	// SmartException
	{
		List<I_EventHandler> eventHandlerList = eventHandlerMap.get(eventHandler);
		if (eventHandlerList != null)
			return eventHandlerList.remove(eventHandlerList.indexOf(eventHandler));
		return null;
	}

	/**
	 * NOTE: thread에서만 사용함.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	protected I_Event poll() throws InterruptedException {
		I_Event ev = null;
		synchronized (eq) {
			do {
				ev = (I_Event) (eq.poll());

				if (ev == null) {
					// logger.debug("Event Waiting");
					eq.wait();
					// logger.debug("Event Waiting post");
				} else
					return ev;
			} while (eventThreadRunning);

			return null;
		}
	}

	/**
	 * 이벤트를 추가함. 등록된 이벤트핸들러로 dispath될 것임.
	 * 
	 * @param e
	 * @return
	 */
	public boolean add(I_Event e) {
		// logger.debug("Event added : " + e.getClass().getName());
		synchronized (eq) {
			eq.add(e);

			eq.notify();
			// logger.info("Event added post: " + e.getClass().getName());
		}
		return true;
	}

}
