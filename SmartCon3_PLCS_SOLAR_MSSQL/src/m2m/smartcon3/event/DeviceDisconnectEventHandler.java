/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
package m2m.smartcon3.event;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.MGW300;
import m2m.smartcon3.device.I_Device.NETWORK_STATE;

/**
 * EventHandler는 이벤트를 처리하는 넘이다.
 * 
 * EventManager에 등록하면, 특정 이벤트(클래스)가 발생하였을 때 알려준다.
 */
public class DeviceDisconnectEventHandler implements I_EventHandler
{
	public void handleEvent(I_Event ev){
		DeviceDisconnectEvent event = (DeviceDisconnectEvent)ev;
		I_Device device = event.getDevice();
		if(device==null)return;
		if(device.getDeviceType()==E_DeviceType.MGW300){
			MGW300 mgw = (MGW300)device;
			//할게..없네?
		}
	}
}
