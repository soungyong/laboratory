/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
package m2m.smartcon3.event;

import java.util.Calendar;
import java.util.Date;

//import m2m.smartcon.server.DefaultEventManager;


/**
 * SEvent의  구현 베이스 클래스.
 * A table view..
 */
public abstract class EventImplBase implements I_Event
{
	Date		date;
	String	name;
	Object 	param;
	
	public EventImplBase(String name, Object param)
	{
		this.name = name;
		this.param = param;
		this.date = Calendar.getInstance().getTime();
	}

	public Date getDate()
	{
		return date;
	}

	public String getName()
	{
		return name;
	}
	
	public Object getParameter()
	{
		return param;
	}
}
