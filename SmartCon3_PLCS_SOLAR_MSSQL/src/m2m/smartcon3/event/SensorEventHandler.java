package m2m.smartcon3.event;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.LC100;
import m2m.smartcon3.device.MGW300;
import m2m.smartcon3.device.MSN300;
import m2m.smartcon3.module.SensorEventLogger;

public class SensorEventHandler implements I_EventHandler {

	@Override
	public void handleEvent(I_Event ev) {
		SensorEvent event = (SensorEvent) ev;
		I_Device device = event.getDevice();
		if (device == null)
			return;
		
		if (device.getDeviceType() == E_DeviceType.MSN300 && device instanceof MSN300 ) {
			MSN300 msn = (MSN300) device;
			LC100 lc;
			MGW300 mgw;
			if (msn == null)
				return;
			lc = (LC100) (msn.getParent());
			if (lc == null)
				return;
			mgw = (MGW300) (lc.getParent());
			if (mgw == null)
				return;
			SensorEventLogger.getInstance().addEvent(msn);
		}
	}
}
