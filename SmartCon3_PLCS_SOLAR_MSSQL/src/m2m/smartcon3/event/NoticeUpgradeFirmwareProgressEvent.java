package m2m.smartcon3.event;

import m2m.smartcon3.core.EventManager;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.rfup1_0.E_RFUP_Result;

public class NoticeUpgradeFirmwareProgressEvent extends EventImplBase {

	I_Device srcDevice;
	int totalNumOfPacket;
	int currentIndex;

	public NoticeUpgradeFirmwareProgressEvent(I_Device device, int totalNumOfPacket, int currentIndex, String name,
			Object param) {
		super(name, param);
		this.srcDevice = device;
		this.totalNumOfPacket = totalNumOfPacket;
		this.currentIndex = currentIndex;

		try {
			EventManager.getInstance().add(this);
		} catch (Exception e) {
		}
	}

	/**
	 * 이벤트 소스 장치를 리턴함
	 * 
	 * @return
	 */
	public I_Device getDevice() {
		return srcDevice;
	}
	

	public I_Device getSrcDevice() {
		return srcDevice;
	}

	public int getTotalNumOfPacket() {
		return totalNumOfPacket;
	}

	public int getCurrentIndex() {
		return currentIndex;
	}

	public String toString() {
		try {
			// DefaultData data = (DefaultData)//getParameter();
			return getName() + "," + getParameter().toString();
		} catch (Exception e) {
			return getName();
		}
	}
}
