/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
package m2m.smartcon3.event;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Set;
import java.util.concurrent.Semaphore;

import m2m.smartcon3.exception.NullResultException;
import m2m.util.HexUtils;
import m2m.util.MyLog;

/**
 * EventHandler는 이벤트를 처리하는 넘이다.
 * 
 * EventManager에 등록하면, 특정 이벤트(클래스)가 발생하였을 때 알려준다.
 */
public class SynchronizedMessageEventHandler implements I_EventHandler, Runnable
{
	private final Semaphore semaphore = new Semaphore(1);
	Hashtable<Integer, Boolean> resultMap = new Hashtable<Integer, Boolean>();
	Hashtable<Integer, Calendar> resultTime = new Hashtable<Integer, Calendar>();
	
	private SynchronizedMessageEventHandler(){
		new Thread(this).start();
	}
	
	private static SynchronizedMessageEventHandler _instance=null;
	public static synchronized SynchronizedMessageEventHandler getInstance() {
		if(_instance==null) {
			_instance = new SynchronizedMessageEventHandler();
		}
		return _instance;
	}
	
	public synchronized void handleEvent(I_Event ev){		
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(ev instanceof FailToSendMessageEvent){
			FailToSendMessageEvent event = (FailToSendMessageEvent) ev;
			resultMap.put(HexUtils.toInt(HexUtils.toHexString(event.getMessage().getSeqNum())), false);
			resultTime.put(HexUtils.toInt(HexUtils.toHexString(event.getMessage().getSeqNum())), Calendar.getInstance());
			MyLog.i(HexUtils.toHexString(event.getMessage().getSeqNum()));
		}else if(ev instanceof ReceiveResponseMessageEvent){
			ReceiveResponseMessageEvent event = (ReceiveResponseMessageEvent) ev;
			resultMap.put(HexUtils.toInt(HexUtils.toHexString(event.getReqMessage().getSeqNum())), true);
			resultTime.put(HexUtils.toInt(HexUtils.toHexString(event.getReqMessage().getSeqNum())), Calendar.getInstance());
			MyLog.i(HexUtils.toHexString(event.getReqMessage().getSeqNum()));
		}
		semaphore.release();
	}
	
	public boolean getResult(int seqNum) throws NullResultException {
		try {
			semaphore.acquire();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean result=false;
		if(resultMap.get(seqNum)!=null)
			result = resultMap.remove(seqNum);
		else {
			semaphore.release();
			throw new NullResultException(""+seqNum);
		}
		semaphore.release();
		return result;
	}

	@Override
	public void run() {
		ArrayList<Integer> tempList = new ArrayList<Integer>();
		while(true) {
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			tempList.clear();
			try {
				semaphore.acquire();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}
			Set<Integer> keys = resultTime.keySet();
			for(Integer key : keys)
				if(resultTime.get(key).getTimeInMillis() + 10000 < System.currentTimeMillis()){					
					tempList.add(key);
				}
			
			for(Integer key : tempList){
				resultMap.remove(key);
				resultTime.remove(key);
			}
			semaphore.release();
		}
		
	}
}
