package m2m.smartcon3.event;

import m2m.smartcon3.core.EventManager;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.rfup1_0.E_RFUP_Result;

public class NoticeUpgradeFirmwareEvent extends EventImplBase {

	I_Device srcDevice;
	E_RFUP_Result result;

	public NoticeUpgradeFirmwareEvent(I_Device device, E_RFUP_Result result, String name,
			Object param) {
		super(name, param);
		this.srcDevice = device;
		this.result = result;

		try {
			EventManager.getInstance().add(this);
		} catch (Exception e) {
		}
	}

	/**
	 * 이벤트 소스 장치를 리턴함
	 * 
	 * @return
	 */
	public I_Device getDevice() {
		return srcDevice;
	}
	

	public I_Device getSrcDevice() {
		return srcDevice;
	}

	public E_RFUP_Result getResult() {
		return result;
	}

	public String toString() {
		try {
			// DefaultData data = (DefaultData)//getParameter();
			return getName() + "," + getParameter().toString();
		} catch (Exception e) {
			return getName();
		}
	}
}
