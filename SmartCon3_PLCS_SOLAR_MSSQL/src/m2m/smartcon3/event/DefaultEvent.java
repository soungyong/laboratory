/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
package m2m.smartcon3.event;


import m2m.smartcon3.device.*;


/**
 * DefaultEvent
 * 디폴트 이벤트, 
 * - DefaultEventManager에 의해 관리되는 이벤트.
 * - 생성하기만 하면, 자동으로 DefaultEventManager에 post되고, 처리/dispatch된다.
 *  
 * 샘플 이벤트 클래스 구현 : 이 클래스와 같이 EventImplBase클래스를 확장하여 사용하면 됨.
 * 
 * - 05/27 toString() 추가. 이벤트 뷰어 등에서 내용을 보여줄 때 사용함. 
 */
public class DefaultEvent extends EventImplBase
{
	I_Device	srcDevice;
	public DefaultEvent(I_Device device, String name, Object param)
	{
		super(name, param);		
		this.srcDevice = device;
	
		try
		{
			DefaultEventManager.getInstance().add(this);
		}
		catch (Exception e)
		{}
	}
		
	/**
	 * 이벤트 소스 장치를 리턴함
	 * @return
	 */
	public I_Device getDevice()
	{
		return srcDevice;
	}
	
	public String toString()
	{
		try
		{
			//DefaultData	data = (DefaultData)//getParameter();
			return getName() + "," + getParameter().toString();			
		}
		catch (Exception e)
		{
			return getName();
		}
	}
}
