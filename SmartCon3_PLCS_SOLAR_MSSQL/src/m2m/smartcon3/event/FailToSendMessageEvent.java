package m2m.smartcon3.event;

import m2m.smartcon3.core.EventManager;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.protocol.I_Message;

public class FailToSendMessageEvent extends EventImplBase {

	I_Device srcDevice;
	I_Message msg;

	public FailToSendMessageEvent(I_Device device, I_Message msg, String name,
			Object param) {
		super(name, param);
		this.srcDevice = device;
		this.msg = msg;

		try {
			EventManager.getInstance().add(this);
		} catch (Exception e) {
		}
	}

	/**
	 * 이벤트 소스 장치를 리턴함
	 * 
	 * @return
	 */
	public I_Device getDevice() {
		return srcDevice;
	}
	
	public I_Message getMessage(){
		return msg;
	}

	public String toString() {
		try {
			// DefaultData data = (DefaultData)//getParameter();
			return getName() + "," + getParameter().toString();
		} catch (Exception e) {
			return getName();
		}
	}
}
