/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
package m2m.smartcon3.event;


/**
 * EventManager 는 active한 이벤트 관리자이다.
 * 
 * 
 */
public interface I_EventManager
{
	/**
	 * Startup event pump.
	 */
	public void startUp();
	
	/**
	 * Shutdown event pump.
	 */
	public void shutDown();

	public void registerEventHandler(String eventClassName, I_EventHandler myhandler)	;
	
	public I_EventHandler unregisterEventHandler(String eventClassName);
}
