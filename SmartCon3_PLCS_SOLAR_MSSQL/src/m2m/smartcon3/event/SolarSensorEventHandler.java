package m2m.smartcon3.event;

import m2m.smartcon3.database.SlcDatabaseManager;
import m2m.smartcon3.device.SolarSensorGetter;

public class SolarSensorEventHandler implements I_EventHandler {

	@Override
	public void handleEvent(I_Event ev) {
		SolarSensorEvent event = (SolarSensorEvent) ev;
		System.out.println("Event Handling!!!");

		SolarSensorGetter solarSensor = (SolarSensorGetter) event.getDevice();
		try {
			SlcDatabaseManager.getInstance().addSolarSensorValue(solarSensor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		SlcDatabaseManager.getInstance().getMonitoringInterval();
	}
}
