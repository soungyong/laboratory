package m2m.smartcon3.event;

import m2m.smartcon3.core.EventManager;
import m2m.smartcon3.device.I_Device;

public class DeviceStateChangedEvent extends EventImplBase {

	I_Device srcDevice;

	public DeviceStateChangedEvent(I_Device device, String name, Object param) {
		super(name, param);
		this.srcDevice = device;

		try {
			EventManager.getInstance().add(this);
		} catch (Exception e) {
		}
	}

	/**
	 * 이벤트 소스 장치를 리턴함
	 * 
	 * @return
	 */
	public I_Device getDevice() {
		return srcDevice;
	}

	public String toString() {
		try {
			// DefaultData data = (DefaultData)//getParameter();
			return getName() + "," + getParameter().toString();
		} catch (Exception e) {
			return getName();
		}
	}
}
