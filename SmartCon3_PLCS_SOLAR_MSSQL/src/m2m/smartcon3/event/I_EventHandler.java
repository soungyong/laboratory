/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
package m2m.smartcon3.event;

/**
 * EventHandler는 이벤트를 처리하는 넘이다.
 * 
 * EventManager에 등록하면, 특정 이벤트(클래스)가 발생하였을 때 알려준다.
 */
public interface I_EventHandler
{
	public void handleEvent(I_Event ev);
}
