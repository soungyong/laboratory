package m2m.smartcon3.event;

import m2m.smartcon3.core.EventManager;
import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.protocol.I_Message;

public class ReceiveResponseMessageEvent extends EventImplBase {

	I_Device srcDevice;
	I_Message reqMsg;
	I_Message resMsg;

	public ReceiveResponseMessageEvent(I_Device device, I_Message reqMsg, I_Message resMsg, String name,
			Object param) {
		super(name, param);
		this.srcDevice = device;
		this.reqMsg = reqMsg;
		this.resMsg = resMsg;

		try {
			EventManager.getInstance().add(this);
		} catch (Exception e) {
		}
	}

	/**
	 * 이벤트 소스 장치를 리턴함
	 * 
	 * @return
	 */
	public I_Device getDevice() {
		return srcDevice;
	}
	
	public I_Message getReqMessage(){
		return reqMsg;
	}
	
	public I_Message getResMessage(){
		return resMsg;
	}

	public String toString() {
		try {
			// DefaultData data = (DefaultData)//getParameter();
			return getName() + "," + getParameter().toString();
		} catch (Exception e) {
			return getName();
		}
	}
}
