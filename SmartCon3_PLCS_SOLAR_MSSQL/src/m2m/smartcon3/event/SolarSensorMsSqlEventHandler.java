package m2m.smartcon3.event;

import m2m.smartcon3.database.SlcDatabaseManager;
import m2m.smartcon3.device.SolarSensorGetter;

public class SolarSensorMsSqlEventHandler implements I_EventHandler {

	@Override
	public void handleEvent(I_Event ev) {
		SolarSensorMSSQLEvent event = (SolarSensorMSSQLEvent) ev;
		System.out.println("MSSQL Event Handling!!!");
		SolarSensorGetter solarSensor = (SolarSensorGetter) event.getDevice();
		try {
			SlcDatabaseManager.getInstance().addSolarSensorValue_MsSql(solarSensor);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// SlcDatabaseManager.getInstance().getMonitoringInterval();
	}
}
