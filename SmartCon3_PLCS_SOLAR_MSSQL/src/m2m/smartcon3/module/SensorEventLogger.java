package m2m.smartcon3.module;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import m2m.smartcon3.device.MSN300;
import m2m.smartcon3.device.SynchronizedQueue;
import m2m.util.HexUtils;

public class SensorEventLogger implements Runnable{

	static SensorEventLogger _instance = null;
	
	private SensorEventLogger(){
		new Thread(this).start();
	}

	public synchronized static SensorEventLogger getInstance() {

		if (_instance == null) {
			_instance = new SensorEventLogger();
		}

		return _instance;
	}

	HashMap<String, HashMap<Long, Integer>> sensorIdToCountMap = new HashMap<String, HashMap<Long, Integer>>();
	HashMap<String, HashMap<Long, Integer>> sensorIdToSensingLevelMap = new HashMap<String, HashMap<Long, Integer>>();

	public void addEvent(MSN300 msn) {
		String sensorIdString = "";
		HashMap<Long, Integer> sensorEventCounterPerTimeSlot = null;
		HashMap<Long, Integer> sensorLevelPerTimeSlot = null;
		int eventCount = 0;
		Long eventTimeInMinute;

		sensorIdString = HexUtils.toHexString(msn.getParent().getId()) + ","
				+ HexUtils.toHexString(msn.getId());
		eventTimeInMinute = System.currentTimeMillis() / 1000 / 60;
		if (sensorIdToCountMap.containsKey(sensorIdString) == false) {
			sensorEventCounterPerTimeSlot = new HashMap<Long, Integer>();
			sensorLevelPerTimeSlot = new HashMap<Long, Integer>();
			sensorIdToCountMap.put(sensorIdString,
					sensorEventCounterPerTimeSlot);
			sensorIdToSensingLevelMap.put(sensorIdString, sensorLevelPerTimeSlot);
		}
		sensorEventCounterPerTimeSlot = sensorIdToCountMap.get(sensorIdString);
		sensorLevelPerTimeSlot = sensorIdToSensingLevelMap.get(sensorIdString);
		if (sensorEventCounterPerTimeSlot.containsKey(eventTimeInMinute) == false) {
			eventCount = 0;
			sensorEventCounterPerTimeSlot.put(eventTimeInMinute, eventCount);
		}		
		eventCount = sensorEventCounterPerTimeSlot.get(eventTimeInMinute);
		sensorEventCounterPerTimeSlot.put(eventTimeInMinute, eventCount + 1);
		
		if (sensorLevelPerTimeSlot.containsKey(eventTimeInMinute) == false) {
			sensorLevelPerTimeSlot.put(eventTimeInMinute, msn.getSensingLevel());
		}		
	}

	public void removeOldLog() {
		long currentTime = System.currentTimeMillis()/1000/60;
		for (HashMap<Long, Integer> sensorEventCounterPerTimeSlot : sensorIdToCountMap
				.values()) {
			ArrayList<Long> victimList = new ArrayList<Long>();
			for (Long dateTimeInMinute : sensorEventCounterPerTimeSlot.keySet()) {
				if (dateTimeInMinute + 2592000000l < currentTime) {
					victimList.add(dateTimeInMinute);
				}
			}
			for (Long victim : victimList)
				sensorEventCounterPerTimeSlot.remove(victim);
		}
		
		for (HashMap<Long, Integer> sensorLevelPerTimeSlot : sensorIdToSensingLevelMap
				.values()) {
			ArrayList<Long> victimList = new ArrayList<Long>();
			for (Long dateTimeInMinute : sensorLevelPerTimeSlot.keySet()) {
				if (dateTimeInMinute + 2592000000l < currentTime) {
					victimList.add(dateTimeInMinute);
				}
			}
			for (Long victim : victimList)
				sensorLevelPerTimeSlot.remove(victim);
		}
	}

	public long getEventCount(MSN300 msn, Long dateTimeInMinute){
		long cnt=0;
		String idString = "";
		idString = HexUtils.toHexString(msn.getParent().getId()) + ","
		+ HexUtils.toHexString(msn.getId());
		//System.out.println(idString);
		try {
			cnt = sensorIdToCountMap.get(idString).get(dateTimeInMinute);
		}catch(Exception e){
			//e.printStackTrace();
		}
		return cnt;
	}
	
	public int getSensorLevel(MSN300 msn, Long dateTimeInMinute){
		int level=0;
		String idString = "";
		idString = HexUtils.toHexString(msn.getParent().getId()) + ","
		+ HexUtils.toHexString(msn.getId());
		//System.out.println(idString);
		try {
			level = sensorIdToSensingLevelMap.get(idString).get(dateTimeInMinute);
		}catch(Exception e){
			//e.printStackTrace();
		}
		return level;
	}

	@Override
	public void run() {
		while(true){
			try {
				Thread.sleep(360000l);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			removeOldLog();
		}		
	}
}
