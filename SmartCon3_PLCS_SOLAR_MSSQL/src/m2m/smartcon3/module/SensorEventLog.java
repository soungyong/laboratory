package m2m.smartcon3.module;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import m2m.smartcon3.device.LC100;
import m2m.smartcon3.device.MGW300;
import m2m.smartcon3.device.MSN300;

public class SensorEventLog implements Serializable{
	short gatewayId;
	short lcId;
	short sensorId;
	Date eventTime;

	public SensorEventLog(short gatewayId, short lcId, short sensorId) {
		this.gatewayId = gatewayId;
		this.lcId = lcId;
		this.sensorId = sensorId;
		
		eventTime = Calendar.getInstance().getTime();
	}

	public SensorEventLog(MSN300 msn) {
		this(msn.getParent().getParent().getId(), msn.getParent().getId(), msn
				.getId());
	}

	public short getGatewayId() {
		return gatewayId;
	}

	public short getLcId() {
		return lcId;
	}

	public short getSensorId() {
		return sensorId;
	}

	public Date getEventTime() {
		return eventTime;
	}

}
