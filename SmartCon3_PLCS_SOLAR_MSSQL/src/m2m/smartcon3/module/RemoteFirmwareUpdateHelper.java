package m2m.smartcon3.module;

import java.util.Calendar;

import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.I_Device.NETWORK_STATE;
import m2m.smartcon3.device.LC100;
import m2m.smartcon3.device.MGW300;
import m2m.smartcon3.devicenetwork.ResponseWaitMessageQueue;
import m2m.smartcon3.event.NoticeUpgradeFirmwareEvent;
import m2m.smartcon3.event.NoticeUpgradeFirmwareProgressEvent;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.rfup1_0.E_RFUP_Result;
import m2m.smartcon3.protocol.rfup1_0.I_RFUP_Message;
import m2m.smartcon3.protocol.rfup1_0.RFUP_Message_SendDataReq;
import m2m.smartcon3.protocol.rfup1_0.RFUP_Message_SendDataRes;
import m2m.smartcon3.protocol.rfup1_0.RFUP_Message_UpgradeReq;
import m2m.smartcon3.protocol.rfup1_0.RFUP_Message_UpgradeRes;
import m2m.util.HexUtils;
import m2m.util.MyLog;

public class RemoteFirmwareUpdateHelper implements Runnable {
	I_Device device = null;
	byte data[][];
	int dataLen[];
	boolean isSuccessToSendData[];
	short totalLen = 0;
	short index = 0;
	boolean isComplete = false;

	ResponseWaitMessageQueue responseWaitMessageQueue = new ResponseWaitMessageQueue();

	public RemoteFirmwareUpdateHelper(I_Device device, byte[] data) {
		this.device = device;
		totalLen = 0;
		for (int i = 0; i < data.length; i++) {
			if (data[i] == ':')
				totalLen++;
		}
		MyLog.i("TotalLen: " + totalLen);

		this.data = new byte[totalLen][21];
		this.dataLen = new int[totalLen];
		this.isSuccessToSendData = new boolean[totalLen + 1];

		for (int i = 0; i < totalLen; i++) {
			isSuccessToSendData[i] = false;
			this.dataLen[i] = 0;
		}

		int dataLenTemp = 0;
		byte dataTemp[] = new byte[42];

		for (int i = 0; i < data.length; i++) {
			if (data[i] == (byte) 0x0a)
				continue;
			if (data[i] == (byte) 0x0d) {
				if (dataLenTemp < 4)
					continue;
				for (int j = 0; j < dataLenTemp; j += 2) {
					if (index == totalLen)
						break;
					this.data[index][this.dataLen[index]++] = HexUtils
							.toByte("" + (char) dataTemp[j]
									+ (char) dataTemp[j + 1]);
				}
				index++;
				dataLenTemp = 0;
			}
			if (data[i] == ':') {
				dataLenTemp = 0;
				continue;
			} else {
				dataTemp[dataLenTemp++] = data[i];
			}
		}

		new Thread(this).start();

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isComplete == false) {
					responseWaitMessageQueue.increaseTimerOfMessages();
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		}, "Tick Thread").start();
	}

	int upgradeFirmFailCnt = 0;
	Calendar lastResponseMessageReceivedTime = Calendar.getInstance();

	public void handleResponseMessage(I_RFUP_Message rfupMessage) {
		lastResponseMessageReceivedTime = Calendar.getInstance();

		I_Message reqIMsg = responseWaitMessageQueue
				.removeRequestMessageOfResponseMessage((I_Message) rfupMessage);
		if (reqIMsg instanceof RFUP_Message_UpgradeReq) {
			if (((RFUP_Message_UpgradeRes) rfupMessage).getResult() == E_RFUP_Result.SUCCESS) {
				MyLog.i("UpgradeReq: SUCCESS");
				isComplete = true;
				new NoticeUpgradeFirmwareEvent(device,
						((RFUP_Message_UpgradeRes) rfupMessage).getResult(),
						"", null);
			} else {
				MyLog.i("UpgradeReq: FAIL");
				upgradeFirmFailCnt++;
				if (upgradeFirmFailCnt > 5) {
					isComplete = true;
					new NoticeUpgradeFirmwareEvent(
							device,
							((RFUP_Message_UpgradeRes) rfupMessage).getResult(),
							"", null);
				}
			}
		} else if (reqIMsg instanceof RFUP_Message_SendDataReq) {
			if (((RFUP_Message_SendDataRes) rfupMessage).getResult() == E_RFUP_Result.SUCCESS) {
				RFUP_Message_SendDataReq reqMsg = (RFUP_Message_SendDataReq) reqIMsg;
				isSuccessToSendData[reqMsg.getIndex()] = true;
				MyLog.i("SendFirmDataReq: Success");
				MyLog.i("Index: " + reqMsg.getIndex());
			} else
				MyLog.i("SendFirmDataReq: FAIL");
		}
	}

	@Override
	public void run() {
		int cnt = 0;
		short lcId = 0;
		short deviceId = 0;
		int progressIndex = 0;

		if (device instanceof MGW300) {
			lcId = 0;
			deviceId = device.getId();
		} else if (device instanceof LC100) {
			lcId = device.getId();
			deviceId = device.getId();
		} else {
			lcId = device.getParent().getId();
			deviceId = device.getId();
		}

		while (true) {
			if (lastResponseMessageReceivedTime.getTimeInMillis() + 60000 < System
					.currentTimeMillis()) { //1분간 응답 없으면.
				isComplete = true;
				new NoticeUpgradeFirmwareEvent(device, E_RFUP_Result.FAIL, "",
						null);
				lastResponseMessageReceivedTime = Calendar.getInstance();
			}
			
			cnt = 0;
			// 전송 되지 않은 데이터 몽땅 다 전송.
			for (int i = 0; i < totalLen; i++) {
				if (isSuccessToSendData[i] == true)
					continue;
				if (cnt == 0)
					progressIndex = i;
				RFUP_Message_SendDataReq msg = new RFUP_Message_SendDataReq(
						(short) 0x0000, device.getId(), device.getDeviceType(),
						lcId, deviceId, totalLen, (short) i, dataLen[i],
						data[i]);
				sendMessage((I_Message) msg);
				cnt++;
				if (cnt > 30)
					break;
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			if (isComplete)
				break; // upgradeFirmwareRes 받았으면 while 종료.
			
			if (cnt == 0) { // 모두다 전송 되었으면. upgradeFirmwareReq 전송.				
				new NoticeUpgradeFirmwareProgressEvent(device, totalLen, totalLen, "", null);
				RFUP_Message_UpgradeReq msg = new RFUP_Message_UpgradeReq(
						(short) 0x0000, device.getId(), device.getDeviceType(),
						lcId, deviceId);
				sendMessage((I_Message) msg);
			}

			if (cnt == 0) {
				try {
					Thread.sleep(10000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				new NoticeUpgradeFirmwareProgressEvent(device, totalLen,
						progressIndex, "", null);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	private void sendMessage(I_Message msg) {
		if (device.getConnectionState() == NETWORK_STATE.OFFLINE)
			return;
		if (device.getXNetHandler() == null)
			return;

		responseWaitMessageQueue.addMessageToResponseWaitMessageQueue(msg, 20);
		device.getXNetHandler().sendMessage(msg);
	}

	public boolean isComplete() {
		return isComplete;
	}

}
