package m2m.smartcon3.protocol.rfup1_0;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

/**
 * @version v2.1
 * @author funface2
 * 
 */

public enum E_RFUP_Result {
	SUCCESS((byte) 0x00),

	FAIL((byte) 0x10),

	DEFAULT((byte) 0x00);

	private byte byteValue = 0x00;

	private E_RFUP_Result() {
	}

	private E_RFUP_Result(byte value) {
		this.byteValue = value;
	}

	public byte toByte() {
		return byteValue;
	}

	public static E_RFUP_Result getFromByte(byte value)
			throws UnformattedPacketException {
		E_RFUP_Result result;
		switch (value) {
		case 0x00:
			result = E_RFUP_Result.SUCCESS;
			break;
		case 0x10:
			result = E_RFUP_Result.FAIL;
			break;
		default:
			throw new UnformattedPacketException("result Ʋ��, "
					+ HexUtils.toHexString(value));
		}

		return result;
	}
}
