package m2m.smartcon3.protocol.rfup1_0;

/**
 * @version v2.0
 * @author funface2
 * 
 */
public enum E_RFUP_MessageType {
	REQ_UPGRADE((byte) 0x01), 
	RES_UPGRADE((byte) 0x02),
	REQ_SEND_DATA((byte) 0x03), 
	RES_SEND_DATA((byte) 0x04);

	private byte byteValue=0;

	private E_RFUP_MessageType() {
	}

	private E_RFUP_MessageType(byte value) {
		this.byteValue = value;
	}
	
	public byte toByte() {
		return byteValue;
	}
}
