package m2m.smartcon3.protocol.rfup1_0;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class RFUP_Message_SendDataReq extends RFUP_Message {
	E_DeviceType deviceType;
	short lcId;
	short deviceId;
	short totalLen;
	short index;
	int dataLen;
	byte data[];

	public RFUP_Message_SendDataReq() {
		msgType = E_RFUP_MessageType.REQ_SEND_DATA;
	}

	public RFUP_Message_SendDataReq(short srcId, short destId,
			E_DeviceType deviceType, short lcId, short deviceId, short totalLen, short index, int dataLen, byte data[]) {
		this();
		this.srcId = srcId;
		this.destId = destId;

		this.deviceType = deviceType;
		this.lcId = lcId;
		this.deviceId = deviceId;
		this.totalLen = totalLen;
		this.index = index;
		this.dataLen = dataLen;
		this.data = data;

		int len = 0;
		byte payload_byte[] = new byte[12+dataLen];
		payload_byte[len++] = msgType.toByte();
		payload_byte[len++] = (byte) (deviceType.toShort() >> 8);
		payload_byte[len++] = (byte) deviceType.toShort();
		payload_byte[len++] = (byte) (lcId >> 8);
		payload_byte[len++] = (byte) (lcId);
		payload_byte[len++] = (byte) (deviceId >> 8);
		payload_byte[len++] = (byte) (deviceId);
		payload_byte[len++] = (byte) (totalLen >> 8);
		payload_byte[len++] = (byte) (totalLen);
		payload_byte[len++] = (byte) (index >> 8);
		payload_byte[len++] = (byte) (index);
		payload_byte[len++] = (byte) (dataLen);
		for(int i=0; i < dataLen; i++)
			payload_byte[len++] = (byte) data[i];

		getPacketBytes(payload_byte);
	}

	public RFUP_Message_SendDataReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload = super.getPayload(packet);
		if (payload.length <12 )
			throw new UnformattedPacketException("패킷 길이 틀림, size: " + size);

		byte messageType = payload[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		deviceType = E_DeviceType.getByShort((short) (HexUtils.toShort(HexUtils
				.toHexString(payload[1])) * 256 + HexUtils.toShort(HexUtils
				.toHexString(payload[2]))));
		lcId = (short) (HexUtils.toShort(HexUtils.toHexString(payload[3])) * 256 + HexUtils
				.toShort(HexUtils.toHexString(payload[4])));
		deviceId = (short) (HexUtils.toShort(HexUtils.toHexString(payload[5])) * 256 + HexUtils
				.toShort(HexUtils.toHexString(payload[6])));
		totalLen = (short) (HexUtils.toShort(HexUtils.toHexString(payload[7])) * 256 + HexUtils
				.toShort(HexUtils.toHexString(payload[8])));
		index = (short) (HexUtils.toShort(HexUtils.toHexString(payload[9])) * 256 + HexUtils
				.toShort(HexUtils.toHexString(payload[10])));
		dataLen = (short) HexUtils
				.toShort(HexUtils.toHexString(payload[11]));
		if (payload.length != 12+dataLen )
			throw new UnformattedPacketException("패킷 길이 틀림, size: " + size);
		data = new byte[dataLen];
		for(int i=0; i < dataLen; i++)
			data[i] = payload[12+i];
	}

	public E_DeviceType getDeviceType() {
		return deviceType;
	}

	public short getLcId() {
		return lcId;
	}

	public short getDeviceId() {
		return deviceId;
	}
	
	

	public short getTotalLen() {
		return totalLen;
	}

	public short getIndex() {
		return index;
	}

	public int getDataLen() {
		return dataLen;
	}

	public byte[] getData() {
		return data;
	}

	public static void main(String args[]) {
		RFUP_Message_SendDataReq pingReqMsg = new RFUP_Message_SendDataReq(
				(short) 0x1234, (short)0x3212, E_DeviceType.LC100, (short)0x0102, (short)0x0192, (short)1, (short)1, 10, new byte[10]);
		pingReqMsg.setSeqNum((byte) 0x03);
		byte[] data = pingReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			pingReqMsg = new RFUP_Message_SendDataReq(data, data.length);
			data = pingReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
