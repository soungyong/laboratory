package m2m.smartcon3.protocol.rfup1_0;

public interface I_RFUP_Message {	
	static final byte RFUP1_0=(byte) 0xA0;
	public byte[] toBytes();
	public E_RFUP_MessageType getMessageType();
	public int getLength();
	public byte getChecksum();
	public byte getPid();
	public byte getSubPid();
	public short getSeqNum();
	public short getSrcId();
	public short getDestId();
	public short getSeqNumGenerator();
	public void setSeqNum(short seqNum);
}
