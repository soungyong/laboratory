package m2m.smartcon3.protocol.rfup1_0;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.util.HexUtils;

public class RFUP_Message_UpgradeRes extends RFUP_Message {	
	E_DeviceType deviceType;
	short lcId;
	short deviceId;
	E_RFUP_Result result;
	
	public RFUP_Message_UpgradeRes(){
		msgType = E_RFUP_MessageType.RES_UPGRADE;
	}

	public RFUP_Message_UpgradeRes(short srcId, short destId, E_DeviceType deviceType, short lcId, short deviceId, E_RFUP_Result result) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.deviceType = deviceType;
		this.lcId = lcId;
		this.deviceId = deviceId;
		this.result = result;
		
		int len=0;
		byte payload_bytes[] = new byte[8];		
		payload_bytes[len++] = msgType.toByte();
		payload_bytes[len++] = (byte) (deviceType.toShort() >> 8);
		payload_bytes[len++] = (byte) deviceType.toShort();
		payload_bytes[len++] = (byte) (lcId >> 8);
		payload_bytes[len++] = (byte) (lcId);
		payload_bytes[len++] = (byte) (deviceId >> 8);
		payload_bytes[len++] = (byte) (deviceId);
		payload_bytes[len++] = result.toByte();		
		
		super.getPacketBytes(payload_bytes);
	}

	public RFUP_Message_UpgradeRes(byte[] packet, int size) throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		
		if(payload_bytes.length!=8)throw new UnformattedPacketException("패킷 길이 틀림");
		
		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		deviceType = E_DeviceType.getByShort((short) (HexUtils.toShort(HexUtils
				.toHexString(payload_bytes[1])) * 256 + HexUtils.toShort(HexUtils
				.toHexString(payload_bytes[2]))));
		lcId = (short) (HexUtils.toShort(HexUtils.toHexString(payload_bytes[3])) * 256 + HexUtils
				.toShort(HexUtils.toHexString(payload_bytes[4])));
		deviceId = (short) (HexUtils.toShort(HexUtils.toHexString(payload_bytes[5])) * 256 + HexUtils
				.toShort(HexUtils.toHexString(payload_bytes[6])));
		this.result = E_RFUP_Result.getFromByte(payload_bytes[7]);
	}
	
	public E_RFUP_Result getResult() {
		return result;
	}
	
	public E_DeviceType getDeviceType() {
		return deviceType;
	}

	public short getLcId() {
		return lcId;
	}

	public short getDeviceId() {
		return deviceId;
	}

	public static void main(String args[]) {
		RFUP_Message_UpgradeRes pingResMsg = new RFUP_Message_UpgradeRes(
				new Short((short) 0x1234), new Short((short) 0x5678), E_DeviceType.LC100, (short)0x01, (short)0x02, E_RFUP_Result.SUCCESS);
		pingResMsg.setSeqNum((byte)0x01);
		byte[] data = pingResMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");
		
		try {
			pingResMsg = new RFUP_Message_UpgradeRes(data, data.length);
			data = pingResMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
	
	
}
