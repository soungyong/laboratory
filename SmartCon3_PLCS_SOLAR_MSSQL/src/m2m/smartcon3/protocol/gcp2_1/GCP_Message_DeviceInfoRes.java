package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_DeviceInfoRes extends GCP_Message {
	
	short subNodeId;
	short deviceId;
	short deviceType;
	byte deviceVersion;
	
	public GCP_Message_DeviceInfoRes(){
		msgType = E_GCP_MessageType.RES_DEVICEINFO;	
	}
	public GCP_Message_DeviceInfoRes(short srcId, short destId, short subNodeId,
			short deviceId, short deviceType, byte deviceVersion) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
		this.deviceVersion = deviceVersion;

		int len = 0;
		byte data_bytes[] = new byte[8];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(deviceId >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceId).byteValue();
		data_bytes[len++] = new Integer(deviceType >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceType).byteValue();
		data_bytes[len++] = new Integer(deviceVersion).byteValue();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_DeviceInfoRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=8)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		deviceId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])));
		deviceType = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[6])));
		deviceVersion = payload_bytes[7];
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public short getDeviceId() {
		return deviceId;
	}

	public short getDeviceType() {
		return deviceType;
	}

	public int getDeviceVersion() {
		return deviceVersion;
	}

	public static void main(String args[]) {
		GCP_Message_DeviceInfoRes deviceInfoRes = new GCP_Message_DeviceInfoRes(
				(short)0x1321, (short)0x1234, (short)0x4321, (short) 0x3322, (short) 0x2010, (byte) 0x11);
		byte[] data = deviceInfoRes.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoRes = new GCP_Message_DeviceInfoRes(data,
					data.length);
			data = deviceInfoRes.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
