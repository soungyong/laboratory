package m2m.smartcon3.protocol.gcp2_1;

public class GCP_Message_IrControlReq extends GCP_Message {
	
	short subNodeId;
	short irId;
	E_GCP_IrControlCompany company;
	byte[] data;
	
	public GCP_Message_IrControlReq() {
		msgType = E_GCP_MessageType.REQ_IR_CONTROL;
	}
	
	public GCP_Message_IrControlReq(short srcId, short destId, short subNodeId,
			short irId, E_GCP_IrControlCompany company, byte[] data) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.company = company;
		this.data = data;
		
		int len = 0;
		byte data_bytes[] = new byte[7+data.length];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(irId >> 8).byteValue();
		data_bytes[len++] = new Integer(irId).byteValue();
		data_bytes[len++] = company.toByte();
		data_bytes[len++] = new Integer(data.length).byteValue();
		for(int i=0 ; i<data.length ; i++) {
			data_bytes[len++] = data[i];
		}
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_IrControlReq(byte[] packet, int size) {
		//TODO : 나중에하자
	}


}
