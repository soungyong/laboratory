package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_ControlGatewayReq extends GCP_Message {
	

	String deviceIp;
	String serverIp;
	String defaultGatewayIp;
	String netmask;
	int port;
	boolean isSensorSharingMode;

	public GCP_Message_ControlGatewayReq(){
		msgType = E_GCP_MessageType.REQ_CONTROL_GATEWAY;
	}
	
	public GCP_Message_ControlGatewayReq(short srcId, short destId, String deviceIp,
			String serverIp, String defaultGatewayIp, String netmask, int port,
			boolean isSensorSharingMode) throws UnformattedPacketException {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.deviceIp = deviceIp;
		this.serverIp = serverIp;
		this.defaultGatewayIp = defaultGatewayIp;
		this.netmask = netmask;
		this.port = port;
		this.isSensorSharingMode = isSensorSharingMode;

		int len=0;
		byte data_bytes[] = new byte[20];
		data_bytes[len++] = msgType.toByte();		
		for (int i = 0; i < 4; i++)
			data_bytes[len++] = new Integer(deviceIp.split("\\.")[i])
					.byteValue();
		for (int i = 0; i < 4; i++)
			data_bytes[len++] = new Integer(serverIp.split("\\.")[i])
					.byteValue();
		for (int i = 0; i < 4; i++)
			data_bytes[len++] = new Integer(defaultGatewayIp.split("\\.")[i])
					.byteValue();
		for (int i = 0; i < 4; i++)
			data_bytes[len++] = new Integer(netmask.split("\\.")[i])
					.byteValue();
		data_bytes[len++] = new Integer(port >> 8).byteValue();
		data_bytes[len++] = new Integer(port).byteValue();
		data_bytes[len++] = new Integer(isSensorSharingMode ? 1 : 0)
				.byteValue();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_ControlGatewayReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=20)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		deviceIp = "";
		for (int i = 0; i < 4; i++)
			deviceIp += ""
					+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[1 + i]))
					+ (i != 3 ? "." : "");
		serverIp = "";
		for (int i = 0; i < 4; i++)
			serverIp += ""
					+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[5 + i]))
					+ (i != 3 ? "." : "");
		defaultGatewayIp = "";
		for (int i = 0; i < 4; i++)
			defaultGatewayIp += ""
					+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[9 + i]))
					+ (i != 3 ? "." : "");
		netmask = "";
		for (int i = 0; i < 4; i++)
			netmask += ""
					+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[13 + i]))
					+ (i != 3 ? "." : "");
		port = HexUtils.toInt(HexUtils.toHexString(payload_bytes[17])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[18]));
		isSensorSharingMode = (payload_bytes[19] == 0 ? false : true);

	}

	public String getDeviceIp() {
		return deviceIp;
	}

	public String getServerIp() {
		return serverIp;
	}

	public String getDefaultGatewayIp() {
		return defaultGatewayIp;
	}

	public String getNetmask() {
		return netmask;
	}

	public int getPort() {
		return port;
	}

	public boolean isSensorSharingMode() {
		return isSensorSharingMode;
	}

	public static void main(String args[]) throws UnformattedPacketException {
		GCP_Message_ControlGatewayReq stateInfoReqMsg = new GCP_Message_ControlGatewayReq(
				(short)0x1234, (short)0x1321, "192.168.0.50", "192.168.0.100", "192.168.0.1",
				"255.255.255.0", 30011, true);
		byte[] data = stateInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			stateInfoReqMsg = new GCP_Message_ControlGatewayReq(data,
					data.length);
			data = stateInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
