package m2m.smartcon3.protocol.gcp2_1;

import java.util.Calendar;
import java.util.Date;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_AddScheduleReq extends GCP_Message {
	short subNodeId;
	E_GCP_ScheduleType scheduleType;
	int circuitId;
	Date onTime;
	E_GCP_ControlMode ctrlMode;
	
	public GCP_Message_AddScheduleReq(){
		msgType = E_GCP_MessageType.REQ_ADD_SCHEDULE;
	}
	
	public GCP_Message_AddScheduleReq(short srcId, short destId, short subNodeId,
			E_GCP_ScheduleType schedueType, int circuitId, Date onTime, E_GCP_ControlMode ctrlMode) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.scheduleType = schedueType;
		this.circuitId = circuitId;
		this.onTime = onTime;
		this.ctrlMode = ctrlMode;
		
		int len = 0;
		byte data_bytes[] = new byte[8];		
		data_bytes[len++] = msgType.toByte();
		
		Calendar calendarTemp = Calendar.getInstance();
		calendarTemp.setTime(onTime);
		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = scheduleType.toByte(); 
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = new Integer(calendarTemp.get(Calendar.HOUR_OF_DAY)).byteValue();
		data_bytes[len++] = new Integer(calendarTemp.get(Calendar.MINUTE)).byteValue();
		data_bytes[len++] = ctrlMode.toByte();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_AddScheduleReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=8)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		scheduleType = E_GCP_ScheduleType.getFromByte(payload_bytes[3]);	
		circuitId = HexUtils.toInt(HexUtils.toHexString(payload_bytes[4]));
		Calendar calendarTemp = Calendar.getInstance();
		calendarTemp.set(Calendar.HOUR_OF_DAY, new Integer(payload_bytes[5]));
		calendarTemp.set(Calendar.MINUTE, new Integer(payload_bytes[6]));
		onTime = calendarTemp.getTime();
		ctrlMode = E_GCP_ControlMode.getFromByte(payload_bytes[7]);
	}

	public short getSubNodeId() {
		return subNodeId;
	}
	
	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public E_GCP_ScheduleType getScheduleType() {
		return scheduleType;
	}

	public Date getOnTime() {
		return onTime;
	}

	public E_GCP_ControlMode getCtrlMode() {
		return ctrlMode;
	}

	public static void main(String args[]) {
		GCP_Message_AddScheduleReq deviceInfoReqMsg = new GCP_Message_AddScheduleReq(
				(short)0x5312, (short)0x1234, (short)0x4321, E_GCP_ScheduleType.DEFAULT, 10, Calendar.getInstance().getTime()
				, E_GCP_ControlMode.OFF);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_AddScheduleReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
