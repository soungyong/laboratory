package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_NoticeEvent extends GCP_Message {
	
	short subNodeId;
	int dataFormat;
	short sensorId;

	public GCP_Message_NoticeEvent(){
		msgType = E_GCP_MessageType.NOTICE_EVENT;	
	}
	public GCP_Message_NoticeEvent(short srcId, short destId, short subNodeId, int dataFormat, short sensorId) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.dataFormat = dataFormat;
		this.sensorId = sensorId;

		int len = 0;
		byte data_bytes[] = new byte[6];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();		
		data_bytes[len++] = new Integer(dataFormat).byteValue();
		data_bytes[len++] = new Integer(sensorId >> 8).byteValue();
		data_bytes[len++] = new Integer(sensorId).byteValue();
		
		getPacketBytes(data_bytes);

	}

	public GCP_Message_NoticeEvent(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=6)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		dataFormat = HexUtils.toInt(HexUtils.toHexString(payload_bytes[3]));
		sensorId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])) * 256
		+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])));	
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public int getDataFormat() {
		return dataFormat;
	}

	public short getSensorId() {
		return sensorId;
	}

	public static void main(String args[]) {				
		GCP_Message_NoticeEvent deviceInfoReqMsg = new GCP_Message_NoticeEvent(
				(short)0x1521, (short)0x1234, (short)0x4321, 0x01, (short)0x1234);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_NoticeEvent(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
