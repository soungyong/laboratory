package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

/**
 * @version v2.1
 * @author funface2
 * 
 */

public enum E_GCP_ScheduleType {	
	DEFAULT((byte)0x00),
	TODAY((byte)0x01),
	TOMORROW((byte)0x02);		

	private byte byteValue=0x00;

	private E_GCP_ScheduleType() {
	}

	private E_GCP_ScheduleType(byte value) {
		this.byteValue = value;
	}

	public byte toByte() {
		return byteValue;
	}
	
	public static E_GCP_ScheduleType getFromByte(byte value) throws UnformattedPacketException {
		E_GCP_ScheduleType scheduleType;
		switch (value) {
		case 0x00:
			scheduleType = E_GCP_ScheduleType.DEFAULT;
			break;
		case 0x01:
			scheduleType = E_GCP_ScheduleType.TODAY;
			break;
		case 0x02:
			scheduleType = E_GCP_ScheduleType.TOMORROW;
			break;
		default:
			throw new UnformattedPacketException("schedule type  Ʋ��, "
					+ HexUtils.toHexString(value));
		}
		
		return scheduleType;
	}
}
