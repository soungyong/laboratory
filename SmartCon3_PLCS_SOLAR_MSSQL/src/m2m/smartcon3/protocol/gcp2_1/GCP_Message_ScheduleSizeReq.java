package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_ScheduleSizeReq extends GCP_Message {

	short subNodeId;
	E_GCP_ScheduleType scheduleType;

	public GCP_Message_ScheduleSizeReq() {
		msgType = E_GCP_MessageType.REQ_SCHEDULE_SIZE;
	}

	public GCP_Message_ScheduleSizeReq(short srcId, short destId,
			short subNodeId, E_GCP_ScheduleType scheduleType) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.scheduleType = scheduleType;

		int len = 0;
		byte data_bytes[] = new byte[4];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = scheduleType.toByte();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_ScheduleSizeReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 4)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		subNodeId = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[1])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[2])));
		scheduleType = E_GCP_ScheduleType.getFromByte(payload_bytes[3]);
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public E_GCP_ScheduleType getScheduleType() {
		return scheduleType;
	}

	public static void main(String args[]) {
		GCP_Message_ScheduleSizeReq deviceInfoReqMsg = new GCP_Message_ScheduleSizeReq(
				(short) 0x4212, (short) 0x1234, (short) 0x4321, E_GCP_ScheduleType.DEFAULT);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_ScheduleSizeReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
