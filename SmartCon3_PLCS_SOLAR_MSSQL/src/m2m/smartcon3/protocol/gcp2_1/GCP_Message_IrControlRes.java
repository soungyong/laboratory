package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_IrControlRes extends GCP_Message {

	short subNodeId;
	short irId;
	E_GCP_Result result;
	
	public GCP_Message_IrControlRes() {
		msgType = E_GCP_MessageType.RES_IR_CONTROL;
	}
	
	public GCP_Message_IrControlRes(byte[] packet, int size) throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length != 6) throw new UnformattedPacketException("패킷 길이 틀림");
		
		byte messageType = payload_bytes[0];
		if(messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "+HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		irId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])));
		result = E_GCP_Result.getFromByte(payload_bytes[5]);
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public short getIrId() {
		return irId;
	}

	public E_GCP_Result getResult() {
		return result;
	}


}
