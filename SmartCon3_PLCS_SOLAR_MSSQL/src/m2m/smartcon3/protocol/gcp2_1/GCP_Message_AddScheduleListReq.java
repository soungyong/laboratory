package m2m.smartcon3.protocol.gcp2_1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_AddScheduleListReq extends GCP_Message {
	short subNodeId;
	E_GCP_ScheduleType scheduleType;
	int circuitId;
	int numOfSchedule;
	List<Date> onTimeList = new ArrayList<Date>();
	List<E_GCP_ControlMode> ctrlModeList = new ArrayList<E_GCP_ControlMode>();

	public GCP_Message_AddScheduleListReq() {
		msgType = E_GCP_MessageType.REQ_ADD_SCHEDULE_LIST;
	}

	public GCP_Message_AddScheduleListReq(short srcId, short destId,
			short subNodeId, E_GCP_ScheduleType schedueType, int circuitId,
			int numOfSchedule, Date onTime[], E_GCP_ControlMode ctrlMode[]) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.scheduleType = schedueType;
		this.circuitId = circuitId;
		this.numOfSchedule = numOfSchedule;
		this.onTimeList.clear();
		for (int i = 0; i < numOfSchedule; i++)
			this.onTimeList.add(onTime[i]);
		this.ctrlModeList.clear();
		for (int i = 0; i < numOfSchedule; i++)
			this.ctrlModeList.add(ctrlMode[i]);

		int len = 0;
		byte data_bytes[] = new byte[6 + 3 * numOfSchedule];
		data_bytes[len++] = msgType.toByte();

		Calendar calendarTemp = Calendar.getInstance();

		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = scheduleType.toByte();
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = new Integer(numOfSchedule).byteValue();
		for (int i = 0; i < numOfSchedule; i++) {
			calendarTemp.setTime(this.onTimeList.get(i));

			data_bytes[len++] = new Integer(
					calendarTemp.get(Calendar.HOUR_OF_DAY)).byteValue();
			data_bytes[len++] = new Integer(calendarTemp.get(Calendar.MINUTE))
					.byteValue();
			data_bytes[len++] = this.ctrlModeList.get(i).toByte();
		}

		getPacketBytes(data_bytes);
	}

	public GCP_Message_AddScheduleListReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		subNodeId = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[1])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[2])));
		scheduleType = E_GCP_ScheduleType.getFromByte(payload_bytes[3]);
		circuitId = HexUtils.toInt(HexUtils.toHexString(payload_bytes[4]));
		numOfSchedule = HexUtils.toInt(HexUtils.toHexString(payload_bytes[5]));
		if (payload_bytes.length != 6 + numOfSchedule * 3)
			throw new UnformattedPacketException("패킷 길이 틀림");

		Calendar calendarTemp = Calendar.getInstance();
		this.onTimeList.clear();
		this.ctrlModeList.clear();
		
		for (int i = 0; i < numOfSchedule; i++) {
			calendarTemp.set(Calendar.HOUR_OF_DAY,
					new Integer(payload_bytes[6+i*3]));
			calendarTemp.set(Calendar.MINUTE, new Integer(payload_bytes[7+i*3]));
			onTimeList.add(calendarTemp.getTime());
			ctrlModeList.add(E_GCP_ControlMode.getFromByte(payload_bytes[8+i*3]));
		}
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public E_GCP_ScheduleType getScheduleType() {
		return scheduleType;
	}

	public int getNumOfSchedule() {
		return numOfSchedule;
	}

	public List<Date> getOnTimeList() {
		return onTimeList;
	}

	public List<E_GCP_ControlMode> getCtrlModeList() {
		return ctrlModeList;
	}

	public static void main(String args[]) {
		GCP_Message_AddScheduleListReq deviceInfoReqMsg = new GCP_Message_AddScheduleListReq(
				(short) 0x5312, (short) 0x1234, (short) 0x4321,
				E_GCP_ScheduleType.DEFAULT, 10, 0, null, null);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_AddScheduleListReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
