package m2m.smartcon3.protocol.gcp2_1;

import m2m.util.MyLog;

public enum E_GCP_IrButtonSamsung {
	DEFAULT(0),
	POWER(1),
	TEMPURATURE_UP(2),
	TEMPURATURE_DOWN(3),
	MODE(4),
	WINDPOWER(5),
	TURBO(6),
	SAVING(7);
	
	private int intValue = 0;
	
	private E_GCP_IrButtonSamsung() {
		this.intValue = 0;
	}
	
	private E_GCP_IrButtonSamsung(int value) {
		this.intValue = value;
	}
	
	public int toInt() {
		return intValue;
	}
	
	public static E_GCP_IrButtonSamsung getFromInt(int value) {
		E_GCP_IrButtonSamsung button = null;
		switch (value) {
		case 0:
			button = E_GCP_IrButtonSamsung.DEFAULT;
			break;
		case 1:
			button = E_GCP_IrButtonSamsung.POWER;
			break;
		case 2:
			button = E_GCP_IrButtonSamsung.TEMPURATURE_UP;
			break;
		case 3:
			button = E_GCP_IrButtonSamsung.TEMPURATURE_DOWN;
			break;
		case 4:
			button = E_GCP_IrButtonSamsung.MODE;
			break;
		case 5:
			button = E_GCP_IrButtonSamsung.WINDPOWER;
			break;
		case 6:
			button = E_GCP_IrButtonSamsung.TURBO;
			break;
		case 7:
			button = E_GCP_IrButtonSamsung.SAVING;
			break;
		default:
			button = E_GCP_IrButtonSamsung.DEFAULT;
			MyLog.e("Ir Button Samsung Mode Ʋ��, "+value);
			break;
		}
		
		return button;
	}
}
