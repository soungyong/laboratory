package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_SensorMappingInfoReq extends GCP_Message {
	
	short subNodeId;
	int index;

	public GCP_Message_SensorMappingInfoReq(){
		msgType = E_GCP_MessageType.REQ_SENSOR_MAPPING_INFO;	
	}
	
	public GCP_Message_SensorMappingInfoReq(short srcId, short destId, short subNodeId,
			int index) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.index = index;
		
		int len = 0;
		byte data_bytes[] = new byte[4];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(index).byteValue();		

		getPacketBytes(data_bytes);
	}

	public GCP_Message_SensorMappingInfoReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=4)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		index = new Integer(payload_bytes[3]);
	}

	public short getSubNodeId() {
		return subNodeId;
	}
	
	public int getIndex() {
		return index;
	}

	public static void main(String args[]) {
		GCP_Message_SensorMappingInfoReq deviceInfoReqMsg = new GCP_Message_SensorMappingInfoReq(
				(short)0x4123, (short)0x1234, (short)0x4321, 10);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_SensorMappingInfoReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
