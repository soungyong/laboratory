package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_ControlNoticeModeReq extends GCP_Message {
		
	boolean enable=false;

	public GCP_Message_ControlNoticeModeReq() {
		msgType = E_GCP_MessageType.REQ_CONTROL_NOTICEMODE;	
	}
	public GCP_Message_ControlNoticeModeReq(short srcId, short destId, boolean enable) {
		this();
		
		this.srcId = srcId;
		this.destId = destId;
		this.enable = enable;		
		int len = 0;
		byte data_bytes[] = new byte[2];		
		data_bytes[len++] = msgType.toByte();				
		data_bytes[len++] = (byte) (enable ? 0x01:0x00);
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_ControlNoticeModeReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=2)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		enable = payload_bytes[1]==0x00 ? false:true;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public boolean isEnable() {
		return enable;
	}

	public static void main(String args[]) {
		GCP_Message_ControlNoticeModeReq deviceInfoReqMsg = new GCP_Message_ControlNoticeModeReq(
				(short)0x3212, (short)0x1234, false);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_ControlNoticeModeReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
