package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_ResetScheduleReq extends GCP_Message {
	short subNodeId;
	byte circuitId;
	E_GCP_ScheduleType scheduleType;

	public GCP_Message_ResetScheduleReq() {
		msgType = E_GCP_MessageType.REQ_RESET_SCHEDULE;
	}

	public GCP_Message_ResetScheduleReq(short srcId, short destId,
			short subNodeId, E_GCP_ScheduleType scheduleType, byte circuitId) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.scheduleType = scheduleType;
		this.circuitId = circuitId;

		int len = 0;
		byte data_bytes[] = new byte[5];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = scheduleType.toByte();
		data_bytes[len++] = circuitId;

		getPacketBytes(data_bytes);
	}

	public GCP_Message_ResetScheduleReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 5)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		scheduleType = E_GCP_ScheduleType.getFromByte(payload_bytes[3]);
		circuitId = payload_bytes[4];
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}
		
	public E_GCP_ScheduleType getScheduleType() {
		return scheduleType;
	}

	public byte getCircuitId() {
		return circuitId;
	}

	public static void main(String args[]) {
		GCP_Message_ResetScheduleReq deviceInfoReqMsg = new GCP_Message_ResetScheduleReq(
				(short)0x5122, (short)0x1234, (short)0x4321, E_GCP_ScheduleType.DEFAULT, (byte)0xff);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_ResetScheduleReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
