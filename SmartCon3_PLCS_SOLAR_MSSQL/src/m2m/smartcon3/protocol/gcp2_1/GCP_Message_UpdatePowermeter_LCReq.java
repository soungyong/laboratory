package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_UpdatePowermeter_LCReq extends GCP_Message {		
	short subNodeId;
	short mdfId;
	int energyPulse;
	
	public GCP_Message_UpdatePowermeter_LCReq() {
		msgType = E_GCP_MessageType.REQ_UPDATEPOWERMETER_LC;
	}

	public GCP_Message_UpdatePowermeter_LCReq(short srcId, short destId, short subNodeId, short mdfId,
			int energyPulse) {
		this();
		this.destId = destId;
		this.srcId = srcId;
		this.subNodeId = subNodeId;
		this.mdfId = mdfId;
		this.energyPulse = energyPulse;

		int len = 0;
		byte data_bytes[] = new byte[9];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(mdfId >> 8).byteValue();
		data_bytes[len++] = new Integer(mdfId).byteValue();
		data_bytes[len++] = new Integer(energyPulse >> 24).byteValue();
		data_bytes[len++] = new Integer(energyPulse >> 16).byteValue();
		data_bytes[len++] = new Integer(energyPulse >> 8).byteValue();
		data_bytes[len++] = new Integer(energyPulse).byteValue();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_UpdatePowermeter_LCReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=9)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		
		mdfId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])));	
				
		energyPulse = (HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])) << 24) + (HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[6])) << 16) + (HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[7])) << 8) + HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[8]));
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}
	public int getEnergyPulse() {
		return energyPulse;
	}
	
	public static void main(String args[]) {
		GCP_Message_UpdatePowermeter_LCReq deviceInfoReqMsg = new GCP_Message_UpdatePowermeter_LCReq(
				(short)0x1234, (short)0x3456, (short)0x4321, (short)0, 0x12345678);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_UpdatePowermeter_LCReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
