package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_MappingDimmerListReq extends GCP_Message {
		
	short subNodeId;
	int numOfDimmers;
	short dimmerIds[];
	int channelIds[];
	int circuitId;

	public GCP_Message_MappingDimmerListReq(){
		msgType = E_GCP_MessageType.REQ_MAPPING_DIMMER_LIST;	
	}
	
	public GCP_Message_MappingDimmerListReq(short srcId, short destId, short subNodeId,
			int numOfDimmers, short dimmerId[], int channelId[], int circuitId)
			throws UnformattedPacketException {
		this();
		this.srcId = srcId;
		this.destId = destId;		
		this.subNodeId = subNodeId;
		this.numOfDimmers = numOfDimmers;
		this.dimmerIds = dimmerId;
		this.channelIds = channelId;
		this.circuitId = circuitId;

		if (numOfDimmers < 0 || numOfDimmers > 20)
			throw new UnformattedPacketException("센서 수 잘못됨, " + numOfDimmers);
		if (dimmerId == null)
			throw new UnformattedPacketException("센서Id 리스트 잘못됨");
		if (dimmerId.length != numOfDimmers)
			throw new UnformattedPacketException("입력된 센서 수와 센서 id리스트의 크기가 다름");
		
		int len = 0;
		byte data_bytes[] = new byte[5 + numOfDimmers * 3];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(numOfDimmers).byteValue();
		for (int i = 0; i < numOfDimmers; i++) {
			data_bytes[len++] = new Integer(dimmerIds[i] >> 8).byteValue();
			data_bytes[len++] = new Integer(dimmerIds[i]).byteValue();
			data_bytes[len++] = new Integer(channelIds[i]).byteValue();
		}
		data_bytes[len++] = new Integer(circuitId).byteValue();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_MappingDimmerListReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		
		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		numOfDimmers = HexUtils.toInt(HexUtils.toHexString(payload_bytes[3]));

		if(payload_bytes.length!=5 + numOfDimmers * 3)throw new UnformattedPacketException("패킷 길이 틀림");

		dimmerIds = new short[numOfDimmers];
		channelIds = new int[numOfDimmers];
		for (int i = 0; i < numOfDimmers; i++){
			dimmerIds[i] = (short) (HexUtils.toInt(HexUtils
					.toHexString(packet[4 + i * 3]))
					* 256
					+ HexUtils.toInt(HexUtils.toHexString(packet[5 + i * 3])));
			channelIds[i] = HexUtils.toInt(HexUtils.toHexString(packet[6 + i * 3]));
		}

		circuitId = HexUtils.toInt(HexUtils
				.toHexString(packet[4 + 3 * numOfDimmers]));
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public int getNumOfDimmers() {
		return numOfDimmers;
	}

	public short[] getDimmerIds() {
		return dimmerIds;
	}

	public int getCircuitId() {
		return circuitId;
	}
	
	public int[] getChannelIds() {
		return channelIds;
	}

	public static void main(String args[]) throws UnformattedPacketException {
		GCP_Message_MappingDimmerListReq deviceInfoReqMsg = new GCP_Message_MappingDimmerListReq(
				(short)0x1241, (short)0x1234, (short)0x4321, 2, new short[] { 1, 2 }, new int[]{0, 0}, 10);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_MappingDimmerListReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
