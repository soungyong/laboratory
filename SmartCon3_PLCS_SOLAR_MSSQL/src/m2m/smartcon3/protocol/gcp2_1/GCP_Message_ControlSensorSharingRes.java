package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_ControlSensorSharingRes extends GCP_Message {
	
	E_GCP_Result result;

	public GCP_Message_ControlSensorSharingRes(){
		msgType = E_GCP_MessageType.RES_CONTROL_SENSORSHARING;	
	}
	public GCP_Message_ControlSensorSharingRes(short srcId, short destId,
			E_GCP_Result result) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.result = result;

		int len = 0;
		byte data_bytes[] = new byte[2];		
		data_bytes[len++] = msgType.toByte();				
		data_bytes[len++] = result.toByte();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_ControlSensorSharingRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=2)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
				
		result = E_GCP_Result.getFromByte(payload_bytes[1]);
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public E_GCP_Result getResult() {
		return result;
	}

	public static void main(String args[]) {
		GCP_Message_ControlSensorSharingRes deviceInfoReqMsg = new GCP_Message_ControlSensorSharingRes((short)0x2312, (short)
				0x1234, E_GCP_Result.SUCCESS);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_ControlSensorSharingRes(
					data, data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
