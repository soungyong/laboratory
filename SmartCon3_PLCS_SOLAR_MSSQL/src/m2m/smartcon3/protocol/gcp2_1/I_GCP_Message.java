package m2m.smartcon3.protocol.gcp2_1;


public interface I_GCP_Message {
	static final byte GCP2_1=0x21;
	
	public byte[] toBytes();
	public E_GCP_MessageType getMessageType();
	public int getLength();
	public byte getChecksum();
	public short getSrcId();
	public short getDestId();
	public short getSeqNumGenerator();
	public void setSeqNum(short seqNum);
	public short getSeqNum();
}
