package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_ResetDebugLogReq extends GCP_Message {	
	public GCP_Message_ResetDebugLogReq(){
		msgType = E_GCP_MessageType.REQ_RESET_DEBUG_LOG;;	
	}
	
	public GCP_Message_ResetDebugLogReq(short srcId, short destId){
		this();
		this.srcId = srcId;
		this.destId = destId;
		
		int len = 0;
		byte data_bytes[] = new byte[1];		
		data_bytes[len++] = msgType.toByte();		
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_ResetDebugLogReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=1)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));				

	}

	public static void main(String args[]) {
		GCP_Message_ResetDebugLogReq deviceInfoReqMsg = new GCP_Message_ResetDebugLogReq(
				(short)0x1421, (short)0x1234);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_ResetDebugLogReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
