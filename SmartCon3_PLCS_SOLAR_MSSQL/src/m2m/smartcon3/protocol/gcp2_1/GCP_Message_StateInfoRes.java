package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.device.E_DeviceType;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_StateInfoRes extends GCP_Message {

	short subNodeId;
	short deviceId;
	short deviceType;
	String deviceIp = "";
	String serverIp = "";
	String defaultGatewayIp = "";
	String netmask = "";
	int port;
	boolean isSensorSharingMode;
	E_GCP_ControlMode stateInfo[] = new E_GCP_ControlMode[16];
	E_GCP_ControlMode controlInfo[] = new E_GCP_ControlMode[16];
	int sensingLevel;
	int dimmingState;
	short panId;
	int illumination;
	int temperature;
	int current;

	public GCP_Message_StateInfoRes() {
		msgType = E_GCP_MessageType.RES_STATEINFO;
	}

	public GCP_Message_StateInfoRes(short srcId, short destId, short subNodeId, short deviceId, short deviceType, String deviceIp, String serverIp, String defaultGatewayIp, String netmask, int port,
			boolean isSensorSharingMode) throws UnformattedPacketException {
		this();
		this.srcId = srcId;
		this.destId = destId;

		this.subNodeId = subNodeId;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
		this.deviceIp = deviceIp;
		this.serverIp = serverIp;
		this.defaultGatewayIp = defaultGatewayIp;
		this.netmask = netmask;
		this.port = port;
		this.isSensorSharingMode = isSensorSharingMode;

		if (deviceType != 0x2000)
			throw new UnformattedPacketException("devicetype 틀림");

		if (deviceIp.split("\\.").length != 4)
			throw new UnformattedPacketException("deviceIp 잘못됨, " + deviceIp);
		if (serverIp.split("\\.").length != 4)
			throw new UnformattedPacketException("serverIp 잘못됨, " + serverIp);
		if (defaultGatewayIp.split("\\.").length != 4)
			throw new UnformattedPacketException("defaultGatewayIp 잘못됨, " + defaultGatewayIp);
		if (netmask.split("\\.").length != 4)
			throw new UnformattedPacketException("netmask 잘못됨, " + netmask);

		int len = 0;
		byte data_bytes[] = new byte[26];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(deviceId >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceId).byteValue();
		data_bytes[len++] = new Integer(deviceType >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceType).byteValue();

		for (int i = 0; i < 4; i++)
			data_bytes[len++] = new Integer(deviceIp.split("\\.")[i]).byteValue();
		for (int i = 0; i < 4; i++)
			data_bytes[len++] = new Integer(serverIp.split("\\.")[i]).byteValue();
		for (int i = 0; i < 4; i++)
			data_bytes[len++] = new Integer(defaultGatewayIp.split("\\.")[i]).byteValue();
		for (int i = 0; i < 4; i++)
			data_bytes[len++] = new Integer(netmask.split("\\.")[i]).byteValue();
		data_bytes[len++] = new Integer(port >> 8).byteValue();
		data_bytes[len++] = new Integer(port).byteValue();
		data_bytes[len++] = new Integer(isSensorSharingMode ? 1 : 0).byteValue();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_StateInfoRes(short srcId, short destId, short subNodeId, short deviceId, short deviceType, E_GCP_ControlMode stateInfo[], E_GCP_ControlMode controlInfo[], short panId)
			throws UnformattedPacketException {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
		this.stateInfo = stateInfo;
		this.controlInfo = controlInfo;
		this.panId = panId;

		if (deviceType != 0x2010)
			throw new UnformattedPacketException("device type 틀림");

		int len = 0;
		byte data_bytes[] = new byte[41];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(deviceId >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceId).byteValue();
		data_bytes[len++] = new Integer(deviceType >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceType).byteValue();

		for (int i = 0; i < 16; i++)
			data_bytes[len++] = stateInfo[i].toByte();
		for (int i = 0; i < 16; i++)
			data_bytes[len++] = controlInfo[i].toByte();
		data_bytes[len++] = new Integer(panId >> 8).byteValue();
		data_bytes[len++] = new Integer(panId).byteValue();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_StateInfoRes(short srcId, short destId, short subNodeId, short deviceId, short deviceType, int value) throws UnformattedPacketException {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.deviceId = deviceId;
		this.deviceType = deviceType;
		this.sensingLevel = value;
		this.dimmingState = value;

		// if (deviceType != 0x2050 && deviceType != 0x2040)
		// throw new UnformattedPacketException("device type 틀림");

		int len = 0;

		byte data_bytes[] = new byte[8];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(deviceId >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceId).byteValue();
		data_bytes[len++] = new Integer(deviceType >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceType).byteValue();
		data_bytes[len++] = new Integer(sensingLevel).byteValue();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_StateInfoRes(byte[] packet, int size) throws UnformattedPacketException {
		this();

		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 26 && payload_bytes.length != 41 && payload_bytes.length != 8 && payload_bytes.length != 12 && payload_bytes.length != 11)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, " + HexUtils.toHexString(messageType));

		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		deviceId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])));
		deviceType = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[6])));

		if (deviceType == E_DeviceType.MGW300.toShort()) {
			if (payload_bytes.length != 26)
				throw new UnformattedPacketException("패킷 길이 틀림");
			deviceIp = "";
			for (int i = 0; i < 4; i++)
				deviceIp += "" + HexUtils.toInt(HexUtils.toHexString(payload_bytes[7 + i])) + (i != 3 ? "." : "");
			serverIp = "";
			for (int i = 0; i < 4; i++)
				serverIp += "" + HexUtils.toInt(HexUtils.toHexString(payload_bytes[11 + i])) + (i != 3 ? "." : "");
			defaultGatewayIp = "";
			for (int i = 0; i < 4; i++)
				defaultGatewayIp += "" + HexUtils.toInt(HexUtils.toHexString(payload_bytes[15 + i])) + (i != 3 ? "." : "");
			netmask = "";
			for (int i = 0; i < 4; i++)
				netmask += "" + HexUtils.toInt(HexUtils.toHexString(payload_bytes[19 + i])) + (i != 3 ? "." : "");
			port = HexUtils.toInt(HexUtils.toHexString(payload_bytes[23])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[24]));
			isSensorSharingMode = (payload_bytes[25] == 0 ? false : true);

		} else if (deviceType == E_DeviceType.LC100.toShort()) {
			if (payload_bytes.length != 41)
				throw new UnformattedPacketException("패킷 길이 틀림");

			for (int i = 0; i < 16; i++)
				stateInfo[i] = E_GCP_ControlMode.getFromByte(payload_bytes[7 + i]);
			for (int i = 0; i < 16; i++)
				controlInfo[i] = E_GCP_ControlMode.getFromByte(payload_bytes[23 + i]);
			panId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[39])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[40])));
		} else if (deviceType == E_DeviceType.MDT100.toShort()) {
			if (payload_bytes.length != 8)
				throw new UnformattedPacketException("패킷 길이 틀림");
			dimmingState = HexUtils.toInt(HexUtils.toHexString(payload_bytes[7]));
		} else if (deviceType == E_DeviceType.MSN300.toShort()) {

			sensingLevel = HexUtils.toInt(HexUtils.toHexString(payload_bytes[7]));
			if (payload_bytes.length == 12) {
				temperature = HexUtils.toInt(HexUtils.toHexString(payload_bytes[8])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[9]));
				illumination = HexUtils.toInt(HexUtils.toHexString(payload_bytes[10])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[11]));
			}
		} else if (deviceType == E_DeviceType.MDF100.toShort()) {
			if (payload_bytes.length != 11)
				throw new UnformattedPacketException("패킷 길이 틀림");
			temperature = HexUtils.toInt(HexUtils.toHexString(payload_bytes[7])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[8]));
			illumination = HexUtils.toInt(HexUtils.toHexString(payload_bytes[9])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[10]));
		} else if (deviceType == E_DeviceType.MPR200.toShort()) {
			if (payload_bytes.length != 11)
				throw new UnformattedPacketException("패킷 길이 틀림");
			temperature = HexUtils.toInt(HexUtils.toHexString(payload_bytes[7])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[8]));
			current = HexUtils.toInt(HexUtils.toHexString(payload_bytes[9])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[10]));
		} else if (deviceType == E_DeviceType.SolarSensorGetter.toShort()) {
		} else {
			System.out.println(" + Device Type : " + String.format("%04X", deviceType));
			throw new UnformattedPacketException("device Type 틀림");
		}
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public short getDeviceId() {
		return deviceId;
	}

	public int getDeviceType() {
		return deviceType;
	}

	public String getDeviceIp() {
		return deviceIp;
	}

	public String getServerIp() {
		return serverIp;
	}

	public String getDefaultGatewayIp() {
		return defaultGatewayIp;
	}

	public String getNetmask() {
		return netmask;
	}

	public int getPort() {
		return port;
	}

	public boolean isSensorSharingMode() {
		return isSensorSharingMode;
	}

	public E_GCP_ControlMode[] getStateInfo() {
		return stateInfo;
	}

	public E_GCP_ControlMode[] getControlInfo() {
		return controlInfo;
	}

	public int getSensingLevel() {
		return sensingLevel;
	}

	public int getDimmingState() {
		return dimmingState;
	}

	public short getPanId() {
		return panId;
	}

	public int getIllumination() {
		return illumination;
	}

	public int getTemperature() {
		return temperature;
	}

	public int getCurrent() {
		return current;
	}

	public static void main(String args[]) throws UnformattedPacketException {
		GCP_Message_StateInfoRes stateInfoReqMsg = new GCP_Message_StateInfoRes((short) 0x5412, (short) 0x1234, (short) 0x4321, (short) 0x3344, (short) 0x2000, "192.168.0.50", "192.168.0.100",
				"192.168.0.1", "255.255.255.0", 30011, true);
		byte[] data = stateInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			stateInfoReqMsg = new GCP_Message_StateInfoRes(data, data.length);
			data = stateInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		E_GCP_ControlMode stateInfo[] = new E_GCP_ControlMode[16];
		E_GCP_ControlMode controlInfo[] = new E_GCP_ControlMode[16];
		for (int i = 0; i < 16; i++) {
			controlInfo[i] = stateInfo[i] = E_GCP_ControlMode.DEFAULT;
		}
		stateInfoReqMsg = new GCP_Message_StateInfoRes((short) 0x5412, (short) 0x1234, (short) 0x4321, (short) 0x3344, (short) 0x2010, stateInfo, controlInfo, (short) 0);

		data = stateInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			stateInfoReqMsg = new GCP_Message_StateInfoRes(data, data.length);
			data = stateInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		stateInfoReqMsg = new GCP_Message_StateInfoRes((short) 0x5412, (short) 0x1234, (short) 0x4321, (short) 0x3344, (short) 0x2050, 12);

		data = stateInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			stateInfoReqMsg = new GCP_Message_StateInfoRes(data, data.length);
			data = stateInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
