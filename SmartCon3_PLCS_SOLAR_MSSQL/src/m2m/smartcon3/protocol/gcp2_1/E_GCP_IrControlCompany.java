package m2m.smartcon3.protocol.gcp2_1;

import m2m.util.HexUtils;
import m2m.util.MyLog;

public enum E_GCP_IrControlCompany {
	DEFAULT((byte) 0x00),
	SAMSUNG((byte) 0x01),
	LG((byte) 0x02);
	
	private byte byteValue = 0x00;
	
	private E_GCP_IrControlCompany() {
		this.byteValue = (byte) 0x00;
	}
	
	private E_GCP_IrControlCompany(byte value) {
		this.byteValue = value;
	}
	
	public byte toByte() {
		return byteValue;
	}
	
	public static E_GCP_IrControlCompany getFromByte(byte value){
		E_GCP_IrControlCompany companyMode = null;
		switch (value) {
		case 0x00:
			companyMode = E_GCP_IrControlCompany.DEFAULT;
			break;
		case 0x01:
			companyMode = E_GCP_IrControlCompany.SAMSUNG;
			break;
		case 0x02:
			companyMode = E_GCP_IrControlCompany.LG;
			break;
		default:
			companyMode = E_GCP_IrControlCompany.DEFAULT;
			MyLog.e("Company Mode Ʋ��, "+HexUtils.toHexString(value));
			break;
		}
		
		return companyMode;
	}
}
