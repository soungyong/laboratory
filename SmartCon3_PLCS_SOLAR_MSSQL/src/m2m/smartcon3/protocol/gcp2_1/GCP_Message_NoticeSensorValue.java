package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_NoticeSensorValue extends GCP_Message {

	short subNodeId;
	short sensorId;
	int numOfSensor;
	short data[] = new short[16];

	public GCP_Message_NoticeSensorValue() {
		msgType = E_GCP_MessageType.NOTICE_SENSOR_VALUE;
	}

	public GCP_Message_NoticeSensorValue(short srcId, short destId, short subNodeId, short sensorId, int numOfSensor, short data[]) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.sensorId = sensorId;

		int len = 0;
		byte data_bytes[] = new byte[6 + data.length * 2];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(sensorId >> 8).byteValue();
		data_bytes[len++] = new Integer(sensorId).byteValue();
		data_bytes[len++] = new Integer(numOfSensor).byteValue();
		// for (int i = 0; i < numOfSensor; i++) {
		for (int i = 0; i < 16; i++) {
			data_bytes[len++] = new Integer(data[i] >> 8).byteValue();
			data_bytes[len++] = new Integer(data[i]).byteValue();
		}

		getPacketBytes(data_bytes);
	}

	public GCP_Message_NoticeSensorValue(byte[] packet, int size) throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, " + HexUtils.toHexString(messageType));

		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		sensorId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])));
		numOfSensor = (int) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])));

		if (payload_bytes.length != 6 + numOfSensor * 2)
			throw new UnformattedPacketException("패킷 길이 틀림");

		for (int i = 0; i < numOfSensor; i++) {
			data[i] = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[6 + i * 2])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[7 + i * 2])));
		}
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public short getSensorId() {
		return sensorId;
	}

	public int getNumOfSensor() {
		return numOfSensor;
	}

	public void setNumOfSensor(int numOfSensor) {
		this.numOfSensor = numOfSensor;
	}

	public short[] getData() {
		return data;
	}

	public void setData(short[] data) {
		this.data = data;
	}

	public static void main(String args[]) {
		short data2[] = new short[1];
		data2[0] = (short) 0x01;
		GCP_Message_NoticeSensorValue deviceInfoReqMsg = new GCP_Message_NoticeSensorValue((short) 0x1521, (short) 0x1234, (short) 0x4321, (short) 0x1234, 1, data2);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_NoticeSensorValue(data, data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
