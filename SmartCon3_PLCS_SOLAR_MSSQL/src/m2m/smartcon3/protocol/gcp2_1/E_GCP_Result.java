package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

/**
 * @version v2.1
 * @author funface2
 * 
 */

public enum E_GCP_Result {
	SUCCESS((byte) 0x00), UPDATE((byte) 0x01),

	FAIL((byte) 0x10), FAIL_CIRCUIT_ID((byte) 0x11), FAIL_CTRLMODE((byte) 0x12),

	FAIL_OnOffDimmingLevel((byte) 0x21),

	FAIL_ALREADYMAPPED((byte) 0x31),
	
	FAIL_NOT_DEFINED_COMPANY((byte) 0x41), FAIL_DATA_PARSING_ERROR((byte) 0x42), FAIL_UNKNOWN((byte) 0x43),

	DEFAULT((byte) 0x00);

	private byte byteValue = 0x00;

	private E_GCP_Result() {
	}

	private E_GCP_Result(byte value) {
		this.byteValue = value;
	}

	public byte toByte() {
		return byteValue;
	}

	public static E_GCP_Result getFromByte(byte value)
			throws UnformattedPacketException {
		E_GCP_Result result;
		switch (value) {
		case 0x00:
			result = E_GCP_Result.SUCCESS;
			break;
		case 0x01:
			result = E_GCP_Result.UPDATE;
			break;
		case 0x10:
			result = E_GCP_Result.FAIL;
			break;
		case 0x11:
			result = E_GCP_Result.FAIL_CIRCUIT_ID;
			break;
		case 0x12:
			result = E_GCP_Result.FAIL_CTRLMODE;
			break;
		case 0x21:
			result = FAIL_OnOffDimmingLevel;
			break;
		case 0x31:
			result = FAIL_ALREADYMAPPED;
			break;
		default:
			throw new UnformattedPacketException("result Ʋ��, "
					+ HexUtils.toHexString(value));
		}

		return result;
	}
}
