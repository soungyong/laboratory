package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_ControlCircuitReq extends GCP_Message {
	
	short subNodeId;
	int circuitId;
	E_GCP_ControlMode ctrlMode;

	public GCP_Message_ControlCircuitReq()
	{
		msgType = E_GCP_MessageType.REQ_CONTROL_CIRCUIT;
	}
	
	public GCP_Message_ControlCircuitReq(short srcId, short destId, short subNodeId,
			int circuitId, E_GCP_ControlMode ctrlMode) {	
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.circuitId = circuitId;
		this.ctrlMode = ctrlMode;

		int len = 0;
		byte data_bytes[] = new byte[5];

		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = ctrlMode.toByte();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_ControlCircuitReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 5)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		circuitId = new Integer(payload_bytes[3]);

		if (circuitId < 0 || circuitId > 15)
			throw new UnformattedPacketException("Circuit ID 틀림, "
					+ HexUtils.toHexString(circuitId));

		ctrlMode = E_GCP_ControlMode.getFromByte(payload_bytes[4]);
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public E_GCP_ControlMode getCtrlMode() {
		return ctrlMode;
	}

	public static void main(String args[]) {
		GCP_Message_ControlCircuitReq deviceInfoReqMsg = new GCP_Message_ControlCircuitReq(
				(short)0x1234,(short)0x4534, (short)0x4321, 1, E_GCP_ControlMode.ON);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_ControlCircuitReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
