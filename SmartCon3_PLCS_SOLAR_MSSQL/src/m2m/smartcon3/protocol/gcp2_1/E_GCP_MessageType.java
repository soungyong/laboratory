package m2m.smartcon3.protocol.gcp2_1;

/**
 * @version v2.1
 * @author funface2
 * 
 */

public enum E_GCP_MessageType {
	NOTICE_SENSOR_VALUE((byte) 0x10), REQ_NUMOFDEVICE((byte) 0x20), RES_NUMOFDEVICE((byte) 0x21), REQ_DEVICEINFO((byte) 0x22), RES_DEVICEINFO((byte) 0x23), REQ_STATEINFO(
			(byte) 0x24), RES_STATEINFO((byte) 0x25),

	REQ_CONTROL_CIRCUIT((byte) 0x30), RES_CONTROL_CIRCUIT((byte) 0x31), REQ_CONTROL_SENSOR((byte) 0x32), RES_CONTROL_SENSOR((byte) 0x33), REQ_CONTROL_SENSORSHARING(
			(byte) 0x34), RES_CONTROL_SENSORSHARING((byte) 0x35), REQ_CONTROL_GATEWAY((byte) 0x36), RES_CONTROL_GATEWAY((byte) 0x37), REQ_CONTROL_ILLU_SENSOR(
			(byte) 0x38), RES_CONTROL_ILLU_SENSOR((byte) 0x39), REQ_REBOOT((byte) 0x3A), REQ_CONTROL_NOTICEMODE((byte) 0x3B), RES_CONTROL_NOTICEMODE(
			(byte) 0x3C),

	REQ_POWERMETER((byte) 0x40), RES_POWERMETER((byte) 0x41), REQ_UPDATEPOWERMETER_LC((byte) 0x42), REQ_UPDATEPOWERMETER((byte) 0x44),

	REQ_SENSOR_MAPPING_SIZE((byte) 0x50), RES_SENSOR_MAPPING_SIZE((byte) 0x51), REQ_DIMMER_MAPPING_SIZE((byte) 0x52), RES_DIMMER_MAPPING_SIZE((byte) 0x53), REQ_SCHEDULE_SIZE(
			(byte) 0x54), RES_SCHEDULE_SIZE((byte) 0x55), REQ_SENSOR_MAPPING_INFO((byte) 0x56), RES_SENSOR_MAPPING_INFO((byte) 0x57), REQ_DIMMER_MAPPING_INFO(
			(byte) 0x58), RES_DIMMER_MAPPING_INFO((byte) 0x59), REQ_SCHEDULE_INFO((byte) 0x5a), RES_SCHEDULE_INFO((byte) 0x5b), REQ_MAPPING_SENSOR((byte) 0x5c), RES_MAPPING_SENSOR(
			(byte) 0x5d), REQ_MAPPING_SENSOR_LIST((byte) 0x5e), RES_MAPPING_SENSOR_LIST((byte) 0x5f), REQ_RESET_MAPPING_SENSOR((byte) 0x60), RES_RESET_MAPPING_SENSOR(
			(byte) 0x61), REQ_MAPPING_DIMMER((byte) 0x62), RES_MAPPING_DIMMER((byte) 0x63), REQ_MAPPING_DIMMER_LIST((byte) 0x64), RES_MAPPING_DIMMER_LIST(
			(byte) 0x65), REQ_RESET_MAPPING_DIMMER((byte) 0x66), RES_RESET_MAPPING_DIMMER((byte) 0x67), REQ_ADD_SCHEDULE((byte) 0x68), RES_ADD_SCHEDULE(
			(byte) 0x69), REQ_ADD_SCHEDULE_LIST((byte) 0x6a), RES_ADD_SCHEDULE_LIST((byte) 0x6b), REQ_RESET_SCHEDULE((byte) 0x6c), RES_RESET_SCHEDULE(
			(byte) 0x6d), REQ_APPLYING_SCHEDULE((byte) 0x6e), RES_APPLYING_SCHEDULE((byte) 0x6f),

	NOTICE_EVENT((byte) 0x70),

	REQ_SET_SENSING_LEVEL((byte) 0x80), RES_SET_SENSING_LEVEL((byte) 0x81),

	REQ_IR_CONTROL((byte) 0x85), RES_IR_CONTROL((byte) 0x86),

	REQ_DEBUG_LOG((byte) 0x90), RES_DEBUG_LOG((byte) 0x91), REQ_RESET_DEBUG_LOG((byte) 0x92), REQ_DEBUG_LOG_LC((byte) 0x93), RES_DEBUG_LOG_LC((byte) 0x94), REQ_RESET_DEBUG_LOG_LC(
			(byte) 0x95),

	BOAT_SENSOR_MESSAGE((byte) 0x97), TILT_MESSAGE((byte) 0x98), REQ_TILT_CONTROL((byte) 0x99),

	DEFAULT((byte) 0x00);

	private byte byteValue = 0x00;

	private E_GCP_MessageType() {
	}

	private E_GCP_MessageType(byte value) {
		this.byteValue = value;
	}

	public byte toByte() {
		return byteValue;
	}
}
