package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_DebugLogLCRes extends GCP_Message {			
	short subNodeId;	
	int rebootCount;
	byte lastRebootTime[] = new byte[2];
	int serverConnectionCount;
	byte[] lastServerConnectionTime = new byte[2];

	public GCP_Message_DebugLogLCRes(){
		msgType = E_GCP_MessageType.RES_DEBUG_LOG_LC;	
	}
	
	public GCP_Message_DebugLogLCRes(short srcId, short destId, short subnodeId,
			int rebootCount, byte[] lastRebootTime,
			int serverConnctionCount, byte[] lastServerConnectionTime) {
		this();
		this.srcId = srcId;
		this.destId = destId;		
		
		this.rebootCount = rebootCount;
		this.lastRebootTime = lastRebootTime;
		this.serverConnectionCount = serverConnctionCount;
		this.lastServerConnectionTime = lastServerConnectionTime;

		int len = 0;
		byte data_bytes[] = new byte[11];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subnodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subnodeId).byteValue();
		
		data_bytes[len++] = new Integer(rebootCount >> 8).byteValue();
		data_bytes[len++] = new Integer(rebootCount).byteValue();
		for(int i=0; i < 2; i++)
			data_bytes[len++] = lastRebootTime[i];
		data_bytes[len++] = new Integer(serverConnectionCount >> 8).byteValue();
		data_bytes[len++] = new Integer(serverConnectionCount).byteValue();
		for(int i=0; i < 2; i++)
			data_bytes[len++] = lastServerConnectionTime[i];
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_DebugLogLCRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=11)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
		+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		
		rebootCount = HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[4]));
		for (int i = 0; i < 2; i++)
			lastRebootTime[i] = payload_bytes[5 + i];
		serverConnectionCount = HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[7]))
				* 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[8]));
		for (int i = 0; i < 2; i++)
			lastServerConnectionTime[i] = payload_bytes[9 + i];
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public int getRebootCount() {
		return rebootCount;
	}

	public byte[] getLastRebootTime() {
		return lastRebootTime;
	}

	public int getServerConnectionCount() {
		return serverConnectionCount;
	}

	public byte[] getLastServerConnectionTime() {
		return lastServerConnectionTime;
	}
	
	public short getSubNodeId() {
		return subNodeId;
	}

	public static void main(String args[]) {
		GCP_Message_DebugLogLCRes deviceInfoReqMsg = new GCP_Message_DebugLogLCRes(
				(short)0x4312, (short)0x1234, (short)0x3456, 0x1234, new byte[]{12,8}, 0x3456, new byte[]{12,8});
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_DebugLogLCRes(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
