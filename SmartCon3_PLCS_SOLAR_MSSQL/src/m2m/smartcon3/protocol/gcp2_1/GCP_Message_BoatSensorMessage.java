package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_BoatSensorMessage extends GCP_Message {

	short subNodeId;
	int messageLength;
	String message;

	public GCP_Message_BoatSensorMessage() {
		msgType = E_GCP_MessageType.BOAT_SENSOR_MESSAGE;
	}

	public GCP_Message_BoatSensorMessage(short srcId, short destId, short subNodeId, String command) {
		this();
	}

	public GCP_Message_BoatSensorMessage(byte[] packet, int size) throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, " + HexUtils.toHexString(messageType));

		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		messageLength = payload_bytes[3];
		message = "";
		for (int i = 0; i < messageLength; i++) {
			message = message + (char) payload_bytes[4 + i];
		}
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public static void main(String args[]) {
	}

	public String getMessage() {
		return this.message;
	}
}
