package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_ControlSensorReq extends GCP_Message {
	
	short subNodeId;
	int circuitId;
	int onTime;
	E_GCP_ControlMode onDimmingLevel;
	E_GCP_ControlMode offDimmingLevel;

	public GCP_Message_ControlSensorReq(){
		msgType = E_GCP_MessageType.REQ_CONTROL_SENSOR;		
	}
	
	public GCP_Message_ControlSensorReq(short srcId, short destId, short subNodeId,
			int circuitId, int onTime, E_GCP_ControlMode onDimmingLevel,
			E_GCP_ControlMode offDimmingLevel) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.circuitId = circuitId;
		this.onTime = onTime;
		this.onDimmingLevel = onDimmingLevel;
		this.offDimmingLevel = offDimmingLevel;

		int len = 0;
		byte data_bytes[] = new byte[7];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = new Integer(onTime).byteValue();
		data_bytes[len++] = onDimmingLevel.toByte();
		data_bytes[len++] = offDimmingLevel.toByte();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_ControlSensorReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=7)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		circuitId = new Integer(payload_bytes[3]);

		if (circuitId < 0 || circuitId > 15)
			throw new UnformattedPacketException("Circuit ID 틀림, "
					+ HexUtils.toHexString(circuitId));
		
		onTime = new Integer(payload_bytes[4]);

		onDimmingLevel = E_GCP_ControlMode.getFromByte(payload_bytes[5]);
		offDimmingLevel = E_GCP_ControlMode.getFromByte(payload_bytes[6]);
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public int getOnTime() {
		return onTime;
	}

	public E_GCP_ControlMode getOnDimmingLevel() {
		return onDimmingLevel;
	}

	public E_GCP_ControlMode getOffDimmingLevel() {
		return offDimmingLevel;
	}

	public static void main(String args[]) {
		GCP_Message_ControlSensorReq deviceInfoReqMsg = new GCP_Message_ControlSensorReq(
				(short)0x1234, (short)0x2312, (short)0x4321, 1, 10, E_GCP_ControlMode.ON, E_GCP_ControlMode.OFF);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_ControlSensorReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
