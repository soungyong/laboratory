package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;
import m2m.util.MyLog;

/**
 * @version v2.1
 * @author funface2
 * 
 */

public enum E_GCP_ControlMode {
	OFF((byte) 0x00), ON((byte) 0x01), SENSOR((byte) 0x02), SCHEDULE(
			(byte) 0x03), DIMMING_0((byte) 0x10), DIMMING_1((byte) 0x11), DIMMING_2(
			(byte) 0x12), DIMMING_3((byte) 0x13), DIMMING_4((byte) 0x14), DIMMING_5(
			(byte) 0x15), DIMMING_6((byte) 0x16), DIMMING_7((byte) 0x17), DIMMING_8(
			(byte) 0x18), DIMMING_9((byte) 0x19), DIMMING_10((byte) 0x1A), ILLU_DIMMING_0(
			(byte) 0x20), ILLU_DIMMING_1((byte) 0x21), ILLU_DIMMING_2(
			(byte) 0x22), ILLU_DIMMING_3((byte) 0x23), ILLU_DIMMING_4(
			(byte) 0x24), ILLU_DIMMING_5((byte) 0x25), ILLU_DIMMING_6(
			(byte) 0x26), ILLU_DIMMING_7((byte) 0x27), ILLU_DIMMING_8(
			(byte) 0x28), ILLU_DIMMING_9((byte) 0x29), ILLU_DIMMING_10(
			(byte) 0x2A),

	DEFAULT((byte) 0xff);

	private byte byteValue = 0x00;

	private E_GCP_ControlMode() {
		this.byteValue = (byte) 0x01;
	}

	private E_GCP_ControlMode(byte value) {
		this.byteValue = value;
	}

	public byte toByte() {
		return byteValue;
	}

	public static E_GCP_ControlMode getFromByte(byte value)
			throws UnformattedPacketException {
		E_GCP_ControlMode ctrlMode;
		switch (value) {
		case 0x00:
			ctrlMode = E_GCP_ControlMode.OFF;
			break;
		case 0x01:
			ctrlMode = E_GCP_ControlMode.ON;
			break;
		case 0x02:
			ctrlMode = E_GCP_ControlMode.SENSOR;
			break;
		case 0x03:
			ctrlMode = E_GCP_ControlMode.SCHEDULE;
			break;
		case 0x10:
			ctrlMode = E_GCP_ControlMode.DIMMING_0;
			break;
		case 0x11:
			ctrlMode = E_GCP_ControlMode.DIMMING_1;
			break;
		case 0x12:
			ctrlMode = E_GCP_ControlMode.DIMMING_2;
			break;
		case 0x13:
			ctrlMode = E_GCP_ControlMode.DIMMING_3;
			break;
		case 0x14:
			ctrlMode = E_GCP_ControlMode.DIMMING_4;
			break;
		case 0x15:
			ctrlMode = E_GCP_ControlMode.DIMMING_5;
			break;
		case 0x16:
			ctrlMode = E_GCP_ControlMode.DIMMING_6;
			break;
		case 0x17:
			ctrlMode = E_GCP_ControlMode.DIMMING_7;
			break;
		case 0x18:
			ctrlMode = E_GCP_ControlMode.DIMMING_8;
			break;
		case 0x19:
			ctrlMode = E_GCP_ControlMode.DIMMING_9;
			break;
		case 0x1A:
			ctrlMode = E_GCP_ControlMode.DIMMING_10;
			break;
		case 0x20:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_0;
			break;
		case 0x21:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_1;
			break;
		case 0x22:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_2;
			break;
		case 0x23:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_3;
			break;
		case 0x24:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_4;
			break;
		case 0x25:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_5;
			break;
		case 0x26:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_6;
			break;
		case 0x27:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_7;
			break;
		case 0x28:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_8;
			break;
		case 0x29:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_9;
			break;
		case 0x2A:
			ctrlMode = E_GCP_ControlMode.ILLU_DIMMING_10;
			break;
		case (byte) 0xff:
			ctrlMode = E_GCP_ControlMode.DEFAULT;
			break;
		case (byte) 0x0A:
			ctrlMode = E_GCP_ControlMode.DEFAULT;
			break;
		default:
			ctrlMode = E_GCP_ControlMode.DEFAULT;
			MyLog.e("ctrl Mode Ʋ��, " + HexUtils.toHexString(value));
			// throw new UnformattedPacketException("ctrl Mode Ʋ��, "
			// + HexUtils.toHexString(value));
		}

		return ctrlMode;
	}
}
