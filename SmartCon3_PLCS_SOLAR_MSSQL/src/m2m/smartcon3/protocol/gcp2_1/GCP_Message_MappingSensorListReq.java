package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_MappingSensorListReq extends GCP_Message {
	short subNodeId;
	short lcId;
	int numOfSensors;
	short sensorIds[];
	int circuitId;
	int onTime;
	E_GCP_ControlMode onDimmingCtrl;
	E_GCP_ControlMode offDimmingCtrl;
	
	public GCP_Message_MappingSensorListReq(){
		msgType = E_GCP_MessageType.REQ_MAPPING_SENSOR_LIST;	
	}
	
	public GCP_Message_MappingSensorListReq(short srcId, short destId, short subNodeId,
			short lcId, int numOfSensors, short sensorId[], int circuitId, int onTime,
			E_GCP_ControlMode onDimmingCtrl,
			E_GCP_ControlMode offDimmingCtrl)
			throws UnformattedPacketException {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.lcId = lcId;
		this.numOfSensors = numOfSensors;
		this.sensorIds = sensorId;
		this.circuitId = circuitId;
		this.onTime = onTime;
		this.onDimmingCtrl = onDimmingCtrl;
		this.offDimmingCtrl = offDimmingCtrl;

		if (numOfSensors < 0 || numOfSensors > 20)
			throw new UnformattedPacketException("센서 수 잘못됨, " + numOfSensors);
		if (sensorId == null)
			throw new UnformattedPacketException("센서Id 리스트 잘못됨");
		if (sensorId.length != numOfSensors)
			throw new UnformattedPacketException("입력된 센서 수와 센서 id리스트의 크기가 다름");

		int len = 0;
		byte data_bytes[] = new byte[10+numOfSensors*2];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(lcId >> 8).byteValue();
		data_bytes[len++] = new Integer(lcId).byteValue();
		data_bytes[len++] = new Integer(numOfSensors).byteValue();
		for(int i=0; i < numOfSensors; i++){
			data_bytes[len++] = new Integer(sensorIds[i] >> 8).byteValue();
			data_bytes[len++] = new Integer(sensorIds[i]).byteValue();
		}
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = new Integer(onTime).byteValue();
		data_bytes[len++] = onDimmingCtrl.toByte();
		data_bytes[len++] = offDimmingCtrl.toByte();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_MappingSensorListReq(byte[] packet, int size)
			throws UnformattedPacketException {		
		this();
		byte[] payload_bytes = super.getPayload(packet);
		
		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));		
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		lcId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])));
		numOfSensors = HexUtils.toInt(HexUtils.toHexString(payload_bytes[5]));

		if (payload_bytes.length!= 10 + numOfSensors * 2)
			throw new UnformattedPacketException("패킷 길이 틀림, size: " + size);

		sensorIds = new short[numOfSensors];
		for (int i = 0; i < numOfSensors; i++)
			sensorIds[i] = (short) (HexUtils.toInt(HexUtils
					.toHexString(payload_bytes[6 + i * 2]))
					* 256
					+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[7 + i * 2])));

		circuitId = HexUtils.toInt(HexUtils.toHexString(payload_bytes[6+2*numOfSensors]));
		onTime = HexUtils.toInt(HexUtils.toHexString(payload_bytes[7+2*numOfSensors]));

		onDimmingCtrl = E_GCP_ControlMode.getFromByte(payload_bytes[8+2*numOfSensors]);
		offDimmingCtrl = E_GCP_ControlMode.getFromByte(payload_bytes[9+2*numOfSensors]);
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}
	
	public int getNumOfSensors() {
		return numOfSensors;
	}

	public short[] getSensorIds() {
		return sensorIds;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public int getOnTime() {
		return onTime;
	}

	public E_GCP_ControlMode getOnDimmingCtrl() {
		return onDimmingCtrl;
	}

	public E_GCP_ControlMode getOffDimmingCtrl() {
		return offDimmingCtrl;
	}
	
	public short getLcId() {
		return lcId;
	}

	public static void main(String args[]) throws UnformattedPacketException {
		GCP_Message_MappingSensorListReq deviceInfoReqMsg = new GCP_Message_MappingSensorListReq(
				(short)0x5212, (short)0x1234, (short)0x4321,(short)0x4321, 2, new short[]{1, 2}, 10, 20, 
				E_GCP_ControlMode.DIMMING_3,
				E_GCP_ControlMode.DIMMING_8);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_MappingSensorListReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
