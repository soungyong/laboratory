package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_SetSensingLevelReq extends GCP_Message {
	
	short subNodeId;	
	short sensorId;
	int level;

	public GCP_Message_SetSensingLevelReq(){
		msgType = E_GCP_MessageType.REQ_SET_SENSING_LEVEL;
	}
	public GCP_Message_SetSensingLevelReq(short srcId, short destId, short subNodeId, short sensorId, int level) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.level = level;
		this.sensorId = sensorId;

		int len = 0;
		byte data_bytes[] = new byte[6];	
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();		
		data_bytes[len++] = new Integer(sensorId >> 8).byteValue();
		data_bytes[len++] = new Integer(sensorId).byteValue();
		data_bytes[len++] = new Integer(level).byteValue();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_SetSensingLevelReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=6)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));		
		sensorId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])) * 256
		+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])));	
		level = HexUtils.toInt(HexUtils.toHexString(payload_bytes[5]));
	}


	public short getSubNodeId() {
		return subNodeId;
	}

	public int getDataFormat() {
		return level;
	}

	public short getSensorId() {
		return sensorId;
	}
	
	public int getLevel() {
		return level;
	}
	public static void main(String args[]) {				
		GCP_Message_SetSensingLevelReq deviceInfoReqMsg = new GCP_Message_SetSensingLevelReq(
				(short)0x5412, (short)0x1234, (short)0x4321, (short)0x1234, 0x01);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_SetSensingLevelReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
