package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_DimmerMappingInfoRes extends GCP_Message {

	short subNodeId;
	short dimmerId;
	int channleId;
	int circuitId;

	public GCP_Message_DimmerMappingInfoRes() {
		msgType = E_GCP_MessageType.RES_DIMMER_MAPPING_INFO;
	}

	public GCP_Message_DimmerMappingInfoRes(short srcId, short destId,
			short subNodeId, short dimmerId, int channelId, int circuitId) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.dimmerId = dimmerId;
		this.channleId = channelId;
		this.circuitId = circuitId;

		int len = 0;
		byte data_bytes[] = new byte[7];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(dimmerId >> 8).byteValue();
		data_bytes[len++] = new Integer(dimmerId).byteValue();
		data_bytes[len++] = new Integer(channleId).byteValue();
		data_bytes[len++] = new Integer(circuitId).byteValue();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_DimmerMappingInfoRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 7)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		subNodeId = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[1])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[2])));

		dimmerId = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[3])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[4])));
		channleId = HexUtils.toInt(HexUtils.toHexString(payload_bytes[5]));
		circuitId = HexUtils.toInt(HexUtils.toHexString(payload_bytes[6]));
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public short getDimmerId() {
		return dimmerId;
	}

	public int getCircuitId() {
		return circuitId;
	}
	
	public int getChannleId() {
		return channleId;
	}

	public static void main(String args[]) {
		GCP_Message_DimmerMappingInfoRes deviceInfoReqMsg = new GCP_Message_DimmerMappingInfoRes(
				(short) 0x2142, (short) 0x1234, (short) 0x4321, (short) 0x2132, 0,
				10);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_DimmerMappingInfoRes(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
