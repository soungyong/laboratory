package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_SensorMappingInfoRes extends GCP_Message {

	short subNodeId;
	short lcId;
	short sensorId;
	int circuitId;
	int onTime;
	E_GCP_ControlMode onDimmingCtrl;
	E_GCP_ControlMode offDimmingCtrl;

	public GCP_Message_SensorMappingInfoRes() {
		msgType = E_GCP_MessageType.RES_SENSOR_MAPPING_INFO;
	}

	public GCP_Message_SensorMappingInfoRes(short srcId, short destId,
			short subNodeId, short lcId, short sensorId, int circuitId, int onTime,
			E_GCP_ControlMode onDimmingCtrl, E_GCP_ControlMode offDimmingCtrl) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.lcId = lcId;
		this.sensorId = sensorId;
		this.circuitId = circuitId;
		this.onTime = onTime;
		this.onDimmingCtrl = onDimmingCtrl;
		this.offDimmingCtrl = offDimmingCtrl;

		int len = 0;
		byte data_bytes[] = new byte[11];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(lcId >> 8).byteValue();
		data_bytes[len++] = new Integer(lcId).byteValue();
		data_bytes[len++] = new Integer(sensorId >> 8).byteValue();
		data_bytes[len++] = new Integer(sensorId).byteValue();
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = new Integer(onTime).byteValue();
		data_bytes[len++] = onDimmingCtrl.toByte();
		data_bytes[len++] = offDimmingCtrl.toByte();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_SensorMappingInfoRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 11)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		subNodeId = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[1])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[2])));
		lcId = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[3])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[4])));
		sensorId = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[5])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[6])));
		circuitId = HexUtils.toInt(HexUtils.toHexString(payload_bytes[7]));
		onTime = HexUtils.toInt(HexUtils.toHexString(payload_bytes[8]));
		onDimmingCtrl = E_GCP_ControlMode.getFromByte(payload_bytes[9]);
		offDimmingCtrl = E_GCP_ControlMode.getFromByte(payload_bytes[10]);
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public short getSensorId() {
		return sensorId;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public int getOnTime() {
		return onTime;
	}

	public E_GCP_ControlMode getOnDimmingCtrl() {
		return onDimmingCtrl;
	}

	public E_GCP_ControlMode getOffDimmingCtrl() {
		return offDimmingCtrl;
	}

	
	public short getLcId() {
		return lcId;
	}

	public static void main(String args[]) {
		GCP_Message_SensorMappingInfoRes deviceInfoReqMsg = new GCP_Message_SensorMappingInfoRes(
				(short) 0x5123, (short) 0x1234, (short) 0x4321, (short) 0x4321, (short) 0x2132,
				10, 10, E_GCP_ControlMode.DIMMING_3,
				E_GCP_ControlMode.DIMMING_8);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_SensorMappingInfoRes(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
