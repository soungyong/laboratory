package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_TiltControlReq extends GCP_Message {

	short subNodeId;
	String command;

	public GCP_Message_TiltControlReq() {
		msgType = E_GCP_MessageType.REQ_TILT_CONTROL;
	}

	public GCP_Message_TiltControlReq(short srcId, short destId, short subNodeId, int len, String command) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.command = command;

		int buffLen = 0;
		byte data_bytes[] = new byte[20];

		data_bytes[buffLen++] = msgType.toByte();
		data_bytes[buffLen++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[buffLen++] = new Integer(subNodeId).byteValue();

		for (int i = 0; i < len; i++) {
			data_bytes[buffLen++] = (byte) command.charAt(i);
		}
		getPacketBytes(data_bytes);
	}

	public GCP_Message_TiltControlReq(byte[] packet, int size) throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 5)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, " + HexUtils.toHexString(messageType));

		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256 + HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public static void main(String args[]) {
	}
}
