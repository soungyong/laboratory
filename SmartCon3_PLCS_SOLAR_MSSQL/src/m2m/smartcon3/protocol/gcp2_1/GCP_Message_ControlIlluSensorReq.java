package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_ControlIlluSensorReq extends GCP_Message {
	
	short subNodeId;
	int circuitId;
	int onTime;
	short onIlluLevel;
	short offIlluLevel;

	public GCP_Message_ControlIlluSensorReq(){
		msgType = E_GCP_MessageType.REQ_CONTROL_ILLU_SENSOR;		
	}
	
	public GCP_Message_ControlIlluSensorReq(short srcId, short destId, short subNodeId,
			int circuitId, int onTime, short onIlluLevel,
			short offIlluLevel) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.circuitId = circuitId;
		this.onTime = onTime;
		this.onIlluLevel = onIlluLevel;
		this.offIlluLevel = offIlluLevel;

		int len = 0;
		byte data_bytes[] = new byte[9];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = new Integer(onTime).byteValue();
		data_bytes[len++] = new Short((short) (onIlluLevel>>8)).byteValue();
		data_bytes[len++] = new Short(onIlluLevel).byteValue();
		data_bytes[len++] = new Short((short) (offIlluLevel>>8)).byteValue();
		data_bytes[len++] = new Short(offIlluLevel).byteValue();
		
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_ControlIlluSensorReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=9)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		circuitId = new Integer(payload_bytes[3]);

		if (circuitId < 0 || circuitId > 15)
			throw new UnformattedPacketException("Circuit ID 틀림, "
					+ HexUtils.toHexString(circuitId));
		
		onTime = new Integer(payload_bytes[4]);

		onIlluLevel = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[6])));
		offIlluLevel = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[7])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[8])));
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public int getOnTime() {
		return onTime;
	}

	public short getOnIlluLevel() {
		return onIlluLevel;
	}

	public short getOffIlluLevel() {
		return offIlluLevel;
	}

	public static void main(String args[]) {
		GCP_Message_ControlIlluSensorReq deviceInfoReqMsg = new GCP_Message_ControlIlluSensorReq(
				(short)0x1234, (short)0x2312, (short)0x4321, 1, 10, (short)400, (short)1000);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_ControlIlluSensorReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
