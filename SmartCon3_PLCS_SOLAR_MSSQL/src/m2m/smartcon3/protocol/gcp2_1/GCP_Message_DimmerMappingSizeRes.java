package m2m.smartcon3.protocol.gcp2_1;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_DimmerMappingSizeRes extends GCP_Message {
	
	
	short subNodeId;
	int numOfDimmerMapping;

	public GCP_Message_DimmerMappingSizeRes(){
		msgType = E_GCP_MessageType.RES_DIMMER_MAPPING_SIZE;	
	}
	public GCP_Message_DimmerMappingSizeRes(short srcId, short destId, short subNodeId,
			int numOfDimmerMapping) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.numOfDimmerMapping = numOfDimmerMapping;
		
		int len = 0;
		byte data_bytes[] = new byte[4];
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(numOfDimmerMapping).byteValue();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_DimmerMappingSizeRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=4)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		numOfDimmerMapping = HexUtils.toInt(HexUtils.toHexString(payload_bytes[3]));
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public int getNumOfMapping() {
		return numOfDimmerMapping;
	}

	public static void main(String args[]) {
		GCP_Message_DimmerMappingSizeRes numOfDeviceResMsg = new GCP_Message_DimmerMappingSizeRes(
				(short)0x4212, (short)0x1234, (short)0x4321, 10);
		byte[] data = numOfDeviceResMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			numOfDeviceResMsg = new GCP_Message_DimmerMappingSizeRes(data,
					data.length);
			data = numOfDeviceResMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}				
	}
}
