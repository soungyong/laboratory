package m2m.smartcon3.protocol.ncp1_0;

/**
 * @version v2.0
 * @author funface2
 * 
 */
public enum E_NCP_MessageType {
	REQ_PING((byte) 0x01), 
	RES_PING((byte) 0x02), 
	REQ_REIGSTER_BROADCAST((byte) 0x05), 
	ASSIGN_TOKEN((byte) 0x06), 
	REQ_TOKEN_RELEASE((byte) 0x07), 
	RES_TOKEN_RELEASE((byte) 0x08), 
	REQ_REGISTER((byte) 0x11), 
	RES_REGISTER((byte) 0x12),
	REQ_REGISTER_NODE((byte) 0x13), 
	RES_REGISTER_NODE((byte) 0x14),
	SEND_TIME_INFO((byte) 0x15);

	private byte byteValue=0;

	private E_NCP_MessageType() {
	}

	private E_NCP_MessageType(byte value) {
		this.byteValue = value;
	}
	
	public byte toByte() {
		return byteValue;
	}
}
