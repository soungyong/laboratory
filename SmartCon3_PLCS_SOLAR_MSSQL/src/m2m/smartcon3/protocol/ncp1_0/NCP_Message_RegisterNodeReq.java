package m2m.smartcon3.protocol.ncp1_0;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class NCP_Message_RegisterNodeReq extends NCP_Message {
	short nodeId;
	short subNodeId;
	short deviceId;
	short deviceType;
	int deviceVersion;
	int firmwareVersion;
	int netAddr;

	public NCP_Message_RegisterNodeReq() {
		msgType = E_NCP_MessageType.REQ_REGISTER_NODE;
	}

	public NCP_Message_RegisterNodeReq(short nodeId, short subNodeId,
			short deviceType, short deviceId, int deviceVersion,
			int firmwareVersion, int netAddr) {
		this();

		this.nodeId = nodeId;
		this.subNodeId = subNodeId;
		this.deviceType = deviceType;
		this.deviceId = deviceId;
		this.deviceVersion = deviceVersion;
		this.firmwareVersion = firmwareVersion;
		this.netAddr = netAddr;

		int len = 0;
		byte payload_bytes[];

		if (deviceType == 0x2010) {
			netAddr = 0x00;
			deviceId = 0;
			payload_bytes = new byte[10];
		} else {
			payload_bytes = new byte[14];
		}

		payload_bytes[len++] = msgType.toByte();
		payload_bytes[len++] = getSeqNumGenerator();
		payload_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		payload_bytes[len++] = new Integer(nodeId).byteValue();
		payload_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		payload_bytes[len++] = new Integer(subNodeId).byteValue();
		payload_bytes[len++] = new Integer(deviceType >> 8).byteValue();
		payload_bytes[len++] = new Integer(deviceType).byteValue();
		if (deviceType == 0x2010) {
			payload_bytes[len++] = new Integer(deviceVersion).byteValue();
			payload_bytes[len++] = new Integer(firmwareVersion).byteValue();
		} else {
			payload_bytes[len++] = new Integer(deviceId >> 8).byteValue();
			payload_bytes[len++] = new Integer(deviceId).byteValue();
			payload_bytes[len++] = new Integer(deviceVersion).byteValue();
			payload_bytes[len++] = new Integer(firmwareVersion).byteValue();
			payload_bytes[len++] = new Integer(netAddr >> 8).byteValue();
			payload_bytes[len++] = new Integer(netAddr).byteValue();
		}

		getPacketBytes(payload_bytes);
	}

	public NCP_Message_RegisterNodeReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);

		if (payload_bytes.length != 10 && payload_bytes.length != 14 && payload_bytes.length != 8)
			throw new UnformattedPacketException("패킷 길이 틀림, size: " + size);

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		nodeId = (short) (HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[2])) * 256 + HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[3])));
		subNodeId = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[4])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[5])));
		deviceType = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[6])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[7])));
		if (deviceType == 0x2010) {
			deviceVersion = payload_bytes[8];
			firmwareVersion = payload_bytes[9];
		} else if (deviceType == 0x2040 || deviceType == 0x2050) {
			deviceId = (short) (HexUtils.toInt(HexUtils
					.toHexString(payload_bytes[8])) * 256 + HexUtils
					.toInt(HexUtils.toHexString(payload_bytes[9])));
			deviceVersion = payload_bytes[10];
			firmwareVersion = payload_bytes[11];
			netAddr = HexUtils.toInt(HexUtils.toHexString(payload_bytes[12]))
					* 256
					+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[13]));
		} else if (deviceType == 0x2090) {
		} else
			throw new UnformattedPacketException();
	}

	public short getDeviceType() {
		return deviceType;
	}

	public int getDeviceVersion() {
		return deviceVersion;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public short getSubNodeId() {
		return subNodeId;
	}

	public short getDeviceId() {
		return deviceId;
	}

	public int getNetAddr() {
		return netAddr;
	}

	public short getNodeId() {
		return nodeId;
	}

	public static void main(String args[]) {
		NCP_Message_RegisterNodeReq registerReqMsg = new NCP_Message_RegisterNodeReq(
				(short) 0x1234, (short) 0x2345, (short) 0x2010, (short) 0x5678,
				0x11, 0x12, 0x9987);
		byte[] data = registerReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			registerReqMsg = new NCP_Message_RegisterNodeReq(data, data.length);
			data = registerReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		registerReqMsg = new NCP_Message_RegisterNodeReq((short) 0x1234,
				(short) 0x3456, (short) 0x2345, (short) 0x2050, 0x11, 0x12,
				0x9987);
		data = registerReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			registerReqMsg = new NCP_Message_RegisterNodeReq(data, data.length);
			data = registerReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
