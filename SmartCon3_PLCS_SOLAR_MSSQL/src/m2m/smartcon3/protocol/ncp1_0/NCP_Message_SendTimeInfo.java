package m2m.smartcon3.protocol.ncp1_0;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class NCP_Message_SendTimeInfo extends NCP_Message {
	
	short nodeId;
	int dataFormat;
	String dateTimeString;
	
	public NCP_Message_SendTimeInfo(){
		msgType = E_NCP_MessageType.SEND_TIME_INFO;
	}

	public NCP_Message_SendTimeInfo(short nodeId, int dataFormat, String dateTimeString) throws UnformattedPacketException {
		this();
		
		this.nodeId = nodeId;		
		if(dateTimeString.length()!=15) throw new UnformattedPacketException();		
		this.dataFormat = dataFormat;
		this.dateTimeString = dateTimeString;
		
		int len=0;		
		byte data_bytes[] = new byte[20];		
		data_bytes[len++] = msgType.toByte();				
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();
		data_bytes[len++] = new Integer(dataFormat).byteValue();
		for(int i=0; i < 15; i++)
			data_bytes[len++] = (byte) dateTimeString.charAt(i);	
		
		getPacketBytes(data_bytes);
	}

	public NCP_Message_SendTimeInfo(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=20)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));
		dataFormat = payload_bytes[4];
		dateTimeString="";
		for (int i = 0; i < 15; i++)
			dateTimeString += (char) payload_bytes[5 + i];
	}

	public int getDataFormat() {
		return dataFormat;
	}

	public String getDateTimeString() {
		return dateTimeString;
	}
	
	public short getNodeId() {
		return nodeId;
	}

	public static void main(String args[]) throws UnformattedPacketException {
		NCP_Message_SendTimeInfo sendTimeInfoMsg = new NCP_Message_SendTimeInfo(
				(short)0x1234, 0x01, "201208141142105");
		byte[] data = sendTimeInfoMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");
		
		try {
			sendTimeInfoMsg = new NCP_Message_SendTimeInfo(data, data.length);
			data = sendTimeInfoMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
