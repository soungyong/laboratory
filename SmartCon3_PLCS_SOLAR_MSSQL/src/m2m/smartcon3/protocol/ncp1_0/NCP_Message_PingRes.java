package m2m.smartcon3.protocol.ncp1_0;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class NCP_Message_PingRes extends NCP_Message {
	short nodeId;
	int protocolVersion;
	int flag;
	int pingInterval;

	public NCP_Message_PingRes() {
		msgType = E_NCP_MessageType.RES_PING;
	}

	public NCP_Message_PingRes(short nodeId, int protocolVersion, int flag,
			int pingInterval) {
		this();
		this.nodeId = nodeId;
		this.protocolVersion = protocolVersion;
		this.flag = flag;
		this.pingInterval = pingInterval;

		int len = 0;
		byte payload_bytes[] = new byte[8];
		payload_bytes[len++] = msgType.toByte();
		payload_bytes[len++] = getSeqNumGenerator();
		payload_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		payload_bytes[len++] = new Integer(nodeId).byteValue();
		payload_bytes[len++] = new Integer(protocolVersion).byteValue();
		payload_bytes[len++] = new Integer(flag).byteValue();
		payload_bytes[len++] = new Integer(pingInterval >> 8).byteValue();
		payload_bytes[len++] = new Integer(pingInterval).byteValue();

		super.getPacketBytes(payload_bytes);
	}

	public NCP_Message_PingRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);

		if (payload_bytes.length != 8)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		this.nodeId = (short) (new Integer(payload_bytes[2]) * 256 + new Integer(
				payload_bytes[3]));

		protocolVersion = payload_bytes[4];
		flag = payload_bytes[5];
		this.pingInterval = HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[6]))
				* 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[7]));
	}

	public int getProtocolVersion() {
		return protocolVersion;
	}

	public int getFlag() {
		return flag;
	}

	public int getPingInterval() {
		return pingInterval;
	}

	public short getNodeId() {
		return nodeId;
	}

	public static void main(String args[]) {
		NCP_Message_PingRes pingResMsg = new NCP_Message_PingRes(new Short(
				(short) 0x1234), 0x12, 0x34, 0x0104);
		pingResMsg.setSeqNum((byte) 0x01);
		byte[] data = pingResMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			pingResMsg = new NCP_Message_PingRes(data, data.length);
			data = pingResMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
