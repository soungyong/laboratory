package m2m.smartcon3.protocol.ncp1_0;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class NCP_Message_RegisterReq extends NCP_Message {
		
	short nodeId;
	int deviceType;
	int deviceVersion;
	int firmwareVersion;
	
	NCP_Message_RegisterReq(){
		msgType = E_NCP_MessageType.REQ_REGISTER;
	}
	
	public NCP_Message_RegisterReq(short nodeId, int deviceType,
			int deviceVersion, int firmwareVersion) {
		this();
		
		this.nodeId = nodeId;
		this.deviceType = deviceType;
		this.deviceVersion = deviceVersion;
		this.firmwareVersion = firmwareVersion;

		int len = 0;
		byte data_bytes[] = new byte[8];
		data_bytes[len++] = msgType.toByte();	
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();
		data_bytes[len++] = new Integer(deviceType >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceType).byteValue();
		data_bytes[len++] = new Integer(deviceVersion).byteValue();
		data_bytes[len++] = new Integer(firmwareVersion).byteValue();

		getPacketBytes(data_bytes);
	}

	public NCP_Message_RegisterReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		
		if (payload_bytes.length != 8)
			throw new UnformattedPacketException("패킷 길이 틀림, size: " + size);

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
		+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));
		deviceType = HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[5]));
		deviceVersion = payload_bytes[6];
		firmwareVersion = payload_bytes[7];
	}
	
	public int getDeviceType() {
		return deviceType;
	}

	public int getDeviceVersion() {
		return deviceVersion;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}
	
	

	public short getNodeId() {
		return nodeId;
	}

	public static void main(String args[]) {
		NCP_Message_RegisterReq reigsterReqMsg = new NCP_Message_RegisterReq(
				(short)0x1234, 0x2050, 0x14, 0x12);
		byte[] data = reigsterReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			reigsterReqMsg = new NCP_Message_RegisterReq(data, data.length);
			data = reigsterReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
