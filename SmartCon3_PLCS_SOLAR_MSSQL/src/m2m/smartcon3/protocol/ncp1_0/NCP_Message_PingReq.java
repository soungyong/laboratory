package m2m.smartcon3.protocol.ncp1_0;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class NCP_Message_PingReq extends NCP_Message {
	short nodeId;
	int protocolVersion;
	int flag;
	
	public NCP_Message_PingReq(){
		msgType = E_NCP_MessageType.REQ_PING;
	}

	public NCP_Message_PingReq(short srcId, int protocolVersion, int flag) {
		this();		
		this.nodeId = srcId;		
		this.protocolVersion = protocolVersion;
		this.flag = flag;

		int len = 0;
		byte payload_byte[] = new byte[6];
		payload_byte[len++] = msgType.toByte();		
		payload_byte[len++] = getSeqNumGenerator();
		payload_byte[len++] = new Integer(srcId>>8).byteValue();
		payload_byte[len++] = new Integer(srcId).byteValue();
		payload_byte[len++] = new Integer(protocolVersion).byteValue();
		payload_byte[len++] = new Integer(flag).byteValue();
		
		getPacketBytes(payload_byte);
	}

	public NCP_Message_PingReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload = super.getPayload(packet);
		if (payload.length != 6)
			throw new UnformattedPacketException("패킷 길이 틀림, size: " + size);
				
		byte messageType = payload[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		nodeId = (short) (new Integer(payload[2])*256 + new Integer(payload[3]));		
		protocolVersion = payload[4];
		flag = payload[5];
	}

	public int getProtocolVersion() {
		return protocolVersion;
	}

	public int getFlag() {
		return flag;
	}
	
	public short getNodeId() {
		return nodeId;
	}

	public static void main(String args[]) {
		NCP_Message_PingReq pingReqMsg = new NCP_Message_PingReq(
				(short)0x1234, 0x55, 0x29);
		pingReqMsg.setSeqNum((byte)0x03);
		byte[] data = pingReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			pingReqMsg = new NCP_Message_PingReq(data, data.length);
			data = pingReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
