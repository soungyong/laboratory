package m2m.smartcon3.protocol.ncp1_0;

public interface I_NCP_Message {	
	static final byte NCP1_0=0x00;
	public byte[] toBytes();
	public E_NCP_MessageType getMessageType();
	public int getLength();
	public byte getChecksum();
	public byte getPid();
	public byte getSubPid();
	public short getSeqNum();
	public byte getSeqNumGenerator();
	public void setSeqNum(short seqNum);
	public short getNodeId();
}
