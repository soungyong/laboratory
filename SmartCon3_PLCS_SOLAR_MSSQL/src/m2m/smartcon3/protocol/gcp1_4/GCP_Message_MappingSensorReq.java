package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.util.HexUtils;

public class GCP_Message_MappingSensorReq extends GCP_Message {		
	short nodeId;
	short sensorId;
	int circuitId;
	int onTime;	
	E_GCP_ControlMode offDimmingCtrl;

	public GCP_Message_MappingSensorReq(){
		msgType = E_GCP_MessageType.REQ_MAPPING_SENSOR;	
	}
	
	public GCP_Message_MappingSensorReq(short nodeId, 
			short sensorId, int circuitId, int onTime,			
			E_GCP_ControlMode offDimmingCtrl) {
		this();
		this.nodeId = nodeId;
		this.sensorId = sensorId;
		this.circuitId = circuitId;
		this.onTime = onTime;
		this.offDimmingCtrl = offDimmingCtrl;

		int len = 0;
		byte data_bytes[] = new byte[9];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();		
		data_bytes[len++] = new Integer(sensorId >> 8).byteValue();
		data_bytes[len++] = new Integer(sensorId).byteValue();
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = new Integer(onTime).byteValue();
		//data_bytes[len++] = offDimmingCtrl.toByte();
		if(offDimmingCtrl == E_GCP_ControlMode.OFF || offDimmingCtrl == E_GCP_ControlMode.ON)
			data_bytes[len++] = (byte)0xff;
		else if(offDimmingCtrl == E_GCP_ControlMode.SENSOR){
			data_bytes[len++] = 0x01;
		}
		else{
			data_bytes[len++]=(byte) (offDimmingCtrl.toByte()-0x10);				
		}
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_MappingSensorReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=9)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));		
		sensorId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])));		
		circuitId = HexUtils.toInt(HexUtils.toHexString(payload_bytes[6]));
		onTime = HexUtils.toInt(HexUtils.toHexString(payload_bytes[7]));
		offDimmingCtrl = E_GCP_ControlMode.getFromByte(payload_bytes[8]);
	}
	
	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public short getSensorId() {
		return sensorId;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public int getOnTime() {
		return onTime;
	}

	public E_GCP_ControlMode getOffDimmingCtrl() {
		return offDimmingCtrl;
	}

	public short getNodeId() {
		return nodeId;
	}

	public static void main(String args[]) {
		GCP_Message_MappingSensorReq deviceInfoReqMsg = new GCP_Message_MappingSensorReq(
				(short)0x1234, (short)0x2132, 10, 10, E_GCP_ControlMode.DIMMING_3);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_MappingSensorReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
