package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.util.HexUtils;

public class GCP_Message_NoticeEvent extends GCP_Message {
	short nodeId;	
	int dataFormat;
	short sensorId;
	
	E_GCP_ControlMode controlState[] = new E_GCP_ControlMode[8];

	public GCP_Message_NoticeEvent(){
		msgType = E_GCP_MessageType.NOTICE_EVENT;	
	}
	public GCP_Message_NoticeEvent(short nodeId, int dataFormat, short sensorId, E_GCP_ControlMode controlState[]) {
		this();
		this.nodeId = nodeId;
		this.dataFormat = dataFormat;
		this.sensorId = sensorId;
		this.controlState = controlState;

		int len = 0;
		byte data_bytes[] = new byte[15];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();			
		data_bytes[len++] = new Integer(dataFormat).byteValue();
		data_bytes[len++] = new Integer(sensorId >> 8).byteValue();
		data_bytes[len++] = new Integer(sensorId).byteValue();
		for(int i=0; i < 8; i++)
			data_bytes[len++] = controlState[i].toByte();
		getPacketBytes(data_bytes);

	}

	public GCP_Message_NoticeEvent(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=15)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));		
		dataFormat = HexUtils.toInt(HexUtils.toHexString(payload_bytes[4]));
		sensorId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])) * 256
		+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[6])));	
		for(int i=0; i < 8; i++)
			controlState[i] = E_GCP_ControlMode.getFromByte(payload_bytes[7+i]);
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public int getDataFormat() {
		return dataFormat;
	}

	public short getSensorId() {
		return sensorId;
	}
	
	public short getNodeId() {
		return nodeId;
	}
		
	public E_GCP_ControlMode[] getControlState() {
		return controlState;
	}
	public static void main(String args[]) {				
		E_GCP_ControlMode controlState[] = new E_GCP_ControlMode[8];
		for(int i=0; i < 8; i++)
			controlState[i] = E_GCP_ControlMode.OFF;
		GCP_Message_NoticeEvent deviceInfoReqMsg = new GCP_Message_NoticeEvent(
				(short)0x4321, 0x01, (short)0x1234, controlState);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_NoticeEvent(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
