package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.util.HexUtils;

public class GCP_Message_SetSensingLevelRes extends GCP_Message {
	short nodeId;	
	short sensorId;
	int level;

	public GCP_Message_SetSensingLevelRes(){
		msgType = E_GCP_MessageType.RES_SET_SENSING_LEVEL;	
	}
	
	public GCP_Message_SetSensingLevelRes(short nodeId, short sensorId, int level) {
		this();
		this.nodeId = nodeId;
		this.sensorId = sensorId;
		this.level = level;

		int len = 0;
		byte data_bytes[] = new byte[7];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();				
		data_bytes[len++] = new Integer(sensorId >> 8).byteValue();
		data_bytes[len++] = new Integer(sensorId).byteValue();
		data_bytes[len++] = new Integer(level).byteValue();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_SetSensingLevelRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=7)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));		
		sensorId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])));
		level = HexUtils.toInt(HexUtils.toHexString(payload_bytes[6]));		
	}
	
	public short getSensorId() {
		return sensorId;
	}


	public short getNodeId() {
		return nodeId;
	}
	
	

	public int getLevel() {
		return level;
	}

	public static void main(String args[]) {
		GCP_Message_SetSensingLevelRes deviceInfoReqMsg = new GCP_Message_SetSensingLevelRes(
				(short)0x4321, (short)0x0001,10);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_SetSensingLevelRes(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
