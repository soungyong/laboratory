package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_PowermeterReq extends GCP_Message {
	short nodeId;

	public GCP_Message_PowermeterReq() {
		msgType = E_GCP_MessageType.REQ_POWERMETER;
	}

	public GCP_Message_PowermeterReq(short nodeId) {
		this();
		this.nodeId = nodeId;

		int len = 0;
		byte data_bytes[] = new byte[4];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();		

		getPacketBytes(data_bytes);
	}

	public GCP_Message_PowermeterReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 4)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		nodeId = (short) (HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[2])) * 256 + HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[3])));		
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public short getNodeId() {
		return nodeId;
	}

	public static void main(String args[]) {
		GCP_Message_PowermeterReq deviceInfoReqMsg = new GCP_Message_PowermeterReq(
				 (short) 0x4321);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_PowermeterReq(data, data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
