package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.util.HexUtils;

public class GCP_Message_MappingDimmerRes extends GCP_Message {
	short nodeId;
	short dimmerId;
	int circuitId;
	E_GCP_Result result;

	public GCP_Message_MappingDimmerRes(){
		msgType = E_GCP_MessageType.RES_MAPPING_DIMMER;	
	}
	public GCP_Message_MappingDimmerRes(short nodeId, short dimmerId, int circuitId,
			E_GCP_Result result) {
		this();
		this.nodeId = nodeId;
		this.dimmerId = dimmerId;
		this.circuitId = circuitId;
		this.result = result;

		int len = 0;
		byte data_bytes[] = new byte[8];		
		data_bytes[len++] = msgType.toByte();	
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();
		data_bytes[len++] = new Integer(dimmerId >> 8).byteValue();
		data_bytes[len++] = new Integer(dimmerId).byteValue();
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = result.toByte();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_MappingDimmerRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=8)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));
		dimmerId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])));
		circuitId = payload_bytes[6];
		//result = E_GCP_Result.getFromByte(payload_bytes[7]);
		if(payload_bytes[7]==0x02){
			result = E_GCP_Result.FAIL_ALREADYMAPPED;
		}else
			result = E_GCP_Result.SUCCESS;		
	}

	public short getDimmerId() {
		return dimmerId;
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public E_GCP_Result getResult() {
		return result;
	}


	public short getNodeId() {
		return nodeId;
	}
	
	
	public int getCircuitId() {
		return circuitId;
	}
	public static void main(String args[]) {
		GCP_Message_MappingDimmerRes deviceInfoReqMsg = new GCP_Message_MappingDimmerRes(
				(short)0x5312, (short)0x4321, 0, E_GCP_Result.SUCCESS);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_MappingDimmerRes(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
