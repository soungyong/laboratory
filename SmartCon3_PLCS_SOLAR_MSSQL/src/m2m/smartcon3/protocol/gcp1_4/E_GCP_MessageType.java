package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

/**
 * @version v2.1
 * @author funface2
 * 
 */

public enum E_GCP_MessageType {	
	REQ_STATEINFO((byte) 0x36),
	RES_STATEINFO((byte) 0x37),
	
	 REQ_CONTROL_CIRCUIT((byte) 0x30),
	RES_CONTROL_CIRCUIT((byte) 0x31),	
	
	REQ_POWERMETER((byte) 0x34),
	RES_POWERMETER((byte) 0x35),
	REQ_UPDATEPOWERMETER((byte) 0x44),
	
	REQ_MAPPING_SENSOR((byte) 0x43),
	RES_MAPPING_SENSOR((byte) 0x47),	
	REQ_RESET_MAPPING_SENSOR((byte)0x80),
	
	REQ_MAPPING_DIMMER((byte) 0x45),
	RES_MAPPING_DIMMER((byte) 0x46),	
	REQ_RESET_MAPPING_DIMMER((byte)0x81),
	
	
	NOTICE_EVENT((byte)0x50),
	
	REQ_SET_SENSING_LEVEL((byte)0x62),
	RES_SET_SENSING_LEVEL((byte)0x63),
	
	REQ_DEBUG_LOG((byte)0x90),
	RES_DEBUG_LOG((byte)0x91),
	REQ_RESET_DEBUG_LOG((byte)0x92),
	REQ_DEBUG_LOG_LC((byte)0x93),
	RES_DEBUG_LOG_LC((byte)0x94),
	REQ_RESET_DEBUG_LOG_LC((byte)0x95),	
	DEFAULT((byte)0x00);	
	

	private byte byteValue=0x00;

	private E_GCP_MessageType() {
	}

	private E_GCP_MessageType(byte value) {
		this.byteValue = value;
	}

	public byte toByte() {
		return byteValue;
	}
	
	public static E_GCP_MessageType getFromByte(byte value) throws UnformattedPacketException {
		E_GCP_MessageType messageType = E_GCP_MessageType.DEFAULT;
		if(value==E_GCP_MessageType.REQ_STATEINFO.byteValue)
			messageType = E_GCP_MessageType.REQ_STATEINFO;
		else if(value==E_GCP_MessageType.RES_STATEINFO.byteValue)
			messageType = E_GCP_MessageType.RES_STATEINFO;
		else if(value==E_GCP_MessageType.REQ_CONTROL_CIRCUIT.byteValue)
			messageType = E_GCP_MessageType.REQ_CONTROL_CIRCUIT;
		else if(value==E_GCP_MessageType.RES_CONTROL_CIRCUIT.byteValue)
			messageType = E_GCP_MessageType.RES_CONTROL_CIRCUIT;
		else if(value==E_GCP_MessageType.REQ_POWERMETER.byteValue)
			messageType = E_GCP_MessageType.REQ_POWERMETER;
		else if(value==E_GCP_MessageType.RES_POWERMETER.byteValue)
			messageType = E_GCP_MessageType.RES_POWERMETER;
		else if(value==E_GCP_MessageType.REQ_UPDATEPOWERMETER.byteValue)
			messageType = E_GCP_MessageType.REQ_UPDATEPOWERMETER;
		else if(value==E_GCP_MessageType.REQ_MAPPING_SENSOR.byteValue)
			messageType = E_GCP_MessageType.REQ_MAPPING_SENSOR;
		else if(value==E_GCP_MessageType.RES_MAPPING_SENSOR.byteValue)
			messageType = E_GCP_MessageType.RES_MAPPING_SENSOR;
		else if(value==E_GCP_MessageType.REQ_RESET_MAPPING_SENSOR.byteValue)
			messageType = E_GCP_MessageType.REQ_RESET_MAPPING_SENSOR;
		else if(value==E_GCP_MessageType.REQ_MAPPING_DIMMER.byteValue)
			messageType = E_GCP_MessageType.REQ_MAPPING_DIMMER;
		else if(value==E_GCP_MessageType.RES_MAPPING_DIMMER.byteValue)
			messageType = E_GCP_MessageType.RES_MAPPING_DIMMER;
		else if(value==E_GCP_MessageType.REQ_RESET_MAPPING_DIMMER.byteValue)
			messageType = E_GCP_MessageType.REQ_RESET_MAPPING_DIMMER;
		else if(value==E_GCP_MessageType.NOTICE_EVENT.byteValue)
			messageType = E_GCP_MessageType.NOTICE_EVENT;
		else if(value==E_GCP_MessageType.REQ_SET_SENSING_LEVEL.byteValue)
			messageType = E_GCP_MessageType.REQ_SET_SENSING_LEVEL;
		else if(value==E_GCP_MessageType.RES_SET_SENSING_LEVEL.byteValue)
			messageType = E_GCP_MessageType.RES_SET_SENSING_LEVEL;
		else if(value==E_GCP_MessageType.REQ_DEBUG_LOG.byteValue)
			messageType = E_GCP_MessageType.REQ_DEBUG_LOG;
		else if(value==E_GCP_MessageType.RES_DEBUG_LOG.byteValue)
			messageType = E_GCP_MessageType.RES_DEBUG_LOG;
		else if(value==E_GCP_MessageType.REQ_RESET_DEBUG_LOG.byteValue)
			messageType = E_GCP_MessageType.REQ_RESET_DEBUG_LOG;
		else if(value==E_GCP_MessageType.REQ_DEBUG_LOG_LC.byteValue)
			messageType = E_GCP_MessageType.REQ_DEBUG_LOG_LC;
		else if(value==E_GCP_MessageType.RES_DEBUG_LOG_LC.byteValue)
			messageType = E_GCP_MessageType.RES_DEBUG_LOG_LC;
		else if(value==E_GCP_MessageType.REQ_RESET_DEBUG_LOG_LC.byteValue)
			messageType = E_GCP_MessageType.REQ_RESET_DEBUG_LOG_LC;
		
		return messageType;
	}
}
