package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.util.HexUtils;

public class GCP_Message_ControlCircuitRes extends GCP_Message {
	short nodeId;
	int circuitId;
	E_GCP_ControlMode ctrlMode;
	

	public GCP_Message_ControlCircuitRes() {
		msgType = E_GCP_MessageType.RES_CONTROL_CIRCUIT;
	}
	
	public GCP_Message_ControlCircuitRes(short nodeId, int circuitId, E_GCP_ControlMode ctrlMode) {
		this();
		this.nodeId = nodeId;
		this.circuitId = circuitId;
		this.ctrlMode = ctrlMode;

		int len = 0;
		byte data_bytes[] = new byte[6];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();		
		data_bytes[len++] = (byte) circuitId;
		data_bytes[len++] = ctrlMode.toByte();

		getPacketBytes(data_bytes);
	}

	public GCP_Message_ControlCircuitRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 6)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		

		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));

		circuitId = payload_bytes[4];
		ctrlMode = E_GCP_ControlMode.getFromByte(payload_bytes[5]);
	}

		
	public E_GCP_MessageType getMsgType() {
		return msgType;
	}
	
	public short getNodeId() {
		return nodeId;
	}
	
	public int getCircuitId() {
		return circuitId;
	}

	public E_GCP_ControlMode getCtrlMode() {
		return ctrlMode;
	}

	public static void main(String args[]) {
		GCP_Message_ControlCircuitRes deviceInfoReqMsg = new GCP_Message_ControlCircuitRes(
				(short)0x1234,  1, E_GCP_ControlMode.OFF);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_ControlCircuitRes(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
