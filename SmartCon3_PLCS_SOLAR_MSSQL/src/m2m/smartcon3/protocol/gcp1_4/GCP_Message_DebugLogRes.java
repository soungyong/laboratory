package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class GCP_Message_DebugLogRes extends GCP_Message {	
	short nodeId;
	byte currentTime[] = new byte[6];
	int rebootCount;
	byte lastRebootTime[] = new byte[5];
	int serverConnectionCount;
	byte[] lastServerConnectionTime = new byte[5];

	public GCP_Message_DebugLogRes(){
		msgType = E_GCP_MessageType.RES_DEBUG_LOG;	
	}
	
	public GCP_Message_DebugLogRes(short nodeId,  
			byte[] currentTime, int rebootCount, byte[] lastRebootTime,
			int serverConnctionCount, byte[] lastServerConnectionTime) {
		this();
		this.nodeId = nodeId;
		this.currentTime = currentTime;
		this.rebootCount = rebootCount;
		this.lastRebootTime = lastRebootTime;
		this.serverConnectionCount = serverConnctionCount;
		this.lastServerConnectionTime = lastServerConnectionTime;

		int len = 0;
		byte data_bytes[] = new byte[24];		
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();
		
		for(int i=0; i < 6; i++)
			data_bytes[len++] = currentTime[i];
		data_bytes[len++] = new Integer(rebootCount >> 8).byteValue();
		data_bytes[len++] = new Integer(rebootCount).byteValue();
		for(int i=0; i < 5; i++)
			data_bytes[len++] = lastRebootTime[i];
		data_bytes[len++] = new Integer(serverConnectionCount >> 8).byteValue();
		data_bytes[len++] = new Integer(serverConnectionCount).byteValue();
		for(int i=0; i < 5; i++)
			data_bytes[len++] = lastServerConnectionTime[i];
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_DebugLogRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=24)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));

		for (int i = 0; i < 6; i++)
			currentTime[i] = payload_bytes[4 + i];
		rebootCount = HexUtils.toInt(HexUtils.toHexString(payload_bytes[10])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[11]));
		for (int i = 0; i < 5; i++)
			lastRebootTime[i] = payload_bytes[12 + i];
		serverConnectionCount = HexUtils
				.toInt(HexUtils.toHexString(payload_bytes[17]))
				* 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[18]));
		for (int i = 0; i < 5; i++)
			lastServerConnectionTime[i] = payload_bytes[19 + i];
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public byte[] getCurrentTime() {
		return currentTime;
	}

	public int getRebootCount() {
		return rebootCount;
	}

	public byte[] getLastRebootTime() {
		return lastRebootTime;
	}

	public int getServerConnectionCount() {
		return serverConnectionCount;
	}

	public byte[] getLastServerConnectionTime() {
		return lastServerConnectionTime;
	}

	public short getNodeId() {
		return nodeId;
	}

	public static void main(String args[]) {
		GCP_Message_DebugLogRes deviceInfoReqMsg = new GCP_Message_DebugLogRes(
				(short)0x3212, new byte[]{12,8,16,17,51, 12}, 0x1234, new byte[]{12,8,16,17, 51}, 0x3456, new byte[]{12,8,16,17, 51});
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_DebugLogRes(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
