package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.util.HexUtils;

public class GCP_Message_ControlCircuitReq extends GCP_Message {
	short nodeId;
	int circuitId;
	E_GCP_ControlMode ctrlMode;
	byte dimmingLevel=(byte) 0xff;

	public GCP_Message_ControlCircuitReq()
	{
		msgType = E_GCP_MessageType.REQ_CONTROL_CIRCUIT;
	}
	
	public GCP_Message_ControlCircuitReq(short nodeId,
			int circuitId, E_GCP_ControlMode ctrlMode) {	
		this();
		this.nodeId = nodeId;		
		this.circuitId = circuitId;
		this.ctrlMode = ctrlMode;
		
		if(ctrlMode == E_GCP_ControlMode.OFF || ctrlMode == E_GCP_ControlMode.ON)
			dimmingLevel=(byte) 0xff;
		else if(ctrlMode == E_GCP_ControlMode.SENSOR){
			dimmingLevel=(byte) 0xff;
		}
		else{
			dimmingLevel=(byte) (ctrlMode.toByte()-0x10);
			ctrlMode = E_GCP_ControlMode.ON;			
		}
		

		int len = 0;
		byte data_bytes[] = new byte[7];

		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();		
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = ctrlMode.toByte();
		data_bytes[len++] = dimmingLevel;

		getPacketBytes(data_bytes);
	}

	public GCP_Message_ControlCircuitReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 7)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));	
		circuitId = new Integer(payload_bytes[4]);

		if (circuitId < 0 || circuitId > 15)
			throw new UnformattedPacketException("Circuit ID 틀림, "
					+ HexUtils.toHexString(circuitId));

		ctrlMode = E_GCP_ControlMode.getFromByte(payload_bytes[5]);
		dimmingLevel = payload_bytes[6];
	}


	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public int getCircuitId() {
		return circuitId;
	}

	public E_GCP_ControlMode getCtrlMode() {
		return ctrlMode;
	}

	public static void main(String args[]) {
		GCP_Message_ControlCircuitReq deviceInfoReqMsg = new GCP_Message_ControlCircuitReq(
				(short)0x1234, 1, E_GCP_ControlMode.ON);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_ControlCircuitReq(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public short getNodeId() {
		// TODO Auto-generated method stub
		return this.nodeId;
	}
}
