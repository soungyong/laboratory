package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.ncp2_0.I_NCP_Message;

public abstract class GCP_Message implements I_GCP_Message, I_Message {
	E_GCP_MessageType msgType;
	int length;
	byte checksum;
	static byte seqNumGenerator=0;
	byte pid;
	byte subPid;
	byte seqNum;	
	byte[] payload_byte;

	public GCP_Message(){
		
	}		
	
	public GCP_Message(byte[] packet, int size) throws UnformattedPacketException {		
		throw new UnformattedPacketException("패킷 길이 틀림, size: "+size);		
	}
	
	public byte[] getPayload(byte[] packet_bytes){		
		this.seqNum = packet_bytes[5];		
		this.length = new Integer(packet_bytes[1]);
		this.checksum = packet_bytes[packet_bytes.length-2];
		
		payload_byte = new byte[packet_bytes.length-6];
		
		for(int i=0; i < payload_byte.length; i++)
			payload_byte[i] = packet_bytes[i+4];
		
		return payload_byte;
	}
		
	public byte[] getPacketBytes(byte[] payload){
		this.payload_byte = payload;
		this.length = payload.length+2;
		this.seqNum = payload[1];
		
		return toBytes();
	}
	
	public byte[] getPacketBytes(){		
		this.length = payload_byte.length+2;
		this.seqNum = getSeqNumGenerator();
		
		return toBytes();
	}
		
	@Override
	public byte[] toBytes() {		
		int len=0;
		byte checksum=0;	
		byte packet_byte[] = new byte[length+4];
		
		packet_byte[len++] = (byte) 0xfa;
		packet_byte[len++] = new Integer(this.length).byteValue();
		packet_byte[len++] = (byte) 0x80;
		packet_byte[len++] = GCP1_4;		
		for(int i=0; i < payload_byte.length; i++)
			packet_byte[len++] = payload_byte[i];
		for(int i=2; i < len; i++)
			checksum+=packet_byte[i];
		packet_byte[len++] = checksum;
		packet_byte[len++] = (byte) 0xaf;
				 
		return packet_byte;
	}

	@Override
	public E_GCP_MessageType getMessageType() {		
		return msgType;
	}

	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		return length;
	}

	public byte getPid() {
		return pid;
	}

	public byte getSubPid() {
		return subPid;
	}

	@Override
	public byte getChecksum() {
		// TODO Auto-generated method stub
		return checksum;
	}
	
	
	
	@Override
	public void setSeqNum(byte seqNum) {
		this.seqNum = seqNum;
	}
	
	@Override
	public byte getSeqNumGenerator() {
		return seqNumGenerator++;
	}
	
	@Override
	public short getSeqNum(){
		return seqNum;
	}
}
