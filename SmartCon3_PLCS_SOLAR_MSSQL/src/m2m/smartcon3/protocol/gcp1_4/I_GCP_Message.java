package m2m.smartcon3.protocol.gcp1_4;


public interface I_GCP_Message {
	static final byte GCP1_4=0x20;
	
	public byte[] toBytes();
	public E_GCP_MessageType getMessageType();
	public int getLength();
	public byte getChecksum();
	public short getNodeId();
	public byte getSeqNumGenerator();
	public void setSeqNum(byte seqNum);
	public short getSeqNum();
}
