package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_Result;
import m2m.util.HexUtils;

public class GCP_Message_MappingSensorRes extends GCP_Message {
	short nodeId;
	short sensorId;
	int circuitId;
	int onTime;
	E_GCP_ControlMode offCtrlMode;
	E_GCP_Result result;

	public GCP_Message_MappingSensorRes(){
		msgType = E_GCP_MessageType.RES_MAPPING_SENSOR;	
	}
	public GCP_Message_MappingSensorRes(short nodeId, short sensorId, int circuitId, int onTime, E_GCP_ControlMode offCtrlMode,
			E_GCP_Result result) {
		this();
		this.nodeId = nodeId;
		this.sensorId = sensorId;
		this.circuitId = circuitId;
		this.onTime = onTime;
		this.offCtrlMode = offCtrlMode;
		this.result = result;

		int len = 0;
		byte data_bytes[] = new byte[10];		
		data_bytes[len++] = msgType.toByte();	
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();
		data_bytes[len++] = new Integer(sensorId >> 8).byteValue();
		data_bytes[len++] = new Integer(sensorId).byteValue();
		data_bytes[len++] = new Integer(circuitId).byteValue();
		data_bytes[len++] = new Integer(onTime).byteValue();
		data_bytes[len++] = offCtrlMode.toByte();
		data_bytes[len++] = result.toByte();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_MappingSensorRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=10)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));
		sensorId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])));
		circuitId = new Integer(payload_bytes[6]);	
		onTime = new Integer(payload_bytes[7]);	
		offCtrlMode = E_GCP_ControlMode.getFromByte(payload_bytes[8]);					
		if(payload_bytes[9]==0x02){
			result = E_GCP_Result.UPDATE;
		}else if(payload_bytes[9]==0x00){
			result = E_GCP_Result.SUCCESS;
		} else
			result = E_GCP_Result.FAIL;	
	}

	public E_GCP_MessageType getMsgType() {
		return msgType;
	}

	public E_GCP_Result getResult() {
		return result;
	}

	public short getNodeId() {
		return nodeId;
	}
		
	public short getSensorId() {
		return sensorId;
	}
	public int getCircuitId() {
		return circuitId;
	}
	public int getOnTime() {
		return onTime;
	}
	public E_GCP_ControlMode getOffCtrlMode() {
		return offCtrlMode;
	}
	public static void main(String args[]) {
		GCP_Message_MappingSensorRes deviceInfoReqMsg = new GCP_Message_MappingSensorRes(
				(short)0x1251,  (short)0x4321, 0, 10, E_GCP_ControlMode.OFF, E_GCP_Result.SUCCESS);
		byte[] data = deviceInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			deviceInfoReqMsg = new GCP_Message_MappingSensorRes(data,
					data.length);
			data = deviceInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
