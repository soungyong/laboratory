package m2m.smartcon3.protocol.gcp1_4;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.gcp2_1.E_GCP_ControlMode;
import m2m.util.HexUtils;

public class GCP_Message_StateInfoRes extends GCP_Message {
	short nodeId;	
	short deviceType;
	
	E_GCP_ControlMode stateInfo[] = new E_GCP_ControlMode[16];	

	public GCP_Message_StateInfoRes() {
		msgType = E_GCP_MessageType.RES_STATEINFO;
	}
	
	public GCP_Message_StateInfoRes(short nodeId, short deviceType, E_GCP_ControlMode stateInfo[]
			) throws UnformattedPacketException {
		this();
		this.nodeId = nodeId;		
		this.deviceType = deviceType;
		this.stateInfo = stateInfo;

		if (deviceType != 0x2010)
			throw new UnformattedPacketException("device type 틀림");

		int len = 0;
		byte data_bytes[] = new byte[22];
		data_bytes[len++] = msgType.toByte();
		data_bytes[len++] = getSeqNumGenerator();
		data_bytes[len++] = new Integer(nodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(nodeId).byteValue();		
		data_bytes[len++] = new Integer(deviceType >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceType).byteValue();

		for (int i = 0; i < 16; i++)
			data_bytes[len++] = stateInfo[i].toByte();
		
		getPacketBytes(data_bytes);
	}

	public GCP_Message_StateInfoRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();

		byte[] payload_bytes = super.getPayload(packet);
		if (payload_bytes.length != 22)
			throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		nodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])));		
		deviceType = (short) (HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[4])) * 256 + HexUtils.toInt(HexUtils
				.toHexString(payload_bytes[5])));

		 if (deviceType == 0x2010) {
			for (int i = 0; i < 16; i++)
				stateInfo[i] = E_GCP_ControlMode.getFromByte(payload_bytes[6 + i]);			
		} 
	}

	public int getDeviceType() {
		return deviceType;
	}

	public E_GCP_ControlMode[] getStateInfo() {
		return stateInfo;
	}

	public short getNodeId() {
		return nodeId;
	}

	public static void main(String args[]) throws UnformattedPacketException {
		
		E_GCP_ControlMode stateInfo[] = new E_GCP_ControlMode[16];
		E_GCP_ControlMode controlInfo[] = new E_GCP_ControlMode[16];
		for(int i=0; i < 16; i++){
			controlInfo[i] = stateInfo[i] = E_GCP_ControlMode.DEFAULT;
		}
		GCP_Message_StateInfoRes stateInfoReqMsg = new GCP_Message_StateInfoRes((short) 0x5412,
				 (short) 0x2010,
				stateInfo);

		byte[] data = stateInfoReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			stateInfoReqMsg = new GCP_Message_StateInfoRes(data, data.length);
			data = stateInfoReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}
