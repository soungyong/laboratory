package m2m.smartcon3.protocol.ncp2_0;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class NCP_Message_RegisterReq extends NCP_Message {
		
	short deviceType;
	int deviceVersion;
	int firmwareVersion;
	
	NCP_Message_RegisterReq(){
		msgType = E_NCP_MessageType.REQ_REGISTER;
	}
	
	public NCP_Message_RegisterReq(short srcId, short destId, short deviceType,
			int deviceVersion, int firmwareVersion) {
		this();
		
		this.srcId = srcId;
		this.destId = destId;
		this.deviceType = deviceType;
		this.deviceVersion = deviceVersion;
		this.firmwareVersion = firmwareVersion;

		int len = 0;
		byte data_bytes[] = new byte[5];
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(deviceType >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceType).byteValue();
		data_bytes[len++] = new Integer(deviceVersion).byteValue();
		data_bytes[len++] = new Integer(firmwareVersion).byteValue();

		getPacketBytes(data_bytes);
	}

	public NCP_Message_RegisterReq(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		
		if (payload_bytes.length != 5)
			throw new UnformattedPacketException("패킷 길이 틀림, size: " + size);

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));

		deviceType = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		deviceVersion = payload_bytes[3];
		firmwareVersion = payload_bytes[4];
	}
	
	public int getDeviceType() {
		return deviceType;
	}

	public int getDeviceVersion() {
		return deviceVersion;
	}

	public int getFirmwareVersion() {
		return firmwareVersion;
	}

	public static void main(String args[]) {
		NCP_Message_RegisterReq reigsterReqMsg = new NCP_Message_RegisterReq(
				(short)0x1234, (short)0x3445, (short)0x2050, (byte)0x14, (byte)0x12);
		byte[] data = reigsterReqMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");

		try {
			reigsterReqMsg = new NCP_Message_RegisterReq(data, data.length);
			data = reigsterReqMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
