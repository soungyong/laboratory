package m2m.smartcon3.protocol.ncp2_0;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class NCP_Message_RegisterRes extends NCP_Message {
	
	int result;
	int timeout;

	public NCP_Message_RegisterRes(){
		msgType = E_NCP_MessageType.RES_REGISTER;
	}
	
	public NCP_Message_RegisterRes(short srcId, short destId, int result, int timeout) {
		this();
		this.srcId = srcId;
		this.destId = destId;
		
		this.result = result;
		this.timeout = timeout;
		
		int len = 0;
		byte data_bytes[] = new byte[5];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(result >> 8).byteValue();
		data_bytes[len++] = new Integer(result).byteValue();
		data_bytes[len++] = new Integer(timeout >> 8).byteValue();
		data_bytes[len++] = new Integer(timeout).byteValue();
		
		getPacketBytes(data_bytes);				
	}

	public NCP_Message_RegisterRes(byte[] packet, int size) throws UnformattedPacketException {
		this();
		byte[] payload_bytes = super.getPayload(packet);
		if(payload_bytes.length!=5)throw new UnformattedPacketException("패킷 길이 틀림");

		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
		
		result = HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2]));
		timeout = HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])) * 256
		+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[4]));		
	}

	public int getResult() {
		return result;
	}

	public int getTimeout() {
		return timeout;
	}

	public static void main(String args[]) {
		NCP_Message_RegisterRes rigisterResMsg = new NCP_Message_RegisterRes(
				(short)0x1234, (short)0x3456, 0x2050, 0x1234);
		byte[] data = rigisterResMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");
		
		try {
			rigisterResMsg = new NCP_Message_RegisterRes(data, data.length);
			data = rigisterResMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
