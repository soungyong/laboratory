package m2m.smartcon3.protocol.ncp2_0;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.util.HexUtils;

public class NCP_Message_RegisterNodeRes extends NCP_Message {
	short subNodeId;
	short deviceType;
	int result;
	
	public NCP_Message_RegisterNodeRes() {
		msgType = E_NCP_MessageType.RES_REGISTER_NODE;
	}

	public NCP_Message_RegisterNodeRes(short srcId, short destId, short subNodeId, short devicetype,
			int result) {
		this();
		
		this.srcId = srcId;
		this.destId = destId;
		this.subNodeId = subNodeId;
		this.deviceType = devicetype;
		this.result = result;
		
		int len = 0;
		byte data_bytes[] = new byte[7];		
		data_bytes[len++] = msgType.toByte();		
		data_bytes[len++] = new Integer(subNodeId >> 8).byteValue();
		data_bytes[len++] = new Integer(subNodeId).byteValue();
		data_bytes[len++] = new Integer(deviceType >> 8).byteValue();
		data_bytes[len++] = new Integer(deviceType).byteValue();
		data_bytes[len++] = new Integer(result >> 8).byteValue();
		data_bytes[len++] = new Integer(result).byteValue();
		
		getPacketBytes(data_bytes);		
	}

	public NCP_Message_RegisterNodeRes(byte[] packet, int size)
			throws UnformattedPacketException {
		this();
		
		byte[] payload_bytes = super.getPayload(packet);
		
		if (payload_bytes.length != 7)
			throw new UnformattedPacketException("패킷 길이 틀림");
		
		byte messageType = payload_bytes[0];
		if (messageType != this.msgType.toByte())
			throw new UnformattedPacketException("메시지 타입 틀림, "
					+ HexUtils.toHexString(messageType));
				
		subNodeId = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[1])) * 256
		+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[2])));
		deviceType = (short) (HexUtils.toInt(HexUtils.toHexString(payload_bytes[3])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[4])));
		result = HexUtils.toInt(HexUtils.toHexString(payload_bytes[5])) * 256
				+ HexUtils.toInt(HexUtils.toHexString(payload_bytes[6]));

	}

	public int getResult() {
		return result;
	}

	public short getDeviceType() {
		return deviceType;
	}
	
	public short getSubNodeId(){
		return subNodeId;
	}

	public static void main(String args[]) {
		NCP_Message_RegisterNodeRes registerNodeResMsg = new NCP_Message_RegisterNodeRes(
				(short)0x1234, (short)0x3456, (short)0x2050, (short)0x0001, 0x1234);
		byte[] data = registerNodeResMsg.toBytes();
		for (int i = 0; i < data.length; i++)
			System.out.print(HexUtils.toHexString(data[i]) + " ");
		System.out.println("");
		
		try {
			registerNodeResMsg = new NCP_Message_RegisterNodeRes(data, data.length);
			data = registerNodeResMsg.toBytes();
			for (int i = 0; i < data.length; i++)
				System.out.print(HexUtils.toHexString(data[i]) + " ");
			System.out.println("");
		} catch (UnformattedPacketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
