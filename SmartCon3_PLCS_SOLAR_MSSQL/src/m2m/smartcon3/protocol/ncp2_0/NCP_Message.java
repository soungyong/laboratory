package m2m.smartcon3.protocol.ncp2_0;

import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.protocol.I_Message;
import m2m.util.HexUtils;

public abstract class NCP_Message implements I_NCP_Message, I_Message {
	E_NCP_MessageType msgType;
	int length;
	byte checksum;
	static short seqNumGenerator=0;
	byte pid;
	byte subPid;
	short seqNum;
	short srcId;
	short destId;
	byte[] payload_byte;

	public NCP_Message() {		
	}

	public NCP_Message(byte[] packet, int size) throws UnformattedPacketException {		
		throw new UnformattedPacketException("패킷 길이 틀림, size: "+size);		
	}
	
	public byte[] getPayload(byte[] packet_bytes){
		this.seqNum = (short) (HexUtils.toShort(HexUtils.toHexString(packet_bytes[4]))*256 + HexUtils.toShort(HexUtils.toHexString(packet_bytes[5])));
		this.destId = (short) (HexUtils.toShort(HexUtils.toHexString(packet_bytes[6]))*256 + HexUtils.toShort(HexUtils.toHexString(packet_bytes[7])));
		this.srcId = (short) (HexUtils.toShort(HexUtils.toHexString(packet_bytes[8]))*256 + HexUtils.toShort(HexUtils.toHexString(packet_bytes[9])));
		this.length = new Integer(packet_bytes[1]);
		this.checksum = packet_bytes[packet_bytes.length-2];
		
		payload_byte = new byte[packet_bytes.length-12];
		
		for(int i=0; i < payload_byte.length; i++)
			payload_byte[i] = packet_bytes[i+10];
		
		return payload_byte;
	}
		
	public byte[] getPacketBytes(byte[] payload){
		this.payload_byte = payload;
		this.length = payload.length+8;
		this.seqNum = getSeqNumGenerator();
		
		return toBytes();
	}
	
	public byte[] getPacketBytes(){		
		this.length = payload_byte.length+8;
		this.seqNum = getSeqNumGenerator();
		
		return toBytes();
	}
		
	@Override
	public byte[] toBytes() {		
		int len=0;
		byte checksum=0;	
		byte packet_byte[] = new byte[length+4];
		
		packet_byte[len++] = (byte) 0xfa;
		packet_byte[len++] = new Integer(this.length).byteValue();
		packet_byte[len++] = (byte) 0x80;
		packet_byte[len++] = NCP2_0;
		packet_byte[len++] = new Short((short) (seqNum>>8)).byteValue();
		packet_byte[len++] = new Short((short) (seqNum)).byteValue();
		packet_byte[len++] = new Short((short) (destId>>8)).byteValue();
		packet_byte[len++] = new Short((short) (destId)).byteValue();
		packet_byte[len++] = new Short((short) (srcId>>8)).byteValue();
		packet_byte[len++] = new Short((short) (srcId)).byteValue();
		for(int i=0; i < payload_byte.length; i++)
			packet_byte[len++] = payload_byte[i];
		for(int i=2; i < len; i++)
			checksum+=packet_byte[i];
		packet_byte[len++] = checksum;
		packet_byte[len++] = (byte) 0xaf;
				 
		return packet_byte;
	}

	@Override
	public E_NCP_MessageType getMessageType() {
		// TODO Auto-generated method stub
		return msgType;
	}

	@Override
	public int getLength() {
		// TODO Auto-generated method stub
		return length;
	}
	
	public byte getPid() {
		return pid;
	}

	public byte getSubPid() {
		return subPid;
	}

	public short getSeqNum() {
		return seqNum;
	}

	@Override
	public byte getChecksum() {
		// TODO Auto-generated method stub
		return checksum;
	}	

	@Override
	public short getSrcId(){
		return this.srcId;
	}
	
	@Override
	public short getDestId() {
		return destId;
	}	
	
	@Override
	public short getSeqNumGenerator() {
		return seqNumGenerator++;
	}
	
	@Override 
	public void setSeqNum(short seqNum){
		this.seqNum = seqNum;
	}
}
