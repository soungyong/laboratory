/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
package m2m.smartcon3.core;

//import m2m.smartcon.server.DeviceManager;
import m2m.smartcon3.device.I_DeviceManager;
import m2m.smartcon3.devicenetwork.DefaultXNetHandler;
import m2m.smartcon3.event.I_EventManager;


/**
 *	Server is registra/maintainer of utility classes
 * - SmartCache : gateway/node device manager
 * - GateAdapter : gateway message handler
 * - NodeAdapter : node message handler
 * - DeviceFactory : gateway/node instance creator
 * 
 */
public interface I_Server
{	
	public I_DeviceManager getDeviceManager();		
	public I_EventManager getEventManager();	
}