/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/06/02.
*/
package m2m.smartcon3.core;

import m2m.smartcon3.device.DefaultDeviceManager;
import m2m.smartcon3.device.I_DeviceManager;
import m2m.smartcon3.devicenetwork.ClientSocketListener;
import m2m.smartcon3.devicenetwork.DefaultXNetHandler;
import m2m.smartcon3.event.DefaultEventManager;
import m2m.smartcon3.event.I_EventManager;

import org.apache.log4j.Logger;


/**
 *	Server is registra/maintainer of utility classes
 * - SmartCache : gateway/node device manager
 * - GateAdapter : gateway message handler
 * - NodeAdapter : node message handler
 * - DeviceFactory : gateway/node instance creator
 * 
 * 
 * usage
 * 
 *		server.register(XXX);	// if you want your handler.
 *		server.init();
 *		server.startUp(port);
 *		server.shutDown();
 *
 * - 06/02 : remove SmartMS dependency.
 * 		;; 서버 코어들을 가지고 있도록 만듦.
 * 
 */
public abstract class ServerImplBase implements I_Server
{
	public static final Logger logger = Logger.getLogger("ServerImplBase");
	
	I_DeviceManager	deviceManager;
	I_EventManager	eventManager;
	
	// server core components
	ClientSocketListener	xnetServer;
	DefaultXNetHandler		xnetProcessor;
	
	// is running or not.
	int state = 0;
	
	public ServerImplBase()
	{
		//
	}
	

	public final I_DeviceManager getDeviceManager()
	{
		return deviceManager;
	}
	public final void registerDeviceManager(I_DeviceManager a)
	{
		deviceManager = a;
	}
	
	public final I_EventManager getEventManager()
	{
		return eventManager;
	}
	public final void registerEventManager(I_EventManager a)
	{
		eventManager = a;
	}

	/**
	 * Get XNet handler.
	 * 
	 * @return XNetProcessor instance as XNetHandler.
	 */
	public final DefaultXNetHandler	getNetHandler()
	{
		return xnetProcessor;
	}
	

	/**
	 * Create default instances if not registered yet. 
	 * @note	
	 */
	public void init()
	{
		if (xnetServer == null)
		{
			xnetServer = ClientSocketListener.getInstance();
		}
		if (xnetProcessor == null)
		{
			//xnetProcessor = new DefaultXNetHandler(xnetServer);
			//xnetProcessor.setXNetServer(xnetServer);				// TODO:
		}
		//xnetServer.setXNetProcessor(xnetProcessor);

		// deviceFactory
		
	
		if (getDeviceManager()==null)
		{
			deviceManager = DefaultDeviceManager.getInstance();			
		}

		if (getEventManager()==null)
		{
			eventManager = DefaultEventManager.getInstance();			
		}

		//xnetServer.setZGateListener(getGatewayAdapter());
		//xnetProcessor.setZGateListener(getGatewayAdapter());
		// node handler.
		//xnetProcessor.setZNodeListener(getNodeAdapter());
	}
	
	/**
	 * 서버 상태
	 * 
	 * @return
	 * 	0 : start up
	 *  0 : shut down
	 */
	public final int getState()
	{
		return state;
	}
	/**
	 * 서버를 시작한다.
	 * 
	 * @param port
	 * @throws SmartException
	 * 		내부 구성요소들이 초기화가 안되었을 경우 등.
	 */
	public final void startUp(int port) //throws SmartException
	{
		if (state != 0)
			return;
		
	//	if (xnetServer == null)
			//throw new SmartException("XNetServer is not created");
			
		//if (xnetProcessor == null)
			//throw new SmartException("XNetProcessor is not created");
		
//		registerEventManager(DefaultEventManager.getInstance());			

		//if (getEventManager()==null)
			//throw new SmartException("EventManager is not registered");

		//if (getDeviceManager()==null)
			//throw new SmartException("DeviceManager is not registered");
				
//
//		if (getGatewayAdapter() == null)
//		{
//			registerGatewayAdapter(new DefaultGatewayAdapter(this));
//		}
//		
//		if (getNodeAdapter()==null)
//		{
//			registerNodeAdapter(new DefaultNodeAdapter(this));			
//		}
//
//		if (getDeviceManager()==null)
//		{
//			registerDeviceManager(new DeviceManager());			
//		}
//
//		if (getEventManager()==null)
//		{
//			//registerEventManager(new DefaultEventManager());			
//			registerEventManager(DefaultEventManager.getInstance());			
//		}
		// check server.
				
		getEventManager().startUp();
		
//		 TODO: 이 걸 제거했으면 좋겠는데..
//		SmartMS.setZGateListener(getGatewayAdapter());
//		SmartMS.setZNodeListener(getNodeAdapter());
//		SmartMS.startUp(port);

		//XNetServer.getInstance().setZGateListener(getGatewayAdapter());
		//XNetProcessor.getInstance().setZGateListener(getGatewayAdapter());
		// node handler.
		//XNetProcessor.getInstance().setZNodeListener(getNodeAdapter());
		
		
		
		//xnetServer.startUp(port);
		
		state = 1;
	}

	/**
	 * 서버를 종료한다.
	 *
	 */
	public void shutDown()
	{
		if (state != 1)
		{
			return;
		}
		
		logger.debug("shutDown..");
		
//		SmartMS.shutDown();
		//XNetServer.getInstance().shutDown();
		//xnetServer.shutDown();

		logger.debug("shutDown..2");
		getEventManager().shutDown();
		
		// whether monitoring started or not.
		//getDeviceManager().stopMonitoring();
		
		//gwAdapter = null;
		//nodeAdapter = null;
		deviceManager = null;		
		eventManager = null;
		// clear all instances.

		state = 0;
		logger.debug("shutDown end");
	}
}