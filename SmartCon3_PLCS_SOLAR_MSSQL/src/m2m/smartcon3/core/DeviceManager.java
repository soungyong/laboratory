package m2m.smartcon3.core;

import java.util.Collection;
import java.util.HashMap;

import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.device.I_Device.NETWORK_STATE;
import m2m.smartcon3.device.LC100;
import m2m.smartcon3.device.MDF100;
import m2m.smartcon3.device.MDT100;
import m2m.smartcon3.device.MGW300;
import m2m.smartcon3.device.MPR200;
import m2m.smartcon3.device.MSN300;
import m2m.smartcon3.device.OldLC100;
import m2m.smartcon3.device.OldMDT100;
import m2m.smartcon3.device.OldMGW300;
import m2m.smartcon3.device.OldMSN300;
import m2m.smartcon3.devicenetwork.I_XNetHandler;
import m2m.smartcon3.protocol.I_Message;
import m2m.smartcon3.protocol.gcp2_1.I_GCP_Message;
import m2m.smartcon3.protocol.ncp2_0.I_NCP_Message;

public class DeviceManager {
	private static DeviceManager _instance = null;

	HashMap<Short, I_Device> deviceMap;

	public synchronized static DeviceManager getInstance() {
		if (_instance == null) {
			_instance = new DeviceManager();
		}
		return _instance;
	}

	private DeviceManager() {
		deviceMap = new HashMap<Short, I_Device>();
	}

	public void addDevice(I_Device device) {
		deviceMap.put(device.getId(), device);
	}

	public boolean removeDevice(I_Device device) {
		if (device instanceof MGW300) {
			MGW300 mgw = (MGW300) device;
			if (mgw.getConnectionState() == NETWORK_STATE.ONLINE)
				return false;
			else {
				mgw.termiate();
				deviceMap.remove(mgw.getId());
				return true;
			}
		} else if (device instanceof OldMGW300) {
			OldMGW300 mgw = (OldMGW300) device;
			if (deviceMap.remove(mgw.getId()) == null)
				return false;
		} else if (device instanceof LC100) {
			MGW300 mgw = (MGW300) (device.getParent());
			return mgw.removeLC((LC100) device);
		} else if (device instanceof OldLC100) {
			OldMGW300 mgw = (OldMGW300) (device.getParent());
			return mgw.removeLC((OldLC100) device);
		} else if (device instanceof MSN300) {
			LC100 lc = (LC100) (device.getParent());
			return lc.removeDevice(device);
		} else if (device instanceof OldMSN300) {
			OldLC100 lc = (OldLC100) (device.getParent());
			return lc.removeDevice(device);
		} else if (device instanceof MDT100) {
			LC100 lc = (LC100) (device.getParent());
			return lc.removeDevice(device);
		} else if (device instanceof OldMDT100) {
			OldLC100 lc = (OldLC100) (device.getParent());
			return lc.removeDevice(device);
		} else if (device instanceof MDF100) {
			LC100 lc = (LC100) (device.getParent());
			return lc.removeDevice(device);
		} else if (device instanceof MPR200) {
			LC100 lc = (LC100) device.getParent();
			return lc.removeDevice(device);
		}
		else
			return false;
		return true;
	}

	public I_Device getGatewayById(short deviceId) {
		return deviceMap.get(deviceId);
	}

	public void AssignXNetHandler(I_XNetHandler xnetHandler) {
		I_Message msg = xnetHandler.getMessage();
		if (msg != null) {
			if (msg instanceof I_NCP_Message) {
				I_NCP_Message ncpMsg = (I_NCP_Message) msg;
				short srcId = ncpMsg.getSrcId();
				I_Device device = getGatewayById(srcId);
				if (device == null) {
					MGW300 mgw300 = new MGW300(srcId);
					addDevice(mgw300);
					device = mgw300;
				}
				if (device != null)
					device.setXNetHandler(xnetHandler);
			} else if (msg instanceof I_GCP_Message) {
				I_GCP_Message gcpMsg = (I_GCP_Message) msg;
				short srcId = gcpMsg.getSrcId();
				I_Device device = getGatewayById(srcId);
				if (device == null) {
					MGW300 mgw300 = new MGW300(srcId);
					addDevice(mgw300);
					device = mgw300;
				}
				if (device != null)
					device.setXNetHandler(xnetHandler);
			} else if (msg instanceof m2m.smartcon3.protocol.ncp1_0.I_NCP_Message) {
				m2m.smartcon3.protocol.ncp1_0.I_NCP_Message gcpMsg = (m2m.smartcon3.protocol.ncp1_0.I_NCP_Message) msg;
				short nodeId = gcpMsg.getNodeId();
				I_Device device = getGatewayById(nodeId);
				if (device == null) {
					OldMGW300 mgw300 = new OldMGW300(nodeId);
					addDevice(mgw300);
					device = mgw300;
				}
				if (device != null)
					device.setXNetHandler(xnetHandler);
			} else if (msg instanceof m2m.smartcon3.protocol.gcp1_4.I_GCP_Message) {
				m2m.smartcon3.protocol.gcp1_4.I_GCP_Message gcpMsg = (m2m.smartcon3.protocol.gcp1_4.I_GCP_Message) msg;
				short nodeId = gcpMsg.getNodeId();
				I_Device device = getGatewayById(nodeId);
				if (device == null) {
					OldMGW300 mgw300 = new OldMGW300(nodeId);
					addDevice(mgw300);
					device = mgw300;
				}
				if (device != null)
					device.setXNetHandler(xnetHandler);
			}
		}
	}

	public int getNumOfGateway() {
		return deviceMap.size();
	}

	public Collection<I_Device> getGatewayList() {
		return deviceMap.values();
	}
}
