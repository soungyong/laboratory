/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
*/
package m2m.smartcon3.core;

import org.apache.log4j.Logger;

//import m2m.smartcon.api.*;

/**
 *	DefaultServer main class.
 *
 * @note 이 클래스는 그냥 참조용으로 만든것이다. 이렇게만 해도, 서버는 돌아간다.
 * 그러나, 내가 처맇해야하는 게이트웨이나, 노드들이 등록되어 있지 않으므로 별 의미는 없는 서버가 될 것이다. 
 */
public class DefaultServer extends ServerImplBase	//implements Runnable
{
	public static final Logger logger = Logger.getLogger("DefaultServer");
	
	
	private DefaultServer()
	{
		//System.out.println(app_version);
		//logger.info(app_version);
	}
}
