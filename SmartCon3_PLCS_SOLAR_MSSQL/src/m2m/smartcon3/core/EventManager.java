/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/13.
 */
package m2m.smartcon3.core;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;

import m2m.smartcon3.event.I_Event;
import m2m.smartcon3.event.I_EventHandler;
import m2m.smartcon3.event.I_EventManager;
import m2m.util.MyLog;

/**
 * Event manager/holder
 */
public class EventManager implements I_EventManager, Serializable {
	// public static final Logger logger =
	// Logger.getLogger("DefaultEventManager");
	private static final long serialVersionUID = -3853247911635568412L;
	// LinkedList eq;
	List<I_Event> eq;
	Hashtable<Integer, List<I_Event>> eventLoggerTable = new Hashtable<Integer, List<I_Event>>();
	Hashtable<Integer, Date> lastAccessTimeToEventLogger = new Hashtable<Integer, Date>();

	// (eventClassName, eventHandler) map.
	// HashMap eventHandlerMap;
	Hashtable<String, I_EventHandler> eventHandlerMap;

	// LinkedList<WBEvent> eq;
	Thread eventPumpThread;

	boolean eventThreadRunning;

	/**
	 * Singleton.
	 */
	private static EventManager instance = null;

	public synchronized static EventManager getInstance() {
		if (instance == null) {
			instance = new EventManager();
			return instance;
		} else {
			return instance;
		}
	}

	private EventManager() {
		eq = Collections.synchronizedList(new LinkedList<I_Event>());
		eventHandlerMap = new Hashtable<String, I_EventHandler>();
		startUp();
	}

	/**
	 * Startup event pump.
	 */
	public void startUp() {
		//
		// logger.info("startDefaultEventManager");

		eventPumpThread = new Thread(new Runnable() {
			public void run() {
				while (eventThreadRunning) {
					try {
						I_Event ev = poll(); // and wait..

						if (ev != null) {
							// find event handler and dispatch
							dispatchEvent(ev);

							// logger.debug("polling new Event");
						} else
							Thread.sleep(100);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		});

		eventThreadRunning = true;
		eventPumpThread.start();

		// logger.info("startDefaultEventManager started..");
	}

	/**
	 * Shutdown event pump.
	 */
	public void shutDown() {
		eventThreadRunning = false;

		eventHandlerMap.clear();

		synchronized (eq) {
			eq.notify();
		}
		// this.notify();
	}

	/**
	 * 이벤트를 등록되어 있는 핸들러로 전달함(dispatch).
	 * 
	 * @param ev
	 */
	private void dispatchEvent(I_Event ev) {
		try {
			// 최하위 concrete 클래스 이름이 우선함.
			I_EventHandler eh = (I_EventHandler) eventHandlerMap.get(ev.getClass().getName());

			if (eh != null) {
				// ev.getClass(),
				eh.handleEvent(ev);
			} else {
				// logger.info("Event handler not found for "
				// + ev.getClass().getName());
			}
		} catch (Exception e) {
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}
	}

	public void registerEventHandler(String eventClassName, I_EventHandler myhandler)
	// throws SmartException
	{
		I_EventHandler eh = (I_EventHandler) eventHandlerMap.get(eventClassName);
		if (eh != null) {
			// throw new SmartException("Duplicated evnet handler");
		}

		// else
		eventHandlerMap.put(eventClassName, myhandler);
	}

	public I_EventHandler unregisterEventHandler(String eventClassName) // throws
	// SmartException
	{
		return (I_EventHandler) eventHandlerMap.remove(eventClassName);
	}

	/**
	 * NOTE: thread에서만 사용함.
	 * 
	 * @return
	 * @throws InterruptedException
	 */
	protected I_Event poll() throws InterruptedException {
		I_Event ev = null;

		if (eq.size() > 0)
			ev = (I_Event) eq.remove(0);
		return ev;
	}

	/**
	 * 이벤트를 추가함. 등록된 이벤트핸들러로 dispath될 것임.
	 * 
	 * @param e
	 * @return
	 */
	public boolean add(I_Event e) {
		// logger.debug("Event added : " + e.getClass().getName());
		addToEventLogger(e);
		eq.add(e);
		return true;
	}

	private void addToEventLogger(I_Event e) {
		Collection<List<I_Event>> events = eventLoggerTable.values();
		for (List<I_Event> eventLogger : events) {
			if (eventLogger.size() > 20000) {
				// MyLog.l("Overflow Event in EventLogger: 20000");
				continue;
			}
			eventLogger.add(e);
		}
	}

	public I_Event getEvent(int eventLoggerId) {
		if (eventLoggerTable.containsKey(eventLoggerId) == false)
			return null;

		Date lastAccessTime = lastAccessTimeToEventLogger.get(eventLoggerId);
		if (lastAccessTime != null) {
			if (lastAccessTime.getTime() + 30000 < System.currentTimeMillis())
				lastAccessTimeToEventLogger.put(eventLoggerId, Calendar.getInstance().getTime());
		}
		List<I_Event> eventLogger = eventLoggerTable.get(eventLoggerId);
		if (eventLogger.size() > 0)
			return eventLogger.remove(0);
		return null;
	}

	public int getNumOfEvent(int eventLoggerId) {
		if (eventLoggerTable.containsKey(eventLoggerId) == false)
			return -1;
		return eventLoggerTable.get(eventLoggerId).size();
	}

	public int getNewEventLoggerId() {
		List<I_Event> eventLogger;
		for (int i = 0; i < 50; i++) {
			if (lastAccessTimeToEventLogger.get(i) != null) {
				if (lastAccessTimeToEventLogger.get(i).getTime() + 60000 < System.currentTimeMillis()) {
					if (eventLoggerTable.get(i) != null) {
						eventLoggerTable.remove(i);
						lastAccessTimeToEventLogger.remove(i);
					}
				}
			}
		}

		for (int i = 0; i < 50; i++) {
			if (eventLoggerTable.containsKey(i) == false) {
				eventLogger = Collections.synchronizedList(new LinkedList<I_Event>());
				eventLoggerTable.put(i, eventLogger);
				lastAccessTimeToEventLogger.put(i, Calendar.getInstance().getTime());
				return i;
			}
		}
		return -1;
	}
}
