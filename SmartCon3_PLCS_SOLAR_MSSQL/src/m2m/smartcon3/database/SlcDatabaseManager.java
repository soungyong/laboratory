package m2m.smartcon3.database;

import java.sql.SQLException;

import m2m.smartcon3.database.info.SolarSensorValueDAO;
import m2m.smartcon3.database.info.SolarSensorValueMsSqlDAO;
import m2m.smartcon3.device.SolarSensorGetter;

public class SlcDatabaseManager {

	static int monitoring_interval_s = -1;

	static SlcDatabaseManager _instance = null;

	public static SlcDatabaseManager getInstance() {
		if (_instance == null) {
			_instance = new SlcDatabaseManager();
		}
		return _instance;
	}

	public void addSolarSensorValue(SolarSensorGetter solarSensor) {

		SolarSensorValueDAO solarSensorValueDAO = new SolarSensorValueDAO(solarSensor);
		try {
			SqlConfig.getSqlMapInstance().update("updateSolarSensorValue", solarSensorValueDAO);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public int getMonitoringInterval() {
		System.out.println("Monitoring Interval : " + monitoring_interval_s);
		if (monitoring_interval_s == -1) {
			try {
				Object result = SqlConfig.getSqlMapInstance().queryForObject("selectMonitoringInterval_s", null);
				Integer resultValue = (Integer) result;
				monitoring_interval_s = resultValue.intValue();
			} catch (SQLException e) {
				e.printStackTrace();
				return 300;
			}
		}

		if (monitoring_interval_s <= -1) {
			return 300;
		}
		return monitoring_interval_s;
	}

	public void addSolarSensorValue_MsSql(SolarSensorGetter solarSensor) {
		for (int i = 0; i < 16; i++) {
			SolarSensorValueMsSqlDAO solarSensorValue_MsSqlDAO = new SolarSensorValueMsSqlDAO(solarSensor, i);

			try {
				Object result = SqlConfig.getSqlMapInstance().queryForObject("selectSolarValue_MsSql", solarSensorValue_MsSqlDAO);
				if (result == null) {
					SqlConfig.getSqlMapInstance().insert("insertSolarSensorValue_MsSql", solarSensorValue_MsSqlDAO);
				} else {
					SqlConfig.getSqlMapInstance().update("updateSolarSensorValue_MsSql", solarSensorValue_MsSqlDAO);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String args[]) {
		for (int i = 0; i < 16; i++) {
			SolarSensorValueMsSqlDAO solarSensorValue_MsSqlDAO = new SolarSensorValueMsSqlDAO();
			solarSensorValue_MsSqlDAO.setSerialNo("" + i);
			solarSensorValue_MsSqlDAO.setVoltage((float) (i / 100.0));
			try {
				Object result = SqlConfig.getSqlMapInstance().queryForObject("selectSolarValue_MsSql", solarSensorValue_MsSqlDAO);
				if (result == null) {
					SqlConfig.getSqlMapInstance().insert("insertSolarSensorValue_MsSql", solarSensorValue_MsSqlDAO);
				} else {
					SqlConfig.getSqlMapInstance().update("updateSolarSensorValue_MsSql", solarSensorValue_MsSqlDAO);
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
