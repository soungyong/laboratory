package m2m.smartcon3.database.info;

public class ReportPowerMeterStateDAOInfo {

	// 응답 시간
	private byte time[] = new byte[14];

	// 게이트웨이 아이디
	private byte gatewayId;

	// 센서 GID
	private byte sensorGid;
	// 센서 SID
	private byte sensorSid;

	// 미터 누적 값, 새로운 값.
	private int meterStackValue;

	// 미터 차이 값
	private int meterDifferenceValue;

	public byte[] getTime() {
		return time;
	}

	public void setTime(byte[] time) {
		this.time = time;
	}

	public byte getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(byte gatewayId) {
		this.gatewayId = gatewayId;
	}

	public byte getSensorGid() {
		return sensorGid;
	}

	public void setSensorGid(byte sensorGid) {
		this.sensorGid = sensorGid;
	}

	public byte getSensorSid() {
		return sensorSid;
	}

	public void setSensorSid(byte sensorSid) {
		this.sensorSid = sensorSid;
	}

	public int getMeterStackValue() {
		return meterStackValue;
	}

	public void setMeterStackValue(int meterStackValue) {
		this.meterStackValue = meterStackValue;
	}

	public int getMeterDifferenceValue() {
		return meterDifferenceValue;
	}

	public void setMeterDifferenceValue(int meterDifferenceValue) {
		this.meterDifferenceValue = meterDifferenceValue;
	}

	public ReportPowerMeterStateDAOInfo() {
		super();
	}

}
