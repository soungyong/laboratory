package m2m.smartcon3.database.info;

public class GetGWStateDAOInfo {
	// 0x01로 고정.
	private final byte msg_format = 0x01;

	private byte time[] = new byte[14];

	// 상태코드
	private short state_code;

	// 점등 예정 시각
	private byte reserved_turn_on_hour;
	private byte reserved_turn_on_minute;
	private byte reserved_turn_on_second;

	// 소등 예정 시각
	private byte reserved_turn_off_hour;
	private byte reserved_turn_off_minute;
	private byte reserved_turn_off_second;

	// 현재 동작 모드(0자동(스케쥴대로동작) 1 수동(누군가가조작하였음))
	private byte running_mode;
	// 현재 점등모드 (0 전체소등 1 전체점등 2격등)
	private byte turn_on_mode;

	// 마그네틱스위치개수
	private byte msw_num;
	// 마그네틱스위치상태 (1 켜짐 0 꺼짐)
	private byte[] msw_state = new byte[8];

	// 분전반문상태 0 닫힘 1 열림
	private byte door_state;

	// 현재 디밍 레벨 상태, default : 모름
	private byte dimming_level = (byte) 0xff;

}
