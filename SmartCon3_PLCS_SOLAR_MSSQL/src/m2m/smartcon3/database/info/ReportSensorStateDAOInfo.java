package m2m.smartcon3.database.info;

public class ReportSensorStateDAOInfo {

	// 응답 시간
	private byte time[] = new byte[14];

	// 게이트웨이 아이디
	private byte gatewayId;

	// 센서 GID
	private byte sensorGid;
	// 센서 SID
	private byte sensorSid;

	// 온도
	private float temperature;

	// 습도
	private float humidity;

	// 조도
	private float lux;

	// 동작 센서
	private float motion;

	// 동작 센서 측정 가능성
	private int motionPossibility;

	// 노드 값
	private int node_id;

	public int getMotionPossibility() {
		return motionPossibility;
	}

	public void setMotionPossibility(int motionPossibility) {
		this.motionPossibility = motionPossibility;
	}

	public byte getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(byte gatewayId) {
		this.gatewayId = gatewayId;
	}

	public byte[] getTime() {
		return time;
	}

	public void setTime(byte[] time) {
		this.time = time;
	}

	public byte getSensorGid() {
		return sensorGid;
	}

	public void setSensorGid(byte sensorGid) {
		this.sensorGid = sensorGid;
	}

	public byte getSensorSid() {
		return sensorSid;
	}

	public void setSensorSid(byte sensorSid) {
		this.sensorSid = sensorSid;
	}

	public float getTemperature() {
		return temperature;
	}

	public void setTemperature(float temperature) {
		this.temperature = temperature;
	}

	public float getHumidity() {
		return humidity;
	}

	public void setHumidity(float humidity) {
		this.humidity = humidity;
	}

	public float getLux() {
		return lux;
	}

	public void setLux(float lux) {
		this.lux = lux;
	}

	public float getMotion() {
		return motion;
	}

	public void setMotion(float motion) {
		this.motion = motion;
	}

	public int getNode_id() {
		return node_id;
	}

	public void setNode_id(int nodeId) {
		node_id = nodeId;
	}

	public ReportSensorStateDAOInfo() {
		super();
	}

}
