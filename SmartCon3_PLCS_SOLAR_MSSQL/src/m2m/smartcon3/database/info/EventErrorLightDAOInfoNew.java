package m2m.smartcon3.database.info;

import m2m.util.StringUtil;

public class EventErrorLightDAOInfoNew {

	private byte[] trouble_time = new byte[14];
	private byte[] trouble_time_last = new byte[14];
	private byte event_reason;
	private byte trouble_device;
	private int gw_id;
	private byte node_gid;
	private byte node_sid;
	private byte lamp_id;
	private byte trouble_state;
	private String trouble_handling_state;
	private byte error_type;

	public byte getError_type() {
		return error_type;
	}

	public void setError_type(byte errorType) {
		error_type = errorType;
	}

	public byte[] getTrouble_time() {
		return trouble_time;
	}

	public void setTrouble_time(byte[] troubleTime) {
		trouble_time = troubleTime;
	}

	public byte[] getTrouble_time_last() {
		return trouble_time_last;
	}

	public void setTrouble_time_last(byte[] troubleTimeLast) {
		trouble_time_last = troubleTimeLast;
	}

	public byte getEvent_reason() {
		return event_reason;
	}

	public void setEvent_reason(byte eventReason) {
		event_reason = eventReason;
	}

	public byte getTrouble_device() {
		return trouble_device;
	}

	public void setTrouble_device(byte troubleDevice) {
		trouble_device = troubleDevice;
	}

	public int getGw_id() {
		return gw_id;
	}

	public void setGw_id(int gwId) {
		gw_id = gwId;
	}

	public byte getNode_gid() {
		return node_gid;
	}

	public void setNode_gid(byte nodeGid) {
		node_gid = nodeGid;
	}

	public byte getNode_sid() {
		return node_sid;
	}

	public void setNode_sid(byte nodeSid) {
		node_sid = nodeSid;
	}

	public byte getLamp_id() {
		return lamp_id;
	}

	public void setLamp_id(byte lampId) {
		lamp_id = lampId;
	}

	public byte getTrouble_state() {
		return trouble_state;
	}

	public void setTrouble_state(byte troubleState) {
		trouble_state = troubleState;
	}

	public String getTrouble_handling_state() {
		return trouble_handling_state;
	}

	public void setTrouble_handling_state(String troubleHandlingState) {
		trouble_handling_state = troubleHandlingState;
	}

	public EventErrorLightDAOInfoNew(byte[] troubleTime, byte[] troubleTimeLast, byte eventReason, byte troubleDevice, int gwId, byte nodeGid, byte nodeSid,
			byte lampId, byte troubleState, String troubleHandlingState) {
		super();
		trouble_time = troubleTime;
		trouble_time_last = troubleTimeLast;
		event_reason = eventReason;
		trouble_device = troubleDevice;
		gw_id = gwId;
		node_gid = nodeGid;
		node_sid = nodeSid;
		lamp_id = lampId;
		trouble_state = troubleState;
		trouble_handling_state = troubleHandlingState;
	}

	public EventErrorLightDAOInfoNew() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String toString() {
		return "GW_ID " + this.gw_id + ", node_gid " + node_gid + ", node_sid " + node_sid + ", lamp_id " + lamp_id + ", event_reason 0x"
				+ StringUtil.toHexString(event_reason) + ", trouble_state 0x" + StringUtil.toHexString(trouble_state);
	}

}
