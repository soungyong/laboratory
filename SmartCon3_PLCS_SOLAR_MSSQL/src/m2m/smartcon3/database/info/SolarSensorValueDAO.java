package m2m.smartcon3.database.info;

import java.util.Date;

import m2m.smartcon3.device.SolarSensorGetter;

public class SolarSensorValueDAO {
	private Date time;
	private String deviceId;

	private int deviceIdDec;

	private int numSensor;

	private float sensorValue_0;
	private float sensorValue_1;
	private float sensorValue_2;
	private float sensorValue_3;
	private float sensorValue_4;
	private float sensorValue_5;
	private float sensorValue_6;
	private float sensorValue_7;
	private float sensorValue_8;
	private float sensorValue_9;
	private float sensorValue_10;
	private float sensorValue_11;
	private float sensorValue_12;
	private float sensorValue_13;
	private float sensorValue_14;
	private float sensorValue_15;

	public SolarSensorValueDAO(SolarSensorGetter solarSensor) {
		this.time = solarSensor.getLastSensorValueReceivedTime().getTime();
		this.deviceId = String.format("0X%04X", solarSensor.getId());
		this.deviceIdDec = solarSensor.getId();

		this.numSensor = solarSensor.getNumOfSensor();

		this.sensorValue_0 = (float) (solarSensor.getSensorValue()[0] / 100.0);
		this.sensorValue_1 = (float) (solarSensor.getSensorValue()[1] / 100.0);
		this.sensorValue_2 = (float) (solarSensor.getSensorValue()[2] / 100.0);
		this.sensorValue_3 = (float) (solarSensor.getSensorValue()[3] / 100.0);
		this.sensorValue_4 = (float) (solarSensor.getSensorValue()[4] / 100.0);
		this.sensorValue_5 = (float) (solarSensor.getSensorValue()[5] / 100.0);
		this.sensorValue_6 = (float) (solarSensor.getSensorValue()[6] / 100.0);
		this.sensorValue_7 = (float) (solarSensor.getSensorValue()[7] / 100.0);
		this.sensorValue_8 = (float) (solarSensor.getSensorValue()[8] / 100.0);
		this.sensorValue_9 = (float) (solarSensor.getSensorValue()[9] / 100.0);
		this.sensorValue_10 = (float) (solarSensor.getSensorValue()[10] / 100.0);
		this.sensorValue_11 = (float) (solarSensor.getSensorValue()[11] / 100.0);
		this.sensorValue_12 = (float) (solarSensor.getSensorValue()[12] / 100.0);
		this.sensorValue_13 = (float) (solarSensor.getSensorValue()[13] / 100.0);
		this.sensorValue_14 = (float) (solarSensor.getSensorValue()[14] / 100.0);
		this.sensorValue_15 = (float) (solarSensor.getSensorValue()[15] / 100.0);
	}

	public int getNumSensor() {
		return numSensor;
	}

	public float getSensorValue_0() {
		return sensorValue_0;
	}

	public float getSensorValue_1() {
		return sensorValue_1;
	}

	public float getSensorValue_2() {
		return sensorValue_2;
	}

	public float getSensorValue_3() {
		return sensorValue_3;
	}

	public float getSensorValue_4() {
		return sensorValue_4;
	}

	public float getSensorValue_5() {
		return sensorValue_5;
	}

	public float getSensorValue_6() {
		return sensorValue_6;
	}

	public float getSensorValue_7() {
		return sensorValue_7;
	}

	public float getSensorValue_8() {
		return sensorValue_8;
	}

	public float getSensorValue_9() {
		return sensorValue_9;
	}

	public float getSensorValue_10() {
		return sensorValue_10;
	}

	public float getSensorValue_11() {
		return sensorValue_11;
	}

	public float getSensorValue_12() {
		return sensorValue_12;
	}

	public float getSensorValue_13() {
		return sensorValue_13;
	}

	public float getSensorValue_14() {
		return sensorValue_14;
	}

	public float getSensorValue_15() {
		return sensorValue_15;
	}

	public Date getTime() {
		return time;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public int getDeviceIdDec() {
		return deviceIdDec;
	}
}
