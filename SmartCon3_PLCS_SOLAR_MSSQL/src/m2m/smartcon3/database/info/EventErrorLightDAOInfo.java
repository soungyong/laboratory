package m2m.smartcon3.database.info;

import java.io.Serializable;

public class EventErrorLightDAOInfo implements Serializable {

	private byte[] date_time = new byte[14];
	private int gw_id;
	private byte gw_state;
	private byte node_group_id;
	private byte node_id;
	private byte event_reason;
	private byte node_sate;
	private byte lamp_count;
	private byte lamp_state_1;
	private byte lamp_control_1;
	private byte lamp_state_2;
	private byte lamp_control_2;
	private byte lamp_state_3;
	private byte lamp_control_3;

	public byte[] getDate_time() {
		return date_time;
	}

	public void setDate_time(byte[] dateTime) {
		date_time = dateTime;
	}

	public int getGw_id() {
		return gw_id;
	}

	public void setGw_id(int gwId) {
		gw_id = gwId;
	}

	public byte getGw_state() {
		return gw_state;
	}

	public void setGw_state(byte gwState) {
		gw_state = gwState;
	}

	public byte getNode_group_id() {
		return node_group_id;
	}

	public void setNode_group_id(byte nodeGroupId) {
		node_group_id = nodeGroupId;
	}

	public byte getNode_id() {
		return node_id;
	}

	public void setNode_id(byte nodeId) {
		node_id = nodeId;
	}

	public byte getEvent_reason() {
		return event_reason;
	}

	public void setEvent_reason(byte eventReason) {
		event_reason = eventReason;
	}

	public byte getNode_sate() {
		return node_sate;
	}

	public void setNode_sate(byte nodeSate) {
		node_sate = nodeSate;
	}

	public byte getLamp_count() {
		return lamp_count;
	}

	public void setLamp_count(byte lampCount) {
		lamp_count = lampCount;
	}

	public byte getLamp_state_1() {
		return lamp_state_1;
	}

	public void setLamp_state_1(byte lampState_1) {
		lamp_state_1 = lampState_1;
	}

	public byte getLamp_control_1() {
		return lamp_control_1;
	}

	public void setLamp_control_1(byte lampControl_1) {
		lamp_control_1 = lampControl_1;
	}

	public byte getLamp_state_2() {
		return lamp_state_2;
	}

	public void setLamp_state_2(byte lampState_2) {
		lamp_state_2 = lampState_2;
	}

	public byte getLamp_control_2() {
		return lamp_control_2;
	}

	public void setLamp_control_2(byte lampControl_2) {
		lamp_control_2 = lampControl_2;
	}

	public byte getLamp_state_3() {
		return lamp_state_3;
	}

	public void setLamp_state_3(byte lampState_3) {
		lamp_state_3 = lampState_3;
	}

	public byte getLamp_control_3() {
		return lamp_control_3;
	}

	public void setLamp_control_3(byte lampControl_3) {
		lamp_control_3 = lampControl_3;
	}

	public EventErrorLightDAOInfo(byte[] dateTime, int gwId, byte gwState, byte nodeGroupId, byte nodeId, byte eventReason, byte nodeSate, byte lampCount,
			byte lampState_1, byte lampControl_1, byte lampState_2, byte lampControl_2, byte lampState_3, byte lampControl_3) {
		super();
		date_time = dateTime;
		gw_id = gwId;
		gw_state = gwState;
		node_group_id = nodeGroupId;
		node_id = nodeId;
		event_reason = eventReason;
		node_sate = nodeSate;
		lamp_count = lampCount;
		lamp_state_1 = lampState_1;
		lamp_control_1 = lampControl_1;
		lamp_state_2 = lampState_2;
		lamp_control_2 = lampControl_2;
		lamp_state_3 = lampState_3;
		lamp_control_3 = lampControl_3;
	}

	public EventErrorLightDAOInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

}
