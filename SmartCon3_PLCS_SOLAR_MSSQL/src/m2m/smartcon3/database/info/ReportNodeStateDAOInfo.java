package m2m.smartcon3.database.info;

public class ReportNodeStateDAOInfo {

	private int gw_id;

	private byte[] time = new byte[14];

	private int node_gid;

	private int node_sid;

	private int node_state;

	private int num_lamp;

	private int lamp_id;

	private int lamp_general_state;

	private int lamp_control_state;

	public int getGw_id() {
		return gw_id;
	}

	public void setGw_id(int gwId) {
		gw_id = gwId;
	}

	public byte[] getTime() {
		return time;
	}

	public void setTime(byte[] time) {
		this.time = time;
	}

	public int getNode_gid() {
		return node_gid;
	}

	public void setNode_gid(int nodeGid) {
		node_gid = nodeGid;
	}

	public int getNode_sid() {
		return node_sid;
	}

	public void setNode_sid(int nodeSid) {
		node_sid = nodeSid;
	}

	public int getNode_state() {
		return node_state;
	}

	public void setNode_state(int nodeState) {
		node_state = nodeState;
	}

	public int getNum_lamp() {
		return num_lamp;
	}

	public void setNum_lamp(int numLamp) {
		num_lamp = numLamp;
	}

	public int getLamp_id() {
		return lamp_id;
	}

	public void setLamp_id(int lampId) {
		lamp_id = lampId;
	}

	public int getLamp_general_state() {
		return lamp_general_state;
	}

	public void setLamp_general_state(int lampGeneralState) {
		lamp_general_state = lampGeneralState;
	}

	public int getLamp_control_state() {
		return lamp_control_state;
	}

	public void setLamp_control_state(int lampControlState) {
		lamp_control_state = lampControlState;
	}

	public String toString() {
		return "Gw_id " + gw_id + ", node_gid " + node_gid + ", node_sid " + node_sid + ", lamp_id " + lamp_id + ", gen_state " + lamp_general_state
				+ ", con_state " + lamp_control_state;
	}

}
