package m2m.smartcon3.database.info;

import java.text.SimpleDateFormat;
import java.util.Date;

import m2m.smartcon3.device.SolarSensorGetter;

public class SolarSensorValueMsSqlDAO {
	private String moduleDiv;
	private String serialNo;
	private float voltage;
	private String status;
	private Date updateDT;
	private String measure1;

	public SolarSensorValueMsSqlDAO(SolarSensorGetter solarSensor, int channel) {
		this.moduleDiv = "PV";
		// this.serialNo = "" + solarSensor.getId() + "-" + (channel + 1);
		// 2014 11 24 ����
		this.serialNo = String.format("%04X", solarSensor.getId()) + "-" + String.format("%03d", (channel + 1));
		this.voltage = (float) (solarSensor.getSensorValue()[channel] / 100.0);
		this.status = "W";
		this.updateDT = solarSensor.getLastSensorValueReceivedTime().getTime();
		// 2014 11 24 �߰�
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		this.measure1 = format.format(this.updateDT);
	}

	public SolarSensorValueMsSqlDAO() {
		this.moduleDiv = "PV";
		this.serialNo = "0";
		this.voltage = 0;
		this.status = "W";
		this.updateDT = new Date();
	}

	public String getModuleDiv() {
		return moduleDiv;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public float getVoltage() {
		return voltage;
	}

	public String getStatus() {
		return status;
	}

	public Date getUpdateDT() {
		return updateDT;
	}

	public void setModuleDiv(String moduleDiv) {
		this.moduleDiv = moduleDiv;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public void setVoltage(float voltage) {
		this.voltage = voltage;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setUpdateDT(Date updateDT) {
		this.updateDT = updateDT;
	}

	public void setMeasure1(String measure1) {
		this.measure1 = measure1;
	}

	public String getMeasure1() {
		return measure1;
	}

}
