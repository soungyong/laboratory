package m2m.smartcon3.database;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import m2m.smartcon3.database.info.EventErrorLightDAOInfo;
import m2m.smartcon3.database.info.EventErrorLightDAOInfoNew;
import m2m.smartcon3.database.info.ReportNodeStateDAOInfo;
import m2m.smartcon3.database.info.ReportPowerMeterStateDAOInfo;
import m2m.smartcon3.database.info.ReportSensorStateDAOInfo;

import com.ibatis.sqlmap.client.SqlMapClient;

public class SlcDAO {

	protected SqlMapClient sqlMapClient;

	public SlcDAO(SqlMapClient sqlMapClient) throws Exception {
		this.sqlMapClient = sqlMapClient;
	}

	// 2010/09/30 �߰�
	public void addEventErrorLight(EventErrorLightDAOInfoNew param) throws SQLException {
		sqlMapClient.insert("addEventErrorLightNew", param);
		return;
	}

	public String getGwCdmaNumber(int gwId) throws SQLException {
		String cdmaNumber = (String) sqlMapClient.queryForObject("getCdmaNumber", gwId);
		return cdmaNumber;
	}

	@SuppressWarnings("unchecked")
	public HashMap getGwState(int gwId) throws SQLException {
		HashMap result = (HashMap) sqlMapClient.queryForObject("getGwState", gwId);
		return result;
	}

	public int getEventErrorLight(EventErrorLightDAOInfo tDaoInfo) throws SQLException {
		int result = (Integer) sqlMapClient.queryForObject("getEventErrorLight", tDaoInfo);
		return result;
	}

	public int getEventErrorLight(EventErrorLightDAOInfoNew tDaoInfo) throws SQLException {
		int result = (Integer) sqlMapClient.queryForObject("getEventErrorLightNew", tDaoInfo);
		return result;
	}

	// 2010/09/30
	@Deprecated
	public void updateEventErrorLight(EventErrorLightDAOInfo tDaoInfo) throws SQLException {
		sqlMapClient.update("updateEventErrorLight", tDaoInfo);
		return;
	}

	// 2010/09/30 �߰�
	public void updateEventErrorLight(EventErrorLightDAOInfoNew param) throws SQLException {
		sqlMapClient.update("updateEventErrorLightNew", param);
		return;
	}

	@SuppressWarnings("unchecked")
	public List getNodeStateAll(int gwId) throws SQLException {
		List result = sqlMapClient.queryForList("getNodeStateAll", gwId);
		return result;
	}

	public int getReportStateRecentCount(int gwId) throws SQLException {
		return (Integer) sqlMapClient.queryForObject("getReportStateRecentCount", gwId);
	}

	public int addReportSensorStateEnv(ReportSensorStateDAOInfo param) throws SQLException {
		sqlMapClient.insert("addReportSensorStateEnv", param);
		return 0;
	}

	public int addReportSensorStateMotion(ReportSensorStateDAOInfo param) throws SQLException {
		sqlMapClient.insert("addReportSensorStateMotion", param);
		return 0;
	}

	public int getNodeIdForReportSensorStateInsert(ReportSensorStateDAOInfo param) throws SQLException {
		return (Integer) sqlMapClient.queryForObject("getNodeIdForReportSensorStateInsert", param);
	}

	public int getWhHhValueRear(ReportPowerMeterStateDAOInfo param) throws SQLException {
		return (Integer) sqlMapClient.queryForObject("getWhHhValueRear", param);
	}

	public void addWhHhValue(ReportPowerMeterStateDAOInfo param) throws SQLException {
		sqlMapClient.insert("addWhHhValue", param);
	}

	public int updateNodeStateRecent(ReportNodeStateDAOInfo tDaoInfo) throws SQLException {
		return sqlMapClient.update("updateNodeStateRecent", tDaoInfo);
	}
}