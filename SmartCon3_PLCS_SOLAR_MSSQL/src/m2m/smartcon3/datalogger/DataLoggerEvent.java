/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/27.
*/
package m2m.smartcon3.datalogger;


import m2m.smartcon3.datalogger.DefaultData;
import m2m.smartcon3.event.DefaultEvent;

/**
 * 데이터 로거 이벤트.
 * @note	이 이벤트는 DataLogger가 로깅을 할 때 발생시킨다.
 */
public class DataLoggerEvent extends DefaultEvent
{	
	/**
	 * By default, 
	 * @param data
	 */
	public DataLoggerEvent(DefaultData data)
	{
		//super((short)0x0000, 0, data.getName(), data.getParameter());
//		super(data.getDeviceType(), data.getDeviceID(), data.getName(), data.getParameter());
		
		// 07/30
		super(data.getDevice(), data.getName(), data.getParameter());
	}	
	public String toString()
	{
		try
		{
			//DefaultData	data = (DefaultData)//getParameter();
			return getName() + "," + getParameter().toString();			
		}
		catch (Exception e)
		{
			return getParameter().getClass().getName();
		}
	}	
}
