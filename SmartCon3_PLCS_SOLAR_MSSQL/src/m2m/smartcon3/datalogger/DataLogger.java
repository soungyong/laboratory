/**
 * Log sensor data to database
 * - power meter sensor
 * - lumination sensor
 */
package m2m.smartcon3.datalogger;

import m2m.smartcon3.event.DefaultEvent;

/**
 * @author Administrator
 *
 */
public interface DataLogger
{
	/**
	 * Log data to database
	 */
	//public void log(String name, Object data)	throws SmartException;

	/**
	 * Log data to database
	 */
	//public void addData(DefaultData data)	throws SmartException;
	
	
	/**
	 * 로거는 각 이벤트에 맞는 로그를 정의해야 한다.
	 * 
	 * ex) 센서 데이터 이벤트에 대해서는 센서 데이터를 로깅한다던지... 하는 정의는 각 패키지에서 해야한다.
	 */
	public void handleEvent(DefaultEvent event);	//throws SmartException;
	
	
	
	public void startUp() ; //throws SmartException;

	public void shutDown();

}
