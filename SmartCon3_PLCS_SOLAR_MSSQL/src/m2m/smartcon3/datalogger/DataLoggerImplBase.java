/**
 * Log sensor data to database
 * - power meter sensor
 * - lumination sensor
 */
package m2m.smartcon3.datalogger;

import java.lang.reflect.Method;
import java.util.LinkedList;

import m2m.smartcon3.event.DefaultEvent;

import org.apache.log4j.Logger;

/**
 * Database logger implementation base class
 */
public abstract class DataLoggerImplBase implements DataLogger
{
	public static final Logger logger = Logger.getLogger("DataLoggerImplBase");

	//LinkedList dataQueue;
	LinkedList<DefaultData> dataQueue;	// JDK6 update

	//LinkedList<WBEvent> eq;
	Thread dataLoggingThread;
	
	boolean dataLoggingRunning;

	public DataLoggerImplBase()
	{
		//dataQueue = new LinkedList();
		dataQueue = new LinkedList<DefaultData>();		// JDK6 update
	}
	//SampleDataLogger
  
	/**
	 * Log data to database
	 * ex. log("addMeterLog", meterInfo);
	 */
	public void log(String logFunctionName, Object param)
		//throws SmartException
	{
		doLog(logFunctionName, param);
	}
	
	/**
	 * Log data to database
	 * 
	 * @param data	name should be logger function name
	 * 						data.parameter는 로거 메소드에 맞는 인수여야 함.
	 */
	protected final void addData(DefaultData data)//	throws SmartException
	{
		if (dataLoggingRunning)
		{
			//TODO:
			//logger.info("Event added : " + e.getClass().getName());
			synchronized(dataQueue)
			{
				dataQueue.add(data);
				
				dataQueue.notify();
				//logger.info("Event added post: " + e.getClass().getName());
			}
			//return 	true;	
		}
		else
		{
			//
		}
	}
	
	/**
	 * 로거는 각 이벤트에 맞는 로그를 정의해야 한다.
	 * 
	 * ex) 센서 데이터 이벤트에 대해서는 센서 데이터를 로깅한다던지... 하는 정의는 각 패키지에서 해야한다.
	 */
	public abstract void handleEvent(DefaultEvent event);//	throws SmartException;


	
	public void startUp()// throws SmartException
	{
		if (dataLoggingRunning)
		{
			//throw new SmartException("DataLogger already started");
		}

		//
		//logger.info("DataLoggerImplBase.thread starting..");

		dataLoggingThread = new Thread(new Runnable(){
			public void run()
			{
				DefaultData	data = null;
				
				while (dataLoggingRunning)
				{
					try
					{
						synchronized(dataQueue)
						{
							data = (DefaultData)dataQueue.poll();		// and wait..
						}
						
						if (data != null)
						{
							//try
							//{
							//find event handler and dispatch
								doLog(data.getName(), data.getParameter());
					
								// 로깅한 후에 이벤트 생성.
								//new DataLoggerEvent("Log OK", data);	// TODO.
								new DataLoggerEvent(data);
								
							//logger.info("polling new Event");
							//}
							//catch (SmartException e)
							//{
								// TODO
							//	e.printStackTrace();
							//}
						}
						else
						{
							synchronized(dataQueue)
							{
								dataQueue.wait();
							}
						}
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			}
		});

		dataLoggingRunning = true;
		dataLoggingThread.start();
		
		//logger.info("DataLoggerImplBase.thread end..");
		
		
	}

	/**
	 * Shutdown event pump.
	 */
	public void shutDown()
	{
		// TODO: wait while logging done.
		if (dataLoggingRunning)
		{
			dataLoggingRunning = false;
	
			synchronized(dataQueue)
			{
				dataQueue.notify();
			}
		}
	}

	/**
	 * Really do log*
	 *
	 */
	@SuppressWarnings("unchecked")
	protected void doLog(String logFunctionName, Object param)
		//throws SmartException
	{
		try
		{
			//Class thisClass = this.getClass();
			Class<? extends DataLoggerImplBase> thisClass = this.getClass();		// JDK6 update.
			
			//Class<? extends Object> cp[] = new Class<? extends Object>[1];
			Class<? extends Object> cp[] = new Class[1];
			cp[0] = param.getClass();
			
			Method m = thisClass.getMethod(logFunctionName, cp);
			if (m == null)
			{
				//throw new SmartException("method not found");
			}
	
			Object params[] = new Object[1];
			params[0] = param;
			
			m.invoke(this, params);
		}
		catch (Exception e)
		{
			//e.printStackTrace();
			// 주) getMethod에서 메소드가 없을 경우에 NoSuchMethodException이 발생함.
			// TODO:
			//throw new SmartException("ERROR :" + logFunctionName + " fail");
			//e.printStackTrace();
		}
	}
}
