/**
	SMART-CON 2  v1.2

	@author	tchan@m2mkorea.co.kr
	@date 2010/05/26
 */
package m2m.smartcon3.datalogger;

import m2m.smartcon3.device.I_Device;
import m2m.smartcon3.event.I_Event;

/**
 * DefaultData 는 데이터베이스 로깅을 위한 이벤트 데이터를 감싸는 클래스이다.
 * 
 * 사용법: 이벤트 핸들러에서 노드의 이벤트를 받아서 처리하고, 로깅할 필요가 있을 경우, DefaultData로 변환하여
 * DataLogger로 넘겨준다.
 * 
 * - 장치타입, id 등 데이터 소스 정보 추가.
 * 
 */
public class DefaultData {
	I_Device srcDevice;
	// short deviceType;
	// int deviceID;

	String name;
	Object param;

	/**
	 * 주로 name은 로깅 function name, param은 로깅할 데이터로 표시된다.
	 * 
	 * @param name
	 * @param param
	 * @deprecated
	 */
	public DefaultData(String name, Object param) {
		this.name = name;
		this.param = param;
	}

	public DefaultData(I_Device device, String name, Object param) {
		this.srcDevice = device;
		// this.deviceType = deviceType;
		// this.deviceID = deviceID;
		this.name = name;
		this.param = param;
	}

	/**
	 * @deprecated
	 * @param deviceType
	 * @param deviceID
	 * @param name
	 * @param param
	 */
	public DefaultData(short deviceType, int deviceID, String name, Object param) {
		// this.deviceType = deviceType;
		// this.deviceID = deviceID;
		this.name = name;
		this.param = param;
	}

	/**
	 * @deprecated
	 * @param ev
	 */
	public DefaultData(I_Event ev) {
		this(ev.getName(), ev.getParameter());
	}

	public String getName() {
		return name;
	}

	public Object getParameter() {
		return param;
	}

	public String toString() {
		try {
			// DefaultData data = (DefaultData)//getParameter();
			return getName() + "," + getParameter().toString();
		} catch (Exception e) {
			return getParameter().getClass().getName();
		}
	}

	public I_Device getDevice() {
		return srcDevice;
	}

	/**
	 * @deprecated use getDevice
	 * @return
	 */
	public int getDeviceID() {
		// return deviceID;
		return -1;
	}

	/**
	 * @deprecated use getDevice
	 * @param deviceID
	 */
	public void setDeviceID(int deviceID) {
		// this.deviceID = deviceID;
		// return -1;
	}

	/**
	 * @deprecated use getDevice
	 * @return
	 */
	public short getDeviceType() {
		// return deviceType;
		return -1;
	}

	/**
	 * @deprecated use getDevice
	 */
	public void setDeviceType(short deviceType) {
		// this.deviceType = deviceType;
	}
}
