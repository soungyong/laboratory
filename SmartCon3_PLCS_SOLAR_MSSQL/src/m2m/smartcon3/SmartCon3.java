package m2m.smartcon3;

import java.io.IOException;
import java.rmi.RemoteException;

import m2m.smartcon3.core.DeviceManager;
import m2m.smartcon3.core.EventManager;
import m2m.smartcon3.devicenetwork.XNetManager;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.event.SensorEvent;
import m2m.smartcon3.event.SensorEventHandler;
import m2m.smartcon3.event.SolarSensorEvent;
import m2m.smartcon3.event.SolarSensorEventHandler;
import m2m.smartcon3.event.SolarSensorMSSQLEvent;
import m2m.smartcon3.event.SolarSensorMsSqlEventHandler;
import m2m.smartcon3.event.SynchronizedMessageEventHandler;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.provider.RMIProviderForRemoteFirmwareUpgrade;
import m2m.smartcon3.provider.RMIProviderForWeb;
import m2m.smartcon3.provider.RMIProviderForWeb2;
import m2m.util.MyLog;

public class SmartCon3 {
	public static void main(String args[]) throws InterruptedException, UnformattedPacketException {
		MyLog.getInstance().setDebuggingLevel(MyLog.WARNING);
		MyLog.l("Version.1.04");
		// 1.04 MPR 추가.

		DeviceManager deviceManager = DeviceManager.getInstance();
		EventManager eventManager = EventManager.getInstance();

		SynchronizedMessageEventHandler smEventHandler = SynchronizedMessageEventHandler.getInstance();
		eventManager.registerEventHandler(FailToSendMessageEvent.class.getName(), smEventHandler);
		eventManager.registerEventHandler(ReceiveResponseMessageEvent.class.getName(), smEventHandler);

		SensorEventHandler sensorEventHandler = new SensorEventHandler();
		eventManager.registerEventHandler(SensorEvent.class.getName(), sensorEventHandler);

		eventManager.registerEventHandler(SolarSensorEvent.class.getName(), new SolarSensorEventHandler());
		eventManager.registerEventHandler(SolarSensorMSSQLEvent.class.getName(), new SolarSensorMsSqlEventHandler());

		try {
			RMIProviderForWeb rmiProviderForWeb = RMIProviderForWeb.getInstance();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			MyLog.e(e1.getMessage());
		}

		try {
			RMIProviderForWeb2 rmiProviderForWeb2 = RMIProviderForWeb2.getInstance();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			MyLog.e(e1.getMessage());
		}

		try {
			RMIProviderForRemoteFirmwareUpgrade rmiProviderForRemoteFirmwareUpgrade = RMIProviderForRemoteFirmwareUpgrade.getInstance();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			MyLog.e(e1.getMessage());
		}

		// SocketProviderForAndroid.getInstance(); //Only for 우송대.

		try {
			XNetManager.getInstance(30011);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			MyLog.e(e.getMessage());
		}

		while (true) {
			System.gc();
			Thread.sleep(1000 * 60 * 30);
		}
	}
}
