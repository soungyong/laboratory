package m2m.smartcon3.exception;

/**
 * 패킷 포맷 틀림 예외.
 * @author funface2
 *
 */
public class UnformattedPacketException extends Exception{
	String msg="";
	public UnformattedPacketException() {		
	}
	
	public UnformattedPacketException(String msg) {
		this.msg = msg;
	}
	
	public String toString() {
		return super.toString()+": "+this.msg;
	}
}
