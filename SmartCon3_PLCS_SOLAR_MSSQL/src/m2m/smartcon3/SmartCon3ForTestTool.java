package m2m.smartcon3;

import java.rmi.RemoteException;
import java.util.Enumeration;

import javax.comm.CommPortIdentifier;

import m2m.smartcon3.core.DeviceManager;
import m2m.smartcon3.core.EventManager;
import m2m.smartcon3.event.FailToSendMessageEvent;
import m2m.smartcon3.event.ReceiveResponseMessageEvent;
import m2m.smartcon3.event.SensorEvent;
import m2m.smartcon3.event.SensorEventHandler;
import m2m.smartcon3.event.SynchronizedMessageEventHandler;
import m2m.smartcon3.exception.UnformattedPacketException;
import m2m.smartcon3.provider.RMIProviderForWeb;
import m2m.smartcon3.tools.device.TestNode;
import m2m.smartcon3.tools.devicenetwork.SerialXNetHandler;
import m2m.util.MyLog;

public class SmartCon3ForTestTool {
	public static void main(String args[]) throws InterruptedException,
			UnformattedPacketException {
		DeviceManager deviceManager = DeviceManager.getInstance();		
		EventManager eventManager = EventManager.getInstance();		
		SynchronizedMessageEventHandler smEventHandler = SynchronizedMessageEventHandler.getInstance();
		eventManager.registerEventHandler(FailToSendMessageEvent.class.getName(), smEventHandler);
		eventManager.registerEventHandler(ReceiveResponseMessageEvent.class.getName(), smEventHandler);
		
		SensorEventHandler sensorEventHandler = new SensorEventHandler();
		eventManager.registerEventHandler(SensorEvent.class.getName(), sensorEventHandler);
		try {
			RMIProviderForWeb rmiProviderForWeb = RMIProviderForWeb.getInstance();
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			MyLog.e(e1.getMessage());
		}

		TestNode node = autoOpenComPort();
		deviceManager.addDevice(node);
				
		while(true) {
			System.gc();
			Thread.sleep(1000*60*30);			
		}
	}
	
	protected static TestNode autoOpenComPort() {
		TestNode node = TestNode.getInstance();
		SerialXNetHandler xnetHandler = null;

		Enumeration ports = CommPortIdentifier.getPortIdentifiers();
		while (ports.hasMoreElements()) {
			CommPortIdentifier port = (CommPortIdentifier) ports.nextElement();
			if (port.getPortType() == CommPortIdentifier.PORT_SERIAL) {
				xnetHandler = new SerialXNetHandler(port.getName(), node);
				node.setXNetHandler(xnetHandler);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (node.getPanId() != (short) 0xffff)
					break;
				xnetHandler.terminate();
				xnetHandler = null;
			}
		}

		if (xnetHandler == null || xnetHandler.isConnected() == false){
			MyLog.e("ComPort ���� ����");
			return null;
		}
		
		return node;
	}
}
