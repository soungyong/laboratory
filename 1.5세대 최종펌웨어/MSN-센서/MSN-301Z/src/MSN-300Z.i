
#pragma used+
sfrb PINB=3;
sfrb DDRB=4;
sfrb PORTB=5;
sfrb PINC=6;
sfrb DDRC=7;
sfrb PORTC=8;
sfrb PIND=9;
sfrb DDRD=0xa;
sfrb PORTD=0xb;
sfrb TIFR0=0x15;
sfrb TIFR1=0x16;
sfrb TIFR2=0x17;
sfrb PCIFR=0x1b;
sfrb EIFR=0x1c;
sfrb EIMSK=0x1d;
sfrb GPIOR0=0x1e;
sfrb EECR=0x1f;
sfrb EEDR=0x20;
sfrb EEARL=0x21;
sfrb EEARH=0x22;
sfrw EEAR=0x21;   
sfrb GTCCR=0x23;
sfrb TCCR0A=0x24;
sfrb TCCR0B=0x25;
sfrb TCNT0=0x26;
sfrb OCR0A=0x27;
sfrb OCR0B=0x28;
sfrb GPIOR1=0x2a;
sfrb GPIOR2=0x2b;
sfrb SPCR=0x2c;
sfrb SPSR=0x2d;
sfrb SPDR=0x2e;
sfrb ACSR=0x30;
sfrb SMCR=0x33;
sfrb MCUSR=0x34;
sfrb MCUCR=0x35;
sfrb SPMCSR=0x37;
sfrb SPL=0x3d;
sfrb SPH=0x3e;
sfrb SREG=0x3f;
#pragma used-

#asm
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_powersave=0x06
	.EQU __sm_standby=0x0C
	.EQU __sm_ext_standby=0x0E
	.SET power_ctrl_reg=smcr
	#endif
#endasm

#pragma used+

void delay_us(unsigned int n);
void delay_ms(unsigned int n);

#pragma used-

void initPort();        
void reset();

void initUsart();

void putchar0(char c);
void putstr0(char *c, int len);
char getchar0();           

extern unsigned int g_adc_data_history[40      ]; 
void initADC();

void initTimer0();   

void initTimer1();

extern void timer0_Fired();   
extern void timer1_Fired();

extern eeprom unsigned char ieeeAddr[8];     
extern unsigned char netAddr[2]; 
extern eeprom unsigned char panId[2];
extern eeprom unsigned char channel;

void sendReqZigbeeInfo();          
void sendSetZigbeePanId(unsigned char id); 
void sendSetZigbeeChannel(unsigned char channel);                       
void sendSetZigbeePreconfig(unsigned char preconfig);
void handleResZigbeeInfo(char *data);
void sendControlDataToUsart0(char *buf, unsigned char len); 
void sendReqZigbeeReset();
int zigbee_getState();

unsigned char getTranslatedCode(char *resultBuf, char* buf, unsigned char len);
unsigned char getOriginalCode(char *resultBuf, char* buf, unsigned char len);

void initRotary();
unsigned char getRotaryValue();

typedef char *va_list;

#pragma used+

char getchar(void);
void putchar(char c);
void puts(char *str);
void putsf(char flash *str);
int printf(char flash *fmtstr,...);
int sprintf(char *str, char flash *fmtstr,...);
int vprintf(char flash * fmtstr, va_list argptr);
int vsprintf(char *str, char flash * fmtstr, va_list argptr);

char *gets(char *str,unsigned int len);
int snprintf(char *str, unsigned int size, char flash *fmtstr,...);
int vsnprintf(char *str, unsigned int size, char flash * fmtstr, va_list argptr);

int scanf(char flash *fmtstr,...);
int sscanf(char *str, char flash *fmtstr,...);

#pragma used-

#pragma library stdio.lib

#pragma used+

char *strcat(char *str1,char *str2);
char *strcatf(char *str1,char flash *str2);
char *strchr(char *str,char c);
signed char strcmp(char *str1,char *str2);
signed char strcmpf(char *str1,char flash *str2);
char *strcpy(char *dest,char *src);
char *strcpyf(char *dest,char flash *src);
unsigned char strlcpy(char *dest,char *src,unsigned char n);	
unsigned char strlcpyf(char *dest,char flash *src,unsigned char n); 
unsigned int strlenf(char flash *str);
char *strncat(char *str1,char *str2,unsigned char n);
char *strncatf(char *str1,char flash *str2,unsigned char n);
signed char strncmp(char *str1,char *str2,unsigned char n);
signed char strncmpf(char *str1,char flash *str2,unsigned char n);
char *strncpy(char *dest,char *src,unsigned char n);
char *strncpyf(char *dest,char flash *src,unsigned char n);
char *strpbrk(char *str,char *set);
char *strpbrkf(char *str,char flash *set);
char *strrchr(char *str,char c);
char *strrpbrk(char *str,char *set);
char *strrpbrkf(char *str,char flash *set);
char *strstr(char *str1,char *str2);
char *strstrf(char *str1,char flash *str2);
char *strtok(char *str1,char flash *str2);

unsigned int strlen(char *str);
void *memccpy(void *dest,void *src,char c,unsigned n);
void *memchr(void *buf,unsigned char c,unsigned n);
signed char memcmp(void *buf1,void *buf2,unsigned n);
signed char memcmpf(void *buf1,void flash *buf2,unsigned n);
void *memcpy(void *dest,void *src,unsigned n);
void *memcpyf(void *dest,void flash *src,unsigned n);
void *memmove(void *dest,void *src,unsigned n);
void *memset(void *buf,unsigned char c,unsigned n);
unsigned int strcspn(char *str,char *set);
unsigned int strcspnf(char *str,char flash *set);
int strpos(char *str,char c);
int strrpos(char *str,char c);
unsigned int strspn(char *str,char *set);
unsigned int strspnf(char *str,char flash *set);

#pragma used-
#pragma library string.lib

#pragma used+

signed char cmax(signed char a,signed char b);
int max(int a,int b);
long lmax(long a,long b);
float fmax(float a,float b);
signed char cmin(signed char a,signed char b);
int min(int a,int b);
long lmin(long a,long b);
float fmin(float a,float b);
signed char csign(signed char x);
signed char sign(int x);
signed char lsign(long x);
signed char fsign(float x);
unsigned char isqrt(unsigned int x);
unsigned int lsqrt(unsigned long x);
float sqrt(float x);
float ftrunc(float x);
float floor(float x);
float ceil(float x);
float fmod(float x,float y);
float modf(float x,float *ipart);
float ldexp(float x,int expon);
float frexp(float x,int *expon);
float exp(float x);
float log(float x);
float log10(float x);
float pow(float x,float y);
float sin(float x);
float cos(float x);
float tan(float x);
float sinh(float x);
float cosh(float x);
float tanh(float x);
float asin(float x);
float acos(float x);
float atan(float x);
float atan2(float y,float x);

#pragma used-
#pragma library math.lib

unsigned int Sensing_Standard_Avr=295;

eeprom unsigned char ieeeAddr[8];     
unsigned char netAddr[2]; 
eeprom unsigned char panId[2];   
eeprom unsigned char channel;
eeprom unsigned int Sensing_Threshold_Avr_Diff=0x0019; 
unsigned char NORMAL=1;

void handleMessage();
void processMessage(char * buf, int len);    
void sendNCPPing_Req();
void handleNCPPing_Res(int flag); 
void sendNCPRegister_Req();
void handleNCPRegister_Res(int zigbeeId, int result);

void handleSendEvent();           
void handleSendEvent_Debug();           
void handleStateInfoReq(unsigned int seqNum);
void handleSetSensingLevelReq(unsigned int seqNum, unsigned int avrDiff);
void handleSetModeReq(unsigned int seqNum, unsigned char mode);

void sendNCPPacket(char *buf, unsigned char len);
void sendGCPPacket(unsigned int seqNum, char *buf, unsigned char len);  
void sendSetSensingLevelRes(unsigned int seqNum, unsigned int avrDiff);
void sendSetModeRes(unsigned int seqNum, unsigned char mode);
void sendSetLevelRes(unsigned int seqNum, unsigned char level);

void initWatchDog();
void resetWatchDog();

unsigned char isReoobt=0;
char g_isSensored=0;     
char g_isSensored_Avr=0;         
char g_isSensored_Avr2=0;
unsigned int g_adc_data_history[40      ];   
unsigned long g_timer0_count_NormalMode=0;  

unsigned int g_zigbeeSendTimer=0; 

unsigned int g_debug_data_history[10];
unsigned char g_debug_data_size=0;

unsigned char RegistrationState=0;    
unsigned char PingCount=0;  
unsigned char RegistrationCount=0;    

unsigned int avrHistory[10];   

inline void getAvrWithin10ms() {        
unsigned char i;          
unsigned long avr=0;                
unsigned long dev=0;   
unsigned int temp=0;       
unsigned int adc_data_history[40      ];

for(i=0; i < 40      ; i++)
adc_data_history[i] = g_adc_data_history[i];

for(i=0; i < 40      ; i++) {  
avr+= adc_data_history[i];
}                                                     
avr /= 40      ; 

for(i=0; i < 40      ; i++){
if(adc_data_history[i] > avr)
temp = adc_data_history[i]-avr;
else
temp = avr - adc_data_history[i];
dev += temp;        
}
dev /= 40      ; 

for(i=0; i < 40      ; i++){
if(adc_data_history[i] > avr)
temp = adc_data_history[i]-avr;
else
temp = avr - adc_data_history[i];
if(temp > dev*2)
adc_data_history[i] = avr;
}           
avr=0;   
for(i=0; i < 40      ; i++)
avr+= adc_data_history[i]; 
avr /= 40      ;        
if(g_zigbeeSendTimer==0){    
for(i=0; i < 10-1; i++)
avrHistory[i] = avrHistory[i+1];

avrHistory[10-1] = avr;   

if(g_debug_data_size < (10-1))
g_debug_data_history[g_debug_data_size++] = avr;
}else if(g_zigbeeSendTimer < 2)
g_zigbeeSendTimer++;
else
g_zigbeeSendTimer=0;     
}       

unsigned char pb3OutCnt=100;
char isSensored(){       
unsigned char isSensored=0; 
unsigned char i=0;  
unsigned char startIndex=0;
unsigned char sensorCount=0;  
g_isSensored_Avr=0;

startIndex=10-3;                  

for(i=startIndex; i < 10; i++)
if(avrHistory[i] < Sensing_Standard_Avr) break;
else if(avrHistory[i] - Sensing_Standard_Avr < Sensing_Threshold_Avr_Diff)break;

if(i==10){
g_isSensored_Avr=i+10;  
isSensored=1; 
}

for(i=startIndex; i < 10; i++)
if(avrHistory[i] > Sensing_Standard_Avr) break;
else if(Sensing_Standard_Avr - avrHistory[i] < Sensing_Threshold_Avr_Diff)break;

if(i==10){
g_isSensored_Avr=i+10;  
isSensored=1; 
}             

for(i=0; i < 10; i++){  
if(avrHistory[i] > Sensing_Standard_Avr){
if(avrHistory[i]- Sensing_Standard_Avr > Sensing_Threshold_Avr_Diff) sensorCount++;
}else{                                                                                 
if(Sensing_Standard_Avr-avrHistory[i] > Sensing_Threshold_Avr_Diff) sensorCount++;
}        
}
if(sensorCount > 3*1.5){
g_isSensored_Avr2=sensorCount+10;   
isSensored=1;
}
else {
g_isSensored_Avr2 = sensorCount;
isSensored|=0;
}       

g_isSensored = isSensored;

if(g_isSensored)
pb3OutCnt=0;
if(pb3OutCnt < 100){
pb3OutCnt++;
PORTB |= 0x08;
}else
PORTB &= ~(0x08);

return g_isSensored;
}                 

unsigned int timer0_cnt_Registration=0;
unsigned int timer0_cnt_Ping=0;
unsigned int timer0_cnt_NetAddr=0;
unsigned int timer0_cnt_PanSetting=0; 

unsigned char prevSensorState=0;
unsigned char sensorState=0;   

unsigned int g_NumOfEventSentCount=0;
unsigned int g_EventSendingInterval=10;
unsigned int g_EventSentTimer=0;
unsigned int g_timer1CountForEventSent=0;

void timer1_Fired() {
getAvrWithin10ms();
#asm("WDR"); 

prevSensorState = sensorState;
sensorState = isSensored();

if(sensorState)
PORTB |= 0x02;                          
else
PORTB &= ~(0x02) ;         

if(zigbee_getState()){    
if(prevSensorState==0){        
if(sensorState==1 && (g_EventSentTimer > g_EventSendingInterval)){                

handleSendEvent();  
g_NumOfEventSentCount++;  
g_EventSentTimer=0;   
if(g_EventSendingInterval<=100) 
g_EventSendingInterval+= 10; 
}                        
}else if((g_EventSentTimer > g_EventSendingInterval) && sensorState==1){
handleSendEvent();
g_NumOfEventSentCount++;
g_EventSentTimer=0;          
if(g_EventSendingInterval<=100) 
g_EventSendingInterval+= 10; 
}
}        
if(g_EventSentTimer < 200)
g_EventSentTimer++;     
g_timer1CountForEventSent++;    

if(g_timer1CountForEventSent >= 100) { 
if(g_EventSendingInterval>=10 && g_NumOfEventSentCount==0)                     
g_EventSendingInterval-=10;
g_NumOfEventSentCount=0;
g_timer1CountForEventSent=0;
}
}

void timer0_Fired(){        
timer0_cnt_NetAddr++;
if(timer0_cnt_NetAddr > 20){ 
sendReqZigbeeInfo();   
if(panId[0]!=0x01 || panId[1]!=getRotaryValue() || (channel != (11+(getRotaryValue()%16)))){   
timer0_cnt_PanSetting++;            
}else{
timer0_cnt_PanSetting=0;
}        
if(timer0_cnt_PanSetting >=3) {
if(panId[1]!=getRotaryValue()) sendSetZigbeePanId(getRotaryValue());
if(channel != (11+(getRotaryValue()%16))) sendSetZigbeeChannel(11+(getRotaryValue()%16));
timer0_cnt_PanSetting=0;              
sendReqZigbeeReset();
}                
timer0_cnt_NetAddr=0;
}    

if(!NORMAL) {           
if(zigbee_getState())handleSendEvent_Debug();

g_timer0_count_NormalMode++;
if(g_timer0_count_NormalMode>3000)
NORMAL=1;    
}

if(zigbee_getState()){ 
if(RegistrationState!=1) {
timer0_cnt_Registration++;
if(timer0_cnt_Registration> 100) {
sendNCPRegister_Req();    
RegistrationCount++;
if(RegistrationCount > 12) {
sendReqZigbeeReset(); 
reset();
RegistrationCount=0;           
}
timer0_cnt_Registration=0;
}        
} else{   
timer0_cnt_Ping++;
if(timer0_cnt_Ping > 100) {
PingCount++;
if(PingCount > 5){
RegistrationState = 0;
timer0_cnt_Registration = 0xFF00;      
PingCount=0;
}else                    
sendNCPPing_Req();
timer0_cnt_Ping=0;    
}            
} 
} 
}

char g_MsgBuf[50];
unsigned int g_MsgBuf_Count=0;
unsigned char data=0;

void handleMessage() {  
if(g_MsgBuf_Count >= 50){
g_MsgBuf_Count=0;
}
data = getchar0();

if(data==0xFA){
g_MsgBuf_Count=0;
g_MsgBuf[g_MsgBuf_Count++] = data;
}else if(data == 0xAF){             
g_MsgBuf[g_MsgBuf_Count++] = data;    
processMessage(g_MsgBuf, g_MsgBuf_Count);  
g_MsgBuf_Count=0;        
}else {
g_MsgBuf[g_MsgBuf_Count++] = data;
}
}     

void processMessage(char *buf, int len) {    
int i=0;
int length=0;
char data[50];          
unsigned char checkSum=0;

length+= getOriginalCode(data, buf+1, len-2); 

if(length-2 != data[0]) return;    

for(i=1; i < length-1; i++)
checkSum+=data[i];
if(checkSum!=data[length-1]) return;

if(data[1]==0x00){   
switch(data[2]) {
case 0x11: { 
handleResZigbeeInfo(data);
}break;
}
}else if(data[1]==0x10){ 

if(data[6]==(0x80)	) {
if(data[7]== (0x01)  	){
unsigned int nodeId = (int)data[10] << 8 | data[11];
if(nodeId != ((int)ieeeAddr[6] << 8 | ieeeAddr[7]))
return;
switch(data[14]) {
case (0x12)	:
handleNCPRegister_Res(nodeId, (int)data[15] << 8 | data[16]);        
break;       
case (0x02)	:
handleNCPPing_Res(data[15]);
break;
}
} else if(data[7] == 0x21) {
int seqNum = (int)data[8] <<8 | data[9];   
int nodeId = (int)data[17] << 8 | data[18];
if(nodeId != ((int)ieeeAddr[6] << 8 | ieeeAddr[7]))
return;
switch(data[14]) {                    
case (0x24):
handleStateInfoReq(seqNum);
break;                
case (0x82):             
handleSetModeReq(seqNum, data[19]);
break;
case (0x80): 
handleSetSensingLevelReq(seqNum, data[19]);
break;   
case (0x3A):
isReoobt=1;
break;
}
}
}
}
}   

void handleNCPRegister_Res(int zigbeeId, int result){   
if(result==0){
RegistrationState=1;         
RegistrationCount=0;
PingCount=0;       
sendNCPPing_Req();
}else
sendReqZigbeeReset(); 
}     
unsigned char tempBuf[64];
void sendNCPRegister_Req(){    
unsigned char len=0;

tempBuf[len++] = (0x11)	;    
tempBuf[len++] = 0x2050   >> 8;  
tempBuf[len++] = 0x2050  ;  
tempBuf[len++] = 0x10;
tempBuf[len++] = 0x01      ;         
sendNCPPacket(tempBuf, len);        
}    

void sendNCPPing_Req(){
unsigned char len=0;
tempBuf[len++] = (0x01)	;    
tempBuf[len++] = 0;    
tempBuf[len++] = 0x00;    
sendNCPPacket(tempBuf, len);        
}   

void handleNCPPing_Res(int flag) {  
PingCount=0;               
if(flag==0x01)
sendNCPRegister_Req();
}

void handleStateInfoReq(unsigned int seqNum){
unsigned char len=0;  
tempBuf[len++] = (0x25);
tempBuf[len++] = 0;
tempBuf[len++] = 0;              
tempBuf[len++] = ieeeAddr[6];
tempBuf[len++] = ieeeAddr[7];
tempBuf[len++] = 0x2050   >> 8;  
tempBuf[len++] = 0x2050  ;    
tempBuf[len++] = Sensing_Threshold_Avr_Diff;

sendGCPPacket(seqNum, tempBuf, len);
}

void handleSendEvent(){
unsigned char len=0;    
tempBuf[len++] = (0x70);
tempBuf[len++] = 0; 
tempBuf[len++] = 0; 
tempBuf[len++] = 0x01;      
tempBuf[len++] = ieeeAddr[6];
tempBuf[len++] = ieeeAddr[7];

sendGCPPacket(0, tempBuf, len);
}    

void handleSendEvent_Debug(){
unsigned char len=0;  
unsigned char i=0;  
tempBuf[len++] = (0x7A);
tempBuf[len++] = 0; 
tempBuf[len++] = 0; 
tempBuf[len++] = ieeeAddr[6];
tempBuf[len++] = ieeeAddr[7];
tempBuf[len++] = ((int)Sensing_Standard_Avr + Sensing_Threshold_Avr_Diff)>>8;
tempBuf[len++] = (int)(Sensing_Standard_Avr + Sensing_Threshold_Avr_Diff);
tempBuf[len++] = g_isSensored_Avr*10;    
tempBuf[len++] = g_isSensored_Avr2*10;      
tempBuf[len++] = g_debug_data_size;

for(i=0; i < g_debug_data_size; i++){
tempBuf[len++] =  g_debug_data_history[i]>>8;
tempBuf[len++] =  g_debug_data_history[i];
}
g_debug_data_size=0;

sendGCPPacket(0, tempBuf, len);
}    

void sendSetSensingLevelRes(unsigned int seqNum, unsigned int avrDiff){
unsigned char len=0;   

tempBuf[len++] = (0x81);
tempBuf[len++] = 0;
tempBuf[len++] = 0;      
tempBuf[len++] = ieeeAddr[6];
tempBuf[len++] = ieeeAddr[7];
tempBuf[len++] = 0;

sendGCPPacket(seqNum, tempBuf, len);
}

void sendSetModeRes(unsigned int seqNum, unsigned char mode){
unsigned char len=0;    
tempBuf[len++] = (0x83);
tempBuf[len++] = 0;
tempBuf[len++] = 0;      
tempBuf[len++] = ieeeAddr[6];
tempBuf[len++] = ieeeAddr[7];
tempBuf[len++] = 0;

sendGCPPacket(seqNum, tempBuf, len);       
}       

void handleSetSensingLevelReq(unsigned int seqNum, unsigned int avrDiff){    
Sensing_Threshold_Avr_Diff = avrDiff;
sendSetSensingLevelRes(seqNum, avrDiff);      
}

void handleSetModeReq(unsigned int seqNum, unsigned char mode){ 
if(mode==0x00)
NORMAL=1;
else
NORMAL=0;   

g_timer0_count_NormalMode=0;

sendSetModeRes(seqNum, mode);
}              

unsigned int seqNumGenerator=0;
void sendNCPPacket(char *payload, unsigned char len) {
int i=0;
static char data[50];
unsigned int length=0;
unsigned char checkSum=0;    
unsigned char lenTemp=0;  
unsigned char seqNum[2];
seqNumGenerator=0xdddd;
seqNum[0] = seqNumGenerator>>8;
seqNum[1] = seqNumGenerator;
seqNumGenerator++;

lenTemp = len+13;

data[length++] = 0xFA;
length+= getTranslatedCode(&(data[length]), &lenTemp, 1);
data[length++] = 0x10;
data[length++] = 0x00;
data[length++] = 0x00;                                   
length+= getTranslatedCode(&(data[length]), netAddr, 2);    
data[length++] = (0x80)	;
data[length++] = (0x01)  	;
length+= getTranslatedCode(&(data[length]), seqNum, 2);    
data[length++] = 0;
data[length++] = 0;
data[length++] = ieeeAddr[6];
data[length++] = ieeeAddr[7];
length+= getTranslatedCode(&(data[length]), payload, len);    

for(i=0; i < len; i++){
checkSum+= payload[i];
}   
checkSum+=0x10+netAddr[0]+netAddr[1]+(0x80)	+(0x01)  	+seqNum[0]+seqNum[1]+ieeeAddr[6]+ieeeAddr[7];

length+= getTranslatedCode(&(data[length]), &checkSum, 1);
data[length++] = 0xAF;   
putstr0(data, length);     
}

void sendGCPPacket(unsigned int seqNum, char *payload, unsigned char len) {
int i=0;
char data[50];
unsigned int length=0;
unsigned char checkSum=0;    
unsigned char lenTemp=0;    
unsigned char seqNum_byte[2];

seqNum_byte[0] = seqNum>>8;
seqNum_byte[1] = seqNum;

lenTemp = len+13;

data[length++] = 0xFA;
length+= getTranslatedCode(&(data[length]), &lenTemp, 1);
data[length++] = 0x10;
data[length++] = 0x00;
data[length++] = 0x00;                                   
length+= getTranslatedCode(&(data[length]), netAddr, 2);    
data[length++] = (0x80)	;
data[length++] = 0x21;
length+= getTranslatedCode(&(data[length]), seqNum_byte, 2);
data[length++] = 0;
data[length++] = 0;
data[length++] = ieeeAddr[6];
data[length++] = ieeeAddr[7];
length+= getTranslatedCode(&(data[length]), payload, len);    

for(i=0; i < len; i++){
checkSum+= payload[i];
}   
checkSum+=0x10+netAddr[0]+netAddr[1]+(0x80)	+0x21+seqNum_byte[0]+seqNum_byte[1]+ieeeAddr[6]+ieeeAddr[7];

length+= getTranslatedCode(&(data[length]), &checkSum, 1);
data[length++] = 0xAF;   
g_zigbeeSendTimer=1;
putstr0(data, length);     
}

void initWatchDog(){

#pragma optsize-
(*(unsigned char *) 0x60)=0x18;

(*(unsigned char *) 0x60)=0x1E;        

#pragma optsize+

}   

void disableWatchDog() {

#pragma optsize-
(*(unsigned char *) 0x60)=0x17;
delay_ms(1);    
(*(unsigned char *) 0x60)=0x07;
#pragma optsize+
}          

void resetWatchDog() {
disableWatchDog();
initWatchDog();
}

void main(void)
{           
unsigned char i=0;     

initPort();   
initRotary();     
initUsart();   
initADC();        
PORTB |= 0x02;

for(i=0; i < 5; i++) {    
PORTB |= 0x02;
delay_ms(200);
PORTB &= ~(0x02) ;
delay_ms(200); 
}
PORTB |= 0x02;

sendReqZigbeeInfo();
delay_ms(500);
PORTB &= ~(0x02) ;

initTimer0();   
initTimer1();  
initWatchDog();     

sendSetZigbeePreconfig(0x15);
delay_ms(100);
sendReqZigbeeReset();
delay_ms(1000);

#asm("sei")   
while (1)
{           
handleMessage();
};
}
