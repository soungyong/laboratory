#ifndef _TIMER__H
#define _TIMER__H
#include "common.h"

//100ms
void initTimer0();   
//10ms
void initTimer1();


extern void timer0_Fired();   
extern void timer1_Fired();
#endif