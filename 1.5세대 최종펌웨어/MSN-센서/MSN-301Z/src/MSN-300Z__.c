/*****************************************************
This program was produced by the
CodeWizardAVR V1.25.3 Standard
Automatic Program Generator
?Copyright 1998-2007 Pavel Haiduc, HP InfoTech s.r.l.
http://www.hpinfotech.com

Project :
Version :
Date    : 8/10/2011
Author  : funface2
Company : m2mkorea
Comments:


Chip type           : ATmega8
Program type        : Application
Clock frequency     : 16.000000 MHz
Memory model        : Small
External SRAM size  : 0
Data Stack size     : 1024
*****************************************************/

#include "common.h"
#include "util.h"
#include "gpio.h"
#include "usart.h"
#include "adc.h"
#include "timer.h"
#include "zigbee.h"
#include "rotary.h"
#include "NCProtocol.h"

#include <stdio.h>
#include <string.h>
#include <math.h>

eeprom unsigned char ieeeAddr[8];
unsigned char netAddr[2];
eeprom unsigned char panId[2];
eeprom unsigned char channel;
eeprom unsigned int Sensing_Threshold_Avr_Diff=0x0014;
eeprom unsigned int Sensing_Standard_Avr=290;
eeprom unsigned char NORMAL=1;

void handleMessage();
void processMessage(char * buf, int len);
void sendNCPPing_Req();
void handleNCPPing_Res(int flag);
void sendNCPRegister_Req();
void handleNCPRegister_Res(int zigbeeId, int result);
void handleGetDeviceInfo();
void handleGetState();
void handleSetOnTime(char value);
void handleSendEvent();
void handleSendEvent_Debug();
void handleSetSensingLevel(unsigned int avrDiff);
void handleSetMode(unsigned char mode);
void handleSetLevel(unsigned char level);

void sendNCPPacket(char *buf, unsigned char len);
void sendDataToUsart0(char *buf, unsigned char len);
void sendSetSensingLevelRes(unsigned int avrDiff);
void sendSetModeRes(unsigned char mode);
void sendSetLevelRes(unsigned char level);

void initWatchDog();
void resetWatchDog();

eeprom unsigned char g_sensorOnTime;
unsigned int g_adc_data;
char g_isSensored=false;
char g_isSensored_Avr=false;
char g_isSensored_Avr2=false;
unsigned int g_adc_data_history[NumOFHistory];
unsigned long g_timer0_count=0;

#define IDLE 0
#define REGISTERED 1
unsigned char RegistrationState=IDLE;
unsigned char PingCount=0;
unsigned char RegistrationCount=0;

int g_min=10000;
int g_max =0;
int g_avr=0;
int g_dev=0;

void initMinMax() {
    g_min = 10000;
    g_max = 0;
}

#define NumOfRecentHistory 3
#define NumOfAvrHistory 10
unsigned int avrHistory[NumOfAvrHistory];

inline void getAvrWithin20ms() {
    unsigned char i;
    unsigned int min=10000;
    unsigned int max=0;
    unsigned long avr=0;
    unsigned long dev=0;
    unsigned int temp=0;
    unsigned int adc_data_history[NumOFHistory];

    for(i=0; i < NumOFHistory; i++)
        adc_data_history[i] = g_adc_data_history[i];

    for(i=0; i < NumOFHistory; i++) {
        avr+= adc_data_history[i];
    }
    avr /= NumOFHistory;
    for(i=0; i < NumOFHistory; i++){
        if(adc_data_history[i] > avr)
            temp = adc_data_history[i]-avr;
        else
            temp = avr - adc_data_history[i];
        dev += temp;
    }
    dev /= NumOFHistory;

    for(i=0; i < NumOFHistory; i++){
         if(adc_data_history[i] > avr)
            temp = adc_data_history[i]-avr;
        else
            temp = avr - adc_data_history[i];
        if(temp > dev)
            adc_data_history[i] = avr;
    }
    avr=0;
    for(i=0; i < NumOFHistory; i++)
        avr+= adc_data_history[i];
    avr /= NumOFHistory;

    for(i=0; i < NumOfAvrHistory-1; i++)
        avrHistory[i] = avrHistory[i+1];

    avrHistory[NumOfAvrHistory-1] = avr;
}

//동작 상태 확인 timer
//100ms 마다 호출.

char isSensored(){
    unsigned char isSensored=0;
    unsigned char i=0;
    unsigned char startIndex=0;
    unsigned char sensorCount=0;
    unsigned long avr=0;
    unsigned long dev=0;
    unsigned int max=0;
    unsigned int min=1000;
    unsigned int temp=0;

    startIndex=NumOfAvrHistory-NumOfRecentHistory;

    for(i=startIndex; i < NumOfAvrHistory; i++)
        if(avrHistory[i] < Sensing_Standard_Avr) break;
        else if(avrHistory[i] - Sensing_Standard_Avr < Sensing_Threshold_Avr_Diff)break;

    if(i==NumOfAvrHistory){
        g_isSensored_Avr=i+10;
        isSensored=true;
    }
    else{
        g_isSensored_Avr=i;
        isSensored|=false;
    }

    for(i=0; i < NumOfAvrHistory; i++){
        if(avrHistory[i] > Sensing_Standard_Avr)
            if(avrHistory[i]- Sensing_Standard_Avr > Sensing_Threshold_Avr_Diff) sensorCount++;
    }
    if(sensorCount > NumOfRecentHistory*1.5){
        g_isSensored_Avr2=sensorCount+10;
        isSensored=true;
    }
    else {
        g_isSensored_Avr2 = sensorCount;
        isSensored|=false;
    }

    for(i=0; i < NumOfAvrHistory; i++){
        if(avrHistory[i] > max) max = avrHistory[i];
        if(avrHistory[i] < min) min = avrHistory[i];
        avr += avrHistory[i];
    }
    avr /= NumOfAvrHistory;

    for(i=0; i < NumOfAvrHistory; i++){
        if(avrHistory[i] > avr) temp = avrHistory[i]-avr;
        else temp = avr - avrHistory[i];
        dev += temp;
    }
    dev /= NumOfAvrHistory;

    g_avr = avr;
    g_dev = dev;
    g_max = max;
    g_min = min;

    g_isSensored = isSensored;
    return g_isSensored;
}

unsigned int timer0_cnt_Registration=0;
unsigned int timer0_cnt_Ping=0;
unsigned int timer0_cnt_NetAddr=0;
unsigned int timer0_cnt_PanSetting=0;
//unsigned int timer0_cnt_LED=0;
unsigned char prevSensorState=false;
unsigned char sensorState=false;

//20ms
void timer1_Fired() {
    getAvrWithin20ms();
    resetWatchDog();
}

//100ms
void timer0_Fired(){
    //update g_isSensored 값.
    prevSensorState = sensorState;
    sensorState = isSensored();

    if(sensorState)
        LED_ON;
    else
        LED_OFF;
    timer0_cnt_NetAddr++;
    if(timer0_cnt_NetAddr > 10){ //setting된 zigbee panID확인.
        sendReqZigbeeInfo();
        if(panId[0]!=0x01 || panId[1]!=getRotaryValue() || zigbee_getState()==0 || (channel != (11+(getRotaryValue()%16)))){
            timer0_cnt_PanSetting++;
        }else{
            timer0_cnt_PanSetting=0;
        }
        if(timer0_cnt_PanSetting >=3) {//setting된 zigbee panID와 rotary switch와 다를시 재 설정.
            if(panId[1]!=getRotaryValue()) sendSetZigbeePanId(getRotaryValue());
            if(channel != (11+(getRotaryValue()%16))) sendSetZigbeeChannel(11+(getRotaryValue()%16));
            timer0_cnt_PanSetting=0;
            sendReqZigbeeReset();
        }
        timer0_cnt_NetAddr=0;
    }

    if(!NORMAL) {
        if(g_timer0_count > 0){
            g_timer0_count=0;
            if(zigbee_getState())handleSendEvent_Debug();
        }
    }

    if(zigbee_getState()){ //Zigbee is connecting to coordinator
        if(RegistrationState!=REGISTERED) {
          timer0_cnt_Registration++;
            if(timer0_cnt_Registration> 100) {
                sendNCPRegister_Req();
                RegistrationCount++;
                if(RegistrationCount > 12) {
                    sendReqZigbeeReset();
                    reset();
                    RegistrationCount=0;
                }
                timer0_cnt_Registration=0;
            }
        } else{
            timer0_cnt_Ping++;
            if(timer0_cnt_Ping > 100) {
                PingCount++;
                if(PingCount > 3){
                    RegistrationState = IDLE;
                    timer0_cnt_Registration = 0xFFFF;
                    PingCount=0;
                }else
                    sendNCPPing_Req();
                timer0_cnt_Ping=0;
            }

            if(prevSensorState==false){
                if(sensorState==true && (g_timer0_count > ((long)10*g_sensorOnTime))){
                    //감지 상태 전송.
                    if(zigbee_getState() && RegistrationState==REGISTERED){
                        handleSendEvent();
                        g_timer0_count=0;
                    }
                }
            }else if((g_timer0_count > ((long)10*g_sensorOnTime)) && sensorState==true){
                if(zigbee_getState() && RegistrationState==REGISTERED){
                    handleSendEvent();
                    g_timer0_count=0;
                }
            }
        }
    }
    g_timer0_count++;
}


char g_MsgBuf[DEFAULT_BUF_SIZE];
unsigned int g_MsgBuf_Count=0;
unsigned char data=0;

inline void handleMessage() {
    if(g_MsgBuf_Count >= DEFAULT_BUF_SIZE){
        g_MsgBuf_Count=0;
    }
    data = getchar0();
    //putchar0('b');
    //putchar0(data);
    if(data==0xFA){
        g_MsgBuf_Count=0;
        g_MsgBuf[g_MsgBuf_Count++] = data;
    }else if(data == 0xAF){
        g_MsgBuf[g_MsgBuf_Count++] = data;
        processMessage(g_MsgBuf, g_MsgBuf_Count);
        g_MsgBuf_Count=0;
    }else {
        g_MsgBuf[g_MsgBuf_Count++] = data;
    }
}

void processMessage(char *buf, int len) {
    int i=0;
    int length=0;
    char data[DEFAULT_BUF_SIZE];
    unsigned char checkSum=0;
//    for(i=0; i < len; i++)
//        putchar0(buf[i]);

    //변환코드 원 코드로 재 변환.
    length+= getOriginalCode(data, buf+1, len-2);

    //Length 확인.
    if(length-2 != data[0]) return;
    //checksum 확인.
    for(i=1; i < length-1; i++)
        checkSum+=data[i];
    if(checkSum!=data[length-1]) return;

    for(i=0; i < length; i++)
        putchar0(data[i]);

    if(data[1]==ZIGBEE_CONTROL_PACKET){   //Zigbee Control
        switch(data[2]) {
        case 0x11: { //설정 정보 조회
            handleResZigbeeInfo(data);
        }break;
        }
    }else if(data[1]==ZIGBEE_DATA_PACKET){ //Application PDU
        //data 처리.
        if(data[6]==NCP_PROTOCOL_ID) {
            int zigbeeId = data[9] << 8 | data[10];
            if(zigbeeId != (ieeeAddr[6] << 8 | ieeeAddr[7]))
                return;
            switch(data[8]) {
            case NCP_RES_REGISTER:
                handleNCPRegister_Res(zigbeeId, data[11] << 8 | data[12]);
                break;
            case NCP_RES_PING:
                handleNCPPing_Res(data[12]);
                break;
            }
            if(data[7] == RADAR_SENSOR_PROTOCOL) {
                switch(data[8]) {
                case GET_DEVICE_INFO_REQ:
                    handleGetDeviceInfo();
                    break;
                case GET_STATE_REQ:
                    handleGetState();
                    break;
                case SEND_ON_TIME_REQ:
                    handleSetOnTime(data[11]);
                    break;
                case SET_MODE_REQ:
                    handleSetMode(data[11]);
                    break;
                case SET_SENSING_LEVEL_REQ:
                    handleSetSensingLevel(((unsigned int)data[11])<<8 | data[12]);
                    break;
                case SET_LEVEL:
                    handleSetLevel(data[11]);
                    break;
                }
            }
        }
    }
}

void handleNCPRegister_Res(int zigbeeId, int result){
    if(result==0){
        RegistrationState=REGISTERED;
        RegistrationCount=0;
        PingCount=0;
    }else
        sendReqZigbeeReset();
}

void handleSetLevel(unsigned char level){
    Sensing_Threshold_Avr_Diff = level;
    sendSetLevelRes(level);
}

void sendNCPRegister_Req(){
    char buf[20];
    unsigned char len=0;
    unsigned char i=0;

    buf[len++] = RADAR_SENSOR_PROTOCOL;
    buf[len++] = NCP_REQ_REGISTER;
    buf[len++] = ieeeAddr[6];
    buf[len++] = ieeeAddr[7];
    buf[len++] = DEVICE_TYPE >> 8;
    buf[len++] = DEVICE_TYPE;
    buf[len++] = DEVICE_VERSION;
    buf[len++] = FW_VERSION;
    sendNCPPacket(buf, len);
}

void sendNCPPing_Req(){
    char buf[20];
    unsigned char len=0;
    unsigned char i=0;
    buf[len++] = RADAR_SENSOR_PROTOCOL;
    buf[len++] = NCP_REQ_PING;
    buf[len++] = ieeeAddr[6];
    buf[len++] = ieeeAddr[7];
    buf[len++] = NCP_VERSION;
    buf[len++] = 0x00;
    sendNCPPacket(buf, len);
}

void handleNCPPing_Res(int flag) {
    PingCount=0;
    if(flag==0x01)
        sendNCPRegister_Req();

}

void handleGetDeviceInfo(){
    char buf[20];
    unsigned char len=0;
    unsigned char i=0;
    buf[len++] = RADAR_SENSOR_PROTOCOL;
    buf[len++] = GET_DEVICE_INFO_RES;
    buf[len++] = ieeeAddr[6];
    buf[len++] = ieeeAddr[7];
    buf[len++] = DEVICE_TYPE >> 8;
    buf[len++] = DEVICE_TYPE;
    buf[len++] = DEVICE_VERSION;
    buf[len++] = FW_VERSION;
    for(i=0; i<8; i++)
        buf[len++] = ieeeAddr[i];

    sendNCPPacket(buf, len);
}
void handleGetState(){
    char buf[10];
    unsigned char len=0;
    buf[len++] = RADAR_SENSOR_PROTOCOL;
    buf[len++] = GET_STATE_RES;
    buf[len++] = ieeeAddr[6];
    buf[len++] = ieeeAddr[7];
    buf[len++] = g_isSensored;
    sendNCPPacket(buf, len);
}
void handleSetOnTime(char value){
    char buf[10];
    unsigned char len=0;

    g_sensorOnTime = value;
    buf[len++] = RADAR_SENSOR_PROTOCOL;
    buf[len++] = SEND_ON_TIME_RES;
    buf[len++] = ieeeAddr[6];
    buf[len++] = ieeeAddr[7];
    buf[len++] = g_sensorOnTime;
    sendNCPPacket(buf, len);
}

void handleSendEvent(){
    char buf[20];
    unsigned char len=0;
    buf[len++] = RADAR_SENSOR_PROTOCOL;
    buf[len++] = SEND_EVENT;
    buf[len++] = ieeeAddr[6];
    buf[len++] = ieeeAddr[7];
    buf[len++] = g_isSensored;

    sendNCPPacket(buf, len);
    initMinMax();
}

void handleSendEvent_Debug(){
    char buf[20];
    unsigned char len=0;
    buf[len++] = RADAR_SENSOR_PROTOCOL;
    buf[len++] = SEND_EVENT_DEBUG;
    buf[len++] = ieeeAddr[6];
    buf[len++] = ieeeAddr[7];
    buf[len++] = ((int)g_max) >>8;
    buf[len++] = (int)g_max;
    buf[len++] = ((int)g_min)>>8;
    buf[len++] = (int)g_min;
    buf[len++] = ((int)g_avr)>>8;
    buf[len++] = (int)g_avr;
    buf[len++] = ((int)g_dev);
    buf[len++] = ((int)Sensing_Standard_Avr + Sensing_Threshold_Avr_Diff)>>8;
    buf[len++] = (int)(Sensing_Standard_Avr + Sensing_Threshold_Avr_Diff);
    buf[len++] = g_isSensored_Avr*10;
    buf[len++] = g_isSensored_Avr2*10;

    sendNCPPacket(buf, len);
    initMinMax();
}

void sendSetSensingLevelRes(unsigned int avrDiff){
    char buf[20];
    unsigned char len=0;
    buf[len++] = RADAR_SENSOR_PROTOCOL;
    buf[len++] = SET_SENSING_LEVEL_RES;
    buf[len++] = ieeeAddr[6];
    buf[len++] = ieeeAddr[7];
    buf[len++] = avrDiff>>8;
    buf[len++] = avrDiff;

    sendNCPPacket(buf, len);
}
void sendSetModeRes(unsigned char mode){
    char buf[20];
    unsigned char len=0;
    buf[len++] = RADAR_SENSOR_PROTOCOL;
    buf[len++] = SET_MODE_RES;
    buf[len++] = ieeeAddr[6];
    buf[len++] = ieeeAddr[7];
    buf[len++] = mode;

    sendNCPPacket(buf, len);
}

void handleSetSensingLevel(unsigned int avrDiff){
    Sensing_Threshold_Avr_Diff = avrDiff;
    sendSetSensingLevelRes(avrDiff);
}

void handleSetMode(unsigned char mode){
    if(mode==0x00)
        NORMAL=1;
    else
        NORMAL=0;

    sendSetModeRes(mode);
}

void sendSetLevelRes(unsigned char level){
    char buf[20];
    unsigned char len=0;
    buf[len++] = RADAR_SENSOR_PROTOCOL;
    buf[len++] = SET_LEVEL_RES;
    buf[len++] = ieeeAddr[6];
    buf[len++] = ieeeAddr[7];
    buf[len++] = level;

    sendNCPPacket(buf, len);
}

void sendNCPPacket(char *buf, unsigned char len) {
    int i=0;
    char data[DEFAULT_BUF_SIZE];
    unsigned int length=0;
    unsigned char checkSum=0;

    data[length++] = 0xFA;
    len+=6;
    length+= getTranslatedCode(&(data[length]), &len, 1);
    data[length++] = 0x10;
    data[length++] = 0x00;
    data[length++] = 0x00;
    length+= getTranslatedCode(&(data[length]), netAddr, 2);
    data[length++] = NCP_PROTOCOL_ID;
    length+= getTranslatedCode(&(data[length]), buf, len-6);

    for(i=0; i < len-6; i++){
        checkSum+= buf[i];
    }
    checkSum+=0x10+netAddr[0]+netAddr[1]+NCP_PROTOCOL_ID;

    length+= getTranslatedCode(&(data[length]), &checkSum, 1);
    data[length++] = 0xAF;
    putstr0(data, length);
}

void sendDataToUsart0(char *buf, unsigned char len){
    int i=0;
    char data[DEFAULT_BUF_SIZE];
    unsigned int length=0;
    unsigned char checkSum=0;

    data[length++] = 0xFA;
    len+=5;
    length+= getTranslatedCode(&(data[length]), &len, 1);
    data[length++] = 0x10;
    data[length++] = 0x00;
    data[length++] = 0x00;
    length+= getTranslatedCode(&(data[length]), netAddr, 2);
    length+= getTranslatedCode(&(data[length]), buf, len-5);

    for(i=0; i < len-5; i++){
        checkSum+= buf[i];
    }
    checkSum+=0x10+netAddr[0]+netAddr[1];

    length+= getTranslatedCode(&(data[length]), &checkSum, 1);
    data[length++] = 0xAF;
    putstr0(data, length);
}

void initWatchDog(){
    // Watchdog Timer initialization
    // Watchdog Timer Prescaler: OSC/2048k
/*    #pragma optsize-
    WDTCR=0x1E;
    WDTCR=0x0E;
    #ifdef _OPTIMIZE_SIZE_
    #pragma optsize+
    #endif
    */
}

void resetWatchDog() {
    #asm("WDR");
}

void main(void)
{
    unsigned char i=0;

    if(g_sensorOnTime <=0)
        g_sensorOnTime = 1;
    initPort();
    initRotary();
    initUsart();
    initADC();
    LED_ON;

    for(i=0; i < 5; i++) {
        LED_ON;
        delay_ms(200);
        LED_OFF;
        delay_ms(200);
    }
    LED_ON;

    sendReqZigbeeInfo();
    delay_ms(500);
    LED_OFF;
    //handleGetDeviceInfo();//부하제어기로 등록.
    initTimer0();
    initTimer1();
    initWatchDog();

/*    while(1) {
        delay_ms(500);
        reset();
    }*/


    // Global enable interrupts
    #asm("sei")
    while (1)
    {
        handleMessage();
//    putchar0('a');
//        putchar0(0xFA);
//        putchar0(g_adc_data >> 8);
//        putchar0(g_adc_data & 0xFF);
//        delay_ms(500);
//        putchar0(0xfa);
    };
}
#include "adc.h"
#include "gpio.h"

void initADC(){

    // ADC initialization
    // ADC Clock frequency: 250.000 kHz
    // ADC Voltage Reference: AREF pin

    ADMUX= ADC_VREF_TYPE & 0xff;
    ADCSRA=0xCF;
//    ADCSRA=0x87;
}

unsigned int adc_cnt=0;
unsigned int adc_index=0;
unsigned int adc_pre_data[10];
// ADC interrupt service routine
interrupt [ADC_INT] void adc_isr(void)
{
    unsigned long avr=0;
    unsigned int i=0;

    for(i=0; i < 10; i++){
        avr+=adc_pre_data[i];
        if(i!=9)adc_pre_data[i] = adc_pre_data[i+1];
    }
    adc_pre_data[9] = ADCW;

    avr/=10;
    g_adc_data_history[adc_index++] =  avr;

    if(adc_index >= NumOFHistory)
        adc_index=0;


    adc_cnt++;

    // Start the AD conversion
    ADCSRA|=0x40;
}
#include "GPIO.h"

// Declare your global variables here
void initPort() {
    DDRC=0x00;
    DDRB=0x03;
    DDRD=0x00;

    PORTB=0x01;
    PORTC=0xFF;
    PORTD=0x00;
}

void reset(){
    PORTB&= ~(0x01);
    delay_ms(100);
    PORTB|= 0x01;
}
#include "NCprotocol.h"
#include "rotary.h"

void initRotary(){
    DDRD |= (0x3C);
    DDRC |= (0x3C);

    PORTD = 0x3C;
    PORTC = 0x3C;
}

unsigned char getRotaryValue() {
    unsigned char id=0;
    if(PIND.2) id |= 0x01;
    if(PIND.3) id |= 0x02;
    if(PIND.4) id |= 0x04;
    if(PIND.5) id |= 0x08;
    if(PINC.2) id |= 0x10;
    if(PINC.3) id |= 0x20;
    if(PINC.4) id |= 0x40;
    if(PINC.5) id |= 0x80;
    return 0xff-id;
}
#include "timer.h"
int timer0_count=0;
int timer1_count=0;

void initTimer0(){
    // Timer/Counter 0 initialization
    // Clock source: System Clock
    // Clock value: 15.625 kHz
    // Mode: Normal top=FFh
    // OC0 output: Disconnected
    ASSR=0x00;
    TCCR0B=0x05;
    TCNT0=0x00;
    // Timer(s)/Counter(s) Interrupt(s) initialization
    TIMSK0|=0x01;
}

void initTimer1() {
    TCCR1A=0x00;
    TCCR1B=0x03;
    TCNT1H=0xFF;
    TCNT1L=0x00;
    ICR1H=0x00;
    ICR1L=0x00;
    OCR1AH=0x00;
    OCR1AL=0x00;
    OCR1BH=0x00;
    OCR1BL=0x00;
    TIMSK1|=0x04;
}

 // Timer 0 overflow interrupt service routine
interrupt [TIM0_OVF] void timer0_ovf_isr(void)
{
    // Place your code here
    if(timer0_count > 5){
        timer0_Fired();
        timer0_count=0;
    }
    timer0_count++;
}

// Timer 1 overflow interrupt service routine
interrupt [TIM1_OVF] void timer1_ovf_isr(void)
{
    TCNT1H=0xF0;
    TCNT1L=0x00;
    timer1_Fired();
}

#include "usart.h"
char rx_buffer1[RX_BUFFER_SIZE1];

#if RX_BUFFER_SIZE1<256
unsigned char rx_wr_index1,rx_rd_index1,rx_counter1;
#else
unsigned int rx_wr_index1,rx_rd_index1,rx_counter1;
#endif

// This flag is set on USART1 Receiver buffer overflow
bit rx_buffer_overflow1;

void initUsart(){
    // USART1 initialization
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART1 Receiver: On
    // USART1 Transmitter: On
    // USART1 Mode: Asynchronous
    // USART1 Baud rate: 115200
    UCSR0A=0x00;
    UCSR0B=0x98;
    UCSR0C=0x06;
    UBRR0H=0x00;
    UBRR0L=0x08;
    #asm("sei")
}

// USART1 Receiver interrupt service routine
interrupt [USART_RXC] void usart_rx_isr(void)
{
    char status,data;
    status=UCSR0A;
    data=UDR0;
    //UDR = data;
    //putchar0(data);
    if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
    {
        rx_buffer1[rx_wr_index1]=data;
        if (++rx_wr_index1 == RX_BUFFER_SIZE1) rx_wr_index1=0;
        if (++rx_counter1 == RX_BUFFER_SIZE1)
        {
            rx_counter1=0;
            rx_buffer_overflow1=1;
        };
   };
}

// Get a character from the USART Receiver buffer
#pragma used+
char getchar0(void)
{
    char data;
    while (rx_counter1==0);
    data=rx_buffer1[rx_rd_index1];
    if (++rx_rd_index1 == RX_BUFFER_SIZE1) rx_rd_index1=0;
    #asm("cli")
    --rx_counter1;
    #asm("sei")
    return data;
}
#pragma used-
// Write a character to the USART Transmitter
#pragma used+
void putchar0(char c)
{
    while ((UCSR0A & DATA_REGISTER_EMPTY)==0);
    UDR0=c;
}
#pragma used-

void putstr0(char *c, int len){
    int i=0;
    for(i=0; i < len; i++)
        putchar(c[i]);
}
#include "zigbee.h"

#define ZIGBEE_IDLE 0
#define ZIGBEE_CONNECTED 1

int zigbee_state=0;
int zigbee_retry_count=0;

void sendReqZigbeeInfo(){
    char buf[10];
    unsigned char len=0;
    buf[len++] = 0x10;
    buf[len++] = 0x00;
    sendControlDataToUsart0(buf, len);
    zigbee_retry_count++;
    if(zigbee_retry_count > 30) {
        zigbee_state = ZIGBEE_IDLE;
    }
}

void sendSetZigbeePanId(unsigned char id){
    char buf[10];
    unsigned char len=0;
    buf[len++] = 0x30;
    buf[len++] = 0x01;
    buf[len++] = id;
    sendControlDataToUsart0(buf, len);
}

void sendSetZigbeeChannel(unsigned char channel){
    char buf[10];
    char i=0;
    unsigned char len=0;
    buf[len++] = 0x40;
    buf[len++] = 0x02;
    buf[len++] = channel;
    sendControlDataToUsart0(buf, len);
}

void handleResZigbeeInfo(char *data){
    unsigned char i=0;
    for(i=0; i < 8; i++)
        ieeeAddr[i] = data[i+3];

    if(netAddr[0]!=data[14] || netAddr[1]!=data[15])
        zigbee_state = ZIGBEE_IDLE;
    else
        zigbee_state = ZIGBEE_CONNECTED;

    netAddr[0] = data[14];
    netAddr[1] = data[15];
    panId[0] = data[12];
    panId[1] = data[13];
    channel = data[11];

    if(netAddr[0]==0xff && netAddr[1]==0xfc)
        zigbee_state = ZIGBEE_IDLE;
    else
        zigbee_state = ZIGBEE_CONNECTED;

    zigbee_retry_count=0;
}

void sendReqZigbeeReset(){
    char buf[10];
    unsigned char len=0;
    buf[len++] = 0x01;
    buf[len++] = 0x01;
    sendControlDataToUsart0(buf, len);
    zigbee_state = ZIGBEE_IDLE;
}

void sendControlDataToUsart0(char *buf, unsigned char len){
    int i=0;
    char data[DEFAULT_BUF_SIZE];
    unsigned int length=0;
    unsigned char checkSum=0;

    data[length++] = 0xFA;
    len+=1;
    length+= getTranslatedCode(&(data[length]), &len, 1);
    data[length++] = ZIGBEE_CONTROL_PACKET;

    length+= getTranslatedCode(&(data[length]), buf, len-1);

    for(i=0; i < len-1; i++){
        checkSum+= buf[i];
    }

    length+= getTranslatedCode(&(data[length]), &checkSum, 1);
    data[length++] = 0xAF;

    putstr0(data, length);
}
int zigbee_getState(){
    return zigbee_state;
}
