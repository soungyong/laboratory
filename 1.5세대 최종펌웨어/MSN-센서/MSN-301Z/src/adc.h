#ifndef _ADC__H  
#define _ADC__H
#include "common.h"

#define ADC_VREF_TYPE 0x00
#define NumOFHistory 40      

extern unsigned int g_adc_data_history[NumOFHistory]; 
void initADC();
#endif