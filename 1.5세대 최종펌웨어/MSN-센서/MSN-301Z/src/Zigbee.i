
#pragma used+
sfrb PINB=3;
sfrb DDRB=4;
sfrb PORTB=5;
sfrb PINC=6;
sfrb DDRC=7;
sfrb PORTC=8;
sfrb PIND=9;
sfrb DDRD=0xa;
sfrb PORTD=0xb;
sfrb TIFR0=0x15;
sfrb TIFR1=0x16;
sfrb TIFR2=0x17;
sfrb PCIFR=0x1b;
sfrb EIFR=0x1c;
sfrb EIMSK=0x1d;
sfrb GPIOR0=0x1e;
sfrb EECR=0x1f;
sfrb EEDR=0x20;
sfrb EEARL=0x21;
sfrb EEARH=0x22;
sfrw EEAR=0x21;   
sfrb GTCCR=0x23;
sfrb TCCR0A=0x24;
sfrb TCCR0B=0x25;
sfrb TCNT0=0x26;
sfrb OCR0A=0x27;
sfrb OCR0B=0x28;
sfrb GPIOR1=0x2a;
sfrb GPIOR2=0x2b;
sfrb SPCR=0x2c;
sfrb SPSR=0x2d;
sfrb SPDR=0x2e;
sfrb ACSR=0x30;
sfrb SMCR=0x33;
sfrb MCUSR=0x34;
sfrb MCUCR=0x35;
sfrb SPMCSR=0x37;
sfrb SPL=0x3d;
sfrb SPH=0x3e;
sfrb SREG=0x3f;
#pragma used-

#asm
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_powersave=0x06
	.EQU __sm_standby=0x0C
	.EQU __sm_ext_standby=0x0E
	.SET power_ctrl_reg=smcr
	#endif
#endasm

#pragma used+

void delay_us(unsigned int n);
void delay_ms(unsigned int n);

#pragma used-

extern eeprom unsigned char ieeeAddr[8];     
extern unsigned char netAddr[2]; 
extern eeprom unsigned char panId[2];
extern eeprom unsigned char channel;

void sendReqZigbeeInfo();          
void sendSetZigbeePanId(unsigned char id); 
void sendSetZigbeeChannel(unsigned char channel);                       
void sendSetZigbeePreconfig(unsigned char preconfig);
void handleResZigbeeInfo(char *data);
void sendControlDataToUsart0(char *buf, unsigned char len); 
void sendReqZigbeeReset();
int zigbee_getState();

unsigned char getTranslatedCode(char *resultBuf, char* buf, unsigned char len);
unsigned char getOriginalCode(char *resultBuf, char* buf, unsigned char len);

void initUsart();

void putchar0(char c);
void putstr0(char *c, int len);
char getchar0();           

int zigbee_state=0; 
int zigbee_retry_count=0;

void sendReqZigbeeInfo(){
char buf[10];
unsigned char len=0;
buf[len++] = 0x10;  
buf[len++] = 0x00;
sendControlDataToUsart0(buf, len);  
zigbee_retry_count++;            
if(zigbee_retry_count > 30) {
zigbee_state = 0;
}
}     

void sendSetZigbeePanId(unsigned char id){
char buf[10];
unsigned char len=0;
buf[len++] = 0x30;  
buf[len++] = 0x01;
buf[len++] = id;
sendControlDataToUsart0(buf, len);  
}

void sendSetZigbeeChannel(unsigned char channel){
char buf[10];
unsigned char len=0;
buf[len++] = 0x40;  
buf[len++] = 0x02;
buf[len++] = channel;
sendControlDataToUsart0(buf, len);  
}

void sendSetZigbeePreconfig(unsigned char preconfig){
char buf[10];
unsigned char len=0;
buf[len++] = 0x40;  
buf[len++] = 0x05;
buf[len++] = preconfig;
sendControlDataToUsart0(buf, len);
}

void handleResZigbeeInfo(char *data){    
unsigned char i=0;     
for(i=0; i < 8; i++)
ieeeAddr[i] = data[i+3];     

if(netAddr[0]!=data[14] || netAddr[1]!=data[15])
zigbee_state = 0;
else
zigbee_state = 1;        

netAddr[0] = data[14];
netAddr[1] = data[15];
panId[0] = data[12];
panId[1] = data[13];     
channel = data[11];

if(netAddr[0]==0xff && netAddr[1]==0xfc)
zigbee_state = 0;
else
zigbee_state = 1;

zigbee_retry_count=0;
}         

void sendReqZigbeeReset(){
char buf[10];
unsigned char len=0;
buf[len++] = 0x01;  
buf[len++] = 0x01;
sendControlDataToUsart0(buf, len); 
zigbee_state = 0;
} 

void sendControlDataToUsart0(char *buf, unsigned char len){ 
int i=0;
char data[50];
unsigned int length=0;
unsigned char checkSum=0;

data[length++] = 0xFA;
len+=1; 
length+= getTranslatedCode(&(data[length]), &len, 1);
data[length++] = 0x00;

length+= getTranslatedCode(&(data[length]), buf, len-1);    

for(i=0; i < len-1; i++){
checkSum+= buf[i];
}   

length+= getTranslatedCode(&(data[length]), &checkSum, 1);
data[length++] = 0xAF;   

putstr0(data, length);
}           
int zigbee_getState(){   
return zigbee_state;
}

unsigned char getTranslatedCode(char *resultBuf, char* buf, unsigned char len){
unsigned char resultLen=0;
unsigned char i=0;
for(i=0; i < len; i++){        
if(buf[i]==0xFA) { 
resultBuf[resultLen++] = 0xFF;
resultBuf[resultLen++] = 0x01;
} else if(buf[i] == 0xAF){
resultBuf[resultLen++] = 0xFF;
resultBuf[resultLen++] = 0x02;
} else if(buf[i] == 0xFF){
resultBuf[resultLen++] = 0xFF;
resultBuf[resultLen++] = 0xFF;
} else
resultBuf[resultLen++] = buf[i];
}  
return resultLen;
}   

unsigned char getOriginalCode(char *resultBuf, char* buf, unsigned char len) {
unsigned char resultLen=0;
unsigned char i=0;
for(i=0; i < len; i++){        
if(buf[i]==0xFF) {      
if(buf[i+1] == 0xFF) 
resultBuf[resultLen++] = 0xFF;
else if(buf[i+1] == 0x01)
resultBuf[resultLen++] = 0xFA;
else if(buf[i+1] == 0x02)
resultBuf[resultLen++] = 0xAF;  
i++;
}else
resultBuf[resultLen++] = buf[i];
}  
return resultLen;
} 
