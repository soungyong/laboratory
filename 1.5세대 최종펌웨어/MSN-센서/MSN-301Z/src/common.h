#ifndef _COMMON__H   
#define _COMMON__H
#include <mega88a.h>    
#include <delay.h>     

#define DEVICE_VERSION 0x10
#define FW_VERSION 0x01      
#define DEVICE_TYPE 0x2050  

#define DEFAULT_BUF_SIZE 50

#define false 0
#define true 1

#endif