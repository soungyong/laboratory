#include "rotary.h"
#include "common.h"

void initRotary(){
    DDRD &= ~(0x3C);    
    DDRC &= ~(0x3C);    
    
    PORTD |= 0x3C;
    PORTC |= 0x3C;
}

unsigned char getRotaryValue() {
    unsigned char id=0;   
    
   if((PINC & 0x20)==0) id |= (0x01 << 7);
    if((PINC & 0x10)==0) id |= (0x01 << 6);
    if((PINC & 0x08)==0) id |= (0x01 << 5);
    if((PINC & 0x04)==0) id |= (0x01 << 4);

    if((PIND & 0x20)==0) id |= (0x01 << 3);
    if((PIND & 0x10)==0) id |= (0x01 << 2);
    if((PIND & 0x08)==0) id |= (0x01 << 1);
    if((PIND & 0x04)==0) id |= (0x01 << 0);    
    
    return id;    
}