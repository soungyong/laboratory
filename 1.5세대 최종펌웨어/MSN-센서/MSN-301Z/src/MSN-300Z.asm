
;CodeVisionAVR C Compiler V2.05.0 Professional
;(C) Copyright 1998-2010 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com

;Chip type                : ATmega88PA
;Program type             : Application
;Clock frequency          : 16.000000 MHz
;Memory model             : Small
;Optimize for             : Size
;(s)printf features       : int, width
;(s)scanf features        : int, width
;External RAM size        : 0
;Data Stack size          : 256 byte(s)
;Heap size                : 0 byte(s)
;Promote 'char' to 'int'  : No
;'char' is unsigned       : Yes
;8 bit enums              : Yes
;global 'const' stored in FLASH: No
;Enhanced core instructions    : On
;Smart register allocation     : On
;Automatic register allocation : On

	#pragma AVRPART ADMIN PART_NAME ATmega88PA
	#pragma AVRPART MEMORY PROG_FLASH 8192
	#pragma AVRPART MEMORY EEPROM 512
	#pragma AVRPART MEMORY INT_SRAM SIZE 1279
	#pragma AVRPART MEMORY INT_SRAM START_ADDR 0x100

	.LISTMAC
	.EQU EERE=0x0
	.EQU EEWE=0x1
	.EQU EEMWE=0x2
	.EQU UDRE=0x5
	.EQU RXC=0x7
	.EQU EECR=0x1F
	.EQU EEDR=0x20
	.EQU EEARL=0x21
	.EQU EEARH=0x22
	.EQU SPSR=0x2D
	.EQU SPDR=0x2E
	.EQU SMCR=0x33
	.EQU MCUSR=0x34
	.EQU MCUCR=0x35
	.EQU WDTCSR=0x60
	.EQU UCSR0A=0xC0
	.EQU UDR0=0xC6
	.EQU SPL=0x3D
	.EQU SPH=0x3E
	.EQU SREG=0x3F
	.EQU GPIOR0=0x1E
	.EQU GPIOR1=0x2A
	.EQU GPIOR2=0x2B

	.DEF R0X0=R0
	.DEF R0X1=R1
	.DEF R0X2=R2
	.DEF R0X3=R3
	.DEF R0X4=R4
	.DEF R0X5=R5
	.DEF R0X6=R6
	.DEF R0X7=R7
	.DEF R0X8=R8
	.DEF R0X9=R9
	.DEF R0XA=R10
	.DEF R0XB=R11
	.DEF R0XC=R12
	.DEF R0XD=R13
	.DEF R0XE=R14
	.DEF R0XF=R15
	.DEF R0X10=R16
	.DEF R0X11=R17
	.DEF R0X12=R18
	.DEF R0X13=R19
	.DEF R0X14=R20
	.DEF R0X15=R21
	.DEF R0X16=R22
	.DEF R0X17=R23
	.DEF R0X18=R24
	.DEF R0X19=R25
	.DEF R0X1A=R26
	.DEF R0X1B=R27
	.DEF R0X1C=R28
	.DEF R0X1D=R29
	.DEF R0X1E=R30
	.DEF R0X1F=R31

	.EQU __SRAM_START=0x0100
	.EQU __SRAM_END=0x04FF
	.EQU __DSTACK_SIZE=0x0100
	.EQU __HEAP_SIZE=0x0000
	.EQU __CLEAR_SRAM_SIZE=__SRAM_END-__SRAM_START+1

	.MACRO __CPD1N
	CPI  R30,LOW(@0)
	LDI  R26,HIGH(@0)
	CPC  R31,R26
	LDI  R26,BYTE3(@0)
	CPC  R22,R26
	LDI  R26,BYTE4(@0)
	CPC  R23,R26
	.ENDM

	.MACRO __CPD2N
	CPI  R26,LOW(@0)
	LDI  R30,HIGH(@0)
	CPC  R27,R30
	LDI  R30,BYTE3(@0)
	CPC  R24,R30
	LDI  R30,BYTE4(@0)
	CPC  R25,R30
	.ENDM

	.MACRO __CPWRR
	CP   R@0,R@2
	CPC  R@1,R@3
	.ENDM

	.MACRO __CPWRN
	CPI  R@0,LOW(@2)
	LDI  R30,HIGH(@2)
	CPC  R@1,R30
	.ENDM

	.MACRO __ADDB1MN
	SUBI R30,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDB2MN
	SUBI R26,LOW(-@0-(@1))
	.ENDM

	.MACRO __ADDW1MN
	SUBI R30,LOW(-@0-(@1))
	SBCI R31,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW2MN
	SUBI R26,LOW(-@0-(@1))
	SBCI R27,HIGH(-@0-(@1))
	.ENDM

	.MACRO __ADDW1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1FN
	SUBI R30,LOW(-2*@0-(@1))
	SBCI R31,HIGH(-2*@0-(@1))
	SBCI R22,BYTE3(-2*@0-(@1))
	.ENDM

	.MACRO __ADDD1N
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	SBCI R22,BYTE3(-@0)
	SBCI R23,BYTE4(-@0)
	.ENDM

	.MACRO __ADDD2N
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	SBCI R24,BYTE3(-@0)
	SBCI R25,BYTE4(-@0)
	.ENDM

	.MACRO __SUBD1N
	SUBI R30,LOW(@0)
	SBCI R31,HIGH(@0)
	SBCI R22,BYTE3(@0)
	SBCI R23,BYTE4(@0)
	.ENDM

	.MACRO __SUBD2N
	SUBI R26,LOW(@0)
	SBCI R27,HIGH(@0)
	SBCI R24,BYTE3(@0)
	SBCI R25,BYTE4(@0)
	.ENDM

	.MACRO __ANDBMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ANDWMNN
	LDS  R30,@0+(@1)
	ANDI R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ANDI R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ANDD1N
	ANDI R30,LOW(@0)
	ANDI R31,HIGH(@0)
	ANDI R22,BYTE3(@0)
	ANDI R23,BYTE4(@0)
	.ENDM

	.MACRO __ANDD2N
	ANDI R26,LOW(@0)
	ANDI R27,HIGH(@0)
	ANDI R24,BYTE3(@0)
	ANDI R25,BYTE4(@0)
	.ENDM

	.MACRO __ORBMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	.ENDM

	.MACRO __ORWMNN
	LDS  R30,@0+(@1)
	ORI  R30,LOW(@2)
	STS  @0+(@1),R30
	LDS  R30,@0+(@1)+1
	ORI  R30,HIGH(@2)
	STS  @0+(@1)+1,R30
	.ENDM

	.MACRO __ORD1N
	ORI  R30,LOW(@0)
	ORI  R31,HIGH(@0)
	ORI  R22,BYTE3(@0)
	ORI  R23,BYTE4(@0)
	.ENDM

	.MACRO __ORD2N
	ORI  R26,LOW(@0)
	ORI  R27,HIGH(@0)
	ORI  R24,BYTE3(@0)
	ORI  R25,BYTE4(@0)
	.ENDM

	.MACRO __DELAY_USB
	LDI  R24,LOW(@0)
__DELAY_USB_LOOP:
	DEC  R24
	BRNE __DELAY_USB_LOOP
	.ENDM

	.MACRO __DELAY_USW
	LDI  R24,LOW(@0)
	LDI  R25,HIGH(@0)
__DELAY_USW_LOOP:
	SBIW R24,1
	BRNE __DELAY_USW_LOOP
	.ENDM

	.MACRO __GETD1S
	LDD  R30,Y+@0
	LDD  R31,Y+@0+1
	LDD  R22,Y+@0+2
	LDD  R23,Y+@0+3
	.ENDM

	.MACRO __GETD2S
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	LDD  R24,Y+@0+2
	LDD  R25,Y+@0+3
	.ENDM

	.MACRO __PUTD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R31
	STD  Y+@0+2,R22
	STD  Y+@0+3,R23
	.ENDM

	.MACRO __PUTD2S
	STD  Y+@0,R26
	STD  Y+@0+1,R27
	STD  Y+@0+2,R24
	STD  Y+@0+3,R25
	.ENDM

	.MACRO __PUTDZ2
	STD  Z+@0,R26
	STD  Z+@0+1,R27
	STD  Z+@0+2,R24
	STD  Z+@0+3,R25
	.ENDM

	.MACRO __CLRD1S
	STD  Y+@0,R30
	STD  Y+@0+1,R30
	STD  Y+@0+2,R30
	STD  Y+@0+3,R30
	.ENDM

	.MACRO __POINTB1MN
	LDI  R30,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW1MN
	LDI  R30,LOW(@0+(@1))
	LDI  R31,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTD1M
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __POINTW1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	.ENDM

	.MACRO __POINTD1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	LDI  R22,BYTE3(2*@0+(@1))
	LDI  R23,BYTE4(2*@0+(@1))
	.ENDM

	.MACRO __POINTB2MN
	LDI  R26,LOW(@0+(@1))
	.ENDM

	.MACRO __POINTW2MN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	.ENDM

	.MACRO __POINTBRM
	LDI  R@0,LOW(@1)
	.ENDM

	.MACRO __POINTWRM
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __POINTBRMN
	LDI  R@0,LOW(@1+(@2))
	.ENDM

	.MACRO __POINTWRMN
	LDI  R@0,LOW(@2+(@3))
	LDI  R@1,HIGH(@2+(@3))
	.ENDM

	.MACRO __POINTWRFN
	LDI  R@0,LOW(@2*2+(@3))
	LDI  R@1,HIGH(@2*2+(@3))
	.ENDM

	.MACRO __GETD1N
	LDI  R30,LOW(@0)
	LDI  R31,HIGH(@0)
	LDI  R22,BYTE3(@0)
	LDI  R23,BYTE4(@0)
	.ENDM

	.MACRO __GETD2N
	LDI  R26,LOW(@0)
	LDI  R27,HIGH(@0)
	LDI  R24,BYTE3(@0)
	LDI  R25,BYTE4(@0)
	.ENDM

	.MACRO __GETB1MN
	LDS  R30,@0+(@1)
	.ENDM

	.MACRO __GETB1HMN
	LDS  R31,@0+(@1)
	.ENDM

	.MACRO __GETW1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	.ENDM

	.MACRO __GETD1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	LDS  R22,@0+(@1)+2
	LDS  R23,@0+(@1)+3
	.ENDM

	.MACRO __GETBRMN
	LDS  R@0,@1+(@2)
	.ENDM

	.MACRO __GETWRMN
	LDS  R@0,@2+(@3)
	LDS  R@1,@2+(@3)+1
	.ENDM

	.MACRO __GETWRZ
	LDD  R@0,Z+@2
	LDD  R@1,Z+@2+1
	.ENDM

	.MACRO __GETD2Z
	LDD  R26,Z+@0
	LDD  R27,Z+@0+1
	LDD  R24,Z+@0+2
	LDD  R25,Z+@0+3
	.ENDM

	.MACRO __GETB2MN
	LDS  R26,@0+(@1)
	.ENDM

	.MACRO __GETW2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	.ENDM

	.MACRO __GETD2MN
	LDS  R26,@0+(@1)
	LDS  R27,@0+(@1)+1
	LDS  R24,@0+(@1)+2
	LDS  R25,@0+(@1)+3
	.ENDM

	.MACRO __PUTB1MN
	STS  @0+(@1),R30
	.ENDM

	.MACRO __PUTW1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	.ENDM

	.MACRO __PUTD1MN
	STS  @0+(@1),R30
	STS  @0+(@1)+1,R31
	STS  @0+(@1)+2,R22
	STS  @0+(@1)+3,R23
	.ENDM

	.MACRO __PUTB1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRB
	.ENDM

	.MACRO __PUTW1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRW
	.ENDM

	.MACRO __PUTD1EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMWRD
	.ENDM

	.MACRO __PUTBR0MN
	STS  @0+(@1),R0
	.ENDM

	.MACRO __PUTBMRN
	STS  @0+(@1),R@2
	.ENDM

	.MACRO __PUTWMRN
	STS  @0+(@1),R@2
	STS  @0+(@1)+1,R@3
	.ENDM

	.MACRO __PUTBZR
	STD  Z+@1,R@0
	.ENDM

	.MACRO __PUTWZR
	STD  Z+@2,R@0
	STD  Z+@2+1,R@1
	.ENDM

	.MACRO __GETW1R
	MOV  R30,R@0
	MOV  R31,R@1
	.ENDM

	.MACRO __GETW2R
	MOV  R26,R@0
	MOV  R27,R@1
	.ENDM

	.MACRO __GETWRN
	LDI  R@0,LOW(@2)
	LDI  R@1,HIGH(@2)
	.ENDM

	.MACRO __PUTW1R
	MOV  R@0,R30
	MOV  R@1,R31
	.ENDM

	.MACRO __PUTW2R
	MOV  R@0,R26
	MOV  R@1,R27
	.ENDM

	.MACRO __ADDWRN
	SUBI R@0,LOW(-@2)
	SBCI R@1,HIGH(-@2)
	.ENDM

	.MACRO __ADDWRR
	ADD  R@0,R@2
	ADC  R@1,R@3
	.ENDM

	.MACRO __SUBWRN
	SUBI R@0,LOW(@2)
	SBCI R@1,HIGH(@2)
	.ENDM

	.MACRO __SUBWRR
	SUB  R@0,R@2
	SBC  R@1,R@3
	.ENDM

	.MACRO __ANDWRN
	ANDI R@0,LOW(@2)
	ANDI R@1,HIGH(@2)
	.ENDM

	.MACRO __ANDWRR
	AND  R@0,R@2
	AND  R@1,R@3
	.ENDM

	.MACRO __ORWRN
	ORI  R@0,LOW(@2)
	ORI  R@1,HIGH(@2)
	.ENDM

	.MACRO __ORWRR
	OR   R@0,R@2
	OR   R@1,R@3
	.ENDM

	.MACRO __EORWRR
	EOR  R@0,R@2
	EOR  R@1,R@3
	.ENDM

	.MACRO __GETWRS
	LDD  R@0,Y+@2
	LDD  R@1,Y+@2+1
	.ENDM

	.MACRO __PUTBSR
	STD  Y+@1,R@0
	.ENDM

	.MACRO __PUTWSR
	STD  Y+@2,R@0
	STD  Y+@2+1,R@1
	.ENDM

	.MACRO __MOVEWRR
	MOV  R@0,R@2
	MOV  R@1,R@3
	.ENDM

	.MACRO __INWR
	IN   R@0,@2
	IN   R@1,@2+1
	.ENDM

	.MACRO __OUTWR
	OUT  @2+1,R@1
	OUT  @2,R@0
	.ENDM

	.MACRO __CALL1MN
	LDS  R30,@0+(@1)
	LDS  R31,@0+(@1)+1
	ICALL
	.ENDM

	.MACRO __CALL1FN
	LDI  R30,LOW(2*@0+(@1))
	LDI  R31,HIGH(2*@0+(@1))
	RCALL __GETW1PF
	ICALL
	.ENDM

	.MACRO __CALL2EN
	LDI  R26,LOW(@0+(@1))
	LDI  R27,HIGH(@0+(@1))
	RCALL __EEPROMRDW
	ICALL
	.ENDM

	.MACRO __GETW1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X
	.ENDM

	.MACRO __GETD1STACK
	IN   R26,SPL
	IN   R27,SPH
	ADIW R26,@0+1
	LD   R30,X+
	LD   R31,X+
	LD   R22,X
	.ENDM

	.MACRO __NBST
	BST  R@0,@1
	IN   R30,SREG
	LDI  R31,0x40
	EOR  R30,R31
	OUT  SREG,R30
	.ENDM


	.MACRO __PUTB1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SN
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNS
	LDD  R26,Y+@0
	LDD  R27,Y+@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMN
	LDS  R26,@0
	LDS  R27,@0+1
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1PMNS
	LDS  R26,@0
	LDS  R27,@0+1
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RN
	MOVW R26,R@0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RNS
	MOVW R26,R@0
	ADIW R26,@1
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RNS
	MOVW R26,R@0
	ADIW R26,@1
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RON
	MOV  R26,R@0
	MOV  R27,R@1
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	RCALL __PUTDP1
	.ENDM

	.MACRO __PUTB1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X,R30
	.ENDM

	.MACRO __PUTW1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1RONS
	MOV  R26,R@0
	MOV  R27,R@1
	ADIW R26,@2
	RCALL __PUTDP1
	.ENDM


	.MACRO __GETB1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R30,Z
	.ENDM

	.MACRO __GETB1HSX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	.ENDM

	.MACRO __GETW1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R31,Z
	MOV  R30,R0
	.ENDM

	.MACRO __GETD1SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R0,Z+
	LD   R1,Z+
	LD   R22,Z+
	LD   R23,Z
	MOVW R30,R0
	.ENDM

	.MACRO __GETB2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R26,X
	.ENDM

	.MACRO __GETW2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	.ENDM

	.MACRO __GETD2SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R1,X+
	LD   R24,X+
	LD   R25,X
	MOVW R26,R0
	.ENDM

	.MACRO __GETBRSX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	LD   R@0,Z
	.ENDM

	.MACRO __GETWRSX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	LD   R@0,Z+
	LD   R@1,Z
	.ENDM

	.MACRO __GETBRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	LD   R@0,X
	.ENDM

	.MACRO __GETWRSX2
	MOVW R26,R28
	SUBI R26,LOW(-@2)
	SBCI R27,HIGH(-@2)
	LD   R@0,X+
	LD   R@1,X
	.ENDM

	.MACRO __LSLW8SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	LD   R31,Z
	CLR  R30
	.ENDM

	.MACRO __PUTB1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __CLRW1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __CLRD1SX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	ST   X+,R30
	ST   X+,R30
	ST   X+,R30
	ST   X,R30
	.ENDM

	.MACRO __PUTB2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z,R26
	.ENDM

	.MACRO __PUTW2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z,R27
	.ENDM

	.MACRO __PUTD2SX
	MOVW R30,R28
	SUBI R30,LOW(-@0)
	SBCI R31,HIGH(-@0)
	ST   Z+,R26
	ST   Z+,R27
	ST   Z+,R24
	ST   Z,R25
	.ENDM

	.MACRO __PUTBSRX
	MOVW R30,R28
	SUBI R30,LOW(-@1)
	SBCI R31,HIGH(-@1)
	ST   Z,R@0
	.ENDM

	.MACRO __PUTWSRX
	MOVW R30,R28
	SUBI R30,LOW(-@2)
	SBCI R31,HIGH(-@2)
	ST   Z+,R@0
	ST   Z,R@1
	.ENDM

	.MACRO __PUTB1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X,R30
	.ENDM

	.MACRO __PUTW1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X,R31
	.ENDM

	.MACRO __PUTD1SNX
	MOVW R26,R28
	SUBI R26,LOW(-@0)
	SBCI R27,HIGH(-@0)
	LD   R0,X+
	LD   R27,X
	MOV  R26,R0
	SUBI R26,LOW(-@1)
	SBCI R27,HIGH(-@1)
	ST   X+,R30
	ST   X+,R31
	ST   X+,R22
	ST   X,R23
	.ENDM

	.MACRO __MULBRR
	MULS R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRRU
	MUL  R@0,R@1
	MOVW R30,R0
	.ENDM

	.MACRO __MULBRR0
	MULS R@0,R@1
	.ENDM

	.MACRO __MULBRRU0
	MUL  R@0,R@1
	.ENDM

	.MACRO __MULBNWRU
	LDI  R26,@2
	MUL  R26,R@0
	MOVW R30,R0
	MUL  R26,R@1
	ADD  R31,R0
	.ENDM

;NAME DEFINITIONS FOR GLOBAL VARIABLES ALLOCATED TO REGISTERS
	.DEF _Sensing_Standard_Avr=R2
	.DEF _NORMAL=R5
	.DEF _isReoobt=R4
	.DEF _g_isSensored=R7
	.DEF _g_isSensored_Avr=R6
	.DEF _g_isSensored_Avr2=R9
	.DEF _g_zigbeeSendTimer=R10
	.DEF _g_debug_data_size=R8
	.DEF _RegistrationState=R13
	.DEF _PingCount=R12

;GPIOR0-GPIOR2 INITIALIZATION VALUES
	.EQU __GPIOR0_INIT=0x00
	.EQU __GPIOR1_INIT=0x00
	.EQU __GPIOR2_INIT=0x00

	.CSEG
	.ORG 0x00

;START OF CODE MARKER
__START_OF_CODE:

;INTERRUPT VECTORS
	RJMP __RESET
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP _timer1_ovf_isr
	RJMP 0x00
	RJMP 0x00
	RJMP _timer0_ovf_isr
	RJMP 0x00
	RJMP _usart_rx_isr
	RJMP 0x00
	RJMP 0x00
	RJMP _adc_isr
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00
	RJMP 0x00

_tbl10_G100:
	.DB  0x10,0x27,0xE8,0x3,0x64,0x0,0xA,0x0
	.DB  0x1,0x0
_tbl16_G100:
	.DB  0x0,0x10,0x0,0x1,0x10,0x0,0x1,0x0

_0x3:
	.DB  0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0
_0x20:
	.DB  0x64
_0x3B:
	.DB  0xA
_0x9C:
	.DB  0x27,0x1,0x0,0x1,0x0,0x0,0x0,0x0
	.DB  0x0,0x0,0x0,0x0
_0x2080060:
	.DB  0x1
_0x2080000:
	.DB  0x2D,0x4E,0x41,0x4E,0x0,0x49,0x4E,0x46
	.DB  0x0

__GLOBAL_INI_TBL:
	.DW  0x01
	.DW  _pb3OutCnt
	.DW  _0x20*2

	.DW  0x01
	.DW  _g_EventSendingInterval
	.DW  _0x3B*2

	.DW  0x0C
	.DW  0x02
	.DW  _0x9C*2

	.DW  0x01
	.DW  __seed_G104
	.DW  _0x2080060*2

_0xFFFFFFFF:
	.DW  0

__RESET:
	CLI
	CLR  R30
	OUT  EECR,R30

;INTERRUPT VECTORS ARE PLACED
;AT THE START OF FLASH
	LDI  R31,1
	OUT  MCUCR,R31
	OUT  MCUCR,R30

;DISABLE WATCHDOG
	LDI  R31,0x18
	WDR
	IN   R26,MCUSR
	CBR  R26,8
	OUT  MCUSR,R26
	STS  WDTCSR,R31
	STS  WDTCSR,R30

;CLEAR R2-R14
	LDI  R24,(14-2)+1
	LDI  R26,2
	CLR  R27
__CLEAR_REG:
	ST   X+,R30
	DEC  R24
	BRNE __CLEAR_REG

;CLEAR SRAM
	LDI  R24,LOW(__CLEAR_SRAM_SIZE)
	LDI  R25,HIGH(__CLEAR_SRAM_SIZE)
	LDI  R26,LOW(__SRAM_START)
	LDI  R27,HIGH(__SRAM_START)
__CLEAR_SRAM:
	ST   X+,R30
	SBIW R24,1
	BRNE __CLEAR_SRAM

;GLOBAL VARIABLES INITIALIZATION
	LDI  R30,LOW(__GLOBAL_INI_TBL*2)
	LDI  R31,HIGH(__GLOBAL_INI_TBL*2)
__GLOBAL_INI_NEXT:
	LPM  R24,Z+
	LPM  R25,Z+
	SBIW R24,0
	BREQ __GLOBAL_INI_END
	LPM  R26,Z+
	LPM  R27,Z+
	LPM  R0,Z+
	LPM  R1,Z+
	MOVW R22,R30
	MOVW R30,R0
__GLOBAL_INI_LOOP:
	LPM  R0,Z+
	ST   X+,R0
	SBIW R24,1
	BRNE __GLOBAL_INI_LOOP
	MOVW R30,R22
	RJMP __GLOBAL_INI_NEXT
__GLOBAL_INI_END:

;GPIOR0-GPIOR2 INITIALIZATION
	LDI  R30,__GPIOR0_INIT
	OUT  GPIOR0,R30
	;__GPIOR1_INIT = __GPIOR0_INIT
	OUT  GPIOR1,R30
	;__GPIOR2_INIT = __GPIOR0_INIT
	OUT  GPIOR2,R30

;HARDWARE STACK POINTER INITIALIZATION
	LDI  R30,LOW(__SRAM_END-__HEAP_SIZE)
	OUT  SPL,R30
	LDI  R30,HIGH(__SRAM_END-__HEAP_SIZE)
	OUT  SPH,R30

;DATA STACK POINTER INITIALIZATION
	LDI  R28,LOW(__SRAM_START+__DSTACK_SIZE)
	LDI  R29,HIGH(__SRAM_START+__DSTACK_SIZE)

	RJMP _main

	.ESEG
	.ORG 0

	.DSEG
	.ORG 0x200

	.CSEG
;/*****************************************************
;This program was produced by the
;CodeWizardAVR V1.25.3 Standard
;Automatic Program Generator
;?Copyright 1998-2007 Pavel Haiduc, HP InfoTech s.r.l.
;http://www.hpinfotech.com
;
;Project :
;Version :
;Date    : 8/10/2011
;Author  : funface2
;Company : m2mkorea
;Comments:
;
;
;Chip type           : ATmega8
;Program type        : Application
;Clock frequency     : 16.000000 MHz
;Memory model        : Small
;External SRAM size  : 0
;Data Stack size     : 1024
;*****************************************************/
;
;#include "common.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_powersave=0x06
	.EQU __sm_standby=0x0C
	.EQU __sm_ext_standby=0x0E
	.SET power_ctrl_reg=smcr
	#endif
;#include "util.h"
;#include "gpio.h"
;#include "usart.h"
;#include "adc.h"
;#include "timer.h"
;#include "zigbee.h"
;#include "rotary.h"
;#include "NCProtocol.h"
;
;#include <stdio.h>
;#include <string.h>
;#include <math.h>
;
;unsigned int Sensing_Standard_Avr=295;
;
;eeprom unsigned char ieeeAddr[8];
;unsigned char netAddr[2];
;eeprom unsigned char panId[2];
;eeprom unsigned char channel;
;eeprom unsigned int Sensing_Threshold_Avr_Diff=0x0019;
;unsigned char NORMAL=1;
;
;void handleMessage();
;void processMessage(char * buf, int len);
;void sendNCPPing_Req();
;void handleNCPPing_Res(int flag);
;void sendNCPRegister_Req();
;void handleNCPRegister_Res(int zigbeeId, int result);
;
;void handleSendEvent();
;void handleSendEvent_Debug();
;void handleStateInfoReq(unsigned int seqNum);
;void handleSetSensingLevelReq(unsigned int seqNum, unsigned int avrDiff);
;void handleSetModeReq(unsigned int seqNum, unsigned char mode);
;
;void sendNCPPacket(char *buf, unsigned char len);
;void sendGCPPacket(unsigned int seqNum, char *buf, unsigned char len);
;void sendSetSensingLevelRes(unsigned int seqNum, unsigned int avrDiff);
;void sendSetModeRes(unsigned int seqNum, unsigned char mode);
;void sendSetLevelRes(unsigned int seqNum, unsigned char level);
;
;void initWatchDog();
;void resetWatchDog();
;
;unsigned char isReoobt=0;
;char g_isSensored=false;
;char g_isSensored_Avr=false;
;char g_isSensored_Avr2=false;
;unsigned int g_adc_data_history[NumOFHistory];
;unsigned long g_timer0_count_NormalMode=0;
;
;unsigned int g_zigbeeSendTimer=0;
;
;#define NumOFDebugHistory 10
;unsigned int g_debug_data_history[NumOFDebugHistory];
;unsigned char g_debug_data_size=0;
;
;#define IDLE 0
;#define REGISTERED 1
;unsigned char RegistrationState=IDLE;
;unsigned char PingCount=0;
;unsigned char RegistrationCount=0;
;
;#define NumOfRecentHistory 3
;#define NumOfAvrHistory 10
;unsigned int avrHistory[NumOfAvrHistory];
;
;inline void getAvrWithin10ms() {
; 0000 005C inline void getAvrWithin10ms() {

	.CSEG
_getAvrWithin10ms:
; 0000 005D     unsigned char i;
; 0000 005E     unsigned long avr=0;
; 0000 005F     unsigned long dev=0;
; 0000 0060     unsigned int temp=0;
; 0000 0061     unsigned int adc_data_history[NumOFHistory];
; 0000 0062 
; 0000 0063     for(i=0; i < NumOFHistory; i++)
	SBIW R28,63
	SBIW R28,25
	LDI  R24,8
	LDI  R26,LOW(80)
	LDI  R27,HIGH(80)
	LDI  R30,LOW(_0x3*2)
	LDI  R31,HIGH(_0x3*2)
	RCALL __INITLOCB
	RCALL __SAVELOCR4
;	i -> R17
;	avr -> Y+88
;	dev -> Y+84
;	temp -> R18,R19
;	adc_data_history -> Y+4
	RCALL SUBOPT_0x0
	LDI  R17,LOW(0)
_0x5:
	CPI  R17,40
	BRSH _0x6
; 0000 0064         adc_data_history[i] = g_adc_data_history[i];
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x2
	MOVW R0,R30
	MOV  R30,R17
	LDI  R26,LOW(_g_adc_data_history)
	LDI  R27,HIGH(_g_adc_data_history)
	RCALL SUBOPT_0x3
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x5
	SUBI R17,-1
	RJMP _0x5
_0x6:
; 0000 0066 for(i=0; i < 40      ; i++) {
	LDI  R17,LOW(0)
_0x8:
	CPI  R17,40
	BRSH _0x9
; 0000 0067         avr+= adc_data_history[i];
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x6
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0x8
; 0000 0068     }
	SUBI R17,-1
	RJMP _0x8
_0x9:
; 0000 0069     avr /= NumOFHistory;
	RCALL SUBOPT_0x9
; 0000 006A 
; 0000 006B     for(i=0; i < NumOFHistory; i++){
	LDI  R17,LOW(0)
_0xB:
	CPI  R17,40
	BRSH _0xC
; 0000 006C         if(adc_data_history[i] > avr)
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0xA
	RCALL __CPD12
	BRSH _0xD
; 0000 006D             temp = adc_data_history[i]-avr;
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0xA
	RJMP _0x99
; 0000 006E         else
_0xD:
; 0000 006F             temp = avr - adc_data_history[i];
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x6
	RCALL SUBOPT_0x7
_0x99:
	RCALL __SWAPD12
	RCALL __SUBD12
	MOVW R18,R30
; 0000 0070         dev += temp;
	MOVW R30,R18
	RCALL SUBOPT_0xB
	RCALL SUBOPT_0x7
	RCALL __ADDD12
	RCALL SUBOPT_0xC
; 0000 0071     }
	SUBI R17,-1
	RJMP _0xB
_0xC:
; 0000 0072     dev /= NumOFHistory;
	RCALL SUBOPT_0xB
	__GETD1N 0x28
	RCALL __DIVD21U
	RCALL SUBOPT_0xC
; 0000 0073 
; 0000 0074     for(i=0; i < NumOFHistory; i++){
	LDI  R17,LOW(0)
_0x10:
	CPI  R17,40
	BRSH _0x11
; 0000 0075          if(adc_data_history[i] > avr)
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0xA
	RCALL __CPD12
	BRSH _0x12
; 0000 0076             temp = adc_data_history[i]-avr;
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0xA
	RJMP _0x9A
; 0000 0077         else
_0x12:
; 0000 0078             temp = avr - adc_data_history[i];
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x6
	RCALL SUBOPT_0x7
_0x9A:
	RCALL __SWAPD12
	RCALL __SUBD12
	MOVW R18,R30
; 0000 0079         if(temp > dev*2)
	__GETD1SX 84
	RCALL __LSLD1
	MOVW R26,R18
	CLR  R24
	CLR  R25
	RCALL __CPD12
	BRSH _0x14
; 0000 007A             adc_data_history[i] = avr;
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0xD
; 0000 007B     }
_0x14:
	SUBI R17,-1
	RJMP _0x10
_0x11:
; 0000 007C     avr=0;
	LDI  R30,LOW(0)
	__CLRD1SX 88
; 0000 007D     for(i=0; i < NumOFHistory; i++)
	LDI  R17,LOW(0)
_0x16:
	CPI  R17,40
	BRSH _0x17
; 0000 007E         avr+= adc_data_history[i];
	RCALL SUBOPT_0x1
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x6
	RCALL SUBOPT_0x7
	RCALL SUBOPT_0x8
	SUBI R17,-1
	RJMP _0x16
_0x17:
; 0000 007F avr /= 40      ;
	RCALL SUBOPT_0x9
; 0000 0080     if(g_zigbeeSendTimer==0){
	MOV  R0,R10
	OR   R0,R11
	BRNE _0x18
; 0000 0081         for(i=0; i < NumOfAvrHistory-1; i++)
	LDI  R17,LOW(0)
_0x1A:
	CPI  R17,9
	BRSH _0x1B
; 0000 0082             avrHistory[i] = avrHistory[i+1];
	MOV  R30,R17
	RCALL SUBOPT_0xE
	RCALL SUBOPT_0xF
	MOV  R26,R17
	LDI  R27,0
	LSL  R26
	ROL  R27
	__ADDW2MN _avrHistory,2
	RCALL __GETW1P
	RCALL SUBOPT_0x5
	SUBI R17,-1
	RJMP _0x1A
_0x1B:
; 0000 0084 avrHistory[10-1] = avr;
	__GETW1SX 88
	__PUTW1MN _avrHistory,18
; 0000 0085 
; 0000 0086         if(g_debug_data_size < (NumOFDebugHistory-1))
	LDI  R30,LOW(9)
	CP   R8,R30
	BRSH _0x1C
; 0000 0087             g_debug_data_history[g_debug_data_size++] = avr;
	MOV  R30,R8
	INC  R8
	RCALL SUBOPT_0x10
	RCALL SUBOPT_0xD
; 0000 0088     }else if(g_zigbeeSendTimer < 2)
_0x1C:
	RJMP _0x1D
_0x18:
	LDI  R30,LOW(2)
	LDI  R31,HIGH(2)
	CP   R10,R30
	CPC  R11,R31
	BRSH _0x1E
; 0000 0089         g_zigbeeSendTimer++;
	MOVW R30,R10
	ADIW R30,1
	MOVW R10,R30
; 0000 008A     else
	RJMP _0x1F
_0x1E:
; 0000 008B         g_zigbeeSendTimer=0;
	CLR  R10
	CLR  R11
; 0000 008C }
_0x1F:
_0x1D:
	RCALL __LOADLOCR4
	ADIW R28,63
	ADIW R28,29
	RET
;
;//동작 상태 확인 timer
;//10ms 마다 호출.
;unsigned char pb3OutCnt=100;

	.DSEG
;char isSensored(){
; 0000 0091 char isSensored(){

	.CSEG
_isSensored:
; 0000 0092     unsigned char isSensored=0;
; 0000 0093     unsigned char i=0;
; 0000 0094     unsigned char startIndex=0;
; 0000 0095     unsigned char sensorCount=0;
; 0000 0096     g_isSensored_Avr=0;
	RCALL __SAVELOCR4
;	isSensored -> R17
;	i -> R16
;	startIndex -> R19
;	sensorCount -> R18
	RCALL SUBOPT_0x11
	LDI  R19,0
	LDI  R18,0
	CLR  R6
; 0000 0097 
; 0000 0098     startIndex=NumOfAvrHistory-NumOfRecentHistory;
	LDI  R19,LOW(7)
; 0000 0099 
; 0000 009A     for(i=startIndex; i < NumOfAvrHistory; i++)
	MOV  R16,R19
_0x22:
	CPI  R16,10
	BRSH _0x23
; 0000 009B         if(avrHistory[i] < Sensing_Standard_Avr) break;
	RCALL SUBOPT_0x12
	RCALL SUBOPT_0x4
	CP   R30,R2
	CPC  R31,R3
	BRLO _0x23
; 0000 009C         else if(avrHistory[i] - Sensing_Standard_Avr < Sensing_Threshold_Avr_Diff)break;
	RCALL SUBOPT_0x12
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x13
	CP   R0,R30
	CPC  R1,R31
	BRLO _0x23
; 0000 009D 
; 0000 009E     if(i==NumOfAvrHistory){
	SUBI R16,-1
	RJMP _0x22
_0x23:
	CPI  R16,10
	BRNE _0x27
; 0000 009F         g_isSensored_Avr=i+10;
	RCALL SUBOPT_0x14
; 0000 00A0         isSensored=true;
; 0000 00A1     }
; 0000 00A2 
; 0000 00A3     for(i=startIndex; i < NumOfAvrHistory; i++)
_0x27:
	MOV  R16,R19
_0x29:
	CPI  R16,10
	BRSH _0x2A
; 0000 00A4         if(avrHistory[i] > Sensing_Standard_Avr) break;
	RCALL SUBOPT_0x12
	RCALL SUBOPT_0x4
	CP   R2,R30
	CPC  R3,R31
	BRLO _0x2A
; 0000 00A5         else if(Sensing_Standard_Avr - avrHistory[i] < Sensing_Threshold_Avr_Diff)break;
	RCALL SUBOPT_0x12
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x15
	CP   R0,R30
	CPC  R1,R31
	BRLO _0x2A
; 0000 00A6 
; 0000 00A7     if(i==NumOfAvrHistory){
	SUBI R16,-1
	RJMP _0x29
_0x2A:
	CPI  R16,10
	BRNE _0x2E
; 0000 00A8         g_isSensored_Avr=i+10;
	RCALL SUBOPT_0x14
; 0000 00A9         isSensored=true;
; 0000 00AA     }
; 0000 00AB 
; 0000 00AC     for(i=0; i < NumOfAvrHistory; i++){
_0x2E:
	LDI  R16,LOW(0)
_0x30:
	CPI  R16,10
	BRSH _0x31
; 0000 00AD         if(avrHistory[i] > Sensing_Standard_Avr){
	RCALL SUBOPT_0x12
	RCALL SUBOPT_0x4
	CP   R2,R30
	CPC  R3,R31
	BRSH _0x32
; 0000 00AE             if(avrHistory[i]- Sensing_Standard_Avr > Sensing_Threshold_Avr_Diff) sensorCount++;
	RCALL SUBOPT_0x12
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x13
	CP   R30,R0
	CPC  R31,R1
	BRSH _0x33
	SUBI R18,-1
; 0000 00AF         }else{
_0x33:
	RJMP _0x34
_0x32:
; 0000 00B0             if(Sensing_Standard_Avr-avrHistory[i] > Sensing_Threshold_Avr_Diff) sensorCount++;
	RCALL SUBOPT_0x12
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x15
	CP   R30,R0
	CPC  R31,R1
	BRSH _0x35
	SUBI R18,-1
; 0000 00B1         }
_0x35:
_0x34:
; 0000 00B2     }
	SUBI R16,-1
	RJMP _0x30
_0x31:
; 0000 00B3     if(sensorCount > NumOfRecentHistory*1.5){
	MOV  R30,R18
	CLR  R31
	CLR  R22
	CLR  R23
	RCALL __CDF1
	MOVW R26,R30
	MOVW R24,R22
	__GETD1N 0x40900000
	RCALL __CMPF12
	BREQ PC+2
	BRCC PC+2
	RJMP _0x36
; 0000 00B4         g_isSensored_Avr2=sensorCount+10;
	MOV  R30,R18
	SUBI R30,-LOW(10)
	MOV  R9,R30
; 0000 00B5         isSensored=true;
	LDI  R17,LOW(1)
; 0000 00B6     }
; 0000 00B7     else {
	RJMP _0x37
_0x36:
; 0000 00B8         g_isSensored_Avr2 = sensorCount;
	MOV  R9,R18
; 0000 00B9         isSensored|=false;
	ORI  R17,LOW(0)
; 0000 00BA     }
_0x37:
; 0000 00BB 
; 0000 00BC     g_isSensored = isSensored;
	MOV  R7,R17
; 0000 00BD 
; 0000 00BE     if(g_isSensored)
	TST  R7
	BREQ _0x38
; 0000 00BF         pb3OutCnt=0;
	LDI  R30,LOW(0)
	STS  _pb3OutCnt,R30
; 0000 00C0     if(pb3OutCnt < 100){
_0x38:
	LDS  R26,_pb3OutCnt
	CPI  R26,LOW(0x64)
	BRSH _0x39
; 0000 00C1         pb3OutCnt++;
	LDS  R30,_pb3OutCnt
	SUBI R30,-LOW(1)
	STS  _pb3OutCnt,R30
; 0000 00C2         PORTB |= 0x08;
	SBI  0x5,3
; 0000 00C3     }else
	RJMP _0x3A
_0x39:
; 0000 00C4         PORTB &= ~(0x08);
	CBI  0x5,3
; 0000 00C5 
; 0000 00C6     return g_isSensored;
_0x3A:
	MOV  R30,R7
	RCALL __LOADLOCR4
	RJMP _0x20A0009
; 0000 00C7 }
;
;unsigned int timer0_cnt_Registration=0;
;unsigned int timer0_cnt_Ping=0;
;unsigned int timer0_cnt_NetAddr=0;
;unsigned int timer0_cnt_PanSetting=0;
;//unsigned int timer0_cnt_LED=0;
;unsigned char prevSensorState=false;
;unsigned char sensorState=false;
;
;//10ms
;unsigned int g_NumOfEventSentCount=0;
;unsigned int g_EventSendingInterval=10;

	.DSEG
;unsigned int g_EventSentTimer=0;
;unsigned int g_timer1CountForEventSent=0;
;
;void timer1_Fired() {
; 0000 00D7 void timer1_Fired() {

	.CSEG
_timer1_Fired:
; 0000 00D8     getAvrWithin10ms();
	RCALL _getAvrWithin10ms
; 0000 00D9     #asm("WDR");
	WDR
; 0000 00DA     //update g_isSensored 값.
; 0000 00DB     prevSensorState = sensorState;
	LDS  R30,_sensorState
	STS  _prevSensorState,R30
; 0000 00DC     sensorState = isSensored();
	RCALL _isSensored
	STS  _sensorState,R30
; 0000 00DD 
; 0000 00DE     if(sensorState)
	CPI  R30,0
	BREQ _0x3C
; 0000 00DF         LED_ON;
	SBI  0x5,1
; 0000 00E0     else
	RJMP _0x3D
_0x3C:
; 0000 00E1         LED_OFF;
	CBI  0x5,1
; 0000 00E2 
; 0000 00E3     if(zigbee_getState()){
_0x3D:
	RCALL _zigbee_getState
	SBIW R30,0
	BREQ _0x3E
; 0000 00E4         if(prevSensorState==false){
	LDS  R30,_prevSensorState
	CPI  R30,0
	BRNE _0x3F
; 0000 00E5             if(sensorState==true && (g_EventSentTimer > g_EventSendingInterval)){
	LDS  R26,_sensorState
	CPI  R26,LOW(0x1)
	BRNE _0x41
	RCALL SUBOPT_0x16
	RCALL SUBOPT_0x17
	BRLO _0x42
_0x41:
	RJMP _0x40
_0x42:
; 0000 00E6                 //감지 상태 전송.
; 0000 00E7                 handleSendEvent();
	RCALL SUBOPT_0x18
; 0000 00E8                 g_NumOfEventSentCount++;
; 0000 00E9                 g_EventSentTimer=0;
; 0000 00EA                 if(g_EventSendingInterval<=100) //1초
	BRSH _0x43
; 0000 00EB                     g_EventSendingInterval+= 10; //100ms
	RCALL SUBOPT_0x19
; 0000 00EC             }
_0x43:
; 0000 00ED         }else if((g_EventSentTimer > g_EventSendingInterval) && sensorState==true){
_0x40:
	RJMP _0x44
_0x3F:
	RCALL SUBOPT_0x16
	RCALL SUBOPT_0x17
	BRSH _0x46
	LDS  R26,_sensorState
	CPI  R26,LOW(0x1)
	BREQ _0x47
_0x46:
	RJMP _0x45
_0x47:
; 0000 00EE             handleSendEvent();
	RCALL SUBOPT_0x18
; 0000 00EF             g_NumOfEventSentCount++;
; 0000 00F0             g_EventSentTimer=0;
; 0000 00F1             if(g_EventSendingInterval<=100) //1초
	BRSH _0x48
; 0000 00F2                 g_EventSendingInterval+= 10; //100ms
	RCALL SUBOPT_0x19
; 0000 00F3         }
_0x48:
; 0000 00F4     }
_0x45:
_0x44:
; 0000 00F5     if(g_EventSentTimer < 200)//2초
_0x3E:
	LDS  R26,_g_EventSentTimer
	LDS  R27,_g_EventSentTimer+1
	CPI  R26,LOW(0xC8)
	LDI  R30,HIGH(0xC8)
	CPC  R27,R30
	BRSH _0x49
; 0000 00F6         g_EventSentTimer++;
	LDI  R26,LOW(_g_EventSentTimer)
	LDI  R27,HIGH(_g_EventSentTimer)
	RCALL SUBOPT_0x1A
; 0000 00F7     g_timer1CountForEventSent++;
_0x49:
	LDI  R26,LOW(_g_timer1CountForEventSent)
	LDI  R27,HIGH(_g_timer1CountForEventSent)
	RCALL SUBOPT_0x1A
; 0000 00F8 
; 0000 00F9     if(g_timer1CountForEventSent >= 100) { //1초
	LDS  R26,_g_timer1CountForEventSent
	LDS  R27,_g_timer1CountForEventSent+1
	CPI  R26,LOW(0x64)
	LDI  R30,HIGH(0x64)
	CPC  R27,R30
	BRLO _0x4A
; 0000 00FA         if(g_EventSendingInterval>=10 && g_NumOfEventSentCount==0)
	LDS  R26,_g_EventSendingInterval
	LDS  R27,_g_EventSendingInterval+1
	SBIW R26,10
	BRLO _0x4C
	LDS  R26,_g_NumOfEventSentCount
	LDS  R27,_g_NumOfEventSentCount+1
	SBIW R26,0
	BREQ _0x4D
_0x4C:
	RJMP _0x4B
_0x4D:
; 0000 00FB             g_EventSendingInterval-=10;
	RCALL SUBOPT_0x16
	SBIW R30,10
	STS  _g_EventSendingInterval,R30
	STS  _g_EventSendingInterval+1,R31
; 0000 00FC         g_NumOfEventSentCount=0;
_0x4B:
	LDI  R30,LOW(0)
	STS  _g_NumOfEventSentCount,R30
	STS  _g_NumOfEventSentCount+1,R30
; 0000 00FD         g_timer1CountForEventSent=0;
	STS  _g_timer1CountForEventSent,R30
	STS  _g_timer1CountForEventSent+1,R30
; 0000 00FE     }
; 0000 00FF }
_0x4A:
	RET
;
;//100ms
;void timer0_Fired(){
; 0000 0102 void timer0_Fired(){
_timer0_Fired:
; 0000 0103     timer0_cnt_NetAddr++;
	LDI  R26,LOW(_timer0_cnt_NetAddr)
	LDI  R27,HIGH(_timer0_cnt_NetAddr)
	RCALL SUBOPT_0x1A
; 0000 0104     if(timer0_cnt_NetAddr > 20){ //setting된 zigbee panID확인.
	LDS  R26,_timer0_cnt_NetAddr
	LDS  R27,_timer0_cnt_NetAddr+1
	SBIW R26,21
	BRLO _0x4E
; 0000 0105         sendReqZigbeeInfo();
	RCALL _sendReqZigbeeInfo
; 0000 0106         if(panId[0]!=0x01 || panId[1]!=getRotaryValue() || (channel != (11+(getRotaryValue()%16)))){
	LDI  R26,LOW(_panId)
	LDI  R27,HIGH(_panId)
	RCALL __EEPROMRDB
	CPI  R30,LOW(0x1)
	BRNE _0x50
	__POINTW2MN _panId,1
	RCALL __EEPROMRDB
	PUSH R30
	RCALL _getRotaryValue
	POP  R26
	CP   R30,R26
	BRNE _0x50
	LDI  R26,LOW(_channel)
	LDI  R27,HIGH(_channel)
	RCALL __EEPROMRDB
	PUSH R30
	RCALL SUBOPT_0x1B
	POP  R26
	CP   R30,R26
	BREQ _0x4F
_0x50:
; 0000 0107             timer0_cnt_PanSetting++;
	LDI  R26,LOW(_timer0_cnt_PanSetting)
	LDI  R27,HIGH(_timer0_cnt_PanSetting)
	RCALL SUBOPT_0x1A
; 0000 0108         }else{
	RJMP _0x52
_0x4F:
; 0000 0109             timer0_cnt_PanSetting=0;
	RCALL SUBOPT_0x1C
; 0000 010A         }
_0x52:
; 0000 010B         if(timer0_cnt_PanSetting >=3) {//setting된 zigbee panID와 rotary switch와 다를시 재 설정.
	LDS  R26,_timer0_cnt_PanSetting
	LDS  R27,_timer0_cnt_PanSetting+1
	SBIW R26,3
	BRLO _0x53
; 0000 010C             if(panId[1]!=getRotaryValue()) sendSetZigbeePanId(getRotaryValue());
	__POINTW2MN _panId,1
	RCALL __EEPROMRDB
	PUSH R30
	RCALL _getRotaryValue
	POP  R26
	CP   R30,R26
	BREQ _0x54
	RCALL _getRotaryValue
	ST   -Y,R30
	RCALL _sendSetZigbeePanId
; 0000 010D             if(channel != (11+(getRotaryValue()%16))) sendSetZigbeeChannel(11+(getRotaryValue()%16));
_0x54:
	LDI  R26,LOW(_channel)
	LDI  R27,HIGH(_channel)
	RCALL __EEPROMRDB
	PUSH R30
	RCALL SUBOPT_0x1B
	POP  R26
	CP   R30,R26
	BREQ _0x55
	RCALL SUBOPT_0x1B
	ST   -Y,R30
	RCALL _sendSetZigbeeChannel
; 0000 010E             timer0_cnt_PanSetting=0;
_0x55:
	RCALL SUBOPT_0x1C
; 0000 010F             sendReqZigbeeReset();
	RCALL _sendReqZigbeeReset
; 0000 0110         }
; 0000 0111         timer0_cnt_NetAddr=0;
_0x53:
	LDI  R30,LOW(0)
	STS  _timer0_cnt_NetAddr,R30
	STS  _timer0_cnt_NetAddr+1,R30
; 0000 0112     }
; 0000 0113 
; 0000 0114     if(!NORMAL) {
_0x4E:
	TST  R5
	BRNE _0x56
; 0000 0115         if(zigbee_getState())handleSendEvent_Debug();
	RCALL _zigbee_getState
	SBIW R30,0
	BREQ _0x57
	RCALL _handleSendEvent_Debug
; 0000 0116 
; 0000 0117         g_timer0_count_NormalMode++;
_0x57:
	LDI  R26,LOW(_g_timer0_count_NormalMode)
	LDI  R27,HIGH(_g_timer0_count_NormalMode)
	RCALL __GETD1P_INC
	__SUBD1N -1
	RCALL __PUTDP1_DEC
; 0000 0118         if(g_timer0_count_NormalMode>3000)
	LDS  R26,_g_timer0_count_NormalMode
	LDS  R27,_g_timer0_count_NormalMode+1
	LDS  R24,_g_timer0_count_NormalMode+2
	LDS  R25,_g_timer0_count_NormalMode+3
	__CPD2N 0xBB9
	BRLO _0x58
; 0000 0119             NORMAL=1;
	LDI  R30,LOW(1)
	MOV  R5,R30
; 0000 011A     }
_0x58:
; 0000 011B 
; 0000 011C     if(zigbee_getState()){ //Zigbee is connecting to coordinator
_0x56:
	RCALL _zigbee_getState
	SBIW R30,0
	BRNE PC+2
	RJMP _0x59
; 0000 011D         if(RegistrationState!=REGISTERED) {
	LDI  R30,LOW(1)
	CP   R30,R13
	BREQ _0x5A
; 0000 011E           timer0_cnt_Registration++;
	LDI  R26,LOW(_timer0_cnt_Registration)
	LDI  R27,HIGH(_timer0_cnt_Registration)
	RCALL SUBOPT_0x1A
; 0000 011F             if(timer0_cnt_Registration> 100) {
	LDS  R26,_timer0_cnt_Registration
	LDS  R27,_timer0_cnt_Registration+1
	CPI  R26,LOW(0x65)
	LDI  R30,HIGH(0x65)
	CPC  R27,R30
	BRLO _0x5B
; 0000 0120                 sendNCPRegister_Req();
	RCALL _sendNCPRegister_Req
; 0000 0121                 RegistrationCount++;
	LDS  R30,_RegistrationCount
	SUBI R30,-LOW(1)
	STS  _RegistrationCount,R30
; 0000 0122                 if(RegistrationCount > 12) {
	LDS  R26,_RegistrationCount
	CPI  R26,LOW(0xD)
	BRLO _0x5C
; 0000 0123                     sendReqZigbeeReset();
	RCALL _sendReqZigbeeReset
; 0000 0124                     reset();
	RCALL _reset
; 0000 0125                     RegistrationCount=0;
	LDI  R30,LOW(0)
	STS  _RegistrationCount,R30
; 0000 0126                 }
; 0000 0127                 timer0_cnt_Registration=0;
_0x5C:
	LDI  R30,LOW(0)
	STS  _timer0_cnt_Registration,R30
	STS  _timer0_cnt_Registration+1,R30
; 0000 0128             }
; 0000 0129         } else{
_0x5B:
	RJMP _0x5D
_0x5A:
; 0000 012A             timer0_cnt_Ping++;
	LDI  R26,LOW(_timer0_cnt_Ping)
	LDI  R27,HIGH(_timer0_cnt_Ping)
	RCALL SUBOPT_0x1A
; 0000 012B             if(timer0_cnt_Ping > 100) {
	LDS  R26,_timer0_cnt_Ping
	LDS  R27,_timer0_cnt_Ping+1
	CPI  R26,LOW(0x65)
	LDI  R30,HIGH(0x65)
	CPC  R27,R30
	BRLO _0x5E
; 0000 012C                 PingCount++;
	INC  R12
; 0000 012D                 if(PingCount > 5){
	LDI  R30,LOW(5)
	CP   R30,R12
	BRSH _0x5F
; 0000 012E                     RegistrationState = IDLE;
	CLR  R13
; 0000 012F                     timer0_cnt_Registration = 0xFF00;
	LDI  R30,LOW(65280)
	LDI  R31,HIGH(65280)
	STS  _timer0_cnt_Registration,R30
	STS  _timer0_cnt_Registration+1,R31
; 0000 0130                     PingCount=0;
	CLR  R12
; 0000 0131                 }else
	RJMP _0x60
_0x5F:
; 0000 0132                     sendNCPPing_Req();
	RCALL _sendNCPPing_Req
; 0000 0133                 timer0_cnt_Ping=0;
_0x60:
	LDI  R30,LOW(0)
	STS  _timer0_cnt_Ping,R30
	STS  _timer0_cnt_Ping+1,R30
; 0000 0134             }
; 0000 0135         }
_0x5E:
_0x5D:
; 0000 0136     }
; 0000 0137 }
_0x59:
	RET
;
;
;char g_MsgBuf[DEFAULT_BUF_SIZE];
;unsigned int g_MsgBuf_Count=0;
;unsigned char data=0;
;
;void handleMessage() {
; 0000 013E void handleMessage() {
_handleMessage:
; 0000 013F     if(g_MsgBuf_Count >= DEFAULT_BUF_SIZE){
	LDS  R26,_g_MsgBuf_Count
	LDS  R27,_g_MsgBuf_Count+1
	SBIW R26,50
	BRLO _0x61
; 0000 0140         g_MsgBuf_Count=0;
	RCALL SUBOPT_0x1D
; 0000 0141     }
; 0000 0142     data = getchar0();
_0x61:
	RCALL _getchar0
	STS  _data,R30
; 0000 0143     //putchar0('b');
; 0000 0144     //putchar0(data);
; 0000 0145     if(data==0xFA){
	RCALL SUBOPT_0x1E
	CPI  R26,LOW(0xFA)
	BRNE _0x62
; 0000 0146         g_MsgBuf_Count=0;
	RCALL SUBOPT_0x1D
; 0000 0147         g_MsgBuf[g_MsgBuf_Count++] = data;
	RJMP _0x9B
; 0000 0148     }else if(data == 0xAF){
_0x62:
	RCALL SUBOPT_0x1E
	CPI  R26,LOW(0xAF)
	BRNE _0x64
; 0000 0149         g_MsgBuf[g_MsgBuf_Count++] = data;
	LDI  R26,LOW(_g_MsgBuf_Count)
	LDI  R27,HIGH(_g_MsgBuf_Count)
	RCALL SUBOPT_0x1A
	RCALL SUBOPT_0x1F
; 0000 014A         processMessage(g_MsgBuf, g_MsgBuf_Count);
	LDI  R30,LOW(_g_MsgBuf)
	LDI  R31,HIGH(_g_MsgBuf)
	RCALL SUBOPT_0x20
	LDS  R30,_g_MsgBuf_Count
	LDS  R31,_g_MsgBuf_Count+1
	RCALL SUBOPT_0x20
	RCALL _processMessage
; 0000 014B         g_MsgBuf_Count=0;
	RCALL SUBOPT_0x1D
; 0000 014C     }else {
	RJMP _0x65
_0x64:
; 0000 014D         g_MsgBuf[g_MsgBuf_Count++] = data;
_0x9B:
	LDI  R26,LOW(_g_MsgBuf_Count)
	LDI  R27,HIGH(_g_MsgBuf_Count)
	RCALL SUBOPT_0x1A
	RCALL SUBOPT_0x1F
; 0000 014E     }
_0x65:
; 0000 014F }
	RET
;
;void processMessage(char *buf, int len) {
; 0000 0151 void processMessage(char *buf, int len) {
_processMessage:
; 0000 0152     int i=0;
; 0000 0153     int length=0;
; 0000 0154     char data[DEFAULT_BUF_SIZE];
; 0000 0155     unsigned char checkSum=0;
; 0000 0156 
; 0000 0157     //변환코드 원 코드로 재 변환.
; 0000 0158     length+= getOriginalCode(data, buf+1, len-2);
	RCALL SUBOPT_0x21
;	*buf -> Y+58
;	len -> Y+56
;	i -> R16,R17
;	length -> R18,R19
;	data -> Y+6
;	checkSum -> R21
	LDI  R21,0
	RCALL SUBOPT_0x22
	LDD  R30,Y+60
	LDD  R31,Y+60+1
	ADIW R30,1
	RCALL SUBOPT_0x20
	LDD  R30,Y+60
	LDD  R31,Y+60+1
	SBIW R30,2
	ST   -Y,R30
	RCALL _getOriginalCode
	RCALL SUBOPT_0x23
	RCALL SUBOPT_0x24
; 0000 0159 
; 0000 015A     //Length 확인.
; 0000 015B     if(length-2 != data[0]) return;
	MOVW R26,R18
	SBIW R26,2
	LDD  R30,Y+6
	RCALL SUBOPT_0x23
	CP   R30,R26
	CPC  R31,R27
	BREQ _0x66
	RJMP _0x20A000A
; 0000 015C     //checksum 확인.
; 0000 015D     for(i=1; i < length-1; i++)
_0x66:
	__GETWRN 16,17,1
_0x68:
	MOVW R30,R18
	SBIW R30,1
	CP   R16,R30
	CPC  R17,R31
	BRGE _0x69
; 0000 015E         checkSum+=data[i];
	RCALL SUBOPT_0x25
	ADD  R26,R16
	ADC  R27,R17
	RCALL SUBOPT_0x26
	RCALL SUBOPT_0x27
	RJMP _0x68
_0x69:
; 0000 015F if(checkSum!=data[length-1]) return;
	MOVW R30,R18
	SBIW R30,1
	RCALL SUBOPT_0x28
	LD   R30,X
	CP   R30,R21
	BREQ _0x6A
	RJMP _0x20A000A
; 0000 0160 
; 0000 0161     if(data[1]==ZIGBEE_CONTROL_PACKET){   //Zigbee Control
_0x6A:
	LDD  R30,Y+7
	CPI  R30,0
	BRNE _0x6B
; 0000 0162         switch(data[2]) {
	LDD  R30,Y+8
; 0000 0163         case 0x11: { //설정 정보 조회
	CPI  R30,LOW(0x11)
	BRNE _0x6E
; 0000 0164             handleResZigbeeInfo(data);
	RCALL SUBOPT_0x22
	RCALL _handleResZigbeeInfo
; 0000 0165         }break;
; 0000 0166         }
_0x6E:
; 0000 0167     }else if(data[1]==ZIGBEE_DATA_PACKET){ //Application PDU
	RJMP _0x70
_0x6B:
	LDD  R26,Y+7
	CPI  R26,LOW(0x10)
	BREQ PC+2
	RJMP _0x71
; 0000 0168         //data 처리.
; 0000 0169         if(data[6]==NCP_PROTOCOL_ID) {
	LDD  R26,Y+12
	CPI  R26,LOW(0x80)
	BREQ PC+2
	RJMP _0x72
; 0000 016A             if(data[7]== PLCS_NCP_ID){
	LDD  R26,Y+13
	CPI  R26,LOW(0x1)
	BRNE _0x73
; 0000 016B                 unsigned int nodeId = (int)data[10] << 8 | data[11];
; 0000 016C                 if(nodeId != ((int)ieeeAddr[6] << 8 | ieeeAddr[7]))
	SBIW R28,2
;	*buf -> Y+60
;	len -> Y+58
;	data -> Y+8
;	nodeId -> Y+0
	LDI  R30,0
	LDD  R31,Y+18
	MOVW R26,R30
	RCALL SUBOPT_0x29
	RCALL SUBOPT_0x2A
	BREQ _0x74
; 0000 016D                     return;
	ADIW R28,2
	RJMP _0x20A000A
; 0000 016E                 switch(data[14]) {
_0x74:
	LDD  R30,Y+22
; 0000 016F                 case NCP_RES_REGISTER:
	CPI  R30,LOW(0x12)
	BRNE _0x78
; 0000 0170                     handleNCPRegister_Res(nodeId, (int)data[15] << 8 | data[16]);
	LD   R30,Y
	LDD  R31,Y+1
	RCALL SUBOPT_0x20
	LDI  R30,0
	LDD  R31,Y+25
	MOVW R26,R30
	LDD  R30,Y+26
	RCALL SUBOPT_0x23
	OR   R30,R26
	OR   R31,R27
	RCALL SUBOPT_0x20
	RCALL _handleNCPRegister_Res
; 0000 0171                     break;
	RJMP _0x77
; 0000 0172                 case NCP_RES_PING:
_0x78:
	CPI  R30,LOW(0x2)
	BRNE _0x77
; 0000 0173                     handleNCPPing_Res(data[15]);
	LDD  R30,Y+23
	RCALL SUBOPT_0x23
	RCALL SUBOPT_0x20
	RCALL _handleNCPPing_Res
; 0000 0174                     break;
; 0000 0175                 }
_0x77:
; 0000 0176             } else if(data[7] == PLCS_GCP_ID) {
	ADIW R28,2
	RJMP _0x7A
_0x73:
	LDD  R26,Y+13
	CPI  R26,LOW(0x21)
	BRNE _0x7B
; 0000 0177                 int seqNum = (int)data[8] <<8 | data[9];
; 0000 0178                 int nodeId = (int)data[17] << 8 | data[18];
; 0000 0179                 if(nodeId != ((int)ieeeAddr[6] << 8 | ieeeAddr[7]))
	SBIW R28,4
;	*buf -> Y+62
;	len -> Y+60
;	data -> Y+10
;	seqNum -> Y+2
;	nodeId -> Y+0
	LDI  R30,0
	LDD  R31,Y+18
	MOVW R26,R30
	RCALL SUBOPT_0x29
	STD  Y+2,R30
	STD  Y+2+1,R31
	LDI  R30,0
	LDD  R31,Y+27
	MOVW R26,R30
	LDD  R30,Y+28
	RCALL SUBOPT_0x23
	OR   R30,R26
	OR   R31,R27
	RCALL SUBOPT_0x2A
	BREQ _0x7C
; 0000 017A                     return;
	ADIW R28,4
	RJMP _0x20A000A
; 0000 017B                 switch(data[14]) {
_0x7C:
	LDD  R30,Y+24
; 0000 017C                 case PLCS_GCP_REQ_STATE_INFO:
	CPI  R30,LOW(0x24)
	BRNE _0x80
; 0000 017D                     handleStateInfoReq(seqNum);
	RCALL SUBOPT_0x2B
	RCALL _handleStateInfoReq
; 0000 017E                     break;
	RJMP _0x7F
; 0000 017F                 case PLCS_GCP_REQ_SET_MODE:
_0x80:
	CPI  R30,LOW(0x82)
	BRNE _0x81
; 0000 0180                     handleSetModeReq(seqNum, data[19]);
	RCALL SUBOPT_0x2B
	LDD  R30,Y+31
	ST   -Y,R30
	RCALL _handleSetModeReq
; 0000 0181                     break;
	RJMP _0x7F
; 0000 0182                 case PLCS_GCP_REQ_SET_SENSINGLEVEL:
_0x81:
	CPI  R30,LOW(0x80)
	BRNE _0x82
; 0000 0183                     handleSetSensingLevelReq(seqNum, data[19]);
	RCALL SUBOPT_0x2B
	LDD  R30,Y+31
	RCALL SUBOPT_0x23
	RCALL SUBOPT_0x20
	RCALL _handleSetSensingLevelReq
; 0000 0184                     break;
	RJMP _0x7F
; 0000 0185                 case PLCS_GCP_REQ_REBOOT:
_0x82:
	CPI  R30,LOW(0x3A)
	BRNE _0x7F
; 0000 0186                     isReoobt=1;
	LDI  R30,LOW(1)
	MOV  R4,R30
; 0000 0187                     break;
; 0000 0188                 }
_0x7F:
; 0000 0189             }
	ADIW R28,4
; 0000 018A         }
_0x7B:
_0x7A:
; 0000 018B     }
_0x72:
; 0000 018C }
_0x71:
_0x70:
_0x20A000A:
	RCALL __LOADLOCR6
	ADIW R28,60
	RET
;
;void handleNCPRegister_Res(int zigbeeId, int result){
; 0000 018E void handleNCPRegister_Res(int zigbeeId, int result){
_handleNCPRegister_Res:
; 0000 018F     if(result==0){
;	zigbeeId -> Y+2
;	result -> Y+0
	LD   R30,Y
	LDD  R31,Y+1
	SBIW R30,0
	BRNE _0x84
; 0000 0190         RegistrationState=REGISTERED;
	LDI  R30,LOW(1)
	MOV  R13,R30
; 0000 0191         RegistrationCount=0;
	LDI  R30,LOW(0)
	STS  _RegistrationCount,R30
; 0000 0192         PingCount=0;
	CLR  R12
; 0000 0193         sendNCPPing_Req();
	RCALL _sendNCPPing_Req
; 0000 0194     }else
	RJMP _0x85
_0x84:
; 0000 0195         sendReqZigbeeReset();
	RCALL _sendReqZigbeeReset
; 0000 0196 }
_0x85:
	RJMP _0x20A0009
;unsigned char tempBuf[64];
;void sendNCPRegister_Req(){
; 0000 0198 void sendNCPRegister_Req(){
_sendNCPRegister_Req:
; 0000 0199     unsigned char len=0;
; 0000 019A 
; 0000 019B     tempBuf[len++] = NCP_REQ_REGISTER;
	RCALL SUBOPT_0x2C
;	len -> R17
	LDI  R26,LOW(17)
	RCALL SUBOPT_0x2D
; 0000 019C     tempBuf[len++] = DEVICE_TYPE >> 8;
	LDI  R26,LOW(32)
	RCALL SUBOPT_0x2D
; 0000 019D     tempBuf[len++] = DEVICE_TYPE;
	LDI  R26,LOW(80)
	RCALL SUBOPT_0x2D
; 0000 019E     tempBuf[len++] = DEVICE_VERSION;
	LDI  R26,LOW(16)
	RCALL SUBOPT_0x2D
; 0000 019F     tempBuf[len++] = FW_VERSION;
	LDI  R26,LOW(1)
	RCALL SUBOPT_0x2E
; 0000 01A0     sendNCPPacket(tempBuf, len);
; 0000 01A1 }
	RJMP _0x20A0008
;
;void sendNCPPing_Req(){
; 0000 01A3 void sendNCPPing_Req(){
_sendNCPPing_Req:
; 0000 01A4     unsigned char len=0;
; 0000 01A5     tempBuf[len++] = NCP_REQ_PING;
	RCALL SUBOPT_0x2C
;	len -> R17
	LDI  R26,LOW(1)
	RCALL SUBOPT_0x2D
; 0000 01A6     tempBuf[len++] = 0;
	RCALL SUBOPT_0x2F
; 0000 01A7     tempBuf[len++] = 0x00;
	LDI  R26,LOW(0)
	RCALL SUBOPT_0x2E
; 0000 01A8     sendNCPPacket(tempBuf, len);
; 0000 01A9 }
	RJMP _0x20A0008
;
;void handleNCPPing_Res(int flag) {
; 0000 01AB void handleNCPPing_Res(int flag) {
_handleNCPPing_Res:
; 0000 01AC     PingCount=0;
;	flag -> Y+0
	CLR  R12
; 0000 01AD     if(flag==0x01)
	LD   R26,Y
	LDD  R27,Y+1
	SBIW R26,1
	BRNE _0x86
; 0000 01AE         sendNCPRegister_Req();
	RCALL _sendNCPRegister_Req
; 0000 01AF }
_0x86:
	ADIW R28,2
	RET
;
;void handleStateInfoReq(unsigned int seqNum){
; 0000 01B1 void handleStateInfoReq(unsigned int seqNum){
_handleStateInfoReq:
; 0000 01B2     unsigned char len=0;
; 0000 01B3     tempBuf[len++] = PLCS_GCP_RES_STATE_INFO;
	RCALL SUBOPT_0x2C
;	seqNum -> Y+1
;	len -> R17
	LDI  R26,LOW(37)
	RCALL SUBOPT_0x2D
; 0000 01B4     tempBuf[len++] = 0;
	RCALL SUBOPT_0x2F
; 0000 01B5     tempBuf[len++] = 0;
	RCALL SUBOPT_0x2F
; 0000 01B6     tempBuf[len++] = ieeeAddr[6];
	RCALL SUBOPT_0x30
; 0000 01B7     tempBuf[len++] = ieeeAddr[7];
; 0000 01B8     tempBuf[len++] = DEVICE_TYPE >> 8;
	RCALL SUBOPT_0x31
	LDI  R26,LOW(32)
	RCALL SUBOPT_0x2D
; 0000 01B9     tempBuf[len++] = DEVICE_TYPE;
	LDI  R26,LOW(80)
	RCALL SUBOPT_0x2D
; 0000 01BA     tempBuf[len++] = Sensing_Threshold_Avr_Diff;
	RCALL SUBOPT_0x32
	RCALL SUBOPT_0x33
; 0000 01BB 
; 0000 01BC     sendGCPPacket(seqNum, tempBuf, len);
	LDD  R30,Y+1
	LDD  R31,Y+1+1
	RCALL SUBOPT_0x34
; 0000 01BD }
	RJMP _0x20A0005
;
;void handleSendEvent(){
; 0000 01BF void handleSendEvent(){
_handleSendEvent:
; 0000 01C0     unsigned char len=0;
; 0000 01C1     tempBuf[len++] = PLCS_GCP_NOTICE_EVENT;
	RCALL SUBOPT_0x2C
;	len -> R17
	LDI  R26,LOW(112)
	RCALL SUBOPT_0x2D
; 0000 01C2     tempBuf[len++] = 0; //subNodeid
	RCALL SUBOPT_0x2F
; 0000 01C3     tempBuf[len++] = 0; //subNodeId
	RCALL SUBOPT_0x2F
; 0000 01C4     tempBuf[len++] = 0x01;
	LDI  R26,LOW(1)
	RCALL SUBOPT_0x2D
; 0000 01C5     tempBuf[len++] = ieeeAddr[6];
	RCALL SUBOPT_0x30
; 0000 01C6     tempBuf[len++] = ieeeAddr[7];
; 0000 01C7 
; 0000 01C8     sendGCPPacket(0, tempBuf, len);
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	RCALL SUBOPT_0x34
; 0000 01C9 }
	RJMP _0x20A0008
;
;void handleSendEvent_Debug(){
; 0000 01CB void handleSendEvent_Debug(){
_handleSendEvent_Debug:
; 0000 01CC     unsigned char len=0;
; 0000 01CD     unsigned char i=0;
; 0000 01CE     tempBuf[len++] = PLCS_GCP_NOTICE_EVENT_DEBUG;
	RCALL __SAVELOCR2
;	len -> R17
;	i -> R16
	RCALL SUBOPT_0x11
	RCALL SUBOPT_0x31
	LDI  R26,LOW(122)
	RCALL SUBOPT_0x2D
; 0000 01CF     tempBuf[len++] = 0; //subNodeid
	RCALL SUBOPT_0x2F
; 0000 01D0     tempBuf[len++] = 0; //subNodeId
	RCALL SUBOPT_0x2F
; 0000 01D1     tempBuf[len++] = ieeeAddr[6];
	RCALL SUBOPT_0x30
; 0000 01D2     tempBuf[len++] = ieeeAddr[7];
; 0000 01D3     tempBuf[len++] = ((int)Sensing_Standard_Avr + Sensing_Threshold_Avr_Diff)>>8;
	RCALL SUBOPT_0x31
	RCALL SUBOPT_0x32
	RCALL __EEPROMRDW
	ADD  R30,R2
	ADC  R31,R3
	RCALL SUBOPT_0x35
; 0000 01D4     tempBuf[len++] = (int)(Sensing_Standard_Avr + Sensing_Threshold_Avr_Diff);
	RCALL SUBOPT_0x32
	RCALL __EEPROMRDB
	ADD  R30,R2
	MOVW R26,R0
	ST   X,R30
; 0000 01D5     tempBuf[len++] = g_isSensored_Avr*10;
	RCALL SUBOPT_0x31
	MOVW R22,R30
	LDI  R26,LOW(10)
	MUL  R6,R26
	MOVW R30,R0
	MOVW R26,R22
	ST   X,R30
; 0000 01D6     tempBuf[len++] = g_isSensored_Avr2*10;
	RCALL SUBOPT_0x31
	MOVW R22,R30
	LDI  R26,LOW(10)
	MUL  R9,R26
	MOVW R30,R0
	MOVW R26,R22
	ST   X,R30
; 0000 01D7     tempBuf[len++] = g_debug_data_size;
	RCALL SUBOPT_0x31
	ST   Z,R8
; 0000 01D8 
; 0000 01D9     for(i=0; i < g_debug_data_size; i++){
	LDI  R16,LOW(0)
_0x88:
	CP   R16,R8
	BRSH _0x89
; 0000 01DA         tempBuf[len++] =  g_debug_data_history[i]>>8;
	RCALL SUBOPT_0x31
	MOVW R0,R30
	MOV  R30,R16
	RCALL SUBOPT_0x10
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x35
; 0000 01DB         tempBuf[len++] =  g_debug_data_history[i];
	MOVW R0,R30
	MOV  R30,R16
	RCALL SUBOPT_0x10
	RCALL SUBOPT_0x36
	RCALL SUBOPT_0x37
	ST   X,R30
; 0000 01DC     }
	SUBI R16,-1
	RJMP _0x88
_0x89:
; 0000 01DD     g_debug_data_size=0;
	CLR  R8
; 0000 01DE 
; 0000 01DF     sendGCPPacket(0, tempBuf, len);
	LDI  R30,LOW(0)
	LDI  R31,HIGH(0)
	RCALL SUBOPT_0x34
; 0000 01E0 }
	RCALL __LOADLOCR2P
	RET
;
;void sendSetSensingLevelRes(unsigned int seqNum, unsigned int avrDiff){
; 0000 01E2 void sendSetSensingLevelRes(unsigned int seqNum, unsigned int avrDiff){
_sendSetSensingLevelRes:
; 0000 01E3     unsigned char len=0;
; 0000 01E4 
; 0000 01E5     tempBuf[len++] = PLCS_GCP_RES_SET_SENSINGLEVEL;
	RCALL SUBOPT_0x2C
;	seqNum -> Y+3
;	avrDiff -> Y+1
;	len -> R17
	LDI  R26,LOW(129)
	RCALL SUBOPT_0x2D
; 0000 01E6     tempBuf[len++] = 0;
	RCALL SUBOPT_0x2F
; 0000 01E7     tempBuf[len++] = 0;
	RCALL SUBOPT_0x2F
; 0000 01E8     tempBuf[len++] = ieeeAddr[6];
	RCALL SUBOPT_0x30
; 0000 01E9     tempBuf[len++] = ieeeAddr[7];
; 0000 01EA     tempBuf[len++] = 0;
	RCALL SUBOPT_0x31
	RCALL SUBOPT_0x38
; 0000 01EB 
; 0000 01EC     sendGCPPacket(seqNum, tempBuf, len);
	LDD  R30,Y+3
	LDD  R31,Y+3+1
	RCALL SUBOPT_0x34
; 0000 01ED }
	LDD  R17,Y+0
	ADIW R28,5
	RET
;
;void sendSetModeRes(unsigned int seqNum, unsigned char mode){
; 0000 01EF void sendSetModeRes(unsigned int seqNum, unsigned char mode){
_sendSetModeRes:
; 0000 01F0     unsigned char len=0;
; 0000 01F1     tempBuf[len++] = PLCS_GCP_RES_SET_MODE;
	RCALL SUBOPT_0x2C
;	seqNum -> Y+2
;	mode -> Y+1
;	len -> R17
	LDI  R26,LOW(131)
	RCALL SUBOPT_0x2D
; 0000 01F2     tempBuf[len++] = 0;
	RCALL SUBOPT_0x2F
; 0000 01F3     tempBuf[len++] = 0;
	RCALL SUBOPT_0x2F
; 0000 01F4     tempBuf[len++] = ieeeAddr[6];
	RCALL SUBOPT_0x30
; 0000 01F5     tempBuf[len++] = ieeeAddr[7];
; 0000 01F6     tempBuf[len++] = 0;
	RCALL SUBOPT_0x31
	RCALL SUBOPT_0x38
; 0000 01F7 
; 0000 01F8     sendGCPPacket(seqNum, tempBuf, len);
	RCALL SUBOPT_0x2B
	LDI  R30,LOW(_tempBuf)
	LDI  R31,HIGH(_tempBuf)
	RCALL SUBOPT_0x39
	RCALL _sendGCPPacket
; 0000 01F9 }
	LDD  R17,Y+0
	RJMP _0x20A0009
;
;void handleSetSensingLevelReq(unsigned int seqNum, unsigned int avrDiff){
; 0000 01FB void handleSetSensingLevelReq(unsigned int seqNum, unsigned int avrDiff){
_handleSetSensingLevelReq:
; 0000 01FC     Sensing_Threshold_Avr_Diff = avrDiff;
;	seqNum -> Y+2
;	avrDiff -> Y+0
	LD   R30,Y
	LDD  R31,Y+1
	LDI  R26,LOW(_Sensing_Threshold_Avr_Diff)
	LDI  R27,HIGH(_Sensing_Threshold_Avr_Diff)
	RCALL __EEPROMWRW
; 0000 01FD     sendSetSensingLevelRes(seqNum, avrDiff);
	RCALL SUBOPT_0x2B
	RCALL SUBOPT_0x2B
	RCALL _sendSetSensingLevelRes
; 0000 01FE }
_0x20A0009:
	ADIW R28,4
	RET
;
;void handleSetModeReq(unsigned int seqNum, unsigned char mode){
; 0000 0200 void handleSetModeReq(unsigned int seqNum, unsigned char mode){
_handleSetModeReq:
; 0000 0201     if(mode==0x00)
;	seqNum -> Y+1
;	mode -> Y+0
	LD   R30,Y
	CPI  R30,0
	BRNE _0x8A
; 0000 0202         NORMAL=1;
	LDI  R30,LOW(1)
	MOV  R5,R30
; 0000 0203     else
	RJMP _0x8B
_0x8A:
; 0000 0204         NORMAL=0;
	CLR  R5
; 0000 0205 
; 0000 0206     g_timer0_count_NormalMode=0;
_0x8B:
	LDI  R30,LOW(0)
	STS  _g_timer0_count_NormalMode,R30
	STS  _g_timer0_count_NormalMode+1,R30
	STS  _g_timer0_count_NormalMode+2,R30
	STS  _g_timer0_count_NormalMode+3,R30
; 0000 0207 
; 0000 0208     sendSetModeRes(seqNum, mode);
	LDD  R30,Y+1
	LDD  R31,Y+1+1
	RCALL SUBOPT_0x20
	LDD  R30,Y+2
	ST   -Y,R30
	RCALL _sendSetModeRes
; 0000 0209 }
	RJMP _0x20A0004
;
;unsigned int seqNumGenerator=0;
;void sendNCPPacket(char *payload, unsigned char len) {
; 0000 020C void sendNCPPacket(char *payload, unsigned char len) {
_sendNCPPacket:
; 0000 020D     int i=0;
; 0000 020E     static char data[DEFAULT_BUF_SIZE];
; 0000 020F     unsigned int length=0;
; 0000 0210     unsigned char checkSum=0;
; 0000 0211     unsigned char lenTemp=0;
; 0000 0212     unsigned char seqNum[2];
; 0000 0213                      seqNumGenerator=0xdddd;
	SBIW R28,2
	RCALL SUBOPT_0x3A
;	*payload -> Y+9
;	len -> Y+8
;	i -> R16,R17
;	length -> R18,R19
;	checkSum -> R21
;	lenTemp -> R20
;	seqNum -> Y+6
	LDI  R21,0
	LDI  R20,0
	LDI  R30,LOW(56797)
	LDI  R31,HIGH(56797)
	STS  _seqNumGenerator,R30
	STS  _seqNumGenerator+1,R31
; 0000 0214     seqNum[0] = seqNumGenerator>>8;
	LDS  R30,_seqNumGenerator+1
	STD  Y+6,R30
; 0000 0215     seqNum[1] = seqNumGenerator;
	LDS  R30,_seqNumGenerator
	STD  Y+7,R30
; 0000 0216     seqNumGenerator++;
	LDI  R26,LOW(_seqNumGenerator)
	LDI  R27,HIGH(_seqNumGenerator)
	RCALL SUBOPT_0x1A
; 0000 0217 
; 0000 0218     lenTemp = len+13;
	LDD  R30,Y+8
	RCALL SUBOPT_0x3B
; 0000 0219 
; 0000 021A     data[length++] = 0xFA;
	RCALL SUBOPT_0x3C
	LDI  R26,LOW(250)
	RCALL SUBOPT_0x3D
; 0000 021B     length+= getTranslatedCode(&(data[length]), &lenTemp, 1);
	RCALL SUBOPT_0x3E
	IN   R30,SPL
	IN   R31,SPH
	RCALL SUBOPT_0x20
	PUSH R20
	RCALL SUBOPT_0x3F
	POP  R20
	RCALL SUBOPT_0x40
; 0000 021C     data[length++] = 0x10;
	RCALL SUBOPT_0x41
	LDI  R26,LOW(16)
	RCALL SUBOPT_0x3D
; 0000 021D     data[length++] = 0x00;
	__ADDWRN 18,19,1
	RCALL SUBOPT_0x3C
	RCALL SUBOPT_0x38
; 0000 021E     data[length++] = 0x00;
	RCALL SUBOPT_0x41
	RCALL SUBOPT_0x38
; 0000 021F     length+= getTranslatedCode(&(data[length]), netAddr, 2);
	MOVW R30,R18
	RCALL SUBOPT_0x3E
	RCALL SUBOPT_0x42
; 0000 0220     data[length++] = NCP_PROTOCOL_ID;
	RCALL SUBOPT_0x41
	LDI  R26,LOW(128)
	RCALL SUBOPT_0x3D
; 0000 0221     data[length++] = PLCS_NCP_ID;
	RCALL SUBOPT_0x43
	RCALL SUBOPT_0x3C
	LDI  R26,LOW(1)
	RCALL SUBOPT_0x3D
; 0000 0222     length+= getTranslatedCode(&(data[length]), seqNum, 2);
	RCALL SUBOPT_0x3E
	RCALL SUBOPT_0x44
; 0000 0223     data[length++] = 0;
	RCALL SUBOPT_0x41
	RCALL SUBOPT_0x38
; 0000 0224     data[length++] = 0;
	RCALL SUBOPT_0x41
	RCALL SUBOPT_0x38
; 0000 0225     data[length++] = ieeeAddr[6];
	RCALL SUBOPT_0x41
	MOVW R0,R30
	RCALL SUBOPT_0x45
	RCALL SUBOPT_0x33
; 0000 0226     data[length++] = ieeeAddr[7];
	RCALL SUBOPT_0x41
	MOVW R0,R30
	RCALL SUBOPT_0x46
	RCALL SUBOPT_0x33
; 0000 0227     length+= getTranslatedCode(&(data[length]), payload, len);
	MOVW R30,R18
	RCALL SUBOPT_0x3E
	LDD  R30,Y+11
	LDD  R31,Y+11+1
	RCALL SUBOPT_0x20
	LDD  R30,Y+12
	RCALL SUBOPT_0x47
; 0000 0228 
; 0000 0229     for(i=0; i < len; i++){
	RCALL SUBOPT_0x48
_0x8D:
	LDD  R30,Y+8
	RCALL SUBOPT_0x49
	BRGE _0x8E
; 0000 022A         checkSum+= payload[i];
	MOVW R30,R16
	LDD  R26,Y+9
	LDD  R27,Y+9+1
	RCALL SUBOPT_0x36
	RCALL SUBOPT_0x26
; 0000 022B     }
	RCALL SUBOPT_0x27
	RJMP _0x8D
_0x8E:
; 0000 022C     checkSum+=0x10+netAddr[0]+netAddr[1]+NCP_PROTOCOL_ID+PLCS_NCP_ID+seqNum[0]+seqNum[1]+ieeeAddr[6]+ieeeAddr[7];
	RCALL SUBOPT_0x4A
	SUBI R30,-LOW(129)
	RCALL SUBOPT_0x4B
	RCALL __EEPROMRDB
	ADD  R0,R30
	RCALL SUBOPT_0x46
	RCALL SUBOPT_0x4C
; 0000 022D 
; 0000 022E     length+= getTranslatedCode(&(data[length]), &checkSum, 1);
	RCALL SUBOPT_0x3E
	IN   R30,SPL
	IN   R31,SPH
	RCALL SUBOPT_0x20
	PUSH R21
	RCALL SUBOPT_0x3F
	POP  R21
	RCALL SUBOPT_0x40
; 0000 022F     data[length++] = 0xAF;
	RCALL SUBOPT_0x41
	LDI  R26,LOW(175)
	STD  Z+0,R26
; 0000 0230     putstr0(data, length);
	LDI  R30,LOW(_data_S0000011000)
	LDI  R31,HIGH(_data_S0000011000)
	RCALL SUBOPT_0x4D
; 0000 0231 }
	RJMP _0x20A0003
;
;void sendGCPPacket(unsigned int seqNum, char *payload, unsigned char len) {
; 0000 0233 void sendGCPPacket(unsigned int seqNum, char *payload, unsigned char len) {
_sendGCPPacket:
; 0000 0234     int i=0;
; 0000 0235     char data[DEFAULT_BUF_SIZE];
; 0000 0236     unsigned int length=0;
; 0000 0237     unsigned char checkSum=0;
; 0000 0238     unsigned char lenTemp=0;
; 0000 0239     unsigned char seqNum_byte[2];
; 0000 023A 
; 0000 023B     seqNum_byte[0] = seqNum>>8;
	SBIW R28,52
	RCALL SUBOPT_0x3A
;	seqNum -> Y+61
;	*payload -> Y+59
;	len -> Y+58
;	i -> R16,R17
;	data -> Y+8
;	length -> R18,R19
;	checkSum -> R21
;	lenTemp -> R20
;	seqNum_byte -> Y+6
	LDI  R21,0
	LDI  R20,0
	LDD  R30,Y+62
	STD  Y+6,R30
; 0000 023C     seqNum_byte[1] = seqNum;
	LDD  R30,Y+61
	STD  Y+7,R30
; 0000 023D 
; 0000 023E     lenTemp = len+13;
	LDD  R30,Y+58
	RCALL SUBOPT_0x3B
; 0000 023F 
; 0000 0240     data[length++] = 0xFA;
	RCALL SUBOPT_0x4E
	RCALL SUBOPT_0x36
	LDI  R30,LOW(250)
	RCALL SUBOPT_0x4F
; 0000 0241     length+= getTranslatedCode(&(data[length]), &lenTemp, 1);
	RCALL SUBOPT_0x50
	IN   R30,SPL
	IN   R31,SPH
	RCALL SUBOPT_0x20
	PUSH R20
	RCALL SUBOPT_0x3F
	POP  R20
	RCALL SUBOPT_0x40
; 0000 0242     data[length++] = 0x10;
	RCALL SUBOPT_0x51
	RCALL SUBOPT_0x52
	LDI  R30,LOW(16)
	RCALL SUBOPT_0x4F
; 0000 0243     data[length++] = 0x00;
	RCALL SUBOPT_0x43
	RCALL SUBOPT_0x52
	RCALL SUBOPT_0x53
; 0000 0244     data[length++] = 0x00;
	RCALL SUBOPT_0x43
	RCALL SUBOPT_0x52
	RCALL SUBOPT_0x53
; 0000 0245     length+= getTranslatedCode(&(data[length]), netAddr, 2);
	RCALL SUBOPT_0x50
	RCALL SUBOPT_0x42
; 0000 0246     data[length++] = NCP_PROTOCOL_ID;
	RCALL SUBOPT_0x51
	RCALL SUBOPT_0x52
	LDI  R30,LOW(128)
	RCALL SUBOPT_0x4F
; 0000 0247     data[length++] = PLCS_GCP_ID;
	RCALL SUBOPT_0x43
	RCALL SUBOPT_0x52
	LDI  R30,LOW(33)
	RCALL SUBOPT_0x4F
; 0000 0248     length+= getTranslatedCode(&(data[length]), seqNum_byte, 2);
	RCALL SUBOPT_0x50
	RCALL SUBOPT_0x44
; 0000 0249     data[length++] = 0;
	RCALL SUBOPT_0x51
	RCALL SUBOPT_0x52
	RCALL SUBOPT_0x53
; 0000 024A     data[length++] = 0;
	RCALL SUBOPT_0x43
	RCALL SUBOPT_0x52
	RCALL SUBOPT_0x53
; 0000 024B     data[length++] = ieeeAddr[6];
	RCALL SUBOPT_0x43
	RCALL SUBOPT_0x4E
	RCALL SUBOPT_0xF
	RCALL SUBOPT_0x45
	RCALL SUBOPT_0x33
; 0000 024C     data[length++] = ieeeAddr[7];
	RCALL SUBOPT_0x51
	RCALL SUBOPT_0x4E
	RCALL SUBOPT_0xF
	RCALL SUBOPT_0x46
	RCALL SUBOPT_0x33
; 0000 024D     length+= getTranslatedCode(&(data[length]), payload, len);
	MOVW R30,R18
	RCALL SUBOPT_0x50
	LDD  R30,Y+61
	LDD  R31,Y+61+1
	RCALL SUBOPT_0x20
	LDD  R30,Y+62
	RCALL SUBOPT_0x47
; 0000 024E 
; 0000 024F     for(i=0; i < len; i++){
	RCALL SUBOPT_0x48
_0x90:
	LDD  R30,Y+58
	RCALL SUBOPT_0x49
	BRGE _0x91
; 0000 0250         checkSum+= payload[i];
	MOVW R30,R16
	LDD  R26,Y+59
	LDD  R27,Y+59+1
	RCALL SUBOPT_0x36
	RCALL SUBOPT_0x26
; 0000 0251     }
	RCALL SUBOPT_0x27
	RJMP _0x90
_0x91:
; 0000 0252     checkSum+=0x10+netAddr[0]+netAddr[1]+NCP_PROTOCOL_ID+PLCS_GCP_ID+seqNum_byte[0]+seqNum_byte[1]+ieeeAddr[6]+ieeeAddr[7];
	RCALL SUBOPT_0x4A
	SUBI R30,-LOW(161)
	RCALL SUBOPT_0x4B
	RCALL __EEPROMRDB
	ADD  R0,R30
	RCALL SUBOPT_0x46
	RCALL SUBOPT_0x4C
; 0000 0253 
; 0000 0254     length+= getTranslatedCode(&(data[length]), &checkSum, 1);
	RCALL SUBOPT_0x50
	IN   R30,SPL
	IN   R31,SPH
	RCALL SUBOPT_0x20
	PUSH R21
	RCALL SUBOPT_0x3F
	POP  R21
	RCALL SUBOPT_0x40
; 0000 0255     data[length++] = 0xAF;
	RCALL SUBOPT_0x51
	RCALL SUBOPT_0x52
	LDI  R30,LOW(175)
	ST   X,R30
; 0000 0256     g_zigbeeSendTimer=1;
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	MOVW R10,R30
; 0000 0257     putstr0(data, length);
	MOVW R30,R28
	ADIW R30,8
	RCALL SUBOPT_0x4D
; 0000 0258 }
	ADIW R28,63
	RET
;
;void initWatchDog(){
; 0000 025A void initWatchDog(){
_initWatchDog:
; 0000 025B     // Watchdog Timer initialization
; 0000 025C     // Watchdog Timer Prescaler: OSC/2048k
; 0000 025D     #pragma optsize-
; 0000 025E     WDTCSR=0x18;
	LDI  R30,LOW(24)
	STS  96,R30
; 0000 025F     //delay_ms(1);
; 0000 0260     WDTCSR=0x1E;
	LDI  R30,LOW(30)
	STS  96,R30
; 0000 0261     //delay_ms(1);
; 0000 0262     //WDTCSR=0x0E;
; 0000 0263     #ifdef _OPTIMIZE_SIZE_
; 0000 0264     #pragma optsize+
; 0000 0265     #endif
; 0000 0266 
; 0000 0267 }
	RET
;
;void disableWatchDog() {
; 0000 0269 void disableWatchDog() {
; 0000 026A     // Watchdog Timer initialization
; 0000 026B     // Watchdog Timer Prescaler: OSC/2048k
; 0000 026C     #pragma optsize-
; 0000 026D     WDTCSR=0x17;
; 0000 026E     delay_ms(1);
; 0000 026F     WDTCSR=0x07;
; 0000 0270     #ifdef _OPTIMIZE_SIZE_
; 0000 0271     #pragma optsize+
; 0000 0272     #endif
; 0000 0273 }
;
;void resetWatchDog() {
; 0000 0275 void resetWatchDog() {
; 0000 0276     disableWatchDog();
; 0000 0277     initWatchDog();
; 0000 0278 }
;
;void main(void)
; 0000 027B {
_main:
; 0000 027C     unsigned char i=0;
; 0000 027D 
; 0000 027E     initPort();
;	i -> R17
	LDI  R17,0
	RCALL _initPort
; 0000 027F     initRotary();
	RCALL _initRotary
; 0000 0280     initUsart();
	RCALL _initUsart
; 0000 0281     initADC();
	RCALL _initADC
; 0000 0282     LED_ON;
	SBI  0x5,1
; 0000 0283 
; 0000 0284     for(i=0; i < 5; i++) {
	LDI  R17,LOW(0)
_0x93:
	CPI  R17,5
	BRSH _0x94
; 0000 0285         LED_ON;
	SBI  0x5,1
; 0000 0286         delay_ms(200);
	RCALL SUBOPT_0x54
; 0000 0287         LED_OFF;
	CBI  0x5,1
; 0000 0288         delay_ms(200);
	RCALL SUBOPT_0x54
; 0000 0289     }
	SUBI R17,-1
	RJMP _0x93
_0x94:
; 0000 028A     LED_ON;
	SBI  0x5,1
; 0000 028B 
; 0000 028C     sendReqZigbeeInfo();
	RCALL _sendReqZigbeeInfo
; 0000 028D     delay_ms(500);
	LDI  R30,LOW(500)
	LDI  R31,HIGH(500)
	RCALL SUBOPT_0x55
; 0000 028E     LED_OFF;
	CBI  0x5,1
; 0000 028F 
; 0000 0290     initTimer0();
	RCALL _initTimer0
; 0000 0291     initTimer1();
	RCALL _initTimer1
; 0000 0292     initWatchDog();
	RCALL _initWatchDog
; 0000 0293     /*
; 0000 0294     while(1) {
; 0000 0295         delay_ms(500);
; 0000 0296         reset();
; 0000 0297     } */
; 0000 0298 
; 0000 0299     sendSetZigbeePreconfig(0x15);
	LDI  R30,LOW(21)
	ST   -Y,R30
	RCALL _sendSetZigbeePreconfig
; 0000 029A     delay_ms(100);
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	RCALL SUBOPT_0x55
; 0000 029B     sendReqZigbeeReset();
	RCALL _sendReqZigbeeReset
; 0000 029C     delay_ms(1000);
	LDI  R30,LOW(1000)
	LDI  R31,HIGH(1000)
	RCALL SUBOPT_0x55
; 0000 029D 
; 0000 029E 
; 0000 029F     // Global enable interrupts
; 0000 02A0     #asm("sei")
	sei
; 0000 02A1     while (1)
_0x95:
; 0000 02A2     {
; 0000 02A3         handleMessage();
	RCALL _handleMessage
; 0000 02A4     };
	RJMP _0x95
; 0000 02A5 }
_0x98:
	RJMP _0x98
;#include "adc.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_powersave=0x06
	.EQU __sm_standby=0x0C
	.EQU __sm_ext_standby=0x0E
	.SET power_ctrl_reg=smcr
	#endif
;#include "gpio.h"
;
;void initADC(){
; 0001 0004 void initADC(){

	.CSEG
_initADC:
; 0001 0005 
; 0001 0006     // ADC initialization
; 0001 0007     // ADC Clock frequency: 250.000 kHz
; 0001 0008     // ADC Voltage Reference: AREF pin
; 0001 0009 
; 0001 000A     ADMUX = 0x00; //select adc input 0
	LDI  R30,LOW(0)
	STS  124,R30
; 0001 000B     ACSR = 0x80;
	LDI  R30,LOW(128)
	OUT  0x30,R30
; 0001 000C     ADCSRA=0xCF;
	LDI  R30,LOW(207)
	STS  122,R30
; 0001 000D     DIDR0=0x01;
	LDI  R30,LOW(1)
	STS  126,R30
; 0001 000E }
	RET
;
;unsigned int adc_cnt=0;
;unsigned int adc_index=0;
;unsigned int adc_pre_data[10];
;// ADC interrupt service routine
;interrupt [ADC_INT] void adc_isr(void)
; 0001 0015 {
_adc_isr:
	ST   -Y,R0
	ST   -Y,R1
	ST   -Y,R22
	ST   -Y,R23
	ST   -Y,R24
	ST   -Y,R25
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0001 0016     unsigned long avr=0;
; 0001 0017     unsigned int i=0;
; 0001 0018 
; 0001 0019     for(i=0; i < 10; i++){
	SBIW R28,4
	LDI  R30,LOW(0)
	ST   Y,R30
	STD  Y+1,R30
	STD  Y+2,R30
	STD  Y+3,R30
	RCALL __SAVELOCR2
;	avr -> Y+2
;	i -> R16,R17
	RCALL SUBOPT_0x48
	RCALL SUBOPT_0x48
_0x20004:
	__CPWRN 16,17,10
	BRSH _0x20005
; 0001 001A         avr+=adc_pre_data[i];
	RCALL SUBOPT_0x56
	RCALL SUBOPT_0x4
	RCALL SUBOPT_0x57
	RCALL SUBOPT_0x7
	RCALL __ADDD12
	RCALL SUBOPT_0x58
; 0001 001B         if(i!=9)adc_pre_data[i] = adc_pre_data[i+1];
	LDI  R30,LOW(9)
	LDI  R31,HIGH(9)
	CP   R30,R16
	CPC  R31,R17
	BREQ _0x20006
	RCALL SUBOPT_0x56
	RCALL SUBOPT_0xF
	MOVW R26,R16
	LSL  R26
	ROL  R27
	__ADDW2MN _adc_pre_data,2
	RCALL __GETW1P
	RCALL SUBOPT_0x5
; 0001 001C     }
_0x20006:
	RCALL SUBOPT_0x27
	RJMP _0x20004
_0x20005:
; 0001 001D     adc_pre_data[9] = ADCW;
	LDS  R30,120
	LDS  R31,120+1
	__PUTW1MN _adc_pre_data,18
; 0001 001E 
; 0001 001F     avr/=10;
	RCALL SUBOPT_0x57
	__GETD1N 0xA
	RCALL __DIVD21U
	RCALL SUBOPT_0x58
; 0001 0020     g_adc_data_history[adc_index++] =  avr;
	LDI  R26,LOW(_adc_index)
	LDI  R27,HIGH(_adc_index)
	RCALL SUBOPT_0x1A
	SBIW R30,1
	LDI  R26,LOW(_g_adc_data_history)
	LDI  R27,HIGH(_g_adc_data_history)
	LSL  R30
	ROL  R31
	RCALL SUBOPT_0x2
	LDD  R26,Y+2
	LDD  R27,Y+2+1
	STD  Z+0,R26
	STD  Z+1,R27
; 0001 0021 
; 0001 0022     if(adc_index >= NumOFHistory)
	LDS  R26,_adc_index
	LDS  R27,_adc_index+1
	SBIW R26,40
	BRLO _0x20007
; 0001 0023         adc_index=0;
	LDI  R30,LOW(0)
	STS  _adc_index,R30
	STS  _adc_index+1,R30
; 0001 0024 
; 0001 0025 
; 0001 0026     adc_cnt++;
_0x20007:
	LDI  R26,LOW(_adc_cnt)
	LDI  R27,HIGH(_adc_cnt)
	RCALL SUBOPT_0x1A
; 0001 0027 
; 0001 0028     // Start the AD conversion
; 0001 0029     ADCSRA|=0x40;
	LDS  R30,122
	ORI  R30,0x40
	STS  122,R30
; 0001 002A }
	RCALL __LOADLOCR2
	ADIW R28,6
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	LD   R25,Y+
	LD   R24,Y+
	LD   R23,Y+
	LD   R22,Y+
	LD   R1,Y+
	LD   R0,Y+
	RETI
;#include "GPIO.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_powersave=0x06
	.EQU __sm_standby=0x0C
	.EQU __sm_ext_standby=0x0E
	.SET power_ctrl_reg=smcr
	#endif
;
;// Declare your global variables here
;void initPort() {
; 0002 0004 void initPort() {

	.CSEG
_initPort:
; 0002 0005     DDRC=0x00;
	LDI  R30,LOW(0)
	OUT  0x7,R30
; 0002 0006     DDRB=0x0B;
	LDI  R30,LOW(11)
	OUT  0x4,R30
; 0002 0007     DDRD=0x00;
	LDI  R30,LOW(0)
	OUT  0xA,R30
; 0002 0008 
; 0002 0009     PORTB=0x01;
	LDI  R30,LOW(1)
	OUT  0x5,R30
; 0002 000A     PORTC=0x00;
	LDI  R30,LOW(0)
	OUT  0x8,R30
; 0002 000B     PORTD=0x00;
	OUT  0xB,R30
; 0002 000C }
	RET
;
;void reset(){
; 0002 000E void reset(){
_reset:
; 0002 000F     PORTB&= ~(0x01);
	CBI  0x5,0
; 0002 0010     delay_ms(100);
	LDI  R30,LOW(100)
	LDI  R31,HIGH(100)
	RCALL SUBOPT_0x55
; 0002 0011     PORTB|= 0x01;
	SBI  0x5,0
; 0002 0012 }
	RET
;#include "NCprotocol.h"
;#include "rotary.h"
;#include "common.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_powersave=0x06
	.EQU __sm_standby=0x0C
	.EQU __sm_ext_standby=0x0E
	.SET power_ctrl_reg=smcr
	#endif
;
;void initRotary(){
; 0005 0004 void initRotary(){

	.CSEG
_initRotary:
; 0005 0005     DDRD &= ~(0x3C);
	IN   R30,0xA
	ANDI R30,LOW(0xC3)
	OUT  0xA,R30
; 0005 0006     DDRC &= ~(0x3C);
	IN   R30,0x7
	ANDI R30,LOW(0xC3)
	OUT  0x7,R30
; 0005 0007 
; 0005 0008     PORTD |= 0x3C;
	IN   R30,0xB
	ORI  R30,LOW(0x3C)
	OUT  0xB,R30
; 0005 0009     PORTC |= 0x3C;
	IN   R30,0x8
	ORI  R30,LOW(0x3C)
	OUT  0x8,R30
; 0005 000A }
	RET
;
;unsigned char getRotaryValue() {
; 0005 000C unsigned char getRotaryValue() {
_getRotaryValue:
; 0005 000D     unsigned char id=0;
; 0005 000E 
; 0005 000F    if((PINC & 0x20)==0) id |= (0x01 << 7);
	RCALL SUBOPT_0x59
;	id -> R17
	SBIS 0x6,5
	ORI  R17,LOW(128)
; 0005 0010     if((PINC & 0x10)==0) id |= (0x01 << 6);
	SBIS 0x6,4
	ORI  R17,LOW(64)
; 0005 0011     if((PINC & 0x08)==0) id |= (0x01 << 5);
	SBIS 0x6,3
	ORI  R17,LOW(32)
; 0005 0012     if((PINC & 0x04)==0) id |= (0x01 << 4);
	SBIS 0x6,2
	ORI  R17,LOW(16)
; 0005 0013 
; 0005 0014     if((PIND & 0x20)==0) id |= (0x01 << 3);
	SBIS 0x9,5
	ORI  R17,LOW(8)
; 0005 0015     if((PIND & 0x10)==0) id |= (0x01 << 2);
	SBIS 0x9,4
	ORI  R17,LOW(4)
; 0005 0016     if((PIND & 0x08)==0) id |= (0x01 << 1);
	SBIS 0x9,3
	ORI  R17,LOW(2)
; 0005 0017     if((PIND & 0x04)==0) id |= (0x01 << 0);
	SBIS 0x9,2
	ORI  R17,LOW(1)
; 0005 0018 
; 0005 0019     return id;
	RJMP _0x20A0007
; 0005 001A }
;#include "timer.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_powersave=0x06
	.EQU __sm_standby=0x0C
	.EQU __sm_ext_standby=0x0E
	.SET power_ctrl_reg=smcr
	#endif
;int timer0_count=0;
;int timer1_count=0;
;
;void initTimer0(){
; 0006 0005 void initTimer0(){

	.CSEG
_initTimer0:
; 0006 0006     // Timer/Counter 0 initialization
; 0006 0007     // Clock source: System Clock
; 0006 0008     // Clock value: 15.625 kHz
; 0006 0009     // Mode: Normal top=FFh
; 0006 000A     // OC0 output: Disconnected
; 0006 000B     ASSR=0x00;
	LDI  R30,LOW(0)
	STS  182,R30
; 0006 000C     TCCR0B=0x05;
	LDI  R30,LOW(5)
	OUT  0x25,R30
; 0006 000D     TCNT0=0x00;
	LDI  R30,LOW(0)
	OUT  0x26,R30
; 0006 000E     // Timer(s)/Counter(s) Interrupt(s) initialization
; 0006 000F     TIMSK0|=0x01;
	LDS  R30,110
	ORI  R30,1
	STS  110,R30
; 0006 0010 }
	RET
;
;void initTimer1() {
; 0006 0012 void initTimer1() {
_initTimer1:
; 0006 0013     TCCR1A=0x00;
	LDI  R30,LOW(0)
	STS  128,R30
; 0006 0014     TCCR1B=0x03;
	LDI  R30,LOW(3)
	STS  129,R30
; 0006 0015     TCNT1H=0xFF;
	LDI  R30,LOW(255)
	STS  133,R30
; 0006 0016     TCNT1L=0x00;
	LDI  R30,LOW(0)
	STS  132,R30
; 0006 0017     ICR1H=0x00;
	STS  135,R30
; 0006 0018     ICR1L=0x00;
	STS  134,R30
; 0006 0019     OCR1AH=0x00;
	STS  137,R30
; 0006 001A     OCR1AL=0x00;
	STS  136,R30
; 0006 001B     OCR1BH=0x00;
	STS  139,R30
; 0006 001C     OCR1BL=0x00;
	STS  138,R30
; 0006 001D     TIMSK1|=0x01;
	LDS  R30,111
	ORI  R30,1
	STS  111,R30
; 0006 001E }
	RET
;
; // Timer 0 overflow interrupt service routine
;interrupt [TIM0_OVF] void timer0_ovf_isr(void)
; 0006 0022 {
_timer0_ovf_isr:
	RCALL SUBOPT_0x5A
; 0006 0023     // Place your code here
; 0006 0024     if(timer0_count > 5){
	LDS  R26,_timer0_count
	LDS  R27,_timer0_count+1
	SBIW R26,6
	BRLT _0xC0003
; 0006 0025         timer0_Fired();
	RCALL _timer0_Fired
; 0006 0026         timer0_count=0;
	LDI  R30,LOW(0)
	STS  _timer0_count,R30
	STS  _timer0_count+1,R30
; 0006 0027     }
; 0006 0028     timer0_count++;
_0xC0003:
	LDI  R26,LOW(_timer0_count)
	LDI  R27,HIGH(_timer0_count)
	RCALL SUBOPT_0x1A
; 0006 0029 }
	RJMP _0xC0004
;
;// Timer 1 overflow interrupt service routine
;interrupt [TIM1_OVF] void timer1_ovf_isr(void)
; 0006 002D {
_timer1_ovf_isr:
	RCALL SUBOPT_0x5A
; 0006 002E     TCNT1H=0xF7;
	LDI  R30,LOW(247)
	STS  133,R30
; 0006 002F     TCNT1L=0xD0;
	LDI  R30,LOW(208)
	STS  132,R30
; 0006 0030     timer1_Fired();
	RCALL _timer1_Fired
; 0006 0031 }
_0xC0004:
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R27,Y+
	LD   R26,Y+
	LD   R25,Y+
	LD   R24,Y+
	LD   R23,Y+
	LD   R22,Y+
	LD   R15,Y+
	LD   R1,Y+
	LD   R0,Y+
	RETI
;
;#include "usart.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_powersave=0x06
	.EQU __sm_standby=0x0C
	.EQU __sm_ext_standby=0x0E
	.SET power_ctrl_reg=smcr
	#endif
;char rx_buffer1[RX_BUFFER_SIZE1];
;
;#if RX_BUFFER_SIZE1<256
;unsigned char rx_wr_index1,rx_rd_index1,rx_counter1;
;#else
;unsigned int rx_wr_index1,rx_rd_index1,rx_counter1;
;#endif
;
;// This flag is set on USART1 Receiver buffer overflow
;bit rx_buffer_overflow1;
;
;void initUsart(){
; 0007 000D void initUsart(){

	.CSEG
_initUsart:
; 0007 000E     // USART1 initialization
; 0007 000F     // Communication Parameters: 8 Data, 1 Stop, No Parity
; 0007 0010     // USART1 Receiver: On
; 0007 0011     // USART1 Transmitter: On
; 0007 0012     // USART1 Mode: Asynchronous
; 0007 0013     // USART1 Baud rate: 115200
; 0007 0014     UCSR0A=0x00;
	LDI  R30,LOW(0)
	STS  192,R30
; 0007 0015     UCSR0B=0x98;
	LDI  R30,LOW(152)
	STS  193,R30
; 0007 0016     UCSR0C=0x06;
	LDI  R30,LOW(6)
	STS  194,R30
; 0007 0017     UBRR0H=0x00;
	LDI  R30,LOW(0)
	STS  197,R30
; 0007 0018     UBRR0L=0x08;
	LDI  R30,LOW(8)
	STS  196,R30
; 0007 0019     #asm("sei")
	sei
; 0007 001A }
	RET
;
;// USART1 Receiver interrupt service routine
;interrupt [USART_RXC] void usart_rx_isr(void)
; 0007 001E {
_usart_rx_isr:
	ST   -Y,R26
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
; 0007 001F     char status,data;
; 0007 0020     status=UCSR0A;
	RCALL __SAVELOCR2
;	status -> R17
;	data -> R16
	LDS  R17,192
; 0007 0021     data=UDR0;
	LDS  R16,198
; 0007 0022     //UDR = data;
; 0007 0023     //putchar0(data);
; 0007 0024     if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
	MOV  R30,R17
	ANDI R30,LOW(0x1C)
	BRNE _0xE0003
; 0007 0025     {
; 0007 0026         rx_buffer1[rx_wr_index1]=data;
	LDS  R30,_rx_wr_index1
	RCALL SUBOPT_0x23
	SUBI R30,LOW(-_rx_buffer1)
	SBCI R31,HIGH(-_rx_buffer1)
	ST   Z,R16
; 0007 0027         if (++rx_wr_index1 == RX_BUFFER_SIZE1) rx_wr_index1=0;
	LDS  R26,_rx_wr_index1
	SUBI R26,-LOW(1)
	STS  _rx_wr_index1,R26
	CPI  R26,LOW(0x80)
	BRNE _0xE0004
	LDI  R30,LOW(0)
	STS  _rx_wr_index1,R30
; 0007 0028         if (++rx_counter1 == RX_BUFFER_SIZE1)
_0xE0004:
	LDS  R26,_rx_counter1
	SUBI R26,-LOW(1)
	STS  _rx_counter1,R26
	CPI  R26,LOW(0x80)
	BRNE _0xE0005
; 0007 0029         {
; 0007 002A             rx_counter1=0;
	LDI  R30,LOW(0)
	STS  _rx_counter1,R30
; 0007 002B             rx_buffer_overflow1=1;
	SBI  0x1E,0
; 0007 002C         };
_0xE0005:
; 0007 002D    };
_0xE0003:
; 0007 002E }
	RCALL __LOADLOCR2P
	LD   R30,Y+
	OUT  SREG,R30
	LD   R31,Y+
	LD   R30,Y+
	LD   R26,Y+
	RETI
;
;// Get a character from the USART Receiver buffer
;#pragma used+
;char getchar0(void)
; 0007 0033 {
_getchar0:
; 0007 0034     char data;
; 0007 0035     while (rx_counter1==0);
	ST   -Y,R17
;	data -> R17
_0xE0008:
	LDS  R30,_rx_counter1
	CPI  R30,0
	BREQ _0xE0008
; 0007 0036     data=rx_buffer1[rx_rd_index1];
	LDS  R30,_rx_rd_index1
	RCALL SUBOPT_0x23
	SUBI R30,LOW(-_rx_buffer1)
	SBCI R31,HIGH(-_rx_buffer1)
	LD   R17,Z
; 0007 0037     if (++rx_rd_index1 == RX_BUFFER_SIZE1) rx_rd_index1=0;
	LDS  R26,_rx_rd_index1
	SUBI R26,-LOW(1)
	STS  _rx_rd_index1,R26
	CPI  R26,LOW(0x80)
	BRNE _0xE000B
	LDI  R30,LOW(0)
	STS  _rx_rd_index1,R30
; 0007 0038     #asm("cli")
_0xE000B:
	cli
; 0007 0039     --rx_counter1;
	LDS  R30,_rx_counter1
	SUBI R30,LOW(1)
	STS  _rx_counter1,R30
; 0007 003A     #asm("sei")
	sei
; 0007 003B     return data;
_0x20A0007:
	MOV  R30,R17
_0x20A0008:
	LD   R17,Y+
	RET
; 0007 003C }
;#pragma used-
;// Write a character to the USART Transmitter
;#pragma used+
;void putchar0(char c)
; 0007 0041 {
_putchar0:
; 0007 0042     while ((UCSR0A & DATA_REGISTER_EMPTY)==0);
;	c -> Y+0
_0xE000C:
	LDS  R30,192
	ANDI R30,LOW(0x20)
	BREQ _0xE000C
; 0007 0043     UDR0=c;
	LD   R30,Y
	STS  198,R30
; 0007 0044 }
	ADIW R28,1
	RET
;#pragma used-
;
;void putstr0(char *c, int len){
; 0007 0047 void putstr0(char *c, int len){
_putstr0:
; 0007 0048     int i=0;
; 0007 0049     for(i=0; i < len; i++)
	RCALL __SAVELOCR2
;	*c -> Y+4
;	len -> Y+2
;	i -> R16,R17
	RCALL SUBOPT_0x48
	RCALL SUBOPT_0x48
_0xE0010:
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	CP   R16,R30
	CPC  R17,R31
	BRGE _0xE0011
; 0007 004A         putchar0(c[i]);
	MOVW R30,R16
	LDD  R26,Y+4
	LDD  R27,Y+4+1
	RCALL SUBOPT_0x36
	LD   R30,X
	ST   -Y,R30
	RCALL _putchar0
	RCALL SUBOPT_0x27
	RJMP _0xE0010
_0xE0011:
; 0007 004B }
	RCALL __LOADLOCR2
	ADIW R28,6
	RET
;#include "zigbee.h"
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_powersave=0x06
	.EQU __sm_standby=0x0C
	.EQU __sm_ext_standby=0x0E
	.SET power_ctrl_reg=smcr
	#endif
;#include "util.h"
;#include "usart.h"
;
;#define ZIGBEE_IDLE 0
;#define ZIGBEE_CONNECTED 1
;
;int zigbee_state=0;
;int zigbee_retry_count=0;
;
;void sendReqZigbeeInfo(){
; 0008 000B void sendReqZigbeeInfo(){

	.CSEG
_sendReqZigbeeInfo:
; 0008 000C     char buf[10];
; 0008 000D     unsigned char len=0;
; 0008 000E     buf[len++] = 0x10;
	RCALL SUBOPT_0x5B
;	buf -> Y+1
;	len -> R17
	RCALL SUBOPT_0x5C
	LDI  R30,LOW(16)
	RCALL SUBOPT_0x5D
; 0008 000F     buf[len++] = 0x00;
	LDI  R30,LOW(0)
	RCALL SUBOPT_0x5E
; 0008 0010     sendControlDataToUsart0(buf, len);
	RCALL _sendControlDataToUsart0
; 0008 0011     zigbee_retry_count++;
	LDI  R26,LOW(_zigbee_retry_count)
	LDI  R27,HIGH(_zigbee_retry_count)
	RCALL SUBOPT_0x1A
; 0008 0012     if(zigbee_retry_count > 30) {
	LDS  R26,_zigbee_retry_count
	LDS  R27,_zigbee_retry_count+1
	SBIW R26,31
	BRLT _0x100003
; 0008 0013         zigbee_state = ZIGBEE_IDLE;
	RCALL SUBOPT_0x5F
; 0008 0014     }
; 0008 0015 }
_0x100003:
	RJMP _0x20A0002
;
;void sendSetZigbeePanId(unsigned char id){
; 0008 0017 void sendSetZigbeePanId(unsigned char id){
_sendSetZigbeePanId:
; 0008 0018     char buf[10];
; 0008 0019     unsigned char len=0;
; 0008 001A     buf[len++] = 0x30;
	RCALL SUBOPT_0x5B
;	id -> Y+11
;	buf -> Y+1
;	len -> R17
	RCALL SUBOPT_0x5C
	LDI  R30,LOW(48)
	RCALL SUBOPT_0x5D
; 0008 001B     buf[len++] = 0x01;
	LDI  R30,LOW(1)
	RJMP _0x20A0006
; 0008 001C     buf[len++] = id;
; 0008 001D     sendControlDataToUsart0(buf, len);
; 0008 001E }
;
;void sendSetZigbeeChannel(unsigned char channel){
; 0008 0020 void sendSetZigbeeChannel(unsigned char channel){
_sendSetZigbeeChannel:
; 0008 0021     char buf[10];
; 0008 0022     unsigned char len=0;
; 0008 0023     buf[len++] = 0x40;
	RCALL SUBOPT_0x5B
;	channel -> Y+11
;	buf -> Y+1
;	len -> R17
	RCALL SUBOPT_0x5C
	LDI  R30,LOW(64)
	RCALL SUBOPT_0x5D
; 0008 0024     buf[len++] = 0x02;
	LDI  R30,LOW(2)
	RJMP _0x20A0006
; 0008 0025     buf[len++] = channel;
; 0008 0026     sendControlDataToUsart0(buf, len);
; 0008 0027 }
;
;void sendSetZigbeePreconfig(unsigned char preconfig){
; 0008 0029 void sendSetZigbeePreconfig(unsigned char preconfig){
_sendSetZigbeePreconfig:
; 0008 002A     char buf[10];
; 0008 002B     unsigned char len=0;
; 0008 002C     buf[len++] = 0x40;
	RCALL SUBOPT_0x5B
;	preconfig -> Y+11
;	buf -> Y+1
;	len -> R17
	RCALL SUBOPT_0x5C
	LDI  R30,LOW(64)
	RCALL SUBOPT_0x5D
; 0008 002D     buf[len++] = 0x05;
	LDI  R30,LOW(5)
_0x20A0006:
	ST   X,R30
; 0008 002E     buf[len++] = preconfig;
	RCALL SUBOPT_0x60
	RCALL SUBOPT_0x23
	MOVW R26,R28
	ADIW R26,1
	RCALL SUBOPT_0x2
	LDD  R26,Y+11
	STD  Z+0,R26
; 0008 002F     sendControlDataToUsart0(buf, len);
	MOVW R30,R28
	ADIW R30,1
	RCALL SUBOPT_0x39
	RCALL _sendControlDataToUsart0
; 0008 0030 }
	LDD  R17,Y+0
	ADIW R28,12
	RET
;
;void handleResZigbeeInfo(char *data){
; 0008 0032 void handleResZigbeeInfo(char *data){
_handleResZigbeeInfo:
; 0008 0033     unsigned char i=0;
; 0008 0034     for(i=0; i < 8; i++)
	RCALL SUBOPT_0x59
;	*data -> Y+1
;	i -> R17
	LDI  R17,LOW(0)
_0x100005:
	CPI  R17,8
	BRSH _0x100006
; 0008 0035         ieeeAddr[i] = data[i+3];
	MOV  R30,R17
	RCALL SUBOPT_0x23
	SUBI R30,LOW(-_ieeeAddr)
	SBCI R31,HIGH(-_ieeeAddr)
	MOVW R0,R30
	MOV  R30,R17
	SUBI R30,-LOW(3)
	RCALL SUBOPT_0x61
	RCALL SUBOPT_0x62
	RCALL SUBOPT_0x37
	RCALL __EEPROMWRB
	SUBI R17,-1
	RJMP _0x100005
_0x100006:
; 0008 0037 if(netAddr[0]!=data[14] || netAddr[1]!=data[15])
	RCALL SUBOPT_0x61
	ADIW R26,14
	LD   R30,X
	LDS  R26,_netAddr
	CP   R30,R26
	BRNE _0x100008
	RCALL SUBOPT_0x61
	ADIW R26,15
	LD   R30,X
	__GETB2MN _netAddr,1
	CP   R30,R26
	BREQ _0x100007
_0x100008:
; 0008 0038         zigbee_state = ZIGBEE_IDLE;
	RCALL SUBOPT_0x5F
; 0008 0039     else
	RJMP _0x10000A
_0x100007:
; 0008 003A         zigbee_state = ZIGBEE_CONNECTED;
	RCALL SUBOPT_0x63
; 0008 003B 
; 0008 003C     netAddr[0] = data[14];
_0x10000A:
	RCALL SUBOPT_0x61
	ADIW R26,14
	LD   R30,X
	STS  _netAddr,R30
; 0008 003D     netAddr[1] = data[15];
	RCALL SUBOPT_0x61
	ADIW R26,15
	LD   R30,X
	__PUTB1MN _netAddr,1
; 0008 003E     panId[0] = data[12];
	RCALL SUBOPT_0x61
	ADIW R26,12
	LD   R30,X
	LDI  R26,LOW(_panId)
	LDI  R27,HIGH(_panId)
	RCALL __EEPROMWRB
; 0008 003F     panId[1] = data[13];
	__POINTW1MN _panId,1
	MOVW R0,R30
	RCALL SUBOPT_0x61
	ADIW R26,13
	RCALL SUBOPT_0x37
	RCALL __EEPROMWRB
; 0008 0040     channel = data[11];
	RCALL SUBOPT_0x61
	ADIW R26,11
	LD   R30,X
	LDI  R26,LOW(_channel)
	LDI  R27,HIGH(_channel)
	RCALL __EEPROMWRB
; 0008 0041 
; 0008 0042     if(netAddr[0]==0xff && netAddr[1]==0xfc)
	LDS  R26,_netAddr
	CPI  R26,LOW(0xFF)
	BRNE _0x10000C
	__GETB2MN _netAddr,1
	CPI  R26,LOW(0xFC)
	BREQ _0x10000D
_0x10000C:
	RJMP _0x10000B
_0x10000D:
; 0008 0043         zigbee_state = ZIGBEE_IDLE;
	RCALL SUBOPT_0x5F
; 0008 0044     else
	RJMP _0x10000E
_0x10000B:
; 0008 0045         zigbee_state = ZIGBEE_CONNECTED;
	RCALL SUBOPT_0x63
; 0008 0046 
; 0008 0047     zigbee_retry_count=0;
_0x10000E:
	LDI  R30,LOW(0)
	STS  _zigbee_retry_count,R30
	STS  _zigbee_retry_count+1,R30
; 0008 0048 }
_0x20A0005:
	LDD  R17,Y+0
_0x20A0004:
	ADIW R28,3
	RET
;
;void sendReqZigbeeReset(){
; 0008 004A void sendReqZigbeeReset(){
_sendReqZigbeeReset:
; 0008 004B     char buf[10];
; 0008 004C     unsigned char len=0;
; 0008 004D     buf[len++] = 0x01;
	RCALL SUBOPT_0x5B
;	buf -> Y+1
;	len -> R17
	RCALL SUBOPT_0x5C
	LDI  R30,LOW(1)
	RCALL SUBOPT_0x5D
; 0008 004E     buf[len++] = 0x01;
	LDI  R30,LOW(1)
	RCALL SUBOPT_0x5E
; 0008 004F     sendControlDataToUsart0(buf, len);
	RCALL _sendControlDataToUsart0
; 0008 0050     zigbee_state = ZIGBEE_IDLE;
	RCALL SUBOPT_0x5F
; 0008 0051 }
_0x20A0002:
	LDD  R17,Y+0
_0x20A0003:
	ADIW R28,11
	RET
;
;void sendControlDataToUsart0(char *buf, unsigned char len){
; 0008 0053 void sendControlDataToUsart0(char *buf, unsigned char len){
_sendControlDataToUsart0:
; 0008 0054     int i=0;
; 0008 0055     char data[DEFAULT_BUF_SIZE];
; 0008 0056     unsigned int length=0;
; 0008 0057     unsigned char checkSum=0;
; 0008 0058 
; 0008 0059     data[length++] = 0xFA;
	RCALL SUBOPT_0x21
;	*buf -> Y+57
;	len -> Y+56
;	i -> R16,R17
;	data -> Y+6
;	length -> R18,R19
;	checkSum -> R21
	LDI  R21,0
	RCALL SUBOPT_0x51
	RCALL SUBOPT_0x28
	LDI  R30,LOW(250)
	ST   X,R30
; 0008 005A     len+=1;
	LDD  R30,Y+56
	SUBI R30,-LOW(1)
	STD  Y+56,R30
; 0008 005B     length+= getTranslatedCode(&(data[length]), &len, 1);
	RCALL SUBOPT_0x64
	MOVW R30,R28
	ADIW R30,58
	RCALL SUBOPT_0x20
	RCALL SUBOPT_0x3F
	RCALL SUBOPT_0x40
; 0008 005C     data[length++] = ZIGBEE_CONTROL_PACKET;
	RCALL SUBOPT_0x51
	RCALL SUBOPT_0x28
	RCALL SUBOPT_0x53
; 0008 005D 
; 0008 005E     length+= getTranslatedCode(&(data[length]), buf, len-1);
	RCALL SUBOPT_0x25
	RCALL SUBOPT_0x2
	RCALL SUBOPT_0x20
	LDD  R30,Y+59
	LDD  R31,Y+59+1
	RCALL SUBOPT_0x20
	LDD  R30,Y+60
	SUBI R30,LOW(1)
	RCALL SUBOPT_0x47
; 0008 005F 
; 0008 0060     for(i=0; i < len-1; i++){
	RCALL SUBOPT_0x48
_0x100010:
	LDD  R30,Y+56
	SUBI R30,LOW(1)
	RCALL SUBOPT_0x49
	BRGE _0x100011
; 0008 0061         checkSum+= buf[i];
	MOVW R30,R16
	LDD  R26,Y+57
	LDD  R27,Y+57+1
	RCALL SUBOPT_0x36
	RCALL SUBOPT_0x26
; 0008 0062     }
	RCALL SUBOPT_0x27
	RJMP _0x100010
_0x100011:
; 0008 0063 
; 0008 0064     length+= getTranslatedCode(&(data[length]), &checkSum, 1);
	RCALL SUBOPT_0x64
	IN   R30,SPL
	IN   R31,SPH
	RCALL SUBOPT_0x20
	PUSH R21
	RCALL SUBOPT_0x3F
	POP  R21
	RCALL SUBOPT_0x40
; 0008 0065     data[length++] = 0xAF;
	RCALL SUBOPT_0x51
	RCALL SUBOPT_0x28
	LDI  R30,LOW(175)
	ST   X,R30
; 0008 0066 
; 0008 0067     putstr0(data, length);
	RCALL SUBOPT_0x22
	ST   -Y,R19
	ST   -Y,R18
	RCALL _putstr0
; 0008 0068 }
	RCALL __LOADLOCR6
	ADIW R28,59
	RET
;int zigbee_getState(){
; 0008 0069 int zigbee_getState(){
_zigbee_getState:
; 0008 006A     return zigbee_state;
	LDS  R30,_zigbee_state
	LDS  R31,_zigbee_state+1
	RET
; 0008 006B }
;
;unsigned char getTranslatedCode(char *resultBuf, char* buf, unsigned char len){
; 0008 006D unsigned char getTranslatedCode(char *resultBuf, char* buf, unsigned char len){
_getTranslatedCode:
; 0008 006E     unsigned char resultLen=0;
; 0008 006F     unsigned char i=0;
; 0008 0070     for(i=0; i < len; i++){
	RCALL __SAVELOCR2
;	*resultBuf -> Y+5
;	*buf -> Y+3
;	len -> Y+2
;	resultLen -> R17
;	i -> R16
	RCALL SUBOPT_0x11
	LDI  R16,LOW(0)
_0x100013:
	LDD  R30,Y+2
	CP   R16,R30
	BRSH _0x100014
; 0008 0071         if(buf[i]==0xFA) {
	RCALL SUBOPT_0x65
	CPI  R26,LOW(0xFA)
	BRNE _0x100015
; 0008 0072             resultBuf[resultLen++] = 0xFF;
	RCALL SUBOPT_0x60
	RCALL SUBOPT_0x66
	RCALL SUBOPT_0x67
; 0008 0073             resultBuf[resultLen++] = 0x01;
	RCALL SUBOPT_0x66
	LDI  R30,LOW(1)
	RJMP _0x100025
; 0008 0074         } else if(buf[i] == 0xAF){
_0x100015:
	RCALL SUBOPT_0x65
	CPI  R26,LOW(0xAF)
	BRNE _0x100017
; 0008 0075             resultBuf[resultLen++] = 0xFF;
	RCALL SUBOPT_0x60
	RCALL SUBOPT_0x66
	RCALL SUBOPT_0x67
; 0008 0076             resultBuf[resultLen++] = 0x02;
	RCALL SUBOPT_0x66
	LDI  R30,LOW(2)
	RJMP _0x100025
; 0008 0077         } else if(buf[i] == 0xFF){
_0x100017:
	RCALL SUBOPT_0x65
	CPI  R26,LOW(0xFF)
	BRNE _0x100019
; 0008 0078             resultBuf[resultLen++] = 0xFF;
	RCALL SUBOPT_0x60
	RCALL SUBOPT_0x66
	RCALL SUBOPT_0x67
; 0008 0079             resultBuf[resultLen++] = 0xFF;
	RCALL SUBOPT_0x66
	LDI  R30,LOW(255)
	RJMP _0x100025
; 0008 007A         } else
_0x100019:
; 0008 007B             resultBuf[resultLen++] = buf[i];
	RCALL SUBOPT_0x60
	RCALL SUBOPT_0x68
	RCALL SUBOPT_0x69
	RCALL SUBOPT_0x6A
_0x100025:
	ST   X,R30
; 0008 007C     }
	SUBI R16,-1
	RJMP _0x100013
_0x100014:
; 0008 007D     return resultLen;
	RJMP _0x20A0001
; 0008 007E }
;
;unsigned char getOriginalCode(char *resultBuf, char* buf, unsigned char len) {
; 0008 0080 unsigned char getOriginalCode(char *resultBuf, char* buf, unsigned char len) {
_getOriginalCode:
; 0008 0081     unsigned char resultLen=0;
; 0008 0082     unsigned char i=0;
; 0008 0083     for(i=0; i < len; i++){
	RCALL __SAVELOCR2
;	*resultBuf -> Y+5
;	*buf -> Y+3
;	len -> Y+2
;	resultLen -> R17
;	i -> R16
	RCALL SUBOPT_0x11
	LDI  R16,LOW(0)
_0x10001C:
	LDD  R30,Y+2
	CP   R16,R30
	BRSH _0x10001D
; 0008 0084         if(buf[i]==0xFF) {
	RCALL SUBOPT_0x65
	CPI  R26,LOW(0xFF)
	BRNE _0x10001E
; 0008 0085             if(buf[i+1] == 0xFF)
	RCALL SUBOPT_0x6B
	LD   R26,X
	CPI  R26,LOW(0xFF)
	BRNE _0x10001F
; 0008 0086                 resultBuf[resultLen++] = 0xFF;
	RCALL SUBOPT_0x60
	RCALL SUBOPT_0x66
	LDI  R30,LOW(255)
	RJMP _0x100026
; 0008 0087             else if(buf[i+1] == 0x01)
_0x10001F:
	RCALL SUBOPT_0x6B
	LD   R26,X
	CPI  R26,LOW(0x1)
	BRNE _0x100021
; 0008 0088                 resultBuf[resultLen++] = 0xFA;
	RCALL SUBOPT_0x60
	RCALL SUBOPT_0x66
	LDI  R30,LOW(250)
	RJMP _0x100026
; 0008 0089             else if(buf[i+1] == 0x02)
_0x100021:
	RCALL SUBOPT_0x6B
	LD   R26,X
	CPI  R26,LOW(0x2)
	BRNE _0x100023
; 0008 008A                 resultBuf[resultLen++] = 0xAF;
	RCALL SUBOPT_0x60
	RCALL SUBOPT_0x66
	LDI  R30,LOW(175)
_0x100026:
	ST   X,R30
; 0008 008B             i++;
_0x100023:
	SUBI R16,-1
; 0008 008C         }else
	RJMP _0x100024
_0x10001E:
; 0008 008D             resultBuf[resultLen++] = buf[i];
	RCALL SUBOPT_0x60
	RCALL SUBOPT_0x68
	RCALL SUBOPT_0x69
	RCALL SUBOPT_0x6A
	ST   X,R30
; 0008 008E     }
_0x100024:
	SUBI R16,-1
	RJMP _0x10001C
_0x10001D:
; 0008 008F     return resultLen;
_0x20A0001:
	MOV  R30,R17
	RCALL __LOADLOCR2
	ADIW R28,7
	RET
; 0008 0090 }
	#ifndef __SLEEP_DEFINED__
	#define __SLEEP_DEFINED__
	.EQU __se_bit=0x01
	.EQU __sm_mask=0x0E
	.EQU __sm_adc_noise_red=0x02
	.EQU __sm_powerdown=0x04
	.EQU __sm_powersave=0x06
	.EQU __sm_standby=0x0C
	.EQU __sm_ext_standby=0x0E
	.SET power_ctrl_reg=smcr
	#endif

	.CSEG

	.CSEG

	.CSEG

	.CSEG

	.CSEG

	.DSEG

	.CSEG

	.DSEG
_g_adc_data_history:
	.BYTE 0x50

	.ESEG
_ieeeAddr:
	.BYTE 0x8

	.DSEG
_netAddr:
	.BYTE 0x2

	.ESEG
_panId:
	.BYTE 0x2
_channel:
	.BYTE 0x1
_Sensing_Threshold_Avr_Diff:
	.DW  0x19

	.DSEG
_g_timer0_count_NormalMode:
	.BYTE 0x4
_g_debug_data_history:
	.BYTE 0x14
_RegistrationCount:
	.BYTE 0x1
_avrHistory:
	.BYTE 0x14
_pb3OutCnt:
	.BYTE 0x1
_timer0_cnt_Registration:
	.BYTE 0x2
_timer0_cnt_Ping:
	.BYTE 0x2
_timer0_cnt_NetAddr:
	.BYTE 0x2
_timer0_cnt_PanSetting:
	.BYTE 0x2
_prevSensorState:
	.BYTE 0x1
_sensorState:
	.BYTE 0x1
_g_NumOfEventSentCount:
	.BYTE 0x2
_g_EventSendingInterval:
	.BYTE 0x2
_g_EventSentTimer:
	.BYTE 0x2
_g_timer1CountForEventSent:
	.BYTE 0x2
_g_MsgBuf:
	.BYTE 0x32
_g_MsgBuf_Count:
	.BYTE 0x2
_data:
	.BYTE 0x1
_tempBuf:
	.BYTE 0x40
_seqNumGenerator:
	.BYTE 0x2
_data_S0000011000:
	.BYTE 0x32
_adc_cnt:
	.BYTE 0x2
_adc_index:
	.BYTE 0x2
_adc_pre_data:
	.BYTE 0x14
_timer0_count:
	.BYTE 0x2
_rx_buffer1:
	.BYTE 0x80
_rx_wr_index1:
	.BYTE 0x1
_rx_rd_index1:
	.BYTE 0x1
_rx_counter1:
	.BYTE 0x1
_zigbee_state:
	.BYTE 0x2
_zigbee_retry_count:
	.BYTE 0x2
__seed_G104:
	.BYTE 0x4

	.CSEG
;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x0:
	__GETWRN 18,19,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:61 WORDS
SUBOPT_0x1:
	MOV  R30,R17
	LDI  R31,0
	MOVW R26,R28
	ADIW R26,4
	LSL  R30
	ROL  R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 19 TIMES, CODE SIZE REDUCTION:16 WORDS
SUBOPT_0x2:
	ADD  R30,R26
	ADC  R31,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 12 TIMES, CODE SIZE REDUCTION:42 WORDS
SUBOPT_0x3:
	LDI  R31,0
	LSL  R30
	ROL  R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 18 TIMES, CODE SIZE REDUCTION:32 WORDS
SUBOPT_0x4:
	ADD  R26,R30
	ADC  R27,R31
	RCALL __GETW1P
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x5:
	MOVW R26,R0
	ST   X+,R30
	ST   X,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:33 WORDS
SUBOPT_0x6:
	__GETD2SX 88
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x7:
	CLR  R22
	CLR  R23
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x8:
	RCALL __ADDD12
	__PUTD1SX 88
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x9:
	RCALL SUBOPT_0x6
	__GETD1N 0x28
	RCALL __DIVD21U
	__PUTD1SX 88
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:28 WORDS
SUBOPT_0xA:
	MOVW R26,R30
	__GETD1SX 88
	CLR  R24
	CLR  R25
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0xB:
	__GETD2SX 84
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0xC:
	__PUTD1SX 84
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0xD:
	RCALL SUBOPT_0x2
	__GETW2SX 88
	STD  Z+0,R26
	STD  Z+1,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:12 WORDS
SUBOPT_0xE:
	LDI  R26,LOW(_avrHistory)
	LDI  R27,HIGH(_avrHistory)
	RJMP SUBOPT_0x3

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0xF:
	RCALL SUBOPT_0x2
	MOVW R0,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x10:
	LDI  R26,LOW(_g_debug_data_history)
	LDI  R27,HIGH(_g_debug_data_history)
	RJMP SUBOPT_0x3

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x11:
	LDI  R17,0
	LDI  R16,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x12:
	MOV  R30,R16
	RJMP SUBOPT_0xE

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x13:
	SUB  R30,R2
	SBC  R31,R3
	MOVW R0,R30
	LDI  R26,LOW(_Sensing_Threshold_Avr_Diff)
	LDI  R27,HIGH(_Sensing_Threshold_Avr_Diff)
	RCALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x14:
	MOV  R30,R16
	SUBI R30,-LOW(10)
	MOV  R6,R30
	LDI  R17,LOW(1)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x15:
	MOVW R26,R30
	MOVW R30,R2
	SUB  R30,R26
	SBC  R31,R27
	MOVW R0,R30
	LDI  R26,LOW(_Sensing_Threshold_Avr_Diff)
	LDI  R27,HIGH(_Sensing_Threshold_Avr_Diff)
	RCALL __EEPROMRDW
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x16:
	LDS  R30,_g_EventSendingInterval
	LDS  R31,_g_EventSendingInterval+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x17:
	LDS  R26,_g_EventSentTimer
	LDS  R27,_g_EventSentTimer+1
	CP   R30,R26
	CPC  R31,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:18 WORDS
SUBOPT_0x18:
	RCALL _handleSendEvent
	LDI  R26,LOW(_g_NumOfEventSentCount)
	LDI  R27,HIGH(_g_NumOfEventSentCount)
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	SBIW R30,1
	LDI  R30,LOW(0)
	STS  _g_EventSentTimer,R30
	STS  _g_EventSentTimer+1,R30
	LDS  R26,_g_EventSendingInterval
	LDS  R27,_g_EventSendingInterval+1
	CPI  R26,LOW(0x65)
	LDI  R30,HIGH(0x65)
	CPC  R27,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x19:
	RCALL SUBOPT_0x16
	ADIW R30,10
	STS  _g_EventSendingInterval,R30
	STS  _g_EventSendingInterval+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 13 TIMES, CODE SIZE REDUCTION:46 WORDS
SUBOPT_0x1A:
	LD   R30,X+
	LD   R31,X+
	ADIW R30,1
	ST   -X,R31
	ST   -X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x1B:
	RCALL _getRotaryValue
	ANDI R30,LOW(0xF)
	SUBI R30,-LOW(11)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x1C:
	LDI  R30,LOW(0)
	STS  _timer0_cnt_PanSetting,R30
	STS  _timer0_cnt_PanSetting+1,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x1D:
	LDI  R30,LOW(0)
	STS  _g_MsgBuf_Count,R30
	STS  _g_MsgBuf_Count+1,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x1E:
	LDS  R26,_data
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x1F:
	SBIW R30,1
	SUBI R30,LOW(-_g_MsgBuf)
	SBCI R31,HIGH(-_g_MsgBuf)
	RCALL SUBOPT_0x1E
	STD  Z+0,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 65 TIMES, CODE SIZE REDUCTION:62 WORDS
SUBOPT_0x20:
	ST   -Y,R31
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x21:
	SBIW R28,50
	RCALL __SAVELOCR6
	__GETWRN 16,17,0
	RJMP SUBOPT_0x0

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x22:
	MOVW R30,R28
	ADIW R30,6
	RJMP SUBOPT_0x20

;OPTIMIZER ADDED SUBROUTINE, CALLED 101 TIMES, CODE SIZE REDUCTION:198 WORDS
SUBOPT_0x23:
	LDI  R31,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 14 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x24:
	__ADDWRR 18,19,30,31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x25:
	MOVW R26,R28
	ADIW R26,6
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x26:
	LD   R30,X
	ADD  R21,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x27:
	__ADDWRN 16,17,1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x28:
	RCALL SUBOPT_0x25
	ADD  R26,R30
	ADC  R27,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x29:
	LDD  R30,Y+19
	RCALL SUBOPT_0x23
	OR   R30,R26
	OR   R31,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:15 WORDS
SUBOPT_0x2A:
	ST   Y,R30
	STD  Y+1,R31
	__POINTW2MN _ieeeAddr,6
	RCALL __EEPROMRDB
	MOV  R31,R30
	LDI  R30,0
	MOVW R0,R30
	__POINTW2MN _ieeeAddr,7
	RCALL __EEPROMRDB
	RCALL SUBOPT_0x23
	OR   R30,R0
	OR   R31,R1
	LD   R26,Y
	LDD  R27,Y+1
	CP   R30,R26
	CPC  R31,R27
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:8 WORDS
SUBOPT_0x2B:
	LDD  R30,Y+2
	LDD  R31,Y+2+1
	RJMP SUBOPT_0x20

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:28 WORDS
SUBOPT_0x2C:
	ST   -Y,R17
	LDI  R17,0
	MOV  R30,R17
	SUBI R17,-1
	RCALL SUBOPT_0x23
	SUBI R30,LOW(-_tempBuf)
	SBCI R31,HIGH(-_tempBuf)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 24 TIMES, CODE SIZE REDUCTION:113 WORDS
SUBOPT_0x2D:
	STD  Z+0,R26
	MOV  R30,R17
	SUBI R17,-1
	RCALL SUBOPT_0x23
	SUBI R30,LOW(-_tempBuf)
	SBCI R31,HIGH(-_tempBuf)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x2E:
	STD  Z+0,R26
	LDI  R30,LOW(_tempBuf)
	LDI  R31,HIGH(_tempBuf)
	RCALL SUBOPT_0x20
	ST   -Y,R17
	RJMP _sendNCPPacket

;OPTIMIZER ADDED SUBROUTINE, CALLED 11 TIMES, CODE SIZE REDUCTION:8 WORDS
SUBOPT_0x2F:
	LDI  R26,LOW(0)
	RJMP SUBOPT_0x2D

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:62 WORDS
SUBOPT_0x30:
	MOVW R0,R30
	__POINTW2MN _ieeeAddr,6
	RCALL __EEPROMRDB
	MOVW R26,R0
	ST   X,R30
	MOV  R30,R17
	SUBI R17,-1
	RCALL SUBOPT_0x23
	SUBI R30,LOW(-_tempBuf)
	SBCI R31,HIGH(-_tempBuf)
	MOVW R0,R30
	__POINTW2MN _ieeeAddr,7
	RCALL __EEPROMRDB
	MOVW R26,R0
	ST   X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 11 TIMES, CODE SIZE REDUCTION:38 WORDS
SUBOPT_0x31:
	MOV  R30,R17
	SUBI R17,-1
	RCALL SUBOPT_0x23
	SUBI R30,LOW(-_tempBuf)
	SBCI R31,HIGH(-_tempBuf)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x32:
	MOVW R0,R30
	LDI  R26,LOW(_Sensing_Threshold_Avr_Diff)
	LDI  R27,HIGH(_Sensing_Threshold_Avr_Diff)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x33:
	RCALL __EEPROMRDB
	MOVW R26,R0
	ST   X,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x34:
	RCALL SUBOPT_0x20
	LDI  R30,LOW(_tempBuf)
	LDI  R31,HIGH(_tempBuf)
	RCALL SUBOPT_0x20
	ST   -Y,R17
	RJMP _sendGCPPacket

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x35:
	MOV  R30,R31
	LDI  R31,0
	MOVW R26,R0
	ST   X,R30
	RJMP SUBOPT_0x31

;OPTIMIZER ADDED SUBROUTINE, CALLED 37 TIMES, CODE SIZE REDUCTION:34 WORDS
SUBOPT_0x36:
	ADD  R26,R30
	ADC  R27,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x37:
	LD   R30,X
	MOVW R26,R0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x38:
	LDI  R26,LOW(0)
	STD  Z+0,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x39:
	RCALL SUBOPT_0x20
	ST   -Y,R17
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x3A:
	RCALL __SAVELOCR6
	__GETWRN 16,17,0
	RJMP SUBOPT_0x0

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x3B:
	SUBI R30,-LOW(13)
	MOV  R20,R30
	MOVW R30,R18
	__ADDWRN 18,19,1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 16 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x3C:
	SUBI R30,LOW(-_data_S0000011000)
	SBCI R31,HIGH(-_data_S0000011000)
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x3D:
	STD  Z+0,R26
	MOVW R30,R18
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x3E:
	RCALL SUBOPT_0x3C
	RJMP SUBOPT_0x20

;OPTIMIZER ADDED SUBROUTINE, CALLED 6 TIMES, CODE SIZE REDUCTION:8 WORDS
SUBOPT_0x3F:
	LDI  R30,LOW(1)
	ST   -Y,R30
	RJMP _getTranslatedCode

;OPTIMIZER ADDED SUBROUTINE, CALLED 13 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x40:
	RCALL SUBOPT_0x23
	RJMP SUBOPT_0x24

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:19 WORDS
SUBOPT_0x41:
	MOVW R30,R18
	__ADDWRN 18,19,1
	RJMP SUBOPT_0x3C

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x42:
	LDI  R30,LOW(_netAddr)
	LDI  R31,HIGH(_netAddr)
	RCALL SUBOPT_0x20
	LDI  R30,LOW(2)
	ST   -Y,R30
	RCALL _getTranslatedCode
	RJMP SUBOPT_0x40

;OPTIMIZER ADDED SUBROUTINE, CALLED 14 TIMES, CODE SIZE REDUCTION:11 WORDS
SUBOPT_0x43:
	__ADDWRN 18,19,1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x44:
	MOVW R30,R28
	ADIW R30,8
	RCALL SUBOPT_0x20
	LDI  R30,LOW(2)
	ST   -Y,R30
	RCALL _getTranslatedCode
	RJMP SUBOPT_0x40

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x45:
	__POINTW2MN _ieeeAddr,6
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x46:
	__POINTW2MN _ieeeAddr,7
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x47:
	ST   -Y,R30
	RCALL _getTranslatedCode
	RJMP SUBOPT_0x40

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x48:
	__GETWRN 16,17,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x49:
	MOVW R26,R16
	RCALL SUBOPT_0x23
	CP   R26,R30
	CPC  R27,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x4A:
	LDS  R26,_netAddr
	SUBI R26,-LOW(16)
	__GETB1MN _netAddr,1
	ADD  R30,R26
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x4B:
	LDD  R26,Y+6
	ADD  R30,R26
	LDD  R26,Y+7
	ADD  R30,R26
	MOV  R0,R30
	RJMP SUBOPT_0x45

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x4C:
	RCALL __EEPROMRDB
	ADD  R30,R0
	ADD  R21,R30
	MOVW R30,R18
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x4D:
	RCALL SUBOPT_0x20
	ST   -Y,R19
	ST   -Y,R18
	RCALL _putstr0
	RCALL __LOADLOCR6
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 16 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x4E:
	MOVW R26,R28
	ADIW R26,8
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x4F:
	ST   X,R30
	MOVW R30,R18
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:6 WORDS
SUBOPT_0x50:
	RCALL SUBOPT_0x4E
	RCALL SUBOPT_0x2
	RJMP SUBOPT_0x20

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x51:
	MOVW R30,R18
	RJMP SUBOPT_0x43

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x52:
	RCALL SUBOPT_0x4E
	RJMP SUBOPT_0x36

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x53:
	LDI  R30,LOW(0)
	RJMP SUBOPT_0x4F

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x54:
	LDI  R30,LOW(200)
	LDI  R31,HIGH(200)
	RCALL SUBOPT_0x20
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x55:
	RCALL SUBOPT_0x20
	RJMP _delay_ms

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x56:
	MOVW R30,R16
	LDI  R26,LOW(_adc_pre_data)
	LDI  R27,HIGH(_adc_pre_data)
	LSL  R30
	ROL  R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x57:
	__GETD2S 2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x58:
	__PUTD1S 2
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 7 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x59:
	ST   -Y,R17
	LDI  R17,0
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x5A:
	ST   -Y,R0
	ST   -Y,R1
	ST   -Y,R15
	ST   -Y,R22
	ST   -Y,R23
	ST   -Y,R24
	ST   -Y,R25
	ST   -Y,R26
	ST   -Y,R27
	ST   -Y,R30
	ST   -Y,R31
	IN   R30,SREG
	ST   -Y,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x5B:
	SBIW R28,10
	RJMP SUBOPT_0x59

;OPTIMIZER ADDED SUBROUTINE, CALLED 10 TIMES, CODE SIZE REDUCTION:43 WORDS
SUBOPT_0x5C:
	MOV  R30,R17
	SUBI R17,-1
	RCALL SUBOPT_0x23
	MOVW R26,R28
	ADIW R26,1
	RJMP SUBOPT_0x36

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x5D:
	ST   X,R30
	RJMP SUBOPT_0x5C

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x5E:
	ST   X,R30
	MOVW R30,R28
	ADIW R30,1
	RJMP SUBOPT_0x39

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x5F:
	LDI  R30,LOW(0)
	STS  _zigbee_state,R30
	STS  _zigbee_state+1,R30
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 12 TIMES, CODE SIZE REDUCTION:9 WORDS
SUBOPT_0x60:
	MOV  R30,R17
	SUBI R17,-1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 8 TIMES, CODE SIZE REDUCTION:5 WORDS
SUBOPT_0x61:
	LDD  R26,Y+1
	LDD  R27,Y+1+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 13 TIMES, CODE SIZE REDUCTION:10 WORDS
SUBOPT_0x62:
	RCALL SUBOPT_0x23
	RJMP SUBOPT_0x36

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:3 WORDS
SUBOPT_0x63:
	LDI  R30,LOW(1)
	LDI  R31,HIGH(1)
	STS  _zigbee_state,R30
	STS  _zigbee_state+1,R31
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x64:
	MOVW R30,R18
	RCALL SUBOPT_0x25
	RCALL SUBOPT_0x2
	RJMP SUBOPT_0x20

;OPTIMIZER ADDED SUBROUTINE, CALLED 4 TIMES, CODE SIZE REDUCTION:13 WORDS
SUBOPT_0x65:
	LDD  R26,Y+3
	LDD  R27,Y+3+1
	CLR  R30
	ADD  R26,R16
	ADC  R27,R30
	LD   R26,X
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 9 TIMES, CODE SIZE REDUCTION:14 WORDS
SUBOPT_0x66:
	LDD  R26,Y+5
	LDD  R27,Y+5+1
	RJMP SUBOPT_0x62

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x67:
	LDI  R30,LOW(255)
	ST   X,R30
	RJMP SUBOPT_0x60

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x68:
	LDD  R26,Y+5
	LDD  R27,Y+5+1
	RCALL SUBOPT_0x23
	RJMP SUBOPT_0xF

;OPTIMIZER ADDED SUBROUTINE, CALLED 5 TIMES, CODE SIZE REDUCTION:2 WORDS
SUBOPT_0x69:
	LDD  R26,Y+3
	LDD  R27,Y+3+1
	RET

;OPTIMIZER ADDED SUBROUTINE, CALLED 2 TIMES, CODE SIZE REDUCTION:1 WORDS
SUBOPT_0x6A:
	CLR  R30
	ADD  R26,R16
	ADC  R27,R30
	RJMP SUBOPT_0x37

;OPTIMIZER ADDED SUBROUTINE, CALLED 3 TIMES, CODE SIZE REDUCTION:4 WORDS
SUBOPT_0x6B:
	MOV  R30,R16
	SUBI R30,-LOW(1)
	RCALL SUBOPT_0x69
	RJMP SUBOPT_0x62


	.CSEG
_delay_ms:
	ld   r30,y+
	ld   r31,y+
	adiw r30,0
	breq __delay_ms1
__delay_ms0:
	__DELAY_USW 0xFA0
	wdr
	sbiw r30,1
	brne __delay_ms0
__delay_ms1:
	ret

__REPACK:
	LDI  R21,0x80
	EOR  R21,R23
	BRNE __REPACK0
	PUSH R21
	RJMP __ZERORES
__REPACK0:
	CPI  R21,0xFF
	BREQ __REPACK1
	LSL  R22
	LSL  R0
	ROR  R21
	ROR  R22
	MOV  R23,R21
	RET
__REPACK1:
	PUSH R21
	TST  R0
	BRMI __REPACK2
	RJMP __MAXRES
__REPACK2:
	RJMP __MINRES

__CDF1U:
	SET
	RJMP __CDF1U0
__CDF1:
	CLT
__CDF1U0:
	SBIW R30,0
	SBCI R22,0
	SBCI R23,0
	BREQ __CDF10
	CLR  R0
	BRTS __CDF11
	TST  R23
	BRPL __CDF11
	COM  R0
	RCALL __ANEGD1
__CDF11:
	MOV  R1,R23
	LDI  R23,30
	TST  R1
__CDF12:
	BRMI __CDF13
	DEC  R23
	LSL  R30
	ROL  R31
	ROL  R22
	ROL  R1
	RJMP __CDF12
__CDF13:
	MOV  R30,R31
	MOV  R31,R22
	MOV  R22,R1
	PUSH R21
	RCALL __REPACK
	POP  R21
__CDF10:
	RET

__ZERORES:
	CLR  R30
	CLR  R31
	CLR  R22
	CLR  R23
	POP  R21
	RET

__MINRES:
	SER  R30
	SER  R31
	LDI  R22,0x7F
	SER  R23
	POP  R21
	RET

__MAXRES:
	SER  R30
	SER  R31
	LDI  R22,0x7F
	LDI  R23,0x7F
	POP  R21
	RET

__CMPF12:
	TST  R25
	BRMI __CMPF120
	TST  R23
	BRMI __CMPF121
	CP   R25,R23
	BRLO __CMPF122
	BRNE __CMPF121
	CP   R26,R30
	CPC  R27,R31
	CPC  R24,R22
	BRLO __CMPF122
	BREQ __CMPF123
__CMPF121:
	CLZ
	CLC
	RET
__CMPF122:
	CLZ
	SEC
	RET
__CMPF123:
	SEZ
	CLC
	RET
__CMPF120:
	TST  R23
	BRPL __CMPF122
	CP   R25,R23
	BRLO __CMPF121
	BRNE __CMPF122
	CP   R30,R26
	CPC  R31,R27
	CPC  R22,R24
	BRLO __CMPF122
	BREQ __CMPF123
	RJMP __CMPF121

__ADDD12:
	ADD  R30,R26
	ADC  R31,R27
	ADC  R22,R24
	ADC  R23,R25
	RET

__SUBD12:
	SUB  R30,R26
	SBC  R31,R27
	SBC  R22,R24
	SBC  R23,R25
	RET

__ANEGD1:
	COM  R31
	COM  R22
	COM  R23
	NEG  R30
	SBCI R31,-1
	SBCI R22,-1
	SBCI R23,-1
	RET

__LSLD1:
	LSL  R30
	ROL  R31
	ROL  R22
	ROL  R23
	RET

__CBD1:
	MOV  R31,R30
	ADD  R31,R31
	SBC  R31,R31
	MOV  R22,R31
	MOV  R23,R31
	RET

__DIVD21U:
	PUSH R19
	PUSH R20
	PUSH R21
	CLR  R0
	CLR  R1
	CLR  R20
	CLR  R21
	LDI  R19,32
__DIVD21U1:
	LSL  R26
	ROL  R27
	ROL  R24
	ROL  R25
	ROL  R0
	ROL  R1
	ROL  R20
	ROL  R21
	SUB  R0,R30
	SBC  R1,R31
	SBC  R20,R22
	SBC  R21,R23
	BRCC __DIVD21U2
	ADD  R0,R30
	ADC  R1,R31
	ADC  R20,R22
	ADC  R21,R23
	RJMP __DIVD21U3
__DIVD21U2:
	SBR  R26,1
__DIVD21U3:
	DEC  R19
	BRNE __DIVD21U1
	MOVW R30,R26
	MOVW R22,R24
	MOVW R26,R0
	MOVW R24,R20
	POP  R21
	POP  R20
	POP  R19
	RET

__GETW1P:
	LD   R30,X+
	LD   R31,X
	SBIW R26,1
	RET

__GETD1P_INC:
	LD   R30,X+
	LD   R31,X+
	LD   R22,X+
	LD   R23,X+
	RET

__PUTDP1_DEC:
	ST   -X,R23
	ST   -X,R22
	ST   -X,R31
	ST   -X,R30
	RET

__SWAPD12:
	MOV  R1,R24
	MOV  R24,R22
	MOV  R22,R1
	MOV  R1,R25
	MOV  R25,R23
	MOV  R23,R1

__SWAPW12:
	MOV  R1,R27
	MOV  R27,R31
	MOV  R31,R1

__SWAPB12:
	MOV  R1,R26
	MOV  R26,R30
	MOV  R30,R1
	RET

__EEPROMRDW:
	ADIW R26,1
	RCALL __EEPROMRDB
	MOV  R31,R30
	SBIW R26,1

__EEPROMRDB:
	SBIC EECR,EEWE
	RJMP __EEPROMRDB
	PUSH R31
	IN   R31,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R30,EEDR
	OUT  SREG,R31
	POP  R31
	RET

__EEPROMWRW:
	RCALL __EEPROMWRB
	ADIW R26,1
	PUSH R30
	MOV  R30,R31
	RCALL __EEPROMWRB
	POP  R30
	SBIW R26,1
	RET

__EEPROMWRB:
	SBIS EECR,EEWE
	RJMP __EEPROMWRB1
	WDR
	RJMP __EEPROMWRB
__EEPROMWRB1:
	IN   R25,SREG
	CLI
	OUT  EEARL,R26
	OUT  EEARH,R27
	SBI  EECR,EERE
	IN   R24,EEDR
	CP   R30,R24
	BREQ __EEPROMWRB0
	OUT  EEDR,R30
	SBI  EECR,EEMWE
	SBI  EECR,EEWE
__EEPROMWRB0:
	OUT  SREG,R25
	RET

__CPD12:
	CP   R30,R26
	CPC  R31,R27
	CPC  R22,R24
	CPC  R23,R25
	RET

__SAVELOCR6:
	ST   -Y,R21
__SAVELOCR5:
	ST   -Y,R20
__SAVELOCR4:
	ST   -Y,R19
__SAVELOCR3:
	ST   -Y,R18
__SAVELOCR2:
	ST   -Y,R17
	ST   -Y,R16
	RET

__LOADLOCR6:
	LDD  R21,Y+5
__LOADLOCR5:
	LDD  R20,Y+4
__LOADLOCR4:
	LDD  R19,Y+3
__LOADLOCR3:
	LDD  R18,Y+2
__LOADLOCR2:
	LDD  R17,Y+1
	LD   R16,Y
	RET

__LOADLOCR2P:
	LD   R16,Y+
	LD   R17,Y+
	RET

__INITLOCB:
__INITLOCW:
	ADD  R26,R28
	ADC  R27,R29
__INITLOC0:
	LPM  R0,Z+
	ST   X+,R0
	DEC  R24
	BRNE __INITLOC0
	RET

;END OF CODE MARKER
__END_OF_CODE:
