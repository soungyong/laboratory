#include "rotary.h"
#include <avr/io.h>

//H_8: PD5
//H_4: PD4
//H_2: PD3
//H_1: PC1

//L_8: PC5
//L_4: PC4
//L_2: PC3
//L_1: PC2

void initRotary(){
	DDRD &= ~0x38;
    DDRC &= ~0x3E;

	PORTD |= 0x38;
	PORTC |= 0x3E;
}

uint8 rotary_GetValue() {
    uint8 id=0;
    
    if((PINC & 0x20)==0) id |= (0x01 << 3);
    if((PINC & 0x10)==0) id |= (0x01 << 2);
    if((PINC & 0x08)==0) id |= (0x01 << 1);
    if((PINC & 0x04)==0) id |= (0x01 << 0);

    if((PIND & 0x20)==0) id |= (0x01 << 7);
    if((PIND & 0x10)==0) id |= (0x01 << 6);
    if((PIND & 0x08)==0) id |= (0x01 << 5);
    if((PINC & 0x02)==0) id |= (0x01 << 4);

    return id;
}
