#include <avr/io.h>
#include <avr/interrupt.h>
#include "rotary.h"
#include "timer.h"
#include "uart.h"
#include "xcps.h"
#include "adc.h"
#include "zrmprotocol.h"
#include "NC_Protocol.h"
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include "MSN_Protocol.h"

#define true 1
#define false 0
#define NodeType	2

volatile uint16 Sensing_Standard_Avr = 320;
volatile uint8 g_isSensored = 0;
volatile uint8 g_isSensored_Avr = 0;
volatile uint8 g_isSensored_Avr2 = 0;
volatile uint8 g_debug_data_size = 0;

uint8 g_isSensoredForTestLamp=0;

#define NumOfRecentHistory 3
#define NumOfAvrHistory 10
unsigned int avrHistory[NumOfAvrHistory];

#define NumOFDebugHistory 10
volatile uint16 g_debug_data_history[NumOFDebugHistory];
volatile uint16 g_adc_data;

uint8 ee_Sensing_Threshold_Avr_Diff EEMEM;
volatile uint8 g_prevSensingDiff;

//-----------------------------------------------------------------------------
//MCU Initialize
void InitMCU() {
	DDRD |= 0xC0;
	PORTD |= 0xc0;

	DDRB |= 0x03;
	PORTB |= 0x03;

	DDRB |= 0x04;
	PORTB |= 0x04;

	DDRB |= 0x08;
	PORTB &= ~0x08;

	DDRD |= 0x02;
	DDRD |= (0x01);

	PORTD = 0x01;
}

void WDT_INIT() {
	MCUSR &= ~(1 << WDRF); // WatchDog Init(Low)
	wdt_enable(WDTO_2S);
	// WatchDog Reset Time(High)
}

void resetZigbee() {
	PORTB &= ~(0x01);
	MSLEEP(100);
	PORTB |= 0x01;
}
//----------------------------------------------------------------------//

uint8 ZigbeePacket[64];
void ZigbeeUsartTask() {
	static int Recvlen = 0;
	static uint8 zigbeeHWResetCount = 0;

	if (timer_isfired(ON_ZIGBEE_PING_TIMER_ID)) {
		ZRMsendPing();
		zigbeeHWResetCount++;
		if (getZigbeeState() == ZRM_CONNECT) {
			timer_clear(ON_ZIGBEE_PING_TIMER_ID);
			timer_set(ON_ZIGBEE_PING_TIMER_ID, 10000);
		} else {
			timer_clear(ON_ZIGBEE_PING_TIMER_ID);
			timer_set(ON_ZIGBEE_PING_TIMER_ID, 1000);
		}
	}

	if (getZigbeeState() > 1) {
		zigbeeHWResetCount = 0;
	} else if (zigbeeHWResetCount > 5) {
		//resetZigbee();
		zigbeeHWResetCount = 0;
	}

	if ((Recvlen = xcps_recv_zigbee(ZigbeePacket, 64)) > 0) {
		uint8 buff_len = 0;
		uint16 Src_Addr;
		uint16 Dst_Addr;
		uint16 destId = 0;

		switch (ZigbeePacket[0]) {
		case 0x00: // Send to GMProtocols (Gateway <-> RFM)
			buff_len = (Recvlen - 1);
			ZRMPMessage(&ZigbeePacket[1], buff_len);
			break;
		case 0x10:
			Dst_Addr = (uint16) (ZigbeePacket[1] << 8) | (ZigbeePacket[2]);
			Src_Addr = (uint16) (ZigbeePacket[3] << 8) | (ZigbeePacket[4]);

			destId = (uint16) (ZigbeePacket[9] << 8) | (ZigbeePacket[10]);
			buff_len = (Recvlen - 5);
			if (destId == tmp_zrmp.zrm_Id)
				XNetHandlerFromZigbee(Src_Addr, &ZigbeePacket[5], buff_len - 5);
			break;

		default: // Error Mesaage
			break;
		}
	}
}

void NCP_Task() {
	if (timer_isfired(NCP_TIMER_ID)) {
		if (ncp_ConnState() == NCP_NET_NOT_CONNECT) {
			if (getZigbeeState() == ZRM_CONNECT)
				ncp_SendRegisterReq();

			timer_clear(NCP_TIMER_ID);
			timer_set(NCP_TIMER_ID, 10000);
		} else {
			if (getZigbeeState() == ZRM_CONNECT)
				ncp_SendPingReq();

			timer_clear(NCP_TIMER_ID);
			timer_set(NCP_TIMER_ID, 20000);
		}
	}
}

void getAdc_10ms() {
	unsigned char i;

	for (i = 0; i < NumOfAvrHistory - 1; i++)
		avrHistory[i] = avrHistory[i + 1];

	avrHistory[NumOfAvrHistory - 1] = g_adc_data;

	if (g_debug_data_size < (NumOFDebugHistory - 1))
		g_debug_data_history[g_debug_data_size++] = g_adc_data;

}

char isSensored() {
	unsigned char isSensored = 0;
	unsigned char i = 0;
	unsigned char startIndex = 0;
	unsigned char sensorCount = 0;
	g_isSensored_Avr = 0;
	g_isSensored_Avr2 = 0;

	startIndex = NumOfAvrHistory - NumOfRecentHistory;

	for (i = startIndex; i < NumOfAvrHistory; i++)
		if (avrHistory[i] < Sensing_Standard_Avr)
			break;
		else if (avrHistory[i] - Sensing_Standard_Avr < MSN_GetSensorDiff())
			break;

	if (i == NumOfAvrHistory) {
		g_isSensored_Avr = i + 10;
		isSensored = true;
	}

	for (i = startIndex; i < NumOfAvrHistory; i++)
		if (avrHistory[i] > Sensing_Standard_Avr)
			break;
		else if (Sensing_Standard_Avr - avrHistory[i] < MSN_GetSensorDiff())
			break;

	if (i == NumOfAvrHistory) {
		g_isSensored_Avr = i + 10;
		isSensored = true;
	}

	for (i = 0; i < NumOfAvrHistory; i++) {
		if (avrHistory[i] > Sensing_Standard_Avr) {
			if (avrHistory[i] - Sensing_Standard_Avr > MSN_GetSensorDiff())
				sensorCount++;
		} else {
			if (Sensing_Standard_Avr - avrHistory[i] > MSN_GetSensorDiff())
				sensorCount++;
		}
	}
	if (sensorCount > NumOfRecentHistory * 1.5) {
		g_isSensored_Avr2 = sensorCount + 10;
		isSensored = true;
	} else {
		g_isSensored_Avr2 = sensorCount;
		isSensored |= false;
	}

	g_isSensored = isSensored;

	return g_isSensored;
}
unsigned char prevSensorState = false;
unsigned char sensorState = false;
uint16 sendTimer = 0;
uint16 sendInterval = 0;
void sensorTask() {
	uint8 diff = MSN_GetSensorDiff();
	if (g_prevSensingDiff != MSN_GetSensorDiff()) {
		g_prevSensingDiff = MSN_GetSensorDiff();
		eeprom_write_block(&(diff), &ee_Sensing_Threshold_Avr_Diff,
				sizeof(uint8));
	}

	if (timer_isfired(SENSOR_10ms_TIMER_ID)) {
		getAdc_10ms();
		//update g_isSensored ��.
		prevSensorState = sensorState;
		sensorState = isSensored();

		if (sensorState){
			PORTB |= (0x02);
			g_isSensoredForTestLamp=1;
		}
		else{
			PORTB &= ~0x02;
		}

		if (getZigbeeState() == ZRM_CONNECT) {
			if (prevSensorState == false) {
				if (sensorState == true) {
					if (sendTimer > sendInterval) {
						GCP_SendNoticeEvent();
						sendTimer = 0;
						sendInterval += 20;
					}
				}
			} else {
				if (sendTimer > sendInterval) {
					GCP_SendNoticeEvent();
					sendTimer = 0;
					sendInterval += 20;
				}
			}
		}
		if (sendTimer > 300)
			sendInterval = 0;
		else
			sendTimer++;
		timer_clear(SENSOR_10ms_TIMER_ID);
		timer_set(SENSOR_10ms_TIMER_ID, 10);
	}
}

uint16 g_timer0_count_NormalMode = 0;
void debugModeTask() {
	if (timer_isfired(SENSOR_DEBUG_MODE_TIMER_ID)) {
		if (!MSN_IsNormalMode()) {
			if (getZigbeeState() == ZRM_CONNECT) {
				GCP_SendNoticeEvent_Debug(g_debug_data_size,
						g_debug_data_history, Sensing_Standard_Avr,
						MSN_GetSensorDiff(), g_isSensored_Avr,
						g_isSensored_Avr2);
				g_debug_data_size = 0;
			}

			g_timer0_count_NormalMode++;
			if (g_timer0_count_NormalMode > 3000) {
				MSN_SetNormalMode(1);
				g_timer0_count_NormalMode = 0;
			}
		}
		timer_clear(SENSOR_DEBUG_MODE_TIMER_ID);
		timer_set(SENSOR_DEBUG_MODE_TIMER_ID, 100);
	}
}

void WTD_Task() {

	if (timer_isfired(ON_WTD_TIMER_ID)) {
		wdt_reset();
		timer_clear(ON_WTD_TIMER_ID);
		timer_set(ON_WTD_TIMER_ID, 100);
	}
}

void ADC_Task() {
	if (timer_isfired(ADC_TIMER_ID)) {
		ADCSRA |= 0x40;
		while ((ADCSRA & 0x10) == 0)
			;

		g_adc_data = ADCW;
		ADCSRA |= 0x10;

		timer_clear(ADC_TIMER_ID);
		timer_set(ADC_TIMER_ID, 10);
	}

}

void TestLamp_Task() {
	static uint8 cnt=0;
	if (timer_isfired(TEST_LAMP_ID)) {
		if(g_isSensoredForTestLamp==1){
			PORTB |= 0x08;
			g_isSensoredForTestLamp=0;
			cnt=0;
		}
		else{
			cnt++;
			if(cnt > 10){
				PORTB &= ~0x08;
				cnt=20;
			}
		}
		timer_clear(TEST_LAMP_ID);
		timer_set(TEST_LAMP_ID, 100);
	}
}
//-----------------------------------------------------------------------------
// Main function
//-----------------------------------------------------------------------------
int main(void) {
	uint8 i = 0;
	uint8 diff;
	InitMCU();
	initRotary();
	InitUART();
	timer_init();
	initADC();

	eeprom_read_block(&(diff), &ee_Sensing_Threshold_Avr_Diff, sizeof(uint8));
	if(diff==0xff || diff==0)
		diff=80;

	g_prevSensingDiff = diff;
	MSN_SetSensorDiff(diff);

	for (i = 0; i < 5; i++) {
		PORTB ^= 0x02;
		MSLEEP(500);
		PORTB ^= 0x02;
		MSLEEP(500);
	}

//MSLEEP(1000);
	WDT_INIT();
	xcps_init_zigbee(USART_Receive, USART_Transmit);

	timer_set(ON_ZIGBEE_PING_TIMER_ID, 1000);
	timer_set(NCP_TIMER_ID, 1000);
	timer_set(ON_WTD_TIMER_ID, 100);
	timer_set(SENSOR_DEBUG_MODE_TIMER_ID, 100);
	timer_set(SENSOR_10ms_TIMER_ID, 10);
	timer_set(ADC_TIMER_ID, 5);
	timer_set(TEST_LAMP_ID, 1000);

	asm("sei");

	while (1) {
		WTD_Task();
		ZigbeeUsartTask();
		NCP_Task();
		sensorTask();
		debugModeTask();
		ADC_Task();
		TestLamp_Task();
	}

	return 0;
}

