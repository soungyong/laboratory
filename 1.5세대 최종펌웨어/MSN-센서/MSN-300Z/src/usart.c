#include "usart.h"
char rx_buffer1[RX_BUFFER_SIZE1];

#if RX_BUFFER_SIZE1<256
unsigned char rx_wr_index1,rx_rd_index1,rx_counter1;
#else
unsigned int rx_wr_index1,rx_rd_index1,rx_counter1;
#endif

// This flag is set on USART1 Receiver buffer overflow
bit rx_buffer_overflow1;                                 

void initUsart(){   
    // USART1 initialization
    // Communication Parameters: 8 Data, 1 Stop, No Parity
    // USART1 Receiver: On
    // USART1 Transmitter: On
    // USART1 Mode: Asynchronous
    // USART1 Baud rate: 115200
    UCSR0A=0x00;
    UCSR0B=0x98;
    UCSR0C=0x06;
    UBRR0H=0x00;
    UBRR0L=0x08;
    #asm("sei")
}  

// USART1 Receiver interrupt service routine
interrupt [USART_RXC] void usart_rx_isr(void)
{
    char status,data;
    status=UCSR0A;
    data=UDR0;       
    //UDR = data;
    //putchar0(data);
    if ((status & (FRAMING_ERROR | PARITY_ERROR | DATA_OVERRUN))==0)
    {
        rx_buffer1[rx_wr_index1]=data;
        if (++rx_wr_index1 == RX_BUFFER_SIZE1) rx_wr_index1=0;
        if (++rx_counter1 == RX_BUFFER_SIZE1)
        {
            rx_counter1=0;
            rx_buffer_overflow1=1;
        };
   };
}

// Get a character from the USART Receiver buffer
#pragma used+
char getchar0(void)
{
    char data;
    while (rx_counter1==0);
    data=rx_buffer1[rx_rd_index1];
    if (++rx_rd_index1 == RX_BUFFER_SIZE1) rx_rd_index1=0;
    #asm("cli")
    --rx_counter1;
    #asm("sei")
    return data;
}
#pragma used-
// Write a character to the USART Transmitter
#pragma used+
void putchar0(char c)
{
    while ((UCSR0A & DATA_REGISTER_EMPTY)==0);
    UDR0=c;
}
#pragma used-

void putstr0(char *c, int len){    
    int i=0;
    for(i=0; i < len; i++)
        putchar0(c[i]);
}