#ifndef _COMMON__H   
#define _COMMON__H
#include <mega88a.h>    
#include <delay.h>     

#define DEVICE_VERSION 0x10
#define FW_VERSION 0x13      
#define DEVICE_TYPE 0x2050  

#define DEFAULT_BUF_SIZE 36

#define false 0
#define true 1


#define RADAR_SENSOR_PROTOCOL        0x30
#define SEND_REGISTER_REQ   0x11
#define SEND_REGISTER_RES   0x12

#define GET_DEVICE_INFO_REQ 0x31
#define GET_DEVICE_INFO_RES 0x32

#define GET_STATE_REQ       0x03
#define GET_STATE_RES       0x04
#define SEND_ON_TIME_REQ    0x21
#define SEND_ON_TIME_RES    0x22
#define SEND_EVENT          0x50    
#define SEND_EVENT_DEBUG    0x51

#define SET_MODE_REQ        0x60
#define SET_MODE_RES        0x61
#define SET_SENSING_LEVEL_REQ   0x70
#define SET_SENSING_LEVEL_RES   0x71   

#define SET_LEVEL           0x80
#define SET_LEVEL_RES       0x81

#endif