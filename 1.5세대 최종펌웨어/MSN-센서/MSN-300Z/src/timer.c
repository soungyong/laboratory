#include "timer.h"
int timer0_count=0;    
int timer1_count=0;

void initTimer0(){
    // Timer/Counter 0 initialization
    // Clock source: System Clock
    // Clock value: 15.625 kHz
    // Mode: Normal top=FFh
    // OC0 output: Disconnected
    ASSR=0x00;
    TCCR0B=0x05;
    TCNT0=0x00;
    // Timer(s)/Counter(s) Interrupt(s) initialization
    TIMSK0|=0x01;
}   

void initTimer1() {
    TCCR1A=0x00;
    TCCR1B=0x03;
    TCNT1H=0xFF;
    TCNT1L=0x00;
    ICR1H=0x00;
    ICR1L=0x00;
    OCR1AH=0x00;
    OCR1AL=0x00;
    OCR1BH=0x00;    
    OCR1BL=0x00;
    TIMSK1|=0x01;
}

 // Timer 0 overflow interrupt service routine
interrupt [TIM0_OVF] void timer0_ovf_isr(void)
{   
    // Place your code here 
    if(timer0_count > 5){
        timer0_Fired();    
        timer0_count=0;
    }                  
    timer0_count++;
}

// Timer 1 overflow interrupt service routine
interrupt [TIM1_OVF] void timer1_ovf_isr(void)
{
    TCNT1H=0xF7;      
    TCNT1L=0xD0;
    timer1_Fired();
}

