#include "adc.h"
#include "gpio.h"

void initADC(){

    // ADC initialization
    // ADC Clock frequency: 250.000 kHz
    // ADC Voltage Reference: AREF pin

    ADMUX = 0x00; //select adc input 0
    ACSR = 0x80;    
    ADCSRA=0xCF;
    DIDR0=0x3E;
    DIDR0&=~(0x3c);
}

unsigned int adc_cnt=0;
unsigned int adc_index=0; 
unsigned int adc_pre_data[10];
// ADC interrupt service routine
interrupt [ADC_INT] void adc_isr(void)
{                                           
    unsigned long avr=0;
    unsigned int i=0;          
    
    for(i=0; i < 10; i++){
        avr+=adc_pre_data[i];
        if(i!=9)adc_pre_data[i] = adc_pre_data[i+1];
    }                                               
    adc_pre_data[9] = ADCW;                        
    
    avr/=10;
    g_adc_data_history[adc_index++] =  avr;

    if(adc_index >= NumOFHistory)
        adc_index=0;
        

    adc_cnt++;
    
    // Start the AD conversion
    ADCSRA|=0x40;
}