#ifndef __NCP_PROTOCOL_H__
#define __NCP_PROTOCOL_H__

#include <stdio.h>

#define NCP_PROTOCOL_ID			(0x80)	// Protocol ID
#define NCP_VERSION         			(0x10)  	// Protocol Version

#define NCP_REQ_PING				(0x01)	// Ping
#define NCP_RES_PING				(0x02)	// Ping 응답

#define NCP_REQ_REGISTER			(0x11)	// 등록 요청
#define NCP_RES_REGISTER			(0x12)	// 등록 요청 응답

#define NCP_REQ_NODE_INFO			(0x17)	// 노드 정보 요청
#define NCP_RES_NODE_INFO			(0x18)	// 노드 정보 요청 응답

#endif

