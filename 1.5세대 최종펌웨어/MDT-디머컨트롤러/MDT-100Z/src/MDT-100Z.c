#include "MDT-100Z.h"
#include "Rotary.h"
#include "NCProtocol.h"


// Avrx hw Peripheral initialization
#define CPUCLK 		16000000L     		// CPU xtal
#define TICKRATE 	1000				// AvrX timer queue Tick rate
#define TCNT0_INIT 	(CPUCLK/128/TICKRATE)


// NB: Clock divisor bits are different between classic and mega103 chips!
// NB: IAR chooses to use different CPU identifiers for their C compiler
// NB: GCC changes the IO bit names just for fun...

#define TCCR0_INIT	((1<<CS02) | (0<<CS01) | (1<<CS00))



uint8 SensorPacket[64];

uint8 targetDimmingLevel=0xff;
uint8 currentDimmingLevel=0xff;

//-----------------------------------------------------------------------------
//MCU Initialize
void InitMCU() 
{
	PORTA = 0x00;	DDRA = 0xFF;
	PORTC = 0x0F;	DDRC = 0xFF;
	PORTD = 0x9F;	DDRD = 0xFF;
	PORTF = 0xFF;	DDRF = 0xFF;	
	PORTG = 0xFF;	DDRG = 0xFF;
}

//----------------------------------------------------------------------//

void resetZigbee(){
    PORTD&= ~(0x80);
    MSLEEP(100);  
    PORTD|= 0x80;  
}

AVRX_MUTEX(TimerSemaphore);
	
AVRX_SIGINT(SIG_OVERFLOW0)
{
	IntProlog();												// Switch to kernel stack/context
	TCNT0 = TCNT0_INIT;

	while(AvrXIntTestSemaphore(&TimerSemaphore) == SEM_WAIT)
        	AvrXIntSetSemaphore(&TimerSemaphore);				// Flush all waiting tasks

	AvrXIntSetSemaphore(&TimerSemaphore);					// Set Semaphore (short path)

    	Epilog();													// Return to tasks
}


/***** Task Led(Toggle) *****/
 AVRX_GCC_TASKDEF(WTD_Task, 256, 1)
{	
	uint16 Toggle_Count = 1;
	uint32 Register_Count = 1;	
	uint8 i=0;
	uint8 mode=0;

	timer_set(ON_WTD_TIMER_ID, 500);
	timer_set(ON_TEST_TIMER_ID, 3000);	
	
	while(1)
	{
		AvrXWaitSemaphore(&TimerSemaphore);
				
		if(timer_isfired(ON_WTD_TIMER_ID))
		{		
			wdt_reset();				
			if(PIND&0x10) PORTD &= ~(0x10);				
			else PORTD |= 0x10;				

			timer_clear(ON_WTD_TIMER_ID);
			timer_set(ON_WTD_TIMER_ID, 500);
		}

		if(timer_isfired(ON_TEST_TIMER_ID))
		{		
			if(rotary_GetValue() == 0x00){
				if(mode==0){
					mode++;
					DMX512.Data[0] = 0x00;
					DMX512Send();
				}else if(mode==1){
					mode++;
					DMX512.Data[0] = 0x50;				
					DMX512Send();
				}else if(mode==2){				
					mode++;
					DMX512.Data[0] = 0xA0;				
					DMX512Send();
				}else if(mode==3){
					mode=0;
					DMX512.Data[0] = 0xFE;				
					DMX512Send();
				}else{
					mode=0;
				}			
			}
			timer_clear(ON_TEST_TIMER_ID);
			timer_set(ON_TEST_TIMER_ID, 3000);
		}
	}
}


AVRX_GCC_TASKDEF(Zigbee_Task, 256, 2)				// USART 1 : Senrsor <-> LoadController
{	
	int Recvlen = 0;
	uint16 count = 1;
	uint8 zigbeeHWResetCount=0;

	uint8 i=0;	
	timer_set(ON_ZIGBEE_PING_TIMER_ID, 3000);
	timer_set(ON_NCP_PING_TIMER_ID, 3000);
	timer_set(ON_NCP_REGISTER_TIMER_ID, 3000);

	tmp_zrmp.zrm_State=ZRM_INIT;
	

	xcps_init_zigbee(USART0_Receive, USART0_Transmit);		
	
	while (1)
    {	
		AvrXWaitSemaphore(&TimerSemaphore);
			
		if(timer_isfired(ON_ZIGBEE_PING_TIMER_ID)){			
			ZRMsendPing();	
			zigbeeHWResetCount++;
						
			timer_clear(ON_ZIGBEE_PING_TIMER_ID);
			timer_set(ON_ZIGBEE_PING_TIMER_ID, 3000);
		}

		if(getState()>1){
			zigbeeHWResetCount=0;
		}else if(zigbeeHWResetCount > 3){
			resetZigbee();
			zigbeeHWResetCount=0;
		}

		if(tmp_zrmp.isUpdated==1)
			timer_set(ON_NCP_PING_TIMER_ID, 0);

		if(timer_isfired(ON_NCP_PING_TIMER_ID)){		
			if(getState() == ZRM_CONNECT && ncp_ConnState()==NCP_NET_REGISTER){
				ncp_SendPingReq(tmp_zrmp.zrm_Id);
				pingCounter++;
				if(pingCounter > 5){
					ncp_NetState = 0;
					pingCounter=0;
				}
			}
						
			timer_clear(ON_NCP_PING_TIMER_ID);
			timer_set(ON_NCP_PING_TIMER_ID, 30000);
		}

		if(timer_isfired(ON_NCP_REGISTER_TIMER_ID)){		
			if(getState() == ZRM_CONNECT && ncp_ConnState()!=NCP_NET_REGISTER)
				ncp_SendRegisterReq(tmp_zrmp.zrm_Id);				
						
			timer_clear(ON_NCP_REGISTER_TIMER_ID);
			timer_set(ON_NCP_REGISTER_TIMER_ID, 30000);
		}

		if(ncp_ConnState()==NCP_NET_REBOOT){
			resetZigbee();
			ZRMSendReset();
			ncp_NetState = NCP_NET_NOT_CONNECT;
		}
		
			
		
		if((Recvlen = xcps_recv_zigbee(SensorPacket, 64)) > 0)
		{
			uint8 buff_len = 0;
			uint16 Src_Addr;
			uint16 Dst_Addr;
			uint16 nodeId;			

			
			switch(SensorPacket[0])
			{		
				case 0x00:			// Send to GMProtocols (Gateway <-> RFM)
					//memcpy(&tmp_sensorData[0], &SensorPacket[1], Recvlen - 1);
					buff_len = (Recvlen - 1);												
					ZRMPMessage(&SensorPacket[1], buff_len);
					break;
				case 0x10:	
					Dst_Addr = (uint16)(SensorPacket[1] << 8) | (SensorPacket[2]);
					Src_Addr = (uint16)(SensorPacket[3] << 8) | (SensorPacket[4]);
					
					//memcpy(&tmp_sensorData[0], , Recvlen - 5);

					buff_len = (Recvlen - 5);
					nodeId= (uint16)(SensorPacket[8] << 8 | SensorPacket[9]);

					if(nodeId == tmp_zrmp.zrm_Id) 
					{						
						if(SensorPacket[5] == 0x80)
							ncp_ProcessMessage(Src_Addr, &(SensorPacket[5]), buff_len);					
						
						if(SensorPacket[5] == 0x80 && SensorPacket[6]==0x11 && SensorPacket[7]==0x32){							
							DMX512.Data[0] = SensorPacket[12];
							DMX512Send();
							if(DMX512.Data[0] > 0xF0)
								PORTC &=~(0x01);								
							else
								PORTC |=0x01;		
						}	
						
					}									

					break;
					
				default:				// Error Mesaage
					break;
			}			
		}

   	}
}

void WDT_INIT()
{
	MCUCSR &= ~(1 << WDRF);				// WatchDog Init(Low)
	wdt_enable(WDTO_2S);					// WatchDog Reset Time(High)
}

//-----------------------------------------------------------------------------
// Main function
//-----------------------------------------------------------------------------
int main(void)
{	
	

	InitMCU();
	initRotary();	
	InitUART();	
	timer_init();

/*	while(1) {
		MSLEEP(500);
		resetZigbee();
	}
*/	
	WDT_INIT();
	
		// initialize dmx driver
	DMX512Init(512);
	DMX512.Data[0] = 0xFF;	
	//DMX512.Data[0] = 0x00;	
	

	DMX512Send();
	PORTC=0x00;
	
	AvrXSetKernelStack(0);
	
	TCCR1A=0x00;
	TCCR1B=0x06;
	TCNT1H=0xFF;
	TCNT1L=0xFF;

	TIMSK |= 0x04;	

	TCNT0 |= TCNT0_INIT;	
	TCCR0 |= TCCR0_INIT;		
	TIMSK |= (1 << TOIE0);    			// Enable Timer overflow interrupt

	ZRMSendSetPreconfig(0x15);
	MSLEEP(100);
	ZRMSendReset();
	MSLEEP(1000);					

	AvrXRunTask(TCB(WTD_Task));
	AvrXRunTask(TCB(Zigbee_Task));	

	Epilog();                   // Switch from AvrX Stack to first task

	while(1)
	{
		// TODO::
		// NOT TO DO
	}
	
	return 0;
}

	

