#ifndef __NCP_PROTOCOL_H__
#define __NCP_PROTOCOL_H__

#include <stdio.h>
#include "Util.h"
#include "ZRMProtocol.h"
#include "XNetProtocol.h"


#define NCP_PROTOCOL_ID			(0x80)	// Protocol ID
#define NCP_DEFAULT_SUBPROTOCOL_ID (0x00)
#define PLCS_NCP_PROTOCOL_ID	(0x20)  // PLCS NCProtocol
#define NCP_VERSION         			(0x20)  	// Protocol Version
#define FW_VERSION				0x13

#define NCP_REQ_PING				(0x01)	// Ping
#define NCP_RES_PING				(0x02)	// Ping 응답

#define NCP_REQ_REGISTER			(0x11)	// 등록 요청
#define NCP_RES_REGISTER			(0x12)	// 등록 요청 응답

#define NCP_REQ_REGISTER_NODE		(0x13)	// 노드 등록 요청
#define NCP_RES_REGISTER_NODE		(0x14)	// 노드 등록 요청 응답

#define NCP_SEND_TIME_INFO				(0x15)

///////////////////////////////////////////////////////////////////////////////////////////
#define NCP_NET_NOT_CONNECT				0	
#define NCP_NET_REGISTER				1
#define NCP_NET_REBOOT					2

extern uint8 pingCounter;
extern uint8 ncp_NetState;
uint8 ncp_ConnState();
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

extern uint8 Min;
extern uint8 Hour;

void ncp_ProcessMessage(uint16 srcAddr, uint8 msg[], int length);
void processNCP(uint16 zigbee_Id, uint8 msg[], int length);

void ncp_ProcessRegisterReq(uint16 zigbee_Id, uint8 msg[], int length);
void ncp_ProcessPingReq(uint16 zigbee_Id, uint8 msg[], int length);
void ncp_ProcessNodeInfoReq(uint16 zigbee_Id, uint8 msg[], int length);

void ncp_ProcessRegisterRes(uint16 zigbee_Id, uint8 msg[], int length);
void ncp_ProcessPingRes(uint16 zigbee_Id, uint8 msg[], int length);
void ncp_ProcessNodeInfoRes(uint16 zigbee_Id, uint8 msg[], int length);
void npc_ProcessSendTimeInfo(uint8 msg[], int length);

void npc_SendTimeInfoRes(uint8 nodeId, uint8 hour, uint8 min);

void ncp_SendPingReq(uint16 nodeId);
void ncp_SendRegisterReq(uint16 nodeId);

void send_NCPResPing(uint16 zigbee_addr, uint16 nodeId, uint16 device_Type, uint8 flag, uint8 timeOut);
void send_NCPResRegister(uint16 zigbee_Id, uint16 node_Id, uint16 deviceType,  uint16 result);
void send_NCPResNodeInfo(uint16 zigbee_Id, uint32 node_Id, uint16 result, uint16 device_Type, uint16 short_Addr);
void send_NCPResNodeList(uint16 zigbee_Id, uint16 device_Type, uint8 dev_Count, uint8 node_Id[], int length);
void send_NCPResCtrlCmd(uint16 zigbee_Id, uint16 ctrl_Cmd, uint16 result, uint16 reason);
void sendReset(uint16 zigbee_Id);

void registerNodetoServer(uint16 node_Id, uint16 device_Type, uint16 nwk_addr);
void registerAllNodetoServer();

#endif

