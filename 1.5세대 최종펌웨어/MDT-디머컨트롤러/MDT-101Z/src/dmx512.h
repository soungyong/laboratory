#ifndef __RIS_DMX512__
#define __RIS_DMX512__ 486

#include "conf.h"

#ifndef	KOREAN
	#define KOREAN	0
#endif

#if !( defined(__AVR_AT43USB355__) || \
	defined(__AVR_AT76C711__) || \
	defined(__AVR_AT86RF401__) || \
	defined(__AVR_AT90PWM1__) || \
	defined(__AVR_ATmega32HVB__) || \
	defined(__AVR_ATmega406__) || \
	defined(__AVR_ATmega8HVA__) || \
	defined(__AVR_ATmega16HVA__) || \
	defined(__AVR_AT90C8534__) || \
	defined(__AVR_AT90S2343__) || \
	defined(__AVR_AT90S2323__) || \
	defined(__AVR_ATtiny13__) || \
	defined(__AVR_ATtiny25__) || \
	defined(__AVR_ATtiny45__) || \
	defined(__AVR_ATtiny85__) || \
	defined(__AVR_ATtiny24__) || \
	defined(__AVR_ATtiny44__) || \
	defined(__AVR_ATtiny84__) || \
	defined(__AVR_ATtiny261__) || \
	defined(__AVR_ATtiny461__) || \
	defined(__AVR_ATtiny861__) || \
	defined(__AVR_ATtiny43U__) || \
	defined(__AVR_ATtiny48__) || \
	defined(__AVR_ATtiny88__) || \
	defined(__AVR_ATtiny28__) || \
	defined(__AVR_AT90S1200__) || \
	defined(__AVR_ATtiny15__) || \
	defined(__AVR_ATtiny12__) || \
	defined(__AVR_ATtiny11__) || \
	defined(__AVR_ATtiny11__) )

#if !defined(F_CPU)
	#if KOREAN
		#error "F_CPU가 선언되지 않았습니다"
	#else
		#error "F_CPU not defined"
	#endif
#else
	#if F_CPU != 8000000 && F_CPU != 12000000 && F_CPU != 16000000
		#if KOREAN
			#error "F_CPU는 8Mhz, 12Mhz하고 16Mhz를 사용해야합니다"
		#else
			#error "F_CPU is 8Mhz, 12Mhz and 16Mhz should be used"
		#endif
	#elif F_CPU == 12000000
		#if KOREAN
			#warning "12Mhz를 사용할 때 Break 시간이 길어집니다"
		#else
			#warning "Break time is lengthened when using 12Mhz."
		#endif
	#endif
#endif

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/version.h>

/* Ram Size Search. And Data Size Adjustment */
#if !defined(DMX512SRAMSIZE)
	#if RAMEND < 0x60
		#if KOREAN
			#error "SRAM을 사용할 수 없습니다"
		#else
			#error "SRAM is not available"
		#endif
	#elif RAMEND < 0x100
		#if KOREAN
			#warning "SRAM 크기가 작습니다. DMX512.Data의 크기를 64Byte로 합니다"
		#else
			#warning "SRAM size is small. DMX512.Data the size of 64Bytes"
		#endif
		#include "dmx512_basic.h"
		#define DMX512SRAMSIZE	64
	#elif RAMEND < 0x200
		#if KOREAN
			#warning "SRAM 크기가 작습니다. DMX512.Data의 크기를 128Byte로 합니다"
		#else
			#warning "SRAM size is small. DMX512.Data the size of 128Bytes"
		#endif
		#include "dmx512_basic.h"
		#define DMX512SRAMSIZE	128
	#elif RAMEND < 0x400
		#if KOREAN
			#warning "SRAM 크기가 작습니다. DMX512.Data의 크기를 256Byte로 합니다"
		#else
			#warning "SRAM size is small. DMX512.Data the size of 256Bytes"
		#endif
		#include "dmx512_basic.h"
		#define DMX512SRAMSIZE	256
	#else
		#include "dmx512_basic.h"
		#define DMX512SRAMSIZE	512
	#endif
#else
	#if DMX512SRAMSIZE > 512
		#if KOREAN
			#warning "DMX512SRAMSIZE의 크기가 512보다 큽니다"
		#else
			#warning "DMX512SRAMSIZE the size is greater than 512"
		#endif
	#endif
	#include "dmx512_basic.h"
#endif

#if defined(__RIS_DMX512_BASIC__)


//	added, syseo : 100402

//	end :

#define DMX512BREAKSIZE ( DMX512SRAMSIZE + 1 )

#define DMX_BAUD	250000
#define BREAK_BAUD	100000

#if __AVR_LIBC_DATE_ < 20071030UL
	#define __SETBAUD_HEADER_CHK__	0
#else
	#define __SETBAUD_HEADER_CHK__	1
#endif

static inline void _UBRR_VALUE(void) __attribute__((always_inline));
static inline void _UBRR_VALUE2(void) __attribute__((always_inline));
#if __SETBAUD_HEADER_CHK__ == 1
	#if ( !defined(UBRR) || defined(UBRRH) ) && ( ( !defined(UBRR0) || defined(UBRR1) ) || defined(UBRR0L) )	
		void _UBRR_VALUE(void)
		{
			#undef BAUD
			#define BAUD DMX_BAUD
			#include <util/setbaud.h>			

			_UBRRH = UBRRH_VALUE;
			_UBRRL = UBRRL_VALUE;
		}
		void _UBRR_VALUE2(void)
		{
			#undef BAUD
			#define BAUD BREAK_BAUD
			#include <util/setbaud.h>
			_UBRRH = UBRRH_VALUE;
			_UBRRL = UBRRL_VALUE;
		}
	#else
		void _UBRR_VALUE(void)
		{
			#undef BAUD
			#define BAUD DMX_BAUD
			#include <util/setbaud.h>
			_UBRR = UBRRL_VALUE;
		}
		void _UBRR_VALUE2(void)
		{
			#undef BAUD
			#define BAUD BREAK_BAUD
			#include <util/setbaud.h>
			_UBRR = UBRRL_VALUE;
		}
	#endif
#else
	#ifndef UBRR
		void _UBRR_VALUE(void)
		{
			_UBRRH = ( ( ( ( (F_CPU) + 8 * (DMX_BAUD) ) / ( 16 * (DMX_BAUD) ) - 1 ) >> 8 ) & 0x0FF ); \
			_UBRRL = ( ( ( ( (F_CPU) + 8 * (DMX_BAUD) ) / ( 16 * (DMX_BAUD) ) - 1 ) ) & 0x0FF ); \
		}
		void _UBRR_VALUE2(void)
		{
			_UBRRH = ( ( ( ( (F_CPU) + 8 * (BREAK_BAUD) ) / ( 16 * (BREAK_BAUD) ) - 1 ) >> 8 ) & 0x0FF ); \
			_UBRRL = ( ( ( ( (F_CPU) + 8 * (BREAK_BAUD) ) / ( 16 * (BREAK_BAUD) ) - 1 ) ) & 0x0FF ); \
		}
	#else
		void _UBRR_VALUE(void)
		{
			_UBRR = ( ( ( ( (F_CPU) + 8 * (DMX_BAUD) ) / ( 16 * (DMX_BAUD) ) - 1 ) ) & 0x0FF ); \
		}
		void _UBRR_VALUE2(void)
		{
			_UBRR = ( ( ( ( (F_CPU) + 8 * (BREAK_BAUD) ) / ( 16 * (BREAK_BAUD) ) - 1 ) ) & 0x0FF ); \
		}
	#endif
#endif

#define dmx512_busy_wait()	{ do { asm volatile(""); }while(!(_UCSRA & ( 1 << _UDRE) )); }

#define TX_BREAK()	{ \
	while(DMX512.Start)	{ asm volatile("");	} \
	_UBRR_VALUE2(); \
	dmx512_busy_wait(); \
	DMX512.Start = 1; \
	DMX512.Cnt = DMX512BREAKSIZE; \
	_UDR = 0; \
	}
#define TX_START()	{ \
	_UBRR_VALUE(); \
	dmx512_busy_wait(); \
	DMX512.Cnt = 0; \
	_UDR = 0; \
	}

static inline void DMX512Init(unsigned int bMax) __attribute__((always_inline));

volatile struct DMX512{
	volatile unsigned char Break;					// Break Flag		: Receive
	volatile unsigned char Start;					// Data Trans Flag	: Trans
	volatile unsigned int Cnt;						// Data Cnt			: Trans, Receive
	volatile unsigned int Max;						// Trans Data Limit	: Trans
	volatile unsigned char Data[DMX512SRAMSIZE];	// DMX512 Data		: Trans, Receive
}DMX512;

/*-ISR(DMX512_TXC_vect)------------------------
// Auto Trans Data
// DMX512.Start Check ( If(DMX512.Start == 1) UDR = DMX512.Data[Cnt++] )
// Trans Limit Check ( if(DMX512.Cnt >= DMX512.Max) DMX512.Start = 0 )
// MAX Trans ID Check ( if(DMX512.Cnt >= 512) DMX512.Start = 0 )
---------------------------------------------*/

//	modified, syseo : 100205    
ISR(USART1_TX_vect)
{				
	if(DMX512.Start)
	{
		if(DMX512.Cnt >= DMX512BREAKSIZE)
		{
			TX_START(); // Start Code
		}
		else if(DMX512.Cnt == DMX512SRAMSIZE || DMX512.Cnt == DMX512.Max)
		{
			DMX512.Start = 0; // Trans End
		}
		else
		{
			asm volatile("nop\n\t nop"); // 2 Cycles Delay
			_UDR = DMX512.Data[DMX512.Cnt++]; // Data Send
		}
	}
}

// end :


/*-ISR(DMX512_RXC_vect)------------------------
// Auto Receive
// Break Check ( DMX512.Break = 1 )
// Start Code Check ( DMX512.Cnt = 0, DMX512.Break = 0 )
// DMX512.Data[DMX512.Cnt++] = UDR;
---------------------------------------------*/

//	modified, syseo : 100205
ISR(USART_RXC_vect)
{
	unsigned char RxBuf;
	unsigned char RxStatus;

#if 1
	RxStatus = _UCSRB;
	RxBuf = _UDR;

	if( !( RxStatus & (1 << _RXB8) ) )	// Frame Error : Break
	{
		DMX512.Break = 1;
	}
	else if(DMX512.Break)	// No Frame Error : Start
	{
		DMX512.Break = 0;
		DMX512.Cnt = 0;
	}
	else				// No Frame Error : Data
	{
		DMX512.Data[DMX512.Cnt++] = RxBuf;
	}
#else
	RxStatus = UCSRA;
	RxBuf = UDR;

	if (( RxStatus & (1 << 4) ) )	// Frame Error : Break
	{
		DMX512.Break = 1;
	}
	else if(DMX512.Break)	// No Frame Error : Start
	{
		DMX512.Break = 0;
		DMX512.Cnt = 0;
	}
	else				// No Frame Error : Data
	{
		DMX512.Data[DMX512.Cnt++] = RxBuf;
	}
#endif

}

//	end :


/*-void-DMX512Init(unsigned char bMax)---------
// One Input, No Output, No Return
// bMax : Trans ID Limit Value
// DMX512.Data[512] <= 0 ; Trans Data
// DMX512.Start <= 0 ; Trans Comp Check Flag
// DMX512.Break <= 0 ; Recv Start Check Flag
// DMX512.Cnt <= 0 ; Trans (Recv) Id Count
// DMX512.Max <= bMax ; Trans ID Limit
//
// Always Inline Function
---------------------------------------------*/
void DMX512Init(unsigned int bMax)
{
	DMX512.Break = 0;
	DMX512.Start = 0;
	for(DMX512.Cnt = 0; DMX512.Cnt < DMX512SRAMSIZE; DMX512.Cnt++)
		DMX512.Data[DMX512.Cnt] = 0;
	DMX512.Cnt = 0;
	DMX512.Max = bMax;
	_UBRR_VALUE();

	#ifdef UBRR1
		_UCSRA=0x00;
		_UCSRB=0x48;
		//_UCSRC=0x06;
		_UBRRH=0x00;
		_UBRRL=0x03;
	#endif

	asm("sei");     // Global enable interrupts
}

/*-void-DMX512Send(void)-----------------------
// No Input, No Output, No Return
// DMX512.Start Check ( while(DMX512.Start) )
// Trans Break ( Baud Rate Change )
// Trans Start Code ( UDR = 0 )
// Trans Comp No Wait
// Trans Start Code and End Fucntion
---------------------------------------------*/
void DMX512Send(void)
{	
	TX_BREAK();		
	//UDR1 = 0;
}

#endif

#else
	#if KOREAN
		#error "DMX512가 가능한 AVR이 아닙니다"
	#else
		#error "This AVR is not possible to DMX512"
	#endif
#endif

#endif // #ifndef __RIS_DMX512__
