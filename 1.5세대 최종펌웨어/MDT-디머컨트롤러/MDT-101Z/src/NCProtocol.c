
#include "NCProtocol.h"

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
+
+
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

// --------------------------------------------------------------------------- //
uint8 node_Type[64];
// --------------------------------------------------------------------------- //


uint8 dev_Count;
uint16 ctrl_Cmd;
uint16 device_Type;
uint32 z_ieee_Id;

uint8 ncp_NetState = NCP_NET_NOT_CONNECT;
uint8 pingCounter=0;
// --------------------------------------------------------------------------- //

void ncp_ProcessMessage(uint16 srcNetAddr, uint8 msg[], int length){	

	switch (msg[8]) {						// Msg Type of NCP			
		case NCP_RES_PING:
			ncp_ProcessPingRes(srcNetAddr, msg, length);			
			break;				
		case NCP_RES_REGISTER:			
			ncp_ProcessRegisterRes(srcNetAddr, msg, length);		
			break;				
	}
}

void ncp_ProcessRegisterRes(uint16 srcNetAddr, uint8 msg[], int length){	
	if(msg[9]==0x00 && msg[10]==0x00){	
		ncp_NetState	 = NCP_NET_REGISTER;	
		ncp_SendPingReq(tmp_zrmp.zrm_Id);			
	}else {
		ncp_NetState= NCP_NET_REBOOT;
	}
}

void ncp_ProcessPingRes(uint16 srcNetAddr, uint8 msg[], int length){
	uint8 flag=0;
	uint8 pingInterval;
	
	pingCounter=0;
	flag = msg[10];
	pingInterval = msg[11];
		
	//if(pingInterval==0) pingInterval = 0xffff;
	if(flag==0x01){
		ncp_SendRegisterReq(tmp_zrmp.zrm_Id);		
	}

	//timer_set(ON_NCP_PING_TIMER_ID, (pingInterval * 1000));
}
uint16 seqNumGenerator=0;
uint16 getSeqNumGenerator(){
	return seqNumGenerator++;
}

void ncp_SendPingReq(uint16 nodeId)
{
	int len = 0;
	uint16 seqNum = getSeqNumGenerator();	

	uint8 tmp_Buff[32];	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;	
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;	
	tmp_Buff[len++] = seqNum>>8;
	tmp_Buff[len++] = seqNum;
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId >> 8;		
	tmp_Buff[len++] = nodeId;	
	tmp_Buff[len++] = NCP_REQ_PING;		
	tmp_Buff[len++] = 0;
	tmp_Buff[len++] = 0;
	
	sendData(0x00, tmp_Buff, len);
}


void ncp_SendRegisterReq(uint16 nodeId)
{
	int len = 0;
	uint16 seqNum = getSeqNumGenerator();	
	uint8 tmp_Buff[32];	

	tmp_Buff[len++] = NCP_PROTOCOL_ID;	
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;	
	tmp_Buff[len++] = seqNum>>8;
	tmp_Buff[len++] = seqNum;
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId >> 8;		
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = NCP_REQ_REGISTER;	
	tmp_Buff[len++] = 0x20;
	tmp_Buff[len++] = 0x40;
	tmp_Buff[len++] = FW_VERSION;
	tmp_Buff[len++] = FW_VERSION;

	sendData(0x00, tmp_Buff, len);
}

void plcs_gcp_SendStateInfoRes(uint16 seqNum, uint16 srcId, uint8 value)
{
	int len = 0;

	uint8 tmp_Buff[32];	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;	
	tmp_Buff[len++] = PLCS_GCP_PROTOCOL_ID;	
	tmp_Buff[len++] = seqNum>>8;
	tmp_Buff[len++] = seqNum;
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = srcId >> 8;		
	tmp_Buff[len++] = srcId;	
	tmp_Buff[len++] = PLCS_GCP_RES_STATE_INFO;		
	tmp_Buff[len++] = 0;
	tmp_Buff[len++] = 0;
	tmp_Buff[len++] = srcId >> 8;		
	tmp_Buff[len++] = srcId;	
	tmp_Buff[len++] = 0x20;
	tmp_Buff[len++] = 0x40;
	tmp_Buff[len++] = value;
	
	sendData(0x00, tmp_Buff, len);
}

uint8 ncp_ConnState()
{
	return ncp_NetState;
}

//End of File

