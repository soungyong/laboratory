#include "rotary.h"
#include <avr/io.h>
#include <avr/iom128.h>


void initRotary(){
    DDRB &= ~(0xF0);    
    DDRE &= ~(0xF0);    

	PORTB |= 0xF0;
	PORTE |= 0xF0;
}

uint8 rotary_GetValue() {
    uint8 id=0;

	id = (PINB & 0xF0) | (PINE>>4);
    
    return 0xff-id;    
}
