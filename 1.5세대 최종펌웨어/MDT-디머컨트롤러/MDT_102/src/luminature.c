#include "luminature.h"
#include <avr/io.h>

void luminature_Init(){
	DDRD |= 0x06;
	PORTD &= ~(0x06);
}
void luminature_ControlLuminatureDimming(uint8 dimmingLevel) {
	if (dimmingLevel <= 0x4D) {
		PORTD |= 0x06;
	} else if (dimmingLevel <= 0x9A) {
		PORTD |= 0x04;
		PORTD &= ~(0x02);
	} else if (dimmingLevel <= 0xCD) {
		PORTD &= ~(0x04);
		PORTD |= 0x02;
	} else {
		PORTD &= ~(0x06);
	}
}
