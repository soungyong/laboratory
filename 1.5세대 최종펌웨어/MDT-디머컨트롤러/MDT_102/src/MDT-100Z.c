#include "MDT-100Z.h"
#include "Rotary.h"
#include "NCProtocol.h"
#include "MD_Protocol.h"
#include "GC_Protocol.h"

#include "luminature.h"

// Avrx hw Peripheral initialization
#define CPUCLK 		16000000L     		// CPU xtal
#define TICKRATE 	1000				// AvrX timer queue Tick rate
#define TCNT0_INIT 	(CPUCLK/128/TICKRATE)

// NB: Clock divisor bits are different between classic and mega103 chips!
// NB: IAR chooses to use different CPU identifiers for their C compiler
// NB: GCC changes the IO bit names just for fun...

#define TCCR0_INIT	((1<<CS02) | (0<<CS01) | (1<<CS00))

uint8 SensorPacket[64];

//-----------------------------------------------------------------------------
//MCU Initialize
void InitMCU() {
	PORTA = 0x00;
	DDRA = 0xFF;
	PORTC = 0x0F;
	DDRC = 0xFF;
	PORTD = 0x9F;
	DDRD = 0xFF;
	PORTF = 0xFF;
	DDRF = 0xFF;
	PORTG = 0xFF;
	DDRG = 0xFF;
}

//----------------------------------------------------------------------//

void resetZigbee() {
	PORTD &= ~(0x80);
	MSLEEP(100);
	PORTD |= 0x80;
}

/***** Task Led(Toggle) *****/
void ledTask() {
	static uint8 mode = 0;
	if (timer_isfired(ON_WTD_TIMER_ID)) {
		wdt_reset();
		if (PIND & 0x10)
			PORTD &= ~(0x10);
		else
			PORTD |= 0x10;

		timer_clear(ON_WTD_TIMER_ID);
		timer_set(ON_WTD_TIMER_ID, 500);
	}

	if (timer_isfired(ON_TEST_TIMER_ID)) {
		if (rotary_GetValue() == 0x00) {
			if (mode == 0) {
				mode++;
				MDP_SendDimmingReqToMDP(0xff, 0x33);
				luminature_ControlLuminatureDimming(0x33);
			} else if (mode == 1) {
				mode++;
				MDP_SendDimmingReqToMDP(0xff, 0x4c);
				luminature_ControlLuminatureDimming(0x4c);
			} else if (mode == 2) {
				mode++;
				MDP_SendDimmingReqToMDP(0xff, 0x7f);
				luminature_ControlLuminatureDimming(0x7f);
			} else if (mode == 3) {
				mode++;
				MDP_SendDimmingReqToMDP(0xff, 0xcc);
				luminature_ControlLuminatureDimming(0xcc);
			} else if (mode == 4) {
				mode = 0;
				MDP_SendDimmingReqToMDP(0xff, 0xFE);
				luminature_ControlLuminatureDimming(0xFE);
			} else {
				mode = 0;
			}
			MDP_SendSetWatchdogReqToMDP(0);
		} else {
			uint8 i = 0;
			for (i = 0; i < 4; i++)
				MDP_SendDimmingReqToMDP(i, MDP_GetLastSendDimmingLevel(i));
			luminature_ControlLuminatureDimming(MDP_GetLastSendDimmingLevel(0));
			MDP_SendSetWatchdogReqToMDP(1);
		}

		timer_clear(ON_TEST_TIMER_ID);
		timer_set(ON_TEST_TIMER_ID, 5000);
	}
}

void ZigbeeUsartTask() {
	static int Recvlen = 0;
	static uint8 zigbeeHWResetCount = 0;

	if (timer_isfired(ON_ZIGBEE_PING_TIMER_ID)) {
		ZRMsendPing();
		zigbeeHWResetCount++;

		timer_clear(ON_ZIGBEE_PING_TIMER_ID);
		timer_set(ON_ZIGBEE_PING_TIMER_ID, 10000);
	}

	if (getState() > 1) {
		zigbeeHWResetCount = 0;
	} else if (zigbeeHWResetCount > 5) {
		resetZigbee();
		zigbeeHWResetCount = 0;
	}

	if ((Recvlen = xcps_recv_zigbee(SensorPacket, 64)) > 0) {
		uint8 buff_len = 0;
		uint16 Src_Addr;
		uint16 Dst_Addr;
		uint16 destId = 0;

		switch (SensorPacket[0]) {
		case 0x00: // Send to GMProtocols (Gateway <-> RFM)
			buff_len = (Recvlen - 1);
			ZRMPMessage(&SensorPacket[1], buff_len);
			break;
		case 0x10:
			Dst_Addr = (uint16) (SensorPacket[1] << 8) | (SensorPacket[2]);
			Src_Addr = (uint16) (SensorPacket[3] << 8) | (SensorPacket[4]);

			destId = (uint16) (SensorPacket[9] << 8) | (SensorPacket[10]);
			buff_len = (Recvlen - 5);
			if (destId == tmp_zrmp.zrm_Id) {
				timer_clear(NCP_PING_ID);
				timer_set(NCP_PING_ID, 2000);

				XNetHandler(Src_Addr, &SensorPacket[5], buff_len - 5);
			}
			break;

		default: // Error Mesaage
			break;
		}
	}

	if (timer_isfired(NCP_PING_ID)) {
		ncp_SendPingReq();

		timer_clear(NCP_PING_ID);
		timer_set(NCP_PING_ID, 2000);
	}
}

void NCP_Task() {
	if (timer_isfired(NCP_TIMER_ID)) {
		if (ncp_ConnState() == NCP_NET_NOT_CONNECT) {
			if (getState() == ZRM_CONNECT)
				ncp_SendRegisterReq();

			timer_clear(NCP_TIMER_ID);
			timer_set(NCP_TIMER_ID, 20000);
		} else {
			if (getState() == ZRM_CONNECT)
				ncp_SendPingReq();

			timer_clear(NCP_TIMER_ID);
			timer_set(NCP_TIMER_ID, 10000);
		}
	}
}

void WDT_INIT() {
	MCUCSR &= ~(1 << WDRF); // WatchDog Init(Low)
	wdt_enable(WDTO_2S);
	// WatchDog Reset Time(High)
}

//-----------------------------------------------------------------------------
// Main function
//-----------------------------------------------------------------------------
int main(void) {

	InitMCU();
	initRotary();
	InitUART();
	timer_init();
	luminature_Init();

	WDT_INIT();

	// initialize dmx driver
	MDP_SendDimmingReqToMDP(0xff, 0xFF);
	luminature_ControlLuminatureDimming(0xfe);

	//DMX512Send();
	PORTC = 0x00;

	TCCR1A = 0x00;
	TCCR1B = 0x06;
	TCNT1H = 0xFF;
	TCNT1L = 0xFF;

	TIMSK |= 0x04;

	ZRMSendSetPreconfig(0x15);
	MSLEEP(100);
	ZRMSendReset();
	MSLEEP(1000);

	timer_set(ON_WTD_TIMER_ID, 500);
	timer_set(ON_TEST_TIMER_ID, 3000);

	timer_set(ON_ZIGBEE_PING_TIMER_ID, 3000);
	timer_set(ON_ONTIME_TIMER_ID, 100);
	timer_set(NCP_TIMER_ID, 2000);

	tmp_zrmp.zrm_State = ZRM_INIT;

	xcps_init_zigbee(USART0_Receive, USART0_Transmit);
	xcps_init_rs485(USART1_Receive, USART1_Transmit);

	while (1) {
		ledTask();
		ZigbeeUsartTask();
		NCP_Task();
	}

	return 0;
}

