#ifndef __RIS_DMX512_BASIC__
#define __RIS_DMX512_BASIC__ 486

#ifndef	KOREAN
	#define KOREAN	0
#endif

#if !defined(UBRR0L) && !defined(UBRRL) && !defined (UBRR) && !defined(UBRR0) && !defined(UBRR1)
	#if KOREAN
		#error "DMX512가 가능한 AVR이 아닙니다"
	#else
		#error "This AVR is not possible to DMX512"
	#endif
#endif

#if !defined(UART_SEL)
	#if defined(UBRR0L) || defined(UBRRL) || defined(UBRR) || defined(UBRR0)
		#if KOREAN
			#warning "UART_SEL을 정의하지 않았습니다. UART0(UART)을 사용합니다"
		#else
			#warning "UART_SEL not defined. So UART0(UART) used"
		#endif
		#undef	UART_SEL
		#define UART_SEL 0
	#elif defined(UBRR1L) || defined(UBRR1)
		#if KOREAN
			#warning "UART_SEL을 정의하지 않았습니다. UART1을 사용합니다"
		#else
			#warning "UART_SEL not defined. So UART1 used"
		#endif
		#undef	UART_SEL
		#define UART_SEL 1
	#elif defined(UBRR2L)
		#if KOREAN
			#warning "UART_SEL을 정의하지 않았습니다. UART2을 사용합니다"
		#else
			#warning "UART_SEL not defined. So UART2 used"
		#endif
		#undef	UART_SEL
		#define UART_SEL 2
	#elif defined(UBRR3L)
		#if KOREAN
			#warning "UART_SEL을 정의하지 않았습니다. UART3을 사용합니다"
		#else
			#warning "UART_SEL not defined. So UART3 used"
		#endif
		#undef	UART_SEL
		#define UART_SEL 3
	#else
		#if KOREAN
			#error "UART를 사용할 수 없습니다"
		#else
			#error "UART is not available"
		#endif
	#endif
#elif UART_SEL > 3
	#if defined(UBRR0L) || defined(UBRRL) || defined(UBRR) || defined(UBRR0)
		#if KOREAN
			#warning "UART_SEL이 4이상입니다. UART0(UART)을 사용합니다"
		#else
			#warning "UART_SEL is the fourth or more. So UART0(UART) used"
		#endif
		#undef	UART_SEL
		#define UART_SEL 0
	#elif defined(UBRR1L) || defined(UBRR1)
		#if KOREAN
			#warning "UART_SEL이 4이상입니다. UART1을 사용합니다"
		#else
			#warning "UART_SEL is the fourth or more. So UART1 used"
		#endif
		#undef	UART_SEL
		#define UART_SEL 1
	#elif defined(UBRR2L)
		#if KOREAN
			#warning "UART_SEL이 4이상입니다. UART2을 사용합니다"
		#else
			#warning "UART_SEL is the fourth or more. So UART2 used"
		#endif
		#undef	UART_SEL
		#define UART_SEL 2
	#elif defined(UBRR3L)
		#if KOREAN
			#warning "UART_SEL이 4이상입니다. UART3을 사용합니다"
		#else
			#warning "UART_SEL is the fourth or more. So UART3 used"
		#endif
		#undef	UART_SEL
		#define UART_SEL 3
	#else
		#if KOREAN
			#error "UART를 사용할 수 없습니다"
		#else
			#error "UART is not available"
		#endif
	#endif
#elif UART_SEL == 3
	#if !defined(UBRR3L)
		#if defined(UBRR0L) || defined(UBRRL) || defined(UBRR) || defined(UBRR0)
			#if KOREAN
				#warning "UART3을 사용하지 못하므로 UART0(UART)을 사용합니다"
			#else
				#warning "UART3 not use. So UART0(UART) used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 0
		#elif defined(UBRR1L) || defined(UBRR1)
			#if KOREAN
				#warning "UART3을 사용하지 못하므로 UART1을 사용합니다"
			#else
				#warning "UART3 not use. So UART1 used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 1
		#elif defined(UBRR2L)
			#if KOREAN
				#warning "UART3을 사용하지 못하므로 UART2를 사용합니다"
			#else
				#warning "UART3 not use. So UART2 used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 2
		#else
			#if KOREAN
				#error "UART를 사용할 수 없습니다"
			#else
				#error "UART is not available"
			#endif
		#endif
	#endif
#elif UART_SEL == 2
	#if !defined(UBRR2L)
		#if defined(UBRR0L) || defined(UBRRL) || defined(UBRR) || defined(UBRR0)
			#if KOREAN
				#warning "UART2를 사용하지 못하므로 UART0(UART)을 사용합니다"
			#else
				#warning "UART2 not use. So UART0(UART) used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 0
		#elif defined(UBRR1L) || defined(UBRR1)
			#if KOREAN
				#warning "UART2를 사용하지 못하므로 UART1을 사용합니다"
			#else
				#warning "UART2 not use. So UART1 used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 1
		#elif defined(UBRR3L)
			#if KOREAN
				#warning "UART2를 사용하지 못하므로 UART3을 사용합니다"
			#else
				#warning "UART2 not use. So UART3 used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 3
		#else
			#if KOREAN
				#error "UART를 사용할 수 없습니다"
			#else
				#error "UART is not available"
			#endif
		#endif
	#endif
#elif UART_SEL == 1
	#if !defined(UBRR1L) && !defined(UBRR1)
		#if defined(UBRR0L) || defined(UBRRL) || defined(UBRR) || defined(UBRR0)
			#if KOREAN
				#warning "UART1를 사용하지 못하므로 UART0(UART)을 사용합니다"
			#else
				#warning "UART1 not use. So UART0(UART) used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 0
		#elif defined(UBRR2L)
			#if KOREAN
				#warning "UART1을 사용하지 못하므로 UART2를 사용합니다"
			#else
				#warning "UART1 not use. So UART2 used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 2
		#elif defined(UBRR3L)
			#if KOREAN
				#warning "UART1을 사용하지 못하므로 UART3을 사용합니다"
			#else
				#warning "UART1 not use. So UART3 used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 3
		#else
			#if KOREAN
				#error "UART를 사용할 수 없습니다"
			#else
				#error "UART is not available"
			#endif
		#endif
	#endif
#elif UART_SEL == 0
	#if !defined(UBRR0L) && !defined(UBRRL) && !defined(UBRR) && !defined(UBRR0)
		#if defined(UBRR1L) || defined(UBRR1)
			#if KOREAN
				#warning "UART0(UART)을 사용하지 못하므로 UART1을 사용합니다"
			#else
				#warning "UART0(UART) not use. So UART1 used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 1
		#elif defined(UBRR2L)
			#if KOREAN
				#warning "UART0(UART)을 사용하지 못하므로 UART2를 사용합니다"
			#else
				#warning "UART0(UART) not use. So UART2 used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 2
		#elif defined(UBRR3L)
			#if KOREAN
				#warning "UART0(UART)을 사용하지 못하므로 UART3을 사용합니다"
			#else
				#warning "UART0(UART) not use. So UART3 used"
			#endif
			#undef	UART_SEL
			#define UART_SEL 3
		#else
			#if KOREAN
				#error "UART를 사용할 수 없습니다"
			#else
				#error "UART is not available"
			#endif
		#endif
	#endif
#endif

/* 통신 인터럽트 설정 *******************************************************/
#if UART_SEL == 0
	#if defined(UART0_RX_vect)
		#define DMX512_RX_vect		UART0_RX_vect
	#elif defined(UART_RX_vect)
		#define DMX512_RX_vect		UART_RX_vect
	#elif defined(USART_RXC_vect)
		#define DMX512_RX_vect		USART_RXC_vect
	#elif defined(USART_RX_vect)
		#define DMX512_RX_vect		USART_RX_vect
	#elif defined(USART0_RXC_vect)
		#define DMX512_RX_vect		USART0_RXC_vect
	#elif defined(USART0_RX_vect)
		#define DMX512_RX_vect		USART0_RX_vect
	#endif
	#if defined(USART0_TXC_vect)
		#define DMX512_TX_vect		USART0_TXC_vect
	#elif defined(USART0_TX_vect)
		#define DMX512_TX_vect		USART0_TX_vect
	#elif defined(USART_TXC_vect)
		#define DMX512_TX_vect		USART_TXC_vect
	#elif defined(USART_TX_vect)
		#define DMX512_TX_vect		USART_TX_vect
	#elif defined(UART0_TX_vect)
		#define DMX512_TX_vect		UART0_TX_vect
	#elif defined(UART_TX_vect)
		#define DMX512_TX_vect		UART_TX_vect
	#endif
	#if defined(UART0_UDRE_vect)
		#define DMX512_UDRE_vect	UART0_UDRE_vect
	#elif defined(UART_UDRE_vect)
		#define DMX512_UDRE_vect	UART_UDRE_vect
	#elif defined(USART0_UDRE_vect)
		#define DMX512_UDRE_vect	USART0_UDRE_vect
	#elif defined(USART_UDRE_vect)
		#define DMX512_UDRE_vect	USART_UDRE_vect
	#endif
#elif UART_SEL == 1
	#if defined(UART1_RX_vect)
		#define DMX512_RX_vect		UART1_RX_vect
	#elif defined(USART1_RXC_vect)
		#define DMX512_RX_vect		USART1_RXC_vect
	#elif defined(USART1_RX_vect)
		#define DMX512_RX_vect		USART1_RX_vect
	#endif
	#if defined(UART1_TX_vect)
		#define DMX512_TX_vect		UART1_TX_vect
	#elif defined(USART1_TXC_vect)
		#define DMX512_TX_vect		USART1_TXC_vect
	#elif defined(USART1_TX_vect)
		#define DMX512_TX_vect		USART1_TX_vect
	#endif
	#if defined(UART1_UDRE_vect)
		#define DMX512_UDRE_vect	UART1_UDRE_vect
	#elif defined(USART1_UDRE_vect)
		#define DMX512_UDRE_vect	USART1_UDRE_vect
	#endif
#elif UART_SEL == 2
	#define DMX512_RX_vect		USART2_RX_vect
	#define DMX512_TX_vect		USART2_TX_vect
	#define DMX512_UDRE_vect	USART2_UDRE_vect
#elif UART_SEL == 3
	#define DMX512_RX_vect		USART3_RX_vect
	#define DMX512_TX_vect		USART3_TX_vect
	#define DMX512_UDRE_vect	USART3_UDRE_vect
#endif
/****************************************************************************/

/* 통신 레지스터 설정 *******************************************************/
#if defined(UBRRH) && !defined(UBRR0H) && ( !defined(UBRR0) || defined(UBRR11) )
	#define _UBRRH	UBRRH
	#if defined(UBRRL)
		#define _UBRRL	UBRRL
	#else
		#define _UBRRL	UBRR
	#endif
	#define _UCSRA	UCSRA
	#define _UCSRB	UCSRB
	#define _UDR	UDR
#elif defined(UBRR0H) && !defined(UBRRH)
	#if UART_SEL == 0
		#define _UBRRH	UBRR0H
		#define _UBRRL	UBRR0L
		#define _UCSRA	UCSR0A
		#define _UCSRB	UCSR0B
		#define _UDR	UDR0
	#elif UART_SEL == 1
		#define _UBRRH	UBRR1H
		#define _UBRRL	UBRR1L
		#define _UCSRA	UCSR1A
		#define _UCSRB	UCSR1B
		#define _UDR	UDR1
	#elif UART_SEL == 2
		#define _UBRRH	UBRR2H
		#define _UBRRL	UBRR2L
		#define _UCSRA	UCSR2A
		#define _UCSRB	UCSR2B
		#define _UDR	UDR2
	#elif UART_SEL == 3
		#define _UBRRH	UBRR3H
		#define _UBRRL	UBRR3L
		#define _UCSRA	UCSR3A
		#define _UCSRB	UCSR3B
		#define _UDR	UDR3
	#endif
#elif defined(UBRR) && !defined(UBRRH)
	#define _UBRR	UBRR
	#if defined(UCR)
		#define _UCSRB	UCR
	#else
		#define _UCSRB	UCSRB
	#endif
	#if defined(USR)
		#define _UCSRA	USR
	#else
		#define _UCSRA	UCSRA
	#endif
	#define _UDR	UDR
#elif defined(UBRR0) || defined(UBRR1)
	#if UART_SEL == 0
		#define _UBRR	UBRR0
		#define _UCSRB	UCSR0B
		#define _UCSRA	UCSR0A
		#define _UDR	UDR0
	#elif UART_SEL == 1
		#define _UBRR	UBRR1
		#if defined(UBRR1L) && defined(UBRR1H)
			#define _UBRRH	UBRR1H
			#define _UBRRL	UBRR1L
		#endif
		#define _UDR	UDR1
		#define _UCSRB	UCSR1B
		#define _UCSRA	UCSR1A
	#endif
#else
	#if KOREAN
		#error "DMX512 레지스터가 정의되지 않았습니다"
	#else
		#error "DMX512 Register not defined"
	#endif
#endif

#if defined(U2X)
	#define _U2X	U2X
#elif defined(U2X0)
	#define _U2X	U2X0
#elif defined(U2X1)
	#define _U2X	U2X1
#elif defined(U2X2)
	#define _U2X	U2X2
#elif defined(U2X3)
	#define _U2X	U2X3
#endif
#if defined(TXEN)
	#define _TXEN	TXEN
#elif defined(TXEN0)
	#define _TXEN	TXEN0
#elif defined(TXEN1)
	#define _TXEN	TXEN1
#elif defined(TXEN2)
	#define _TXEN	TXEN2
#elif defined(TXEN3)
	#define _TXEN	TXEN3
#endif
#if defined(TXCIE)
	#define _TXCIE	TXCIE
#elif defined(TXCIE0)
	#define _TXCIE	TXCIE0
#elif defined(TXCIE1)
	#define _TXCIE	TXCIE1
#elif defined(TXCIE2)
	#define _TXCIE	TXCIE2
#elif defined(TXCIE3)
	#define _TXCIE	TXCIE3
#endif
#if defined(RXCIE)
	#define _RXCIE	RXCIE
#elif defined(RXCIE0)
	#define _RXCIE	RXCIE0
#elif defined(RXCIE1)
	#define _RXCIE	RXCIE1
#elif defined(RXCIE2)
	#define _RXCIE	RXCIE2
#elif defined(RXCIE3)
	#define _RXCIE	RXCIE3
#endif
#if defined(RXEN)
	#define _RXEN	RXEN
#elif defined(RXEN0)
	#define _RXEN	RXEN0
#elif defined(RXEN1)
	#define _RXEN	RXEN1
#elif defined(RXEN2)
	#define _RXEN	RXEN2
#elif defined(RXEN3)
	#define _RXEN	RXEN3
#endif
#if defined(RXC)
	#define _RXC	RXC
#elif defined(RXC0)
	#define _RXC	RXC0
#elif defined(RXC1)
	#define _RXC	RXC1
#elif defined(RXC2)
	#define _RXC	RXC2
#elif defined(RXC3)
	#define _RXC	RXC3
#endif
#if defined(TXC)
	#define _TXC	TXC
#elif defined(TXC0)
	#define _TXC	TXC0
#elif defined(TXC1)
	#define _TXC	TXC1
#elif defined(TXC2)
	#define _TXC	TXC2
#elif defined(TXC3)
	#define _TXC	TXC3
#endif
#if defined(UDRE)
	#define _UDRE	UDRE
#elif defined(UDRE0)
	#define _UDRE	UDRE0
#elif defined(UDRE1)
	#define _UDRE	UDRE1
#elif defined(UDRE2)
	#define _UDRE	UDRE2
#elif defined(UDRE3)
	#define _UDRE	UDRE3
#endif
#if defined(RXB8)
	#define _RXB8	RXB8
#elif defined(RXB80)
	#define _RXB8	RXB80
#elif defined(RXB81)
	#define _RXB8	RXB81
#elif defined(RXB82)
	#define _RXB8	RXB82
#elif defined(RXB83)
	#define _RXB8	RXB83
#endif
#if defined(TXB8)
	#define _TXB8	TXB8
#elif defined(TXB80)
	#define _TXB8	TXB80
#elif defined(TXB81)
	#define _TXB8	TXB81
#elif defined(TXB82)
	#define _TXB8	TXB82
#elif defined(TXB83)
	#define _TXB8	TXB83
#endif
/****************************************************************************/

#endif // #ifndef __RIS_DMX512_BASIC__
