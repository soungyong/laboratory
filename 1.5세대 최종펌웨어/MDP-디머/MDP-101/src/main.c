//-----------------------------------------------------------------------------
//	reference
//
#ifndef	__AVR_ATmega8__
#error	This is only for mega8 mcu.
#endif
#if	defined(__GNUC__) // GNU Compiler
#include "avr/io.h"
#include "avr/interrupt.h"
#else
#erro	"not supported yet"
#endif

#include <stdlib.h>	// rand
#include "conf.h"		// dmx dimmer conf.
#include "types.h"
#include "util.h"
#include "timer.h"

#include "dmx512.h"

#define ENABLE		1
#define DISABLE		0	

#define ON			1
#define OFF			0

//LED Driver shutdown
#define SHUT_DOWN(x)		do{\
								if(x==DISABLE) PORTC |= 0x01;\
								else	   PORTC &= ~0x01;\
							   }while(0)
//PWM
#define PWM_OUT(x)			do{OCR1AL = x;}while(0)

//-----------------------------------------------------------------------------
//	definition
//

//-----------------------------------------------------------------------------
//	functions
//
void _WDR() {
	int i = 0;

	WDTCR = 0x18;
	WDTCR = 0x00;
	for (i = 0; i < 5; i++)
		;

	WDTCR = 0x18;
	WDTCR = 0x0F;
	for (i = 0; i < 5; i++)
		;
}

void at8_init(void);
void dmx_init(void);

void delay(long t) {
	while (t--)
		;
}

int main() {
	//
	uint8 DIPC = 0;
	uint16 dmxID = 0;
	uint16 tmpID = 0;
	uint8 jiffies = 0;
	uint8 value = 0;
	uint8 fadeOutCnt=0;

	MSLEEP(2000);
	//
	_WDR();

	//
	at8_init();

	// init system timer. (using TIM0)
	timer_init();

	asm("sei");
	// Global enable interrupts

	_WDR();

	// initialize dmx driver
	DMX512Init(512);

	// initialize dmx buffer, dmxID = 0
	DMX512.Data[dmxID] = 0xFF;

	// Shutdown is toggled for stabilization of led driver chipset
	timer_set(0, 100);

	//fadeIn, fadeOut Dimming control timer
	timer_set(1, 50);

	//ChannelId reading timer: Rotary Switch.
	timer_set(2, 1000);
	while (1) {
		SHUT_DOWN(ENABLE);
		SHUT_DOWN(DISABLE);
		if (timer_isfired(0)) {
			break;
		}
	}
	SHUT_DOWN(DISABLE);
	PWM_OUT(0xFF);

	while (1) {
		_WDR();

		if (timer_isfired(3)) {
			DIPC = (PINC & 0x1E);

			dmxID = MAX_DMX_ID;

			if (DIPC & (0x01 << 4))
				dmxID -= 8;
			if (DIPC & (0x01 << 3))
				dmxID -= 4;
			if (DIPC & (0x01 << 2))
				dmxID -= 2;
			if (DIPC & (0x01 << 1))
				dmxID -= 1;

			dmxID = 0;

			timer_set(3, 1000); // 1sec
		}

		// dimming level control : PWM
		if (OCR1AL != DMX512.Data[dmxID]) {
			if (OCR1AL == 0x00) {
				SHUT_DOWN(ENABLE);
				jiffies = 1;
			} else {
				if (jiffies == 1) {
					jiffies = 0;

					// Shutdown is toggled for stabilization of led driver chipset
					timer_set(0, 100);
					while (1) {
						SHUT_DOWN(ENABLE);
						SHUT_DOWN(DISABLE);
						if (timer_isfired(0))
							break;
					}
					SHUT_DOWN(DISABLE);
					jiffies = 0;
				}
			}

			if (timer_isfired(1)) {
				if (DMX512.Data[dmxID] > OCR1AL) {
					value = (DMX512.Data[dmxID] - OCR1AL) / 10;
					if (value < 5)
						value = 1;
					PWM_OUT(OCR1AL+value);
				} else {
					value = (OCR1AL - DMX512.Data[dmxID]) / 10;
					if (value > 5)
						value = 3;
					else
						value = 1;

					if (fadeOutCnt < 5)
						value = 0;
					else
						fadeOutCnt = 0;

					fadeOutCnt++;
					PWM_OUT(OCR1AL-value);
				}
				timer_clear(1);
				timer_set(1, 1);
			}
		}

	}

	return 0;
}

//-----------------------------------------------------------------------------
//
void at8_init(void) {
	//-------------------------------
	PORTB = 0x3F;
	DDRB = 0x00;
	PORTC = 0x00;
	DDRC = 0x00;

	//-------------------------------
	// dmx dimmer, initialization
	// 
	// led driver and pwm
	// 	: PB1(A_PWM)-O, PB2(B_PWM)-O, PC1(A_GPIO)-O, PC0(B_GPIO)-O
	// dip switch
	// 	: PD5(8Digit)-?, PD6(4Digit)-?, PD7(2Digit)-I, PB0(1Digit)-I
	// dmx512
	// 	: PD0(UART)-I,
	//
	PORTB = 0x02;
	DDRB = 0x02;
	PORTC = 0x1E;
	DDRC = 0x1E;

	//-------------------------------
	// Timer/Counter 0 initialization
	// Clock source: System Clock
	// Clock value: 15.625 kHz
	TCCR0 = 0x05;
//	TCNT0=0x69;
	TCNT0 = 0xF0;

	// Timer/Counter 1 initialization
	// Clock source: System Clock
	// Clock value: 16000.000 kHz
	// Mode: Ph. correct PWM top=01FFh
	// OC1A output: Non-Inv.
	// OC1B output: Discon.
	// Noise Canceler: Off
	// Input Capture on Falling Edge
	// Timer 1 Overflow Interrupt: Off
	// Input Capture Interrupt: Off
	// Compare A Match Interrupt: Off
	// Compare B Match Interrupt: Off
	TCCR1A = 0xA1; // 0xA2: 9 bit. top=0x01FF., 0xA1: 8 bit. top=0x00FF.
	TCCR1B = 0x03; // no prescaling, 2=1/8 prescale, 3=1/64 prescale, ...
	TCNT1H = 0x00; // initial counting value
	TCNT1L = 0x00;
	ICR1H = 0x00;
	ICR1L = 0x00; //0x00;
	OCR1AH = 0x00;
	OCR1AL = 0x00; //0x00; (A-port) dimming control value. (initial value)
	OCR1BH = 0x00;
	OCR1BL = 0x00; //0x00; (B-port) dimming control value. (initial value)

	// Timer/Counter 2 initialization
	// Clock source: System Clock
	// Clock value: Timer 2 Stopped
	// Mode: Normal top=FFh
	// OC2 output: Disconnected
	ASSR = 0x00;
	TCCR2 = 0x00;
	TCNT2 = 0x00;
	OCR2 = 0x00;

	// External Interrupt(s) initialization
	// INT0: Off
	// INT1: Off
	MCUCR = 0x00;

	// Timer(s)/Counter(s) Interrupt(s) initialization
	TIMSK = 0x01;
}

// end of main.
