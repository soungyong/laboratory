/*
#include "NCProtocol_old.h"
#include "PLCS_NCProtocol_Server.h"


// --------------------------------------------------------------------------- //
device_Info tmp_DevInfo_D;
device_Info *tmp_DevInfo_P;
// --------------------------------------------------------------------------- //

// --------------------------------------------------------------------------- //
uint8 tmp_Buff[32];
uint8 tmp_msg[32];
// --------------------------------------------------------------------------- //


// --------------------------------------------------------------------------- //
uint8 ctrl_mode;
uint8 ssr_id;
uint8 sensor_id;
uint8 time_cnt;

uint8 min;
uint8 hour;

uint8 device_Ver;
uint8 fw_Ver;
uint8 protocol_Ver;
uint8 dev_Count;
uint8 ctrl_Cmd;

uint8 Ncp_Ping_Check = 1;

uint16 device_Type;
uint32 z_ieee_Id;

// --------------------------------------------------------------------------- //


// --------------------------------------------------------------------------- //
void processNCP(uint16 Zigbee_id, uint8 msg[], int length)
{
	uint8 i;
	uint8 Dim_Value, State_Value;
	uint8 on_time;
	
	uint16 Dst_id;
	uint16 deviceType;
	
	uint32 power_merter;

	switch (msg[0])			// Msg Type of NCP
	{
		case NCP_REQ_PING:
			ncp_HandlePingReq(msg, length);
			break;			
		case NCP_REQ_REGISTER:
			ncp_HandleRegisterReq(msg, length);
			break;
		case NCP_REQ_REGISTER_NODE:
			ncp_HandleRegisterNodeReq(msg, length);
			break;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		case NCP_RES_CONTROL:
			{
				ssr_id = msg[3];		
				ctrl_mode = msg[4];
				
				printf_P(PSTR("\nPLCS_RES_CONTROL (0x%02X)"), msg[0]);
				printf_P(PSTR("\n + Node_Id : 0x%04X"), Zigbee_id);
				printf_P(PSTR("\n + SSR_Id : 0x%02X"), ssr_id);
				printf_P(PSTR("\n + Ctrl_Mode : 0x%02X"), ctrl_mode);

				if(ctrl_mode == 0x02)
				{
					on_time = msg[5];
					
					printf_P(PSTR("\n + On_Time : 0x%02X"), on_time);

					send_NCPResControl_Sensor(Zigbee_id, ssr_id, ctrl_mode, on_time);			// Based by Sensor
				}
				else	
					// Send to Server
					send_NCPResControl(Zigbee_id, ssr_id, ctrl_mode);							// Based by On / Off / Schedule
			}
			break;

		case NCP_RES_POWER_METER:
			{
				power_merter = ((uint32)msg[3] << 24) |((uint32)msg[4] << 16) | ((uint32)msg[5] << 8) | ((uint32)msg[6]);
				
				printf_P(PSTR("\nPLCS_RES_POWER_METER (0x%02X)"), msg[0]);
				printf_P(PSTR("\n + Node_Id : 0x%02X"), Zigbee_id);
				printf_P(PSTR("\n + Power_Merter : 0x%lX"), power_merter);

				send_NCPResPowerMeter(Zigbee_id, power_merter);
			}
			break;

		case NCP_RES_STATE_INFO:
			{
				uint16 nodeId = msg[1] << 8 | msg[2];
				uint8 length=0;
				deviceType = (uint16)((msg[3] << 8) | msg[4]);
				
				
				printf_P(PSTR("\nPLCS_RES_STATE_INFO (0x%02X)"), msg[0]);
				printf_P(PSTR("\n + Src_Id : 0x%04X"), Zigbee_id);
				printf_P(PSTR("\n + Device Type : 0x%04X"), deviceType);

				switch(deviceType)
				{
					case PLCS_LC_100Z_TYPE:
						length = 16;

					break;

					case PLCS_ZDIMMER_TYPE:
						Dst_id = (uint16)((msg[5] << 8) | msg[6]);
						Dim_Value = msg[7];
						
						printf_P(PSTR("\n + Dst_Id : 0x%04X"), Dst_id);
						printf_P(PSTR("\n + Dimming Level : 0x%02X"), Dim_Value);

						length=1;
					
					break;

					case PLCS_ZSENSOR_TYPE:
						Dst_id = (uint16)((msg[5] << 8) | msg[6]);
						State_Value = msg[7];
						
						printf_P(PSTR("\n + Dst_Id : 0x%04X"), Dst_id);
						printf_P(PSTR("\n + State : 0x%02X"), State_Value);

						length=1;

					break;

					default:
						break;
				}
				plcs_NCP_SendStateInfoRes(nodeId, deviceType, &msg[5], length);
			}
			break;

		case NCP_SEND_EVENT:
			{
				uint8 dataFormat;

				dataFormat = msg[3];
				
				Dst_id = (uint16)((msg[4] << 8) | msg[5]);
				
				printf_P(PSTR("\nPLCS_SEND_EVENT (0x%02X)"), msg[0]);
				printf_P(PSTR("\n + Node_Id : 0x%04X"), Zigbee_id);

				switch (dataFormat)
				{	
					case 0x01:

						for(i = 0; i < 8; i++)
						{
							printf_P(PSTR("\n + SSR_Id[%d] : 0x%02X"), i, msg[i+6]);
							tmp_msg[i] = msg[6 + i];
						}

						send_ControlEvent(Zigbee_id, dataFormat, Dst_id, tmp_msg);
						break;

					case 0x02:
						printf_P(PSTR("\n + Sensor Module Error : 0x%04X"), Dst_id);

						send_ErrorEvent(Zigbee_id, dataFormat, Dst_id);
						
						break;

					case 0x03:
						printf_P(PSTR("\n + Sensor Communication Error : 0x%04X"), Dst_id);

						send_ErrorEvent(Zigbee_id, dataFormat, Dst_id);

						break;

					case 0x04:
						printf_P(PSTR("\n + Dimmer Communication Error : 0x%04X"), Dst_id);

						send_ErrorEvent(Zigbee_id, dataFormat, Dst_id);

						break;

					default:
						break;
				}
			}
			break;

		case NCP_RES_ON_TIME:
			{
				Dst_id = (uint16)((msg[3] << 8) | msg[4]);
				on_time = msg[5];
				
				printf_P(PSTR("\nNCP_RES_ON_TIME (0x%02X)"), msg[0]);
				printf_P(PSTR("\n + Src_Id : 0x%04X"), Zigbee_id);
				printf_P(PSTR("\n + Dst_Id : 0x%04X"), Dst_id);
				printf_P(PSTR("\n + On Time : 0x%02X"), on_time);

				send_NCPResOnTime(Zigbee_id, Dst_id, on_time);
			}

			break;
			
		default:
			break;
		}
}

void ncp_HandlePingReq(uint8 msg[], int length){
	// Request NCP Ping
	uint16 zigbeeId;
	uint8 protocol_Ver;

	zigbeeId = msg[1] << 8 | msg[2];
	protocol_Ver = msg[3];

	printf_P(PSTR("\nNCP_REQ_PING (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Node_Id : 0x%04X"), zigbeeId);
	printf_P(PSTR("\n + Protocol_Ver : 0x%02X"), protocol_Ver);

	// Send Response Ping
	//if(isContainNodeOfId(msg[1] << 8 | msg[2]))
		send_NCPResPing(zigbeeId, protocol_Ver, PING_TIME);

	timer_set(NCP_PING_TIMER_ID, 1000);
}
void ncp_HandleRegisterReq(uint8 msg[], int length){
	uint16 zigbeeId;	
	uint16 deviceType;
	uint8 device_Ver;
	uint8 fw_Ver;

	min = ds1307_read(1);
	hour = ds1307_read(2);

	zigbeeId = msg[1] << 8 | msg[2];
	device_Type = (uint16)((msg[3] << 8) | msg[4]);
	device_Ver = msg[5];
	fw_Ver = msg[6];

	printf_P(PSTR("\nNCP_REQ_REGISTER (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Device_Type : 0x%04X"), deviceType);
	printf_P(PSTR("\n + Device_Ver : 0x%02X"), device_Ver);
	printf_P(PSTR("\n + F/W_Ver : 0x%02X"), fw_Ver);

	//updateDeviceTable(Zigbee_id, Zigbee_id, device_Type, device_Ver, fw_Ver);
	//updateMappingTable(msg[1] << 8 | msg[2], 0x0000, device_Type);
	send_NCPResRegister(zigbeeId, 0x0000, 0x0030);

	send_NCPReqRegisterLC(zigbeeId, deviceType, device_Ver, fw_Ver);

	send_TimeInfo(zigbeeId, hour, min);
	
	timer_set(NCP_PING_COUNT_ID, 1000);
}
void ncp_HandleRegisterNodeReq(uint8 msg[], int length){
	uint16 zigbeeId;
	uint16 deviceType;
	uint8 device_Ver;
	uint8 fw_Ver;

	zigbeeId = msg[1] << 8 | msg[2];	

	deviceType = (uint16)((msg[3] << 8) | msg[4]);
	device_Ver = msg[5];
	fw_Ver = msg[6];	
	
	printf_P(PSTR("\nNCP_REQ_REGISTER_NODE (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Src_Addr : 0x%02X"), zigbeeId);				
	printf_P(PSTR("\n + Device_Type : 0x%02X"), deviceType);
	printf_P(PSTR("\n + Device_Ver : 0x%02X"), device_Ver);
	printf_P(PSTR("\n + F/W_Ver : 0x%02X"), fw_Ver);	
}
void ncp_HandleSendTimeInfo(uint8 msg[], int length);




void send_NCPResRegister(uint16 node_Id, uint16 result, uint16 timeOut)
{
	
}

void send_NCPResRegisterNode(uint16 node_Id, uint16 deviceType, uint16 result)
{
	int len = 0;
	
	printf_P(PSTR("\nNCP_RES_REGISTERNode_RES"));
	printf_P(PSTR("\n + node_Id : 0x%04X"), node_Id);
	printf_P(PSTR("\n + deviceType : 0x%02X"), deviceType);
	printf_P(PSTR("\n + result : 0x%02X"), result);

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_RES_REGISTER_NODE;
	tmp_Buff[len++] = (uint8)(node_Id >> 8);
	tmp_Buff[len++] = (uint8)node_Id;
	tmp_Buff[len++] = (uint8)(deviceType >> 8);
	tmp_Buff[len++] = (uint8)(deviceType);
	tmp_Buff[len++] = (uint8)(result >> 8);
	tmp_Buff[len++] = (uint8)(result);
	

	xcps_send(tmp_Buff, len);
	MSLEEP(3);
}



void send_TimeInfo(uint16 Node_Id, uint8 tmp_Hour, uint8 tmp_Min)
{
	int len = 0;
	
	printf_P(PSTR("\nNCP_SEND_TIMEINFO"));
	printf_P(PSTR("\n + node_Id : 0x%04X"), Node_Id);
	printf_P(PSTR("\n + tmp_Hour : 0x%02X"), tmp_Hour);
	printf_P(PSTR("\n + tmp_Min : 0x%02X"), tmp_Min);

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_TIME_INFO;
	tmp_Buff[len++] = (uint8)(Node_Id >> 8);
	tmp_Buff[len++] = Node_Id;
	tmp_Buff[len++] = tmp_Hour;
	tmp_Buff[len++] = tmp_Min;

	xcps_send(tmp_Buff, len);
	MSLEEP(3);
}

void send_NCPResStateInfo_LC100Z(uint16 tmp_Node_Id, uint16 tmp_DeviceType, uint8 msg[])
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_RES_STATE_INFO;
	tmp_Buff[len++] = (uint8)(tmp_Node_Id >> 8);
	tmp_Buff[len++] = tmp_Node_Id;
	tmp_Buff[len++] = (uint8)(tmp_DeviceType >> 8);
	tmp_Buff[len++] = tmp_DeviceType;

	tmp_Buff[len++] = msg[0];
	tmp_Buff[len++] = msg[1];
	tmp_Buff[len++] = msg[2];
	tmp_Buff[len++] = msg[3];
	tmp_Buff[len++] = msg[4];
	tmp_Buff[len++] = msg[5];
	tmp_Buff[len++] = msg[6];
	tmp_Buff[len++] = msg[7];
	tmp_Buff[len++] = msg[8];
	tmp_Buff[len++] = msg[9];
	tmp_Buff[len++] = msg[10];
	tmp_Buff[len++] = msg[11];
	tmp_Buff[len++] = msg[12];
	tmp_Buff[len++] = msg[13];
	tmp_Buff[len++] = msg[14];
	tmp_Buff[len++] = msg[15];
	
	xcps_send(tmp_Buff, len);
	MSLEEP(3);
}



//---------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------//

void send_NCPResControl(uint8 Node_Id, uint8 tmp_Ssr_Id, uint8 tmp_Ctrl_Mode)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_RES_CONTROL;
	tmp_Buff[len++] = (uint8)(Node_Id >> 8);
	tmp_Buff[len++] = Node_Id;
	tmp_Buff[len++] = tmp_Ssr_Id;
	tmp_Buff[len++] = tmp_Ctrl_Mode;

	xcpsnet_send(tmp_Buff, len);
}


void send_NCPResControl_Sensor(uint16 Node_Id, uint8 tmp_Ssr_Id, uint8 tmp_Ctrl_Mode, uint8 tmp_On_Time)
{
	int len = 0;
	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_RES_CONTROL;
	tmp_Buff[len++] = (uint8)(Node_Id >> 8);
	tmp_Buff[len++] = Node_Id;
	tmp_Buff[len++] = tmp_Ssr_Id;
	tmp_Buff[len++] = tmp_Ctrl_Mode;
	tmp_Buff[len++] = tmp_On_Time;

	xcpsnet_send(tmp_Buff, len);
}


void send_NCPResPowerMeter(uint8 tmp_Node_Id, uint32 tmp_power_nerter)
{
	
}





void send_NCPResStateInfo_ZModule(uint16 tmp_Node_Id, uint16 tmp_DeviceType, uint16 tmp_DstId, uint8 msg)
{
	int len = 0;

//	tmp_Buff[len++] = PLCS_RES_STATE_INFO;
	tmp_Buff[len++] = (uint8)(tmp_Node_Id >> 8);
	tmp_Buff[len++] = tmp_Node_Id;
	tmp_Buff[len++] = (uint8)(tmp_DeviceType >> 8);
	tmp_Buff[len++] = tmp_DeviceType;
	tmp_Buff[len++] = (uint8)(tmp_DstId >> 8);
	tmp_Buff[len++] = tmp_DstId;	
	
	sendToServer(tmp_Buff, len);
}



void send_ControlEvent(uint16 tmp_Node_Id, uint8 DataFormat, uint16 tmp_Dst_Id, uint8 msg[])
{
	int len = 0;

//	tmp_Buff[len++] = PLCS_SEND_EVENT;
	tmp_Buff[len++] = (uint8)(tmp_Node_Id >> 8);
	tmp_Buff[len++] = tmp_Node_Id;
	tmp_Buff[len++] = DataFormat;
	tmp_Buff[len++] = (uint8)(tmp_Dst_Id >> 8);
	tmp_Buff[len++] = tmp_Dst_Id;

	memcpy(&tmp_Buff[len], msg, sizeof(uint8) * 8);
	len += sizeof(uint8) * 8;	
	
	xcpsnet_send(tmp_Buff, len);
}



void send_ErrorEvent(uint16 tmp_Node_Id, uint8 DataFormat, uint16 tmp_Dst_Id)
{
	int len = 0;

//	tmp_Buff[len++] = PLCS_SEND_EVENT;
	tmp_Buff[len++] = (uint8)(tmp_Node_Id >> 8);
	tmp_Buff[len++] = tmp_Node_Id;
	tmp_Buff[len++] = DataFormat;
	tmp_Buff[len++] = (uint8)(tmp_Dst_Id >> 8);
	tmp_Buff[len++] = tmp_Dst_Id;

	xcpsnet_send(tmp_Buff, len);
}




void send_NCPResOnTime(uint16 tmp_Node_Id, uint16 tmp_Dst_Id, uint8 tmp_OnTime)
{
	int len = 0;

//	tmp_Buff[len++] = PLCS_RES_ONTIME;
	tmp_Buff[len++] = (uint8)(tmp_Node_Id >> 8);
	tmp_Buff[len++] = tmp_Node_Id;
	tmp_Buff[len++] = (uint8)(tmp_Dst_Id >> 8);
	tmp_Buff[len++] = tmp_Dst_Id;
	tmp_Buff[len++] = tmp_OnTime;

	sendToServer(tmp_Buff, len);
}

void send_NCPReqRegisterLC(uint16 ControllerID, uint16 DeviceType, uint8 Device_Ver, uint8 FW_Ver)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_REGISTER_NODE;
	//tmp_Buff[len++] = (uint8)(NCP_GW_ID >> 8);
	//tmp_Buff[len++] = (uint8)(NCP_GW_ID);
	tmp_Buff[len++] = (uint8)(ControllerID >> 8);
	tmp_Buff[len++] = (uint8)(ControllerID);
	tmp_Buff[len++] = (uint8)(DeviceType >> 8);
	tmp_Buff[len++] = (uint8)(DeviceType);
	tmp_Buff[len++] = Device_Ver;						// Device Version
	tmp_Buff[len++] = FW_Ver;							// Firmware Version

	xcpsnet_send(tmp_Buff, len);
	MSLEEP(3);
}


void send_NCPReqRegisterNode(uint16 ControllerID, uint32 NodeID, uint16 DeviceType, uint16 ShortADDR)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_REGISTER_NODE;
//	tmp_Buff[len++] = (uint8)(NCP_GW_ID >> 8);
//	tmp_Buff[len++] = (uint8)(NCP_GW_ID);
	tmp_Buff[len++] = (uint8)(ControllerID >> 8);
	tmp_Buff[len++] = (uint8)(ControllerID);
	tmp_Buff[len++] = (uint8)(NodeID >> 8);
	tmp_Buff[len++] = (uint8)NodeID;
	tmp_Buff[len++] = (uint8)(DeviceType >> 8);
	tmp_Buff[len++] = (uint8)(DeviceType);		
	tmp_Buff[len++] = 0x10;								// Device Version
	tmp_Buff[len++] = 0x10;								// Firmware Version
	if(DeviceType!=DEVICETYPE_LC100){
		tmp_Buff[len++] = (uint8)(ShortADDR >> 8);
		tmp_Buff[len++] = (uint8)ShortADDR;
	}

	xcpsnet_send(tmp_Buff, len);
	MSLEEP(3);
}

///////////////////////////
void ncp_SendControlReq(uint8 nodeId, uint8 ssrId, uint8 ctrlMode, uint8 dimmingLevel)
{	
	int len = 0;
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NPC_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_CONTROL;								// Load Controller Protocol
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = ssrId;
	tmp_Buff[len++] = ctrlMode;
	tmp_Buff[len++] = dimmingLevel;

	xcps_send(tmp_Buff, len);
}


void ncp_SendDimmingReq(uint16 tmp_src_Id, uint16 tmp_dst_Id, uint8 tmp_Dimming)
{
	int len = 0;
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NPC_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_DIMMING;
	tmp_Buff[len++] = (uint8)(tmp_src_Id >> 8);
	tmp_Buff[len++] = tmp_src_Id;
	tmp_Buff[len++] = (uint8)(tmp_dst_Id >> 8);
	tmp_Buff[len++] = tmp_dst_Id;
	tmp_Buff[len++] = tmp_Dimming;

	xcps_send(tmp_Buff, len);
}

void ncp_SendPowerMeterReq(uint16 tmp_Node_Id)
{
	int len = 0;
	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NPC_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_POWER_METER;
	tmp_Buff[len++] = (uint8)(tmp_Node_Id >> 8);
	tmp_Buff[len++] = tmp_Node_Id;	

	xcps_send(tmp_Buff, len);
}


void ncp_SendStateInfoReq(uint16 tmp_Node_Id, uint16 tmp_Dst_Id, uint16 device_type){
	int len = 0;

	printf_P(PSTR("\n NCP_SEND_STATEINFO_REQ"));
	printf_P(PSTR("\n + Node ID : 0x%02X%02X"), tmp_Node_Id);
	printf_P(PSTR("\n + DeviceType : 0x%02X%02X"), device_type);
	printf_P(PSTR("\n + SubNodeId : 0x%04X"), tmp_Dst_Id);

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NPC_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_STATE_INFO;
	tmp_Buff[len++] = (uint8)(tmp_Node_Id >> 8);
	tmp_Buff[len++] = tmp_Node_Id;
	tmp_Buff[len++] = device_type >>8;
	tmp_Buff[len++] = device_type;
	tmp_Buff[len++] = tmp_Dst_Id >>8;
	tmp_Buff[len++] = tmp_Dst_Id;

	xcps_send(tmp_Buff, len);
}

void ncp_SendSchedule(uint16 tmp_Cont_id, uint8 time_cnt, uint8 msg[], uint8 length)
{
	int len = 0;
	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NPC_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_SEND_SCHEDULE;
	tmp_Buff[len++] = (uint8)(tmp_Cont_id >> 8);
	tmp_Buff[len++] = tmp_Cont_id;
	tmp_Buff[len++] = time_cnt;

	memcpy(&tmp_Buff[len], msg, length);
	len += length;

	xcps_send(tmp_Buff, len);
}

void ncp_SendUpdateSchedule(uint16 tmp_Cont_id, uint8 time_cnt, uint8 msg[], uint8 length)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NPC_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_SEND_SCHEDULE;
	tmp_Buff[len++] = (uint8)(tmp_Cont_id >> 8);
	tmp_Buff[len++] = tmp_Cont_id;
	tmp_Buff[len++] = time_cnt;

	memcpy(&tmp_Buff[len], msg, length);
	len += length;

	xcps_send(tmp_Buff, len);
}

void ncp_SendMappingInfo(uint16 nodeId, uint16 sensorId, uint8 ssrId, uint8 onTime, uint8 offDimmingLevel)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NPC_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_SEND_MAPPING_INFO;	
	tmp_Buff[len++] = nodeId >> 8;
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = sensorId >> 8;
	tmp_Buff[len++] = sensorId;
	tmp_Buff[len++] = ssrId;
	tmp_Buff[len++] = onTime;
	tmp_Buff[len++] = offDimmingLevel;
	
	xcps_send(tmp_Buff, len);
}

void ncp_SendDimmerMappingInfo(uint16 nodeId, uint16 dimmerId, uint8 ssrId)
{

}

void ncp_SendOnTime(uint16 tmp_Node_Id, uint16 tmp_Dst_Id, uint8 tmp_OnTime)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NPC_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_SEND_ON_TIME;
	tmp_Buff[len++] = (uint8)(tmp_Node_Id >> 8);
	tmp_Buff[len++] = tmp_Node_Id;
	tmp_Buff[len++] = (uint8)(tmp_Dst_Id >> 8);
	tmp_Buff[len++] = tmp_Dst_Id;
	tmp_Buff[len++] = tmp_OnTime;

	xcps_send(tmp_Buff, len);
}

//end of file
*/
