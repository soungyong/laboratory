#ifndef __NCP_PROTOCOL_H__
#define __NCP_PROTOCOL_H__

#include "Util.h"
#include "DeviceInfo.h"
#include "PLCS_NCProtocol_Server.h"
#include "../iinchip/types.h"
#include "DS1307_i2c.h"
#include "Xcpsnet.h"
#include "XNetProtocol.h"
#include "Timer.h"

// ---------------------------------------------------------------------------------------------- //
#define NCP_PROTOCOL_ID				(0x80) 	// Protocol ID
#define NCP_DEFAULT_SUB_PROTOCOL_ID				(0x00) 	// Protocol ID
#define PLCS_NPC_PROTOCOL_ID		(0x20)	// subProtocolID
#define NCP_VERSION         				(0x10)  	// Protocol Version

#define NCP_REQ_PING					(0x01)	// Ping
#define NCP_RES_PING					(0x02)	// Ping 응답

#define NCP_REQ_REGISTER				(0x11)	// 등록 요청
#define NCP_RES_REGISTER				(0x12)	// 등록 요청 응답 

#define NCP_REQ_REGISTER_NODE		(0x13)	// 노드 등록 요청
#define NCP_RES_REGISTER_NODE		(0x14)	// 노드 등록 요청 응답

#define NCP_REQ_TIME_INFO			(0x15) //노드 시간 정보 전달

#define NCP_REQ_CONTROL				(0x30)	// 노드 제어요청
#define NCP_RES_CONTROL				(0x31)	// 노드 제어요청 응답
#define NCP_REQ_DIMMING				(0x32)	// 디밍 제어요청
#define NCP_RES_DIMMING				(0x33)	// 디밍 제어요청 응답
#define NCP_REQ_POWER_METER			(0x34)	// 전력량 요청
#define NCP_RES_POWER_METER			(0x35)	// 전력량 요청 응답
#define NCP_REQ_STATE_INFO			(0x36)	// 노드 상태 정보 요청
#define NCP_RES_STATE_INFO			(0x37)	// 노드 상태 정보 요청 응답

#define NCP_SEND_SCHEDULE			(0x41)	// 스케줄 정보 전달
#define NCP_UPDATE_SCHEDULE			(0x42)	// 스케줄 정보 업데이트
#define NCP_SEND_MAPPING_INFO		(0x43)	// 센서 맵핑 정보
#define NCP_SEND_DIMMER_MAPPING_INFO		(0x45)	// 센서 맵핑 정보
#define NCP_UPDATE_POWER_MERTER		(0x44)	// 전력량 정보 전달(Server -> Load Controller)

#define NCP_SEND_EVENT				(0x50)	// 이벤트 정보 전달

#define NCP_SEND_ON_TIME				(0x60)	// 센서 동작감지 차단 시간 전달
#define NCP_RES_ON_TIME				(0x61)	// 센서 동작감지 차단 시간 전달 응답
// ---------------------------------------------------------------------------------------------- //


void processNCP(uint16 Zigbee_id, uint8 msg[], int length);

void send_NCPResPing(uint16 nodeID, uint8 flag, uint16 ping_interval);

void send_NCPResRegister(uint16 node_Id, uint16 result, uint16 timeOut);
void send_NCPResRegisterNode(uint16 node_Id, uint16 deviceType, uint16 result);

void send_TimeInfo(uint16 Node_Id, uint8 tmp_Hour, uint8 tmp_Min);


//---------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------//
//---------------------------------------------------------------------------------------------------------//
void ncp_SendControlReq(uint8 nodeId, uint8 ssrId, uint8 ctrlMode, uint8 dimmingLevel);
void ncp_SendDimmingReq(uint16 tmp_src_Id, uint16 tmp_dst_Id, uint8 tmp_Dimming);
void ncp_SendPowerMeterReq(uint16 tmp_Node_Id);
void ncp_SendStateInfoReq(uint16 tmp_Node_Id, uint16 tmp_Dst_Id, uint16 device_type);
void ncp_SendSchedule(uint16 tmp_Cont_id, uint8 time_cnt, uint8 msg[], uint8 length);
void ncp_SendUpdateSchedule(uint16 tmp_Cont_id, uint8 time_cnt, uint8 msg[], uint8 length);
void ncp_SendMappingInfo(uint16 tmp_Cont_id, uint16 sensorId, uint8 ssrId, uint8 onTime, uint8 offDimmingLevel);
void ncp_SendDimmerMappingInfo(uint16 nodeId, uint16 dimmerId, uint8 ssrId);
void ncp_SendOnTime(uint16 tmp_Node_Id, uint16 tmp_Dst_Id, uint8 tmp_OnTime);

//void ncp_HandlePingReq(uint8 msg[], int length);
//void ncp_HandleRegisterReq(uint8 msg[], int length);
//void ncp_HandleRegisterNodeReq(uint8 msg[], int length);
//void ncp_HandleSendTimeInfo(uint8 msg[], int length);

void send_NCPResControl(uint8 Node_Id, uint8 tmp_Ssr_Id, uint8 tmp_Ctrl_Mode);

void send_NCPResControl_Sensor(uint16 Node_Id, uint8 tmp_Ssr_Id, uint8 tmp_Ctrl_Mode, uint8 tmp_On_Time);

void send_NCPResPowerMeter(uint8 tmp_Node_Id, uint32 tmp_power_nerter);

void send_NCPResStateInfo_LC100Z(uint16 tmp_Node_Id, uint16 tmp_DeviceType, uint8 msg[]);

void send_NCPResStateInfo_ZModule(uint16 tmp_Node_Id, uint16 tmp_DeviceType, uint16 tmp_DstId, uint8 msg);

void send_ControlEvent(uint16 tmp_Node_Id, uint8 DataFormat, uint16 tmp_Dst_Id, uint8 msg[]);

void send_ErrorEvent(uint16 tmp_Node_Id, uint8 DataFormat, uint16 tmp_Dst_Id);

void send_NCPResOnTime(uint16 tmp_Node_Id, uint16 tmp_Dst_Id, uint8 tmp_OnTime);

void send_NCPReqRegisterLC(uint16 ControllerID, uint16 DeviceType, uint8 Device_Ver, uint8 FW_Ver);
void send_NCPReqRegisterNode(uint16 ControllerID, uint32 NodeID, uint16 DeviceType, uint16 ShortADDR);



#endif
