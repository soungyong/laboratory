#ifndef __XCPS_NETL_H__
#define __XCPS_NET_H__


#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <stdio.h>
#include <string.h>

#include "../iinchip/types.h"
#include "../iinchip/w5100.h"
#include "../iinchip/socket.h"
 

#define MY_NET_MAC				{0x00, 0x08, 0xdc, 0x00, (0xff&(NCP_GW_ID>>8)), (0xff&NCP_GW_ID)}

#if 0
#define MY_SERVER_IP				{211, 230, 56, 46}					// Server IP
#define MY_SOURCEIP				{211, 230, 56, 55}					// MY Source IP
#define MY_NET_GWIP				{211, 230, 56, 1}					// MY Gateway IP
#define MY_SUBNET				{255, 255, 255, 128}					// MY Subnet Mask
#endif

#if 1
#define MY_SERVER_IP			{192, 168, 0, 10}					// Server IP
#define MY_SOURCEIP				{192, 168, 0, 50}					// MY Source IP
#define MY_NET_GWIP			{192, 168, 0, 1}					// Default Gateway IP
#define MY_SUBNET				{255, 255, 255, 0}				// MY Subnet Mask
#endif


#define MY_NET_MEMALLOC		0x55							// MY iinchip memory allocation

#define SOCK_TCPC				0
#define XCPSNET_MAX_PDU		128


void InitNET(void);

int xcpsnet_send(const uint8 *data, int length);

int xcpsnet_recv(uint8 *buff, int buff_length);


#endif

