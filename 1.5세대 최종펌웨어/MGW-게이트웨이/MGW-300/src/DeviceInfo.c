
#include "DeviceInfo.h"



// ---------------------------------------------------------------------------------------------- //
// ------------------ Initialize Mapping Table  ------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void initDeviceTable()
{
	deviceInfoTable.size = 0;
}




// ---------------------------------------------------------------------------------------------- //
// ------------------ Find Node Mapping Table  ------------------------------------------------------ //
// ---------------------------------------------------------------------------------------------- //
device_Info *findNodeById(uint16 node_Id)
{
	uint8 i=0;
	for(i=0; i < deviceInfoTable.size; i++) {
		if(deviceInfoTable.deviceInfo[i].nodeId == node_Id) return &(deviceInfoTable.deviceInfo[i]);
	}
	return NULL;
}

int isContainNodeOfId(uint16 nodeId)
{
	uint8 i=0;
	for(i=0; i < deviceInfoTable.size; i++)
		if(deviceInfoTable.deviceInfo[i].nodeId == nodeId) return 1;
	return 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
device_Info *addDeviceTable(uint16 node_Id, uint8 device_version, uint8 fw_version)
{
	uint8 i = 0;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].nodeId = node_Id;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].deviceVersion = device_version;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].fwVersion = fw_version;
	deviceInfoTable.size++;

	return NULL;
}



int updateDeviceTable(uint16 node_Id, uint8 device_version, uint8 fw_version)
{
	device_Info *pUpdate;

	if(((pUpdate = findNodeById(node_Id)) != NULL))
	{
		pUpdate->deviceVersion = device_version;
		pUpdate->fwVersion = fw_version;
		return 0;
	}
	else
	{
		addDeviceTable(node_Id, device_version, fw_version);
		MSLEEP(3);
	}
	return 0;
}





// ---------------------------------------------------------------------------------------------- //
// ------------------ Remove Node & Mapping Table  ------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void removeNode(uint16 node_Id)
{
	uint8 i=0, j=0;

	for(i=0; i < deviceInfoTable.size; i++) {
		if(deviceInfoTable.deviceInfo[i].nodeId==node_Id) {
			for(j=i; j < deviceInfoTable.size -1; j++) {
				deviceInfoTable.deviceInfo[j] = deviceInfoTable.deviceInfo[j+1];
			}
			break;
		}
	}
/*	device_Info *pRemove_Node = pHead->next;

	while(pRemove_Node != pTail)
	{
		if(pRemove_Node->ieee_Id == node_Id)
			free(pRemove_Node);

		pRemove_Node = pHead->next;
	}*/
}


void removeDeviceTable()
{
	deviceInfoTable.size = 0;
/*
	device_Info *pRemove_Table = pHead->next;

	while(pRemove_Table != pTail)
	{
		pHead->next = pRemove_Table->next;
		free(pRemove_Table);

		pRemove_Table = pHead->next;
	}
	*/
}





// ---------------------------------------------------------------------------------------------- //
// ------------------ Count Node & Device Table  ---------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint8 countDeviceTable(uint16 node_Type)
{
	uint8 device_count=0;
	uint8 i=0;

	for(i=0; i < deviceInfoTable.size; i++)
		if(deviceInfoTable.deviceInfo[i].deviceType == node_Type)
			device_count++;
	return device_count;

/*	device_Info *pCount = pHead->next;
	uint8 device_count = 0;
	
	while(pCount != pTail)
	{
		if(pCount->device_Type == node_Type)
			device_count++;

		pCount = pCount->next;
	}
	
	return device_count;
	*/
}


uint8 countDevice()
{
	return deviceInfoTable.size;
/*	device_Info *pCount_Device = pHead->next;
	uint8 device_count = 0;
	
	while(pCount_Device != pTail)
	{
		device_count++;

		pCount_Device = pCount_Device->next;
	}
	
	return device_count;*/
}


// ---------------------------------------------------------------------------------------------- //
// ------------------ Print Node & Mapping Table  ---------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void printDeviceTable()
{
	uint8 i = 0;
	for(i=0; i < deviceInfoTable.size; i++)
		printf_P(PSTR("\n%d: %04X"), ++i, deviceInfoTable.deviceInfo[i].nodeId);
/*	device_Info *pMap = pHead->next;

	while((pMap != pTail) && (pMap!= NULL))
	{
		printf_P(PSTR("\n%d: %02X-%02X"), ++i, pMap->ieee_Id, pMap->nwk_Addr);

		pMap = pMap->next;
	}*/
}


// Enf of File
