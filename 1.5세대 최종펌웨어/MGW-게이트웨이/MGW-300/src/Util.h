#ifndef __UTIL_H__
#define __UTIL_H__

#include	<stdio.h>
#include	<util/delay.h>

#define DEBUG_ENABLE 0

#define		MSLEEP(x)		_delay_ms(x)
#define		USLEEP(x)		_delay_us(x)




//------------------------------------------------------------------------//
//-------------------------- Timer Count -----------------------------------//
//------------------------------------------------------------------------//
#define TIME_COUNT					1			// per 1min's


#define PING_TIME					60



//------------------------------------------------------------------------//
//-------------------------- Timer ID -------------------------------------//
//------------------------------------------------------------------------//
#define GCP_PING_TIMER_ID			0
#define NCP_PING_TIMER_ID			1
#define NCP_PING_COUNT_ID			2
#define RECONNECT_TIMER					3

#define TOKEN_TIMER_ID			    4
#define LED_TIMER_ID				5

#define ON_TIME_COUNT_ID			6
#define TIME_COUNT_ID				8



//------------------------------------------------------------------------//
//-------------------------- EEPROM ID ------------------------------------//
//------------------------------------------------------------------------//
#define CONTROLLER_ID				11






#endif

