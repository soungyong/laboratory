#include "PLCS_NCProtocol_LC100.h"
#include "PLCS_NCProtocol_Server.h"
#include "XNetProtocol.h"

/////////////////
uint8 tmp_Buff[64];
///////////////////
uint16 token=-1; //-1: 할당된 LC 없음. 0: GW가 사용중. 1~0xffff: 해당 ID의 LC가 사용중.

// --------------------------------------------------------------------------- //
// --------------------- Process PLCS (Gateway <-> LC100)  ---------------------- //
// --------------------------------------------------------------------------- //
void plcs_NCP_LC100_ProcessMessage(uint8 msg[], int length)
{
	uint16 nodeId=0;
	uint8 msgId=0;
	
	msgId = msg[1];
	nodeId = msg[2] << 8 | msg[3];
		

	switch(msg[0])
	{
		case NCP_REQ_REGISTER:
			plcs_NCP_LC100_HandleRegisterReq(msg, length);
			break;
		case NCP_REQ_PING:
			plcs_NCP_LC100_HandlePingReq(msg, length);
			break;		
		case NCP_REQ_REGISTER_NODE:		
			plcs_NCP_LC100_HandleRegisterNodeReq(msg, length);	
			break;
		case PLCS_NCP_RES_CONTROL:
			plcs_NCP_LC100_HandleControlRes(msg, length);
			break;					
		case PLCS_NCP_RES_DIMMING:
			plcs_NCP_LC100_HandleDimmingRes(msg, length);
			break;					
		case PLCS_NCP_RES_POWER_METER:			
			plcs_NCP_LC100_HandlePowerMeterRes(msg, length);			
			break;		
		case PLCS_NCP_RES_STATE_INFO:
			plcs_NCP_LC100_HandleStateInfoRes(msg, length);			
			break;		
		case PLCS_NCP_RES_ONTIME:
			plcs_NCP_LC100_HandleResOnTime(msg, length);
			break;		
		case PLCS_NCP_SEND_EVENT:
			if(isOnSensorSharing==1)
				plcs_NCP_LC100_HandleSendEvenNeighbor(msg, length);
			plcs_NCP_LC100_HandleSendEvent(msg, length);			
			break;		
		case PLCS_NCP_SEND_DIMMER2SSR_MAPPING_INFO_RES:
			plcs_NCP_LC100_HandleSendDimmer2SSRMappingInfoRes(msg, length);
			break;
		case PLCS_NCP_SEND_MAPPING_INFO_RES:
			plcs_NCP_LC100_HandleSendMappingInfoRes(msg, length);
			break;		
		case PLCS_NCP_DEBUG_LOG_LC_RES:
			plcs_NCP_LC100_HandleDebugLogLCRes(msg, length);
			break;
		case PLCS_NCP_SEND_PATTERN_RES:
			plcs_NCP_LC100_HandleSendPatternRes(msg, length);
			break;
		case PLCS_SNCP_SET_SENSING_LEVEL_RES:
			plcs_NCP_LC100_HandleSetSensingLevelRes(msg, length);
			break;
		case NCP_REQ_TOKEN_RELEASE:
			plcs_NCP_LC100_HandleReleaseTokenReq(msg, length);
			break;
		default:
			break;
	}
}
void plcs_NCP_LC100_HandleRegisterReq(uint8 msg[], int length) {
	uint16 zigbeeId;	
	uint16 deviceType;
	uint8 device_Ver;
	uint8 fw_Ver;
	uint8 min, hour;

	min = ds1307_read(1);
	hour = ds1307_read(2);

	zigbeeId = msg[2] << 8 | msg[3];
	deviceType = (uint16)((msg[4] << 8) | msg[5]);
	device_Ver = msg[6];
	fw_Ver = msg[7];

	//printf_P(PSTR("\nNCP_REQ_REGISTER (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n NodeId:  (0x%02X)"), zigbeeId);
	//printf_P(PSTR("\n + Device_Type : 0x%04X"), deviceType);
	//printf_P(PSTR("\n + Device_Ver : 0x%02X"), device_Ver);
	//printf_P(PSTR("\n + F/W_Ver : 0x%02X"), fw_Ver);

	updateDeviceTable(zigbeeId, device_Ver, fw_Ver);		
	plcs_NCP_LC100_SendTimeInfo(zigbeeId, hour, min);
	plcs_NCP_SERVER_SendRegisterNodeReq(zigbeeId, zigbeeId, deviceType, &msg[6], length-6);	
	
}

void plcs_NCP_LC100_HandlePingReq(uint8 msg[], int length){
	uint16 nodeId;
	uint8 protocol_Ver;
	uint8 flag=0;

	nodeId = msg[2] << 8 | msg[3];
	protocol_Ver = msg[4];
	flag = msg[5];

	//printf_P(PSTR("\nNCP_REQ_PING (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + NodeId : 0x%04X"), nodeId);				
	//printf_P(PSTR("\n + flag : 0x%02X"), flag);
	//printf_P(PSTR("\n + Protocol_Ver : 0x%02X"), protocol_Ver);

	// Send Response Ping
	if(isContainNodeOfId(nodeId))
		plcs_NCP_LC100_SendPingRes(nodeId, flag, PING_TIME);

	timer_set(NCP_PING_TIMER_ID, 1000);
}

void plcs_NCP_LC100_HandleRegisterNodeReq(uint8 msg[], int length){
	uint16 nodeId;
	uint16 subNodeId;
	uint16 deviceType;
		
	nodeId = msg[2] << 8 | msg[3];	
	subNodeId = msg[4] << 8 | msg[5];
	deviceType = (uint16)((msg[6] << 8) | msg[7]);
		
			
	//printf_P(PSTR("\nNCP_REQ_REGISTER_NODE (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + NodeId : 0x%04X"), nodeId);				
	//printf_P(PSTR("\n + subNodeId : 0x%04X"), subNodeId);				
	//printf_P(PSTR("\n + Device_Type : 0x%02X"), deviceType);
		
	plcs_NCP_SERVER_SendRegisterNodeReq(nodeId, subNodeId, deviceType, &msg[8], length-8);
}

void plcs_NCP_LC100_HandleSendMappingInfoRes(uint8 msg[], int length){
	uint16 nodeId;
	uint16 sensorId;
	uint8 ssrId;
	uint8 onTime;
	uint8 offDimmingLevel;
	uint8 result;

	nodeId = msg[2] << 8 | msg[3];
	sensorId = msg[4] << 8 | msg[5];
	ssrId = msg[6];
	onTime = msg[7];
	offDimmingLevel = msg[8];
	result = msg[9];

	//printf_P(PSTR("\nNCP_RES_DimmerSSRMapping (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + NodeId : 0x%04X"), nodeId);				
	//printf_P(PSTR("\n + sensorId : 0x%04X"), sensorId);				
	//printf_P(PSTR("\n + ssrId : 0x%02X"), ssrId);				
	//printf_P(PSTR("\n + onTime : 0x%02X"), onTime);
	//printf_P(PSTR("\n + offDimmingLevel : 0x%02X"), offDimmingLevel);
	//printf_P(PSTR("\n + result : 0x%02X"), result);				
	
	plcs_NCP_SERVER_SendMappingInfoRes(nodeId, sensorId, ssrId, onTime, offDimmingLevel, result);	
}

void plcs_NCP_LC100_HandleSendDimmer2SSRMappingInfoRes(uint8 msg[], int length){
	uint16 nodeId;
	uint16 dimmerId;
	uint8 ssrId;
	uint8 result;

	nodeId = msg[2] << 8 | msg[3];
	dimmerId = msg[4] << 8 | msg[5];
	ssrId = msg[6];
	result = msg[7];

	//printf_P(PSTR("\nNCP_RES_SensorSSRMapping (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + NodeId : 0x%04X"), nodeId);				
	//printf_P(PSTR("\n + dimmerId : 0x%04X"), dimmerId);				
	//printf_P(PSTR("\n + ssrId : 0x%02X"), ssrId);				
	//printf_P(PSTR("\n + result : 0x%02X"), result);				
	
	plcs_NCP_SERVER_SendDimmer2SSRMappingInfoRes(nodeId, dimmerId, ssrId, result);	
}



void plcs_NCP_LC100_HandleStateInfoRes(uint8 msg[], int length){
	uint16 nodeId;
	uint16 deviceType;

	nodeId = msg[2] << 8 | msg[3];
	deviceType = msg[4] << 8 | msg[5];

	//printf_P(PSTR("\nPLCS_NCP_HANDLEStateInfoRes (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id (0x%04X)"), nodeId);
	//printf_P(PSTR("\n + deviceType (0x%04X)"), nodeId);

	plcs_NCP_SERVER_SendStateInfoRes(nodeId, deviceType, &msg[6], length-6);	
}

void plcs_NCP_LC100_HandleControlRes(uint8 msg[], int length){
	uint16 nodeId;
	uint8 ssrId;
	uint8 ctrlMode;
	uint8 onTime;

	nodeId = msg[2] << 8 | msg[3];
	ssrId = msg[4];		
	ctrlMode = msg[5];

	//printf_P(PSTR("\nPLCS_NCP_RES_CONTROL (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + SSR_Id : 0x%02X"), ssrId);
	//printf_P(PSTR("\n + Ctrl_Mode : 0x%02X"), ctrlMode);

	if(ctrlMode == 0x02){
		onTime = msg[6];	
		//printf_P(PSTR("\n + On_Time : 0x%02X"), onTime);		
	}	
	plcs_NCP_SERVER_SendControlRes(nodeId, ssrId, ctrlMode);
}

void plcs_NCP_LC100_HandlePowerMeterRes(uint8 msg[], int length){
	uint16 nodeId;
	uint32 powerMeterValue;

	nodeId = msg[2] << 8 | msg[3];
	powerMeterValue = ((uint32)msg[4] << 24) |((uint32)msg[5] << 16) | ((uint32)msg[6] << 8) | ((uint32)msg[7]);
				
	//printf_P(PSTR("\nPLCS_RES_POWER_METER (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%02X"), nodeId);
	//printf_P(PSTR("\n + Power_Merter : 0x%lX"), powerMeterValue);

	plcs_NCP_SERVER_SendPowerMeterRes(nodeId, powerMeterValue);
}

void plcs_NCP_LC100_HandleResOnTime(uint8 msg[], int length){
	uint16 nodeId;
	uint16 subNodeId;
	uint8 onTime;

	nodeId = msg[2] << 8 | msg[3];
	subNodeId = msg[4] << 8 | msg[5];

	onTime = msg[6];
	
	//printf_P(PSTR("\nNCP_RES_ON_TIME (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Src_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + Dst_Id : 0x%04X"), subNodeId);
	//printf_P(PSTR("\n + On Time : 0x%02X"), onTime);

	plcs_NCP_SERVER_SendOnTimeRes(nodeId, subNodeId, onTime);
}

void plcs_NCP_LC100_HandleSendEvent(uint8 msg[], int length){
	uint8 i;
	uint16 nodeId;
	uint16 subNodeId;
	uint8 dataFormat;
	
	nodeId = msg[2] << 8 | msg[3];
	dataFormat = msg[4];
	subNodeId = msg[5] <<  8 | msg[6];	
	
	//printf_P(PSTR("\nPLCS_SEND_EVENT (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + SubNode_Id : 0x%04X"), subNodeId);

	switch (dataFormat)
	{	
		case 0x01:

			//for(i = 0; i < 8; i++)
				//printf_P(PSTR("\n + SSR_Id[%d] : 0x%02X"), i, msg[i+7]);
			plcs_NCP_SERVER_SendEvent(nodeId, dataFormat, &(msg[5]), length-5);
			break;
		case 0x02:
			//printf_P(PSTR("\n + Sensor Module Error : 0x%04X"), subNodeId);
//			send_ErrorEvent(nodeId, dataFormat, subNodeId);			
			break;
		case 0x03:
			//printf_P(PSTR("\n + Sensor Communication Error : 0x%04X"), subNodeId);
//			send_ErrorEvent(nodeId, dataFormat, subNodeId);
			break;
		case 0x04:
			//printf_P(PSTR("\n + Dimmer Communication Error : 0x%04X"), subNodeId);
//			send_ErrorEvent(nodeId, dataFormat, subNodeId);
			break;
		default:
			break;
	}
}

void plcs_NCP_LC100_HandleDimmingRes(uint8 msg[], int length){
	int len=0;
	uint16 nodeId;
	uint16 subNodeId;
	uint8 dimmingLevel;

	nodeId = msg[2] << 8 | msg[3];
	subNodeId = msg[4] << 8 | msg[5];
	dimmingLevel = msg[6];

	plcs_NCP_SERVER_SendDimmingRes(nodeId, subNodeId, dimmingLevel);
}

void plcs_NCP_LC100_HandleDebugLogLCRes(uint8 msg[], int length){	
	uint16 nodeId;

	//printf_P(PSTR("\n PLCS_NCP_LC100_DEBUG_LC_Res"));
	nodeId = msg[2] << 8 | msg[3];
	
	plcs_NCP_SERVER_SendDebugLogLCRes(nodeId, &(msg[2]), length-2);	
}

void plcs_NCP_LC100_HandleSendPatternRes(uint8 msg[], int length){
	uint16 nodeId;
	uint8 ssrId;
	uint8 result;
	
	nodeId = msg[2] << 8 | msg[3];
	ssrId = msg[4];
	result = msg[5];

	//printf_P(PSTR("\n PLCS_NCP_SEND_PATTERN_RES"));	
	//printf_P(PSTR("\n + SSR ID : 0x%02X"), ssrId);
	//printf_P(PSTR("\n + Result : 0x%02X"), result);
	
	plcs_NCP_SERVER_SendPatternRes(nodeId, ssrId, result);
}

void plcs_NCP_LC100_HandleSetSensingLevelRes(uint8 msg[], int length){
	uint16 nodeId;
	uint16 sensorId;
	uint8 level;
	nodeId = msg[2] << 8 | msg[3];
	sensorId = msg[4] << 8 | msg[5];
	level = msg[6];

	//printf_P(PSTR("\n PLCS_SNCP_SET_SENSING_LEVEL_RES (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n Node ID (0x%02X)"), nodeId);
	//printf_P(PSTR("\n Sensor ID (0x%02X)"), sensorId);
	//printf_P(PSTR("\n Level (0x%02X)"), level);
	
	plcs_NCP_SERVER_SendSetSensingLevelRes(nodeId, sensorId, level);
}

void plcs_NCP_LC100_HandleSendEvenNeighbor(uint8 msg[], int length){
	uint16 nodeId;
	uint16 sensorId;
	uint8 state;
	uint8 i;

	nodeId = msg[2] << 8 | msg[3];
	sensorId = msg[5] << 8 | msg[6];
	state = 0x01;
	
	if(sensorId==0)return;
	
	//printf_P(PSTR("\n plcs_NCP_LC100_HandleSendEvenNeighbor"));
	for(i=0; i < deviceInfoTable.size; i++)
		if(nodeId!=deviceInfoTable.deviceInfo[i].nodeId){
			plcs_NCP_LC100_SendEventNeighbor(deviceInfoTable.deviceInfo[i].nodeId, sensorId, state);			
		}
	
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//SendProtocol
////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void plcs_NCP_LC100_SendDimmingReq(uint16 tmp_src_Id, uint16 tmp_dst_Id, uint8 tmp_Dimming)
{
	int len = 0;
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_REQ_DIMMING;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(tmp_src_Id >> 8);
	tmp_Buff[len++] = tmp_src_Id;
	tmp_Buff[len++] = (uint8)(tmp_dst_Id >> 8);
	tmp_Buff[len++] = tmp_dst_Id;
	tmp_Buff[len++] = tmp_Dimming;

	sendToController(tmp_Buff, len);
}



void plcs_NCP_LC100_SendPowerMeterReq(uint16 tmp_Node_Id)
{
	int len = 0;
	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_REQ_POWER_METER;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(tmp_Node_Id >> 8);
	tmp_Buff[len++] = tmp_Node_Id;	

	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_SendStateInfoReq(uint16 nodeId, uint16 subNodeId, uint16 deviceType){
	int len = 0;

	//printf_P(PSTR("\n PLCS_NCP_SEND_STATEINFO_REQ"));
	//printf_P(PSTR("\n + Node ID : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + DeviceType : 0x%04X"), deviceType);
	//printf_P(PSTR("\n + SubNodeId : 0x%04X"), subNodeId);

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_REQ_STATE_INFO;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = deviceType >>8;
	tmp_Buff[len++] = deviceType;
	tmp_Buff[len++] = subNodeId >>8;
	tmp_Buff[len++] = subNodeId;

	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_SendSchedule(uint16 tmp_Cont_id, uint8 time_cnt, uint8 msg[], uint8 length)
{
	int len = 0;
	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_SCHEDULE;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(tmp_Cont_id >> 8);
	tmp_Buff[len++] = tmp_Cont_id;
	tmp_Buff[len++] = time_cnt;

	memcpy(&tmp_Buff[len], msg, length);
	len += length;

	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_SendUpdateSchedule(uint16 tmp_Cont_id, uint8 time_cnt, uint8 msg[], uint8 length)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_UPDATE_SCHEDULE;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(tmp_Cont_id >> 8);
	tmp_Buff[len++] = tmp_Cont_id;
	tmp_Buff[len++] = time_cnt;

	memcpy(&tmp_Buff[len], msg, length);
	len += length;

	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_SendMappingInfo(uint16 nodeId, uint16 sensorId, uint8 ssrId, uint8 onTime, uint8 offDimmingLevel)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_MAPPING_INFO;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId >> 8;
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = sensorId >> 8;
	tmp_Buff[len++] = sensorId;
	tmp_Buff[len++] = ssrId;
	tmp_Buff[len++] = onTime;
	tmp_Buff[len++] = offDimmingLevel;
	
	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_ResetMappingInfo(uint16 nodeId){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RESET_MAPPING_INFO;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId >> 8;
	tmp_Buff[len++] = nodeId;	
	
	sendToController(tmp_Buff, len);
}


void plcs_NCP_LC100_SendOnTime(uint16 tmp_Node_Id, uint16 tmp_Dst_Id, uint8 tmp_OnTime)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_ONTIME;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(tmp_Node_Id >> 8);
	tmp_Buff[len++] = tmp_Node_Id;
	tmp_Buff[len++] = (uint8)(tmp_Dst_Id >> 8);
	tmp_Buff[len++] = tmp_Dst_Id;
	tmp_Buff[len++] = tmp_OnTime;

	sendToController(tmp_Buff, len);
}


void plcs_NCP_LC100_SendControlReq(uint8 nodeId, uint8 ssrId, uint8 ctrlMode, uint8 dimmingLevel)
{	
	int len = 0;
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_REQ_CONTROL;								// Load Controller Protocol
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = ssrId;
	tmp_Buff[len++] = ctrlMode;
	tmp_Buff[len++] = dimmingLevel;

	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_SendPingRes(uint16 nodeID, uint8 flag, uint16 pingInterval)
{
	int len = 0;

	//printf_P(PSTR("\nNCP_SEND_PING_RES"));
	//printf_P(PSTR("\n + node_Id : 0x%04X"), nodeID);
	//printf_P(PSTR("\n + flag : 0x%02X"), flag);
	//printf_P(PSTR("\n + interval : 0x%02X"), pingInterval);

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_RES_PING;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeID >> 8);
	tmp_Buff[len++] = (uint8)(nodeID);
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = flag;
	tmp_Buff[len++] = pingInterval>>8;
	tmp_Buff[len++] = pingInterval;
	
	sendToController(tmp_Buff, len);
	MSLEEP(3);
}
void plcs_NCP_LC100_SendRegisterRes(uint16 nodeId, uint16 result, uint16 timeout){
	int len = 0;

	//printf_P(PSTR("\nNCP_SEND_REGISTER_RES"));
	//printf_P(PSTR("\n + node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + result : 0x%02X"), result);
	//printf_P(PSTR("\n + timeOut : 0x%02X"), timeout);

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_RES_REGISTER;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = (uint8)(result >> 8);
	tmp_Buff[len++] = (uint8)(result);
	tmp_Buff[len++] = (uint8)(timeout >> 8);
	tmp_Buff[len++] = (uint8)(timeout);

	sendToController(tmp_Buff, len);
	MSLEEP(3);
}

void plcs_NCP_LC100_SendTimeInfo(uint16 nodeId, uint8 hour, uint8 min){
	int len = 0;
	
	//printf_P(PSTR("\nNCP_SEND_TIMEINFO"));
	//printf_P(PSTR("\n + node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + tmp_Hour : 0x%02X"), hour);
	//printf_P(PSTR("\n + tmp_Min : 0x%02X"), min);

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_SEND_TIME_INFO;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = hour;
	tmp_Buff[len++] = min;

	sendToController(tmp_Buff, len);
	MSLEEP(3);
}

void plcs_NPC_LC100_SendUpdatePowerMeterReq(uint16 nodeId, uint32 powerMeter){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_UPDATE_POWER_METER;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = (uint8)(powerMeter >> 24);
	tmp_Buff[len++] = (uint8)(powerMeter >> 16);
	tmp_Buff[len++] = (uint8)(powerMeter >> 8);
	tmp_Buff[len++] = powerMeter;

	sendToController(tmp_Buff, len);	
}

void plcs_NCP_LC100_SendDimmerMappingInfo(uint16 nodeId, uint16 dimmerId, uint8 ssrId){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_DIMMER_MAPPING_INFO;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = dimmerId>>8;
	tmp_Buff[len++] = dimmerId;
	tmp_Buff[len++] = ssrId;
	
	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_ResetDimmerMappingInfo(uint16 nodeId){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RESET_DIMMER_MAPPING_INFO;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;	
	
	sendToController(tmp_Buff, len);
}
	

void plcs_NCP_LC100_DebugLogReq(uint16 nodeId){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_DEBUG_LOG_LC_REQ;	
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;	
	
	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_DebugLogReset(uint16 nodeId){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_DEBUG_LOG_LC_RESET;	
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;	
	
	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_SendPattern(uint16 nodeId, uint8 ssrId, uint8 patternCnt, uint8 msg[], uint8 length){
	int len = 0;
	int i=0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_PATTERN;	
	tmp_Buff[len++] = 0x00;	
	
	for(i=0; i < length; i++)
		tmp_Buff[len++] = msg[i];
	
	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_SendRetetPattern(uint16 nodeId, uint8 ssrId){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RESET_PATTERN;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;	
	tmp_Buff[len++] = ssrId;	
	
	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_SetSensingLevel(uint16 nodeId, uint16 sensorId, uint8 level){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_SNCP_SET_SENSING_LEVEL;	
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;	
	tmp_Buff[len++] = sensorId >>8;
	tmp_Buff[len++] = sensorId;
	tmp_Buff[len++] = level;
	
	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_SendAck(uint8 msgId, uint16 nodeId){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_ACK;	
	tmp_Buff[len++] = msgId;	
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;		
	
	sendToController(tmp_Buff, len);
}

uint16 plcs_NCP_LC100_GetToken(){
	return token;
}

char plcs_NCP_LC100_AssignToken(uint16 nodeId){
	int len = 0;

	if(token!=-1) return -1; //token 이미 할당된후 해제 되지 않음.
	if(nodeId == token) { //이미 할당된 노드. 할당 해제.
		plcs_NCP_LC100_SendReleaseTokenRes(nodeId);
		return -1;
	}
	
	MSLEEP(5);
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_ASSIGN_TOKEN;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;		
	sendToController(tmp_Buff, len);

	return 0;
}
void plcs_NCP_LC100_HandleReleaseTokenReq(uint8 msg[], int length){	
	uint16 nodeId;

	nodeId = msg[2] << 8 | msg[3];
	
	//printf_P(PSTR("\nplcs_NCP_LC100_HandleReleaseTokenReq (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n NodeId:  (0x%02X)"), nodeId);	
	plcs_NCP_LC100_SendReleaseTokenRes(nodeId);
	
	token = -1;	
}

void plcs_NCP_LC100_SendReleaseTokenRes(uint16 nodeId){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_RES_TOKEN_RELEASE;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;		
	
	sendToController(tmp_Buff, len);

}
void plcs_NCP_LC100_SendRegisterBroadcastReq(char flag){
	int len = 0;	
	char i;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_REGISTER_BROADCAST;
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = 0xff;
	tmp_Buff[len++] = 0xff;
	tmp_Buff[len++] = flag;
	tmp_Buff[len++] = deviceInfoTable.size;
	for(i=0; i < deviceInfoTable.size; i++){
		tmp_Buff[len++] = deviceInfoTable.deviceInfo[i].nodeId>>8;
		tmp_Buff[len++] = deviceInfoTable.deviceInfo[i].nodeId;
	}	
	
	sendToController(tmp_Buff, len);
}

void plcs_NCP_LC100_SendEventNeighbor(uint16 nodeId, uint16 sensorId, uint8 sensorState){
	
	int len = 0;		
	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_EVENT_NEIGHBOR;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;	
	tmp_Buff[len++] = sensorId>>8;
	tmp_Buff[len++] = sensorId;
	tmp_Buff[len++] = sensorState;
		
	sendToController(tmp_Buff, len);
}
