
#include <avr/eeprom.h>
#include "XNetProtocol.h"

uint8 tmp_Data_Server[64];
uint8 tmp_Data_Controler[64];


void XNetPacketFromController(uint8 *msg, int buff_length){
	uint8 i =0;
	uint8 tmp_msg[64];
	int len = 0;	
	
	for(i = 0; i < buff_length-2; i++)
		tmp_msg[i] = msg[i+2];

	len = buff_length - 2;

	switch(msg[0])
	{
		case NCP_PROTOCOL_ID:
			plcs_NCP_LC100_ProcessMessage(tmp_msg, len); 						
			break;		
		default:
			break;
	}
}

void XNetPacketFromServer(uint8 *msg, int buff_length){
	uint8 i =0;
	uint8 tmp_msg[64];
	int len = 0;	
	
	for(i = 0; i < buff_length-2; i++)
		tmp_msg[i] = msg[i+2];

	len = buff_length - 2;

	switch(msg[0])
	{
		case NCP_PROTOCOL_ID:
			plcs_NCP_Server_ProcessMessage(tmp_msg, len); 			
			break;		
		default:
			break;
	}
}

void XNetPacket(uint8 *msg, int buff_length)										// Paser Server <-> Gateway / Server <-> Controller_Node
{	
	uint8 i =0;
	uint8 tmp_msg[64];
	int len = 0;
	uint16 Controller_id;
	uint16 Controller_addr;

	for(i = 0; i < buff_length-2; i++)
		tmp_msg[i] = msg[i+2];

	len = buff_length - 2;

	switch(msg[0])
	{
		case NCP_PROTOCOL_ID:			
			break;		
		default:
			break;
	}
}



void sendToController(uint8 msg[], int msg_len)
{
	int len = 0;

	memcpy(&tmp_Data_Controler[len], msg, msg_len);
	len += msg_len;

	xcps_send(tmp_Data_Controler, len);
	MSLEEP(3);
}

void sendToServer(uint8 msg[], int msg_len)
{
	int len = 0;
	if (getSn_SR(SOCK_TCPC)!= SOCK_ESTABLISHED)return;
	memcpy(&tmp_Data_Server[len], msg, msg_len);
	len += msg_len;

	xcpsnet_send(tmp_Data_Server, len);
	MSLEEP(3);
}


//end of file

