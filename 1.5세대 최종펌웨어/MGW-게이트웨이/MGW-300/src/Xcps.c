
#include "Xcps.h"
#include "Uart.h"


// protocol message format start/end flag.
#define	ZS_SFLAG	0xFA
#define	ZS_EFLAG	0xAF


/**
	temporary rx packet.
*/
uint8 xcps_rx_packet[XCPS_MAX_PDU];


// 03/01. xcp zigbee uart transport receive state.
static int xcps_state = 0;
int xcps_rxlen = 0;		// total length received.
int xcps_pdu_len = 0;	// packet payload length.

/**
	uart interface to use.
*/
static usart1_getter	xcps_getter = (usart1_getter)0;
static usart1_putter	xcps_putter = (usart1_putter)0;

int xcps_init(usart1_getter getter, usart1_putter putter)
{
	xcps_getter = getter;
	xcps_putter = putter;
	
	return 0;
}

int xcps_send(const uint8 *data, int length)
{
	int i, msgLen;
    	uint8 buff[XCPS_MAX_PDU];
	int	checkSum = 0;
	
	// check putter.
	if (!xcps_putter) return -1;

    	msgLen = 0;        
    	buff[msgLen++] = ZS_SFLAG; 			//Start Byte_1
    	buff[msgLen++] = (uint8)(length); 		//Dest Addr

	for (i = 0; i < length; i++)
		buff[msgLen++] = data[i];

	// calc checksum.
	for (i = 2; i < msgLen; i++)
		checkSum += ((uint8)buff[i] & 0xFF);
	
	buff[msgLen++] = (uint8)(checkSum & 0xFF);


	// end flag.
    	buff[msgLen++] = ZS_EFLAG;

	//if(buff[4]!=0x06 && buff[4]!=0x08) printf("\nG->N :");
    	for(i=0; i < msgLen; i++)
    	{
        	xcps_putter(buff[i]); 
	//	if(buff[4]!=0x06 && buff[4]!=0x08)printf_P(PSTR("%02X."), buff[i]);
    	}

	return 0;
}


/**

*/
int xcps_recv(uint8 *buff, int buff_length)
{
	uint8 temp;
	int i;
	int checkSum = 0;
	
	// check getter.
	if (!xcps_getter)
		return -1;
	

	if(xcps_getter(&temp) < 1 )
		return 0;

	//if(temp==0xfa)printf_P(PSTR("\n"));
	//printf_P(PSTR("%02X."), temp);
	

	if(xcps_rxlen >= XCPS_MAX_PDU)
		xcps_state=0;	
	switch (xcps_state)
	{
		case 0:
			xcps_rxlen = 0;

			if (temp == ZS_SFLAG)
			{
				xcps_state = 1;
				xcps_rx_packet[xcps_rxlen++] = temp;
			}
					
			else if (temp == ZS_EFLAG)
			{
				// don't change state, keep find start flag.
			}
		break;

		case 1:	// found start flag
			xcps_rx_packet[xcps_rxlen++] = temp;
			xcps_state = 2;
			xcps_pdu_len = temp;
			
		break;

		case 2:	// found length
			// fill data.
			xcps_rx_packet[xcps_rxlen++] = temp;
				
			// check length.
			if (xcps_rxlen >= (2 + xcps_pdu_len))
				xcps_state = 3;
		break;

		case 3:	// data end, check checksum.				
			for (i = 2; i < (2 + xcps_pdu_len); i++)
				checkSum += (xcps_rx_packet[i] & 0xFF);

			if (temp == (uint8)checkSum )// Checksum ok.
				xcps_state = 4;
			
			else
				xcps_state = 0;
		break;

		case 4:
			if (temp == ZS_EFLAG)
			{
				xcps_state = 0;

				// return data to caller.
				for (i = 0;  i < xcps_pdu_len; i++)
					buff[i] = xcps_rx_packet[2+i];
				
				return xcps_pdu_len;
			}
			else
			{
				// TODO:
				xcps_state = 0;
			}
		break;
		
	default:
		// if you here, something wrong. --> recover to state 0.
		xcps_state = 0;
		
		break;
	}
	return 0;
}


//end of file
