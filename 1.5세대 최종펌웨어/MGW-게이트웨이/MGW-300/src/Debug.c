#include "Debug.h"
#include <avr/eeprom.h>
#include "ds1307_i2c.h"

DebugInfo_st debugInfo EEMEM;

void debug_UpdateLog(){
	eeprom_write_block(&debug_DebugInfo, &debugInfo, sizeof(DebugInfo_st));	
}

void debug_ReadLog(){
	eeprom_read_block(&debug_DebugInfo, &debugInfo, sizeof(DebugInfo_st));
}

void debug_Reset(){
	uint8 i=0;
	debug_DebugInfo.rebootCnt=0;
	debug_DebugInfo.serverConnectionCnt=0;
	for(i=0; i <5; i++){
		debug_DebugInfo.lastRebootTime[i]=0;
		debug_DebugInfo.lastServerConnectionTime[i]=0;
	}
	debug_UpdateLog();
}

void debug_UpdateReboot(){	
	debug_DebugInfo.rebootCnt++;
	debug_DebugInfo.lastRebootTime[0] = ds1307_read(6);
	debug_DebugInfo.lastRebootTime[1] = ds1307_read(5);
	debug_DebugInfo.lastRebootTime[2] = ds1307_read(4);
	debug_DebugInfo.lastRebootTime[3] = ds1307_read(2);
	debug_DebugInfo.lastRebootTime[4] = ds1307_read(1);	
	debug_UpdateLog();
}

void debug_UpdateConnection(){
	debug_DebugInfo.serverConnectionCnt++;
	debug_DebugInfo.lastServerConnectionTime[0] = ds1307_read(6);
	debug_DebugInfo.lastServerConnectionTime[1] = ds1307_read(5);
	debug_DebugInfo.lastServerConnectionTime[2] = ds1307_read(4);
	debug_DebugInfo.lastServerConnectionTime[3] = ds1307_read(2);
	debug_DebugInfo.lastServerConnectionTime[4] = ds1307_read(1);	
	debug_UpdateLog();
}
