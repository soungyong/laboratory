#ifndef __PLCS_NCP_LC100_PROTOCOL_H__
#define __PLCS_NCP_LC100_PROTOCOL_H__

#include "PLCS_NCProtocol.h"
#include "../iinchip/types.h"

void plcs_NCP_LC100_ProcessMessage(uint8 msg[], int length);

void plcs_NCP_LC100_HandlePingReq(uint8 msg[], int length);
void plcs_NCP_LC100_HandleRegisterReq(uint8 msg[], int length);
void plcs_NCP_LC100_HandleRegisterNodeReq(uint8 msg[], int length);

void plcs_NCP_LC100_HandleStateInfoRes(uint8 msg[], int length);
void plcs_NCP_LC100_HandleControlRes(uint8 msg[], int length);
void plcs_NCP_LC100_HandlePowerMeterRes(uint8 msg[], int length);
void plcs_NCP_LC100_HandleResOnTime(uint8 msg[], int length);
void plcs_NCP_LC100_HandleSendEvent(uint8 msg[], int length);
void plcs_NCP_LC100_HandleSendTimeInfo(uint8 msg[], int length);
void plcs_NCP_LC100_HandleDimmingRes(uint8 msg[], int length);
void plcs_NCP_LC100_HandleSendDimmer2SSRMappingInfoRes(uint8 msg[], int length);
void plcs_NCP_LC100_HandleSendMappingInfoRes(uint8 msg[], int length);
void plcs_NCP_LC100_HandleDebugLogLCRes(uint8 msg[], int length);
void plcs_NCP_LC100_HandleSendPatternRes(uint8 msg[], int length);
void plcs_NCP_LC100_HandleSetSensingLevelRes(uint8 msg[], int length);
void plcs_NCP_LC100_HandleSendEvenNeighbor(uint8 msg[], int length);

void plcs_NCP_LC100_SendRegisterRes(uint16 nodeId, uint16 result, uint16 timeout);
void plcs_NCP_LC100_SendPingRes(uint16 nodeID, uint8 flag, uint16 pingInterval);
void plcs_NCP_LC100_SendStateInfoRes(uint16 nodeId, uint16 deviceType, uint8 msg[], uint8 len);
void plcs_NCP_LC100_SendTimeInfo(uint16 nodeId, uint8 hour, uint8 min);
void plcs_NCP_LC100_SendAck(uint8 msgId, uint16 nodeId);

void plcs_NCP_LC100_SendControlReq(uint8 nodeId, uint8 ssrId, uint8 ctrlMode, uint8 dimmingLevel);
void plcs_NCP_LC100_SendDimmingReq(uint16 nodeId, uint16 tmp_dst_Id, uint8 tmp_Dimming);
void plcs_NCP_LC100_SendPowerMeterReq(uint16 nodeId);
void plcs_NPC_LC100_SendUpdatePowerMeterReq(uint16 nodeId, uint32 powerMeter);
void plcs_NCP_LC100_SendStateInfoReq(uint16 tmp_Node_Id, uint16 tmp_Dst_Id, uint16 device_type);
void plcs_NCP_LC100_SendSchedule(uint16 nodeId, uint8 time_cnt, uint8 msg[], uint8 length);
void plcs_NCP_LC100_SendUpdateSchedule(uint16 tmp_Cont_id, uint8 time_cnt, uint8 msg[], uint8 length);
void plcs_NCP_LC100_SendMappingInfo(uint16 nodeId, uint16 sensorId, uint8 ssrId, uint8 onTime, uint8 offDimmingLevel);
void plcs_NCP_LC100_ResetMappingInfo(uint16 nodeId);
void plcs_NCP_LC100_SendDimmerMappingInfo(uint16 nodeId, uint16 dimmerId, uint8 ssrId);
void plcs_NCP_LC100_ResetDimmerMappingInfo(uint16 nodeId);
void plcs_NCP_LC100_SendOnTime(uint16 nodeId, uint16 tmp_Dst_Id, uint8 tmp_OnTime);
void plcs_NCP_LC100_DebugLogReq(uint16 nodeId);
void plcs_NCP_LC100_DebugLogReset(uint16 nodeId);
void plcs_NCP_LC100_SendPattern(uint16 nodeId, uint8 ssrId, uint8 patternCnt, uint8 msg[], uint8 length);
void plcs_NCP_LC100_SendRetetPattern(uint16 nodeId, uint8 ssrId);
void plcs_NCP_LC100_SetSensingLevel(uint16 nodeId, uint16 sensorId, uint8 level);
void plcs_NCP_LC100_SendEventNeighbor(uint16 nodeId, uint16 sensorId, uint8 sensorState);

uint16 plcs_NCP_LC100_GetToken();
char plcs_NCP_LC100_AssignToken(uint16 nodeId);
void plcs_NCP_LC100_HandleReleaseTokenReq(uint8 msg[], int length);
void plcs_NCP_LC100_SendReleaseTokenRes(uint16 nodeId);
void plcs_NCP_LC100_SendRegisterBroadcastReq(char flag);

extern uint8 isOnSensorSharing;

#endif
