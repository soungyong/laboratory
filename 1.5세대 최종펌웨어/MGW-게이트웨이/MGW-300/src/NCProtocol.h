#ifndef __NCP_PROTOCOL_H__
#define __NCP_PROTOCOL_H__

#include "Util.h"
#include "Xcps.h"
#include "Xcpsnet.h"
#include "DeviceInfo.h"
#include "ScheduleTime.h"
#include "../iinchip/types.h"
#include "DS1307_i2c.h"
#include "timer.h"

// ------------------------------------------------------------------- //
// --------------- Process NCP Basic (Gateway <-> Server) ---------------- //
// ------------------------------------------------------------------- //
#define NCP_PROTOCOL_ID					(0x80)
#define NCP_DEFAULT_SUB_PROTOCOL_ID		(0x00)

#define NCP_VERSION						(0x10)
#define NCP_PROTOCOL_VER				(0x20)


#define NCP_REQ_PING					(0x01)
#define NCP_RES_PING					(0x02)

#define NCP_REQ_REGISTER_BROADCAST		(0x05)
#define NCP_ASSIGN_TOKEN		  		(0x06)
#define NCP_REQ_TOKEN_RELEASE	  		(0x07)
#define NCP_RES_TOKEN_RELEASE	  		(0x08)
#define NCP_REQ_REGISTER				(0x11)
#define NCP_RES_REGISTER				(0x12)

#define NCP_REQ_REGISTER_NODE			(0x13)
#define NCP_RES_REGISTER_NODE			(0x14)
#define NCP_SEND_TIME_INFO				(0x15)

#define NCP_ACK							(0x99)

#define NCP_GW_TYPE 					(uint16)(0x9101)
#define NCP_GW_ID						(uint16)(0x0004)
#define NCP_GW_VER						(uint8)(0x10)
#define NCP_FW_VER						(uint8)(0x13)

#endif

