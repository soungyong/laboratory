#ifndef __PLCS_NCP_SERVER_PROTOCOL_H__
#define __PLCS_NCP_SERVER_PROTOCOL_H__

#include "PLCS_NCProtocol.h"

#define NET_NOT_CONNECT				0	
#define NET_REGISTER				1

extern uint8 plcs_NCP_SERVER_NetState;
extern uint16 plcs_NCP_SERVER_PingInterval;
extern uint8 plcs_NCP_SERVER_Ping_Count;

void plcs_NCP_SERVER_ProcessMessage(uint8 msg[], int length);

void plcs_NCP_SERVER_HandlePingRes(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleRegisterRes(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleRegisterNodeRes(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleSendTimeInfo(uint8 msg[], int length);

void plcs_NCP_SERVER_HandleControlReq(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleDimmingReq(uint8 msg[], int length);
void plcs_NCP_SERVER_HandlePowerMeterReq(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleUpdatePowerMeter(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleStateInfoReq(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleSendSchedule(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleUpdateSchedule(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleSendMappingInfo(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleResetMappingInfo(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleSendDimmerMappingInfo(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleResetDimmerMappingInfo(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleSendOnTime(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleDebugLogReq(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleDebugLogReset(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleDebugLogLCReq(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleDebugLogLCReset(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleSendPattern(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleResetPattern(uint8 msg[], int length);
void plcs_NCP_SERVER_HandleSetSensingLevel(uint8 msg[], int length);


void plcs_NCP_SERVER_SendPingReq();
void plcs_NCP_SERVER_SendRegisterReq();
void plcs_NCP_SERVER_SendRegisterNodeReq(uint16 nodeId, uint32 subNodeId, uint16 DeviceType, uint8 msg[], uint8 length);

void plcs_NCP_SERVER_SendEvent(uint16 nodeId, uint8 dataFormat, uint8 msg[], uint8 length);
void plcs_NCP_SERVER_SendStateInfoRes(uint16 nodeId, uint16 deviceType, uint8 msg[], uint8 len);
void plcs_NCP_SERVER_SendControlRes(uint8 nodeId, uint8 ssrId, uint8 ctrlMode);
void plcs_NCP_SERVER_SendDimmingRes(uint16 nodeId, uint16 subNodeId, uint8 dimmingLevel);
void plcs_NCP_SERVER_SendPowerMeterRes(uint16 nodeId, uint32 powerMeterValue);
void plcs_NCP_SERVER_SendOnTimeRes(uint16 nodeId, uint16 subNodeId, uint8 onTime);
void plcs_NCP_SERVER_SendDimmer2SSRMappingInfoRes(uint16 nodeId, uint16 dimmerId, uint8 ssrId, uint8 result);
void plcs_NCP_SERVER_SendMappingInfoRes(uint16 nodeId, uint16 sensorId, uint8 ssrId, uint8 onTime, uint8 offDimmingLevel, uint8 result);
void plcs_NCP_SERVER_SendDebugLogRes(uint16 rebootCnt, uint8 lastRebootTime[], uint16 serverConnectionCnt, uint8 lastServerConnectionTime[]);
void plcs_NCP_SERVER_SendDebugLogLCRes(uint16 nodeId, uint8 msg[], uint8 length);
void plcs_NCP_SERVER_SendPatternRes(uint16 nodeId, uint8 ssrId, uint8 result);
void plcs_NCP_SERVER_SendSetSensingLevelRes(uint16 nodeId, uint16 sensorId, uint8 level);

uint8 plcs_NCP_SERVER_GetNetworkState();
uint16 plcs_NCP_SERVER_GetPingIntervalTime();
uint8 plcs_NCP_SERVER_GetPingCount();

#endif
