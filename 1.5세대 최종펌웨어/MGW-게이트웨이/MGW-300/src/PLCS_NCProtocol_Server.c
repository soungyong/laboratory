#include "PLCS_NCProtocol_Server.h"
#include "PLCS_NCProtocol_LC100.h"
#include "XNetProtocol.h"
#include "debug.h"


/////////////////
uint8 tmp_Buff[64];
///////////////////

// --------------------------------------------------------------------------- //
// --------------------- Process PLCS (Gateway <-> Server)  ---------------------- //
// --------------------------------------------------------------------------- //
//uint16 cntForNCPTest=0;

void plcs_NCP_Server_ProcessMessage(uint8 msg[], int length)
{
	switch(msg[0])
	{		
		case NCP_RES_REGISTER:
			plcs_NCP_SERVER_HandleRegisterRes(msg, length);		
			break;
		case NCP_RES_PING: 
			plcs_NCP_SERVER_HandlePingRes(msg, length);
			break;		
		case NCP_RES_REGISTER_NODE:		
			plcs_NCP_SERVER_HandleRegisterNodeRes(msg, length);	
			break;
		case NCP_SEND_TIME_INFO:
			plcs_NCP_SERVER_HandleSendTimeInfo(msg, length);
			break;	
		case PLCS_NCP_REQ_CONTROL:
			plcs_NCP_SERVER_HandleControlReq(msg, length);
			break;					
		case PLCS_NCP_REQ_DIMMING:
			plcs_NCP_SERVER_HandleDimmingReq(msg, length);
			break;					
		case PLCS_NCP_REQ_POWER_METER:			
			plcs_NCP_SERVER_HandlePowerMeterReq(msg, length);			
			break;		
		case PLCS_NCP_UPDATE_POWER_METER:
			plcs_NCP_SERVER_HandleUpdatePowerMeter(msg, length);			
			break;
		case PLCS_NCP_REQ_STATE_INFO:
			plcs_NCP_SERVER_HandleStateInfoReq(msg, length);			
			break;		
		case PLCS_NCP_SEND_SCHEDULE:
			plcs_NCP_SERVER_HandleSendSchedule(msg, length);						
			break;			
		case PLCS_NCP_UPDATE_SCHEDULE:
			plcs_NCP_SERVER_HandleUpdateSchedule(msg, length);			
			break;
		case PLCS_NCP_SEND_MAPPING_INFO:
			plcs_NCP_SERVER_HandleSendMappingInfo(msg, length);			
			break;
		case PLCS_NCP_RESET_MAPPING_INFO: 
			plcs_NCP_SERVER_HandleResetMappingInfo(msg, length);
			break;
		case PLCS_NCP_SEND_DIMMER_MAPPING_INFO:
			plcs_NCP_SERVER_HandleSendDimmerMappingInfo(msg, length);						
			break;
		case PLCS_NCP_RESET_DIMMER_MAPPING_INFO:
			plcs_NCP_SERVER_HandleResetDimmerMappingInfo(msg, length);						
			break;
		case PLCS_NCP_SEND_ONTIME:
			plcs_NCP_SERVER_HandleSendOnTime(msg, length);			
			break;					
		case PLCS_NCP_DEBUG_LOG_REQ:
			plcs_NCP_SERVER_HandleDebugLogReq(msg, length);
			break;
		case PLCS_NCP_DEBUG_LOG_RESET:
			plcs_NCP_SERVER_HandleDebugLogReset(msg, length);
			break;
		case PLCS_NCP_DEBUG_LOG_LC_REQ:
			plcs_NCP_SERVER_HandleDebugLogLCReq(msg, length);
			break;	
		case PLCS_NCP_DEBUG_LOG_LC_RESET:
			plcs_NCP_SERVER_HandleDebugLogLCReset(msg, length);
			break;
		case PLCS_NCP_SEND_PATTERN:
			plcs_NCP_SERVER_HandleSendPattern(msg, length);
			break;
		case PLCS_NCP_RESET_PATTERN:
			plcs_NCP_SERVER_HandleResetPattern(msg, length);
			break;
		case PLCS_SNCP_SET_SENSING_LEVEL: 
			plcs_NCP_SERVER_HandleSetSensingLevel(msg, length);
			break;
		default:
			break;
	}
}

void plcs_NCP_SERVER_HandleRegisterRes(uint8 msg[], int length){
	uint16 timeOut;
	uint8 i;

	timeOut =(uint16)((msg[6] << 8) | msg[7]);

	//printf_P(PSTR("\nPLCS_NCP_SERVER_RES_REGISTER (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + G/W ID : 0x%02X%02X"), msg[2], msg[3]);
	//printf_P(PSTR("\n + Result : 0x%02X%02X"), msg[4], msg[5]);
	//printf_P(PSTR("\n + Registration Timeout = 0x%04X"), timeOut);

	// Add Function Timer + Ping Interval
	if(plcs_NCP_SERVER_NetState != NET_REGISTER){
		debug_UpdateConnection();
		for(i=0; i < deviceInfoTable.size; i++) {
			plcs_NCP_LC100_SendPingRes(deviceInfoTable.deviceInfo[i].nodeId, 0x01, PING_TIME);
		}		
	}

	plcs_NCP_SERVER_NetState = NET_REGISTER;
	plcs_NCP_SERVER_SendPingReq();
}

void plcs_NCP_SERVER_HandlePingRes(uint8 msg[], int length) {
	uint16 nodeId;
	uint8 flag=0;
	uint8 protocolVer;
	uint16 pingInterval;

	nodeId = msg[2] << 8 | msg[3];
	protocolVer = msg[4];
	flag = msg[5];
	pingInterval = (uint16)((msg[6] << 8) | msg[7]);

	//printf_P(PSTR("\nPLCS_NCP_SREVER_RES_PING (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Version : 0x%02X"), protocolVer);
	//printf_P(PSTR("\n + flag : %02X"), flag);
	//printf_P(PSTR("\n + Ping_Interval : %04X"), pingInterval);

	plcs_NCP_SERVER_Ping_Count=0;

	// Add Function Timer + Ping Interval
	if(pingInterval==0) pingInterval=0xffff;
	//timer_set(NCP_PING_TIMER_ID, ((uint32)pingInterval * 1000));
	plcs_NCP_SERVER_PingInterval = pingInterval;


	if(flag==0x01) { //재 등록.
		uint16 i=0;
		plcs_NCP_SERVER_SendRegisterReq();
		//하위 노드들도 재 등록 요청.		
		//printf_P(PSTR("\n + deviceTableSize : %d"),deviceInfoTable.size);
		for(i=0; i < deviceInfoTable.size; i++) {
			plcs_NCP_LC100_SendPingRes(deviceInfoTable.deviceInfo[i].nodeId, 0x01, PING_TIME);
		}
	}
}

void plcs_NCP_SERVER_HandleRegisterNodeRes(uint8 msg[], int length){
	uint16 nodeId;
	uint16 deviceType;
	uint8 result;

	nodeId = msg[2] << 8 | msg[3];
	deviceType = msg[4] << 8 | msg[5];
	result = (uint16)((msg[6] << 8) | msg[7]);

	//printf_P(PSTR("\nNCP_RES_REGISTER_NODE (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + Device Type : 0x%04X"), deviceType);
	//printf_P(PSTR("\n + Result : 0x%02X"), result);	
	
	if(deviceType==DEVICETYPE_LC100) {
		uint8 min, hour;
		min = ds1307_read(1);
		hour = ds1307_read(2);

		plcs_NCP_LC100_SendRegisterRes(nodeId, result, 0x0030);		
		plcs_NCP_LC100_SendTimeInfo(nodeId, hour, min);
	}
}

void plcs_NCP_SERVER_HandleSendTimeInfo(uint8 msg[], int length){
	uint8 i;
	uint16 year, month;
	uint8 date, day, hour, min, sec;
	
	uint8 date_Buff[16];
	
	//printf_P(PSTR("\nPLCS_TIME_INFO (0x%02X)"), msg[0]);

	if(msg[2] == 0x01)
	{
		for(i =0; i < (length-2); i++)
			date_Buff[i] = (msg[3+i] - '0');
	}
	
	year = (date_Buff[3] *10) + (date_Buff[4]);
	month = (uint8)((date_Buff[5] * 10) + date_Buff[6]);
	date = (uint8)((date_Buff[7] * 10) + date_Buff[8]);
	day = (uint8)(date_Buff[9]);
	hour = (uint8)((date_Buff[10] * 10) + date_Buff[11]);
	min = (uint8)((date_Buff[12] * 10) + date_Buff[13]);
	sec = (uint8)((date_Buff[14] * 10) + date_Buff[15]);

	//printf_P(PSTR("\n + Date : %04X.%02X.%02X %02X:%02X:%02X"), year, month, date, hour, min, sec);
	//printf_P(PSTR("\n + Date : %d.%d.%d %d:%d:%d"), year, month, date, hour, min, sec);

	ds1307_initial_config(sec, min, hour, day, date, month, year);
	for(i=0; i < deviceInfoTable.size; i++) {		
		plcs_NCP_LC100_SendTimeInfo(deviceInfoTable.deviceInfo[i].nodeId, hour, min);
	}
}

void plcs_NCP_SERVER_HandleControlReq(uint8 msg[], int length){	
	uint16 nodeId;
	uint8 ssrId;
	uint8 ctrlMode;
	uint8 dimmingLevel;

	nodeId = msg[2] << 8 | msg[3];
	ssrId = msg[4];		
	ctrlMode = msg[5];
	dimmingLevel = msg[6];

	//printf_P(PSTR("\nPLCS_NCP_REQ_CONTROL (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + SSR_Id : 0x%02X"), ssrId);
	//printf_P(PSTR("\n + Ctrl_Cmd : 0x%02X"), ctrlMode);
	//printf_P(PSTR("\n + DimmingLevel : 0x%02X"), dimmingLevel);	
	plcs_NCP_LC100_SendControlReq(nodeId, ssrId, ctrlMode, dimmingLevel);	
}

void plcs_NCP_SERVER_HandleDimmingReq(uint8 msg[], int length){	
	uint16 nodeId;
	uint16 dimmerId;
	uint8 dimmingLevel;

	nodeId = msg[2] << 8 | msg[3];
	dimmerId = (uint16)((msg[4] << 8) | msg[5]);
	dimmingLevel = msg[6];
	
	//printf_P(PSTR("\nPLCS_NCP_REQ_DIMMING (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + SRC_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + DST_Id : 0x%04X"), dimmerId);
	//printf_P(PSTR("\n + Dimming : 0x%02X"), dimmingLevel);

	plcs_NCP_LC100_SendDimmingReq(nodeId, dimmerId, dimmingLevel);
}

void plcs_NCP_SERVER_HandlePowerMeterReq(uint8 msg[], int length){
	uint16 nodeId;
	nodeId = msg[2] << 8 | msg[3];
		
	//printf_P(PSTR("\nPLCS_NCP_REQ_POWER_METER (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);
	
	plcs_NCP_LC100_SendPowerMeterReq(nodeId);	
}

void plcs_NCP_SERVER_HandleUpdatePowerMeter(uint8 msg[], int length){
	uint16 nodeId;
	uint32 meterValue;

	nodeId = msg[2] << 8 | msg[3];
	meterValue = ((uint32)msg[4] << 24) |((uint32)msg[5] << 16) | ((uint32)msg[6] << 8) | ((uint32)msg[7]);
	
	//printf_P(PSTR("\nPLCS_NCP_UPDATE_POWER_METER (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + Meter_Value : 0x%lX"), meterValue);
	plcs_NPC_LC100_SendUpdatePowerMeterReq(nodeId, meterValue);	
}

void plcs_NCP_SERVER_HandleStateInfoReq(uint8 msg[], int length){
	uint16 nodeId;
	uint16 deviceType;
	uint16 subNodeId;

	nodeId = msg[2] << 8 | msg[3];
	deviceType = (uint16)((msg[4] << 8 ) | msg[5]);
		
	//printf_P(PSTR("\nPLCS_NCP_REQ_STATE_INFO (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + DEVICE TYPE : 0x%04X"), deviceType);				
	
	switch (deviceType)
	{
		case PLCS_LC_100Z_TYPE:	
			subNodeId=0;														
			break;
		case PLCS_ZDIMMER_TYPE:
		case PLCS_ZSENSOR_TYPE:
			subNodeId = (uint16)((msg[6] << 8) | msg[7]);																		
			break;					
	}
	plcs_NCP_LC100_SendStateInfoReq(nodeId, subNodeId, deviceType);
}

void plcs_NCP_SERVER_HandleSendSchedule(uint8 msg[], int length){
	uint16 nodeId;
	uint8 timeCnt;
	uint8 i;
	uint8 ssrId;
	uint8 startTime;
	uint8 endTime;

	nodeId = msg[2] <<8 | msg[3];
	timeCnt = msg[4];
	ssrId = msg[5];
	//printf_P(PSTR("\nPLCS_NCP_SEND_SCHEDULE (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);

	for(i=0; i < timeCnt; i++) {		
		startTime = msg[6+i*3];
		endTime = msg[7+i*3];		
		//printf_P(PSTR("\n + SSR_ID:0x%02X, Start_Time : 0x%02X, EndTime:0x%02X"), ssrId, startTime, endTime);		
	}

	//addScheduleTable(timeCnt, &msg[4], length-4);
	plcs_NCP_LC100_SendSchedule(nodeId, timeCnt, &msg[5], length-5);
}

void plcs_NCP_SERVER_HandleUpdateSchedule(uint8 msg[], int length){
	uint16 nodeId;
	uint8 timeCnt;
	uint8 i;
	uint8 ssrId;
	uint8 startTime;
	uint8 endTime;

	nodeId = msg[2] << 8 | msg[3];
	timeCnt = msg[4];
	ssrId = msg[5];
	//printf_P(PSTR("\nPLCS_NCP_UPDATE_SCHEDULE (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);

	for(i=0; i < timeCnt; i++) {		
		startTime = msg[6+i*3];
		endTime = msg[7+i*3];		
		//printf_P(PSTR("\n + SSR_ID:0x%02X, Start_Time : 0x%02X, EndTime:0x%02X"), ssrId, startTime, endTime);		
	}

	plcs_NCP_LC100_SendUpdateSchedule(nodeId, timeCnt, &msg[5], length-5);	
}

void plcs_NCP_SERVER_HandleSendMappingInfo(uint8 msg[], int length){			
	uint16 nodeId;
	uint16 sensorId;
	uint8 ssrId;
	uint8 onTime;
	uint8 offDimmingLevel;
	
	nodeId = msg[2] << 8 | msg[3];
	sensorId = msg[4] << 8 | msg[5];
	ssrId = msg[6];
	onTime = msg[7];
	offDimmingLevel = msg[8];
	
	//printf_P(PSTR("\nPLCS_NCP_SEND_MAPPING_INFO (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + NodeId : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + SensorId : 0x%04X"), sensorId);
	//printf_P(PSTR("\n + SSRId : 0x%04X"), ssrId);
	//printf_P(PSTR("\n + OnTime : 0x%04X"), onTime);
	//printf_P(PSTR("\n + offDimmingLevel : 0x%04X"), offDimmingLevel);

	plcs_NCP_LC100_SendMappingInfo(nodeId, sensorId, ssrId, onTime, offDimmingLevel);
}

void plcs_NCP_SERVER_HandleResetMappingInfo(uint8 msg[], int length){
	uint16 nodeId;		
	
	nodeId = msg[2] << 8 | msg[3];
		
	//printf_P(PSTR("\nPLCS_NCP_RESET_MAPPING_INFO (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + NodeId : 0x%04X"), nodeId);	

	plcs_NCP_LC100_ResetMappingInfo(nodeId);
}

void plcs_NCP_SERVER_HandleSendDimmerMappingInfo(uint8 msg[], int length){		
	uint16 nodeId;
	uint16 dimmerId;
	uint8 ssrId;

	nodeId = msg[2] <<8 | msg[3];
	dimmerId = msg[4] << 8 | msg[5];
	ssrId = msg[6];
	
	//printf_P(PSTR("\nPLCS_NCP_SEND_DimmerMAPPING_INFO (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + DimmerId : 0x%04X"), dimmerId);
	//printf_P(PSTR("\n + SSRId : 0x%04X"), ssrId);

	plcs_NCP_LC100_SendDimmerMappingInfo(nodeId, dimmerId, ssrId);	
}

void plcs_NCP_SERVER_HandleResetDimmerMappingInfo(uint8 msg[], int length){
	uint16 nodeId;	

	nodeId = msg[2] <<8 | msg[3];	
	
	//printf_P(PSTR("\nPLCS_NCP_SEND_DimmerMAPPING_INFO (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);	

	plcs_NCP_LC100_ResetDimmerMappingInfo(nodeId);
}

void plcs_NCP_SERVER_HandleSendOnTime(uint8 msg[], int length){
	uint16 nodeId;
	uint16 sensorId;
	uint8 onTime;

	nodeId = msg[2] << 8 | msg[3];
	sensorId = msg[4] | msg[5];
	onTime = msg[6];

	//printf_P(PSTR("\n PLCS_NCP_SEND_ONTIME (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + Sensor_Id : 0x%04X"), sensorId);
	//printf_P(PSTR("\n + OnTime : 0x%02X"), onTime);
	
	plcs_NCP_LC100_SendOnTime(nodeId, sensorId, onTime);
}

void plcs_NCP_SERVER_HandleDebugLogReq(uint8 msg[], int length){
	//printf_P(PSTR("\n PLCS_NCP_DEBUG_LOG_REQ (0x%02X)"), msg[0]);
	//debug_UpdateLog();	
	plcs_NCP_SERVER_SendDebugLogRes(debug_DebugInfo.rebootCnt, debug_DebugInfo.lastRebootTime, debug_DebugInfo.serverConnectionCnt, debug_DebugInfo.lastServerConnectionTime);
}

void plcs_NCP_SERVER_HandleDebugLogReset(uint8 msg[], int length){
	//printf_P(PSTR("\n PLCS_NCP_DEBUG_LOG_RESET (0x%02X)"), msg[0]);
	debug_Reset();	
}

void plcs_NCP_SERVER_HandleDebugLogLCReq(uint8 msg[], int length){
	uint16 nodeId;
	//printf_P(PSTR("\n PLCS_NCP_DEBUG_LOG_LC_REQ (0x%02X)"), msg[0]);
	//cntForNCPTest++;
	//printf_P(PSTR("\n Count (0x%02X)"), cntForNCPTest);
	nodeId = msg[2] << 8 | msg[3];
	plcs_NCP_LC100_DebugLogReq(nodeId);
	
}

void plcs_NCP_SERVER_HandleDebugLogLCReset(uint8 msg[], int length){
	uint16 nodeId;
	//printf_P(PSTR("\n PLCS_NCP_DEBUG_LOG_LC_RESET (0x%02X)"), msg[0]);
//cntForNCPTest=0;
	nodeId = msg[2] << 8 | msg[3];
	plcs_NCP_LC100_DebugLogReset(nodeId);
}

void plcs_NCP_SERVER_HandleSendPattern(uint8 msg[], int length){
	uint16 nodeId;
	uint8 ssrId;
	uint8 patternCnt;	

	nodeId = msg[2] << 8 | msg[3];
	ssrId = msg[4];
	patternCnt = msg[5];	

	//printf_P(PSTR("\n PLCS_NCP_SEND_PATTERN (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n nodeId (0x%02X)"), nodeId);
	//printf_P(PSTR("\n ssrId (0x%02X)"), ssrId);
	//printf_P(PSTR("\n patternCnt (0x%02X)"), patternCnt);

	plcs_NCP_LC100_SendPattern(nodeId, ssrId, patternCnt, &(msg[2]), length-2);
}

void plcs_NCP_SERVER_HandleResetPattern(uint8 msg[], int length){
	uint16 nodeId;
	uint8 ssrId;
	//printf_P(PSTR("\n PLCS_NCP_RESET_PATTERN (0x%02X)"), msg[0]);

	nodeId = msg[2] << 8 | msg[3];
	ssrId = msg[4];
	plcs_NCP_LC100_SendRetetPattern(nodeId, ssrId);
}

void plcs_NCP_SERVER_HandleSetSensingLevel(uint8 msg[], int length){
	uint16 nodeId;
	uint16 sensorId;
	uint8 level;
	nodeId = msg[2] << 8 | msg[3];
	sensorId = msg[4] << 8 | msg[5];
	level = msg[6];

	//printf_P(PSTR("\n PLCS_SNCP_SET_SENSING_LEVEL (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n Node ID (0x%02X)"), nodeId);
	//printf_P(PSTR("\n Sensor ID (0x%02X)"), sensorId);
	//printf_P(PSTR("\n Level (0x%02X)"), level);
	
	plcs_NCP_LC100_SetSensingLevel(nodeId, sensorId, level);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
////////////////////////////////////////////////////////////////////////////////////////////////////////////

void plcs_NCP_SERVER_SendStateInfoRes(uint16 nodeId, uint16 deviceType, uint8 msg[], uint8 length){
	int len = 0;
	int i=0;
	uint8 tmp_Buff[32];

	//printf_P(PSTR("\n PLCS_NCP_SEND_RES_STATE_INFO (0x%02X)"), msg[0]);
	//printf_P(PSTR("\n + Node_Id : 0x%04X"), nodeId);
	//printf_P(PSTR("\n + Device_Type : 0x%04X"), deviceType);

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RES_STATE_INFO;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = deviceType>>8;	
	tmp_Buff[len++] = deviceType;	
	for(i=0; i < length; i++)tmp_Buff[len++] = msg[i];
 
	sendToServer(tmp_Buff, len);
	MSLEEP(3);
}
	
void plcs_NCP_SERVER_SendPingReq()
{
	int len = 0;
	
	//printf_P(PSTR("\n NCP_REQ_PING"));
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_PING;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(NCP_GW_ID >> 8);
	tmp_Buff[len++] = (uint8)(NCP_GW_ID);
	tmp_Buff[len++] = NCP_VERSION;
	tmp_Buff[len++] = 0x00;
	
	sendToServer(tmp_Buff, len);
	MSLEEP(3);
}

void plcs_NCP_SERVER_SendRegisterNodeReq(uint16 nodeId, uint32 subNodeId, uint16 DeviceType, uint8 msg[], uint8 length){
	int len = 0;
	int i=0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_REGISTER_NODE;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(NCP_GW_ID >> 8);
	tmp_Buff[len++] = (uint8)(NCP_GW_ID);
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)(nodeId);
	tmp_Buff[len++] = (uint8)(DeviceType >> 8);
	tmp_Buff[len++] = (uint8)(DeviceType);	
	for(i=0; i < length; i++)
		tmp_Buff[len++] = msg[i];

	sendToServer(tmp_Buff, len);
	MSLEEP(3);
}

void plcs_NCP_SERVER_SendRegisterReq()
{
	int len = 0;
	 
	//printf_P(PSTR("\nNCP_REGISTER_REQ"));
	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUB_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_REGISTER;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(NCP_GW_ID >> 8);
	tmp_Buff[len++] = (uint8)(NCP_GW_ID);
	tmp_Buff[len++] = (uint8)(NCP_GW_TYPE >> 8);
	tmp_Buff[len++] = (uint8)(NCP_GW_TYPE);
	tmp_Buff[len++] = NCP_GW_VER;
	tmp_Buff[len++] = NCP_FW_VER;
 
	sendToServer(tmp_Buff, len);
	MSLEEP(3);
}

void plcs_NCP_SERVER_SendControlRes(uint8 nodeId, uint8 ssrId, uint8 ctrlMode){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RES_CONTROL;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = ssrId;
	tmp_Buff[len++] = ctrlMode;

	sendToServer(tmp_Buff, len);
}

void plcs_NCP_SERVER_SendPowerMeterRes(uint16 nodeId, uint32 powerMeterValue){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RES_POWER_METER;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = (uint8)(powerMeterValue >> 24);
	tmp_Buff[len++] = (uint8)(powerMeterValue >> 16);
	tmp_Buff[len++] = (uint8)(powerMeterValue >> 8);
	tmp_Buff[len++] = powerMeterValue;
	
	sendToServer(tmp_Buff, len);
}

void plcs_NCP_SERVER_SendOnTimeRes(uint16 nodeId, uint16 subNodeId, uint8 onTime){
	int len = 0;
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RES_ONTIME;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = (uint8)(subNodeId >> 8);
	tmp_Buff[len++] = subNodeId;
	tmp_Buff[len++] = onTime;

	sendToServer(tmp_Buff, len);
}

void plcs_NCP_SERVER_SendEvent(uint16 nodeId, uint8 dataFormat, uint8 msg[], uint8 length){
	int len=0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_EVENT;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = dataFormat;		

	memcpy(&tmp_Buff[len], msg, length);
	len += sizeof(uint8) * length;			
	
	sendToServer(tmp_Buff, len);
}
void plcs_NCP_SERVER_SendDimmingRes(uint16 nodeId, uint16 subNodeId, uint8 dimmingLevel){
	int len=0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RES_DIMMING;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = (uint8)(subNodeId >> 8);
	tmp_Buff[len++] = subNodeId;
	tmp_Buff[len++] = dimmingLevel;		
	
	sendToServer(tmp_Buff, len);
}

void plcs_NCP_SERVER_SendDimmer2SSRMappingInfoRes(uint16 nodeId, uint16 dimmerId, uint8 ssrId, uint8 result){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_DIMMER2SSR_MAPPING_INFO_RES;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = (uint8)(dimmerId >> 8);
	tmp_Buff[len++] = (uint8)dimmerId;	
	tmp_Buff[len++] = (uint8)ssrId;	
	tmp_Buff[len++] = (uint8)(result);

	sendToServer(tmp_Buff, len);
}

void plcs_NCP_SERVER_SendMappingInfoRes(uint16 nodeId, uint16 sensorId, uint8 ssrId, uint8 onTime, uint8 offDimmingLevel, uint8 result){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_MAPPING_INFO_RES;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = (uint8)(sensorId >> 8);
	tmp_Buff[len++] = (uint8)sensorId;	
	tmp_Buff[len++] = (uint8)ssrId;	
	tmp_Buff[len++] = (uint8)onTime;	
	tmp_Buff[len++] = (uint8)offDimmingLevel;

	tmp_Buff[len++] = (uint8)(result);

	sendToServer(tmp_Buff, len);
}

void plcs_NCP_SERVER_SendDebugLogRes(uint16 rebootCnt, uint8 lastRebootTime[], uint16 serverConnectionCnt, uint8 lastServerConnectionTime[]){
	int len = 0;
	uint8 i=0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_DEBUG_LOG_RES;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(NCP_GW_ID >> 8);
	tmp_Buff[len++] = (uint8)(NCP_GW_ID);	
	tmp_Buff[len++] = ds1307_read(6);
	tmp_Buff[len++] = ds1307_read(5);
	tmp_Buff[len++] = ds1307_read(4);
	tmp_Buff[len++] = ds1307_read(2);
	tmp_Buff[len++] = ds1307_read(1);
	tmp_Buff[len++] = ds1307_read(0);
	tmp_Buff[len++] = (uint8)(rebootCnt >> 8);
	tmp_Buff[len++] = (uint8)rebootCnt;	
	for(i=0; i < 5; i++)
		tmp_Buff[len++] = lastRebootTime[i];
	tmp_Buff[len++] = (uint8)(serverConnectionCnt>>8);
	tmp_Buff[len++] = (uint8)(serverConnectionCnt);
	for(i=0; i < 5; i++)
		tmp_Buff[len++] = lastServerConnectionTime[i];	

	sendToServer(tmp_Buff, len);
}

void plcs_NCP_SERVER_SendDebugLogLCRes(uint16 nodeId, uint8 msg[], uint8 length){
	int len=0;
	uint8 i=0;
	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_DEBUG_LOG_LC_RES;
	tmp_Buff[len++] = 0x00;
	
	tmp_Buff[len++] = (uint8)(NCP_GW_ID >> 8);
	tmp_Buff[len++] = (uint8)(NCP_GW_ID);

	for(i=0; i < length; i++)
		tmp_Buff[len++] = msg[i];
	
	sendToServer(tmp_Buff, len);
}

void plcs_NCP_SERVER_SendPatternRes(uint16 nodeId, uint8 ssrId, uint8 result){
	int len = 0;
	uint8 i=0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_PATTERN_RES;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = ssrId;
	tmp_Buff[len++] = result;
	
	sendToServer(tmp_Buff, len);
}

void plcs_NCP_SERVER_SendSetSensingLevelRes(uint16 nodeId, uint16 sensorId, uint8 level){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_SNCP_SET_SENSING_LEVEL_RES;	
	tmp_Buff[len++] = 0x00;	
	tmp_Buff[len++] = nodeId>>8;
	tmp_Buff[len++] = nodeId;	
	tmp_Buff[len++] = sensorId >>8;
	tmp_Buff[len++] = sensorId;
	tmp_Buff[len++] = level;
	
	sendToServer(tmp_Buff, len);
}

uint8 plcs_NCP_SERVER_GetNetworkState(){
	return plcs_NCP_SERVER_NetState;
}

uint16 plcs_NCP_SERVER_GetPingIntervalTime(){
	return plcs_NCP_SERVER_PingInterval;
}

uint8 plcs_NCP_SERVER_GetPingCount(){
	return plcs_NCP_SERVER_Ping_Count;	       
}
