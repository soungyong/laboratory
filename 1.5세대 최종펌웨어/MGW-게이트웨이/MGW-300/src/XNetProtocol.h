#ifndef __XNET_PROTOCOL_H__
#define __XNET_PROTOCOL_H__


#include <string.h>
#include "../iinchip/types.h"
#include "NCProtocol.h"
#include "PLCS_NCProtocol_Server.h"
#include "Xcps.h" 
#include "Util.h"

// -- XNetMessage Handler -- // 

void XNetPacket(uint8 *msg, int buff_length);
void XNetPacketFromController(uint8 *msg, int buff_length);
void XNetPacektFromServer(uint8 *msg, int buff_length);

void sendToController(uint8 msg[], int msg_len);

void sendToServer(uint8 msg[], int msg_len);

#endif
