#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "../iinchip/types.h"

typedef struct {
	uint16 rebootCnt;
	uint8 lastRebootTime[5];
	uint16 serverConnectionCnt;
	uint8 lastServerConnectionTime[5];
}DebugInfo_st;

DebugInfo_st debug_DebugInfo;


void debug_UpdateLog();
void debug_ReadLog();
void debug_Reset();
void debug_UpdateReboot();
void debug_UpdateConnection();

#endif
