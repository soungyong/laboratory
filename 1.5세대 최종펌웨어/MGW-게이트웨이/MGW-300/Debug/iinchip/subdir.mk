################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../iinchip/socket.c \
../iinchip/w5100.c 

OBJS += \
./iinchip/socket.o \
./iinchip/w5100.o 

C_DEPS += \
./iinchip/socket.d \
./iinchip/w5100.d 


# Each subdirectory must supply rules for building sources it contributes
iinchip/%.o: ../iinchip/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega128 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


