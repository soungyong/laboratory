################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../avrx/AvrXFifo.o \
../avrx/avrx_canceltimer.o \
../avrx/avrx_canceltimermessage.o \
../avrx/avrx_eeprom.o \
../avrx/avrx_generatesinglestepinterrupt.o \
../avrx/avrx_halt.o \
../avrx/avrx_message.o \
../avrx/avrx_priority.o \
../avrx/avrx_recvmessage.o \
../avrx/avrx_reschedule.o \
../avrx/avrx_resetsemaphore.o \
../avrx/avrx_semaphores.o \
../avrx/avrx_singlestep.o \
../avrx/avrx_starttimermessage.o \
../avrx/avrx_suspend.o \
../avrx/avrx_tasking.o \
../avrx/avrx_terminate.o \
../avrx/avrx_testsemaphore.o \
../avrx/avrx_timequeue.o \
../avrx/monitor.o 

C_SRCS += \
../avrx/AvrXFifo.c 

S_UPPER_SRCS += \
../avrx/avrx_canceltimer.S \
../avrx/avrx_canceltimer_exp.S \
../avrx/avrx_canceltimermessage.S \
../avrx/avrx_eeprom.S \
../avrx/avrx_generatesinglestepinterrupt.S \
../avrx/avrx_halt.S \
../avrx/avrx_iar_vect.S \
../avrx/avrx_message.S \
../avrx/avrx_priority.S \
../avrx/avrx_recvmessage.S \
../avrx/avrx_reschedule.S \
../avrx/avrx_resetsemaphore.S \
../avrx/avrx_semaphores.S \
../avrx/avrx_singlestep.S \
../avrx/avrx_starttimermessage.S \
../avrx/avrx_suspend.S \
../avrx/avrx_tasking.S \
../avrx/avrx_terminate.S \
../avrx/avrx_testsemaphore.S \
../avrx/avrx_timequeue.S \
../avrx/avrx_timequeue_exp.S \
../avrx/monitor.S \
../avrx/serialio.S 

OBJS += \
./avrx/AvrXFifo.o \
./avrx/avrx_canceltimer.o \
./avrx/avrx_canceltimer_exp.o \
./avrx/avrx_canceltimermessage.o \
./avrx/avrx_eeprom.o \
./avrx/avrx_generatesinglestepinterrupt.o \
./avrx/avrx_halt.o \
./avrx/avrx_iar_vect.o \
./avrx/avrx_message.o \
./avrx/avrx_priority.o \
./avrx/avrx_recvmessage.o \
./avrx/avrx_reschedule.o \
./avrx/avrx_resetsemaphore.o \
./avrx/avrx_semaphores.o \
./avrx/avrx_singlestep.o \
./avrx/avrx_starttimermessage.o \
./avrx/avrx_suspend.o \
./avrx/avrx_tasking.o \
./avrx/avrx_terminate.o \
./avrx/avrx_testsemaphore.o \
./avrx/avrx_timequeue.o \
./avrx/avrx_timequeue_exp.o \
./avrx/monitor.o \
./avrx/serialio.o 

C_DEPS += \
./avrx/AvrXFifo.d 

S_UPPER_DEPS += \
./avrx/avrx_canceltimer.d \
./avrx/avrx_canceltimer_exp.d \
./avrx/avrx_canceltimermessage.d \
./avrx/avrx_eeprom.d \
./avrx/avrx_generatesinglestepinterrupt.d \
./avrx/avrx_halt.d \
./avrx/avrx_iar_vect.d \
./avrx/avrx_message.d \
./avrx/avrx_priority.d \
./avrx/avrx_recvmessage.d \
./avrx/avrx_reschedule.d \
./avrx/avrx_resetsemaphore.d \
./avrx/avrx_semaphores.d \
./avrx/avrx_singlestep.d \
./avrx/avrx_starttimermessage.d \
./avrx/avrx_suspend.d \
./avrx/avrx_tasking.d \
./avrx/avrx_terminate.d \
./avrx/avrx_testsemaphore.d \
./avrx/avrx_timequeue.d \
./avrx/avrx_timequeue_exp.d \
./avrx/monitor.d \
./avrx/serialio.d 


# Each subdirectory must supply rules for building sources it contributes
avrx/%.o: ../avrx/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -O0 -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega128 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

avrx/%.o: ../avrx/%.S
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Assembler'
	avr-gcc -x assembler-with-cpp -g2 -gstabs -mmcu=atmega128 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


