################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/DS1307_i2c.c \
../src/Debug.c \
../src/DeviceInfo.c \
../src/NCProtocol.c \
../src/NCProtocol_old.c \
../src/PLCS_NCProtocol.c \
../src/PLCS_NCProtocol_LC100.c \
../src/PLCS_NCProtocol_Server.c \
../src/ScheduleTime.c \
../src/Timer.c \
../src/Uart.c \
../src/Xcps.c \
../src/Xcpsnet.c \
../src/main.c \
../src/xnetprotocol.c 

OBJS += \
./src/DS1307_i2c.o \
./src/Debug.o \
./src/DeviceInfo.o \
./src/NCProtocol.o \
./src/NCProtocol_old.o \
./src/PLCS_NCProtocol.o \
./src/PLCS_NCProtocol_LC100.o \
./src/PLCS_NCProtocol_Server.o \
./src/ScheduleTime.o \
./src/Timer.o \
./src/Uart.o \
./src/Xcps.o \
./src/Xcpsnet.o \
./src/main.o \
./src/xnetprotocol.o 

C_DEPS += \
./src/DS1307_i2c.d \
./src/Debug.d \
./src/DeviceInfo.d \
./src/NCProtocol.d \
./src/NCProtocol_old.d \
./src/PLCS_NCProtocol.d \
./src/PLCS_NCProtocol_LC100.d \
./src/PLCS_NCProtocol_Server.d \
./src/ScheduleTime.d \
./src/Timer.d \
./src/Uart.d \
./src/Xcps.d \
./src/Xcpsnet.d \
./src/main.d \
./src/xnetprotocol.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega128 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


