################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
O_SRCS += \
../src/CNCProtocol.o \
../src/DeviceInfo.o \
../src/MappingTable.o \
../src/NCProtocol.o \
../src/ScheduleTime.o \
../src/Timer.o \
../src/Uart.o \
../src/XNetProtocol.o \
../src/Xcps.o \
../src/ZRMProtocol.o \
../src/main.o 

C_SRCS += \
../src/CNCProtocol.c \
../src/DeviceInfo.c \
../src/DimmerProtocol.c \
../src/MappingTable.c \
../src/NCProtocol.c \
../src/PLCS_NCProtocol.c \
../src/PatternBasedControl.c \
../src/RadarSensorProtocol.c \
../src/SSRContol.c \
../src/ScheduleTime.c \
../src/TestDeviceProtocol.c \
../src/Timer.c \
../src/Uart.c \
../src/XNetProtocol.c \
../src/Xcps.c \
../src/ZRMProtocol.c \
../src/debug.c \
../src/main.c \
../src/rotary.c 

OBJS += \
./src/CNCProtocol.o \
./src/DeviceInfo.o \
./src/DimmerProtocol.o \
./src/MappingTable.o \
./src/NCProtocol.o \
./src/PLCS_NCProtocol.o \
./src/PatternBasedControl.o \
./src/RadarSensorProtocol.o \
./src/SSRContol.o \
./src/ScheduleTime.o \
./src/TestDeviceProtocol.o \
./src/Timer.o \
./src/Uart.o \
./src/XNetProtocol.o \
./src/Xcps.o \
./src/ZRMProtocol.o \
./src/debug.o \
./src/main.o \
./src/rotary.o 

C_DEPS += \
./src/CNCProtocol.d \
./src/DeviceInfo.d \
./src/DimmerProtocol.d \
./src/MappingTable.d \
./src/NCProtocol.d \
./src/PLCS_NCProtocol.d \
./src/PatternBasedControl.d \
./src/RadarSensorProtocol.d \
./src/SSRContol.d \
./src/ScheduleTime.d \
./src/TestDeviceProtocol.d \
./src/Timer.d \
./src/Uart.d \
./src/XNetProtocol.d \
./src/Xcps.d \
./src/ZRMProtocol.d \
./src/debug.d \
./src/main.d \
./src/rotary.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: AVR Compiler'
	avr-gcc -Wall -g2 -gstabs -Os -fpack-struct -fshort-enums -std=gnu99 -funsigned-char -funsigned-bitfields -mmcu=atmega128 -DF_CPU=16000000UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


