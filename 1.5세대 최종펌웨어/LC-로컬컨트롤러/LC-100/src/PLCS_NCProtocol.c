#include "PLCS_NCProtocol.h"

 
#include "NCProtocol.h"
#include "SSRControl.h"
#include "RadarSensorProtocol.h"
#include "debug.h"
#include "PatternBasedControl.h"

// --------------------------------------------------------------------------- //
device_Info tmp_DevInfo_D;
device_Info *tmp_DevInfo_P;

SensorSSRMappingInfo_st *SSRTable;
// --------------------------------------------------------------------------- //

uint8 tmp_Buff[32];
uint16 count=0;

void plcs_NCP_ProcessMessage(uint8 msg[], int length){
	uint16 zigbee_Id;

	zigbee_Id = msg[2] << 8 | msg[3];

	switch (msg[0]) {						// Msg Type of NCP	
		case NCP_REQ_PING:
			ncp_ProcessPingReq(zigbee_Id, msg, length);			
			break;		
		case NCP_RES_PING:
			ncp_ProcessPingRes(zigbee_Id, msg, length);			
			break;		
		case NCP_REQ_REGISTER_BROADCAST:
			ncp_ProcessRegisterBoradcastReq(zigbee_Id, msg, length);
			break;
		case NCP_ASSIGN_TOKEN:
			ncp_ProcessAssignToken(zigbee_Id, msg, length);
			break;
		case NCP_RES_TOKEN_RELEASE:
			ncp_ProcessReleaseTokenRes(zigbee_Id, msg, length);
			break;	
		case NCP_REQ_REGISTER:			
			ncp_ProcessRegisterReq(zigbee_Id, msg, length);		
			break;		
		case NCP_RES_REGISTER:			
			ncp_ProcessRegisterRes(zigbee_Id, msg, length);		
			break;				
		case PLCS_NCP_REQ_CONTROL:
			plcs_NCP_HandleControlReq(msg, length);
			break;
		case PLCS_NCP_REQ_DIMMING:
			plcs_NCP_HandleDimminReq(msg, length);
			break;		
		case PLCS_NCP_REQ_POWER_METER:
			plcs_NCP_PowerMeterReq(msg, length);			
			break;
		case PLCS_NCP_UPDATE_POWER_MERTER:
			plcs_NCP_UpdatePowerMeter(msg, length);			
			break;
		case PLCS_NCP_REQ_STATE_INFO:
			plcs_NCP_StateInfo(msg, length);
			break;
		case PLCS_NCP_SEND_SCHEDULE:				// Add Schedule Mapping Table
			plcs_NCP_SendSchedule(msg, length);			
			break;
		case PLCS_NCP_UPDATE_SCHEDULE:				// Update Schedule Mapping Table
			plcs_NCP_UpdateSchedule(msg, length);			
			break;
		case PLCS_NCP_SEND_MAPPING_INFO:
			plcs_NCP_SendMappingInfo(msg, length);			
			break;
		case PLCS_NCP_RESET_MAPPING_INFO:
			plcs_NCP_HandleResetMappingInfo(msg, length);			
			break;
		case PLCS_NCP_SEND_DIMMER2SSR_MAPPING_INFO:
			plcs_NCP_SendDimmer2SSRMappingInfo(msg, length);
			break;				
		case PLCS_NCP_RESET_DIMMER_MAPPING_INFO:
			plcs_NCP_HandleResetDimmer2SSRMappingInfo(msg, length);
			break;	
		case PLCS_NCP_SEND_ON_TIME:
			plcs_NCP_SendOnTime(msg, length);			
			break;		
		case PLCS_NCP_DEBUG_LOG_LC_REQ:
			plcs_NCP_HandleDebugLogLCReq(msg, length);
			break;
		case PLCS_NCP_DEBUG_LOG_LC_RESET: 
			plcs_NCP_HandleDebugLogLCReset(msg, length);
			break;		
		case PLCS_NCP_SEND_PATTERN:
			plcs_NCP_HandleSendPattern(msg, length);
			break;
		case PLCS_NCP_RESET_PATTERN:
			plcs_NCP_HandleResetPattern(msg, length);
			break;	
		case PLCS_SNCP_SET_SENSING_LEVEL:
			plcs_NCP_HandleSetSensingLevel(msg, length);
			break;
		case PLCS_NCP_SEND_EVENT_NEIGHBOR:		
			plcs_NCP_HandleEventNeigbor(msg, length);
			break;
		default:
			break;		
	}
}

void plcs_NCP_HandleControlReq(uint8 msg[], int length){
	uint8 i = 0;
	uint16 zigbee_Id;
	int ssrId;
	uint8 dimmingLevel;
	uint8 ctrlMode;
	uint8 isPattern=0;	
	
	zigbee_Id = msg[2] << 8 | msg[3];
	ssrId = (int)msg[4];
	ctrlMode = msg[5];
	dimmingLevel = msg[6];
	if(ctrlMode==PATTERN) isPattern=1;

#ifdef DEBUG_ENABLE				
	printf_P(PSTR("\plcs_NCP_HandleControlReq (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Dst_ID : 0x%04X"), zigbee_Id);
	printf_P(PSTR("\n + ssrId : 0x%02X"), ssrId);
	printf_P(PSTR("\n + ctrlMode : 0x%02X"), ctrlMode);
	printf_P(PSTR("\n + Dimming Value : 0x%02X"), dimmingLevel);
#endif

	handleDeviceControl(zigbee_Id, ssrId, dimmingLevel, ctrlMode, isPattern);
	
	plcs_NCP_SendControlRes(zigbee_Id, ssrId, ctrlMode);
}

void plcs_NCP_HandleDimminReq(uint8 msg[], int length){
	uint16 nodeId;
	uint16 dimmerId;
	uint8 dimmingLevel;

	nodeId = msg[2] << 8 | msg[3];
	dimmerId = msg[4] << msg[5];	
	dimmingLevel = msg[6];

#ifdef DEBUG_ENABLE				
	printf_P(PSTR("\nplcs_REQ_DIMMING (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Dst_ID : 0x%04X"), dimmerId);
	printf_P(PSTR("\n + Dimming Value : 0x%02X"), dimmingLevel);
#endif
	dimmer_SendReqDimming( dimmerId, dimmer_DimmingConvert(dimmingLevel));
	
	plcs_NCP_SendDimmingRes(nodeId, dimmerId, dimmingLevel);
}
void plcs_NCP_PowerMeterReq(uint8 msg[], int length){	
	uint16 nodeId;
	
	nodeId = msg[2] << 8 | msg[3];

#ifdef DEBUG_ENABLE				
	printf_P(PSTR("\nplcs_REQ_POWER_METER (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Zigbee_ID : 0x%04X"), nodeId);
#endif				

	plcs_NCP_SendPowerMeterRes(nodeId, g_PowerMeterValue);
}


void plcs_NCP_UpdatePowerMeter(uint8 msg[], int length){
	uint16 nodeId;
	uint32 powerMeterValue;

	nodeId = msg[2] << 8 | msg[3];
	powerMeterValue = ((uint32)msg[4]) << 24 |((uint32)msg[5]) << 16 | ((uint32)msg[6]) << 8 | ((uint32)msg[7]);

#ifdef DEBUG_ENABLE				
	printf_P(PSTR("\nplcs_UPDATE_POWER_MERTER (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Zigbee_ID : 0x%04X"), nodeId);
	printf_P(PSTR("\n + Power : 0x%08X"), powerMeterValue);
#endif
	// Write to EEPROM
	if(powerMeterValue==0x00)
		plcs_NCP_PowerMeterReq(msg, length);
	else if(powerMeterValue == 0xffffffff)  {
		eeprom_write_dword((uint32_t*)POWER_METER_VALUE, 0);
		g_PowerMeterValue = 0;
	}
	else{
		eeprom_write_dword((uint32_t*)POWER_METER_VALUE, powerMeterValue);
		g_PowerMeterValue = powerMeterValue;
	}
}

void plcs_NCP_StateInfo(uint8 msg[], int length){	
	uint16 nodeId;
	uint16 deviceType;
	uint16 subNodeId;

	nodeId = msg[2] << 8 | msg[3];
	deviceType = (uint16)((msg[4] << 8) | msg[5]);
#ifdef DEBUG_ENABLE				
	printf_P(PSTR("\nplcs_REQ_STATE_INFO (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Zigbee_ID : 0x%04X"), nodeId);
	printf_P(PSTR("\n + Device_Type : 0x%04X"), deviceType);
#endif
	
	switch(deviceType)
	{
		case PLCS_LC_100Z_TYPE:									
			break;
		case PLCS_ZDIMMER_TYPE:
		case PLCS_ZSENSOR_TYPE:										// Send to Sensor
			subNodeId = (uint16)((msg[6] << 8) | msg[7]);
			break;
		default:
			break;
	}
	plcs_NCP_SendStateInfoRes(nodeId, deviceType, subNodeId);
}

void plcs_NCP_HandleDebugLogLCReq(uint8 msg[], int length){
	uint16 nodeId;	
	nodeId = msg[2] << 8 | msg[3];
	//plcs_NCP_SendEvent(tmp_zrmp.zrm_Addr, 0, 0x01);	
	plcs_NCP_SendDebugLogLCRes(nodeId, debug_DebugInfo.rebootCnt, debug_DebugInfo.lastRebootTime, debug_DebugInfo.serverConnectionCnt, debug_DebugInfo.lastServerConnectionTime);

}

void plcs_NCP_HandleDebugLogLCReset(uint8 msg[], int length){
	debug_Reset();
	count=0;
}

void plcs_NCP_HandleSendPattern(uint8 msg[], int length){
	uint16 nodeId;
	uint8 ssrId;
	uint8 patternCnt;	
	uint8 i=0;
	uint8 result=0;

	nodeId = msg[2] << 8 | msg[3];
	ssrId = msg[4];
	patternCnt = msg[5];	

	for(i=0; i < patternCnt; i++)
		if(addPatternInfoToTable(ssrId, msg[6+i*2], msg[7+i*2]))
			result=1;
	
	plcs_NCP_SendPatternRes(nodeId, ssrId, result);
}
void plcs_NCP_HandleResetPattern(uint8 msg[], int length){
	uint16 nodeId;
	uint8 ssrId;

	nodeId = msg[2] << 8 | msg[3];
	ssrId = msg[4];	

	resetPatternTable(ssrId);
}

void plcs_NCP_HandleSetSensingLevel(uint8 msg[], int length){
	uint16 nodeId;
	uint16 sensorId;
	uint8 level;
	nodeId = msg[2] << 8 | msg[3];
	sensorId = msg[4] << 8 | msg[5];
	level = msg[6];
	
	RadarSensor_SendSetSensingLevel(sensorId, level);
	//plcs_NCP_LC100_SetSensingLevel(nodeId, sensorId, level);
}

void plcs_NCP_HandleEventNeigbor(uint8 msg[], int length){
	uint16 i=0;
	uint16 nodeId = msg[2] << 8 | msg[3];
	uint16 sensorId = msg[4] << 8 | msg[5];

	for(i=0; i < sensorSSRMappingTable.size; i++) {
		if(sensorSSRMappingTable.mappingInfo[i].sensorId == sensorId){
			if(tmp_SSRInfo.expirationTime[sensorSSRMappingTable.mappingInfo[i].ssrId] < sensorSSRMappingTable.mappingInfo[i].onTime || tmp_SSRInfo.expirationTime[sensorSSRMappingTable.mappingInfo[i].ssrId]==0xff){
				tmp_SSRInfo.timerCnt[sensorSSRMappingTable.mappingInfo[i].ssrId]=0;
				tmp_SSRInfo.expirationTime[sensorSSRMappingTable.mappingInfo[i].ssrId] = sensorSSRMappingTable.mappingInfo[i].onTime;
				tmp_SSRInfo.dimming_level[sensorSSRMappingTable.mappingInfo[i].ssrId] = sensorSSRMappingTable.mappingInfo[i].offDimmingLevel;
			}else {
				tmp_SSRInfo.timerCnt[sensorSSRMappingTable.mappingInfo[i].ssrId]=0;
			}
		}
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
/////////////////////////////////////////////////////////////////////////////////////////////////////////

void plcs_NCP_SendSchedule(uint8 msg[], int length){
	uint8 i;
	uint16 nodeId;
	uint8 ssrId;
	uint8 timeCount;
	uint8 startTime;
	uint8 endTime;
	
	nodeId = msg[2] << 8 | msg[3];
	timeCount = msg[4];
	ssrId = msg[5];
	
	for(i=0; i < timeCount; i++) {		
		startTime = msg[6+i*3];
		endTime = msg[7+i*3];
		
		addScheduleTable(ssrId, startTime, endTime);
	}	
	npc_SendTimeInfoRes(nodeId, Hour, Min);
}
void plcs_NCP_UpdateSchedule(uint8 msg[], int length){
	uint8 i;
	uint16 nodeId;
	uint8 ssrId;
	uint8 timeCount;
	uint8 startTime;
	uint8 endTime;
	
	nodeId = msg[2] << 8 | msg[3];
	timeCount = msg[4];
	ssrId = msg[5];			

	resetScheduleTimeTable(ssrId);	
	
	for(i=0; i < timeCount; i++) {
		startTime = msg[6+i*3];
		endTime = msg[7+i*3];		
		addScheduleTable(ssrId, startTime, endTime);
	}
}

void plcs_NCP_SendMappingInfo(uint8 msg[], int length){
	uint16 nodeId;
	uint16 sensorId;
	uint8 ssrId;
	uint8 onTime;
	uint8 offDimmingLevel;
	uint8 result=0;

	nodeId = msg[2] << 8 | msg[3];
	sensorId = (uint16)((msg[4] << 8) | msg[5]);
	ssrId = msg[6];
	onTime = msg[7];
	offDimmingLevel = msg[8];

	result = updateMappingTable(sensorId, ssrId, onTime, offDimmingLevel);
	if(result==1)
		result = 0;
	else if(result==2)
		result = 2;
	else 
		result = 0x01;

	plcs_NCP_SendMappingInfoRes(nodeId, sensorId, ssrId, onTime, offDimmingLevel, result);		
}

void plcs_NCP_SendDimmer2SSRMappingInfo(uint8 msg[], int length){
	uint16 nodeId;	
	uint16 dimmerId;				
	uint8 ssrId;
	uint8 result=0;
	
	nodeId = msg[2] << 8 | msg[3];	
	dimmerId = (uint16)((msg[4] << 8) | msg[5]);
	ssrId = msg[6];	
				

	if(updateDimmerMappingTable(dimmerId, ssrId))
		result = 0x00;
	else
		result = 0x02;

	plcs_NCP_SendDimmer2SSRMappingInfoRes(nodeId, dimmerId, ssrId, result);
}


void plcs_NCP_SendOnTime(uint8 msg[], int length){
	uint16 nodeId;
	uint16 sensorId;
	uint8 onTime;

	nodeId = msg[2] << 8 | msg[3];
	sensorId = (uint16)((msg[4] << 8) | msg[5]);
	onTime = msg[6];
#ifdef DEBUG_ENABLE				
	printf_P(PSTR("\nplcs_SEND_ON_TIME (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Dst_ID : 0x%04X"), sensorId);
	printf_P(PSTR("\n + On_Time : 0x%04X"), onTime);
#endif
	RadarSensor_SendOnTimeReq(sensorId, onTime);
}

void plcs_NCP_SendControlRes(uint16 nodeId, uint8 ssrId, uint8 ctrlMode){
	int len = 0;
	uint8 tmp_Buff[32];

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RES_CONTROL;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = ssrId;
	tmp_Buff[len++] = ctrlMode;
	
	sendServerMessage(tmp_Buff, len);
}

void plcs_NCP_SendPowerMeterRes(uint16 nodeId, uint32 powerMeterValue)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RES_POWER_METER;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = (uint8)(powerMeterValue >> 24);
	tmp_Buff[len++] = (uint8)(powerMeterValue >> 16);
	tmp_Buff[len++] = (uint8)(powerMeterValue >> 8);
	tmp_Buff[len++] = (uint8)(powerMeterValue);

	sendServerMessage(tmp_Buff, len);
}

void plcs_NCP_SendStateInfoRes(uint8 nodeId, uint16 deviceType, uint16 subNodeId)
{
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RES_STATE_INFO;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
		
	switch(deviceType) {
	case LC_100Z_TYPE :
		tmp_Buff[len++] = (uint8)(deviceType >> 8);
		tmp_Buff[len++] = (uint8)(deviceType);
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[0];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[1];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[2];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[3];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[4];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[5];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[6];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[7];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[8];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[9];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[10];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[11];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[12];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[13];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[14];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[15];
		break;
	case ZSENSOR_TYPE:
	case ZDIMMER_TYPE:
		tmp_DevInfo_P = findNodeById(subNodeId);
		if(tmp_DevInfo_P==NULL) return;
		if(tmp_DevInfo_P->deviceType!=deviceType) return;
		tmp_Buff[len++] = (uint8)(tmp_DevInfo_P->deviceType >> 8);
		tmp_Buff[len++] = (uint8)(tmp_DevInfo_P->deviceType);
		tmp_Buff[len++] = tmp_DevInfo_P->ieeeId >> 8;
		tmp_Buff[len++] = tmp_DevInfo_P->ieeeId;					
		break;	
	}

	sendServerMessage(tmp_Buff, len);
}

void plcs_NCP_SendSendONTimeRes(uint16 nodeId, uint16 subNodeId, uint8 onTime){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RES_ON_TIME;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = (uint8)(subNodeId >> 8);
	tmp_Buff[len++] = (uint8)subNodeId;	
	tmp_Buff[len++] = (uint8)(onTime);

	sendServerMessage(tmp_Buff, len);
}

void plcs_NCP_SendDimmingRes(uint16 nodeId, uint16 subNodeId, uint8 dimmingLevel){
	int len=0;
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_RES_DIMMING;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = (uint8)(subNodeId >> 8);
	tmp_Buff[len++] = subNodeId;
	tmp_Buff[len++] = dimmingLevel;		
	
	sendServerMessage(tmp_Buff, len);
}

void plcs_NCP_SendEvent(uint16 tmp_node_id, uint16 tmp_dst_ID, uint8 DataFormat)
{
	int len = 0;	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_EVENT;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(tmp_node_id >> 8);
	tmp_Buff[len++] = (uint8)tmp_node_id;
	tmp_Buff[len++] = DataFormat;
	tmp_Buff[len++] = (uint8)(tmp_dst_ID >> 8);
	tmp_Buff[len++] = (uint8)tmp_dst_ID;

	switch(DataFormat) {
	case 0x01:
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[0];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[1];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[2];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[3];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[4];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[5];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[6];
		tmp_Buff[len++] = tmp_SSRInfo.SSR_State[7];							
		break;
	case 0x02:		
		break;
	case 0x03:
		break;
	case 0x04:
		break;
	case 0x05:
		break;
	}		

	sendServerMessage(tmp_Buff, len);
}

void plcs_NCP_HandleResetMappingInfo(uint8 msg[], int length){
	sensorSSRMappingTable.size = 0;
	sensorMappingInfo_WriteToEEPRom(0);	
}

void plcs_NCP_HandleResetDimmer2SSRMappingInfo(uint8 msg[], int length){
	dimmerSSRMappingTable.size = 0;
	dimmerMappingInfo_WriteToEEPRom(0);
}

void plcs_NCP_SendDimmer2SSRMappingInfoRes(uint16 nodeId, uint16 dimmerId, uint8 ssrId, uint8 result){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_DIMMER2SSR_MAPPING_INFO_RES;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = (uint8)(dimmerId >> 8);
	tmp_Buff[len++] = (uint8)dimmerId;	
	tmp_Buff[len++] = (uint8)ssrId;	
	tmp_Buff[len++] = (uint8)(result);

	sendServerMessage(tmp_Buff, len);
}

void plcs_NCP_SendMappingInfoRes(uint16 nodeId, uint16 sensorId, uint8 ssrId, uint8 onTime, uint8 offDimmingLevel, uint8 result){
	int len = 0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_MAPPING_INFO_RES;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = (uint8)(sensorId >> 8);
	tmp_Buff[len++] = (uint8)sensorId;	
	tmp_Buff[len++] = (uint8)ssrId;	
	tmp_Buff[len++] = (uint8)onTime;	
	tmp_Buff[len++] = (uint8)offDimmingLevel;

	tmp_Buff[len++] = (uint8)(result);

	sendServerMessage(tmp_Buff, len);
}

void plcs_NCP_SendDebugLogLCRes(uint16 nodeId, uint16 rebootCnt, uint8 lastRebootTime[], uint16 serverConnectionCnt, uint8 lastServerConnectionTime[]){
	int len = 0;
	uint8 i=0;
	count++;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_DEBUG_LOG_LC_RES;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = (uint8)(rebootCnt >> 8);
	tmp_Buff[len++] = (uint8)rebootCnt;
	//tmp_Buff[len++] = (uint8)(count >> 8);
	//tmp_Buff[len++] = (uint8)count;
		
	for(i=0; i < 2; i++)
		tmp_Buff[len++] = lastRebootTime[i];
	tmp_Buff[len++] = (uint8)(serverConnectionCnt>>8);
	tmp_Buff[len++] = (uint8)(serverConnectionCnt);
	for(i=0; i < 2; i++)
		tmp_Buff[len++] = lastServerConnectionTime[i];	
	
	sendServerMessage(tmp_Buff, len);
}

void plcs_NCP_SendPatternRes(uint16 nodeId, uint8 ssrId, uint8 result){
	int len = 0;
	uint8 i=0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_SEND_PATTERN_RES;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = ssrId;
	tmp_Buff[len++] = result;
	
	sendServerMessage(tmp_Buff, len);
}

void plcs_NCP_SendSetSensingLevelRes(uint16 nodeId, uint16 sensorId, uint8 level){
	int len = 0;
	uint8 i=0;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_NCP_PROTOCOL_ID;
	tmp_Buff[len++] = PLCS_SNCP_SET_SENSING_LEVEL_RES;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8)(nodeId >> 8);
	tmp_Buff[len++] = (uint8)nodeId;
	tmp_Buff[len++] = sensorId >>8;
	tmp_Buff[len++] = sensorId;
	tmp_Buff[len++] = level;
	
	sendServerMessage(tmp_Buff, len);
}


