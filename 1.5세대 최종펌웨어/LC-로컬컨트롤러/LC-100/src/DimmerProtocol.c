#include "DimmerProtocol.h"
#include "ZRMProtocol.h"


void dimmer_SendReqDimming(uint8 ssrId, uint8 DimLEVEL)
{
	uint16 i=0;
	int len = 0;
	uint8 tmp_Buff2[10];
	device_Info* deviceInfo_P;

	for(i=0; i < dimmerSSRMappingTable.size; i++){
		if(dimmerSSRMappingTable.mappingInfo[i].ssrId == ssrId) {
			deviceInfo_P = findNodeById(dimmerSSRMappingTable.mappingInfo[i].dimmerId);
			if(deviceInfo_P!=NULL) {
				len=0;
				tmp_Buff2[len++] = NCP_PROTOCOL_ID;
				tmp_Buff2[len++] = DIMMER_PROTOCOL_ID; 
				tmp_Buff2[len++] = PLCS_NCP_REQ_DIMMING;
				tmp_Buff2[len++] = deviceInfo_P->ieeeId >> 8;
				tmp_Buff2[len++] = deviceInfo_P->ieeeId;
				tmp_Buff2[len++] = deviceInfo_P->ieeeId >> 8;
				tmp_Buff2[len++] = deviceInfo_P->ieeeId;
				tmp_Buff2[len++] = dimmer_DimmingConvert(DimLEVEL);

				sendData(deviceInfo_P->nwkAddr, tmp_Buff2, len);	
				//MSLEEP(100);
			}
		}
	}	
}

void dimmer_SendReqDimmingByNodeId(uint16 nodeId, uint8 DimLEVEL)
{
	uint16 i=0;
	int len = 0;
	uint8 tmp_Buff2[10];
	device_Info* deviceInfo_P;

	deviceInfo_P = findNodeById(nodeId);
	if(deviceInfo_P==NULL) return;

	len=0;
	tmp_Buff2[len++] = NCP_PROTOCOL_ID;
	tmp_Buff2[len++] = DIMMER_PROTOCOL_ID;
	tmp_Buff2[len++] = PLCS_NCP_REQ_DIMMING;
	tmp_Buff2[len++] = deviceInfo_P->ieeeId >> 8;
	tmp_Buff2[len++] = deviceInfo_P->ieeeId;
	tmp_Buff2[len++] = deviceInfo_P->ieeeId >> 8;
	tmp_Buff2[len++] = deviceInfo_P->ieeeId;
	tmp_Buff2[len++] = dimmer_DimmingConvert(DimLEVEL);

	sendData(deviceInfo_P->nwkAddr, tmp_Buff2, len);
}

uint8 dimmer_DimmingConvert(uint8 DimLevel)
{
	switch(DimLevel)
	{
		case 0x00:
			return 0x00;
		break;

		case 0x01:
			return 0x20;
		break;

		case 0x02:
			return 0x34;
		break;
				
		case 0x03:
			return 0x4D;
		break;
			
		case 0x04:
			return 0x67;
		break;

		case 0x05:
			return 0x80;
		break;

		case 0x06:
			return 0x9A;
		break;

		case 0x07:
			return 0xB3;
		break;

		case 0x08:
			return 0xCD;
		break;

		case 0x09:
			return 0xE6;
		break;

		case 0x0A:
			return 0xFE;
		break;

		default:
			return 0x00;
		break;
	}
}
