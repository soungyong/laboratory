#include "main.h"
#include "SSRControl.h"
#include "DimmerProtocol.h"
#include "debug.h"
#include "PatternBasedControl.h"
#include "Rotary.h"

// Avrx hw Peripheral initialization
#define CPUCLK 		16000000L     		// CPU xtal
#define TICKRATE 	1000				// AvrX timer queue Tick rate
#define TCNT0_INIT 	(CPUCLK/128/TICKRATE)

// NB: Clock divisor bits are different between classic and mega103 chips!
// NB: IAR chooses to use different CPU identifiers for their C compiler
// NB: GCC changes the IO bit names just for fun...

#define TCCR0_INIT	((1<<CS02) | (0<<CS01) | (1<<CS00))

#define	ACCESS_0WAIT			0
#define	ACCESS_1WAIT			1
#define	ACCESS_2WAIT			2
#define	ACCESS_3WAIT			3
#define	ACCESS_NUM_WAIT		ACCESS_3WAIT

device_Info tmp_DevInfo_D;

//Sensor_Table *SSRID;

uint8 Min, Hour;
uint8 NowTime;

uint8 SensorPacket[64];
uint8 GatewayPacket[64];

volatile uint32 g_PowerMeterValue = 0;

void xmem_enable(void) __attribute__ ((naked)) __attribute ((section(".init1")));

//-----------------------------------------------------------------------------
//MCU Initialize
void InitMCU() {
	PORTA = 0x00;
	DDRA = 0xFF;
	PORTC = 0x00;
	DDRC = 0xFF;
	PORTD = 0x80;
	DDRD = 0x90;
	PORTF = 0xFF;
	DDRF = 0xFF;
	PORTG = 0xFF;
	DDRG = 0xFF;
}

void resetZigbee() {
	PORTD &= ~(0x80);
	MSLEEP(100);
	PORTD |= 0x80;
}

void xmem_enable(void) {
#if (ACCESS_NUM_WAIT == ACCESS_0WAIT)
	MCUCR = 0x80;
	XMCRA=0x40;

#elif (ACCESS_NUM_WAIT == ACCESS_1WAIT)
	MCUCR = 0xC0; // MCU control regiseter : enable external ram
	XMCRA=0x40;// External Memory Control Register A :
			   // Low sector   : 0x1100 ~ 0x7FFF
			   // Upper sector : 0x8000 ~ 0xFFFF

#elif (ACCESS_NUM_WAIT == ACCESS_2WAIT )
	MCUCR = 0x80;
	XMCRA=0x42;

#elif (ACCESS_NUM_WAIT == ACCESS_3WAIT)
	MCUCR = 0x80;
	XMCRA = 0x4A;
	XMCRB = 0x80;

#else
#error "unknown atmega128 number wait type"
#endif
}

//----------------------------------------------------------------------//

void WDT_INIT() {
	MCUCSR &= ~(1 << WDRF); // WatchDog Init(Low)
	wdt_enable(WDTO_2S);
	// WatchDog Reset Time(High)
}

ISR(TIMER1_OVF_vect) {
	if (g_PowerMeterValue == 0)
		g_PowerMeterValue = eeprom_read_dword((uint32_t*) POWER_METER_VALUE);
	g_PowerMeterValue++;
	if (g_PowerMeterValue % 10000 == 0)
		eeprom_write_dword((uint32_t*) POWER_METER_VALUE, g_PowerMeterValue);

	TCNT1H = 0xFF;
	TCNT1L = 0xFF;
	TIMSK |= 0x04;
}

void Led_Task() {
	static uint8 state = 0;

	if (timer_isfired(ON_LED_TIMER_ID)) {
		wdt_reset();

		if (state == 1)
			STATE_LED_OFF();
		else
			STATE_LED_ON();

		state = !state;

		timer_clear(ON_LED_TIMER_ID);
		timer_set(ON_LED_TIMER_ID, 500);
	}
}

void ControlTask() {
	static uint8 i = 0;
	static uint8 report_Count = 0;
	static uint16 whReport_Count = 0;
	//static uint32 Register_Count = 1;
	static uint8 dimming_Count = 0;
	static uint8 prevDimmingState[16];

	if (timer_isfired(ON_TIME_COUNT_ID)) {
		report_Count++;
		if (report_Count > 20) {
			plcs_NCP_SendEvent(tmp_zrmp.zrm_Addr, 0, 0x01);
			deviceInfo_UpdateSSRInfo();
			report_Count = 0;
		}
		whReport_Count++;
		if (whReport_Count > 600) {
			if (ncp_ConnState() == NCP_NET_REGISTER)
				plcs_NCP_SendPowerMeterRes(tmp_zrmp.zrm_Addr,
						g_PowerMeterValue);
			whReport_Count = 0;
		}

		for (i = 0; i < NumOfChannels; i++) {
			if (tmp_SSRInfo.ctrlMode[i] == SENSOR) {
				if (tmp_SSRInfo.expirationTime[i] == 0x00) {
					if (tmp_SSRInfo.dimming_level[i] == 0xff) {
						SSR_Off_Index(i);
						if (dimming_Count > 10){
							prevDimmingState[i] = 0x00;
							dimmer_SendReqDimming(i, 0x00);
						}
					} else {
						SSR_On_Index(i);
						if (dimming_Count > 10){
							prevDimmingState[i] = tmp_SSRInfo.dimming_level[i];
							dimmer_SendReqDimming(i,
									tmp_SSRInfo.dimming_level[i]);
						}
					}

				} else if (tmp_SSRInfo.expirationTime[i] == 0xffff)
					SSR_On_Index(i);
				else {
					tmp_SSRInfo.timerCnt[i] = tmp_SSRInfo.timerCnt[i] + 1;
					if (tmp_SSRInfo.expirationTime[i] * 10
							<= tmp_SSRInfo.timerCnt[i]) {
						if (tmp_SSRInfo.dimming_level[i] == 0xff) {
							SSR_Off_Index(i);
						} else {
							SSR_On_Index(i);
							if (dimming_Count > 10){
								prevDimmingState[i] = tmp_SSRInfo.dimming_level[i];
								dimmer_SendReqDimming(i,
										tmp_SSRInfo.dimming_level[i]);
							}
						}
						tmp_SSRInfo.timerCnt[i] = 0;
						tmp_SSRInfo.expirationTime[i] = 0;
					} else {
						SSR_On_Index(i);
						if (dimming_Count > 10 || prevDimmingState[i]!=0x0A){
							prevDimmingState[i] = 0x0A;
							dimmer_SendReqDimming(i, 0x0A);
						}
					}
				}
			} else if (tmp_SSRInfo.ctrlMode[i] == ON) {
				if (dimming_Count > 10){
					prevDimmingState[i] = tmp_SSRInfo.dimming_level[i];
					dimmer_SendReqDimming(i, tmp_SSRInfo.dimming_level[i]);
					SSR_On_Index(i);
				}
			} else if (tmp_SSRInfo.ctrlMode[i] == OFF){
				if (dimming_Count > 10){
					prevDimmingState[i] = 0x00;
					dimmer_SendReqDimming(i, 0);
					SSR_Off_Index(i);
				}
			}
		}
		timer_clear(ON_TIME_COUNT_ID);
		timer_set(ON_TIME_COUNT_ID, 100);

		if (dimming_Count > 10)
			dimming_Count = 0;
		dimming_Count++;
	}
}

void Sensor_Task() {
	static int Recvlen = 0;
	static uint16 count = 1;
	static uint16 zigbeeHWResetCount = 0;
	static uint16 zigbeeHWResetCount_NetworkTrouble = 0;
	static uint8 i = 0;

	if (timer_isfired(ZIGBEE_PING_ID)) {
		if (count++ == 5) {
			ZRMsendPing();
			count = 0;
		}

		zigbeeHWResetCount++;
		zigbeeHWResetCount_NetworkTrouble++;

		timer_clear(ZIGBEE_PING_ID);
		timer_set(ZIGBEE_PING_ID, 1000);
	}

	if (tmp_zrmp.zrm_State == ZRM_PING_COMPLETE) {
		zigbeeHWResetCount = 0;
	} else if (zigbeeHWResetCount > 30) {
		resetZigbee();
		zigbeeHWResetCount = 0;
	}

	if (zigbeeHWResetCount_NetworkTrouble > 60) {
		resetZigbee();
		zigbeeHWResetCount_NetworkTrouble = 0;
		if (deviceInfoTable.size > 0) {
			for (i = 0; i < NumOfChannels; i++)
				if (tmp_SSRInfo.ctrlMode[i] == SENSOR)
					if (tmp_SSRInfo.expirationTime[i] != 0xffff) {
						tmp_SSRInfo.timerCnt[i] = 0;
						tmp_SSRInfo.expirationTime[i] = 100;
					}
		}
	}

	if ((Recvlen = xcps_recv_zigbee(SensorPacket, 64)) > 0) {
		uint8 buff_len = 0;
		uint16 Src_Addr;
		uint16 Dst_Addr;

		switch (SensorPacket[0]) {
		case 0x10: // Send to XNetMessage (Server <- Gateway <- Node)
			if (getState() == ZRM_CONNECT) {
				Dst_Addr = (uint16) (SensorPacket[1] << 8) | (SensorPacket[2]);
				Src_Addr = (uint16) (SensorPacket[3] << 8) | (SensorPacket[4]);

				//memcpy(&tmp_sensorData[0], , Recvlen - 5);

				buff_len = (Recvlen - 5);

				XNetHandler(Src_Addr, &SensorPacket[5], buff_len);
				zigbeeHWResetCount_NetworkTrouble = 0;
			}
			break;
		case 0x00: // Send to GMProtocols (Gateway <-> RFM)
			//memcpy(&tmp_sensorData[0], &SensorPacket[1], Recvlen - 1);
			buff_len = (Recvlen - 1);
			ZRMPMessage(&SensorPacket[1], buff_len);
			break;

		default: // Error Mesaage
			break;
		}
	}

}

void GatewayRx_Task() {
	static uint8 Recvlen;

	if ((Recvlen = xcps_recv_rs485(GatewayPacket, 64)) > 0) {
		uint16 dst_Addr;

		dst_Addr = (uint16) (GatewayPacket[4] << 8) | (GatewayPacket[5]);
		if (dst_Addr == tmp_zrmp.zrm_Addr || dst_Addr == 0xffff) {
			XNetCommand(GatewayPacket, Recvlen);
		}
	} // End of (Recvlen = xcps...)
}

void GatewayTx_Task() {
	static uint8 cnt = 0;
	if (timer_isfired(RESEND_TIMER_ID)) {
		if (hasToken())
			if (sendStackedServerMessage() == 0 && cnt > 10) {
				send_NCPReqReleaseToken();
				cnt = 0;
			}
		if (cnt < 100)
			cnt++;
		timer_clear(RESEND_TIMER_ID);
		timer_set(RESEND_TIMER_ID, 10);
	}

	if (timer_isfired(ON_REGISTER_GW_TIMER_ID)) {
		if ((ncp_ConnState() != NCP_NET_REGISTER)
				&& (getState() == ZRM_CONNECT))
			ncp_SendRegisterReq(tmp_zrmp.zrm_Addr);

		timer_clear(ON_REGISTER_GW_TIMER_ID);
		timer_set(ON_REGISTER_GW_TIMER_ID, 2500);
	}

	if (timer_isfired(ON_REGISTER_TIMER_ID)) //All Node Registartion
	{
		if ((ncp_ConnState() == NCP_NET_REGISTER)
				&& (getState() == ZRM_CONNECT)) {
			registerAllNodetoServer();

			timer_clear(ON_REGISTER_TIMER_ID);
			timer_set(ON_REGISTER_TIMER_ID, 600000);
		} else {
			timer_clear(ON_REGISTER_TIMER_ID);
			timer_set(ON_REGISTER_TIMER_ID, 10000);
		}
	}

	if (timer_isfired(NCP_PING_TIMER_ID)) {
		ncp_SendPingReq(tmp_zrmp.zrm_Addr);

		timer_clear(NCP_PING_TIMER_ID);
		timer_set(NCP_PING_TIMER_ID, 10000);
	}
}

//-----------------------------------------------------------------------------
// Main function
//-----------------------------------------------------------------------------
int main(void) {
	InitMCU();
	initRotary();
	xmem_enable();
	InitUART();
	timer_init();

	SSR_OFF(0xFF);
	MSLEEP(500);
	SSR_ON(0xFF);

	MSLEEP(2000);
	WDT_INIT();

	TCCR1A = 0x00;
	TCCR1B = 0x06;
	TCNT1H = 0xFF;
	TCNT1L = 0xFF;

	TIMSK |= 0x04;

	initScheduleTimeTable();
	initDeviceTable();
	initMappingTable();
	initDimmerMappingTable();
	initPatternTable();

	debug_ReadLog();
	debug_UpdateReboot();

	xcps_init_zigbee(USART1_Receive, USART1_Transmit);
	xcps_init_rs485(USART0_Receive, USART0_Transmit);

	//ZRMSendSetPreconfig(0x13);
	//MSLEEP(100);
	//ZRMSendReset();
	//MSLEEP(1000);

	timer_set(ON_LED_TIMER_ID, 500);
	//timer_set(TIME_COUNT_ID, (TIME_COUNT * 60000));
	//timer_set(PATTERN_COUNT_ID, (1000 * 60));
	//timer_set(SCHEDULE_COUNT_ID, (1000 * 60));
	timer_set(ON_TIME_COUNT_ID, 2000);
	timer_set(ON_REGISTER_GW_TIMER_ID, 5000);
	timer_set(RESEND_TIMER_ID, 10);
	timer_set(ZIGBEE_PING_ID, 1000);
	timer_set(ON_REGISTER_TIMER_ID, 600000);

	while (1) {
		Led_Task();
		ControlTask();
		Sensor_Task();
		GatewayRx_Task();
		GatewayTx_Task();
	}

	return 0;
}

