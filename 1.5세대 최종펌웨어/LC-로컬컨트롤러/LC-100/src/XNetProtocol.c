
#include "XNetProtocol.h"



// --------------------------------------------------------------------------- //
#define MaxMessageListSize 50
uint8 sendMessageList[MaxMessageListSize][XCPS_MAX_PDU];
uint8 messageLength[MaxMessageListSize];
uint8 messageList_Tail=0;
uint8 messageList_Head=0;

uint8 getMessageListSize() {
	if(messageList_Tail >= messageList_Head)
		return messageList_Tail-messageList_Head;
	else
		return MaxMessageListSize+messageList_Tail - messageList_Head;
}
// --------------------------------------------------------------------------- //


void XNetHandler(uint16 zigbee_Id, uint8 msg[], int buff_length)
{	
	uint8 tmp_msg[64];
	uint8 tmp_msgLen;

	uint16 LCZ_Id;											// LC-100Z Zigbee Addr

	switch(msg[0])
	{
		case NCP_PROTOCOL_ID: 
		{
			switch(msg[1]) {				
				case RADAR_SENSOR_PROTOCOL_ID: 
				{
					processNCP_RadarSensor(zigbee_Id, &msg[2], buff_length-2);
				}break;
				case TEST_DEVICE_PROTOCOL_ID:
				{
					processNCP_TestDevice(zigbee_Id, &msg[2], buff_length-2);
				}break;
				default: 
				{
					ncp_ProcessMessage(zigbee_Id, &msg[2], buff_length-2);
					break;
				}
			}			
		}break;
		/*case N2G_ZIGBEE:									// Node to Gateway
		{
			switch(msg[1])								// PID of NCP
			{
				case NCP:								// Send to.... NCP (NCProtocol.c ����), MSG From Node To Server						
					memcpy(&tmp_msg[0], &msg[2], buff_length - 2);
					tmp_msgLen = (buff_length - 2);
						
					processNCP(zigbee_Id, tmp_msg, tmp_msgLen);
					break;

				case CNCP:
					LCZ_Id = (uint16)((msg[2] << 8) | msg[3]);						
					memcpy(&tmp_msg[0], &msg[2], buff_length - 2);

					tmp_msgLen = (buff_length - 2);
						
					Zigbee_CNCP(LCZ_Id, tmp_msg, tmp_msgLen);
					break;
				default:
					// ERROR
					break;
			}
		}break;
		case N2S_ETHERNET:
		{				
		}break;
		*/
		default:
			break;
	}
}


void XNetCommand(uint8 msg[], int buff_length)
{
	uint8 tmp_gatewayData[64];
	uint8 buff_len = 0;

	switch(msg[0])
	{
		case NCP_PROTOCOL_ID:
			{				
				memcpy(&tmp_gatewayData[0], &msg[2], buff_length-2);
				buff_len = buff_length-2;

				switch(msg[1]) {				
					case PLCS_NCP_PROTOCOL_ID: 					
						plcs_NCP_ProcessMessage(&tmp_gatewayData[0], buff_len);
						break;	
					default:
						ncp_ProcessMessage(tmp_gatewayData[2] << 8 | tmp_gatewayData[3], &tmp_gatewayData[0], buff_len);
						break;
				}							
			}
			break;

		default:
			break;
	}
}


char sendStackedServerMessage() {	
	if(getMessageListSize() > 0) {			
		messageList_Head+=1;
		xcps_send_rs485(sendMessageList[messageList_Head-1], messageLength[messageList_Head-1]);					
		if(messageList_Head>=MaxMessageListSize)
			messageList_Head=0;				
		return 1;
	}	
	return 0;
}

void sendServerMessage(uint8 *msg, uint8 buff_length)
{
	uint8 i=0;
	int len = 0;		
	
	if(getMessageListSize() >= (MaxMessageListSize-2)) {
		return;
	}
	
	msg[3] = messageList_Tail;	
	memcpy(sendMessageList[messageList_Tail], msg, buff_length);	
	messageLength[messageList_Tail] = (uint8) buff_length;
	messageList_Tail+=1;
	if(messageList_Tail>=MaxMessageListSize)
		messageList_Tail=0;		
}

void sendServerMessageWithoutQueue(uint8 *msg, int buff_length) {
	xcps_send_rs485(msg, buff_length);
}

//end of file

