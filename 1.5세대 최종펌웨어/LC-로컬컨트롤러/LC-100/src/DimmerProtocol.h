#ifndef __DIMMERPROTOCOL_H
#define __DIMMERPROTOCOL_H

#include "Util.h"
#include "NCProtocol.h"

#define DIMMER_PROTOCOL_ID 0x11

void dimmer_SendReqDimming(uint8 ssrId, uint8 DimLEVEL);
void dimmer_SendReqDimmingByNodeId(uint16 nodeId, uint8 DimLEVEL);
uint8 dimmer_DimmingConvert(uint8 DimLevel);


#endif
