#include "RadarSensorProtocol.h"
#include "MappingTable.h"
#include "ZRMProtocol.h"

void processNCP_TestDevice(uint16 srcNetAddr, uint8 msg[], int length) {
	switch (msg[0]) {
	case NCP_REQ_REGISTER:
		ncp_ProcessRegisterReq(srcNetAddr, msg, length);
		break;
	case NCP_REQ_PING:
		ncp_ProcessPingReq(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_FIRMWARE_VERSION_REQ:
		processNCP_TestDevice_FirmwareVersion_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_CONTROLINFO_REQ:
		processNCP_TestDevice_ControlInfo_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_CONTROL_REQ:
		processNCP_TestDevice_Control_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DEVICELIST_SIZE_REQ:
		processNCP_TestDevice_DeviceList_Size_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DEVICEINFO_REQ:
		processNCP_TestDevice_DeviceInfo_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_SENSOR_LEVEL_SET:
		processNCP_TestDevice_SensorLevel_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_DMX_CONTROL_REQ:
		processNCP_TestDevice_DMX_Control_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_SENSOR_MAPPING_SIZE_REQ:
		processNCP_TestDevice_SensorMappingSize_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DIMMER_MAPPING_SIZE_REQ:
		processNCP_TestDevice_DimmerMappingSize_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_SENSOR_MAPPING_INFO_REQ:
		processNCP_TestDevice_SensorMappingInfo_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DIMMER_MAPPING_INFO_REQ:
		processNCP_TestDevice_DimmerMappingInfo_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_SENSOR_MAPPING_REQ:
		processNCP_TestDevice_SensorMapping_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DIMMER_MAPPING_REQ:
		processNCP_TestDevice_DimmerMapping_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_SENSOR_MAPPING_RESET_REQ:
		processNCP_TestDevice_SensorMappingReset_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_DIMMER_MAPPING_RESET_REQ:
		processNCP_TestDevice_DimmerMappingReset_Req(srcNetAddr, msg, length);
		break;
	case TEST_DEVICE_LC_POWERMETER_REQ:
		processNCP_TestDevice_Powermeter_Req(srcNetAddr, msg, length);
		break;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////
//Handle Message Function
////////////////////////////////////////////////////////////////////////////////////////////////////

void processNCP_TestDevice_FirmwareVersion_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;
	uint8 version = 0;
	nodeId = msg[1] << 8 | msg[2];

	version = PLCS_LC_FW_VER;

	TestDevice_FirmwareVersion_Res(nodeId, version);
}

void processNCP_TestDevice_ControlInfo_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;
	nodeId = msg[1] << 8 | msg[2];

	TestDevice_ControlInfo_Res(nodeId);
}

void processNCP_TestDevice_Control_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;
	uint8 circuitId = 0;
	uint8 controlState = 0;

	nodeId = msg[1] << 8 | msg[2];
	circuitId = msg[3];
	controlState = msg[4];

	handleDeviceControl(nodeId, circuitId, 0xff, controlState, 0);

	TestDevice_Control_Res(nodeId, 0x00);
}

void processNCP_TestDevice_DeviceList_Size_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;
	uint8 numOfDevice = 0;
	nodeId = msg[1] << 8 | msg[2];

	numOfDevice = deviceInfoTable.size;

	TestDevice_DeviceList_Size_Res(nodeId, numOfDevice);
}
void processNCP_TestDevice_DeviceInfo_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;
	uint8 index = 0;

	uint16 deviceId = 0;
	uint16 deviceType = 0;
	uint8 deviceVersion = 0;
	device_Info *device_P;

	nodeId = msg[1] << 8 | msg[2];
	index = msg[3];

	if (index >= deviceInfoTable.size)
		return;

	device_P = &(deviceInfoTable.deviceInfo[index]);
	deviceId = device_P->ieeeId;
	deviceType = device_P->deviceType;
	deviceVersion = device_P->fwVersion;

	TestDevice_DeviceInfo_Res(nodeId, deviceId, deviceType, deviceVersion);

}
void processNCP_TestDevice_SensorLevel_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;

	uint16 deviceId = 0;
	uint8 level = 0;
	device_Info *device_P;

	nodeId = msg[1] << 8 | msg[2];
	deviceId = msg[3] << 8 | msg[4];
	level = msg[5];

	device_P = findNodeById(deviceId);

	if (device_P == NULL) {
		TestDevice_SensorLevel_Res(nodeId, 0x01);
		return;
	}

	RadarSensor_SendSetSensingLevel(deviceId, level);
	TestDevice_SensorLevel_Res(nodeId, 0x00);

}
void processNCP_TestDevice_DMX_Control_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;

	uint16 deviceId = 0;
	uint8 level = 0;
	device_Info *device_P;

	nodeId = msg[1] << 8 | msg[2];
	deviceId = msg[3] << 8 | msg[4];
	level = msg[5];

	device_P = findNodeById(deviceId);

	if (device_P == NULL) {
		TestDevice_DMX_Control_Res(nodeId, 0x01);
		return;
	}

	dimmer_SendReqDimmingByNodeId(deviceId, level);
	TestDevice_DMX_Control_Res(nodeId, 0x00);
}

void processNCP_TestDevice_SensorMappingSize_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;

	nodeId = msg[1] << 8 | msg[2];

	TestDevice_SensorMappingSize_Res(nodeId);
}

void processNCP_TestDevice_DimmerMappingSize_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;

	nodeId = msg[1] << 8 | msg[2];

	TestDevice_DimmerMappingSize_Res(nodeId);
}

void processNCP_TestDevice_SensorMappingInfo_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;
	uint8 index = 0;

	nodeId = msg[1] << 8 | msg[2];
	index = msg[3];

	TestDevice_SensorMappingInfo_Res(nodeId, index);
}

void processNCP_TestDevice_DimmerMappingInfo_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;
	uint8 index = 0;

	nodeId = msg[1] << 8 | msg[2];
	index = msg[3];

	TestDevice_DimmerMappingInfo_Res(nodeId, index);
}

void processNCP_TestDevice_SensorMapping_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;
	uint16 sensorId;
	uint8 circuitId;
	uint8 onTime;
	uint8 offDimmingLevel;

	uint8 result = 0;

	nodeId = msg[1] << 8 | msg[2];
	sensorId = msg[3] << 8 | msg[4];
	circuitId = msg[5];
	onTime = msg[6];
	offDimmingLevel = msg[7];

	if(offDimmingLevel==0x00)
		offDimmingLevel = 0xff;

	result = updateMappingTable(sensorId, circuitId, onTime, offDimmingLevel);
	if (result == 1)
		result = 0;
	else if (result == 2)
		result = 2;
	else
		result = 0x01;

	TestDevice_SensorMapping_Res(nodeId, result);
}

void processNCP_TestDevice_DimmerMapping_Req(uint16 srcNetAddr, uint8 msg[],
		int length) {
	uint16 nodeId = 0;
	uint16 dimmerId;
	uint8 circuitId;

	uint8 result = 0;

	nodeId = msg[1] << 8 | msg[2];
	dimmerId = msg[3] << 8 | msg[4];
	circuitId = msg[5];

	if (updateDimmerMappingTable(dimmerId, circuitId))
		result = 0x00;
	else
		result = 0x02;

	TestDevice_DimmerMapping_Res(nodeId, result);
}

void processNCP_TestDevice_SensorMappingReset_Req(uint16 srcNetAddr,
		uint8 msg[], int length) {
	uint16 nodeId = 0;

	nodeId = msg[1] << 8 | msg[2];

	sensorSSRMappingTable.size = 0;
	sensorMappingInfo_WriteToEEPRom(0);

	TestDevice_SensorMappingReset_Res(nodeId, 0x00);
}

void processNCP_TestDevice_DimmerMappingReset_Req(uint16 srcNetAddr,
		uint8 msg[], int length) {
	uint16 nodeId = 0;

	nodeId = msg[1] << 8 | msg[2];

	dimmerSSRMappingTable.size = 0;
	dimmerMappingInfo_WriteToEEPRom(0);

	TestDevice_DimmerMappingReset_Res(nodeId, 0x00);
}

void processNCP_TestDevice_Powermeter_Req(uint16 srcNetAddr,
		uint8 msg[], int length) {
	uint16 nodeId = 0;

	nodeId = msg[1] << 8 | msg[2];


	TestDevice_Powermeter_Res(nodeId, g_PowerMeterValue);
}
////////////////////////////////////////////////////////////////////////////////////////////////////
//Send Function
////////////////////////////////////////////////////////////////////////////////////////////////////

void TestDevice_FirmwareVersion_Res(uint16 zigbee_Id, uint8 version) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_FIRMWARE_VERSION_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) version;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_ControlInfo_Res(uint16 zigbee_Id) {
	uint8 tmp_Buff[25];
	int len = 0;
	device_Info *device_P;
	uint8 i = 0;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_CONTROLINFO_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	for (i = 0; i < NumOfChannels; i++) {
		if (tmp_SSRInfo.ctrlMode[i] == 0xff)
			tmp_Buff[len++] = 0x0A;
		else
			tmp_Buff[len++] = tmp_SSRInfo.ctrlMode[i];
	}

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_Control_Res(uint16 zigbee_Id, uint8 result) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_CONTROL_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) (result);

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_DeviceList_Size_Res(uint16 zigbee_Id, uint8 numOfDevice) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_DEVICELIST_SIZE_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) numOfDevice;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_DeviceInfo_Res(uint16 zigbee_Id, uint16 deviceId, uint16 type,
		uint8 version) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_DEVICEINFO_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) (deviceId >> 8);
	tmp_Buff[len++] = (uint8) deviceId;
	tmp_Buff[len++] = (uint8) (type >> 8);
	tmp_Buff[len++] = (uint8) type;
	tmp_Buff[len++] = (uint8) version;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);

}
void TestDevice_SensorLevel_Res(uint16 zigbee_Id, uint8 result) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_SENSOR_LEVEL_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) result;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_DMX_Control_Res(uint16 zigbee_Id, uint8 result) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_DMX_CONTROL_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) result;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_SensorMappingSize_Res(uint16 zigbee_Id) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_SENSOR_MAPPING_SIZE_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) sensorSSRMappingTable.size;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}
void TestDevice_DimmerMappingSize_Res(uint16 zigbee_Id) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_DIMMER_MAPPING_SIZE_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) dimmerSSRMappingTable.size;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_SensorMappingInfo_Res(uint16 zigbee_Id, uint8 index) {
	uint8 tmp_Buff[20];
	int len = 0;
	device_Info *device_P;
	uint16 sensorId;
	uint8 circuitId;
	uint8 onTime;
	uint8 offDimmingLevel;

	if (index >= sensorSSRMappingTable.size)
		return;

	sensorId = sensorSSRMappingTable.mappingInfo[index].sensorId;
	circuitId = sensorSSRMappingTable.mappingInfo[index].ssrId;
	onTime = sensorSSRMappingTable.mappingInfo[index].onTime;
	offDimmingLevel = sensorSSRMappingTable.mappingInfo[index].offDimmingLevel;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_SENSOR_MAPPING_INFO_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) (sensorId >> 8);
	tmp_Buff[len++] = (uint8) (sensorId);
	tmp_Buff[len++] = (uint8) (circuitId);
	tmp_Buff[len++] = (uint8) (onTime);
	if (offDimmingLevel == 0xff)
		tmp_Buff[len++] = (uint8) (0x00);
	else
		tmp_Buff[len++] = (uint8) (offDimmingLevel);

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_DimmerMappingInfo_Res(uint16 zigbee_Id, uint8 index) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;
	uint16 dimmerId;
	uint8 circuitId;

	if (index >= dimmerSSRMappingTable.size)
		return;

	dimmerId = dimmerSSRMappingTable.mappingInfo[index].dimmerId;
	circuitId = dimmerSSRMappingTable.mappingInfo[index].ssrId;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_DIMMER_MAPPING_INFO_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) (dimmerId >> 8);
	tmp_Buff[len++] = (uint8) (dimmerId);
	tmp_Buff[len++] = (uint8) (circuitId);

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_SensorMapping_Res(uint16 zigbee_Id, uint8 result) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_SENSOR_MAPPING_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) result;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_DimmerMapping_Res(uint16 zigbee_Id, uint8 result) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_DIMMER_MAPPING_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) result;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_SensorMappingReset_Res(uint16 zigbee_Id, uint8 result) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_SENSOR_MAPPING_RESET_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) result;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_DimmerMappingReset_Res(uint16 zigbee_Id, uint8 result) {
	uint8 tmp_Buff[10];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_DIMMER_MAPPING_RESET_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) result;

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}

void TestDevice_Powermeter_Res(uint16 zigbee_Id, uint32 value) {
	uint8 tmp_Buff[20];
	int len = 0;
	device_Info *device_P;

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_PROTOCOL_ID;
	tmp_Buff[len++] = TEST_DEVICE_LC_POWERMETER_RES;
	tmp_Buff[len++] = (uint8) (zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8) (zigbee_Id);
	tmp_Buff[len++] = (uint8) (value>>24);
	tmp_Buff[len++] = (uint8) (value>>16);
	tmp_Buff[len++] = (uint8) (value>>8);
	tmp_Buff[len++] = (uint8) (value);

	device_P = findNodeById(zigbee_Id);
	if (device_P == NULL)
		return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}
