#ifndef __PATTERN_BASED_CONTROL_H__
#define __PATTERN_BASED_CONTROL_H__


#include <stdio.h>
#include <string.h>  		// strcmp(), strcpy(), strlen() 함수등 주로 문자열과 관련된 함수사용시
#include <stdlib.h>  		// malloc() 함수 사용
#include <avr/pgmspace.h>
#include <avr/eeprom.h>

#include "Util.h"
#include "Uart.h"

#define MaxNumOfPatternInfo 10
#define MaxNumOfSSR	16


typedef struct
{
	uint8 startTime;
	uint8 ctrlMode;	
}Pattern_Info;

typedef struct {
	Pattern_Info patternInfo[MaxNumOfSSR][MaxNumOfPatternInfo];
	uint8 size[MaxNumOfSSR];
}Pattern_Table_st;

Pattern_Table_st patternTable;

void initPatternTable();
int addPatternInfoToTable(uint8 ssrId, uint8 startTime, uint8 ctrlMode);
void patternTable_WriteToEEPRom(uint8 ssrId, uint8 index);
uint8 getCtrlModeOnTime(uint8 ssrId, uint8 currentTime);
void resetPatternTable(uint8 ssrId);

#endif

