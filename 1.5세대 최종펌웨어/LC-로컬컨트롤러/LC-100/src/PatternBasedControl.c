#include "PatternBasedControl.h"
#include "PLCS_NCProtocol.h"

uint8 ee_PatternInfoTableSize[MaxNumOfSSR] EEMEM;
Pattern_Info ee_PatternInfo[MaxNumOfSSR][MaxNumOfPatternInfo] EEMEM;

void initPatternTable(){
	uint8 i=0;
	uint16 j=0;
	for(i=0; i < MaxNumOfSSR; i++) {
		patternTable.size[i] = 0;
		eeprom_read_block(&(patternTable.size[i]), &ee_PatternInfoTableSize[i], sizeof(uint8));	
		if(patternTable.size[i] >= MaxNumOfPatternInfo)
			patternTable.size[i] = 0;
		
		for(j=0; j < patternTable.size[i]; j++) 
			eeprom_read_block(&(patternTable.patternInfo[i][j]), &(ee_PatternInfo[i][j]), sizeof(Pattern_Info));
	}			
}

int addPatternInfoToTable(uint8 ssrId, uint8 startTime, uint8 ctrlMode){
	if(ssrId >= MaxNumOfSSR) return 0;
	if(patternTable.size[ssrId] >=MaxNumOfPatternInfo) return 0;

	patternTable.patternInfo[ssrId][patternTable.size[ssrId]].startTime = startTime;
	patternTable.patternInfo[ssrId][patternTable.size[ssrId]].ctrlMode = ctrlMode;
	patternTable.size[ssrId]++;

	patternTable_WriteToEEPRom(ssrId, patternTable.size[ssrId]-1);
}

uint8 getCtrlModeOnTime(uint8 ssrId, uint8 currentTime){
	uint8 i=0;
	uint8 ctrlMode=ON;
	uint8 startTime=0;
	uint8 flag=0;

	if(ssrId >= MaxNumOfSSR) ctrlMode = ON;
	if(patternTable.size[ssrId]==0) 
		ctrlMode = ON;
	else {		
		//현재 시각 보다 앞쪽 패턴 제어 처리
		for(i=0; i < patternTable.size[ssrId]; i++) {
			if(patternTable.patternInfo[ssrId][i].startTime <= currentTime && patternTable.patternInfo[ssrId][i].startTime>=startTime){
				startTime = patternTable.patternInfo[ssrId][i].startTime;
				ctrlMode = patternTable.patternInfo[ssrId][i].ctrlMode;
				flag=1;
			}			
		}
		//현재 시각 보다 앞쪽 패턴이 없을 경우 다음 마지막 패턴 처리.(하루는 원형으로 반복되니까.)
		if(flag==0){
			startTime = patternTable.patternInfo[ssrId][0].startTime;
			ctrlMode = patternTable.patternInfo[ssrId][0].ctrlMode;
			for(i=1; i < patternTable.size[ssrId]; i++) {
				if(patternTable.patternInfo[ssrId][i].startTime >= currentTime && patternTable.patternInfo[ssrId][i].startTime>=startTime){
					startTime = patternTable.patternInfo[ssrId][i].startTime;
					ctrlMode = patternTable.patternInfo[ssrId][i].ctrlMode;				
				}		
			}
		}
	}
					
	return ctrlMode;
}

void resetPatternTable(uint8 ssrId){
	if(ssrId >= MaxNumOfSSR) return 0;	

	patternTable.size[ssrId]=0;

	patternTable_WriteToEEPRom(ssrId, 0);
}

void patternTable_WriteToEEPRom(uint8 ssrId, uint8 index){
	eeprom_write_block(&(patternTable.size[ssrId]), &ee_PatternInfoTableSize[ssrId], sizeof(uint8));		
	eeprom_write_block(&(patternTable.patternInfo[ssrId][index]), &(ee_PatternInfo[ssrId][index]), sizeof(Pattern_Info));	
}


