#include "DeviceInfo.h"
#include "SSRControl.h"

SSR_State_Info ee_SSRInfo EEMEM;

void handleDeviceControl(uint16 zigbee_Id, int ssrId, uint8 dimmingLevel,
		uint8 ctrlMode, uint8 isPattern) {
	uint8 i = 0;

	if (isPattern)
		if (ssrId == 0xff)
			for (i = 0; i < NumOfChannels; i++)
				tmp_SSRInfo.isPatternMode[i] = 0x00;
		else
			tmp_SSRInfo.isPatternMode[ssrId] = 0x00;

	switch (ctrlMode) {
	case OFF:
		if (dimmingLevel == 0xFF) {
			if (ssrId == 0xFF) {
				SSR_OFF(ssrId);
				for (i = 0; i < NumOfChannels; i++) {
					tmp_SSRInfo.ctrlMode[i] = OFF;
					tmp_SSRInfo.SSR_State[i] = OFF;
					tmp_SSRInfo.expirationTime[i] = 0;
					tmp_SSRInfo.dimming_level[i] = 0xFF;
					dimmer_SendReqDimming(i, 0x00);
				}
			} else {
				if (ssrId <= 7)
					SSR_OFF((1 << (7-ssrId)));

				tmp_SSRInfo.ctrlMode[ssrId] = OFF;
				tmp_SSRInfo.SSR_State[ssrId] = OFF;
				tmp_SSRInfo.expirationTime[ssrId] = 0;
				tmp_SSRInfo.dimming_level[ssrId] = 0xFF;
				dimmer_SendReqDimming(ssrId, 0x00);
			}
		} else {
			if (ssrId == 0xFF) {
				SSR_ON(ssrId);
				for (i = 0; i < NumOfChannels; i++) {
					tmp_SSRInfo.ctrlMode[i] = ON;
					tmp_SSRInfo.SSR_State[i] = ON;
					tmp_SSRInfo.dimming_level[i] = 0x00;
					tmp_SSRInfo.expirationTime[i] = 0xffff;
					dimmer_SendReqDimming(i, 0x00);
				}
			} else {
				if (ssrId <= 7)
					SSR_ON((1 << (7-ssrId)));

				tmp_SSRInfo.ctrlMode[ssrId] = ON;
				tmp_SSRInfo.SSR_State[ssrId] = ON;
				tmp_SSRInfo.dimming_level[ssrId] = 0x00;
				tmp_SSRInfo.expirationTime[i] = 0xffff;
				dimmer_SendReqDimming(ssrId, 0x00);
			}
		}
		//plcs_NCP_SendEvent(tmp_zrmp.zrm_Addr, 0x00, 0x01);
		break;
	case ON:
		if (dimmingLevel == 0xFF) {
			if (ssrId == 0xFF) {
				SSR_ON(ssrId);

				for (i = 0; i < NumOfChannels; i++) {
					tmp_SSRInfo.ctrlMode[i] = ON;
					tmp_SSRInfo.SSR_State[i] = ON;
					tmp_SSRInfo.dimming_level[i] = 0x0A;
					tmp_SSRInfo.expirationTime[i] = 0xffff;
					dimmer_SendReqDimming(i, 0x0A);
				}
			} else {
				if (ssrId <= 7)
					SSR_ON((1 << (7-ssrId)));

				tmp_SSRInfo.ctrlMode[ssrId] = ON;
				tmp_SSRInfo.SSR_State[ssrId] = ON;
				tmp_SSRInfo.dimming_level[ssrId] = 0x0A;
				tmp_SSRInfo.expirationTime[ssrId] = 0xffff;
				dimmer_SendReqDimming(ssrId, 0x0A);
			}
		} else {
			if (ssrId == 0xFF) {
				SSR_ON(ssrId);

				for (i = 0; i < NumOfChannels; i++) {
					tmp_SSRInfo.ctrlMode[i] = ON;
					tmp_SSRInfo.SSR_State[i] = ON;
					tmp_SSRInfo.dimming_level[i] = dimmingLevel;
					tmp_SSRInfo.expirationTime[i] = 0xffff;
					dimmer_SendReqDimming(i, dimmingLevel);

				}
			} else {
				if (ssrId <= 7)
					SSR_ON((1 << (7-ssrId)));

				tmp_SSRInfo.ctrlMode[ssrId] = ON;
				tmp_SSRInfo.SSR_State[ssrId] = ON;
				tmp_SSRInfo.dimming_level[ssrId] = dimmingLevel;
				tmp_SSRInfo.expirationTime[ssrId] = 0xffff;
				dimmer_SendReqDimming(ssrId, dimmingLevel);
			}
		}
		//plcs_NCP_SendEvent(tmp_zrmp.zrm_Addr, 0x00, 0x01);
		break;
	case SENSOR:
		if (ssrId == 0xFF) {
			for (i = 0; i < NumOfChannels; i++) {
				tmp_SSRInfo.ctrlMode[i] = SENSOR;
				tmp_SSRInfo.SSR_State[i] = SENSOR;
				tmp_SSRInfo.dimming_level[i] = dimmingLevel;
				tmp_SSRInfo.expirationTime[i] = 0x00;
			}
		} else {
			tmp_SSRInfo.ctrlMode[ssrId] = SENSOR;
			tmp_SSRInfo.SSR_State[ssrId] = SENSOR;
			tmp_SSRInfo.dimming_level[ssrId] = dimmingLevel;
			tmp_SSRInfo.expirationTime[ssrId] = 0xFF;
		}
		break;

	case SCHEDULE:
		//timer_set(SCHEDULE_COUNT_ID, 60000);					// Start Schedule Control...Check per 1min...

		if (ssrId == 0xFF) {
			for (i = 0; i < NumOfChannels; i++) {
				tmp_SSRInfo.ctrlMode[i] = SCHEDULE;
				tmp_SSRInfo.SSR_State[i] = SCHEDULE;
			}
		} else {
			tmp_SSRInfo.ctrlMode[ssrId] = SCHEDULE;
			tmp_SSRInfo.SSR_State[ssrId] = SCHEDULE;
		}
		break;
	case PATTERN:
		if (ssrId == 0xFF) {
			for (i = 0; i < NumOfChannels; i++) {
				tmp_SSRInfo.isPatternMode[i] = PATTERN;
			}
		} else {
			tmp_SSRInfo.isPatternMode[ssrId] = PATTERN;
		}
		break;

	default:
		break;
	}
	//eeprom에 설정 정보 저장.
	if (ssrId == 0xff)
		ssrInfo_WriteToEEPRom();
	else
		ssrInfo_WriteToEEPRomOfIndex(ssrId);
}

void ssrInfo_WriteToEEPRom() {
	eeprom_write_block(&tmp_SSRInfo, &ee_SSRInfo, sizeof(SSR_State_Info));
}

void ssrInfo_WriteToEEPRomOfIndex(uint8 ssrId) {
	eeprom_write_block(&(tmp_SSRInfo.ctrlMode[ssrId]),
			&(ee_SSRInfo.ctrlMode[ssrId]), sizeof(uint8));
	eeprom_write_block(&(tmp_SSRInfo.isPatternMode[ssrId]),
			&(ee_SSRInfo.isPatternMode[ssrId]), sizeof(uint8));
	eeprom_write_block(&(tmp_SSRInfo.dimming_level[ssrId]),
			&(ee_SSRInfo.dimming_level[ssrId]), sizeof(uint8));
	eeprom_write_block(&(tmp_SSRInfo.expirationTime[ssrId]),
			&(ee_SSRInfo.expirationTime[ssrId]), sizeof(uint16));
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Initialize Mapping Table  ------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void initDeviceTable() {
	uint8 i = 0;
	deviceInfoTable.size = 0;
	/*deviceInfoTable.deviceInfo = NULL;
	while (1) {
		deviceInfoTable.deviceInfo = (device_Info*) malloc(
				sizeof(device_Info) * 40);
		if (deviceInfoTable.deviceInfo != NULL)
			break;
	}*/
	eeprom_read_block(&tmp_SSRInfo, &ee_SSRInfo, sizeof(SSR_State_Info));

	for (i = 0; i < NumOfChannels; i++)
		tmp_SSRInfo.timerCnt[i] = 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Find Node Mapping Table  ------------------------------------------------------ //
// ---------------------------------------------------------------------------------------------- //
device_Info *findNodeById(uint16 node_Id) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++) {
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id)
			return &(deviceInfoTable.deviceInfo[i]);
	}
	return NULL;
}

int isContainNodeOfId(uint16 Ieee_Addr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].ieeeId == Ieee_Addr)
			return 1;
	return 0;
}

int isContainNode(uint16 node_Id, uint16 nwkAddr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].nwkAddr == nwkAddr
				&& deviceInfoTable.deviceInfo[i].ieeeId == node_Id)
			return 1;

	return 0;
}

int isContainNodeOfNwkAddr(uint16 nwk_Addr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].nwkAddr == nwk_Addr)
			return 1;

	return 0; // 찾는 값이 없으면 '0'을 리턴
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
device_Info *addDeviceTable(uint16 node_Id, uint16 nwkAddr, uint16 node_Type,
		uint8 device_version, uint8 fw_version) {
	uint8 i = 0;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].ieeeId = node_Id;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].nwkAddr = nwkAddr;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].deviceType = node_Type;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].deviceVersion =
			device_version;
	deviceInfoTable.deviceInfo[deviceInfoTable.size].fwVersion = fw_version;

	deviceInfoTable.size++;

	return NULL;
}

int updateDeviceTable(uint16 node_Id, uint16 nwkAddr, uint16 node_Type,
		uint8 device_version, uint8 fw_version) {
	device_Info *pUpdate;

	if (((pUpdate = findNodeById(node_Id)) != NULL)
			&& (pUpdate->nwkAddr) != nwkAddr) {
		pUpdate->nwkAddr = nwkAddr;
		pUpdate->deviceType = node_Type;
		pUpdate->deviceVersion = device_version;
		pUpdate->fwVersion = fw_version;
		return 0;
	} else {
		if (isContainNodeOfId(node_Id) == 0)
			addDeviceTable(node_Id, nwkAddr, node_Type, device_version,
					fw_version);
	}
	return 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Remove Node & Mapping Table  ------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void removeNode(uint16 node_Id) {
	uint8 i = 0, j = 0;

	for (i = 0; i < deviceInfoTable.size; i++) {
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id) {
			for (j = i; j < deviceInfoTable.size - 1; j++)
				deviceInfoTable.deviceInfo[j] =
						deviceInfoTable.deviceInfo[j + 1];
			deviceInfoTable.size--;
			break;
		}
	}
}

void removeDeviceTable() {
	deviceInfoTable.size = 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Count Node & Device Table  ---------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint8 countDeviceTable(uint16 node_Type) {
	uint8 device_count = 0;
	uint8 i = 0;

	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].deviceType == node_Type)
			device_count++;
	return device_count;

}

uint8 countDevice() {
	return deviceInfoTable.size;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Get Node ID Table  ----------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
uint16 getNetAddr(uint16 node_Id) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].ieeeId == node_Id)
			return deviceInfoTable.deviceInfo[i].nwkAddr;
	return 0;
}

uint16 getNodeId(uint16 nwk_Addr) {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		if (deviceInfoTable.deviceInfo[i].nwkAddr == nwk_Addr)
			return deviceInfoTable.deviceInfo[i].ieeeId;
	return 0;

}

void deviceInfo_UpdateSSRInfo() {
	tmp_PrevSSRInfo = tmp_SSRInfo;
}

int deviceInfo_IsUpdatedSSRInfo() {
	uint8 i = 0;
	for (i = 0; i < NumOfChannels; i++) {
		if (tmp_PrevSSRInfo.SSR_State[i] != tmp_SSRInfo.SSR_State[i])
			return 1;
		if (tmp_PrevSSRInfo.expirationTime[i] != tmp_SSRInfo.expirationTime[i])
			return 1;
	}

	return 0;
}

// ---------------------------------------------------------------------------------------------- //
// ------------------ Print Node & Mapping Table  ---------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void printDeviceTable() {
	uint16 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		printf_P(PSTR("\n%d: %02X-%02X"), ++i,
				deviceInfoTable.deviceInfo[i].ieeeId,
				deviceInfoTable.deviceInfo[i].nwkAddr);
}

// Enf of File
