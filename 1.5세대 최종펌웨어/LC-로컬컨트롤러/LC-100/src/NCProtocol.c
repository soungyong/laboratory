#include "NCProtocol.h"
#include "debug.h"

/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 +
 +
 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

// --------------------------------------------------------------------------- //
device_Info tmp_DevInfo_D;
device_Info *tmp_DevInfo_P;

// --------------------------------------------------------------------------- //
uint8 node_Type[64];
// --------------------------------------------------------------------------- //

uint8 dev_Count;
uint16 ctrl_Cmd;
uint16 device_Type;
uint32 z_ieee_Id;

uint8 ncp_NetState = NCP_NET_NOT_CONNECT;

char token = 0;
// --------------------------------------------------------------------------- //

void ncp_ProcessMessage(uint16 srcAddr, uint8 msg[], int length) {

	switch (msg[0]) { // Msg Type of NCP
	case NCP_REQ_PING:
		ncp_ProcessPingReq(srcAddr, msg, length);
		break;
	case NCP_RES_PING:
		ncp_ProcessPingRes(srcAddr, msg, length);
		break;
	case NCP_REQ_REGISTER:
		ncp_ProcessRegisterReq(srcAddr, msg, length);
		break;
	case NCP_RES_REGISTER:
		ncp_ProcessRegisterRes(srcAddr, msg, length);
		break;
	case NCP_SEND_TIME_INFO:
		npc_ProcessSendTimeInfo(msg, length);
		break;
	case NCP_REQ_REGISTER_BROADCAST:
		ncp_ProcessRegisterBoradcastReq(srcAddr, msg, length);
		break;
	case NCP_ASSIGN_TOKEN:
		ncp_ProcessAssignToken(srcAddr, msg, length);
		break;
	case NCP_RES_TOKEN_RELEASE:
		ncp_ProcessReleaseTokenRes(srcAddr, msg, length);
		break;
	}
}

void ncp_ProcessRegisterReq(uint16 nwk_Addr, uint8 msg[], int length) {
	int i = 0;
	uint16 nodeId;
	uint16 deviceType;
	uint8 deviceVersion;
	uint8 fwVersion;
	// Request Register
	nodeId = (uint16) ((msg[1] << 8) | (msg[2]));
	deviceType = (uint16) ((msg[3] << 8) | (msg[4]));
	deviceVersion = (uint8) msg[5];
	fwVersion = (uint8) msg[6];

	npc_SendRegisterNodeReq(nodeId, deviceType, nwk_Addr);
	send_NCPResRegister(nwk_Addr, nodeId, deviceType, 0x0000);
	updateDeviceTable(nodeId, nwk_Addr, deviceType, deviceVersion, fwVersion);

	/*if (isContainNodeOfId(nodeId) == 0
	 && isContainNodeOfNwkAddr(nwk_Addr) == 0) {
	 send_NCPResRegister(nwk_Addr, nodeId, deviceType, 0x0000);
	 updateDeviceTable(nodeId, nwk_Addr, deviceType, deviceVersion,
	 fwVersion);
	 npc_SendRegisterNodeReq(nodeId, deviceType, nwk_Addr);
	 } else if (isContainNodeOfNwkAddr(nwk_Addr) == 0) {
	 send_NCPResRegister(nwk_Addr, nodeId, deviceType, 0x0000);
	 updateDeviceTable(nodebId, nwk_Addr, deviceType, deviceVersion,
	 fwVersion);
	 } else if (getNetAddr(nodeId) == nwk_Addr) {
	 send_NCPResRegister(nwk_Addr, nodeId, deviceType, 0x0000);
	 } else {
	 removeNode(nodeId);
	 send_NCPResRegister(nwk_Addr, nodeId, deviceType, 0x0001);
	 }*/
}

void ncp_ProcessPingReq(uint16 srcAddr, uint8 msg[], int length) {
	uint16 nodeId;
	device_Info *pDeviceInfo;

	nodeId = msg[1] << 8 | msg[2];
	pDeviceInfo = findNodeById(nodeId);

	if (pDeviceInfo != NULL && srcAddr == pDeviceInfo->nwkAddr) {
		send_NCPResPing(srcAddr, msg[1] << 8 | msg[2], device_Type, 0x00, 30);
	}else if (pDeviceInfo != NULL){
		pDeviceInfo->nwkAddr = srcAddr;
		send_NCPResPing(srcAddr, msg[1] << 8 | msg[2], device_Type, 0x00, 30);
	}
}
/*
 void ncp_ProcessNodeInfoReq(uint16 zigbee_Id, uint8 msg[], int length){
 z_ieee_Id = (uint32)(((uint32)msg[1] << 24) |((uint32) msg[2] << 16) | ((uint32)msg[3] << 8) | ((uint32)msg[4]));

 tmp_DevInfo_P = findNodeById(z_ieee_Id);
 #ifdef DEBUG_ENABLE
 printf_P(PSTR("\nNCP_REQ_NODE_INFO (0x%02X)"), msg[0]);
 printf_P(PSTR("\n + IEEE_ID : 0x%lX"), tmp_DevInfo_P->ieeeId);
 printf_P(PSTR("\n + Device_Type : 0x%04X"), tmp_DevInfo_P->deviceType);
 printf_P(PSTR("\n + Nwk_Addr : 0x%04X"), tmp_DevInfo_P->nwkAddr);
 #endif
 if(tmp_DevInfo_P->ieeeId)
 send_NCPResNodeInfo(zigbee_Id, tmp_DevInfo_P->ieeeId,  0x0000, tmp_DevInfo_P->deviceType, tmp_DevInfo_P->nwkAddr);

 else
 send_NCPResNodeInfo(zigbee_Id, z_ieee_Id, 0x0001, 0x0000, 0xFFFF);
 }*/

void ncp_ProcessRegisterRes(uint16 zigbee_Id, uint8 msg[], int length) {
#ifdef DEBUG_ENABLE
	printf_P(PSTR("\nNCP_RES_REGISTER (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Zigbee_ID : 0x%04X"), zigbee_Id);
	printf_P(PSTR("\n + Result : 0x%02X%02X"), msg[4], msg[5]);
#endif
	if (ncp_NetState != NCP_NET_REGISTER)
		debug_UpdateConnection();

	ncp_NetState = NCP_NET_REGISTER;
	ncp_SendPingReq(tmp_zrmp.zrm_Addr);
	registerAllNodetoServer();

}
void ncp_ProcessPingRes(uint16 zigbee_Id, uint8 msg[], int length) {
	uint8 flag = 0;
	uint8 pingInterval;

	flag = msg[5];
	pingInterval = msg[6] << 8 | msg[7];
#ifdef DEBUG_ENABLE				
	printf_P(PSTR("\nNCP_RES_PING (0x%02X)"), msg[0]);
	printf_P(PSTR("\n + Zigbee_ID : 0x%04X"), zigbee_Id);
	printf_P(PSTR("\n + Protocol Ver : 0x%02X"), msg[4]);
	printf_P(PSTR("\n + Ping Interval : 0x%04X"), pingInterval);
#endif

	if (pingInterval == 0)
		pingInterval = 0xffff;
	if (flag == 0x01) {
		ncp_SendRegisterReq(tmp_zrmp.zrm_Addr);
		registerAllNodetoServer();
	}

	timer_set(NCP_PING_TIMER_ID, (pingInterval * 1000));
}

void npc_ProcessSendTimeInfo(uint8 msg[], int length) {
	uint16 nodeId;
	uint8 hour;
	uint8 min;

	nodeId = msg[2] << 8 | msg[3];
	hour = msg[4];
	min = msg[5];

	Hour = hour;
	Min = min;

	npc_SendTimeInfoRes(nodeId, Hour, Min);

}

void ncp_ProcessNodeInfoRes(uint16 zigbee_Id, uint8 msg[], int length) {
}

void ncp_SendPingReq(uint16 nodeId) {
	int len = 0;

#ifdef DEBUG_ENABLE				
	printf_P(PSTR("\nNCP_Send_PING (0x%02X)"));
	printf_P(PSTR("\n + Zigbee_ID : 0x%04X"), nodeId);
#endif
	uint8 tmp_Buff[32];
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUBPROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_PING;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8) (nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = NCP_VERSION;
	tmp_Buff[len++] = 0;

	sendServerMessage(tmp_Buff, len);
}

void ncp_SendRegisterReq(uint16 nodeId) {
	int len = 0;
	uint8 tmp_Buff[32];

#ifdef DEBUG_ENABLE				
	printf_P(PSTR("\nNCP_Send_Register (0x%02X)"));
	printf_P(PSTR("\n + Zigbee_ID : 0x%04X"), nodeId);
#endif
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUBPROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_REGISTER;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8) (nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = (uint8) (PLCS_LC_100Z_TYPE >> 8);
	tmp_Buff[len++] = (uint8) PLCS_LC_100Z_TYPE;
	tmp_Buff[len++] = PLCS_LC_DEVICE_VER;
	tmp_Buff[len++] = PLCS_LC_FW_VER;

	sendServerMessage(tmp_Buff, len);
}

void ncp_SendRegisterReqWithoutQueue(uint16 nodeId) {
	int len = 0;
	uint8 tmp_Buff[32];

#ifdef DEBUG_ENABLE				
	printf_P(PSTR("\nNCP_Send_Register (0x%02X)"));
	printf_P(PSTR("\n + Zigbee_ID : 0x%04X"), nodeId);
#endif
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUBPROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_REGISTER;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8) (nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = (uint8) (PLCS_LC_100Z_TYPE >> 8);
	tmp_Buff[len++] = (uint8) PLCS_LC_100Z_TYPE;
	tmp_Buff[len++] = PLCS_LC_DEVICE_VER;
	tmp_Buff[len++] = PLCS_LC_FW_VER;

	sendServerMessageWithoutQueue(tmp_Buff, len);
}

void npc_SendTimeInfoRes(uint8 nodeId, uint8 hour, uint8 min) {
	int len = 0;
	uint8 tmp_Buff[32];

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUBPROTOCOL_ID;
	tmp_Buff[len++] = NCP_SEND_TIME_INFO;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = (uint8) (nodeId >> 8);
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = hour;
	tmp_Buff[len++] = min;

	sendServerMessage(tmp_Buff, len);
}

/////////////////////////////////////////////////
void send_NCPResPing(uint16 dstAddr, uint16 nodeId, uint16 device_Type,
		uint8 flag, uint8 timeOut) {
	int len = 0;
	uint8 tmp_Buff[32];

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUBPROTOCOL_ID;
	tmp_Buff[len++] = NCP_RES_PING;
	tmp_Buff[len++] = nodeId >> 8;
	tmp_Buff[len++] = nodeId;
	tmp_Buff[len++] = NCP_VERSION;
	tmp_Buff[len++] = (uint8) (flag);
	tmp_Buff[len++] = (uint8) (timeOut >> 8);
	tmp_Buff[len++] = (uint8) (timeOut);

	sendData(dstAddr, tmp_Buff, len);
}

void send_NCPResRegister(uint16 nwk_Addr, uint16 node_Id, uint16 deviceType,
		uint16 result) {
	int len = 0;
	uint8 tmp_Buff[32];

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUBPROTOCOL_ID;
	tmp_Buff[len++] = NCP_RES_REGISTER;
	tmp_Buff[len++] = (uint8) (node_Id >> 8);
	tmp_Buff[len++] = (uint8) (node_Id);
	tmp_Buff[len++] = (uint8) (result >> 8);
	tmp_Buff[len++] = (uint8) (result);
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = 0x1e;

	sendData(nwk_Addr, tmp_Buff, len);
}

/*
 void send_NCPResNodeInfo(uint16 zigbee_Id, uint32 node_Id, uint16 result, uint16 device_Type, uint16 short_Addr)
 {
 int len = 0;
 uint8 tmp_Buff[32];

 tmp_Buff[len++] = G2N_ZIGBEE;
 tmp_Buff[len++] = NCP_PROTOCOL_ID;
 tmp_Buff[len++] = NCP_RES_NODE_INFO;
 tmp_Buff[len++] = (uint8)(result >> 8);
 tmp_Buff[len++] = (uint8)(result);
 tmp_Buff[len++] = (uint8)(node_Id >> 24);
 tmp_Buff[len++] = (uint8)(node_Id >> 16);
 tmp_Buff[len++] = (uint8)(node_Id >> 8);
 tmp_Buff[len++] = (uint8)(node_Id);
 tmp_Buff[len++] = (uint8)(device_Type >> 8);
 tmp_Buff[len++] = (uint8)(device_Type);
 tmp_Buff[len++] = (uint8)(short_Addr >> 8);
 tmp_Buff[len++] = (uint8)(short_Addr);

 sendData(zigbee_Id, tmp_Buff, len);
 }


 void send_NCPResNodeList(uint16 zigbee_Id, uint16 device_Type, uint8 dev_Count, uint8 node_Id[], int length)
 {
 int len = 0;
 uint8 tmp_Buff[32];

 tmp_Buff[len++] = G2N_ZIGBEE;
 tmp_Buff[len++] = NCP_PROTOCOL_ID;
 tmp_Buff[len++] = NCP_RES_NODE_LIST;
 tmp_Buff[len++] = (uint8)(device_Type >> 8);
 tmp_Buff[len++] = (uint8)(device_Type);
 tmp_Buff[len++] = (uint8)dev_Count;

 memcpy(&tmp_Buff[len], node_Id, length);
 len  += length;

 sendData(zigbee_Id, tmp_Buff, len);
 }

 void send_NCPResCtrlCmd(uint16 zigbee_Id, uint16 ctrl_Cmd, uint16 result, uint16 reason)
 {
 int len = 0;
 uint8 tmp_Buff[32];

 tmp_Buff[len++] = G2N_ZIGBEE;
 tmp_Buff[len++] = NCP_PROTOCOL_ID;
 tmp_Buff[len++] = NCP_RES_CONTROL;
 tmp_Buff[len++] = (uint8)(ctrl_Cmd >> 8);
 tmp_Buff[len++] = (uint8)(ctrl_Cmd);
 tmp_Buff[len++] = (uint8)(result >> 8);
 tmp_Buff[len++] = (uint8)(result);
 tmp_Buff[len++] = (uint8)(reason >> 8);
 tmp_Buff[len++] = (uint8)(reason);

 sendData(zigbee_Id, tmp_Buff, len);
 }


 void sendReset(uint16 zigbee_Id)
 {
 int len = 0;
 uint8 tmp_Buff[32];

 tmp_Buff[len++] = G2N_ZIGBEE;
 tmp_Buff[len++] = NCP_CONTROL_DATA;
 tmp_Buff[len++] = 0x01;
 tmp_Buff[len++] = 0x00;

 sendData_Remote(zigbee_Id, tmp_Buff, len);
 }*/

void npc_SendRegisterNodeReq(uint16 node_Id, uint16 device_Type,
		uint16 nwk_addr) {
	int len = 0;
	uint8 tmp_Buff[32];

	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUBPROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_REGISTER_NODE;
	tmp_Buff[len++] = 0x00;

	tmp_Buff[len++] = tmp_zrmp.zrm_Addr >> 8;
	tmp_Buff[len++] = tmp_zrmp.zrm_Addr;

	tmp_Buff[len++] = node_Id >> 8;
	tmp_Buff[len++] = node_Id;

	tmp_Buff[len++] = (uint8) (device_Type >> 8);
	tmp_Buff[len++] = (uint8) (device_Type);

	switch (device_Type) {
	case LC_100Z_TYPE:
		tmp_Buff[len++] = (uint8) (LC_DEVICE_VER);
		tmp_Buff[len++] = (uint8) (LC_FW_VER);
		break;
	case ZDIMMER_TYPE:
	case ZSENSOR_TYPE:
		tmp_DevInfo_P = findNodeById(node_Id);
		if (tmp_DevInfo_P == NULL)
			return;
		if (tmp_DevInfo_P->deviceType != device_Type)
			return;
		tmp_Buff[len++] = tmp_DevInfo_P->ieeeId >> 8;
		tmp_Buff[len++] = tmp_DevInfo_P->ieeeId;
		tmp_Buff[len++] = tmp_DevInfo_P->deviceVersion;
		tmp_Buff[len++] = tmp_DevInfo_P->fwVersion;
		tmp_Buff[len++] = tmp_DevInfo_P->nwkAddr >> 8;
		tmp_Buff[len++] = tmp_DevInfo_P->nwkAddr;

		break;
	}

	//tmp_Buff[len++] = (uint8)(nwk_addr >> 8);
	//tmp_Buff[len++] = (uint8)(nwk_addr);
	sendServerMessage(tmp_Buff, len);
	//xcps_send_rs485(tmp_Buff, len);
	//MSLEEP(3);
}

void registerNodetoServer_test() {
	int len = 0;
	uint8 tmp_Buff[32];
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUBPROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_REGISTER_NODE;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = tmp_zrmp.zrm_Addr >> 8;
	tmp_Buff[len++] = tmp_zrmp.zrm_Addr;

	tmp_Buff[len++] = 0x20;
	tmp_Buff[len++] = 0x50;

	tmp_Buff[len++] = 0x01;
	tmp_Buff[len++] = 0x01;
	tmp_Buff[len++] = 0x10;
	tmp_Buff[len++] = 0x10;
	tmp_Buff[len++] = 0x20;
	tmp_Buff[len++] = 0x20;

	//tmp_Buff[len++] = (uint8)(nwk_addr >> 8);
	//tmp_Buff[len++] = (uint8)(nwk_addr);

	sendServerMessage(tmp_Buff, len);
}

void registerAllNodetoServer() {
	uint8 i = 0;
	for (i = 0; i < deviceInfoTable.size; i++)
		npc_SendRegisterNodeReq(deviceInfoTable.deviceInfo[i].ieeeId,
				deviceInfoTable.deviceInfo[i].deviceType,
				deviceInfoTable.deviceInfo[i].nwkAddr);
	//if(deviceInfoTable.size > 0) 
	//	registerNodetoServer_test();//registerNodetoServer(deviceInfoTable.deviceInfo[i].ieeeId, deviceInfoTable.deviceInfo[i].deviceType, deviceInfoTable.deviceInfo[i].nwkAddr);		
}
uint8 ncp_ConnState() {
	return ncp_NetState;
}

char hasToken() {
	if (token == 0)
		return 0;

	return 1;
}

void ncp_ProcessRegisterBoradcastReq(uint16 zigbee_Id, uint8 msg[], int length) {
	char numOfNode;
	char flag;
	uint16 nodeId;
	char isRegistered = 0;
	char i;

	flag = msg[4];
	numOfNode = msg[5];
	for (i = 0; i < numOfNode; i++) {
		nodeId = msg[6 + i * 2] << 8 | msg[7 + i * 2];
		if (nodeId == tmp_zrmp.zrm_Addr)
			isRegistered = 1;
	}

	if (isRegistered == 0) {
		switch (flag) {
		case 0:
			MSLEEP(tmp_zrmp.zrm_Addr % 17 * 70);
			break;
		case 1:
			MSLEEP(tmp_zrmp.zrm_Addr % 13 * 70);
			break;
		case 2:
			MSLEEP(tmp_zrmp.zrm_Addr % 11 * 70);
			break;
		case 3:
			MSLEEP(tmp_zrmp.zrm_Addr % 7 * 70);
			break;
		case 4:
			MSLEEP(tmp_zrmp.zrm_Addr % 5 * 70);
			break;
		case 5:
			MSLEEP(tmp_zrmp.zrm_Addr % 3 * 70);
			break;
		}

		ncp_SendRegisterReqWithoutQueue(tmp_zrmp.zrm_Addr);
	}
}

void ncp_ProcessAssignToken(uint16 zigbee_Id, uint8 msg[], int length) {
	token = 1;
}

void ncp_ProcessReleaseTokenRes(uint16 zigbee_Id, uint8 msg[], int length) {
	token = 0;
}

void send_NCPReqReleaseToken() {
	uint8 len = 0;
	uint8 tmp_Buff[32];
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = NCP_DEFAULT_SUBPROTOCOL_ID;
	tmp_Buff[len++] = NCP_REQ_TOKEN_RELEASE;
	tmp_Buff[len++] = 0x00;
	tmp_Buff[len++] = tmp_zrmp.zrm_Addr >> 8;
	tmp_Buff[len++] = tmp_zrmp.zrm_Addr;

	sendServerMessage(tmp_Buff, len);
}

//End of File

