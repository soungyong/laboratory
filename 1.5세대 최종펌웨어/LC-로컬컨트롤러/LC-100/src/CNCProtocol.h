#ifndef __CNCP_PROTOCOL_H__
#define __CNCP_PROTOCOL_H__


#include <avr/io.h>
#include <avr/eeprom.h>
#include <stdio.h>
#include "DeviceInfo.h"
#include "Util.h"
#include "ZRMProtocol.h"
#include "XNet.h"
#include "XNetProtocol.h"
#include "MappingTable.h"
#include "Timer.h"
#include "ScheduleTime.h"
/*


// ---------------------------------------------------------------------------------------------- //
#define CNCP_PROTOCOL_ID				(0x03) 	// Protocol ID
#define CNCP_VERSION         				(0x10)  	// Protocol Version
#define DIMMER_PROTOCOL_ID				(0x11)

#define CNCP_REQ_PING					(0x01)	// Ping
#define CNCP_RES_PING					(0x02)	// Ping 응답

#define CNCP_REQ_REGISTER				(0x11)	// 등록 요청
#define CNCP_RES_REGISTER				(0x12)	// 등록 요청 응답 

#define CNCP_REQ_REGISTER_NODE		(0x13)	// 노드 등록 요청
#define CNCP_RES_REGISTER_NODE		(0x14)	// 노드 등록 요청 응답

#define CNCP_REQ_CONTROL				(0x30)	// 노드 제어요청
#define CNCP_RES_CONTROL				(0x31)	// 노드 제어요청 응답
#define CNCP_REQ_DIMMING				(0x32)	// 디밍 제어요청
#define CNCP_RES_DIMMING				(0x33)	// 디밍 제어요청 응답
#define CNCP_REQ_POWER_METER			(0x34)	// 전력량 요청
#define CNCP_RES_POWER_METER			(0x35)	// 전력량 요청 응답
#define CNCP_REQ_STATE_INFO			(0x36)	// 노드 상태 정보 요청
#define CNCP_RES_STATE_INFO			(0x37)	// 노드 상태 정보 요청 응답

#define CNCP_SEND_SCHEDULE			(0x41)	// 스케줄 정보 전달
#define CNCP_UPDATE_SCHEDULE			(0x42)	// 스케줄 정보 업데이트
#define CNCP_SEND_MAPPING_INFO		(0x43)	// 센서 맵핑 정보
#define CNCP_SEND_DIMMER2SSR_MAPPING_INFO	(0x45)	// 디머 맵핑 정보
#define CNCP_UPDATE_POWER_MERTER		(0x44)	// 전력량 정보 전달(Server -> Load Controller)

#define CNCP_SEND_EVENT				(0x50)	// 이벤트 정보 전달

#define CNCP_SEND_ON_TIME				(0x60)	// 센서 동작감지 차단 시간 전달
#define CNCP_RES_ON_TIME				(0x61)	// 센서 동작감지 차단 시간 전달 응답
// ---------------------------------------------------------------------------------------------- //


// ---------------------------------------------------------------------------------------------- //
#define READY							0
#define REGISER_STATE					1
// ---------------------------------------------------------------------------------------------- //


#define OFF								0x00
#define ON								0x01
#define SENSOR							0x02
#define SCHEDULE						0x03

#define LC_100Z_TYPE		(uint16)(0x2010)
#define LC_DEVICE_VER		(uint8)(0x10)
#define LC_FW_VER			(uint8)(0x10)

#define ZDIMMER_TYPE		(uint16)(0x2040)
#define ZSENSOR_TYPE		(uint16)(0x2050)





// -------------------------------------------------------------------------------------- //
void Gateway_CNCP(uint16 zigbee_Id, uint8 msg[], int length);

uint8 GetSensorID();
uint8 GetScheduleID();
uint8 GetPortID(int ssrID);
uint8 Conn_State();

void send_CNCPReqPing(uint16 tmp_node_id);
void send_CNCPReqRegister(uint16 tmp_node_id);
void send_CNCPReqRegisterNode(uint16 tmp_node_Id);
void send_CNCPReqRegisterNodeALL();
void send_CNCPReqPowerMerter_Value(uint16 tmp_node_id, uint32 tmp_PM_Value);
void send_CNCPReqStateInfoNode(uint16 SrcID, uint16 DstID);
void send_CNCPResStateInfo(uint8 tmp_node_id, uint16 device_type, uint16 tmp_dst_id);
void send_CNCPSendOnTime(uint16 SrcID, uint16 DstID, uint8 on_time);
void send_CNCPResControl(uint16 tmp_node_id, uint8 tmp_ssrId, uint8 tmp_CtrlMode);
// -------------------------------------------------------------------------------------- //


// -------------------------------------------------------------------------------------- //
void Zigbee_CNCP(uint16 zigbee_Id, uint8 msg[], int length);

void send_CNCPResDimming(uint16 tmp_node_id, uint16 tmp_dst_ID, uint8 tmp_dimValue);
void send_CNCPResStateInfoNode(uint16 tmp_node_id, uint16 tmp_dst_ID, uint8 tmp_Value);
void send_CNCPSendEvent(uint16 tmp_node_id, uint16 tmp_dst_ID, uint8 DataFormat);
void send_CNCPResOnTime(uint16 tmp_node_id, uint16 tmp_dst_ID, uint8 tmp_Value);

uint8 Dimming_Convert(uint8 DimLevel);
// -------------------------------------------------------------------------------------- //
*/

#endif

