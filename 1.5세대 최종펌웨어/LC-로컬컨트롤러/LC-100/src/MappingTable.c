#include "MappingTable.h"
#include <avr/eeprom.h>


//SensorSSRMappingTable_st ee_SensorMappingInfo EEMEM;
SensorSSRMappingInfo_st ee_SensorMappingInfo[MaxNumOfSensorSSRMappingInfo] EEMEM;
uint16 ee_SensorSSRMappingTableSize EEMEM;

//DimmerSSRMappingTable_st ee_DimmerMappingInfo EEMEM;
DimmerSSRMappingInfo_st ee_DimmerSSRMappingInfo[MaxNumOfDimmerSSRMappingInfo] EEMEM;
uint16 ee_DimmerSSRMappingTableSize EEMEM;

// ---------------------------------------------------------------------------------------------- //
// ------------------ Initialize Mapping Table  ------------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
void initMappingTable()
{
	uint16 i=0;

	sensorSSRMappingTable.size = 0;
	/*while(1){
		sensorSSRMappingTable.mappingInfo = (SensorSSRMappingInfo_st*) malloc(sizeof(SensorSSRMappingInfo_st)*MaxNumOfSensorSSRMappingInfo);	
		if(sensorSSRMappingTable.mappingInfo!=NULL)
			break;
	}*/

	eeprom_read_block(&(sensorSSRMappingTable.size), &ee_SensorSSRMappingTableSize, sizeof(uint16));	
	if(sensorSSRMappingTable.size >= MaxNumOfSensorSSRMappingInfo)
		sensorSSRMappingTable.size = 0;

	for(i=0; i < sensorSSRMappingTable.size; i++) 
		eeprom_read_block(&(sensorSSRMappingTable.mappingInfo[i]), &(ee_SensorMappingInfo[i]), sizeof(SensorSSRMappingInfo_st));		
}

void sensorMappingInfo_WriteToEEPRom(uint16 index){		
	
	eeprom_write_block(&(sensorSSRMappingTable.size), &ee_SensorSSRMappingTableSize, sizeof(uint16));	
	
	eeprom_write_block(&(sensorSSRMappingTable.mappingInfo[index]), &(ee_SensorMappingInfo[index]), sizeof(SensorSSRMappingInfo_st));	
	
}
// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
SensorSSRMappingInfo_st *addMappingTable(uint16 zigbeeID, uint8 tmp_SSRID, uint8 onTime, uint8 dimming_level)
{
	if(sensorSSRMappingTable.size >= MaxNumOfSensorSSRMappingInfo)
		return NULL;

	sensorSSRMappingTable.mappingInfo[sensorSSRMappingTable.size].sensorId = zigbeeID;
	sensorSSRMappingTable.mappingInfo[sensorSSRMappingTable.size].ssrId = tmp_SSRID;
	sensorSSRMappingTable.mappingInfo[sensorSSRMappingTable.size].onTime = onTime;
	sensorSSRMappingTable.mappingInfo[sensorSSRMappingTable.size].offDimmingLevel = dimming_level;
	sensorSSRMappingTable.size++;


	return &(sensorSSRMappingTable.mappingInfo[sensorSSRMappingTable.size-1]);
}


int updateMappingTable(uint16 zigbeeID, uint8 tmp_SSRID, uint8 onTime, uint8 dimming_level)
{
	int index;
	int result=0;
	if((index=findById(zigbeeID, tmp_SSRID))!=-1) {
		sensorSSRMappingTable.mappingInfo[index].onTime = onTime;
		sensorSSRMappingTable.mappingInfo[index].offDimmingLevel = dimming_level;		
		result = 2;
	}else if(addMappingTable(zigbeeID, tmp_SSRID, onTime, dimming_level)!=NULL)
		result = 1;
	else 
		return 0;

	index=findById(zigbeeID, tmp_SSRID);

	if(index!=-1)
		sensorMappingInfo_WriteToEEPRom(index);

	return result;
}




int findById(uint16 zigbee_Id, uint8 ssrId)
{
	uint16 i=0;	
	for(i=0; i < sensorSSRMappingTable.size; i++)
		if(zigbee_Id == sensorSSRMappingTable.mappingInfo[i].sensorId && ssrId == sensorSSRMappingTable.mappingInfo[i].ssrId)
			return i;	
	return -1;										// 찾는 값이 없으면 '-1'을 리턴
}

void printMappingInfoTable(){
	uint16 i = 0;
	for(i=0; i < sensorSSRMappingTable.size; i++)
		printf_P(PSTR("\n%d: %04x-%02x, OnTime: %02x, off: %02x"), ++i, sensorSSRMappingTable.mappingInfo[i].sensorId, sensorSSRMappingTable.mappingInfo[i].ssrId, sensorSSRMappingTable.mappingInfo[i].onTime, sensorSSRMappingTable.mappingInfo[i].offDimmingLevel);
}


// ---------------------------------------------------------------------------------------------- //
// ------------------ Add & Update Mapping Table  --------------------------------------------------- //
// ---------------------------------------------------------------------------------------------- //
DimmerSSRMappingInfo_st *addDimmerMappingTable(uint16 zigbeeID, uint8 tmp_SSRID)
{
	if(dimmerSSRMappingTable.size >= MaxNumOfDimmerSSRMappingInfo)
		return NULL;

	dimmerSSRMappingTable.mappingInfo[dimmerSSRMappingTable.size].dimmerId = zigbeeID;
	dimmerSSRMappingTable.mappingInfo[dimmerSSRMappingTable.size].ssrId = tmp_SSRID;	
	dimmerSSRMappingTable.size++;

	return &dimmerSSRMappingTable.mappingInfo[dimmerSSRMappingTable.size-1];
}


int updateDimmerMappingTable(uint16 zigbeeID, uint8 tmp_SSRID)
{
	int index;
	int result;

	if((index=findDimmerById(zigbeeID))!=-1) {		
		return 0;
	}else {
		if(addDimmerMappingTable(zigbeeID, tmp_SSRID)==NULL)
			return 0;
		else
			result = 1;
	}	

	index=findDimmerById(zigbeeID);
	if(index!=-1)
		dimmerMappingInfo_WriteToEEPRom(index);
	return result;
}


int findDimmerByPair(uint16 zigbee_Id, uint8 ssrId)
{
	uint16 i=0;	
	for(i=0; i < dimmerSSRMappingTable.size; i++)
		if(zigbee_Id == dimmerSSRMappingTable.mappingInfo[i].dimmerId && ssrId == dimmerSSRMappingTable.mappingInfo[i].ssrId)
			return i;	
	return -1;										// 찾는 값이 없으면 '-1'을 리턴
}

int findDimmerById(uint16 zigbee_Id)
{
	uint16 i=0;	
	for(i=0; i < dimmerSSRMappingTable.size; i++)
		if(zigbee_Id == dimmerSSRMappingTable.mappingInfo[i].dimmerId)
			return i;	
	return -1;										// 찾는 값이 없으면 '-1'을 리턴
}

void printDimmerMappingInfoTable(){
	uint16 i = 0;
	for(i=0; i < dimmerSSRMappingTable.size; i++)
		printf_P(PSTR("\n%d: %02x-%02x"), ++i, dimmerSSRMappingTable.mappingInfo[i].dimmerId, dimmerSSRMappingTable.mappingInfo[i].ssrId);
}


void initDimmerMappingTable()
{	
	uint16 i=0;

	dimmerSSRMappingTable.size = 0;
	/*while(1){
		dimmerSSRMappingTable.mappingInfo = (DimmerSSRMappingInfo_st*) malloc(sizeof(DimmerSSRMappingInfo_st)*MaxNumOfDimmerSSRMappingInfo);
		if(dimmerSSRMappingTable.mappingInfo!=NULL)
			break;
	}*/
	eeprom_read_block(&(dimmerSSRMappingTable.size), &ee_DimmerSSRMappingTableSize, sizeof(uint16));	
	
	if(dimmerSSRMappingTable.size >= MaxNumOfDimmerSSRMappingInfo)
		dimmerSSRMappingTable.size = 0;

	for(i=0; i < dimmerSSRMappingTable.size; i++) 
		eeprom_read_block(&(dimmerSSRMappingTable.mappingInfo[i]), &(ee_DimmerSSRMappingInfo[i]), sizeof(DimmerSSRMappingInfo_st));
}

void dimmerMappingInfo_WriteToEEPRom(uint16 index){		
	eeprom_write_block(&(dimmerSSRMappingTable.size), &ee_DimmerSSRMappingTableSize, sizeof(uint16));		
	eeprom_write_block(&(dimmerSSRMappingTable.mappingInfo[index]), &(ee_DimmerSSRMappingInfo[index]), sizeof(DimmerSSRMappingInfo_st));	
}

// Enf of File
