#include "RadarSensorProtocol.h"
#include "MappingTable.h"
#include "ZRMProtocol.h"

void processNCP_RadarSensor(uint16 zigbee_Id, uint8 msg[], int length){
	switch(msg[0]) {
		case NCP_REQ_REGISTER: 
			ncp_ProcessRegisterReq(zigbee_Id, msg, length);
			break;
		case NCP_REQ_PING:
			ncp_ProcessPingReq(zigbee_Id, msg, length);
			break;
		//case NCP_REQ_NODE_INFO:
			//ncp_ProcessNodeInfoReq(zigbee_Id, msg, length);
			//break;
		case RADAR_SENSOR_GET_DEVICE_INFO_RES:
			processNCP_RadarSensor_GetDeviceInfo_Res(zigbee_Id, msg, length);
			break;
		case RADAR_SENSOR_GET_STATE_RES:
			processNCP_RadarSensor_GetState_Res(zigbee_Id, msg, length);
			break;
		case RADAR_SENSOR_SEND_ON_TIME_RES:
			processNCP_RadarSensor_SendOnTime_Res(zigbee_Id, msg, length);
			break;
		case RADAR_SENSOR_SEND_EVENT_RES:
			processNCP_RadarSensor_SendEvent_Res(zigbee_Id, msg, length);
			break;
		case RADAR_SENSOR_SET_SENSING_LEVEL_RES:
			processNCP_RadarSensor_SetSensingLevelRes(zigbee_Id, msg, length);
			break;
	}	
}

void processNCP_RadarSensor_GetDeviceInfo_Res(uint16 zigbee_Id, uint8 msg[], int length){
}

void processNCP_RadarSensor_GetState_Res(uint16 zigbee_Id, uint8 msg[], int length){
}

void processNCP_RadarSensor_SendOnTime_Res(uint16 zigbee_Id, uint8 msg[], int length){
	uint16 nodeId;
	uint16 subNodeId;
	uint8 onTime;

	nodeId = tmp_zrmp.zrm_Addr;
	subNodeId = msg[1] << 8 | msg[2];
	onTime = msg[3];

#ifdef DEBUG_ENABLE
	printf_P(PSTR("\n processNCP_RadarSensor_SendOnTime_Res"));
#endif
	
	plcs_NCP_SendSendONTimeRes(nodeId, subNodeId, onTime);
}

void processNCP_RadarSensor_SendEvent_Res(uint16 zigbee_Id, uint8 msg[], int length){	
	uint16 i=0;	
	uint16 sensorId = msg[1] << 8 | msg[2];

	for(i=0; i < sensorSSRMappingTable.size; i++) {
		if(sensorSSRMappingTable.mappingInfo[i].sensorId == sensorId){
			if(tmp_SSRInfo.expirationTime[sensorSSRMappingTable.mappingInfo[i].ssrId] < sensorSSRMappingTable.mappingInfo[i].onTime || tmp_SSRInfo.expirationTime[sensorSSRMappingTable.mappingInfo[i].ssrId]==0xff){
				tmp_SSRInfo.timerCnt[sensorSSRMappingTable.mappingInfo[i].ssrId]=0;
				tmp_SSRInfo.expirationTime[sensorSSRMappingTable.mappingInfo[i].ssrId] = sensorSSRMappingTable.mappingInfo[i].onTime;
				tmp_SSRInfo.dimming_level[sensorSSRMappingTable.mappingInfo[i].ssrId] = sensorSSRMappingTable.mappingInfo[i].offDimmingLevel;
			}else {
				tmp_SSRInfo.timerCnt[sensorSSRMappingTable.mappingInfo[i].ssrId]=0;
			}
		}
	}
	
	plcs_NCP_SendEvent(tmp_zrmp.zrm_Addr, (msg[1] << 8 | msg[2]), 0x01);	
}

void processNCP_RadarSensor_SetSensingLevelRes(uint16 sensorAddr, uint8 msg[], int length){
	uint16 nodeId;
	uint16 sensorId;
	uint8 level;

	nodeId = tmp_zrmp.zrm_Addr;
	sensorId = msg[1] << 8 | msg[2];
	level = msg[3];
	
	plcs_NCP_SendSetSensingLevelRes(nodeId, sensorId, level);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Send
////////////////////////////////////////////////////////////////////////////////////////////////////////////

void RadarSensor_SendOnTimeReq(uint16 zigbee_Id, uint8 onTime){
	uint8 tmp_Buff[10];
	int len = 0;	
	device_Info *device_P;
#ifdef DEBUG_ENABLE
	printf_P(PSTR("\n RadarSensor_SendOnTimeReq"));
	printf_P(PSTR("\n ZigbeeID: 0x%04X"), zigbee_Id);
#endif
	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = RADAR_SENSOR_PROTOCOL_ID;
	tmp_Buff[len++] = RADAR_SENSOR_SEND_ON_TIME_REQ;		
	tmp_Buff[len++] = (uint8)(zigbee_Id >> 8);
	tmp_Buff[len++] = (uint8)(zigbee_Id);
	tmp_Buff[len++] = (uint8) onTime;	

	device_P = findNodeById(zigbee_Id);
	if(device_P == NULL) return;
#ifdef DEBUG_ENABLE
	printf_P(PSTR("\n nwkAddr: 0x%04X"), device_P->nwkAddr);
#endif
	sendData(device_P->nwkAddr, tmp_Buff, len);
}


void RadarSensor_SendSetSensingLevel(uint16 sensorId, uint8 level){
	uint8 tmp_Buff[10];
	int len = 0;	
	device_Info *device_P;
	
	tmp_Buff[len++] = NCP_PROTOCOL_ID;
	tmp_Buff[len++] = RADAR_SENSOR_PROTOCOL_ID;
	tmp_Buff[len++] = RADAR_SENSOR_SET_SENSING_LEVEL;		
	tmp_Buff[len++] = (uint8)(sensorId >> 8);
	tmp_Buff[len++] = (uint8)(sensorId);
	tmp_Buff[len++] = (uint8) level;	

	device_P = findNodeById(sensorId);
	if(device_P == NULL) return;

	sendData(device_P->nwkAddr, tmp_Buff, len);
}
