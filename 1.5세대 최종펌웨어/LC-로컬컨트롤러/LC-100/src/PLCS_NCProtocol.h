#ifndef __PLCS_NCP_PROTOCOL_H__
#define __PLCS_NCP_PROTOCOL_H__

#include "NCProtocol.h"

// ---------------------------------------------------------------------------------------------- //
#define PLCS_NCP_PROTOCOL_ID						(0x20) 	// Protocol ID
#define PLCS_NCP_VERSION         					(0x10)  	// Protocol Version
#define PLCS_LC_DEVICE_VER							0x10
#define PLCS_LC_FW_VER								0x1B

#define PLCS_LC_100Z_TYPE							(uint16)(0x2010)
#define PLCS_ZDIMMER_TYPE							(uint16)(0x2040)
#define PLCS_ZSENSOR_TYPE							(uint16)(0x2050)

#define PLCS_NCP_REQ_CONTROL						(0x30)	// 노드 제어요청
#define PLCS_NCP_RES_CONTROL						(0x31)	// 노드 제어요청 응답
#define PLCS_NCP_REQ_DIMMING						(0x32)	// 디밍 제어요청
#define PLCS_NCP_RES_DIMMING						(0x33)	// 디밍 제어요청 응답
#define PLCS_NCP_REQ_POWER_METER					(0x34)	// 전력량 요청
#define PLCS_NCP_RES_POWER_METER					(0x35)	// 전력량 요청 응답
#define PLCS_NCP_REQ_STATE_INFO						(0x36)	// 노드 상태 정보 요청
#define PLCS_NCP_RES_STATE_INFO						(0x37)	// 노드 상태 정보 요청 응답

#define PLCS_NCP_SEND_SCHEDULE						(0x41)	// 스케줄 정보 전달
#define PLCS_NCP_UPDATE_SCHEDULE					(0x42)	// 스케줄 정보 업데이트
#define PLCS_NCP_SEND_MAPPING_INFO					(0x43)	// 센서 맵핑 정보
#define PLCS_NCP_SEND_MAPPING_INFO_RES              (0x47)  //센서 매핑 정보 응답
#define PLCS_NCP_RESET_MAPPING_INFO					(0x80)
#define PLCS_NCP_SEND_DIMMER2SSR_MAPPING_INFO		(0x45)	// 디머 맵핑 정보
#define PLCS_NCP_SEND_DIMMER2SSR_MAPPING_INFO_RES	(0x46)	// 디머 맵핑 정보 응답
#define PLCS_NCP_RESET_DIMMER_MAPPING_INFO			(0x81)
#define PLCS_NCP_UPDATE_POWER_MERTER				(0x44)	// 전력량 정보 전달(Server -> Load Controller)

#define PLCS_NCP_SEND_EVENT							(0x50)	// 이벤트 정보 전달
#define PLCS_NCP_SEND_EVENT_NEIGHBOR				(0x55)	// 이웃노드 이벤트 정보 전달

#define PLCS_NCP_SEND_ON_TIME						(0x60)	// 센서 동작감지 차단 시간 전달
#define PLCS_NCP_RES_ON_TIME						(0x61)	// 센서 동작감지 차단 시간 전달 응답
#define PLCS_SNCP_SET_SENSING_LEVEL					(0x62)
#define PLCS_SNCP_SET_SENSING_LEVEL_RES				(0x63)

#define PLCS_NCP_SEND_PATTERN						(0x70)
#define PLCS_NCP_SEND_PATTERN_RES					(0x71)
#define PLCS_NCP_RESET_PATTERN						(0x72)

#define PLCS_NCP_DEBUG_LOG_REQ						(0x90)
#define PLCS_NCP_DEBUG_LOG_RES						(0x91)
#define PLCS_NCP_DEBUG_LOG_RESET					(0x92)
#define PLCS_NCP_DEBUG_LOG_LC_REQ					(0x93)
#define PLCS_NCP_DEBUG_LOG_LC_RES					(0x94)
#define PLCS_NCP_DEBUG_LOG_LC_RESET					(0x95)

// ---------------------------------------------------------------------------------------------- //

void plcs_NCP_ProcessMessage(uint8 msg[], int length);
void plcs_NCP_HandleControlReq(uint8 msg[], int length);
void plcs_NCP_HandleDimminReq(uint8 msg[], int length);
void plcs_NCP_PowerMeterReq(uint8 msg[], int length);
void plcs_NCP_StateInfo(uint8 msg[], int length);
void plcs_NCP_SendSchedule(uint8 msg[], int length);
void plcs_NCP_UpdateSchedule(uint8 msg[], int length);
void plcs_NCP_SendMappingInfo(uint8 msg[], int length);
void plcs_NCP_SendDimmer2SSRMappingInfo(uint8 msg[], int length);
void plcs_NCP_UpdatePowerMeter(uint8 msg[], int length);
void plcs_NCP_SendOnTime(uint8 msg[], int length);
void plcs_NCP_HandleResetMappingInfo(uint8 msg[], int length);
void plcs_NCP_HandleResetDimmer2SSRMappingInfo(uint8 msg[], int length);
void plcs_NCP_HandleDebugLogLCReq(uint8 msg[], int length);
void plcs_NCP_HandleDebugLogLCReset(uint8 msg[], int length);
void plcs_NCP_HandleSendPattern(uint8 msg[], int length);
void plcs_NCP_HandleResetPattern(uint8 msg[], int length);
void plcs_NCP_HandleSetSensingLevel(uint8 msg[], int length);
void plcs_NCP_HandleEventNeigbor(uint8 msg[], int length);

void plcs_NCP_SendSendONTimeRes(uint16 nodeId, uint16 subNodeId, uint8 onTime);
void plcs_NCP_SendControlRes(uint16 nodeId, uint8 ssrId, uint8 ctrlMode);
void plcs_NCP_SendDimmingRes(uint16 nodeId, uint16 subNodeId, uint8 dimmingLevel);
void plcs_NCP_SendPowerMeterRes(uint16 nodeId, uint32 powerMeterValue);
void plcs_NCP_SendStateInfoRes(uint8 nodeId, uint16 deviceType, uint16 subNodeId);
void plcs_NCP_SendEvent(uint16 tmp_node_id, uint16 tmp_dst_ID, uint8 DataFormat);
void plcs_NCP_SendDimmer2SSRMappingInfoRes(uint16 nodeId, uint16 dimmerId, uint8 ssrId, uint8 result);
void plcs_NCP_SendMappingInfoRes(uint16 nodeId, uint16 sensorId, uint8 ssrId, uint8 onTime, uint8 offDimmingLevel, uint8 result);
void plcs_NCP_SendDebugLogLCRes(uint16 nodeId, uint16 rebootCnt, uint8 lastRebootTime[], uint16 serverConnectionCnt, uint8 lastServerConnectionTime[]);
void plcs_NCP_SendPatternRes(uint16 nodeId, uint8 ssrId, uint8 result);
void plcs_NCP_SendSetSensingLevelRes(uint16 nodeId, uint16 sensorId, uint8 level);


#define LC_100Z_TYPE		(uint16)(0x2010)
#define LC_DEVICE_VER		(uint8)(0x10)
#define LC_FW_VER			(uint8)(0x10)

#define ZDIMMER_TYPE		(uint16)(0x2040)
#define ZSENSOR_TYPE		(uint16)(0x2050)

extern volatile uint32 g_PowerMeterValue;

#endif
