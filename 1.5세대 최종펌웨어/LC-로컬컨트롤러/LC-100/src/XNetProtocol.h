#ifndef __XNET_PROTOCOL_H__
#define __XNET_PROTOCOL_H__


#include <string.h>
#include "Util.h"
#include "NCProtocol.h"
#include "plcs_ncprotocol.h"
#include "radarSensorProtocol.h"
#include "CNCProtocol.h"
#include "Xcps.h"
#include "ZRMProtocol.h"
#include "TestDeviceProtocol.h"


// --------------- XNetMessage Handler --------------- //
void XNetHandler(uint16 zigbee_Id, uint8 buff[], int buff_length);

void XNetCommand(uint8 msg[], int buff_length);

void sendServerMessage(uint8 *msg, uint8 buff_length);

char sendStackedServerMessage();


#endif

