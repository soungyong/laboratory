
#include "ScheduleTime.h"

uint8 ee_ScheduleTimeSize[MaxNumOfSSR] EEMEM;
Schedule_Info ee_ScheduleTime[MaxNumOfSSR][MaxNumOfScheduleInfo] EEMEM;

void initScheduleTimeTable(){
	uint8 i=0;
	uint8 j=0;
	for(i=0; i < MaxNumOfSSR; i++) {
		scheduleTable.size[i] = 0;
		eeprom_read_block(&(scheduleTable.size[i]), &ee_ScheduleTimeSize[i], sizeof(uint8));	
		if(scheduleTable.size[i] >= MaxNumOfScheduleInfo)
			scheduleTable.size[i] = 0;
		
		for(j=0; j < scheduleTable.size[i]; j++) 
			eeprom_read_block(&(scheduleTable.scheduleInfo[i][j]), &(ee_ScheduleTime[i][j]), sizeof(Schedule_Info));
	}	
}

// --------------------------------------------------------------------------------------------------------- //
// ------------------ Add Schedule Table ---------------------------------------------------------------------- //
// --------------------------------------------------------------------------------------------------------- //
int addScheduleTable(uint8 ssrId, uint8 onStartTime, uint8 onEndTime)
{
	if(scheduleTable.size[ssrId] >= MaxNumOfScheduleInfo)
		return 0;
	if(ssrId >= MaxNumOfSSR) return 0;

	scheduleTable.scheduleInfo[ssrId][scheduleTable.size[ssrId]].onStartTime = onStartTime;
	scheduleTable.scheduleInfo[ssrId][scheduleTable.size[ssrId]].onEndTime = onEndTime;
	scheduleTable.size[ssrId]++;
	scheduleTable_WriteToEEPRom(ssrId, scheduleTable.size[ssrId]-1);	

	return 1;
}

uint8 isOnTime(uint8 ssrId, uint8 currentTime) {
	uint8 i=0;

	if(scheduleTable.size[ssrId]==0) return 1;

	for(i=0; i < scheduleTable.size[ssrId]; i++) {
		if(scheduleTable.scheduleInfo[ssrId][i].onStartTime == scheduleTable.scheduleInfo[ssrId][i].onEndTime){			
		} else if(scheduleTable.scheduleInfo[ssrId][i].onStartTime < scheduleTable.scheduleInfo[ssrId][i].onEndTime){
			if(scheduleTable.scheduleInfo[ssrId][i].onStartTime <= currentTime && scheduleTable.scheduleInfo[ssrId][i].onEndTime > currentTime)
				return 1;
		} else {
			if(scheduleTable.scheduleInfo[ssrId][i].onStartTime <= currentTime || scheduleTable.scheduleInfo[ssrId][i].onEndTime < currentTime)
				return 1;
		}
	}
		
	return 0;
}

uint8 isContainStartTime(uint8 ssrId, uint8 startTime)
{
	uint8 i = 0;	

	for(i=0; i < scheduleTable.size[ssrId]; i++)
		if(scheduleTable.scheduleInfo[ssrId][i].onStartTime == startTime)
			return 1;

	return 0;
}

uint8 isContainEndTime(uint8 ssrId, uint8 endTime)
{
	uint8 i = 0;	
	for(i=0; i < scheduleTable.size[ssrId]; i++)
		if(scheduleTable.scheduleInfo[ssrId][i].onEndTime == endTime)
			return 1;

	return 0;
}

void scheduleTable_WriteToEEPRom(uint8 ssrId, uint8 index){
	eeprom_write_block(&(scheduleTable.size[ssrId]), &ee_ScheduleTimeSize[ssrId], sizeof(uint8));		
	eeprom_write_block(&(scheduleTable.scheduleInfo[ssrId][index]), &(ee_ScheduleTime[ssrId][index]), sizeof(Schedule_Info));	
}

void resetScheduleTimeTable(uint8 ssrId){
	scheduleTable.size[ssrId]=0;
	scheduleTable_WriteToEEPRom(ssrId, 0);
}
// End of File
